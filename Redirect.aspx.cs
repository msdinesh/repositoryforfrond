using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Configuration;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;

namespace Frond
{
	/// <summary>
	/// The re-direct page redirects to other pages within Frond, or even to pages in other Bonobo Apps.
	/// It is needed for two reasons:
	///  - some pages need several BnbEntity parameters - this page can use one to work out the others before re-directing
	///  - some reports need hyperlinks but only have vol refs or placement refs - this page can work out the correct IDs from the Ref No or Placement Refs
	///  - when transferring to another application, session transfer keys need to be generated and passed, which this page can do.
	/// Currently accepts the following types of querystring:
	///		BnbSomething=guid
	///			will re-direct to appropriate page within Frond
	///		PLACEREF=placementref
	///			will translate ref to an ID and re-direct to appropriate page within Frond
	///		TransferTo=app&BnbSomething=guid
	///			will re-direct to the appropriate page of the appropriate app
	///			(this may be done by calling the equivalent redirect page in the target app)
	/// </summary>
	public partial class Redirect : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			this.AutoRedirect();

			
			

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


		// uses first querystring parameter to decide where to re-direct user to
		private void AutoRedirect()
		{
			// if transferto is specified, call other proc
			if (Request.QueryString["TransferTo"] != null && Request.QueryString["TransferTo"] != "")
			{
				// ignore if it says frond!
				// (fix for raja's changes to Renderer)
				if (this.Request.QueryString["TransferTo"] != "Frond")
				{
					this.TransferToOtherApp();
					return;
				}
			}
			// otherwise, we are transferring within this app
			int paramIndex = this.FindFirstParamNotLike("TransferTo");
			string queryParam = this.Request.QueryString.Keys[paramIndex];
			string queryValue = this.Request.QueryString[paramIndex];
			string reDirectURL = null;
			Guid guidQueryValue = Guid.Empty;
			bool valueIsGuid = BnbRuleUtils.IsGUID(queryValue);
			if (valueIsGuid)
				guidQueryValue = new Guid(queryValue);

			switch(queryParam)
			{
				case("BnbPosition"):
					if (valueIsGuid)
						reDirectURL = String.Format("EditPages/PlacementPage.aspx?BnbPosition={0}",guidQueryValue);
					break;
				case("BnbApplication"):
					reDirectURL = String.Format("EditPages/ApplicationPage.aspx?BnbApplication={0}",queryValue);
					break;
				case("BnbIndividual"):
					reDirectURL = Redirect.GetIndividualRedirectURL(new Guid(queryValue));
					break;
				case("BnbFunding"):
					reDirectURL = Redirect.GetFundingRedirectURL(new Guid(queryValue));
					break;
				case("BnbVirtualFunder"):
					reDirectURL = Redirect.GetVirtualFunderRedirectURL(new Guid(queryValue));
					break;
                case("BnbProject"):
                    reDirectURL = String.Format("EditPages/ProjectPage.aspx?BnbProject={0}", queryValue);
                    break;
                case("BnbGrant"):
                    reDirectURL = Redirect.GetGrantRedirectURL(new Guid(queryValue));
                    break;
				case("PLACEREF"):
					Guid positionID = Redirect.SearchByPositionRef(queryValue);
					if (positionID != Guid.Empty)
						reDirectURL = String.Format("EditPages/PlacementPage.aspx?BnbPosition={0}",positionID);
					break;
					
				case("REFNO"):
					Guid individualID = Redirect.SearchByRefNo(queryValue);
					if (individualID!=Guid.Empty)
					{
						reDirectURL = Redirect.GetIndividualRedirectURL(individualID);
						if (reDirectURL == "")
							throw new ApplicationException(String.Format("Individual {0} is not a volunteer and Frond cannot display non-volunteers at the moment",
									queryValue));
					}
					break;
					
				default:
					reDirectURL = null;
					break;
			}
			if (reDirectURL == null)
				throw new ApplicationException(String.Format("Redirect.aspx did not know how to parse Querystring {0}={1}",
					queryParam, queryValue));

			hyperRedirect.NavigateUrl = reDirectURL;
			this.Response.Redirect(reDirectURL);
			
		}

        private static string GetGrantRedirectURL(Guid grantID)
        {
            BnbGrant tempGrant = BnbGrant.Retrieve(grantID);
            // is it a contract or a proposal?
            if (tempGrant.IsContract)
                return String.Format("EditPages/ContractPage.aspx?BnbProject={0}&BnbGrant={1}",
                    tempGrant.ProjectID.ToString().ToUpper(), tempGrant.ID.ToString().ToUpper());
            else
                return String.Format("EditPages/ProposalPage.aspx?BnbProject={0}&BnbGrant={1}",
                    tempGrant.ProjectID.ToString().ToUpper(), tempGrant.ID.ToString().ToUpper());

        }

		private void TransferToOtherApp()
		{
			string appName = Request.QueryString["TransferTo"];
			int paramIndex = this.FindFirstParamNotLike("TransferTo");
			
			string queryParam = null;
			string queryValue = null;			
			
			// if another parameter found
			if (paramIndex > -1)
			{
				// get it into vars so we can pass it on
				queryParam = this.Request.QueryString.Keys[paramIndex];
				queryValue = this.Request.QueryString[paramIndex];			
			}

            // make a hashtable of the querystring params
            Hashtable otherParams = new Hashtable();
            foreach (string keyLoop in this.Request.QueryString.Keys)
                otherParams.Add(keyLoop, this.Request.QueryString[keyLoop]);
            // strip out the transferTo param
            otherParams.Remove("TransferTo");

			bool isLive = !BnbEngine.SessionManager.GetConfigurationInfo().IsTestDatabase;
			string appUrl = "";
			string serverApp = "";

			switch(appName)
			{
                    //To be Removed Starfish-frond integration
                //case("Starfish"):
                //    if (isLive)
                //        serverApp = ConfigurationSettings.AppSettings["RedirectStarfishLive"].ToString();
                //    else
                //        // if not live, try starfish2 on the same server
                //        serverApp = ConfigurationSettings.AppSettings["RedirectStarfishTraining"].ToString();

                //    if (queryParam != null)
                //        appUrl = serverApp + "Redirect.aspx?{1}={2}&TransferKey={0}";
                //    else
                //        appUrl = serverApp + "SFMenu.aspx?TransferKey={0}";

                //    break;
				case("PGMS"):
					// please note that transfers only work when both apps log
					// into the same database!
					if (isLive)
						serverApp = ConfigurationSettings.AppSettings["RedirectPGMSLive"].ToString();
					else
						serverApp = serverApp = ConfigurationSettings.AppSettings["RedirectPGMSTraining"].ToString();
					
					appUrl = serverApp + "History.aspx?TransferKey={0}";
					break;
				case("BonoboMerge"):
					// please note that transfers only work when both apps log
					// into the same database!
					if (isLive)
						serverApp = ConfigurationSettings.AppSettings["RedirectBonoboMergeLive"].ToString();
					else
						serverApp = ConfigurationSettings.AppSettings["RedirectBonoboMergeTraining"].ToString();
					appUrl = serverApp; 
					break;
                case("VsoDms"):
                    // please note that transfers only work when both apps log
                    // into the same database!
                    if (isLive)
                        serverApp = ConfigurationSettings.AppSettings["RedirectVsoDmsLive"].ToString();
                    else
                        serverApp = ConfigurationSettings.AppSettings["RedirectVsoDmsTraining"].ToString();

                    // which page we are going to depends on params
                    if (otherParams.ContainsKey("N"))
                        serverApp = serverApp + "ViewDocAsHtml.aspx";
                    else
                        serverApp = serverApp + "mainpage.aspx";

                    appUrl = serverApp + "?TransferKey={0}";
                    // pass on other parameters if there are any
                    if (otherParams.Count > 0)
                    {
                        foreach (string key in otherParams.Keys)
                            appUrl = appUrl + String.Format("&{0}={1}",
                                         key, otherParams[key].ToString());

                    }
                    break;
			}

			if (appUrl == "")
				throw new ApplicationException(String.Format("Redirect.aspx did not know how to parse AppName {0}",
					appName));

			// get a key to transfer session
			Guid transferKey = BnbEngine.SessionManager.GetSessionTransferKey();
			string destinationUrl = String.Format(appUrl, transferKey.ToString(), queryParam, queryValue );
			hyperRedirect.NavigateUrl = destinationUrl;
			this.Response.Redirect(destinationUrl);


		}

		/// <summary>
		/// inspects the querystring and returns the index of the first parameter that
		/// does NOT match the notLike param passed in.
		/// Returns -1 if none found
		/// </summary>
		/// <returns></returns>
		private int FindFirstParamNotLike(string notLike)
		{
			int check = 0;
			int found = -1;
			while ((check < Request.QueryString.Count) && found == -1)
			{
				string paramName = Request.QueryString.Keys[check];
				if (paramName!=notLike)
					found = check;

				check = check + 1;
			}
			return found;

		}

		/// <summary>
		/// Returns the correct URL for the Funding page based on the passed in fundingID
		/// </summary>
		/// <param name="fundingID"></param>
		/// <returns></returns>
		public static string GetFundingRedirectURL(Guid fundingID)
		{
			BnbFunding tempFunding = BnbFunding.Retrieve(fundingID);
			Guid posID = tempFunding.PositionID;
			Guid appID = tempFunding.ApplicationID;

			if (appID != Guid.Empty)
				return String.Format("EditPages/FundingPage.aspx?BnbApplication={0}&BnbFunding={1}",
						appID.ToString().ToUpper(), fundingID.ToString().ToUpper());

			if (posID != Guid.Empty)
				return String.Format("EditPages/FundingPage.aspx?BnbPosition={0}&BnbFunding={1}",
					posID.ToString().ToUpper(), fundingID.ToString().ToUpper());

			// if neither ID is found then thats an error
			throw new ApplicationException(String.Format("Funding record {0} is not linked to an application or a position so could not re-direct",
						fundingID));
		}

		/// <summary>
		/// Returns the correct url for the Frond placement page (currently FundedPlacement)
		/// based on the passed-in ID
		/// </summary>
		/// <param name="positionID"></param>
		/// <returns></returns>
		public static string GetPositionRedirectURL(Guid positionID)
		{
			
			return String.Format("EditPages/PlacementPage.aspx?BnbPosition={0}",
				positionID.ToString().ToUpper());
		}



		/// <summary>
		/// Returns the correct url for the Frond organisation page (currently FundedPlacement)
		/// based on the passed-in ID
		/// </summary>
		/// <param name="positionID"></param>
		/// <returns></returns>
		public static string GetVirtualFunderRedirectURL(Guid virtualFunderID)
		{
			BnbQuery findIndv = new BnbQuery("vwBonoboFindIndividualFunder", "tblIndividualKeyInfo_IndividualID", virtualFunderID.ToString());
			DataTable indvResults = findIndv.Execute().Tables["Results"];
			if (indvResults.Rows.Count > 0)
				return String.Format("EditPages/IndividualPage.aspx?BnbIndividual={0}", virtualFunderID.ToString().ToUpper());
			else
				return String.Format("EditPages/OrganisationPage.aspx?BnbCompany={0}", virtualFunderID.ToString().ToUpper());
		}

		/// <summary>
		/// Returns the correct URL for the Frond volunteer page (currently FundedVolunteers)
		/// based on the passed-in ID
		/// If the individual is not a volunteer, currently returns empty string
		/// </summary>
		/// <param name="individualID"></param>
		/// <returns></returns>
		public static string GetIndividualRedirectURL(Guid individualID)
		{
			BnbIndividual tempIndv = BnbDomainObject.GeneralRetrieve(individualID, typeof(BnbIndividual)) as BnbIndividual;
			// are they a vol?
			if (tempIndv.CurrentApplication != null)
				return String.Format("EditPages/ApplicationPage.aspx?BnbApplication={0}",
					tempIndv.CurrentApplication.ID.ToString().ToUpper());
			else
				return String.Format("EditPages/IndividualPage.aspx?BnbIndividual={0}",
					individualID.ToString().ToUpper());
		}

		/// <summary>
		/// Returns the IndividualID corresponding to the specified RefNo
		/// If not found, returns empty Guid
		/// </summary>
		/// <param name="refNo"></param>
		/// <returns></returns>
		public static Guid SearchByRefNo(string refNo)
		{
			BnbQuery findIndv = new BnbQuery("vwBonoboFindIndividualGC", "tblIndividualKeyInfo_old_indv_id", refNo);
			DataTable indvResults = findIndv.Execute().Tables["Results"];
			if (indvResults.Rows.Count > 0)
				return (Guid)indvResults.Rows[0]["tblIndividualKeyInfo_IndividualID"];

			return Guid.Empty;

		}


		/// <summary>
		/// Returns the PositionID corresponding to the specified PositionRef/Placement Ref
		/// If not found, returns empty Guid
		/// </summary>
		/// <param name="positionRef"></param>
		/// <returns></returns>
		public static Guid SearchByPositionRef(string positionRef)
		{
			BnbQuery findObject = new BnbQuery("vwBonoboFindPosition", "Custom_FullPositionReference", positionRef);
			DataTable queryResults = findObject.Execute().Tables["Results"];
			if (queryResults.Rows.Count > 0)
				return (Guid)queryResults.Rows[0]["tblPosition_PositionID"];

			return Guid.Empty;
		}


        public static string GetEmployerRedirectURL(Guid employerID)
        {
            return String.Format("EditPages\\EmployerPage.aspx?BnbEmployer={0}",
                employerID.ToString());
        }

        public static string GetRoleRedirectURL(Guid roleID)
        {
            BnbRole tempRole = BnbDomainObject.GeneralRetrieve(roleID, typeof(BnbRole)) as BnbRole;
            return String.Format("EditPages\\JobPage.aspx?BnbEmployer={0}&BnbRole={1}",
                tempRole.EmployerID, tempRole.ID);
        }

        /// <summary>
        /// Returns the RoleID corresponding to the specified RoleRef
        /// If not found, returns empty Guid
        /// </summary>
        /// <param name="roleRef"></param>
        /// <returns></returns>
        public static Guid SearchByRoleRef(string roleRef)
        {
            BnbQuery findObject = new BnbQuery("vwBonoboFindRole", "Custom_FullRoleReference", roleRef);
            DataTable queryResults = findObject.Execute().Tables["Results"];
            if (queryResults.Rows.Count > 0)
                return (Guid)queryResults.Rows[0]["tblRole_RoleID"];

            return Guid.Empty;
        }


        /// <summary>
        /// Returns the EmployerID corresponding to the specified EmployerRef
        /// If not found, returns empty Guid
        /// </summary>
        /// <param name="employerRef"></param>
        /// <returns></returns>
        public static Guid SearchByEmployerRef(string employerRef)
        {

            BnbQuery findObject = new BnbQuery("vwBonoboFindEmployer", "Custom_FullEmployerReference", employerRef);
            DataTable queryResults = findObject.Execute().Tables["Results"];
            if (queryResults.Rows.Count > 0)
                return (Guid)queryResults.Rows[0]["tblEmployer_EmployerID"];

            return Guid.Empty;
        }

        public static string GetProgrammeRedirectURL(Guid programmeID)
        {
            return String.Format("EditPages\\ProgrammePage.aspx?BnbProgramme={0}",
                programmeID.ToString());
        }

        /// <summary>
        /// Returns the ProgrammeID corresponding to the specified ProgrammeRef
        /// If not found, returns empty Guid
        /// </summary>
        /// <param name="programmeRef"></param>
        /// <returns></returns>
        public static Guid SearchByProgrammeRef(string programmeRef)
        {
            BnbQuery findObject = new BnbQuery("vwBonoboFindProgramme", "tblProgramme_ProgrammeRef", programmeRef);
            DataTable queryResults = findObject.Execute().Tables["Results"];
            if (queryResults.Rows.Count > 0)
                return (Guid)queryResults.Rows[0]["tblProgramme_programmeID"];

            return Guid.Empty;
        } 		
	}
}
