using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;

namespace Frond
{
	/// <summary>
	/// Summary description for SFError.
	/// </summary>
	public partial class Error : System.Web.UI.Page
	{
	
		private int m_errorTicketID = -1;
		private string m_queryErrorMessage = "";

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (BnbEngine.SessionManager.HasBonoboSessionStarted())
				BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);

			// get errorTicketID from querystring
			if (this.Request.QueryString["ErrorTicketID"] != null)
				m_errorTicketID = int.Parse(this.Request.QueryString["ErrorTicketID"]);

			// get error message from querystring
			// (this is a backup incase error logging failed)
			if (this.Request.QueryString["ErrorMessage"] != null)
				m_queryErrorMessage = this.Request.QueryString["ErrorMessage"];

			if (!this.IsPostBack)
				this.ShowErrorInfo(m_errorTicketID);


		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void ShowErrorInfo(int errorTicketID)
		{
			string errorSummary = "";
			string errorDetails = "";
			if (errorTicketID > -1)
			{
				DataTable errorInfo = BnbEngine.SessionManager.RetrieveLoggedError(errorTicketID);
				DataRow errorRow = errorInfo.Rows[0];
				errorSummary = String.Format("<strong>ErrorTicketID = {0}</strong><br/>"
					+ "ErrorMessage = {1}<br/>ErrorPage = {2}<br/>ErrorTime = {3}",
					errorRow["ErrorTicketID"],
					errorRow["ExceptionMessage"],
					errorRow["PageURL"],
					((DateTime)errorRow["TimeStamp"]).ToString("dd/MMM/yyyy HH:mm:ss"));
				
				errorDetails = String.Format("ErrorClass = {0}<br/>"
					+"StackTrace:<br/>{1}",
					errorRow["ExceptionClass"],
					errorRow["ExceptionStack"].ToString().Replace("\n","<br/>").Replace("--->","<br/>--->"));
				
			}
			else
			{
				errorSummary = String.Format("<strong>ErrorTicketID = {0}</strong><br/>"
					+ "ErrorMessage = {1}<br/>ErrorPage = {2}<br/>ErrorTime = {3}",
					"(Not logged)",
					m_queryErrorMessage,
					"(Unknown)",
					DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss"));
				
				errorDetails = String.Format("No Details available");
			}
			labelErrorSummary.Text = errorSummary;
			labelErrorDetails.Text = errorDetails;
				

		}

		protected void linkButtonMoreDetails_Click(object sender, System.EventArgs e)
		{
			panelErrorDetails.Visible = true;
		}
	}
}
