using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using System.Text;
using BonoboWebControls.DataGrids;
using BonoboWebControls;
using System.IO;

namespace Frond
{
	/// <summary>
	/// Summary description for SFMenu.
	/// </summary>
	public partial class Menu : System.Web.UI.Page
    {

        #region " Local Declarations "

        //Local Declartion for Calculation of OverAllGridView 
        decimal numberOfProjectsTotal = 0;
        decimal liveFullyTotal = 0;
        decimal livePartialTotal = 0;
        decimal projectBudgetTotal = 0;
        decimal fundsSecuredTotal = 0;
        decimal matchedFundingTotal = 0;
        decimal placementFillTotal = 0;
        decimal requiredTotal = 0;
        decimal priorityProjectTotal = 0;
        //Local Declartion for Calculation of FiscalYearGridView 
        decimal numberOfProjectsTotalFiscalReport = 0;
        decimal liveFullyTotalFiscalReport = 0;
        decimal livePartialTotalFiscalReport = 0;
        decimal projectBudgetTotalFiscalReport = 0;
        decimal fundsSecuredTotalFiscalReport = 0;
        decimal matchedFundingTotalFiscalReport = 0;
        //TPT Added for PGMSSRONE-2
        decimal incomeReceivedTotalFiscalReport = 0;
        decimal incomeExpectedTotalFiscalReport = 0;
        decimal placementFillTotalFiscalReport = 0;
        decimal requiredTotalFiscalReport = 0;
        decimal priorityProjectTotalFiscalReport = 0;
        //TPT Amended:PGMSSRONE-3
        //Local Declaration for Calculation of gvVolCount
        decimal numberOfRestrictedVol = 0;
        decimal numberOfUnrestrictedVol = 0;

        protected BonoboWebControls.DataGrids.BnbDataGridForView bnbvwVolunteers;
		protected UserControls.Title titleBar;
		protected UserControls.MenuBar menuBar;
        int alertColumn = 2;
        bool showingTabs = false;
		//private int m_ProgrammeOfficeID = -1;

		//private bool m_isUserFromProgrammeOffice = false;
		protected System.Web.UI.WebControls.HyperLink htpTest;
		//private bool m_isUserFromOverseasProgrammeManagement = false;
	
        private int m_ProgrammeOfficeID = -1;

       
        private bool m_isUserFromProgrammeOffice = false;
        private bool m_isUserFromOverseasProgrammeManagement = false;

        #endregion

        #region " Page_Load "

        protected void Page_Load(object sender, System.EventArgs e)
		{
			if (!this.IsPostBack)
			{
				titleBar.TitleText = "Main Menu";
				uxVirtualAppName.Text = menuBar.VirtualApplicationName;				

                //Reports Link 
                hlAllProjects.NavigateUrl = "javascript:void(0);";
                hlAllGrants.NavigateUrl = "javascript:void(0);";
                hlAllProjects.Attributes.Add("onclick", "javascript:" + GetPopupWindowString("Reports/ExcelReport.aspx?ViewName=vwBonoboAllProjectReports", "AllProjectReports"));
                hlAllGrants.Attributes.Add("onclick", "javascript:" + GetPopupWindowString("Reports/ExcelReport.aspx?ViewName=vwBonoboAllContractReports", "AllContractReports"));
                hlAllLiveProjects.NavigateUrl = "javascript:void(0);";
                hlAllLiveProjects.Attributes.Add("onclick", "javascript:" + GetPopupWindowString("Reports/ExcelReport.aspx?ViewName=vwBonoboLiveProjectReports", "AllLiveProjectReports"));
                hlAllIncomeClaimReports.NavigateUrl = "javascript:void(0);";
                hlAllIncomeClaimReports.Attributes.Add("onclick", "javascript:" + GetPopupWindowString("Reports/ExcelReport.aspx?ViewName=vwBonoboIncomeClaimReports", "AllIncomeClaimReports"));
                hlAllReportingDeadLine.NavigateUrl = "javascript:void(0);";
                hlAllReportingDeadLine.Attributes.Add("onclick", "javascript:" + GetPopupWindowString("Reports/ExcelReport.aspx?ViewName=vwBonoboReportingDeadline", "AllReportingDeadLineReports"));
                hlAllProjectsHighRisk.NavigateUrl = "javascript:void(0);";
                hlAllProjectsHighRisk.Attributes.Add("onclick", "javascript:" + GetPopupWindowString("Reports/ExcelReport.aspx?ViewName=vwBonoboHighRiskProjectReports", "AllHighRiskProjectReports"));
                hlAllPriorityProject.NavigateUrl = "javascript:void(0);";
                hlAllPriorityProject.Attributes.Add("onclick", "javascript:" + GetPopupWindowString("Reports/ExcelReport.aspx?ViewName=vwBonoboAllPriorityProjects", "AllPriorityProjectReports"));

                // for Snap Shot Reports
                SnapShotUserAccessPermission();
                SnapShotOverAllReport();
                SnapShotFiscalYearReport();
                SnapShotMyProjectReportForHighPriorityProject();
                SnapShotMyProjectReport();

                //TPT Amended: PGMSSRONE-3
                SnapShotVolunteerFundingCount();
			}

            this.MaintainScrollPositionOnPostBack = true;
			// Put user code to initialize the page here
			if(ClientScript.IsStartupScriptRegistered("ForceDefaultToScript")) return;
            ClientScript.RegisterStartupScript(this.GetType(),"ForceDefaultToScript", getKeyPressedScript());
			textBoxQuickStart.Attributes.Add("onkeydown","fnTrapKeyPress(" + buttonQuickStart.ClientID + ", event)");
			
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
            m_ProgrammeOfficeID = BnbEngine.SessionManager.GetSessionInfo().DefaultProgrammeOfficeID;
            //m_ProgrammeOfficeID = 1012;
            m_isUserFromProgrammeOffice =  (m_ProgrammeOfficeID != -1);
            m_isUserFromOverseasProgrammeManagement =  (BnbEngine.SessionManager.GetSessionInfo().DepartmentID == 3015);
            
            bool visible = (m_isUserFromProgrammeOffice || m_isUserFromOverseasProgrammeManagement);

            bnbvwVolunteers.ViewName += (m_isUserFromOverseasProgrammeManagement) ? "ForRPOs" : "";
       
            pnlVolunteersGrid.Visible = visible;
            bnbvwVolunteers.Visible = visible;
            pnlCOuntMatrix.Visible = visible;
            if (visible)
            {
                doAddHyperlinksToDataGrid();
                doFilterDataGridForViewControl();
            }

            if (pnlCOuntMatrix.Visible)
                RenderCountMatrix(bnbvwVolunteers.Results);
            if (bnbvwVolunteers.Visible)
                ApplyStyleToAlertColumn();
		}

        #endregion

        #region " GetPopupWindowString "

        public string GetPopupWindowString(string url, string popuprefName)
        {
            string s = String.Format("window.open('{0}','{1}','menubar=yes,scrollbars=yes,top=0,left=0,width=500,height=400,resizable=yes');", url, popuprefName);
            return s;
        }

        #endregion

        #region " getKeyPressedScript "

        private string getKeyPressedScript() 
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("<SCRIPT language=\"javascript\">" + Environment.NewLine);
			sb.Append("function fnTrapKeyPress(btn, event){" + Environment.NewLine);
			sb.Append("   if (event.keyCode == 13) {" + Environment.NewLine);
			sb.Append("      event.returnValue=false;" + Environment.NewLine);
			sb.Append("      event.cancel = true;" + Environment.NewLine);
			sb.Append("      btn.click();" + Environment.NewLine);
			sb.Append("   }" + Environment.NewLine);
			sb.Append("} " + Environment.NewLine);
			sb.Append("</SCRIPT>" + Environment.NewLine);
			return sb.ToString();
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			this.buttonQuickStart.Click +=new EventHandler(buttonQuickStart_Click);
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

        #region " buttonQuickStart_Click "

        protected void buttonQuickStart_Click(object sender, System.EventArgs e)
		{
			textBoxQuickStart.Text = System.Web.HttpUtility.HtmlEncode(textBoxQuickStart.Text);

			if (textBoxQuickStart.Text == "")
				labelQuickStartFeedback.Text = "Please enter a ref in the box and press Go again.";
			else
			{
				string parseRef = textBoxQuickStart.Text.Trim();
				string domainObjectType = null;
				BnbRuleUtils.ParseReference(ref parseRef, ref domainObjectType);
                BnbQuery findObject;
                DataTable queryResults;
				switch(domainObjectType)
				{
                    case ("BnbProgramme"):
                        Guid programmeID = Redirect.SearchByProgrammeRef(parseRef);
                        if (programmeID != Guid.Empty)
                        {
                            Response.Redirect(Redirect.GetProgrammeRedirectURL(programmeID));
                        }
                        else labelQuickStartFeedback.Text = String.Format("Programme with Refno {0} could not be found",
                                 parseRef);
                        break;
                        //Integration
					case("BnbIndividual"):
						Guid individualID = Redirect.SearchByRefNo(parseRef);
						if (individualID != Guid.Empty)
						{
							string url = Redirect.GetIndividualRedirectURL(individualID);
							if (url!="")
								Response.Redirect(url);
							else
								labelQuickStartFeedback.Text = String.Format("Individual with Refno {0} is not a Volunteer",
									parseRef);
						}
						else
							labelQuickStartFeedback.Text = String.Format("Volunteer with Refno {0} could not be found",
								parseRef);
						break;
					case("BnbEmployer"):
                        Guid employerID = Redirect.SearchByEmployerRef(parseRef);
                        if (employerID != Guid.Empty)
                        {
                            Response.Redirect(Redirect.GetEmployerRedirectURL(employerID));
                        }
                        else
                            labelQuickStartFeedback.Text = String.Format("Employer with Reference {0} could not be found",
                                parseRef);
                        break;
					case("BnbRole"):
                    
                        Guid roleID = Redirect.SearchByRoleRef(parseRef);
                        if (roleID != Guid.Empty)
                        {
                            Response.Redirect(Redirect.GetRoleRedirectURL(roleID));
                        }
                        else
                            labelQuickStartFeedback.Text = String.Format("Job with Reference {0} could not be found",
                                parseRef);
                        break;
					case("BnbPosition"):
						Guid positionID = Redirect.SearchByPositionRef(parseRef);
						if (positionID != Guid.Empty)
						{
							Response.Redirect(Redirect.GetPositionRedirectURL(positionID));
						}
						else
							labelQuickStartFeedback.Text = String.Format("Placement with Reference {0} could not be found",
								parseRef);
						break;
					default:
						labelQuickStartFeedback.Text = String.Format("Could not understand reference {0} ... please check it and try again.",
							parseRef);
						break;
				}
			}
        }

        #endregion

        #region " doFilterDataGridForViewControl "
        //<!-- stf-67
        private void doFilterDataGridForViewControl()
        {
            //if(!m_isUserFromProgrammeOffice) return;

            //filter programme office id
            BnbCriteria criteria = new BnbCriteria();
            if (m_isUserFromProgrammeOffice)
            {
                BnbNumericCondition progoffice = new BnbNumericCondition("lkuCountry_ProgrammeOfficeID", m_ProgrammeOfficeID, BnbNumericCompareOptions.EqualTo);
                criteria.QueryElements.Add(progoffice);
            }
            bnbvwVolunteers.Criteria = criteria;
        }
        //-->

        #endregion

        #region " doAddHyperlinksToDataGrid "

        private void doAddHyperlinksToDataGrid()
        {
            //add hyperlink to datagrid for Ref No
            BnbDataGridHyperlink refnolink = new BnbDataGridHyperlink();
            refnolink.HrefTemplate = "EditPages/ApplicationPage.aspx?BnbApplication={0}";
            refnolink.ColumnNameToRenderControl = "tblIndividualKeyInfo_old_indv_id";
            refnolink.KeyColumnNames = new string[] { "tblApplication_ApplicationID" };

            //add hyperlink to datagrid for Placement Ref
            BnbDataGridHyperlink placereflink = new BnbDataGridHyperlink();
            placereflink.HrefTemplate = "EditPages/PlacementPage.aspx?BnbPosition={0}";
            placereflink.ColumnNameToRenderControl = "Custom_FullPositionReference";
            placereflink.KeyColumnNames = new string[] { "tblPosition_PositionID" };

            bnbvwVolunteers.ColumnControls.Add(refnolink);
            bnbvwVolunteers.ColumnControls.Add(placereflink);
            if (!m_isUserFromOverseasProgrammeManagement) return;

            BnbDataGridGroupTabs tabs = new BnbDataGridGroupTabs();
            tabs.KeyColumnName = "lkuCountry_Description";
            tabs.XmlUrlForGroupList = @"Xml\Regions.xml";
            tabs.CssClassForActiveTab = "activetab";
            tabs.CssClassForInactiveTab = "inactivetab";
            tabs.CssClassForDisabledTab = "deadtab";
            tabs.TabsType = DataGridTabsType.TopRowVertical;
            bnbvwVolunteers.DataGridGroupTabs = tabs;
            showingTabs = true;
        }

        #endregion

        #region " RenderCountMatrix "

        private void RenderCountMatrix(DataTable gridResults)
        {
            tblCountMatrix.Rows.Clear();

            if (gridResults.Rows.Count > 0)
            {
                int countryColIdx = gridResults.Columns["lkuCountry_Description"].Ordinal;
                int vsoGroupIDColIdx = gridResults.Columns["lkuVSOType_VSOGroupID"].Ordinal;

                RenderHeader();

                HtmlTableRow row;
                HtmlTableCell cell;

                string country = gridResults.Rows[0].ItemArray[countryColIdx].ToString();

                BnbLookupDataTable goalsTable = BnbEngine.LookupManager.GetLookup("lkuMonitorObjective");
                string goal = string.Empty;
                DataRow[] rowCollection = null;

                int countLTV = 0;
                int countSTV = 0;
                int countYouth = 0;
                int countCUSO = 0;
                int countGX = 0;

                foreach (BnbLookupRow lookupRow in goalsTable)
                {
                    goal = lookupRow.Description;
                    goal = goal.Replace("'", "''");
                    rowCollection = gridResults.Select("Custom_LeadMonitorObjective ='" + goal + "'");

                    int count = 0;
                    int rowCount = 0;

                    if (rowCollection.Length > 0)
                    {
                        count = 0;
                        rowCount = 0;

                        row = new HtmlTableRow();

                        cell = new HtmlTableCell();
                        cell.InnerText = country + " " + lookupRow.Description;
                        cell.Attributes.Add("class", "volCountMatrixRowFirst");
 
                        row.Cells.Add(cell);

                        rowCollection = gridResults.Select("Custom_LeadMonitorObjective ='" + goal
                                        + "' and lkuVSOType_VSOGroupID ='" + BnbConst.VSOGroup_VSO +"'");

                        count = rowCollection.Length;
                        rowCount += count;
                        countLTV += count;

                        cell = new HtmlTableCell();
                        cell.InnerText = count.ToString();
                        cell.Attributes.Add("class", "volCountMatrixRowRest");

                        row.Cells.Add(cell);

                        rowCollection = gridResults.Select("Custom_LeadMonitorObjective ='" + goal
                                       + "' and lkuVSOType_VSOGroupID ='" + BnbConst.VSOGroup_STP + "'");

                        count = rowCollection.Length;
                        rowCount += count;
                        countSTV+=count;

                        cell = new HtmlTableCell();
                        cell.InnerText = count.ToString();
                        cell.Attributes.Add("class", "volCountMatrixRowRest");

                        row.Cells.Add(cell);

                        rowCollection = gridResults.Select("Custom_LeadMonitorObjective ='" + goal
                                       + "' and lkuVSOType_VSOGroupID ='" + BnbConst.VSOGroup_YouthProgrammes + "'");

                        count = rowCollection.Length;
                        rowCount += count;
                        countYouth+=count;

                        cell = new HtmlTableCell();
                        cell.InnerText = count.ToString();
                        cell.Attributes.Add("class", "volCountMatrixRowRest");

                        row.Cells.Add(cell);

                        //rowCollection = gridResults.Select("Custom_LeadMonitorObjective ='" + goal
                        //               + "' and lkuVSOType_VSOGroupID ='" + BnbConst.VSOGroup_Exchanges + "'");

                        //count = rowCollection.Length;
                        //rowCount += count;
                        //countGX+=count;

                        //cell = new HtmlTableCell();
                        //cell.InnerText = count.ToString();
                        //cell.Attributes.Add("class", "volCountMatrixRowRest");

                        //row.Cells.Add(cell);

                        //TPT Ammended:VAF-1057
                        //rowCollection = gridResults.Select("Custom_LeadMonitorObjective ='" + goal
                        //               + "' and lkuVSOType_VSOGroupID ='" + BnbConst.VSOGroup_CUSO + "'");

                        //count = rowCollection.Length;
                        //rowCount += count;
                        //countCUSO+=count;

                        //cell = new HtmlTableCell();
                        //cell.InnerText = count.ToString();
                        //cell.Attributes.Add("class", "volCountMatrixRowRest");

                        //row.Cells.Add(cell);

                        cell = new HtmlTableCell();
                        cell.InnerText = rowCount.ToString();
                        cell.Attributes.Add("class", "volCountMatrixRowRest");

                        row.Cells.Add(cell);

                        tblCountMatrix.Rows.Add(row);

                    }

                }
                //TPT Ammended:VAF-1057
                RenderFooter(countLTV,countSTV,countYouth,countGX);
                
            }
        }

        #endregion

        #region " RenderHeader "

        private void RenderHeader()
        {

            HtmlTableRow row;
            HtmlTableCell cell;

            row = new HtmlTableRow();

            cell = new HtmlTableCell();
            cell.InnerText = "Country / Goal";
            cell.Attributes.Add("class", "volCountMatrixHeadFirst");

            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.InnerText = "LTV";
            cell.Attributes.Add("class", "volCountMatrixHeadRest");

            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.InnerText = "STV";
            cell.Attributes.Add("class", "volCountMatrixHeadRest");

            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.InnerText = "Youth";
            cell.Attributes.Add("class", "volCountMatrixHeadRest");

            row.Cells.Add(cell);

            //cell = new HtmlTableCell();
            //cell.InnerText = "GX";
            //cell.Attributes.Add("class", "volCountMatrixHeadRest");

            //row.Cells.Add(cell);

            //TPT Ammended:VAF-1057
            //cell = new HtmlTableCell();
            //cell.InnerText = "CUSO";
            //cell.Attributes.Add("class", "volCountMatrixHeadRest");

            //row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.InnerText = "Total";
            cell.Attributes.Add("class", "volCountMatrixHeadRest");

            row.Cells.Add(cell);

            tblCountMatrix.Rows.Add(row);
        }

        #endregion

        #region " RenderFooter "

        private void RenderFooter(int countLTV, int countSTV, int countYouth, int countGX)
        {
            HtmlTableRow row = new HtmlTableRow();
            HtmlTableCell cell = new HtmlTableCell();

            cell = new HtmlTableCell();
            cell.InnerText = "Total";
            cell.Attributes.Add("class", "volCountMatrixHeadFirst");

            row.Cells.Add(cell);


            cell = new HtmlTableCell();
            cell.InnerText = countLTV.ToString();
            cell.Attributes.Add("class", "volCountMatrixRowRest");

            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.InnerText = countSTV.ToString();
            cell.Attributes.Add("class", "volCountMatrixRowRest");

            row.Cells.Add(cell);


            cell = new HtmlTableCell();
            cell.InnerText = countYouth.ToString();
            cell.Attributes.Add("class", "volCountMatrixRowRest");

            row.Cells.Add(cell);


            //cell = new HtmlTableCell();
            //cell.InnerText = countGX.ToString();
            //cell.Attributes.Add("class", "volCountMatrixRowRest");

            //row.Cells.Add(cell);

            //TPT Ammended:VAF-1057
            //cell = new HtmlTableCell();
            //cell.InnerText = countCUSO.ToString();
            //cell.Attributes.Add("class", "volCountMatrixRowRest");

            //row.Cells.Add(cell);

            int totalCount = countLTV + countSTV + countYouth + countGX;

            cell = new HtmlTableCell();
            cell.InnerText = totalCount.ToString();
            cell.Attributes.Add("class", "volCountMatrixHeadRest");

            row.Cells.Add(cell);

            tblCountMatrix.Rows.Add(row);
        }

    #endregion

        #region " ApplyStyleToAlertColumn "

        private void ApplyStyleToAlertColumn()
        {
            StringBuilder js = new StringBuilder();
            js.Append("function ApplyStyleToAlertColumn(){");
            if (!(showingTabs))
            js.Append("var tbl  = document.getElementById('"+ bnbvwVolunteers.ClientID +"');");
            else
            js.Append("var tbl  = document.getElementById('" + bnbvwVolunteers.ClientID + "_0');");
            js.Append("if (tbl != null){");
            js.Append("var rows = tbl.getElementsByTagName('tr');");
            js.Append("for (var row=0; row<rows.length;row++) {");
            js.Append("var cells = rows[row].getElementsByTagName('td');");
            js.Append("if (cells != null) if(cells.length>=" + (alertColumn + 1).ToString() + ")cells[" + alertColumn.ToString() + "].style.color='orange';}}}");
            if (!(Page.ClientScript.IsClientScriptBlockRegistered("AlertStyle")))
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "AlertStyle", js.ToString(), true);
        }

        #endregion

        #region "Over All Grid View"

        //OverAll Report for the Manager
        private void SnapShotOverAllReport()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_Manager))
            {
                BnbQuery query = new BnbQuery("vwBonoboOverAllReports");
                BnbQueryDataSet dataSet = query.Execute();
                DataTable dt = dataSet.Tables["Results"];
                OverAllGridView.DataSource = dt;
                OverAllGridView.DataBind();
            }
        }

        // Row Bound for the OverAll Grid View To Display the Total of each column in the Data Grid Footer
        protected void OverAllGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Literal ltlTotalNumberOfProjects = (Literal)e.Row.FindControl("ltlTotalNumberOfProjects");
                Literal ltlTotalNumberOfLiveFullyProjects = (Literal)e.Row.FindControl("ltlTotalNumberOfLiveFullyProjects");
                Literal ltlTotalNumberOfLivePartialProjects = (Literal)e.Row.FindControl("ltlTotalNumberOfLivePartialProjects");
                Literal ltlTotalProjectBudgets = (Literal)e.Row.FindControl("ltlTotalProjectBudgets");
                Literal ltlTotalFundsSecured = (Literal)e.Row.FindControl("ltlTotalFundsSecured");
                Literal ltlTotalMatchedFundingRequired = (Literal)e.Row.FindControl("ltlTotalMatchedFundingRequired");
                Literal ltlTotalIncomeExpected = (Literal)e.Row.FindControl("ltlTotalIncomeExpected");
                Literal ltlTotalPlacementFilled = (Literal)e.Row.FindControl("ltlTotalPlacementFilled");
                Literal ltlTotalPlacementRequired = (Literal)e.Row.FindControl("ltlTotalPlacementRequired");
                Literal ltlTotalNumberofPriorityProjects = (Literal)e.Row.FindControl("ltlTotalNumberofPriorityProjects");
               
                decimal totalNumberOfProjects = Decimal.Parse(ltlTotalNumberOfProjects.Text);
                decimal totalNumberOfLiveFullyProjects = Decimal.Parse(ltlTotalNumberOfLiveFullyProjects.Text);
                decimal totalNumberOfLivePartialProjects = Decimal.Parse(ltlTotalNumberOfLivePartialProjects.Text);
                decimal totalProjectBudgets = Decimal.Parse(ltlTotalProjectBudgets.Text);
                decimal totalFundsSecured = Decimal.Parse(ltlTotalFundsSecured.Text);
                decimal totalMatchedFundingRequired = Decimal.Parse(ltlTotalMatchedFundingRequired.Text);
                decimal totalPlacementFilled= Decimal.Parse(ltlTotalPlacementFilled.Text);
                decimal totalPlacementRequired = Decimal.Parse(ltlTotalPlacementRequired.Text);
                decimal totalNumberofPriorityProjects = Decimal.Parse(ltlTotalNumberofPriorityProjects.Text);

                numberOfProjectsTotal += totalNumberOfProjects;
                liveFullyTotal+= totalNumberOfLiveFullyProjects;
                livePartialTotal += totalNumberOfLivePartialProjects;
                projectBudgetTotal += totalProjectBudgets;
                fundsSecuredTotal += totalFundsSecured;
                matchedFundingTotal += totalMatchedFundingRequired;
                placementFillTotal += totalPlacementFilled;
                requiredTotal += totalPlacementRequired;
                priorityProjectTotal += totalNumberofPriorityProjects;
               
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Literal ltlTotalNumberOfProjects = (Literal)e.Row.FindControl("ltlTotalNumberOfProjects");
                Literal ltlTotalNumberOfLiveFullyProjects = (Literal)e.Row.FindControl("ltlTotalNumberOfLiveFullyProjects");
                Literal ltlTotalNumberOfLivePartialProjects = (Literal)e.Row.FindControl("ltlTotalNumberOfLivePartialProjects");
                Literal ltlTotalProjectBudgets = (Literal)e.Row.FindControl("ltlTotalProjectBudgets");
                Literal ltlTotalFundsSecured = (Literal)e.Row.FindControl("ltlTotalFundsSecured");
                Literal ltlTotalMatchedFundingRequired = (Literal)e.Row.FindControl("ltlTotalMatchedFundingRequired");
                Literal ltlTotalIncomeExpected = (Literal)e.Row.FindControl("ltlTotalIncomeExpected");
                Literal ltlTotalNumberofPriorityProjects = (Literal)e.Row.FindControl("ltlTotalNumberofPriorityProjects");
                Literal ltlTotalPlacementFillRequired = (Literal)e.Row.FindControl("ltlTotalPlacementFillRequired") as Literal;

                ltlTotalNumberOfProjects.Text = numberOfProjectsTotal.ToString();
                ltlTotalNumberOfLiveFullyProjects.Text = liveFullyTotal.ToString();
                ltlTotalNumberOfLivePartialProjects.Text = livePartialTotal.ToString();
                ltlTotalProjectBudgets.Text = projectBudgetTotal.ToString("n0");
                ltlTotalFundsSecured.Text = fundsSecuredTotal.ToString("n0");
                ltlTotalMatchedFundingRequired.Text = matchedFundingTotal.ToString("n0");
                ltlTotalPlacementFillRequired.Text = placementFillTotal.ToString() + " | " + requiredTotal.ToString();
                ltlTotalNumberofPriorityProjects.Text = priorityProjectTotal.ToString();
            }

        }

        #endregion

        #region "Fiscal Year Grid View"

        //FiscalYear Report for the Manager
        private void SnapShotFiscalYearReport()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_Manager))
            {
                BnbQuery query = new BnbQuery("vwBonoboFiscalYearReports");
                BnbQueryDataSet dataSet = query.Execute();
                DataTable dt = dataSet.Tables["Results"];
                FiscalYearGridView.DataSource = dt;
                FiscalYearGridView.DataBind();
            }
        }

        // Row Bound for the FiscalYear Grid View To Display the Total of each column in the Data Grid Footer
        protected void FiscalYearGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Literal ltlFiscalReportTotalNumberOfProjects = (Literal)e.Row.FindControl("ltlFiscalReportTotalNumberOfProjects");
                Literal ltlFiscalReportTotalNumberOfLiveFullyProjects = (Literal)e.Row.FindControl("ltlFiscalReportTotalNumberOfLiveFullyProjects");
                Literal ltlFiscalReportTotalNumberOfLivePartialProjects = (Literal)e.Row.FindControl("ltlFiscalReportTotalNumberOfLivePartialProjects");
                Literal ltlFiscalReportTotalProjectBudgets = (Literal)e.Row.FindControl("ltlFiscalReportTotalProjectBudgets");
                Literal ltlFiscalReportTotalFundsSecured = (Literal)e.Row.FindControl("ltlFiscalReportTotalFundsSecured");
                Literal ltlFiscalReportTotalMatchedFundingRequired = (Literal)e.Row.FindControl("ltlFiscalReportTotalMatchedFundingRequired");
                Literal ltlFiscalReportTotalIncomeExpected = (Literal)e.Row.FindControl("ltlFiscalReportTotalIncomeExpected");
                //TPT Added for PGMSSRONE-2
                Literal ltlFiscalReportTotalIncomeReceived = (Literal)e.Row.FindControl("ltlFiscalReportTotalIncomeReceived");
                Literal ltlFiscalReportTotalPlacementFilled = (Literal)e.Row.FindControl("ltlFiscalReportTotalPlacementFilled");
                Literal ltlFiscalReportTotalPlacementRequired = (Literal)e.Row.FindControl("ltlFiscalReportTotalPlacementRequired");
                Literal ltlFiscalReportTotalNumberofPriorityProjects = (Literal)e.Row.FindControl("ltlFiscalReportTotalNumberofPriorityProjects");

                decimal totalNumberOfProjectsFiscalReport = Decimal.Parse(ltlFiscalReportTotalNumberOfProjects.Text);
                decimal totalNumberOfLiveFullyProjectsFiscalReport = Decimal.Parse(ltlFiscalReportTotalNumberOfLiveFullyProjects.Text);
                decimal totalNumberOfLivePartialProjectsFiscalReport = Decimal.Parse(ltlFiscalReportTotalNumberOfLivePartialProjects.Text);
                decimal totalProjectBudgetsFiscalReport = Decimal.Parse(ltlFiscalReportTotalProjectBudgets.Text);
                decimal totalFundsSecuredFiscalReport = Decimal.Parse(ltlFiscalReportTotalFundsSecured.Text);
                decimal totalMatchedFundingRequiredFiscalReport = Decimal.Parse(ltlFiscalReportTotalMatchedFundingRequired.Text);
                decimal totalIncomeExpectedFiscalReport = Decimal.Parse(ltlFiscalReportTotalIncomeExpected.Text);
                //TPT Added for PGMSSRONE-2
                decimal totalIncomeReceivedFiscalReport = Decimal.Parse(ltlFiscalReportTotalIncomeReceived.Text);
                decimal totalPlacementFilledFiscalReport = Decimal.Parse(ltlFiscalReportTotalPlacementFilled.Text);
                decimal totalPlacementRequiredFiscalReport = Decimal.Parse(ltlFiscalReportTotalPlacementRequired.Text);
                decimal totalNumberofPriorityProjectsFiscalReport = Decimal.Parse(ltlFiscalReportTotalNumberofPriorityProjects.Text);

                numberOfProjectsTotalFiscalReport += totalNumberOfProjectsFiscalReport;
                liveFullyTotalFiscalReport += totalNumberOfLiveFullyProjectsFiscalReport;
                livePartialTotalFiscalReport += totalNumberOfLivePartialProjectsFiscalReport;
                projectBudgetTotalFiscalReport += totalProjectBudgetsFiscalReport;
                fundsSecuredTotalFiscalReport += totalFundsSecuredFiscalReport;
                matchedFundingTotalFiscalReport += totalMatchedFundingRequiredFiscalReport;
                incomeExpectedTotalFiscalReport += totalIncomeExpectedFiscalReport;
                //TPT Added for PGMSSRONE-2
                incomeReceivedTotalFiscalReport += totalIncomeReceivedFiscalReport;
                placementFillTotalFiscalReport += totalPlacementFilledFiscalReport;
                requiredTotalFiscalReport += totalPlacementRequiredFiscalReport;
                priorityProjectTotalFiscalReport += totalNumberofPriorityProjectsFiscalReport;            
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Literal ltlFiscalReportTotalNumberOfProjects = (Literal)e.Row.FindControl("ltlFiscalReportTotalNumberOfProjects");
                Literal ltlFiscalReportTotalNumberOfLiveFullyProjects = (Literal)e.Row.FindControl("ltlFiscalReportTotalNumberOfLiveFullyProjects");
                Literal ltlFiscalReportTotalNumberOfLivePartialProjects = (Literal)e.Row.FindControl("ltlFiscalReportTotalNumberOfLivePartialProjects");
                Literal ltlFiscalReportTotalProjectBudgets = (Literal)e.Row.FindControl("ltlFiscalReportTotalProjectBudgets");
                Literal ltlFiscalReportTotalFundsSecured = (Literal)e.Row.FindControl("ltlFiscalReportTotalFundsSecured");
                Literal ltlFiscalReportTotalMatchedFundingRequired = (Literal)e.Row.FindControl("ltlFiscalReportTotalMatchedFundingRequired");
                Literal ltlFiscalReportTotalIncomeExpected = (Literal)e.Row.FindControl("ltlFiscalReportTotalIncomeExpected");
                //TPT Added for PGMSSRONE-2
                Literal ltlFiscalReportTotalIncomeReceived = (Literal)e.Row.FindControl("ltlFiscalReportTotalIncomeReceived");
                Literal ltlFiscalReportTotalNumberofPriorityProjects = (Literal)e.Row.FindControl("ltlFiscalReportTotalNumberofPriorityProjects");
                Literal ltlFiscalReportTotalPlacementFillRequired = (Literal)e.Row.FindControl("ltlFiscalReportTotalPlacementFillRequired") as Literal;
                
                ltlFiscalReportTotalNumberOfProjects.Text = numberOfProjectsTotalFiscalReport.ToString();
                ltlFiscalReportTotalNumberOfLiveFullyProjects.Text = liveFullyTotalFiscalReport.ToString();
                ltlFiscalReportTotalNumberOfLivePartialProjects.Text = livePartialTotalFiscalReport.ToString();
                ltlFiscalReportTotalProjectBudgets.Text = projectBudgetTotalFiscalReport.ToString("n0");
                ltlFiscalReportTotalFundsSecured.Text = fundsSecuredTotalFiscalReport.ToString("n0");
                ltlFiscalReportTotalMatchedFundingRequired.Text = matchedFundingTotalFiscalReport.ToString("n0");
                ltlFiscalReportTotalIncomeExpected.Text = incomeExpectedTotalFiscalReport.ToString("n0");
                //TPT Added for PGMSSRONE-2
                ltlFiscalReportTotalIncomeReceived.Text = incomeReceivedTotalFiscalReport.ToString("n0");
                ltlFiscalReportTotalPlacementFillRequired.Text = placementFillTotalFiscalReport.ToString() + " | " + requiredTotalFiscalReport.ToString();
                ltlFiscalReportTotalNumberofPriorityProjects.Text = priorityProjectTotalFiscalReport.ToString();
            }

        }
        #endregion

        #region "My Projects Grid View"

        //My Project Reports for the PO-Staff
        private void SnapShotMyProjectReport()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_POStaff))
            {
                Guid uid = BnbEngine.SessionManager.GetSessionInfo().UserID;
                BnbTextCondition txtCondition1 = new BnbTextCondition("tblProject_ProjectAccountableOfficerID", uid.ToString(), BnbTextCompareOptions.ExactMatch, BnbLogicalOperatorType.Or);
                BnbTextCondition txtCondition2 = new BnbTextCondition("tblProject_ProjectResponsibleOfficerID", uid.ToString(), BnbTextCompareOptions.ExactMatch, BnbLogicalOperatorType.Or);
                BnbTextCondition txtCondition3 = new BnbTextCondition("tblProject_ProjectContactID", uid.ToString(), BnbTextCompareOptions.ExactMatch, BnbLogicalOperatorType.Or);
                BnbCriteria criteria = new BnbCriteria(txtCondition1);
                criteria.QueryElements.Add(txtCondition2);
                criteria.QueryElements.Add(txtCondition3);
                BnbColumnList columnList = new BnbColumnList("tblProject_ProjectPriority, tblProject_EndDate DESC");
                BnbQuery query = new BnbQuery("vwBonoboExistingProjectReports", criteria, columnList);
                BnbQueryDataSet dataSet = query.Execute();
                DataTable dt = dataSet.Tables["Results"];
                MyProjectReportsGridView.DataSource = dt;
                MyProjectReportsGridView.DataBind();
            }
        }

        // display the High Priority Projects in the My Project section when Fund Raiser comes
        private void SnapShotMyProjectReportForHighPriorityProject()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_FundRaiser))
            {               
                Guid uid = BnbEngine.SessionManager.GetSessionInfo().UserID;
                BnbTextCondition txtCondition1 = new BnbTextCondition("tblProject_ProjectAccountableOfficerID", uid.ToString(), BnbTextCompareOptions.ExactMatch, BnbLogicalOperatorType.Or);
                BnbTextCondition txtCondition2 = new BnbTextCondition("tblProject_ProjectResponsibleOfficerID", uid.ToString(), BnbTextCompareOptions.ExactMatch, BnbLogicalOperatorType.Or);
                BnbTextCondition txtCondition3 = new BnbTextCondition("tblProject_ProjectContactID", uid.ToString(), BnbTextCompareOptions.ExactMatch, BnbLogicalOperatorType.Or);
                BnbTextCondition txtCondition4 = new BnbTextCondition("tblProject_ProjectPriority", "High", BnbTextCompareOptions.ExactMatch, BnbLogicalOperatorType.And);
                BnbCriteria criteria = new BnbCriteria(txtCondition1);
                criteria.QueryElements.Add(txtCondition2);
                criteria.QueryElements.Add(txtCondition3);
                criteria.QueryElements.Add(txtCondition4);
                BnbColumnList columnList = new BnbColumnList("tblProject_ProjectPriority, tblProject_EndDate DESC");
                BnbQuery query = new BnbQuery("vwBonoboExistingProjectReports", criteria, columnList);
                BnbQueryDataSet dataSet = query.Execute();
                DataTable dt = dataSet.Tables["Results"];
                MyProjectReportsGridView.DataSource = dt;
                MyProjectReportsGridView.DataBind();
           }

        }

        // Row Bound for the My Projects Grid View To Display the High Priority Project in Yellow Color
        protected void MyProjectReportsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_POStaff)) || BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_FundRaiser))
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Center;
                    e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Center;
                    e.Row.Cells[15].HorizontalAlign = HorizontalAlign.Center;
                    // determine the value of the priority field as high
                    string prior = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "tblProject_ProjectPriority"));
                    if (prior == "HIGH")
                        //set the background row color as yellow
                        e.Row.BackColor = Color.Yellow;
                }
            }
        }

        #endregion

        #region "My Project Grid View Sort"
        //Sort My Projects Reports According to the Drop Down list Selected Items
        protected void btnSortMyProject_Click(object sender, EventArgs e)
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_POStaff))
            {
                String expression = "";
                expression = SortList1.SelectedValue;
                if (expression != null)
                {
                    Guid uid = BnbEngine.SessionManager.GetSessionInfo().UserID;
                    BnbTextCondition txtCondition1 = new BnbTextCondition("tblProject_ProjectAccountableOfficerID", uid.ToString(), BnbTextCompareOptions.ExactMatch, BnbLogicalOperatorType.Or);
                    BnbTextCondition txtCondition2 = new BnbTextCondition("tblProject_ProjectResponsibleOfficerID", uid.ToString(), BnbTextCompareOptions.ExactMatch, BnbLogicalOperatorType.Or);
                    BnbTextCondition txtCondition3 = new BnbTextCondition("tblProject_ProjectContactID", uid.ToString(), BnbTextCompareOptions.ExactMatch, BnbLogicalOperatorType.Or);
                    BnbCriteria criteria = new BnbCriteria(txtCondition1);
                    criteria.QueryElements.Add(txtCondition2);
                    criteria.QueryElements.Add(txtCondition3);
                    BnbColumnList columnList = new BnbColumnList(expression.ToString());
                    BnbQuery query = new BnbQuery("vwBonoboExistingProjectReports", criteria, columnList);
                    BnbQueryDataSet dataSet = query.Execute();
                    DataTable dt = dataSet.Tables["Results"];
                    MyProjectReportsGridView.DataSource = dt;
                    MyProjectReportsGridView.DataBind();
                }

            }

            else if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_FundRaiser))
            {
                String expression = "";
                expression = SortList1.SelectedValue;
                if (expression != null)
                {
                    Guid uid = BnbEngine.SessionManager.GetSessionInfo().UserID;
                    BnbTextCondition txtCondition1 = new BnbTextCondition("tblProject_ProjectAccountableOfficerID", uid.ToString(), BnbTextCompareOptions.ExactMatch, BnbLogicalOperatorType.Or);
                    BnbTextCondition txtCondition2 = new BnbTextCondition("tblProject_ProjectResponsibleOfficerID", uid.ToString(), BnbTextCompareOptions.ExactMatch, BnbLogicalOperatorType.Or);
                    BnbTextCondition txtCondition3 = new BnbTextCondition("tblProject_ProjectContactID", uid.ToString(), BnbTextCompareOptions.ExactMatch, BnbLogicalOperatorType.Or);
                    BnbTextCondition txtCondition4 = new BnbTextCondition("tblProject_ProjectPriority", "High", BnbTextCompareOptions.ExactMatch, BnbLogicalOperatorType.And);
                    BnbCriteria criteria = new BnbCriteria(txtCondition1);
                    criteria.QueryElements.Add(txtCondition2);
                    criteria.QueryElements.Add(txtCondition3);
                    criteria.QueryElements.Add(txtCondition4);
                    BnbColumnList columnList = new BnbColumnList(expression.ToString());
                    BnbQuery query = new BnbQuery("vwBonoboExistingProjectReports", criteria, columnList);
                    BnbQueryDataSet dataSet = query.Execute();
                    DataTable dt = dataSet.Tables["Results"];
                    MyProjectReportsGridView.DataSource = dt;
                    MyProjectReportsGridView.DataBind();
                }
            }
        }
        #endregion

        #region "User Access Permission For SnapShot Reports"
        //Establish the Access permission for Snapshot reports for the Different User        
        private void SnapShotUserAccessPermission()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_FundRaiser) || BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_POStaff) || BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_Manager))
            {
                if ((BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_Manager)) && (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_POStaff)) && ((BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_FundRaiser))))
                {
                    SnapShotTab.Enabled = true;
                    pnlOverAll.Visible = true;
                    pnlFiscalyear.Visible = true;
                    pnlMyProjects.Visible = true;
                }
                else if ((BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_Manager)) && (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_POStaff)))
                {
                    SnapShotTab.Enabled = true;
                    pnlOverAll.Visible = true;
                    pnlFiscalyear.Visible = true;
                    pnlMyProjects.Visible = true;
                }
                else if ((BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_FundRaiser)) && (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_Manager)))
                {
                    SnapShotTab.Enabled = true;
                    pnlOverAll.Visible = true;
                    pnlFiscalyear.Visible = true;
                    pnlMyProjects.Visible = true;
                }
                else if ((BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_FundRaiser)) && (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_POStaff)))
                {
                    SnapShotTab.Enabled = true;
                    pnlOverAll.Visible = false;
                    pnlFiscalyear.Visible = false;
                    pnlMyProjects.Visible = true;
                }
                else if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_FundRaiser))
                {
                    SnapShotTab.Enabled = true;
                    pnlOverAll.Visible = false;
                    pnlFiscalyear.Visible = false;
                    pnlMyProjects.Visible = true;
                }
                else if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_POStaff))
                {
                    SnapShotTab.Enabled = true;
                    pnlOverAll.Visible = false;
                    pnlFiscalyear.Visible = false;
                    pnlMyProjects.Visible = true;
                }
                else if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_Manager))
                {
                    SnapShotTab.Enabled = true;
                    pnlOverAll.Visible = true;
                    pnlFiscalyear.Visible = true;
                    pnlMyProjects.Visible = false;
                }
            }
            else
            {
                SnapShotTab.Visible = false;
                pnlOverAll.Visible = false;
                pnlFiscalyear.Visible = false;
                pnlMyProjects.Visible = false;
            }

        }
        #endregion

        #region "Export SnapShot"
        //The Snap Shot Link to Export the Visible Grid in Excel Report for the Different User 
        protected void hlExportSnapShot_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=YourSnapShotReports.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.xls";
            // TPT amended: Included System.Text.Encoding.GetEncoding(1252)to remove (Â) symbol from the Snapshot Excel Reports column heading
            Response.ContentEncoding = System.Text.Encoding.GetEncoding(1252);
            Response.Write("<style> .text {mso-number-format:/; } </style>");
            StringWriter sw = new StringWriter();
            HtmlTextWriter wr = new HtmlTextWriter(sw);
            if (OverAllGridView.Visible)
                OverAllGridView.RenderControl(wr);
            if (FiscalYearGridView.Visible)
                FiscalYearGridView.RenderControl(wr);
            //TPT Amended:PGMSSRONE-3
            if (gvVolCount.Visible)
                gvVolCount.RenderControl(wr);
            if (MyProjectReportsGridView.Visible)
            {
                MyProjectReportsGridView.Columns[0].Visible = false;
                MyProjectReportsGridView.RenderControl(wr);
            }
            Response.Write(sw.ToString());
            Response.End();
        }
        public override void VerifyRenderingInServerForm(Control control)
        {

            /* Verifies that a Form control was rendered */

        }
        #endregion
        
        #region "Restricted/UnRestricted Volunteer Funding Count in Snap Shot"

        //TPT Amended: PGMSSRONE-3
        private void SnapShotVolunteerFundingCount()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_Manager))
            {
                BnbQuery query = new BnbQuery("vwBonoboVolunteerFundingCount");
                BnbQueryDataSet dataSet = query.Execute();
                DataTable dt = dataSet.Tables["Results"];
                gvVolCount.DataSource = dt;
                gvVolCount.DataBind();
            }
        }

        protected void gvVolCount_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Literal ltlTotalNumberOfRestrictedVol = (Literal)e.Row.FindControl("ltlTotalNumberOfRestrictedVol");
                Literal ltlTotalNumberOfUnrestrictedVol = (Literal)e.Row.FindControl("ltlTotalNumberOfUnrestrictedVol");

                decimal totalNumberOfRestrictedVol = Decimal.Parse(ltlTotalNumberOfRestrictedVol.Text);
                decimal totalNumberOfUnrestrictedVol = Decimal.Parse(ltlTotalNumberOfUnrestrictedVol.Text);

                numberOfRestrictedVol += totalNumberOfRestrictedVol;
                numberOfUnrestrictedVol += totalNumberOfUnrestrictedVol;
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Literal ltlTotalNumberOfRestrictedVol = (Literal)e.Row.FindControl("ltlTotalNumberOfRestrictedVol");
                Literal ltlTotalNumberOfUnrestrictedVol = (Literal)e.Row.FindControl("ltlTotalNumberOfUnrestrictedVol");

                ltlTotalNumberOfRestrictedVol.Text = numberOfRestrictedVol.ToString("n0");
                ltlTotalNumberOfUnrestrictedVol.Text = numberOfUnrestrictedVol.ToString("n0");
            }
        }

        #endregion
    }

}

