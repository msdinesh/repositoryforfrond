<%@ Register TagPrefix="uc1" TagName="Title" Src="UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="MyAccount.aspx.cs" AutoEventWireup="True" Inherits="Frond.MyAccount" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>SFHistory</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK rel="stylesheet" type="text/css" href="Css/FrondDefault.css">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="Images/myaccount24silver.gif"></uc1:title>
				<p><b style="COLOR:#9966ff">This page shows Account information for the currently 
						logged-in user</b></p>
				<bnbpagecontrol:BnbTabControl id="BnbTabControl1" runat="server" IgnoreWorkareas=true GetShowAllTabsVerticallyFromSession="false"></bnbpagecontrol:BnbTabControl>
				<asp:Panel id="UserTab" runat="server" CssClass="tcTab" BnbTabName="User Info">
					<DIV class="sectionheading" style="width:50%">User</DIV>
					<TABLE class="fieldtable" width="80%">
						<TR>
							<TD class="label" width="25%">Full Name</TD>
							<TD>
								<asp:label id="uxFullName" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Login Name</TD>
							<TD>
								<asp:label id="uxLoginName" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Default Country</TD>
							<TD>
								<bnbdatacontrol:bnbstandalonelookupcontrol id="uxCountry" runat="server" CssClass="lookupcontrol" LookupTableName="lkuCountry"
									LookupControlType="ReadOnlyLabel" ListBoxRows="4" DataTextField="Description" Width="250px" Height="22px"></bnbdatacontrol:bnbstandalonelookupcontrol></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Default Programme Office</TD>
							<TD>
								<bnbdatacontrol:bnbstandalonelookupcontrol id="uxProgrammeOffice" runat="server" CssClass="lookupcontrol" LookupTableName="lkuProgrammeOffice"
									LookupControlType="ReadOnlyLabel" ListBoxRows="4" DataTextField="Description" Width="250px" Height="22px"></bnbdatacontrol:bnbstandalonelookupcontrol></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Default Recruitment Base</TD>
							<TD>
								<bnbdatacontrol:bnbstandalonelookupcontrol id="uxRecruitmentBase" runat="server" CssClass="lookupcontrol" LookupTableName="lkuRecruitmentBase"
									LookupControlType="ReadOnlyLabel" ListBoxRows="4" DataTextField="Description" Width="250px" Height="22px"></bnbdatacontrol:bnbstandalonelookupcontrol></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Department</TD>
							<TD>
								<bnbdatacontrol:bnbstandalonelookupcontrol id="uxDepartment" runat="server" CssClass="lookupcontrol" LookupTableName="appDepartment"
									LookupControlType="ReadOnlyLabel" ListBoxRows="4" DataTextField="Description" Width="250px" Height="22px"></bnbdatacontrol:bnbstandalonelookupcontrol></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Email</TD>
							<TD>
								<asp:Label id="uxUserEmail" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">App Style</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="uxWebStyle" runat="server" CssClass="lookupcontrol" LookupTableName="vlkuBonoboWebStyle"
									LookupControlType="ReadOnlyLabel" ListBoxRows="4" DataTextField="Description" Width="250px" Height="22px"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
					</TABLE>
					<DIV class="sectionheading"  style="width:50%">Roles</DIV>
					<TABLE class="fieldtable" width="80%">
						<TR>
							<TD class="label" width="25%">Roles</TD>
							<TD width="75%">
								<asp:label id="uxWorkareas" runat="server"></asp:label></TD>
						</TR>
					</TABLE>
				</asp:Panel>
				<asp:Panel id="PermissionTab" runat="server" CssClass="tcTab" BnbTabName="Permissions">
					<asp:DataGrid id="uxPermissionGrid" runat="server" CssClass="DataGrid" AutoGenerateColumns="False">
						<HeaderStyle CssClass="datagridheader"></HeaderStyle>
						<Columns>
							<asp:BoundColumn DataField="PermissionType" HeaderText="Permission Type"></asp:BoundColumn>
							<asp:BoundColumn DataField="Permission" HeaderText="Permission"></asp:BoundColumn>
						</Columns>
					</asp:DataGrid>
				</asp:Panel>
				<asp:Panel id="OptionsTab" runat="server" CssClass="tcTab" BnbTabName="Options">
	                <TABLE class="fieldtable" width="80%">
						<TR>
							<TD class="label">Display Data Tabs Vertically Down Page</TD>
							<TD>
                                <asp:CheckBox ID="uxShowAllTabsVertCheckBox" runat="server" /></TD>
						</TR>
						<tr><td>&nbsp;</td><td>
                            <asp:Button ID="uxSaveOptions" runat="server" Text="Save Options" OnClick="uxSaveOptions_Click" /></td></tr>
						</TABLE>
								
				</asp:Panel>
			</div>
		</form>
	</body>
</HTML>
