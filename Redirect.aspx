<%@ Page language="c#" Codebehind="Redirect.aspx.cs" AutoEventWireup="True" Inherits="Frond.Redirect" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Redirect</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="Css/FrondDefault.css">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<P>Redirect.aspx</P>
			<P>This page should automatically re-direct to the correct EditPage</P>
			<P>If that has not happened, try clicking here:</P>
			<P>
				<asp:HyperLink id="hyperRedirect" runat="server">Redirect</asp:HyperLink></P>
			<P>&nbsp;</P>
		</form>
	</body>
</HTML>
