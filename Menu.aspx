<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbpagecontrols" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>

<%@ Page Language="c#" Codebehind="Menu.aspx.cs" AutoEventWireup="True" Inherits="Frond.Menu" %>

<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html>
<head>
    <title>SFMenu</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
    
    <script type="text/javascript" language="javascript">
    function showHidePanel(hyperlinkID,pnlID)
		{
		var panelID= document.getElementById(pnlID);
		var hpText=document.getElementById(hyperlinkID);		
		if(panelID.style.display=='block')
		{
		panelID.style.display='none';
		hpText.innerHTML="+ Show Details";
		}
		else
		{
		panelID.style.display='block';
		hpText.innerHTML="- Hide Details";
		}
		}
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="Images/mainmenu24silver.gif"></uc1:Title>
            <p>
            </p>
            <bnbpagecontrols:BnbTabControl ID="BnbTabControl1" runat="server" GetShowAllTabsVerticallyFromSession="false">
            </bnbpagecontrols:BnbTabControl>
            <asp:Panel ID="MenuTab" CssClass="tcTab" BnbTabName="Menu" runat="server" Width="450px">
                <div class="sectionheading" style="text-align: center">
                    Quick Find</div>
                <div class="mainMenuBox">
                    <p>
                        Enter a Volunteer, Placement, Job, Employer or Programme reference number<br>
                        e.g. '707732', 'CH200/1/1',&nbsp;'GH548/0001', 'U217', 'U000116'<br>
                        <asp:TextBox ID="textBoxQuickStart" runat="server"></asp:TextBox>
                        <asp:Button ID="buttonQuickStart" runat="server" Text="Go" OnClick="buttonQuickStart_Click">
                        </asp:Button>
                        <asp:Label ID="labelQuickStartFeedback" runat="server" ForeColor="Red"></asp:Label></p>
                </div>
                <div class="sectionheading" style="text-align: center">
                    Add New</div>
                <div class="mainMenuBox">
                    <p>
                        Select from dropdown list and press Go to add new records<br>
                        <nobr>
                            Add new
                            <select id="cboNewType" name="cboNewType">
                              <option value="EditPages/EmployerPage.aspx?BnbEmployer=new" selected>Employer</option>
                                    <option value="EditPages/WizardNewOvsOrganisation.aspx?BnbOrganisation=new">Overseas Organisation</option>
                                    <option value="EditPages/ProgrammePage.aspx?BnbProgramme=new&amp;PageState=new&amp;DisableRedirect=true">Programme</option>
                                <option value="EditPages/WizardNewOrganisation.aspx?BnbOrganisation=new">Organisation</option>
                                <option value="EditPages/WizardNewIndividual.aspx?BnbIndividual=new">Individual</option>
                                <option value="EditPages/WizardNewEvent.aspx?BnbEvent=new">Event</option>
                                <option value="EditPages/WizardNewEnquiry.aspx?BnbApplication=new">Enquirer or Applicant</option>
                                <option value="EditPages/WizardNewProject.aspx?BnbProject=new&amp;PageState=new&amp;DisableRedirect=true">
                                    Project</option>
                                <option value="EditPages/WizardNewOrganisation.aspx">Organisation (Donor)</option>
                            </select>
                            <a href="javascript:location.href=document.forms[0].elements['cboNewType'].value;"
                                target="_top">Go</a>
                        </nobr>
                    </p>
                </div>
                <div class="sectionheading" style="text-align: center">
                    Find Data</div>
                <div class="mainMenuBox">
                    <p>
                        <a href="FindGeneral.aspx?FindTypeID=6">Find Individual</a></p>
                    <p>
                        <a href="FindGeneral.aspx?FindTypeID=3">Find Application</a></p>
                    <p>
                        <a href="FindGeneral.aspx?FindTypeID=2">Find Placement</a></p>
                    <p>
                        <a href="FindGeneral.aspx?FindTypeID=5">Find Organisation</a></p>
                    <p>
                        <a href="FindGeneral.aspx?FindTypeID=11">Find Event</a></p>
                     <p>
                        <a href="EditPages/FindGeographical.aspx">Find Volunteers Using a Map</a></p>

                </div>
            </asp:Panel>
            <asp:Panel ID="PGMSMenuTab" CssClass="tcTab" BnbTabName="PGMS Menu" runat="server"
                Width="450px">
                <div class="sectionheading" style="text-align: center">
                    Add New</div>
                <div class="mainMenuBox">
                    <p>
                        Add new: <a href="EditPages/WizardNewProject.aspx?BnbProject=new&amp;PageState=new&amp;DisableRedirect=true">
                            Programme/Project</a></p>
                    <p>
                        Add new: <a href="EditPages/WizardNewOrganisation.aspx">Organisation (Donor)</a></p>
                </div>
                <div class="sectionheading" style="text-align: center">
                    Find Data</div>
                <div class="mainMenuBox">
                    <p>
                        <a href="FindGeneral.aspx?FindTypeID=17">Find Project</a></p>
                    <p>
                        <a href="FindGeneral.aspx?FindTypeID=13">Find Proposal</a></p>
                    <p>
                        <a href="FindGeneral.aspx?FindTypeID=14">Find Contract</a></p>
                    <p>
                        <a href="FindGeneral.aspx?FindTypeID=5">Find Organisation</a></p>
                    <p>
                        <a href="FindGeneral.aspx?FindTypeID=16">Reports Due</a></p>
                </div>
                <div class="sectionheading" style="text-align: center">
                    PGMS Excel Reports</div>
                <div class="mainMenuBox">
                    <p>
                        Reports in Excel:</p>
                            <p>
                                <asp:HyperLink ID="hlAllProjects" runat="server">All Project Reports</asp:HyperLink></p>
                            <p>
                                 <asp:HyperLink ID="hlAllLiveProjects" runat="server">All Live Project Reports</asp:HyperLink>
                            </p>
                            <p>
                                <asp:HyperLink ID="hlAllIncomeClaimReports" runat="server">All Income Claim Reports</asp:HyperLink>
                            </p>
                            <p>
                                <asp:HyperLink ID="hlAllReportingDeadLine" runat="server">All Reporting DeadLine Reports</asp:HyperLink>
                            </p>
                            <p>
                                <asp:HyperLink ID="hlAllGrants" runat="server">All Grants Reports</asp:HyperLink>
                            </p>
                            <p>
                                <asp:HyperLink ID="hlAllProjectsHighRisk" runat="server">All Projects High Risk Reports</asp:HyperLink>
                            </p>
                            <p>
                                <asp:HyperLink ID="hlAllPriorityProject" runat="server">All Priority Project Reports</asp:HyperLink>
                            </p>
                </div>
            </asp:Panel>
            <asp:Panel ID="UtilitiesTab" CssClass="tcTab" BnbTabName="Utilities" runat="server"
                Width="450px">
                <div class="sectionheading" style="text-align: center">
                    Admin</div>
                <div class="mainMenuBox">
                    <p>
                        <a href="MyAccount.aspx">My Account</a></p>
                    <p>
                        <a href="About.aspx">About
                            <asp:Label ID="uxVirtualAppName" runat="server"></asp:Label></a></p>
                </div>
                <div class="sectionheading" style="text-align: center">
                    Utilities</div>
                <div class="mainMenuBox">
                    <p>
                        <a href="EditPages/FundCodeList.aspx">View Fund Codes</a></p>
                    <p>
                        <a href="EditPages/FundChargeRateList.aspx">View Fund Charge Rates</a></p>
                    <p>
                        <a href="EditPages/ActionsBatch.aspx">Actions Batch</a></p>
                    <p>
                        <a href="EditPages/MailingExtractUtil.aspx">Mailing Extract</a></p>
                    <p>
                        <a href="EditPages/RVConversionUtil.aspx">RV Conversion</a></p>
                    <p>
                        <a href="EditPages/SourceGroupPage.aspx">Source Editor</a></p>
                    <p>
                        <a href="EditPages/RegistryLabelsUtil.aspx">Registry Labels</a></p>
                    <p>
                        <a href="FindGeneral.aspx?FindTypeID=4">NI Batch (Find)</a></p>
                    <p>
                        <a href="EditPages/EGateway/EGatewayUtil.aspx">E-Gateway (Manage Online Applications)</a></p>
                    <p>
                        <a href="EditPages/XmlImportPage.aspx">Xml Import</a></p>
                    <p>
                        <a href="EditPages/Merge/frmMergeWizard.aspx">Merge</a></p>
                    <p>
                        <a href="EditPages/Merge/frmBulkMerge.aspx">Bulk Merge</a></p>
                    &nbsp; &nbsp;
                </div>
                <asp:Panel ID="panelOtherApps" runat="server" Visible="False" Enabled="false">
                   <div class="sectionheading" style="text-align: center">
                        Other Applications</div>
                    <div class="mainMenuBox">
                        <p>
                            <asp:HyperLink ID="hypStarfishTransfer" runat="server" Visible="False" NavigateUrl="Redirect.aspx?TransferTo=Starfish">Transfer to Starfish</asp:HyperLink></p>
                        <p>
                            <asp:HyperLink ID="hypPGMSTransfer" runat="server" Visible="False" NavigateUrl="Redirect.aspx?TransferTo=PGMS">Transfer to PGMS</asp:HyperLink></p>
                    </div> 
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="OurVolunteersTab" CssClass="tcTab" BnbTabName="Our Volunteers" runat="server"
                Width="850px">
                <p>
                <asp:Panel ID="pnlCOuntMatrix" runat="server">
                    <table border="1" cellpadding ="0" cellspacing="0" class="volCountMatrixTable" runat="server" id="tblCountMatrix" ></table>
               </asp:Panel>
                
                    <asp:Panel ID="pnlVolunteersGrid" runat="server">
                        <bnbdatagrid:BnbDataGridForView ID="bnbvwVolunteers" runat="server" displaynoresultmessage="False"
                            DisplayResultCount="False" ViewLinksText="View" NewLinkText="New" NewLinkVisible="False"
                            ViewLinksVisible="False" hideonnewbuttonclick="true" Width="100%" CssClass="datagrid" 
                            DomainObject="BnbApplication" ColumnsToExclude="tblApplication_VSOGroupID, lkuCountry_Description,lnkApplicationStatus_StatusGroupID,tblApplication_SOSConfirmed,tblApplication_EOSDate,tblIndividualKeyInfo_IndividualID,lkuVSOType_VSOGroupID" 
                            GuidKey="tblApplication_ApplicationID" showonnewbuttonclick="False"
                            viewname="vwBonoboVolunteers2" redirectpage="EditPages/ApplicationPage.aspx" displaynoresultmessageonly="False"></bnbdatagrid:bnbdatagridforview>
                    </asp:Panel>
                </p>
            </asp:Panel>
            <p>
                <asp:Label ID="lblDebug" runat="server"></asp:Label></p>
                <asp:Panel ID="SnapShotTab" CssClass="tcTab" BnbTabName="Snap Shot" runat="server">
                <div style="text-align: right">
                    <p>
                        <asp:LinkButton ID="HyperLink1" runat="server" OnClick="hlExportSnapShot_Click"><b>EXPORT SNAPSHOT</b></asp:LinkButton>
                    </p>
                </div>
                <asp:Panel ID="pnlOverAll" runat="server">
                    <table cellspacing="0px">
                        <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlOverAll','OverAllViewID'); return false;">
                            <td style="width: 500px; padding: 5px;">
                                Over All</td>
                            <td align="right" style="width: 450px; padding: 5px;">
                                <a href="#" id="hlOverAll">- Hide Details</a></td>
                        </tr>
                    </table>
                    &nbsp;
                    <div id="OverAllViewID" style="display: block;">
                        <asp:GridView ID="OverAllGridView" CssClass="datagrid" runat="server" AutoGenerateColumns="false"
                            ShowFooter="true" OnRowDataBound="OverAllGridView_RowDataBound">
                            <Columns>
                                <asp:BoundField DataField="Region" HeaderText="Region" FooterText="Total" FooterStyle-Font-Bold="true" />
                                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Number Of Projects">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlTotalNumberOfProjects" runat="server" Text='<%# Bind("NumberOfProjects")%>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Literal ID="ltlTotalNumberOfProjects" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Number of Live Fully Funded Projects">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlTotalNumberOfLiveFullyProjects" runat="server" Text='<%# Bind("LiveFully")%>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Literal ID="ltlTotalNumberOfLiveFullyProjects" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Number of Live Partial Funded Projects">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlTotalNumberOfLivePartialProjects" runat="server" Text='<%# Bind("LivePartial")%>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Literal ID="ltlTotalNumberOfLivePartialProjects" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Total of Project Budgets (�)">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlTotalProjectBudgets" runat="server" Text='<%# Bind("TotalProjectBudgets", "{0:N0}")%>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Literal ID="ltlTotalProjectBudgets" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Total of Funds Secured (�)">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlTotalFundsSecured" runat="server" Text='<%# Bind("TotalFundsSecured", "{0:N0}") %>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Literal ID="ltlTotalFundsSecured" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Total of Matched Funding Requirement (�)">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlTotalMatchedFundingRequired" runat="server" Text='<%# Bind("TotalMatchedFundingRequired", "{0:N0}")%>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Literal ID="ltlTotalMatchedFundingRequired" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Number of PlacementFill | Number of Required">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlTotalPlacementFillRequired" runat="server" Text='<%# Bind("PlacementFillRequired")%>' />
                                        <asp:Literal ID="ltlTotalPlacementFilled" runat="server" Text='<%# Bind("PlacementFilled")%>'
                                            Visible="false" />
                                        <asp:Literal ID="ltlTotalPlacementRequired" runat="server" Text='<%# Bind("PlacementRequired")%>'
                                            Visible="false" />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <center>
                                            <asp:Literal ID="ltlTotalPlacementFilled" runat="server" Visible="false" />
                                            <asp:Literal ID="ltlTotalPlacementRequired" runat="server" Visible="false" />
                                            <asp:Literal ID="ltlTotalPlacementFillRequired" runat="server"></asp:Literal></center>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Number of Priority Projects">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlTotalNumberofPriorityProjects" runat="server" Text='<%# Bind("NumberofPriorityProjects")%>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <center>
                                            <asp:Literal ID="ltlTotalNumberofPriorityProjects" runat="server" /></center>
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#f2f2ff" Font-Bold="True" HorizontalAlign="Right" />
                        </asp:GridView>
                    </div>
                    <br />
                </asp:Panel>
                <asp:Panel ID="pnlFiscalyear" runat="server">
                    <table cellspacing="0px">
                        <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlFiscalYear','FiscalYearViewID'); return false;">
                            <td style="width: 500px; padding: 5px;">
                                Fiscal Year</td>
                            <td align="right" style="width: 450px; padding: 5px;">
                                <a href="#" id="hlFiscalYear">- Hide Details</a></td>
                        </tr>
                    </table>
                    &nbsp;
                    <div id="FiscalYearViewID" style="display: block;">
                        <asp:GridView ID="FiscalYearGridView" runat="server" CssClass="datagrid" AutoGenerateColumns="false"
                            ShowFooter="true" OnRowDataBound="FiscalYearGridView_RowDataBound">
                            <Columns>
                                <asp:BoundField DataField="Region" HeaderText="Region" FooterText="Total" FooterStyle-Font-Bold="true" />
                                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Number Of Projects">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalNumberOfProjects" runat="server" Text='<%# Bind("NumberOfProjects")%>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalNumberOfProjects" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Number of Live Fully Funded Projects">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalNumberOfLiveFullyProjects" runat="server" Text='<%# Bind("LiveFully")%>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalNumberOfLiveFullyProjects" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Number of Live Partial Funded Projects">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalNumberOfLivePartialProjects" runat="server"
                                            Text='<%# Bind("LivePartial")%>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalNumberOfLivePartialProjects" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Total of Project Budgets (�)">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalProjectBudgets" runat="server" Text='<%# Bind("TotalProjectBudgets", "{0:N0}")%>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalProjectBudgets" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Total of Funds Secured (�)">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalFundsSecured" runat="server" Text='<%# Bind("TotalFundsSecured", "{0:N0}") %>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalFundsSecured" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Total of Matched Funding Requirement (�)">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalMatchedFundingRequired" runat="server" Text='<%# Bind("TotalMatchedFundingRequired", "{0:N0}")%>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalMatchedFundingRequired" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Income Expected This Year (�)">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalIncomeExpected" runat="server" Text='<%# Bind("IncomeExpected", "{0:N0}")%>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalIncomeExpected" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Income Received (�)">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalIncomeReceived" runat="server" Text='<%# Bind("IncomeReceived", "{0:N0}")%>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalIncomeReceived" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Number of PlacementFill | Number of Required">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalPlacementFillRequired" runat="server" Text='<%# Bind("PlacementFillRequired")%>' />
                                        <asp:Literal ID="ltlFiscalReportTotalPlacementFilled" runat="server" Text='<%# Bind("PlacementFilled")%>'
                                            Visible="false" />
                                        <asp:Literal ID="ltlFiscalReportTotalPlacementRequired" runat="server" Text='<%# Bind("PlacementRequired")%>'
                                            Visible="false" />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <center>
                                            <asp:Literal ID="ltlFiscalReportTotalPlacementFilled" runat="server" Visible="false" />
                                            <asp:Literal ID="ltlFiscalReportTotalPlacementRequired" runat="server" Visible="false" />
                                            <asp:Literal ID="ltlFiscalReportTotalPlacementFillRequired" runat="server"></asp:Literal></center>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Number of Priority Projects">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlFiscalReportTotalNumberofPriorityProjects" runat="server" Text='<%# Bind("NumberofPriorityProjects")%>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <center>
                                            <asp:Literal ID="ltlFiscalReportTotalNumberofPriorityProjects" runat="server" /></center>
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#f2f2ff" Font-Bold="True" HorizontalAlign="Right" />
                        </asp:GridView>
                    </div>
                    <br />
                </asp:Panel>
                <!--TPT Amended: PGMSSRONE-3 -->
                <asp:Panel ID="pnlVolunteerCount" runat="server">
                <table cellspacing="0px">
                        <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlVolunteerCount','VolunteerCountViewID'); return false;">
                            <td style="width: 500px; padding: 5px;">
                                Restricted/Unrestricted Volunteers</td>
                            <td align="right" style="width: 450px; padding: 5px;">
                                <a href="#" id="hlVolunteerCount">- Hide Details</a></td>
                        </tr>
                    </table>
                    &nbsp;
                    <div id="VolunteerCountViewID" style="display: block;">
                        <asp:GridView ID="gvVolCount" runat="server" AutoGenerateColumns="false"
                            ShowFooter="true" OnRowDataBound="gvVolCount_RowDataBound" CssClass="datagrid">
                            <Columns>
                            <asp:BoundField DataField="Region" HeaderText="Region" HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#f2f2ff" FooterText="Total"  FooterStyle-HorizontalAlign="Left" FooterStyle-Font-Bold="true" />
                             <asp:TemplateField  ItemStyle-HorizontalAlign="Left" HeaderText="Number Of Restricted Volunteers">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlTotalNumberOfRestrictedVol" runat="server" Text='<%# Bind("Restricted","{0:N0}")%>' />
                                     </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Literal ID="ltlTotalNumberOfRestrictedVol" runat="server" />
                                    </FooterTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Number Of Unrestricted Volunteers">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlTotalNumberOfUnrestrictedVol" runat="server" Text='<%# Bind("Unrestricted","{0:N0}")%>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Literal ID="ltlTotalNumberOfUnrestrictedVol" runat="server" />
                                    </FooterTemplate>
                             </asp:TemplateField>
                            </Columns>
                             <FooterStyle BackColor="#f2f2ff" Font-Bold="True" HorizontalAlign="Left" />
                        </asp:GridView>
                    </div>
                    <br />
                </asp:Panel>
                <!--TPT Amended: PGMSSRONE-3 -->
                <asp:Panel ID="pnlMyProjects" runat="server">
                    <table cellspacing="0px">
                        <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlMyProjects','ExtProjectReportViewID'); return false;">
                            <td style="width: 500px; padding: 5px;">
                                My Projects</td>
                            <td align="right" style="width: 450px; padding: 5px;">
                                <a href="#" id="hlMyProjects">- Hide Details</a></td>
                        </tr>
                    </table>
                    &nbsp;
                    <div id="ExtProjectReportViewID" style="display: block;">
                        <div style="text-align: right">
                            <asp:DropDownList ID="SortList1" runat="server">
                                <asp:ListItem Selected="true" Value="tblProject_ProjectPriority ASC,tblProject_ProjectTitle ASC">Project Title</asp:ListItem>
                                <asp:ListItem Value="tblProject_ProjectPriority ASC,tblProject_StartDate DESC">Start Date</asp:ListItem>
                                <asp:ListItem Value="tblProject_ProjectPriority ASC,tblProject_EndDate DESC">End Date</asp:ListItem>
                                <asp:ListItem Value="tblProject_ProjectPriority ASC,lkuProgrammeOfficeRegion_Description ASC">Programme Office Region</asp:ListItem>
                                <asp:ListItem Value="tblProject_ProjectPriority ASC,lkuProgrammeOfficeSubRegion_Description ASC">Programme Office Sub Region</asp:ListItem>
                                <asp:ListItem Value="tblProject_ProjectPriority ASC,lkuProgrammeOffice_Description ASC">Programme Office</asp:ListItem>
                                <asp:ListItem Value="tblProject_ProjectPriority ASC,ProjectObjectives ASC">VSO Goals</asp:ListItem>
                                <asp:ListItem Value="tblProject_ProjectPriority ASC,Custom_ProgrammeArea ASC">Programme Area</asp:ListItem>
                                <asp:ListItem Value="tblProject_ProjectPriority ASC,ProjectStatus ASC">Project Status</asp:ListItem>
                            </asp:DropDownList>&nbsp;<asp:Button ID="btnSort" runat="server" Text="Go" OnClick="btnSortMyProject_Click" />
                        </div>
                        &nbsp;
                        <asp:GridView Width="2000px" ID="MyProjectReportsGridView" CssClass="datagrid" BorderStyle="Solid"
                            BorderColor="silver" BorderWidth="1px" runat="server" AutoGenerateColumns="False"
                            OnRowDataBound="MyProjectReportsGridView_RowDataBound">
                            <HeaderStyle />
                            <Columns>
                                <asp:HyperLinkField DataNavigateUrlFormatString="~/EditPages/ProjectPage.aspx?BnbProject={0}"
                                    DataTextFormatString="View" DataTextField="tblProject_ProjectID" DataNavigateUrlFields="tblProject_ProjectID">
                                    <ItemStyle Width="1%" />
                                </asp:HyperLinkField>
                                <asp:BoundField DataField="tblProject_ProjectTitle" HeaderStyle-HorizontalAlign="Left"
                                    HeaderText="Project Title">
                                    <ItemStyle Width="15%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="tblProject_ProjectAbbrev" HeaderStyle-HorizontalAlign="Left"
                                    HeaderText="ABBREV">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="tblProject_ProjectCode" HeaderStyle-HorizontalAlign="Left"
                                    HeaderText="Project Fund Code">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="tblProject_ProjectSummary" HeaderStyle-HorizontalAlign="Left"
                                    HeaderText="Project Summary">
                                    <ItemStyle Width="15%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="tblProject_StartDate" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Start Date" DataFormatString="{0:dd/MMM/yyyy}">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="tblProject_EndDate" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="End Date" DataFormatString="{0:dd/MMM/yyyy}">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="lkuProgrammeOfficeRegion_Description" HeaderStyle-HorizontalAlign="Left"
                                    HeaderText="Programme Office Region">
                                    <ItemStyle Width="12%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="lkuProgrammeOffice_Description" HeaderStyle-HorizontalAlign="Left"
                                    HeaderText="Programme Office">
                                    <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="lkuProgrammeOfficeSubRegion_Description" HeaderStyle-HorizontalAlign="Left"
                                    HeaderText="Programme Office Sub Region">
                                    <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ProjectObjectives" HeaderStyle-HorizontalAlign="Left"
                                    HeaderText="VSO Goals">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Custom_ProgrammeArea" HeaderStyle-HorizontalAlign="Left"
                                    HeaderText="Programme Area">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ProjectStatus" HeaderStyle-HorizontalAlign="Left" HeaderText="Project Status">
                                    <ItemStyle Width="15%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="tblCompany_DonorOrg" HeaderStyle-HorizontalAlign="Left"
                                    HeaderText="Donor Org">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Custom_GrantAccountable" HeaderStyle-HorizontalAlign="Left"
                                    HeaderText="Grant Accountable">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Custom_ProjectResponsible" HeaderStyle-HorizontalAlign="Left"
                                    HeaderText="Project Responsible">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="tblProject_ProjectPriority" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Project Priority">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="tblProject_ProjectAccountableOfficerID" Visible="False">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    &nbsp;
                </asp:Panel>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
