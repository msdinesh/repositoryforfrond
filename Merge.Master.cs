using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Frond
{
    public partial class Merge : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // this.Page.MaintainScrollPositionOnPostBack = true;
        }


        #region "Property for hidden filed"        
        public HtmlGenericControl HiddenDivUnwntd
        {
            get
            {
                return dvHiddenFieldsUnwanted;
            }

            set
            {
                dvHiddenFieldsUnwanted = value;
            }
        }

        public HtmlGenericControl HiddenDivTarget
        {
            get
            {
                return dvHiddenFiledsTarget;
            }

            set
            {
                dvHiddenFiledsTarget = value;
            }
        }
        #endregion
    }
}
