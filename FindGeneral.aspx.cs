using System;
using System.Collections;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine.Query;
using BonoboEngine;
using BonoboWebControls;
using BonoboWebControls.Collections;
namespace Frond
{
	/// <summary>
	/// Summary description for TestFindPage2.
	/// </summary>
	public partial class FindGeneral : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
        private string domainObjectType = "";
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			lblMessages.Text = "";
			if (!this.IsPostBack)
			{
				titleBar.TitleText = "Find Menu";
				string findid = HttpContext.Current.Request.QueryString["FindTypeID"]+"";

				cboFindType.Attributes.Add("onchange","javascript:showLoadingMessage('Loading...');");
				cboFindType.AutoPostBack = true;
				// get the datasource for this control from the xml file
				DataSet findMetaSet = this.GetFindMetaData();
				DataTable findMetaTable = findMetaSet.Tables["FindOption"];
				// default findid if not specified
				if (findid=="")
				{
					// default to 17 if found
                    if (IsFindAllowed("17", findMetaTable))
                        findid = "17";
                    // next try 3 (project)
                    else if (IsFindAllowed("3", findMetaTable))
                        findid = "3";
                    else 
                    {
						// otherwise just use the first one
						if (findMetaTable.Rows.Count > 0)
							findid = findMetaTable.Rows[0]["ID"].ToString();
						else
							// no finds available
							Response.Redirect(String.Format("Message.aspx?Message={0}",
								Server.UrlEncode("You do not have permission to access any Finds")), true);
					}
				}

				// is findID in the list?
				DataRow[] searchRows = findMetaTable.Select("ID = '" + findid + "'");
				if (searchRows.Length == 0)
					Response.Redirect(String.Format("Message.aspx?Message={0}",
						Server.UrlEncode("You do not have permission to access this Find")), true);

				cboFindType.DataSource = findMetaTable;
				cboFindType.DataMember = "FindOption";
				cboFindType.DataTextField = "Name";
				cboFindType.DataValueField = "ID";
				cboFindType.SelectedValue = findid;
				cboFindType.DataBind();

				cmdFind.Attributes.Add("onclick","javascript:showLoadingMessage('Searching...');");
                doAdvancedFindHyperlink();
                
			}
            
			BnbWorkareaManager.SetPageStyleOfUser(this);
			BnbCriteriaBuilderComposite1.FindTypeID = int.Parse(cboFindType.SelectedValue);
            CheckWordButtonVisibility();
            ProjectCheckBoxVisibility();
            cboFindType.SelectedIndexChanged += new EventHandler(cboFindType_SelectedIndexChanged);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

        private bool IsFindAllowed(string findid, DataTable findMetaTable)
        {
            DataRow[] defaultSearchRows = findMetaTable.Select("ID = '" + findid + "'");
            return (defaultSearchRows.Length > 0);
        }

		private DataSet GetFindMetaData()
		{
			DataSet findMeta  = new DataSet();
			findMeta.ReadXml(Request.MapPath(BnbDataGridForQueryDataSet1.MetaDataXmlUrl));

			// if workareas are turned on, we want to filter the list
			if (BnbWebFormManager.WorkareaChecking)
			{
				BnbSessionInfo sesh = BnbEngine.SessionManager.GetSessionInfo();
				DataTable findOption = findMeta.Tables["FindOption"];
				for(int i=findOption.Rows.Count-1;i>=0;i--)
				{
					string viewName = findOption.Rows[i]["QueryView"].ToString();
					if (! sesh.AllowFindView(viewName))
						findOption.Rows[i].Delete();
				}
			}

			return findMeta;
		}

		protected void cmdFind_Click(object sender, System.EventArgs e)
		{
			this.PerformFind(false);
            
		}

        private void CheckWordButtonVisibility()
        {
            // disables Word button when user selects any option different from: Project, Proposal, Contract and Reports Due.
            //TPT:added ID:20 to condition - iteration 5
            cmdWord.Enabled = (cboFindType.SelectedValue == "13" || cboFindType.SelectedValue == "14" || cboFindType.SelectedValue == "16" || cboFindType.SelectedValue == "20");
            cmdWord.Visible = (cboFindType.SelectedValue == "13" || cboFindType.SelectedValue == "14" || cboFindType.SelectedValue == "16" || cboFindType.SelectedValue == "20");
        }

        private void PerformFind(bool isForReport)
		{
			// get criteria from the CriteriaBuilderComposite
			BnbCriteria findCriteria = null;
      
			
            try 
			{
				findCriteria = BnbCriteriaBuilderComposite1.Criteria;
			}
			catch(BnbCriteriaException ce)
			{
				lblMessages.Text = ce.Message;
			}
			if (findCriteria != null)
			{
				//add a new criteria to the search if the unfilled checkbox has been selected.
				//this criteria brings back only records that the fill status is not cancelled

				//loop through all the find criteria
				foreach(BonoboEngine.Query.BnbCondition qe in findCriteria.QueryElements)
				{
					//if the cirteria is a boolean condition and is custom_unfilled
					if(qe.GetType().FullName == "BonoboEngine.Query.BnbBooleanCondition")
					{
						BonoboEngine.Query.BnbBooleanCondition thisCondition = (BonoboEngine.Query.BnbBooleanCondition)qe;
						if(thisCondition.FieldName == "Custom_Unfilled")
						{
							//add a new find condition to the query to also only bring back records that have not had their fill status cancelled
							BonoboEngine.Query.BnbTextCondition cndFillStatus = new BnbTextCondition("lkuPositionFillStatus_Description","Cancelled",BonoboEngine.Query.BnbTextCompareOptions.AnyPartOfField,BnbLogicalOperatorType.AndNot);
							findCriteria.QueryElements.Add(cndFillStatus);
							break;
						}
					}
				}

                //TPT:Added this condition to query to filter closed and abandoned projects - Iteration 5
                if (cboFindType.SelectedValue == "17" || cboFindType.SelectedValue == "20")
                {
                    if (chkinclude.Checked == false)
                    {
                        BonoboEngine.Query.BnbTextCondition projectstatus = new BnbTextCondition("lkuProjectStatus_Description", "Closed", BonoboEngine.Query.BnbTextCompareOptions.AnyPartOfField, BnbLogicalOperatorType.AndNot);
                        BonoboEngine.Query.BnbTextCondition projectstatus1 = new BnbTextCondition("lkuProjectStatus_Description", "Abandoned", BonoboEngine.Query.BnbTextCompareOptions.AnyPartOfField, BnbLogicalOperatorType.AndNot);
                        findCriteria.QueryElements.Add(projectstatus);
                        findCriteria.QueryElements.Add(projectstatus1);
                    }
                }
				
				// find view name
				DataTable findOption = this.GetFindMetaData().Tables["FindOption"];
				DataRow findRow = findOption.Select("ID = '" + cboFindType.SelectedValue + "'")[0];
//				string viewName = findRow["QueryView"].ToString();
                string viewName = findRow["QueryView"].ToString() + ((isForReport) ? "ForReport" : "");
                string findName = findRow["Name"].ToString();
				string linkTarget = findRow["LinkTarget"].ToString();
				domainObjectType = findRow["DomainObjectType"].ToString();


				// run view
				BnbQuery  findQuery = new BnbQuery(viewName, findCriteria);
                findQuery.MaxRows = (isForReport) ? 0 : 250;
//                findQuery.MaxRows = 250;
				findQuery.NoLocks = true;
                Session["findQuery"] = findQuery;

                BnbQueryDataSet findResults = null;
				try
				{
					findResults = findQuery.Execute();
				} 
				catch (System.Data.SqlClient.SqlException ex)
				{
					if (ex.Message.StartsWith("Timeout expired"))
					{
						lblMessages.Text = "Your search was taking too long - please narrow your search. ";
						if (this.LogFinds)
							doLogFind(findName, findQuery, 0, true);
					
					}
					else 
						throw ex;

				}
				if (findResults != null)
				{
					// apply rules to results
					BonoboDomainObjects.BnbQueryRules.ApplyRulesToQueryData(findResults);

					if (this.LogFinds)
						doLogFind(findName, findQuery, findResults.Tables["Results"].Rows.Count, false);
					

					// bind to view grid
					BnbDataGridForQueryDataSet1.IsMaxRowResultsExceeded = findQuery.MaxRowsExceeded;
					BnbDataGridForQueryDataSet1.LinkTarget = linkTarget;
					BnbDataGridForQueryDataSet1.DomainObjectType = domainObjectType;
					BnbDataGridForQueryDataSet1.QueryDataSet = findResults;
                    doSetUpDataGridColumns();

                }
			}
			
		}

		private void doLogFind(string findName, BnbQuery findQuery, int resultCount, bool timedOut)
		{
			// get a description of the user criteria
			BnbQueryBuilder qb = new BnbQueryBuilder(findQuery);
			string criteriaDesc = findQuery.Criteria.AsUserDescription(qb);
			if (!timedOut)
			{
				BnbWebFormManager.LogAction(String.Format("Find - {0}",findName),
					String.Format("Criteria: {0}  Rows Returned: {1}",
					criteriaDesc,
					resultCount), this);
			}
			else
			{
				BnbWebFormManager.LogAction(String.Format("Find - {0} (Timed Out)",findName),
					String.Format("Criteria: {0}  Rows Returned: 0 (Timed Out)",
					criteriaDesc,
					resultCount), this);

			}
		}

		/// <summary>
		/// returns true if 'LogFinds' is turned on in web.config
		/// </summary>
		private bool LogFinds
		{
			get
			{
			
				if (ConfigurationSettings.AppSettings["LogFinds"] == null)
					return false;
				bool checking = Boolean.Parse (ConfigurationSettings.AppSettings["LogFinds"].ToString());
				return checking;
			}
		
		}

        public string GetPopupWindowString(string url, string popuprefName)
        {
            string s = String.Format("window.open('{0}','{1}','menubar=yes,scrollbars=yes,top=0,left=0,width=500,height=400,resizable=yes');", url, popuprefName);
            return s;
        }

        //<!-- PGM 166
        protected void cmdWord_Click(object sender, System.EventArgs e)
        {
            try
            {
                PerformFind(true);
                Response.Write("<script language=\"JavaScript\">" + GetPopupWindowString("Reports/WordReport.aspx?domainObjectType=" + domainObjectType, "PrintReport") + "</script>");
            }
            catch (BnbCriteriaException ce)
            {
                lblMessages.Text = ce.Message;
            }
        }

        private void doSetUpDataGridColumns()
        {
            BnbDataGridColumnCollection cols = BnbDataGridForQueryDataSet1.Columns;
            string colname = cols.GetColumnName("Amount Requested (�)");
            if (colname != null) cols[colname].FormatExpression = "#,0";

            colname = "";
            colname = cols.GetColumnName("Amount Approved (�)");
            if (colname != null) cols[colname].FormatExpression = "#,0";

            colname = "";
            colname = cols.GetColumnName("Contract Amount (�)");
            if (colname != null) cols[colname].FormatExpression = "#,0";


        }
        //TPT : Added the following methods, for setting properties of clear button & checkbox - iteration 5
        private void ProjectCheckBoxVisibility()
        {
            chkinclude.Visible = (cboFindType.SelectedValue == "17" || cboFindType.SelectedValue == "20");
        }
        protected void cmdclear_Click(object sender, EventArgs e)
        {
            BnbCriteriaBuilderComposite1.ClearValues = true;
            chkinclude.Checked = false;
        }

        private void doAdvancedFindHyperlink()
        {
            string findid = HttpContext.Current.Request.QueryString["FindTypeID"] + "";
            if (findid == "17" || cboFindType.SelectedValue == "17")
            {
                lnkAdvancedFind.Visible = true;
                lnkAdvancedFind.Text = "Switch to Advanced Find >>";
                lnkAdvancedFind.NavigateUrl = "FindGeneral.aspx?FindTypeID=20";
            }
            if (findid == "20" || cboFindType.SelectedValue == "20")
            {
                lnkAdvancedFind.Visible = true;
                lnkAdvancedFind.Text = "Switch to Basic Find >>";
                lnkAdvancedFind.NavigateUrl = "FindGeneral.aspx?FindTypeID=17";
            }
        }

        private void cboFindType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboFindType.SelectedValue == "17")
            {
                lnkAdvancedFind.Visible = true;
                lnkAdvancedFind.Text = "Switch to Advanced Find >>";
                lnkAdvancedFind.NavigateUrl = "FindGeneral.aspx?FindTypeID=20";
            }
            else if (cboFindType.SelectedValue == "20")
            {
                lnkAdvancedFind.Visible = true;
                lnkAdvancedFind.Text = "Switch to Basic Find >>";
                lnkAdvancedFind.NavigateUrl = "FindGeneral.aspx?FindTypeID=17";
            }
            else
                lnkAdvancedFind.Visible = false;
        }

	}
}
