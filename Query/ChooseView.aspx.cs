using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine.Query;
using BonoboEngine;

namespace Frond.Query
{
	/// <summary>
	/// Summary description for ChooseView.
	/// </summary>
	public partial class ChooseView : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			BnbEngine.SessionManager.ClearObjectCache();
            BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
			if (!this.IsPostBack)
			{
				this.SetupStandardQueryGrid();
				this.SetupUserQueryGrid();
				this.SetupGlobalQueryGrid();
				this.SetupAllViewsGrid();
				titleBar.TitleText = "Query Chooser";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void SetupStandardQueryGrid()
		{
			DataTable standard = ChooseView.GetStandardQueryData();
			standard.DefaultView.Sort = "QueryName";
			uxStandardQueryGrid.DataSource = standard.DefaultView;
			uxStandardQueryGrid.DataBind();

		}

		private void SetupUserQueryGrid()
		{
			BnbCriteria uqCrit = new BnbCriteria();
			BnbListCondition list = new BnbListCondition("tblStoredQuery_OwnerUserID", BnbEngine.SessionManager.GetSessionInfo().UserID);
			uqCrit.QueryElements.Add(list);
			uxUserQueryGrid.Criteria = uqCrit;
		}

		private void SetupGlobalQueryGrid()
		{
			BnbCriteria uqCrit = new BnbCriteria();
			BnbBooleanCondition bc  = new BnbBooleanCondition("tblStoredQuery_SharePublic", true);
			uqCrit.QueryElements.Add(bc);
			uxGlobalQueryGrid.Criteria = uqCrit;
		}

		private void SetupAllViewsGrid()
		{
			BnbQueryBuilder qb = new BnbQueryBuilder();
			DataTable all  = qb.AvailableFindViews();
			all.DefaultView.Sort = "ViewUserAlias";
			uxAllViewsGrid.DataSource = all.DefaultView;
			uxAllViewsGrid.DataBind();

		}

		public static DataTable GetStandardQueryData()
		{
			DataSet loadFromXml = new DataSet();
			loadFromXml.ReadXml(HttpRuntime.AppDomainAppPath + "/xml/BonoboQueryStandard.xml", XmlReadMode.InferSchema);
			DataTable standard = loadFromXml.Tables["StandardQuery"];
			return standard;
		}
	}
}
