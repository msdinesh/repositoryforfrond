<%@ Register TagPrefix="uc1" TagName="WaitMessagePanel" Src="../UserControls/WaitMessagePanel.ascx" %>
<%@ Page language="c#" Codebehind="CustomQuery.aspx.cs" AutoEventWireup="True" Inherits="Frond.Query.CustomQuery" %>
<%@ Register TagPrefix="bnbcustomsearchcomposite" Namespace="BonoboWebControls.CriteriaControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>CustomSearch</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal><LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet"><asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<script language="javascript">
   // code to remember scroll-bar position between postbacks
   // (like SmartNavigation, but without the bugs)
   // also needs hidden coords textboxes at end of page
   function AltSmartNav_GetCoords()
   {
      var scrollX, scrollY;
      
      if (document.all)
      {
         if (!document.documentElement.scrollLeft)
            scrollX = document.body.scrollLeft;
         else
            scrollX = document.documentElement.scrollLeft;
               
         if (!document.documentElement.scrollTop)
            scrollY = document.body.scrollTop;
         else
            scrollY = document.documentElement.scrollTop;
      }   
      else
      {
         scrollX = window.pageXOffset;
         scrollY = window.pageYOffset;
      }
   
      document.forms["Form1"].hiddenXCoord.value = scrollX;
      document.forms["Form1"].hiddenYCoord.value = scrollY;
   }
   
   function AltSmartNav_Scroll()
   {
      var x = document.forms["Form1"].hiddenXCoord.value;
      var y = document.forms["Form1"].hiddenYCoord.value;
      window.scrollTo(x, y);
   }
   
   window.onload = AltSmartNav_Scroll;
   window.onscroll = AltSmartNav_GetCoords;
   window.onkeypress = AltSmartNav_GetCoords;
   window.onclick = AltSmartNav_GetCoords;
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent"><uc1:title id="titleBar" runat="server" imagesource="../Images/query24silver.gif"></uc1:title>
				<p><asp:Label ID="uxWelcomeMessage" Runat="server">This Query page is like the Find page but gives you more ways to customise your searching. Click on the Help link in the sidebar for more details. </br>
<b>Please make certain you never use this data for mailing purposes, some of the volunteers might be mail blocked.</b></asp:Label><asp:Label id="uxPermissionMessage" runat="server" CssClass="feedback" Visible="False"></asp:Label>
				</p>
				<div id="querycontrols">
					<table style="WIDTH: 100%">
						<tr>
							<td>Query:
								<asp:dropdownlist id="uxViewCombo" runat="server" Width="208px" AutoPostBack="True" onselectedindexchanged="uxViewCombo_SelectedIndexChanged"></asp:dropdownlist><asp:label id="uxQueryName" runat="server"></asp:label>&nbsp;&nbsp;&nbsp;&nbsp;
								<A href="ChooseView.aspx">More Queries ...</A>&nbsp;&nbsp;&nbsp;&nbsp; <A href="QueryHistory.aspx">
									Query History ...</A>
							</td>
							<td style="TEXT-ALIGN: right"><asp:checkbox id="uxRememberQuery" runat="server" AutoPostBack="True" Text="Remember Query"></asp:checkbox></td>
						</tr>
					</table>
					<P><bnbcustomsearchcomposite:bnbquerybuildercomposite id="uxQueryBuilderComposite" runat="server"></bnbcustomsearchcomposite:bnbquerybuildercomposite></P>
					<P>
						<table style="WIDTH: 100%">
							<tr>
								<td><asp:button id="uxRunQueryButton" runat="server" Text="Run Query" onclick="uxRunQueryButton_Click"></asp:button></td>
								<td style="TEXT-ALIGN: right"><asp:button id="uxSaveQueryAsButton" runat="server" Text="Save As ..."></asp:button><asp:button id="uxSaveQueryButton" runat="server" Text="Save"></asp:button><asp:panel id="uxSavePanel" runat="server" Visible="False">
										<P>Please enter a name for the Query:
											<asp:TextBox id="uxQueryNameTextBox" runat="server"></asp:TextBox>
											<asp:Button id="uxSaveConfirmButton" runat="server" Text="Save"></asp:Button><BR>
											<asp:CheckBox id="uxGlobalCheckbox" runat="server" Text="Make the Query global"></asp:CheckBox>
											<asp:Button id="uxCancelSaveButton" runat="server" Text="Cancel"></asp:Button></P>
										<P>
											<asp:Label id="uxSaveFeedback" runat="server" CssClass="feedback"></asp:Label></P>
									</asp:panel></td>
							</tr>
						</table>
					</P>
					<P><bnbdatagrid:bnbsmartgrid id="uxSmartGrid" runat="server" OnExportClicked="uxSmartGrid_ExportClicked" ShowCopyButton="True" ShowExportButton="True"></bnbdatagrid:bnbsmartgrid></P>
				</div>
				<P><asp:textbox id="uxHiddenViewChanged" runat="server" Visible="False"></asp:textbox><asp:textbox id="uxHiddenStoredQueryID" runat="server" Visible="False"></asp:textbox>
					<asp:TextBox id="uxHiddenNextColumnDisplayList" runat="server" Visible="False"></asp:TextBox>
					<asp:TextBox id="uxHiddenNextSortColumn" runat="server" Visible="False"></asp:TextBox>
					<asp:TextBox id="uxHiddenNextSortDescending" runat="server" Visible="False"></asp:TextBox>&nbsp;
					<asp:TextBox id="hiddenXCoord" runat="server" style="VISIBILITY:hidden"></asp:TextBox>
					<asp:TextBox id="hiddenYCoord" runat="server" style="VISIBILITY:hidden"></asp:TextBox>
					<uc1:WaitMessagePanel id="uxWaitMessagePanel" runat="server" DivToHide="querycontrols"></uc1:WaitMessagePanel></P>
			</div>
		</form>
	</body>
</HTML>
