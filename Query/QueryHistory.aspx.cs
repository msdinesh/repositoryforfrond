using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboWebControls;

namespace Frond.Query
{
	/// <summary>
	/// Summary description for QueryHistory.
	/// </summary>
	public partial class QueryHistory : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			BnbEngine.SessionManager.ClearObjectCache();
            BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
			if (!this.IsPostBack)
			{
				titleBar.TitleText = "Query History";
				uxHistoryRowsTextbox.Text = "20";
				this.SetupQueryHistoryGrid();
			}
		}

		private void SetupQueryHistoryGrid()
		{
			BnbCriteria historyCrit = new BnbCriteria();
			BnbListCondition list = new BnbListCondition("appQueryLog_UserID", BnbEngine.SessionManager.GetSessionInfo().UserID);
			historyCrit.QueryElements.Add(list);
			//uxQueryHistoryGrid.Criteria = historyCrit;
			//uxQueryHistoryGrid.MaxRows = 20;

			BnbQuery q = new BnbQuery("vwBonoboUserQueryHistory", historyCrit);
			q.MaxRows = this.HistoryRows;
			BnbQueryDataSet results = q.Execute();
			uxQueryHistoryGrid2.DataSource = results.Tables["Results"];
			uxQueryHistoryGrid2.DataBind();
		}

		public int HistoryRows
		{
			get
			{
				if (BnbRuleUtils.IsNumericInt(uxHistoryRowsTextbox.Text))
				{
					if (int.Parse(uxHistoryRowsTextbox.Text) < 1000)
						return int.Parse(uxHistoryRowsTextbox.Text);
					else
						return 1000;
				}
				else
					return 20;
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void uxRefreshButton_Click(object sender, System.EventArgs e)
		{
			this.SetupQueryHistoryGrid();
		}
	}
}
