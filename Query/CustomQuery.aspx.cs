using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Meta;
using BonoboEngine.Query;

namespace Frond.Query
{
	/// <summary>
	/// CustomQuery is a page for building and running custom Queries
	/// Its a bit like the old Find page but allows much more user customisation.
	/// the page is designed to load Queries from several different sources:
	/// - Standard Queries (defined in an XML file) can be selected via a combo box
	/// - Stored Queries can be loaded via the ChooseView.aspx page
	/// - Base Views can be used via the ChooseView.aspx page
	/// - the current Query (and results) can be 'Remembered' (via the Remember Query checkbox) and 
	///   the criteria and results will be automatically re-displayed when the user returns to the page.
	///   
	/// This page also allows StoredQueries to be saved or updated via the Save buttons.
	/// Users must have Workarea-access to the Views used by the Queries to run them.
	/// </summary>
	public partial class CustomQuery : System.Web.UI.Page
	{
		
		protected UserControls.Title titleBar;

		

		protected void Page_Load(object sender, System.EventArgs e)
		{
			BnbEngine.SessionManager.ClearObjectCache();

			uxQueryBuilderComposite.RegisterClientScriptBlockToPage();
			// if user has dev-admin permission, show extra buttons on the querybuilder control
			if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(1037))
				uxQueryBuilderComposite.AllowDevAdminFunctions = true;

			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
			
			if (!this.IsPostBack)
			{
				titleBar.TitleText = "Query";
				uxRunQueryButton.Attributes.Add("onclick","javascript:showLoadingMessage('Searching...');");
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				uxHiddenViewChanged.Text = "1";
				this.PopulateStandardQueryCombo();

				uxSmartGrid.ClearSettings();
				uxSmartGrid.HyperlinkMap = this.MakeHyperlinkMap();	

				// set up the page based on what's in the querystring
				this.SetupBasedOnQueryString();
			
		
				this.ShowSaveButtons();

			}
			

		}

		/// <summary>
		/// On the first page load, this sets up the intial query
		/// it may be...
		/// - a stored query (from BnbStoredQuery)
		/// - a standard query (from xml)
		/// - a base view
		/// - a retrieved query from appQueryLog
		/// - the remembered query from last time
		/// </summary>
		private void SetupBasedOnQueryString()
		{
			// if the querystring says load a StoredQuery...
			if (Request.QueryString["BnbStoredQuery"] != null)
			{
				Guid sqID = new Guid(Request.QueryString["BnbStoredQuery"]);
				uxHiddenStoredQueryID.Text = sqID.ToString();
				this.LoadStoredQuery(sqID);
				return;
			}

			// otherwise, if the querystring says load a StandardQuery (from XML file) ...
			if (Request.QueryString["StandardQueryID"] != null)
			{
				string standardID = Request.QueryString["StandardQueryID"];
				// if its not in the combo, means user doesnt have permission
				if (uxViewCombo.Items.FindByValue(standardID) == null)
				{
					this.ShowNoPermissionMessage();
				}
				else
				{
					uxViewCombo.SelectedValue = standardID;
					this.StandardQuerySelected();
				}
				return;
			}

			// otherwise, if the querystring says build a query from a base view ...
			if (Request.QueryString["ViewName"] != null)
			{
				string viewName = Request.QueryString["ViewName"];
				this.ViewSelected(viewName);
				return;
			}

			// otherwise, if the querystring says retrieve a previous query from the log  ...
			if (Request.QueryString["QueryLogID"] != null)
			{
				Guid queryLogID = new Guid(Request.QueryString["QueryLogID"]);
				this.LoadQueryFromQueryLog(queryLogID);
				return;
			}

			// otherwise, did the user check 'Remember Query'?
			if (Session["CustomQuery_Remember"] != null &&
				(bool)Session["CustomQuery_Remember"] == true)
			{
				this.RecallRememberedQuery();
				return;
			}
			
			
			// finally, if none of the above, show the first row from the
			// standard query combo
			this.StandardQuerySelected();
			
		}

		private void Page_OnPreRender(object sender, System.EventArgs e)
		{
		}

		/// <summary>
		/// Loads the standard queries (from xml file) into the combo
		/// If user does not have workarea-access to the views, they will not
		/// be included in the combo
		/// </summary>
		private void PopulateStandardQueryCombo()
		{
			DataTable standard = ChooseView.GetStandardQueryData();
			// remove any that user does not have permission for
			BnbSessionInfo seshInfo = BnbEngine.SessionManager.GetSessionInfo();
			for(int i=standard.Rows.Count-1;i>=0;i--)
			{
				DataRow rowLoop = standard.Rows[i];
				if (!seshInfo.AllowFindView(rowLoop["ViewName"].ToString()))
					rowLoop.Delete();
			}


			standard.DefaultView.Sort = "QueryName";
			uxViewCombo.DataSource = standard.DefaultView ;
			uxViewCombo.DataValueField = "ID";
			uxViewCombo.DataTextField = "QueryName";
			uxViewCombo.DataBind();
            uxViewCombo.SelectedValue = "10";
			
		}

		/// <summary>
		/// called when the user does not have the right permissions for the selected view
		/// </summary>
		private void ShowNoPermissionMessage()
		{
			uxPermissionMessage.Text = "You do not have permission to run Queries on the selected View. <a href=\"ChooseView.aspx\">Please select a different Query/View.</a>";
			uxPermissionMessage.Visible = true;
			uxWelcomeMessage.Visible = false;
			uxSmartGrid.Visible = false;
			uxQueryBuilderComposite.Visible = false;
			uxViewCombo.Visible = false;
			uxRunQueryButton.Visible = false;
			uxSaveQueryAsButton.Visible = false;
			uxSaveQueryButton.Visible = false;
			uxRememberQuery.Visible = false;

		}

		/// <summary>
		/// Called when a standard query (from the XML file) has been selected
		/// in the combo box.
		/// Loads the details into the relevant controls
		/// </summary>
		private void StandardQuerySelected()
		{
			uxSmartGrid.Visible = false;
			int standardID = int.Parse(uxViewCombo.SelectedValue);
			DataTable standard = ChooseView.GetStandardQueryData();
			DataRow[] findRows = standard.Select("ID = '" + standardID.ToString() + "'"); 
			if (findRows.Length != 1)
			{
				uxQueryBuilderComposite.Visible = false;
				return;
			}
			DataRow standardRow = findRows[0];
			// set up querybuilder
			uxQueryBuilderComposite.ViewName = standardRow["ViewName"].ToString();
            // TPT amended for PGMSSRONE-8: to excluded the columns in QueryBuilderComposite dropdownlist
            uxQueryBuilderComposite.ExcludeColumns = standardRow["ExcludeColumns"].ToString();
			string xmlCriteria = standardRow["CriteriaXML"].ToString();
			// trim spaces, carriage returns and tabs from start and end
			xmlCriteria= xmlCriteria.Trim(new char[]{(char)13, (char)10, (char)32, (char)9});
			// turn xml into wheretable
			BnbCriteria newCrit = BnbCriteria.BuildFromXml(xmlCriteria);
			BnbQueryBuilder qb = new BnbQueryBuilder(uxQueryBuilderComposite.ViewName);
			qb.Query.Criteria = newCrit;
			DataTable defaultWhere = qb.CriteriaAsWhereTable();
			uxQueryBuilderComposite.WhereTable = defaultWhere;
			uxQueryBuilderComposite.DefaultAdvancedMode();

			this.NextColumnDisplayList = standardRow["ColumnDisplayList"].ToString();
			this.NextSortColumn = standardRow["SortColumn"].ToString();
			if (standardRow["SortDescending"].ToString() != "")
				this.NextSortDescending = (bool)standardRow["SortDescending"];


			uxHiddenViewChanged.Text = "1";

		}

		/// <summary>
		/// Called when a standard query (from the XML file) has been selected
		/// in the combo box.
		/// Loads the details into the relevant controls
		/// </summary>
		private void LoadQueryFromQueryLog(Guid queryLogID)
		{
			uxSmartGrid.Visible = false;
			BnbQueryBuilder tempqb = new BnbQueryBuilder();
			DataTable oldQuery = tempqb.GetQueryHistory(queryLogID);

			DataRow oldRow = oldQuery.Rows[0];

			// set up querybuilder
			uxQueryBuilderComposite.ViewName = oldRow["ViewName"].ToString();
			string xmlCriteria = oldRow["CriteriaXML"].ToString();
			
			// turn xml into wheretable
			BnbCriteria newCrit = BnbCriteria.BuildFromXml(xmlCriteria);
			
			BnbQueryBuilder qb = new BnbQueryBuilder(uxQueryBuilderComposite.ViewName);
			qb.Query.Criteria = newCrit;
			DataTable defaultWhere = qb.CriteriaAsWhereTable();
			uxQueryBuilderComposite.WhereTable = defaultWhere;
			uxQueryBuilderComposite.DefaultAdvancedMode();

			this.NextColumnDisplayList = oldRow["ColumnDisplayList"].ToString();
			this.NextSortColumn = oldRow["SortColumn"].ToString();
			if (oldRow["SortDescending"].ToString() != "")
				this.NextSortDescending = (bool)oldRow["SortDescending"];

			this.ShowViewUserAlias(uxQueryBuilderComposite.ViewName);

			uxHiddenViewChanged.Text = "1";

		}

	

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			
			this.uxRememberQuery.CheckedChanged += new System.EventHandler(this.uxRememberQuery_CheckedChanged);
			this.uxSaveQueryAsButton.Click +=new EventHandler(uxSaveQueryAsButton_Click);
			this.uxSaveQueryButton.Click +=new EventHandler(uxSaveQueryButton_Click);
			this.uxSaveConfirmButton.Click += new System.EventHandler(this.uxSaveConfirmButton_Click);
			this.uxCancelSaveButton.Click += new System.EventHandler(this.uxCancelSaveButton_Click);
			this.uxSmartGrid.OptionsChanged +=new EventHandler(uxSmartGrid_OptionsChanged);
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		

		/// <summary>
		/// Runs the Query and displays the results
		/// </summary>
		private void RunQuery()
		{
			if (uxQueryBuilderComposite.CheckFilter())
			{
				BnbQueryBuilder  qb = uxQueryBuilderComposite.GetQueryBuilder(true);
				qb.Query.Timeout = 60*5;
				qb.Query.NoLocks = true;
				// show summary in query builder
				uxQueryBuilderComposite.ShowCriteriaUserDescription(qb);
                
                

				// work out the smartgrid options (cols to display, etc)
				if (uxHiddenViewChanged.Text == "1")
				{
					qb.ColumnDisplayList = this.NextColumnDisplayList;
					qb.SortColumn = this.NextSortColumn;
					qb.SortDescending = this.NextSortDescending;
					if (uxHiddenStoredQueryID.Text != "")
					{
						BnbStoredQuery sq = BnbStoredQuery.Retrieve(new Guid(uxHiddenStoredQueryID.Text));
						qb.BasedOnStoredQueryID = sq.ID;
					}
					uxHiddenViewChanged.Text = "0";
				} 
				else
				{
					// if view has been run before get details
					if (uxSmartGrid.Visible)
					{
						qb.ColumnDisplayList = uxSmartGrid.ColumnDisplayList;
						qb.SortColumn = uxSmartGrid.SortColumn;
						qb.SortDescending = uxSmartGrid.SortDescending;
					}
				}
				this.NextColumnDisplayList = "";
				this.NextSortColumn = "";
				this.NextSortDescending = false;

				// run view
				BnbQueryDataSet queryResults = null;
				try
				{
					queryResults = qb.ExecuteQueryWithLogging();
				} 
				catch (System.Data.SqlClient.SqlException ex)
				{
					if (ex.Message.StartsWith("Timeout expired"))
					{
						uxQueryBuilderComposite.WarningMessage = "Your query was taking too long - please narrow your search. ";
					}
					else 
						throw ex;

				}
				if (queryResults != null)
				{
					// apply rules to results
					BonoboDomainObjects.BnbQueryRules.ApplyRulesToQueryData(queryResults);

					// if no column list then generate default
					if (qb.ColumnDisplayList == null || qb.ColumnDisplayList == "")
						qb.GenerateDefaultColumnDisplayList(queryResults);

					// bind to smart grid
					uxSmartGrid.QueryDataSet = queryResults;
					uxSmartGrid.ColumnDisplayList = qb.ColumnDisplayList;
					uxSmartGrid.SortColumn = qb.SortColumn;
					uxSmartGrid.SortDescending = qb.SortDescending;
					uxSmartGrid.Visible = true;
					if (uxRememberQuery.Checked)
					{
						this.RememberQueryCriteriaInfo();
						this.RememberResultsInfo();
					}
				}
				
			}

		}

		/// <summary>
		/// remembers the current settings from the QueryBuilder control
		/// i.e. the filter criteria
		/// </summary>
		private void RememberQueryCriteriaInfo()
		{
			Session["CustomQuery_Remember"] = true;
			Session["CustomQuery_View"] = uxQueryBuilderComposite.ViewName;
            // TPT amended for PGMSSRONE-8: to excluded the columns in QueryBuilderComposite dropdownlist
            Session["CustomQuery_Exclude"] = uxQueryBuilderComposite.ExcludeColumns;
			Session["CustomQuery_WhereTable"] = uxQueryBuilderComposite.WhereTable;
			Session["CustomQuery_AdvancedMode"] = uxQueryBuilderComposite.AdvancedMode;
		}

		/// <summary>
		/// remembers the current settings from the SmartGrid control
		/// i.e. the results, and the column display/sort settings
		/// </summary>
		private void RememberResultsInfo()
		{
			Session["CustomQuery_Remember"] = true;
			Session["CustomQuery_QueryDataSet"] = uxSmartGrid.QueryDataSet;
			Session["CustomQuery_ColumnDisplayList"] = uxSmartGrid.ColumnDisplayList;
			Session["CustomQuery_SortColumn"] = uxSmartGrid.SortColumn;
			Session["CustomQuery_SortDescending"] = uxSmartGrid.SortDescending;
		}

		/// <summary>
		/// Loads the 'remembered query' from session variables
		/// into the page controls so that it is re-displayed
		/// </summary>
		private void RecallRememberedQuery()
		{
			uxQueryBuilderComposite.ViewName = Session["CustomQuery_View"].ToString();
            // TPT amended for PGMSSRONE-8: to excluded the columns in QueryBuilderComposite dropdownlist
            uxQueryBuilderComposite.ExcludeColumns = Session["CustomQuery_Exclude"].ToString();
			uxQueryBuilderComposite.WhereTable = (DataTable)Session["CustomQuery_WhereTable"];
			uxQueryBuilderComposite.AdvancedMode = (bool)Session["CustomQuery_AdvancedMode"];

			uxSmartGrid.QueryDataSet = (BnbQueryDataSet)Session["CustomQuery_QueryDataSet"];
			uxSmartGrid.ColumnDisplayList = Session["CustomQuery_ColumnDisplayList"].ToString();
			if (Session["CustomQuery_SortColumn"] != null)
				uxSmartGrid.SortColumn = Session["CustomQuery_SortColumn"].ToString();
			uxSmartGrid.SortDescending = (bool)Session["CustomQuery_SortDescending"];

			// select view in combo if possible
			uxViewCombo.SelectedItem.Selected = false;
			foreach(ListItem itemLoop in uxViewCombo.Items)
				if (itemLoop.Value == uxQueryBuilderComposite.ViewName)
					itemLoop.Selected = true;
				
			uxRememberQuery.Checked = true;
		}

		/// <summary>
		/// The hyperlink map tells the BnbSmartGrid how to build hyperlinks for Guid fields.
		/// It is created in this fn and passed to the SmartGrid.
		/// </summary>
		/// <returns></returns>
		private DataTable MakeHyperlinkMap()
		{
			DataTable map = uxSmartGrid.MakeEmptyHyperlinkMap();
			DataRow newMap = map.NewRow();
			newMap["TableName"] = "tblIndividualKeyInfo";
			newMap["HyperlinkUrlPattern"] = "../EditPages/IndividualPage.aspx?BnbIndividual={0}";
			map.Rows.Add(newMap);

			newMap = map.NewRow();
			newMap["TableName"] = "tblApplication";
			newMap["HyperlinkUrlPattern"] = "../EditPages/ApplicationPage.aspx?BnbApplication={0}";
			map.Rows.Add(newMap);

			newMap = map.NewRow();
			newMap["TableName"] = "tblPosition";
			newMap["HyperlinkUrlPattern"] = "../EditPages/PlacementPage.aspx?BnbPosition={0}";
			map.Rows.Add(newMap);

			newMap = map.NewRow();
			newMap["TableName"] = "tblCompany";
			newMap["HyperlinkUrlPattern"] = "../EditPages/OrganisationPage.aspx?BnbCompany={0}";
			map.Rows.Add(newMap);

			newMap = map.NewRow();
			newMap["TableName"] = "tblEvent";
			newMap["HyperlinkUrlPattern"] = "../EditPages/EventPage.aspx?BnbEvent={0}";
			map.Rows.Add(newMap);

			newMap = map.NewRow();
			newMap["TableName"] = "tblFunding";
			newMap["HyperlinkUrlPattern"] = "../EditPages/FundingPage.aspx?BnbFunding={0}";
			map.Rows.Add(newMap);

            newMap = map.NewRow();
            newMap["TableName"] = "tblProject";
            newMap["HyperlinkUrlPattern"] = "../EditPages/ProjectPage.aspx?BnbProject={0}";
            map.Rows.Add(newMap);

            newMap = map.NewRow();
            newMap["TableName"] = "tblGrant";
            newMap["HyperlinkUrlPattern"] = "../Redirect.aspx?BnbGrant={0}";
            map.Rows.Add(newMap);

			return map;

		}

		private void uxRememberQuery_CheckedChanged(object sender, System.EventArgs e)
		{
			if (uxRememberQuery.Checked)
			{
				this.RememberQueryCriteriaInfo();
				this.RememberResultsInfo();
			}
			else
			{
				Session.Remove("CustomQuery_Remember");
			}	
		}

		/// <summary>
		/// Saves the current query using the BnbStoredQuery domain object
		/// either saves a new one or overwrites and existing one as neccessary
		/// </summary>
		/// <param name="queryName"></param>
		private void SaveQuery(string queryName)
		{
			BnbStoredQuery sq = null;
			// if textbox is disabled then we are doing a repeat save
			if (uxQueryNameTextBox.Enabled == false)
			{
				Guid storedqueryID = new Guid(uxHiddenStoredQueryID.Text);
				sq = BnbStoredQuery.Retrieve(storedqueryID);
			}
			else
			{
				// new recprd
				sq = new BnbStoredQuery(true);
			}
			BnbEditManager em = new BnbEditManager();
			BnbQuery xmlq = uxQueryBuilderComposite.GetQuery(false);
			BnbQuery userq = uxQueryBuilderComposite.GetQuery(true);
			BnbQueryBuilder qbuser = new BnbQueryBuilder(userq);
			sq.RegisterForEdit(em);
			if (sq.IsNew)
				sq.OwnerUserID = BnbEngine.SessionManager.GetSessionInfo().UserID;

			sq.ViewName = uxQueryBuilderComposite.ViewName;
			sq.QueryName = queryName;
			sq.CriteriaXml = xmlq.Criteria.AsXml();
			sq.CriteriaUser = userq.Criteria.AsUserDescription(qbuser);

			// has the query been executed?
			if (uxHiddenViewChanged.Text == "1")
			{
				// if not, is there a loaded query to copy the settings from?
				if (uxHiddenStoredQueryID.Text != "")
				{
					Guid originalID = new Guid(uxHiddenStoredQueryID.Text);
					// only bother copying if we are saving to a different ID
					if (!sq.ID.Equals(originalID))
					{
						BnbStoredQuery originalSq = BnbStoredQuery.Retrieve(originalID);
						sq.ColumnDisplayList = originalSq.ColumnDisplayList;
						sq.SortColumn = originalSq.SortColumn;
						sq.SortDescending = originalSq.SortDescending;
					}
				}
			}
			else
			{
				// query has been executed so get output settings from smartgrid

				sq.ColumnDisplayList = uxSmartGrid.ColumnDisplayList;
				sq.SortColumn = uxSmartGrid.SortColumn;
				sq.SortDescending = uxSmartGrid.SortDescending;
			}

			sq.SharePublic = uxGlobalCheckbox.Checked;
			
			try
			{
				em.SaveChanges();
				uxHiddenStoredQueryID.Text = sq.ID.ToString();
				this.ShowSaveButtons();
				this.ShowStoredQueryName(sq);
			} 
			catch(BnbProblemException pe)
			{
				uxSaveFeedback.Text = "Could not save because:<br/>";
				foreach(BnbProblem p in pe.Problems)
					uxSaveFeedback.Text += p.Message + "<br/>";

			}
		}

		private void uxSmartGrid_OptionsChanged(object sender, EventArgs e)
		{
			if (uxRememberQuery.Checked)
				this.RememberResultsInfo();
		}

		

		private void uxSaveConfirmButton_Click(object sender, System.EventArgs e)
		{
			this.SaveQuery(uxQueryNameTextBox.Text);
		}

		/// <summary>
		/// When a base-view has been selected from the ChooseView page
		/// this fn loads the default info for the view into the page controls
		/// </summary>
		/// <param name="viewName"></param>
		private void ViewSelected(string viewName)
		{
			// check permissions for view
			if (!BnbEngine.SessionManager.GetSessionInfo().AllowFindView(viewName))
			{
				this.ShowNoPermissionMessage();
				return;
			}

			uxHiddenViewChanged.Text = "1";
			uxQueryBuilderComposite.ViewName = viewName;
			BnbQueryBuilder qb = new BnbQueryBuilder(uxQueryBuilderComposite.ViewName);
			DataTable defaultWhere = qb.MakeEmptyWhereTable();
			uxQueryBuilderComposite.WhereTable = defaultWhere;
			uxQueryBuilderComposite.AdvancedMode = false;
			
			this.NextColumnDisplayList = "";
			this.NextSortColumn = "";
			this.NextSortDescending = false;

			this.ShowViewUserAlias(viewName);

			

			uxSmartGrid.Visible = false;
		}

		/// <summary>
		/// When a stored query has been selected on the ChooseView.aspx page
		/// this fn loads the BnbStoredQuery and copies the relevant data
		/// over to the page controls
		/// </summary>
		/// <param name="storedQueryID"></param>
		private void LoadStoredQuery(Guid storedQueryID)
		{		
			uxHiddenViewChanged.Text = "1";
			BnbStoredQuery sq = BnbStoredQuery.Retrieve(storedQueryID);

			// check permissions for view
			if (!BnbEngine.SessionManager.GetSessionInfo().AllowFindView(sq.ViewName))
			{
				this.ShowNoPermissionMessage();
				return;
			}
			
			
			uxQueryBuilderComposite.ViewName = sq.ViewName;
			BnbCriteria newCrit = BnbCriteria.BuildFromXml(sq.CriteriaXml);
			BnbQueryBuilder qb = new BnbQueryBuilder(uxQueryBuilderComposite.ViewName);
			qb.Query.Criteria = newCrit;
			DataTable loadedWhere = qb.CriteriaAsWhereTable();
			uxQueryBuilderComposite.WhereTable = loadedWhere;
			uxQueryBuilderComposite.DefaultAdvancedMode();
		
			this.NextColumnDisplayList = sq.ColumnDisplayList;
			this.NextSortColumn = sq.SortColumn;
			this.NextSortDescending = sq.SortDescending;

			this.ShowStoredQueryName(sq);
			uxSmartGrid.Visible = false;
			
		}

		/// <summary>
		/// When running a stored query, this hides the 'standard query' combo
		/// and shows the stored query name in a label instead
		/// </summary>
		/// <param name="sq"></param>
		private void ShowStoredQueryName(BnbStoredQuery sq)
		{
			// hide select view
			uxViewCombo.Visible = false;
			uxQueryName.Text = String.Format("<b>{0}</b> (Stored Query)",sq.QueryName);
		}

		private void ShowViewUserAlias(string viewName)
		{
			// hide select view
			uxViewCombo.Visible = false;
			
			// get alias
			string viewAlias = null;
			BnbQueryBuilder qb = new BnbQueryBuilder();
			DataTable all = qb.AvailableFindViews();
			DataRow[] findView = all.Select("ViewName = '" + viewName + "'");
			if (findView.Length == 1)
				viewAlias = findView[0]["ViewUserAlias"].ToString();

			if (viewAlias == null)
				viewAlias = viewName;

			uxQueryName.Text = String.Format("<b>{0}</b>",viewAlias);
		}



		private void uxSaveAsFindButton_Click(object sender, System.EventArgs e)
		{
			
		}

		private void uxCancelSaveButton_Click(object sender, System.EventArgs e)
		{
			this.ShowSaveButtons();
		}

		/// <summary>
		/// Works out which Save buttons to show
		/// based on what query has been loaded
		/// </summary>
		private void ShowSaveButtons()
		{
			// no save buttons if the 'no permission' message is visible
			if (uxPermissionMessage.Visible)
			{
				uxSaveQueryAsButton.Visible = false;
				uxSaveQueryButton.Visible = false;
				return;
			}
			uxSaveFeedback.Text = "";
			uxSavePanel.Visible = false;
			uxSaveQueryAsButton.Visible = true;
			if (uxHiddenStoredQueryID.Text != "")
				uxSaveQueryButton.Visible = true;
			else
				uxSaveQueryButton.Visible = false;

		}

		protected void uxRunQueryButton_Click(object sender, System.EventArgs e)
		{
			this.RunQuery();
		}

		protected void uxViewCombo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.StandardQuerySelected();
		}

		/// <summary>
		/// when a query is loaded, the column display list cannot be loaded straight into the SmartGrid
		/// because it is not yet visible. It is stored here instead until the first run of the query.
		/// </summary>
		public string NextColumnDisplayList
		{
			get{return uxHiddenNextColumnDisplayList.Text;}
			set{uxHiddenNextColumnDisplayList.Text = value;}
		}

		/// <summary>
		/// when a query is loaded, the SortColumn cannot be loaded straight into the SmartGrid
		/// because it is not yet visible. It is stored here instead until the first run of the query.
		/// </summary>
		public string NextSortColumn
		{
			get{return uxHiddenNextSortColumn.Text;}
			set{uxHiddenNextSortColumn.Text = value;}
		}

		/// <summary>
		/// when a query is loaded, the SortDescending cannot be loaded straight into the SmartGrid
		/// because it is not yet visible. It is stored here instead until the first run of the query.
		/// </summary>
		public bool NextSortDescending
		{
			get
			{
				if (uxHiddenNextSortDescending.Text == "")
					return false;
				return bool.Parse(uxHiddenNextSortDescending.Text);
			}
			set{uxHiddenNextSortDescending.Text = value.ToString();}
		}

		private void uxSaveQueryAsButton_Click(object sender, EventArgs e)
		{
			uxSaveQueryButton.Visible = false;
			uxSaveQueryAsButton.Visible = false;
			uxSavePanel.Visible = true;		
			uxQueryNameTextBox.Text = "";
			uxGlobalCheckbox.Checked = false;
			uxQueryNameTextBox.Enabled = true;
		}

		private void uxSaveQueryButton_Click(object sender, EventArgs e)
		{
			uxSaveQueryButton.Visible = false;
			uxSaveQueryAsButton.Visible = false;
			uxSavePanel.Visible = true;
			Guid storedQueryID = new Guid(uxHiddenStoredQueryID.Text);
			BnbStoredQuery sq = BnbStoredQuery.Retrieve(storedQueryID);
			uxQueryNameTextBox.Text = sq.QueryName;
			uxQueryNameTextBox.Enabled = false;
			uxGlobalCheckbox.Checked = sq.SharePublic;
		}

        

        protected void uxSmartGrid_ExportClicked(object sender, BonoboWebControls.DataGrids.BnbSmartGrid.BnbSmartGridExportEventArgs e)
        {
            // add some headerlines
            string queryName = "";
            if (uxViewCombo.Visible)
                queryName = uxViewCombo.SelectedItem.Text;
            else
                queryName = uxQueryName.Text.Replace("<b>", "").Replace("</b>", "");

            e.HeaderLines.Add(String.Format("Query - {0}", queryName));
            e.HeaderLines.Add(String.Format("Exported at {0}", DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss")));
            e.HeaderLines.Add(uxQueryBuilderComposite.SummaryMessage);
        }
	}
}
