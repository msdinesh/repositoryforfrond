<%@ Page language="c#" Codebehind="QueryHistory.aspx.cs" AutoEventWireup="True" Inherits="Frond.Query.QueryHistory" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Query History</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal><LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet"><asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent"><uc1:title id="titleBar" runat="server" imagesource="../Images/query24silver.gif"></uc1:title>
				<p>This page shows your recent Query runs. Click on a row to re-load the Query.
				</p>
				<p><a href="CustomQuery.aspx">Back to Query page ...</a>&nbsp;&nbsp;&nbsp;&nbsp;<A href="ChooseView.aspx">More Queries ...</A></p>
				<P>
					<asp:DataGrid id="uxQueryHistoryGrid2" runat="server" CssClass="datagrid" AutoGenerateColumns="False">
						<HeaderStyle CssClass="datagridheader"></HeaderStyle>
						<Columns>
							<asp:HyperLinkColumn Text="Select" DataNavigateUrlField="appQueryLog_QueryLogID" DataNavigateUrlFormatString="CustomQuery.aspx?QueryLogID={0}"></asp:HyperLinkColumn>
							<asp:BoundColumn DataField="appQueryLog_Timestamp" HeaderText="Time" DataFormatString="{0:dd/MMM/yyyy HH:mm:ss}"></asp:BoundColumn>
							<asp:BoundColumn DataField="appMetaView_ViewUserAlias" HeaderText="View"></asp:BoundColumn>
							<asp:BoundColumn DataField="tblStoredQuery_QueryName" HeaderText="Stored Query"></asp:BoundColumn>
							<asp:BoundColumn DataField="appQueryLog_CriteriaUser" HeaderText="Criteria"></asp:BoundColumn>
							<asp:BoundColumn DataField="appQueryLog_RowsReturned" HeaderText="Rows Returned"></asp:BoundColumn>
							<asp:BoundColumn DataField="appQueryLog_RunSeconds" HeaderText="Run Seconds"></asp:BoundColumn>
						</Columns>
					</asp:DataGrid></P>
				<P>History rows:
					<asp:TextBox id="uxHistoryRowsTextbox" runat="server" Width="31px"></asp:TextBox>
					<asp:Button id="uxRefreshButton" runat="server" Text="Refresh" onclick="uxRefreshButton_Click"></asp:Button></P>
				<P>&nbsp;</P>
			</div>
		</form>
	</body>
</HTML>
