<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="ChooseView.aspx.cs" AutoEventWireup="True" Inherits="Frond.Query.ChooseView" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>ChooseView</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal><LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet"><asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent"><uc1:title id="titleBar" runat="server"  imagesource="../Images/query24silver.gif"></uc1:title>
				<p>This page allows you to choose from a wide range of Queries. Click through the tabs to see more details. See also: <a href="QueryHistory.aspx">Query History</a></p>
				<bnbpagecontrol:bnbtabcontrol id="BnbTabControl1" runat="server" IgnoreWorkareas="true"></bnbpagecontrol:bnbtabcontrol>
				<asp:panel id="StandardTab" runat="server" BnbTabName="Standard Queries" CssClass="tcTab">
					<p>The Standard Queries are the built-in queries that everyone sees. They are also available in the combo box on the main Query page.</p>
					<asp:DataGrid id="uxStandardQueryGrid" runat="server" CssClass="datagrid" Width="100%" AutoGenerateColumns="False">
						<HeaderStyle CssClass="datagridheader"></HeaderStyle>
						<Columns>
							<asp:HyperLinkColumn Text="Select" DataNavigateUrlField="ID" DataNavigateUrlFormatString="CustomQuery.aspx?StandardQueryID={0}"></asp:HyperLinkColumn>
							<asp:BoundColumn DataField="QueryName" HeaderText="Query Name"></asp:BoundColumn>
							<asp:BoundColumn DataField="Description" HeaderText="Description"></asp:BoundColumn>
						</Columns>
					</asp:DataGrid>
				</asp:panel><asp:panel id="UserFindTab" runat="server" BnbTabName="Your Queries" CssClass="tcTab">
					<p>This tab shows any Stored Queries that you have saved.</p>
					<bnbdatagrid:BnbDataGridForView id="uxUserQueryGrid" runat="server" CssClass="datagrid" RedirectPage="CustomQuery.aspx"
						DomainObject="BnbStoredQuery" GuidKey="tblStoredQuery_StoredQueryID" ViewLinksText="Select" NewLinkText="New"
						NewLinkVisible="False" ViewLinksVisible="true" ShowResultsOnNewButtonClick="false" DisplayResultCount="false"
						DisplayNoResultMessageOnly="True" Width="100%" ViewName="vwBonoboStoredQuery" ColumnNames="tblStoredQuery_QueryName, appMetaView_ViewUserAlias, tblStoredQuery_CriteriaUser"
						NoResultMessage="No saved Queries were found"></bnbdatagrid:BnbDataGridForView>
				</asp:panel><asp:panel id="PublicTab" runat="server" BnbTabName="Shared Queries" CssClass="tcTab">
					<p>This tab shows any Stored Queries saved by other users that have been marked as 'Global'.</p>
					<bnbdatagrid:BnbDataGridForView id="uxGlobalQueryGrid" runat="server" CssClass="datagrid" RedirectPage="CustomQuery.aspx"
						DomainObject="BnbStoredQuery" GuidKey="tblStoredQuery_StoredQueryID" ViewLinksText="Select" NewLinkText="New"
						NewLinkVisible="False" ViewLinksVisible="true" ShowResultsOnNewButtonClick="false" DisplayResultCount="false"
						DisplayNoResultMessageOnly="True" Width="100%" ViewName="vwBonoboStoredQuery" ColumnNames="tblStoredQuery_QueryName, Custom_QueryOwner, appMetaView_ViewUserAlias, tblStoredQuery_CriteriaUser"
						NoResultMessage="No global queries were found"></bnbdatagrid:BnbDataGridForView>
				</asp:panel><asp:panel id="AdvancedTab" runat="server" BnbTabName="Advanced" CssClass="tcTab">
					<p>This tab gives a list of all Query and Find views available on the system. When selecting these views, you will have to
					build up the criteria on the Query page from scratch.</p>
					<asp:DataGrid id="uxAllViewsGrid" runat="server" CssClass="datagrid" Width="100%" AutoGenerateColumns="False">
						<HeaderStyle CssClass="datagridheader"></HeaderStyle>
						<Columns>
							<asp:HyperLinkColumn Text="Select" DataNavigateUrlField="ViewName" DataNavigateUrlFormatString="CustomQuery.aspx?ViewName={0}"></asp:HyperLinkColumn>
							<asp:BoundColumn DataField="ViewUserAlias" HeaderText="View Name"></asp:BoundColumn>
							<asp:BoundColumn DataField="ViewName" HeaderText="View (sql name)"></asp:BoundColumn>
						</Columns>
					</asp:DataGrid>
				</asp:panel></div>
		</form>
	</body>
</HTML>
