using System;
using System.Text;
using System.Collections.Specialized;
using System.Xml;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using BonoboDomainObjects;
using BonoboEngine.Query;
using BonoboEngine;

namespace Frond.Utilities
{
    /// <summary>
    /// This class is used to parse the Xml data specific to Job, Placement
    /// </summary>
    public class PlacementParser : XmlDataParser
    {

        #region " private declarations "

        private Guid m_employer = BnbDomainObject.NullGuid;
        private string m_roleName = BnbDomainObject.NullString;
        private string m_roleAbbrev = BnbDomainObject.NullString;
        private string m_roleComments = BnbDomainObject.NullString;

        private Collection<int> m_roleSkills = null;
        private Collection<int> m_positionThemes = null;
        private Collection<int> m_positionObjectives = null;
        private Collection<int> m_positionVolunteerRoles = null;
        private Collection<int> m_roleSkillOrders = null;
        private Collection<int> m_positionThemeOrders = null;
        private Collection<int> m_positionObjectiveOrders = null;
        private Collection<int> m_positionVolunteerRoleOrders = null;

        private DateTime m_summarySentDate = BnbDomainObject.NullDateTime;
        private DateTime m_summaryReceivedDate = BnbDomainObject.NullDateTime;
        private string m_summaryFilename = BnbDomainObject.NullString;
        private DateTime m_descriptionSentDate = BnbDomainObject.NullDateTime;
        private DateTime m_descriptionReceivedDate = BnbDomainObject.NullDateTime;
        private string m_descriptionFilename = BnbDomainObject.NullString;
        private DateTime m_firmDate = BnbDomainObject.NullDateTime;
        private DateTime m_tentativeDate = BnbDomainObject.NullDateTime;
        private int m_tentativeReason = BnbDomainObject.NullInt;
        private DateTime m_earliestStartDate = BnbDomainObject.NullDateTime;
        private string m_priority = BnbDomainObject.NullString;
        private bool m_overseasLink = false;
        private float m_duration = BnbDomainObject.NullInt;
        private Guid m_programmeOfficer = BnbDomainObject.NullGuid;
        private bool m_replacement = false;
        private string m_positionComments = BnbDomainObject.NullString;
        private int m_fillStatus = BnbDomainObject.NullInt;
        private DateTime m_cancelledDate = BnbDomainObject.NullDateTime;
        private int m_cancelledReason = BnbDomainObject.NullInt;
        private string m_pOSummaryFilename = BnbDomainObject.NullString;
        private string m_pODescriptionFilename = BnbDomainObject.NullString;
        private int m_vSOType = BnbDomainObject.NullInt;
        private int m_programmeArea = BnbDomainObject.NullInt;
        private DateTime m_recruitmentStartDate = BnbDomainObject.NullDateTime;
        private DateTime m_recruitmentEndDate = BnbDomainObject.NullDateTime;

        private HybridDictionary m_xmlMappings = null;
        private XmlNode m_placementRow = null;
        private DataParserUtil m_parserUtil;
        private string searchKey = string.Empty;
        private string nodeVal = string.Empty;
        private const string vwEmployer = "vlkuEmployer";
        private const string vwXmlMapping = "vwXmlMappingTemplate";
        private const string vwRoleSkill = "vlkuSkill";
        private const string vwTheme = "vlkuTheme";
        private const string vwVolunteerRole = "vlkuVolunteerRole";
        private const string vwProgramArea = "vlkuProgramAreaForImport";
        private const string vwVSOType = "vlkuVSOType";
        private const string vwPositionFIllStatus = "vlkuPositionFillStatus";
        private const string vwProgramOfficer = "vlkuProgramOfficer";
        private const string vwTentativeReason = "vlkuTentativeReasonForImport";
        private const string vwMonitorObjective = "vlkuMonitorObjective";
        private const string vwPositionCancelReason = "vlkuPositionCancelReason";

        private const string colXmlMapping = "tblXmlMappingTemplateItem_XmlMappingTemplateID";
        private const string colEmployer = "Description";
        private const string colRoleSkill = "Description";
        private const string colTheme = "Description";
        private const string colVolunteerRole = "Description";
        private const string colProgramArea = "Description";
        private const string colVSOType = "Description";
        private const string colPositionFIllStatus = "Description";
        private const string colProgramOfficer = "Description";
        private const string colTentativeReason = "Description";
        private const string colMonitorObjective = "Description";
        private const string colPositionCancelReason = "Description";

        #endregion

        #region " Constructor "
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="xmlMappingID"></param>
        public PlacementParser(int xmlMappingID)
        {
            m_parserUtil = new DataParserUtil();
            string mappingID = xmlMappingID.ToString();
            m_xmlMappings = m_parserUtil.GetXmlMappings(vwXmlMapping, colXmlMapping, mappingID);

            // Initialize collections
            m_roleSkills = new Collection<int>();
            m_positionThemes = new Collection<int>();
            m_positionObjectives = new Collection<int>();
            m_positionVolunteerRoles = new Collection<int>();
            m_roleSkillOrders = new Collection<int>();
            m_positionThemeOrders = new Collection<int>();
            m_positionObjectiveOrders = new Collection<int>();
            m_positionVolunteerRoleOrders = new Collection<int>();

        }
        #endregion

        #region " RegisterPlacementRow "
        /// <summary>
        /// To set the placement row for parsing
        /// </summary>
        /// <param name="placementRow"></param>
        public void RegisterPlacementRow(XmlNode placementRow)
        {
            m_placementRow = placementRow;
        }

        #endregion

        #region " GetPlacementRows "
        /// <summary>
        /// To get the list of placement rows
        /// </summary>
        /// <param name="xmlSchemaFile"></param>
        /// <param name="xmlFile"></param>
        /// <returns></returns>
        public XmlNodeList GetPlacementRows(string xmlSchemaFile, string xmlFile)
        {
            return base.GetListOfRows(xmlSchemaFile, xmlFile);
        }
        #endregion

        #region " Employer "

        public Guid Employer
        {
            get
            {

                if (m_xmlMappings.Contains(BnbConst.XmlMap_Employer))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_Employer].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_employer = m_parserUtil.GetRowIDAsGuid(vwEmployer, colEmployer, nodeVal, searchKey);
                }

                return m_employer;
            }
        }
        #endregion

        #region " RoleName "

        public string RoleName
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_RoleName))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_RoleName].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_roleName = nodeVal;
                }

                return m_roleName;
            }
        }
        #endregion

        #region " RoleAbbrev "

        public string RoleAbbrev
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_RoleAbbrev))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_RoleAbbrev].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_roleAbbrev = nodeVal;
                }

                return m_roleAbbrev;
            }
        }
        #endregion

        #region " RoleComments "

        public string RoleComments
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_RoleComments))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_RoleComments].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_roleComments = nodeVal;
                }

                return m_roleComments;
            }
        }

        #endregion

        #region " RoleSkills "

        public Collection<int> RoleSkills
        {
            get
            {
                Collection<string> skillsForMapping;
                if (m_xmlMappings.Contains(BnbConst.XmlMap_RoleSkill))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_RoleSkill].ToString();
                    skillsForMapping = base.GetListOfXmlNodeValues(m_placementRow, searchKey);
                    m_roleSkills = m_parserUtil.GetIdMappedCollection(skillsForMapping, vwRoleSkill, colRoleSkill, searchKey);
                }

                return m_roleSkills;
            }
        }

        #endregion

        #region " RoleSkillOrders "

        public Collection<int> RoleSkillOrders
        {
            get
            {
                Collection<string> skillOrdersForMapping;
                if (m_xmlMappings.Contains(BnbConst.XmlMap_RoleSkillOrder))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_RoleSkillOrder].ToString();
                    skillOrdersForMapping = base.GetListOfXmlNodeValues(m_placementRow, searchKey);
                    m_roleSkillOrders = m_parserUtil.GetNullMappedIntegerCollection(skillOrdersForMapping, searchKey);
                }

                return m_roleSkillOrders;
            }
        }
        #endregion

        #region " SummarySentDate "

        public DateTime SummarySentDate
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_SummarySentDate))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_SummarySentDate].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_summarySentDate = m_parserUtil.DateTimeCoversion(nodeVal, searchKey);

                }

                return m_summarySentDate;
            }
        }
        #endregion

        #region " SummaryReceivedDate "

        public DateTime SummaryReceivedDate
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_SummaryReceivedDate))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_SummaryReceivedDate].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_summaryReceivedDate = m_parserUtil.DateTimeCoversion(nodeVal, searchKey);
                }

                return m_summaryReceivedDate;
            }
        }
        #endregion

        #region " SummaryFilename "

        public string SummaryFilename
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_SummaryFilename))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_SummaryFilename].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_summaryFilename = nodeVal;
                }

                return m_summaryFilename;
            }
        }
        #endregion

        #region " DescriptionSentDate "

        public DateTime DescriptionSentDate
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_DescriptionSentDate))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_DescriptionSentDate].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_descriptionSentDate = m_parserUtil.DateTimeCoversion(nodeVal, searchKey);
                }

                return m_descriptionSentDate;
            }
        }
        #endregion

        #region " DescriptionReceivedDate "


        public DateTime DescriptionReceivedDate
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_DescriptionReceivedDate))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_DescriptionReceivedDate].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_descriptionReceivedDate = m_parserUtil.DateTimeCoversion(nodeVal, searchKey);
                }

                return m_descriptionReceivedDate;
            }
        }
        #endregion

        #region " DescriptionFilename "

        public string DescriptionFilename
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_DescriptionFilename))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_DescriptionFilename].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_descriptionFilename = nodeVal;
                }

                return m_descriptionFilename;
            }
        }
        #endregion

        #region " FirmDate "


        public DateTime FirmDate
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_FirmDate))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_FirmDate].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_firmDate = m_parserUtil.DateTimeCoversion(nodeVal, searchKey);
                }

                return m_firmDate;
            }
        }
        #endregion

        #region " TentativeDate "

        public DateTime TentativeDate
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_TentativeDate))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_TentativeDate].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_tentativeDate = m_parserUtil.DateTimeCoversion(nodeVal, searchKey);
                }

                return m_tentativeDate;
            }
        }
        #endregion

        #region " TentativeReason "

        public int TentativeReason
        {
            get
            {

                if (m_xmlMappings.Contains(BnbConst.XmlMap_TentativeReason))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_TentativeReason].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_tentativeReason = m_parserUtil.GetRowIDAsInt(vwTentativeReason, colTentativeReason, nodeVal, searchKey);
                }

                return m_tentativeReason;
            }
        }
        #endregion

        #region " EarliestStartDate "


        public DateTime EarliestStartDate
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_EarliestStartDate))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_EarliestStartDate].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_earliestStartDate = m_parserUtil.DateTimeCoversion(nodeVal, searchKey);
                }

                return m_earliestStartDate;
            }
        }
        #endregion

        #region " Priority "

        public string Priority
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_Priority))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_Priority].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_priority = nodeVal;
                }

                return m_priority;
            }
        }
        #endregion

        #region " OverseasLink "

        public bool OverseasLink
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_OverseasLink))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_OverseasLink].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_overseasLink = m_parserUtil.BooleanCoversion(m_parserUtil.GetZeroOneMappedValue(nodeVal), searchKey);
                }

                return m_overseasLink;
            }
        }
        #endregion

        #region " Duration "

        public float Duration
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_Duration))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_Duration].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_duration = m_parserUtil.FloatCoversion(nodeVal, searchKey);
                }

                return m_duration;
            }
        }
        #endregion

        #region " ProgrammeOfficer "


        public Guid ProgrammeOfficer
        {
            get
            {

                if (m_xmlMappings.Contains(BnbConst.XmlMap_ProgrammeOfficer))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_ProgrammeOfficer].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_programmeOfficer = m_parserUtil.GetRowIDAsGuid(vwProgramOfficer, colProgramOfficer, nodeVal, searchKey);
                }

                return m_programmeOfficer;

            }

        }
        #endregion

        #region " Replacement "

        public bool Replacement
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_Replacement))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_Replacement].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_replacement = m_parserUtil.BooleanCoversion(m_parserUtil.GetZeroOneMappedValue(nodeVal), searchKey);
                }

                return m_replacement;
            }
        }
        #endregion

        #region " PositionComments "

        public string PositionComments
        {
            get
            {

                if (m_xmlMappings.Contains(BnbConst.XmlMap_PositionComments))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_PositionComments].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_positionComments = nodeVal;
                }

                return m_positionComments;
            }
        }
        #endregion

        #region " FillStatus "

        public int FillStatus
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_FillStatus))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_FillStatus].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_fillStatus = m_parserUtil.GetRowIDAsInt(vwPositionFIllStatus, colPositionFIllStatus, nodeVal, searchKey);
                }

                return m_fillStatus;

            }

        }
        #endregion

        #region " CancelledDate "

        public DateTime CancelledDate
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_CancelledDate))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_CancelledDate].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_cancelledDate = m_parserUtil.DateTimeCoversion(nodeVal, searchKey);
                }

                return m_cancelledDate;
            }
        }
        #endregion

        #region " CancelledReason "

        public int CancelledReason
        {
            get
            {
                int mappedCancelReason = BnbDomainObject.NullInt;
                if (m_xmlMappings.Contains(BnbConst.XmlMap_CancelledReason))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_CancelledReason].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_cancelledReason = m_parserUtil.GetRowIDAsInt(vwPositionCancelReason, colPositionCancelReason, nodeVal, searchKey);
                }

                return m_cancelledReason;
            }

        }
        #endregion

        #region " POSummaryFilename "

        public string POSummaryFilename
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_POSummaryFilename))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_POSummaryFilename].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_pOSummaryFilename = nodeVal;
                }

                return m_pOSummaryFilename;
            }
        }
        #endregion

        #region " PODescriptionFilename "

        public string PODescriptionFilename
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_PODescriptionFilename))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_PODescriptionFilename].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_pODescriptionFilename = nodeVal;
                }

                return m_pODescriptionFilename;
            }
        }
        #endregion

        #region " VSOType "

        public int VSOType
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_VSOType))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_VSOType].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_vSOType = m_parserUtil.GetRowIDAsInt(vwVSOType, colVSOType, nodeVal, searchKey);
                }

                return m_vSOType;

            }
        }
        #endregion

        #region " ProgrammeArea "

        public int ProgrammeArea
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_ProgrammeArea))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_ProgrammeArea].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_programmeArea = m_parserUtil.GetRowIDAsInt(vwProgramArea, colProgramArea, nodeVal, searchKey);
                }

                return m_programmeArea;
            }

        }
        #endregion

        #region " RecruitmentStartDate "

        public DateTime RecruitmentStartDate
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_Replacement))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_RecruitmentStartDate].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_recruitmentStartDate = m_parserUtil.DateTimeCoversion(nodeVal, searchKey);
                }

                return m_recruitmentStartDate;
            }
        }
        #endregion

        #region " RecruitmentEndDate "

        public DateTime RecruitmentEndDate
        {
            get
            {
                if (m_xmlMappings.Contains(BnbConst.XmlMap_RecruitmentEndDate))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_RecruitmentEndDate].ToString();
                    nodeVal = base.GetSingleXmlNodeValue(m_placementRow, searchKey);
                    if (!(nodeVal.Trim() == string.Empty))
                        m_recruitmentEndDate = m_parserUtil.DateTimeCoversion(nodeVal, searchKey);
                }

                return m_recruitmentEndDate;
            }
        }
        #endregion

        #region " PositionThemes "

        public Collection<int> PositionThemes
        {
            get
            {
                Collection<string> themesForMapping;
                if (m_xmlMappings.Contains(BnbConst.XmlMap_Theme))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_Theme].ToString();
                    themesForMapping = base.GetListOfXmlNodeValues(m_placementRow, searchKey);
                    m_positionThemes = m_parserUtil.GetIdMappedCollection(themesForMapping, vwTheme, colTheme, searchKey);
                }

                return m_positionThemes;
            }
        }
        #endregion

        #region " PositionThemeOrders "

        public Collection<int> PositionThemeOrders
        {
            get
            {
                Collection<string> themeOrdersForMapping;
                if (m_xmlMappings.Contains(BnbConst.XmlMap_ThemeOrder))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_ThemeOrder].ToString();
                    themeOrdersForMapping = base.GetListOfXmlNodeValues(m_placementRow, searchKey);
                    m_positionThemeOrders = m_parserUtil.GetNullMappedIntegerCollection(themeOrdersForMapping, searchKey);
                }

                return m_positionThemeOrders;
            }
        }
        #endregion

        #region " PositionObjectiveOrders "

        public Collection<int> PositionObjectiveOrders
        {
            get
            {
                Collection<string> objectiveOrdersForMapping;
                if (m_xmlMappings.Contains(BnbConst.XmlMap_MonitorObjectiveOrder))
                {
                    searchKey = string.Empty;
                    searchKey = m_xmlMappings[BnbConst.XmlMap_MonitorObjectiveOrder].ToString();
                    objectiveOrdersForMapping = base.GetListOfXmlNodeValues(m_placementRow, searchKey);
                    m_positionObjectiveOrders = m_parserUtil.GetNullMappedIntegerCollection(objectiveOrdersForMapping, searchKey);
                }

                return m_positionObjectiveOrders;
            }
        }
        #endregion

        #region " PositionObjectives"

        public Collection<int> PositionObjectives
        {
            get
            {
                Collection<string> objectivesForMapping;
                if (m_xmlMappings.Contains(BnbConst.XmlMap_MonitorObjective))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_MonitorObjective].ToString();
                    objectivesForMapping = base.GetListOfXmlNodeValues(m_placementRow, searchKey);
                    m_positionObjectives = m_parserUtil.GetIdMappedCollection(objectivesForMapping, vwMonitorObjective, colMonitorObjective, searchKey);
                }

                return m_positionObjectives;
            }
        }
        #endregion

        #region " PositionVolunteerRoles "

        public Collection<int> PositionVolunteerRoles
        {
            get
            {
                Collection<string> volunteerRolesForMapping;
                if (m_xmlMappings.Contains(BnbConst.XmlMap_VolunteerRole))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_VolunteerRole].ToString();
                    volunteerRolesForMapping = base.GetListOfXmlNodeValues(m_placementRow, searchKey);
                    m_positionVolunteerRoles = m_parserUtil.GetIdMappedCollection(volunteerRolesForMapping, vwVolunteerRole, colVolunteerRole, searchKey);
                }

                return m_positionVolunteerRoles;
            }
        }
        #endregion

        #region " PositionVolunteerRoleOrders "

        public Collection<int> PositionVolunteerRoleOrders
        {
            get
            {
                Collection<string> volunteerRoleOrdersForMapping;
                if (m_xmlMappings.Contains(BnbConst.XmlMap_VolunteerRoleOrder))
                {
                    searchKey = m_xmlMappings[BnbConst.XmlMap_VolunteerRoleOrder].ToString();
                    volunteerRoleOrdersForMapping = base.GetListOfXmlNodeValues(m_placementRow, searchKey);
                    m_positionVolunteerRoleOrders = m_parserUtil.GetNullMappedIntegerCollection(volunteerRoleOrdersForMapping, searchKey);
                }

                return m_positionVolunteerRoleOrders;
            }
        }
        #endregion


    }
}
