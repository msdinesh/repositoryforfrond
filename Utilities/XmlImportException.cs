using System;

namespace Frond.Utilities
{
    /// <summary>
    /// This class is used to handle exceptions occured during Xml Import
    /// </summary>
    public class XmlImportException:ApplicationException
    {
        /// <summary>
		/// Standard Exception constructor
		/// </summary>
		public XmlImportException()
		{
		}

		/// <summary>
		/// Standard Exception constructor
		/// </summary>
		public XmlImportException(string message)
			: base(message)
		{
		}

		/// <summary>
		/// Standard Exception constructor
		/// </summary>
        public XmlImportException(string message, Exception inner)
			: base(message, inner)
		{
		}
    }
}
