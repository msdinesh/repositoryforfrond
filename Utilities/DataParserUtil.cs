using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using BonoboEngine;
using BonoboEngine.Query;

namespace Frond.Utilities
{
    /// <summary>
    /// Utilities used for xml data parsing
    /// </summary>
    public class DataParserUtil
    {

        #region " Constructor "
        /// <summary>
        /// Constructor
        /// </summary>
        public DataParserUtil()
        {
        }

        #endregion

        #region " Private declarations "

        private const string Mapping_TableKey = "tblXmlMappingTemplateItem_TableAttribute";
        private const string Mapping_XmlKey = "tblXmlMappingTemplateItem_XmlAttribute";
        private const string ResultSet = "Results";
        private const string IdentityColumn = "ID";
        private const string ExcludeKey = "Exclude";
        private const string ExcludeValue = "0";

        #endregion

        #region " GetRowIDAsInt "

        /// <summary>
        /// To get row ID as Integer
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="columnName"></param>
        /// <param name="searchString"></param>
        /// <param name="xmlTag"></param>
        /// <returns></returns>
        public int GetRowIDAsInt(string viewName, string columnName, string searchString, string xmlTag)
        {

            return (int)GetRowID(viewName, columnName, searchString, xmlTag);

        }

        #endregion

        #region " GetRowIDAsGuid "

        /// <summary>
        /// To get row ID as Guid
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="columnName"></param>
        /// <param name="searchString"></param>
        /// <param name="xmlTag"></param>
        /// <returns></returns>
        public Guid GetRowIDAsGuid(string viewName, string columnName, string searchString, string xmlTag)
        {

            return (Guid)GetRowID(viewName, columnName, searchString, xmlTag);

        }

        #endregion

        #region " GetRowID "
        /// <summary>
        /// To get row ID
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="columnName"></param>
        /// <param name="searchString"></param>
        /// <param name="xmlTag"></param>
        /// <returns></returns>
        public object GetRowID(string viewName, string columnName, string searchString, string xmlTag)
        {

            BnbTextCondition exclude = new BnbTextCondition(ExcludeKey, ExcludeValue, BnbTextCompareOptions.ExactMatch);
            BnbTextCondition description = new BnbTextCondition(columnName, searchString, BnbTextCompareOptions.ExactMatch);

            BnbCriteria criteria = new BnbCriteria();
            criteria.QueryElements.Add(description);
            criteria.QueryElements.Add(exclude);
            BnbQuery query = new BnbQuery(viewName, criteria);
            DataTable dt = query.Execute().Tables[ResultSet];

            if (dt.Rows.Count <= 0)
            {
                throw new XmlImportException(xmlTag + " is Invalid");
            }
            return dt.Rows[0][IdentityColumn];
        }
        #endregion

        #region " GetIdMappedCollection "

        /// <summary>
        /// To get mapped Id collection
        /// </summary>
        /// <param name="namedCollection"></param>
        /// <param name="viewName"></param>
        /// <param name="columnName"></param>
        /// <param name="xmlTag"></param>
        /// <returns></returns>
        public Collection<int> GetIdMappedCollection(Collection<string> namedCollection, string viewName,
                                             string columnName, string xmlTag)
        {
            Collection<int> resultSet = new Collection<int>();
            int mappedValue = 0;

            foreach (string key in namedCollection)
            {
                if (key.Trim() == string.Empty)
                    resultSet.Add(BnbDomainObject.NullInt);
                else
                {
                    mappedValue = GetRowIDAsInt(viewName, columnName, key, xmlTag);
                    resultSet.Add(mappedValue);
                 
                }
            }
            return resultSet;
        }

        #endregion

        #region " GetIdMappedCollection "

        /// <summary>
        /// To get Integer Null value mapped collection
        /// </summary>
        /// <param name="namedCollection"></param>
        /// <param name="viewName"></param>
        /// <param name="columnName"></param>
        /// <param name="xmlTag"></param>
        /// <returns></returns>
        public Collection<int> GetNullMappedIntegerCollection(Collection<string> collection, string xmlTag)
        {
            Collection<int> resultSet = new Collection<int>();

            foreach (string val in collection)
            {
                if (val.Trim() == string.Empty)
                    resultSet.Add(BnbDomainObject.NullInt);
                else
                {
                    try { resultSet.Add(Convert.ToInt32(val)); }
                    catch (Exception ex) { throw new XmlImportException(xmlTag + " is invalid");}
                }
            }
            return resultSet;
        }

        #endregion

        #region " GetXmlMappings "
        /// <summary>
        /// To get Xml Mappings
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="columnName"></param>
        /// <param name="searchString"></param>
        /// <returns></returns>
        public HybridDictionary GetXmlMappings(string viewName, string columnName, string searchString)
        {

            BnbQuery query = new BnbQuery(viewName, columnName, searchString);
            DataTable mappingTable = query.Execute().Tables[ResultSet];
            if (mappingTable.Rows.Count <= 0)
            {
                throw new XmlImportException("Template mapping is empty. Please fill the template mappings");
            }
            HybridDictionary xmlMappings = new HybridDictionary();

            foreach (DataRow mappingRow in mappingTable.Rows)
            {
                string tableKey = mappingRow[Mapping_TableKey].ToString().Trim();
                string xmlKey = mappingRow[Mapping_XmlKey].ToString().Trim();
                xmlMappings.Add(tableKey, xmlKey);

            }
            return xmlMappings;
        }

        #endregion

        #region " GetZeroOneMappedValue "
        /// <summary>
        /// To Map "0" to "False" and "1" to "True" (if the xml has 0, 1 for boolean)
        /// </summary>
        /// <param name="inputVal"></param>
        /// <returns></returns>
        public string GetZeroOneMappedValue(string inputVal)
        {
            if (inputVal.Trim() == "0")
                return "False";
            else if (inputVal.Trim() == "1")
                return "True";
            else
                return inputVal;
        }

        #endregion

        #region " DateTimeCoversion "

        /// <summary>
        /// To convert the value to Date Time
        /// </summary>
        /// <param name="inputVal"></param>
        /// <param name="xmlTag"></param>
        /// <returns></returns>
        public DateTime DateTimeCoversion(string inputVal, string xmlTag)
        {
            try
            {
                return Convert.ToDateTime(inputVal);
            }
            catch (InvalidCastException castEx)
            {
                     throw new XmlImportException(xmlTag + " is Invalid." );
            }


        }

        #endregion

        #region " IntegerCoversion "

        /// <summary>
        /// To convert the value to Integer
        /// </summary>
        /// <param name="inputVal"></param>
        /// <param name="xmlTag"></param>
        /// <returns></returns>
        public int IntegerCoversion(string inputVal, string xmlTag)
        {
            try
            {
                return Convert.ToInt32(inputVal);
            }
            catch (Exception castEx)
            {
                throw new XmlImportException(xmlTag + " is Invalid. ");
            }


        }

#endregion

        #region " FloatCoversion "

        /// <summary>
        /// To convert the value to float
        /// </summary>
        /// <param name="inputVal"></param>
        /// <param name="xmlTag"></param>
        /// <returns></returns>
        public float FloatCoversion(string inputVal, string xmlTag)
        {
            try
            {
                return Convert.ToSingle(inputVal);
            }
            catch (InvalidCastException castEx)
            {
                throw new XmlImportException(xmlTag + " is Invalid. ");
            }


        }

#endregion

        #region " BooleanCoversion "

        /// <summary>
        /// To convert the value to Boolean
        /// </summary>
        /// <param name="inputVal"></param>
        /// <param name="xmlTag"></param>
        /// <returns></returns>
        public bool BooleanCoversion(string inputVal, string xmlTag)
        {
            try
            {
                return Convert.ToBoolean(inputVal);
            }
            catch (InvalidCastException castEx)
            {
                throw new XmlImportException(xmlTag + " is Invalid. ");
            }


        }

        #endregion
    }
}
