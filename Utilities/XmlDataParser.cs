using System;
using System.Text;
using System.IO;
using System.Collections.Specialized;
using System.Xml;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Frond.Utilities
{
    /// <summary>
    /// Class to parse the data in the xml
    /// </summary>
    public class XmlDataParser
    {

        #region " Constructor "

        /// <summary>
        /// Constructor
        /// </summary>
        public XmlDataParser()
        {
        }

        #endregion

        #region " Private Declarations "

        private const string m_rootString = "<XmlRoot></XmlRoot>";

        #endregion

        #region " GetListOfRows "

        /// <summary>
        /// To Split xml rows
        /// </summary>
        /// <param name="xmlSchema"></param>
        /// <param name="xml"></param>
        /// <returns></returns>
        public XmlNodeList GetListOfRows(string xmlSchema, string xml)
        {
            XmlDataDocument doc = new XmlDataDocument();
            try
            {
                doc.DataSet.ReadXmlSchema(xmlSchema);
            }
            catch (IOException ex) { throw new XmlImportException("Couldn't able to read the Xml schema"); }
            catch (Exception ex) { throw new XmlImportException("Provided Xml schema is invalid"); }

            try
            {
                doc.DataSet.ReadXml(xml);
            }
            catch (IOException ex) { throw new XmlImportException("Couldn't able to read the Xml file"); }
            catch (Exception ex) { throw new XmlImportException("Xml file doesn't match with the schema or Provided Xml file is invalid."); }

            XmlElement rootElement = doc.DocumentElement;

            if (!(rootElement == null))
            {
                if (rootElement.HasChildNodes)
                    return rootElement.ChildNodes;
                else
                    return null;
            }
            else
                return null;

        }

        #endregion

        #region " GetXmlValue "

        /// <summary>
        /// To get the Xml node value 
        /// </summary>
        /// <param name="xmlNodeToParse"></param>
        /// <param name="searchKey"></param>
        /// <returns></returns>
        public XmlNodeList GetXmlValue(XmlNode xmlNodeToParse, string searchKey)
        {
            XmlDocument xmlDocumentObject = new XmlDocument();
            xmlDocumentObject.LoadXml(m_rootString);

            XmlNode nodeToParse = xmlDocumentObject.ImportNode(xmlNodeToParse, true);
            xmlDocumentObject.DocumentElement.AppendChild(nodeToParse);

            XmlNodeList nodeList = xmlDocumentObject.GetElementsByTagName(searchKey);

            return nodeList;
        }

        #endregion

        #region " GetListOfXmlNodeValues "
        /// <summary>
        /// To get the list of Xml node values
        /// </summary>
        /// <param name="xmlNodeToParse"></param>
        /// <param name="searchKey"></param>
        /// <returns></returns>
        public Collection<string> GetListOfXmlNodeValues(XmlNode xmlNodeToParse, String searchKey)
        {
            Collection<string> nodeValueCollection = new Collection<string>();
            XmlNodeList xmlNodeList = GetXmlValue(xmlNodeToParse, searchKey);

            if ((xmlNodeList.Count > 0))
            {
                foreach (XmlNode node in xmlNodeList)
                {

                    nodeValueCollection.Add(node.ChildNodes[0].Value);
                }
            }
            return nodeValueCollection;
        }

        #endregion

        #region " GetSingleXmlNodeValue "

        /// <summary>
        /// To get single value from the given Xml node
        /// </summary>
        /// <param name="xmlNodeToParse"></param>
        /// <param name="searchKey"></param>
        /// <returns></returns>
        public string GetSingleXmlNodeValue(XmlNode xmlNodeToParse, String searchKey)
        {
            string nodeValue = string.Empty;
            XmlNodeList nodeList = GetXmlValue(xmlNodeToParse, searchKey);
            if ((nodeList.Count > 0))
            {
                nodeValue = nodeList[0].ChildNodes[0].Value;
            }
            return nodeValue;
        }

        #endregion

    }
}
