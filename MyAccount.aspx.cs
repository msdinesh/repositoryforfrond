using System;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Reflection;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;


namespace Frond
{
	/// <summary>
	/// Summary description for MyAccount.
	/// </summary>
	public partial class MyAccount : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Panel ConfigInfoPanel;
		protected System.Web.UI.WebControls.Label lblDatabaseServer;
		protected System.Web.UI.WebControls.Label lblDatabase;
		protected System.Web.UI.WebControls.Label lblAssemblyVersion;
		protected System.Web.UI.WebControls.Label lblBonoboEngine;
		protected System.Web.UI.WebControls.Label lblBonoboDomainObjects;
		protected System.Web.UI.WebControls.Label lblBonoboWebControls;
		protected System.Web.UI.WebControls.Label lblHostIP;
		protected System.Web.UI.WebControls.Label lblVSOOffice;
		protected System.Web.UI.WebControls.Label lblSessionAllowed;
		protected System.Web.UI.HtmlControls.HtmlTable tblVersions;
		protected UserControls.Title titleBar;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.ShowCurrentUserInfo();
                this.ShowUserOptions();
			}
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
		}

        private void ShowUserOptions()
        {
            BnbAdminUserOptions userOptions = BnbAdminUserOptions.RetrieveByUserID(BnbEngine.SessionManager.GetSessionInfo().UserID);

            uxShowAllTabsVertCheckBox.Checked = userOptions.ShowAllTabsVertically;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void ShowCurrentUserInfo()
		{
			BnbSessionInfo sesh = BnbEngine.SessionManager.GetSessionInfo();
			uxFullName.Text = sesh.FullName;
			uxLoginName.Text = sesh.LoginName;
			uxCountry.SelectedValue = sesh.DefaultCountryID;
			uxDepartment.SelectedValue = sesh.DepartmentID;
			uxProgrammeOffice.SelectedValue = sesh.DefaultProgrammeOfficeID;
			uxRecruitmentBase.SelectedValue = sesh.DefaultRecruitmentBaseID;
			uxWebStyle.SelectedValue = sesh.WebStyleID;
			uxUserEmail.Text = sesh.EmailAddress;

			// also put user in title
			titleBar.TitleText = "My Account - " + sesh.FullName;

			/*
			StringBuilder sb = new StringBuilder();
			foreach(string sLoop in permissionArray)
				sb.AppendFormat("{0}<br/>",sLoop);
			uxPermissions.Text = sb.ToString();*/
			string[] waArray = sesh.GetWorkareaList();
			StringBuilder sb = new StringBuilder();
			foreach(string sLoop in waArray)
				sb.AppendFormat("{0}<br/>",sLoop);
			uxWorkareas.Text = sb.ToString();

			int[] permissionIDArray = sesh.GetPermissionIDList();
			// build a nice datatable of permissions, with types
			DataTable permTable = new DataTable();
			permTable.Columns.Add("PermissionID", typeof(int));
			permTable.Columns.Add("Permission", typeof(string));
			permTable.Columns.Add("PermissionType", typeof(string));
			BnbLookupDataTable permLookup = BnbEngine.LookupManager.GetLookup("vlkuBonoboPermission");
			// transfer the permissions into the datatable
			foreach(int permID in permissionIDArray)
			{
				DataRow addPermRow = permTable.NewRow();
				addPermRow["PermissionID"] = permID;
				BnbLookupRow permInfo = permLookup.FindByID(permID);
				if (permInfo != null)
				{
					addPermRow["Permission"] = permInfo["Permission"];
					addPermRow["PermissionType"] = permInfo["PermissionType"];
				}
				permTable.Rows.Add(addPermRow);
			}
			// sort the results
			permTable.DefaultView.Sort = "PermissionType, Permission";
			uxPermissionGrid.DataSource = permTable.DefaultView;
			uxPermissionGrid.DataBind();
			
			

		}

        protected void uxSaveOptions_Click(object sender, EventArgs e)
        {
            this.StoreUserOptions();
        }

        private void StoreUserOptions()
        {
            BnbAdminUserOptions userOptions = BnbAdminUserOptions.RetrieveByUserID(BnbEngine.SessionManager.GetSessionInfo().UserID);
            BnbEditManager em = new BnbEditManager();
            userOptions.RegisterForEdit(em);
            userOptions.ShowAllTabsVertically = uxShowAllTabsVertCheckBox.Checked;
            
            em.SaveChanges();
            this.Session["ShowAllTabsVertically"] = userOptions.ShowAllTabsVertically;
            this.ShowUserOptions();

            
        }


	}
}
