<%@ Page language="c#" Codebehind="Logout.aspx.cs" AutoEventWireup="True" Inherits="Frond.Logout" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>SFLogout</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK rel="stylesheet" type="text/css" href="Css/FrondDefault.css">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
						<uc1:Title id="titleBar" runat="server" imagesource="Images/logout24silver.gif"></uc1:Title>
						<P>
							<asp:HyperLink id="hlLogin" runat="server" NavigateUrl="Menu.aspx">Login again</asp:HyperLink></P>
						<P>&nbsp;</P>
			</div>
		</form>
	</body>
</HTML>
