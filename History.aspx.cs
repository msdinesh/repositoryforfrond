using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboWebControls;
using BonoboDomainObjects;

namespace Frond
{
	/// <summary>
	/// Summary description for SFHistory.
	/// </summary>
	public partial class History : System.Web.UI.Page
	{
		
		protected UserControls.Title titleBar;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// do the stylesheet
			BnbWorkareaManager.SetPageStyleOfUser(this);


			// Put user code to initialize the page here
			if (!this.IsPostBack)
			{
				titleBar.TitleText = "Screen History";
				this.InitControls();
				this.ShowHistory();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		public int HistoryRows
		{
			get
			{
				if (BnbRuleUtils.IsNumericInt(textHistoryRows.Text))
				{
					if (int.Parse(textHistoryRows.Text) < 1000)
						return int.Parse(textHistoryRows.Text);
					else
						return 1000;
				}
				else
					return 10;
			}
		}

		private void ShowHistory()
		{
			string appName = BnbWebFormManager.ApplicationName();
			DataTable history = BnbEngine.SessionManager.GetPageHistory(appName, this.HistoryRows);
			
			// Remove table characters from "Screen/Record" column
			foreach (DataRow historyRow in history.Rows)
			{
				(historyRow[1]) = BnbWebControlServices.GetHtmlEncodedString((string)historyRow[1]);
			}

			dataGridHistory.DataSource = history;
			dataGridHistory.DataBind();
		}

		protected void buttonRefresh_Click(object sender, System.EventArgs e)
		{
            this.SetHistoryRowsOption(this.HistoryRows.ToString());
			this.ShowHistory();
		}

        private void SetHistoryRowsOption(string historyString)
        {
            if (!BnbRuleUtils.IsNumericInt(historyString))
                // ignore it
                return;

            int historyInt = int.Parse(historyString);
            BnbAdminUserOptions userOpt = BnbAdminUserOptions.RetrieveByUserID(BnbEngine.SessionManager.GetSessionInfo().UserID);
            BnbEditManager em = new BnbEditManager();
            userOpt.RegisterForEdit(em);
            userOpt.HistoryRows = historyInt;
            em.SaveChanges();
            // also copy to session
            this.Session["HistoryRows"] = userOpt.HistoryRows;
        }

		
		private void InitControls()
		{
            string cookieHistoryRows = this.GetHistoryRowsOption();
			// default to 10 if no sensible value in cookie
			if (cookieHistoryRows == null || cookieHistoryRows.Trim() == "" || !BnbRuleUtils.IsNumericInt(cookieHistoryRows))
				cookieHistoryRows = "10";

			textHistoryRows.Text = cookieHistoryRows;
		}

        private string GetHistoryRowsOption()
        {
            int historyRows = 10;
            // should have been put into session during login
            if (this.Page.Session["HistoryRows"] != null)
                historyRows = (int)this.Page.Session["HistoryRows"];
            return historyRows.ToString();
        }
	}
}
