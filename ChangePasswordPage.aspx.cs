using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using System.Data.SqlClient;

namespace Frond
{
    /// <summary>
    /// Summary description for SFChangePassword.
    /// </summary>
    public partial class ChangePasswordPage : System.Web.UI.Page
    {
        //protected UserControls.Title titleBar;
        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            if (!IsPostBack)
                titleBar.TitleText = "Change Password";
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        protected void btnChange_Click(object sender, System.EventArgs e)
        {
            string newpassword1 = txtNewPassword.Text;
            string newpassword2 = txtConfirmNewPassword.Text;

            lblMessage.ForeColor = Color.Red;

            if (!isPasswordSafe(newpassword1) || !isPasswordSafe(newpassword2))
            {
                lblMessage.Text = "Illegal characters were used in the new password. Please use alphanumeric characters only.";
                return;
            }

            if (newpassword1 != newpassword2)
            {
                lblMessage.Text = "Both passwords did not match. Please try again";
                return;
            }

            try
            {
                BnbEngine.SessionManager.UpdateBdPw(newpassword1);
                lblMessage.ForeColor = Color.Green;
                lblMessage.Text = "Your password has been changed";
            }
            catch (SqlException sqle)
            {
                lblMessage.Text = sqle.Message;
            }
        }

        private bool isPasswordSafe(string pw)
        {
            string pwinHtml = HttpUtility.HtmlEncode(pw);
            return (pwinHtml == pw);
        }
    }
}
