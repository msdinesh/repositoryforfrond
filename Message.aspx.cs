using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Frond
{
	/// <summary>
	/// Summary description for Message.
	/// </summary>
	public partial class Message : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (!this.IsPostBack)
			{	
				titleBar.TitleText = "Message";
				string message = "<No message found>";
				if (Request.QueryString["Message"] != null)
					message = Server.UrlDecode(Request.QueryString["Message"]);
				labelMessage.Text = message;
			}
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
