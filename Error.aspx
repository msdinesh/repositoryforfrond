<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="UserControls/MenuBar.ascx" %>
<%@ Page language="c#" Codebehind="Error.aspx.cs" AutoEventWireup="True" Inherits="Frond.Error" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Error</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<TABLE id="Table2" class="errortable" style="BACKGROUND-COLOR:#ffff00">
					<TR>
						<td class="titleicon" width="24">
							<IMG class="icon24" src="Images\bug24yellow.gif">
						</td>
						<TD><span style="FONT-SIZE: 20px;COLOR: #9966ff"> Error</span></TD>
					</TR>
				</TABLE>
				<table>
					<tr>
						<td style="PADDING-RIGHT:10px;PADDING-LEFT:10px;PADDING-BOTTOM:10px;PADDING-TOP:10px">
							<p><b>An unexpected error has occured.</b></p>
							<p>Its unfortunate, but it does happen from time to time.</p>
							<p>With some types of error (e.g. connection problems) you may be OK if you re-try 
								whatever it was you were doing. If you're feeling lucky, press the 'back' 
								button in your browser, or go back to the main menu and try again.</p>
							<p>Alas, some types of error never go away no matter how many times you try.</p>
							<p><strong>If you want to tell the IT department about the problem, please 
									copy-and-paste the text in the box below, and email it to the IT Helpdesk.</strong><br>
								Also provide as much information as you can about what you were doing when the 
								error happened (that will usually help them to track down the problem faster)</p>
							<P></P>
							<Table class="errortable">
								<tr>
									<td>
										&nbsp;&nbsp;&nbsp;<br>
										<asp:Label id="labelErrorSummary" runat="server"></asp:Label><br>
										&nbsp;</td>
								</tr>
							</Table>
							<P>
								<asp:LinkButton id="linkButtonMoreDetails" runat="server" onclick="linkButtonMoreDetails_Click">More Details...</asp:LinkButton></P>
							<P>
								<asp:Panel id="panelErrorDetails" runat="server" Visible="False">
									<P>The box below has more details about the error. Don't worry about emailing this 
										bit to anyone, as it has already been logged to the database. Just copy and 
										paste the info in the box above.</P>
									<TABLE class="errortable">
										<TR>
											<TD>&nbsp;&nbsp;&nbsp;<BR>
												<asp:Label id="labelErrorDetails" runat="server"></asp:Label><BR>
												&nbsp;&nbsp;&nbsp;</TD>
										</TR>
									</TABLE>
								</asp:Panel>
							<P></P>
						</td>
					</tr>
				</table>
			</div>
		</form>
	</body>
</HTML>
