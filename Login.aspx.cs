using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Reflection;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using BonoboWebControls;

namespace Frond
{
	/// <summary>
	/// Summary description for SFLogin.
	/// </summary>
	public partial class Login : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
		protected System.Web.UI.WebControls.Panel panelImageOne;
		protected System.Web.UI.WebControls.Panel panelImageTwo;
		protected System.Web.UI.WebControls.Panel panelVSOLogo;
		protected UserControls.MenuBar menuBar;


		protected void Page_Load(object sender, System.EventArgs e)
		{
			// set web styleid of menu bar based on URL
			menuBar.OverrideWebStyleID = Login.DeriveWebStyleIDFromURL(this.Request);
			BnbWorkareaManager.SetPageStyle(this, Login.DeriveWebStyleIDFromURL(this.Request));

		
			if (!Page.IsPostBack)
			{
				
				titleBar.TitleText = String.Format("Welcome to {0} - Please log in",
					menuBar.VirtualApplicationName);

				tblVersions.Visible = false;
				// try cookie login
				if (BnbEngine.SessionManager.StartNewSessionWithLoginCookie(BnbConst.Permission_Frond))
				{
					BnbEngine.SessionManager.ClosePreviousSimilarSessionsForUser();
					//Redirect them to where they came from or to the menu.
					FormsAuthentication.RedirectFromLoginPage(txtUsername.Text, false);
                    this.AfterLoginTasks();
					
				}

				// disable remember me if cookie logins not allowed
				if (!BnbEngine.SessionManager.AllowCookieLogin())
					cbRememberMe.Enabled = false;

				this.ShowSplashTable();
                Session["MergeTarget"] = 0;
                Session["MergeUnwanted"] = 0;

                //Initialise IndividualID session;
                Session["MergeTargetID"] = "";
                Session["MergeUnwantedID"] = "";
			}
		
			if(Session["LoggedIn"] != null && ((bool)Session["LoggedIn"]))
			{
				lblLoginResult.Visible = true;
				lblLoginResult.Text = "As a security measure, users are allowed to keep Frond open and idle for a limit amount of time.<br>This idle time has now expired.  Please log back in to continue.";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

        /// <summary>
        /// a few tasks to do after a login has happened
        /// </summary>
        private void AfterLoginTasks()
        {
            Session["LoggedIn"] = true;	
            Guid userID = BnbEngine.SessionManager.GetSessionInfo().UserID;
	        // copy user options into session so that they are available
            BnbAdminUserOptions userOptions = BnbAdminUserOptions.RetrieveByUserID(userID);
            this.Session["HistoryRows"] = userOptions.HistoryRows;
            this.Session["ShowAllTabsVertically"] = userOptions.ShowAllTabsVertically;


        }

		protected void cmdLogin_Click(object sender, System.EventArgs e)
		{
			bool frondPermission = BnbEngine.SessionManager.SessionlessUserHasPermission(txtUsername.Text, BnbConst.Permission_Frond);
			if (frondPermission)
			{
				bool authenticate = BnbEngine.SessionManager.StartNewSession(txtUsername.Text, txtPassword.Text);
				if (authenticate)
				{
					BnbEngine.SessionManager.ClosePreviousSimilarSessionsForUser();
					if (cbRememberMe.Checked)
						BnbEngine.SessionManager.MakeLoginCookieAfterLogin();
					
					FormsAuthentication.RedirectFromLoginPage(txtUsername.Text, false);
                    Session["LoggedIn"] = true;
                    Session["MergeTargetID"] = "";
                    Session["MergeUnwantedID"] = "";
                    Session["MergeTarget"] = "";
                    Session["MergeUnwanted"] = "";
                    this.AfterLoginTasks();
				}
				else
				{
					lblLoginResult.Text = BnbEngine.SessionManager.GetLDAPError();
					if (lblLoginResult.Text == "")
						lblLoginResult.Text = "Login failed for '" + txtUsername.Text + "'";
					lblLoginResult.Visible=true;
				}
			}
			else
			{
				lblLoginResult.Text = "Login failed for '" + txtUsername.Text + "' (check permissions)";
				lblLoginResult.Visible=true;			
			}
		}



		//gets database and version info displays it on the form
		private void GetDatabaseInfo()
		{
			// show database info
			BnbConfigurationInfo bonoboConfig = BnbEngine.SessionManager.GetConfigurationInfo();
			lblDatabaseServer.Text = bonoboConfig.DatabaseServer;
			lblDatabase.Text = bonoboConfig.DatabaseName;
			if (bonoboConfig.IsTestDatabase)
				lblDatabase.Text += " (Test Database)";
			else
				lblDatabase.Text += " (Live Database)";


            uxUrlDesc.Text = this.Request.Url.ToString();

			// show assembly versions
			// show assembly versions
			Hashtable assemblyList = About.BonoboAssemblyVersions();

			if (assemblyList.ContainsKey("Frond"))
				lblAssemblyVersion.Text = assemblyList["Frond"].ToString();
			if (assemblyList.ContainsKey("BonoboEngine"))
				lblBonoboEngine.Text = assemblyList["BonoboEngine"].ToString();
			if (assemblyList.ContainsKey("BonoboDomainObjects"))
				lblBonoboDomainObjects.Text = assemblyList["BonoboDomainObjects"].ToString();
			if (assemblyList.ContainsKey("BonoboWebControls"))
				lblBonoboWebControls.Text = assemblyList["BonoboWebControls"].ToString();
			if (assemblyList.ContainsKey("SF2ReportsEngine"))
				lblSF2ReportsEngine.Text = assemblyList["SF2ReportsEngine"].ToString();

			//Find out their client ip address
			System.Net.IPAddress clientIP=(System.Net.IPAddress.Parse(HttpContext.Current.Request.UserHostAddress));
			
			BnbIPSniffer.BnbDeriveOfficeResults ipResults = BnbIPSniffer.DeriveOfficeFromIPAddress(clientIP);
			
			lblHostIP.Text =(string) clientIP.ToString();
			lblVSOOffice.Text=ipResults.OfficeDescription;
			lblSessionAllowed.Text=ipResults.SessionTimeout.ToString() + " minutes";
				
		}

		protected void cmdShowVersions_Click(object sender, System.EventArgs e)
		{
			if (!tblVersions.Visible )
			{
				GetDatabaseInfo();
				tblVersions.Visible=true;
				cmdShowVersions.Visible = false;
			}
			
			
		}

		/// <summary>
		/// Usually WebStyleID is based on the logged in users settings.
		/// But when no-one is logged in, it has to be worked out by looking at the URL
		/// </summary>
		/// <param name="currentRequest"></param>
		/// <returns></returns>
		public static int DeriveWebStyleIDFromURL(HttpRequest currentRequest)
		{
            string url = currentRequest.Url.ToString();
            if (url.ToLower().IndexOf("frond") > -1)
				return 10;
            if (url.ToLower().IndexOf("starfish") > -1)
				return 11;
            if (url.ToLower().IndexOf("pgms") > -1)
				return 12;
			
			return 10;
		}

		private void ShowSplashTable()
		{
			// only show the splash table for ofices marked as having 'good connection speed'
			System.Net.IPAddress clientIP=(System.Net.IPAddress.Parse(HttpContext.Current.Request.UserHostAddress));
			BnbIPSniffer.BnbDeriveOfficeResults ipResults = BnbIPSniffer.DeriveOfficeFromIPAddress(clientIP);
			bool showSplash = false;
			if (ipResults.OfficeID > 0)
			{
				BnbLookupDataTable officeLookup = BnbEngine.LookupManager.GetLookup("lkuOffice");
				BnbLookupRow officeRow = officeLookup.FindByID(ipResults.OfficeID);
				if (officeRow != null)
				{
					showSplash = (bool)officeRow["GoodConnectionSpeed"];
				}
			}

			if (!showSplash)
			{
				// hide the cells with images
				uxSplashTable.Rows[0].Cells[1].Visible = false;
				uxSplashTable.Rows[1].Cells[0].Visible = false;
				uxSplashTable.Rows[1].Cells[1].Visible = false;
			}
		}
	}
}
