using System;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Reflection;
using BonoboEngine;
using BonoboEngine.Query;


namespace Frond
{
	/// <summary>
	/// Summary description for MyAccount.
	/// </summary>
	public partial class About : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label uxFullName;
		protected System.Web.UI.WebControls.Label uxLoginName;
		protected BonoboWebControls.DataControls.BnbStandaloneLookupControl uxCountry;
		protected BonoboWebControls.DataControls.BnbStandaloneLookupControl uxProgrammeOffice;
		protected BonoboWebControls.DataControls.BnbStandaloneLookupControl uxRecruitmentBase;
		protected BonoboWebControls.DataControls.BnbStandaloneLookupControl uxDepartment;
		protected System.Web.UI.WebControls.Label uxWorkareas;
		protected System.Web.UI.WebControls.Panel UserTab;
		protected System.Web.UI.WebControls.DataGrid uxPermissionGrid;
		protected System.Web.UI.WebControls.Panel PermissionTab;
		protected System.Web.UI.WebControls.Label uxUserEmail;
		protected BonoboWebControls.DataControls.BnbStandaloneLookupControl uxWebStyle;
		protected UserControls.Title titleBar;
		protected UserControls.MenuBar menuBar;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (!this.IsPostBack)
			{
				this.GetDatabaseInfo();
				this.GetCreditsInfo();
				titleBar.TitleText = "About " + menuBar.VirtualApplicationName;
                this.ShowApplicationName(menuBar.VirtualApplicationName);
			}
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
		}

        private void ShowApplicationName(string virtualAppName)
        {
            uxVirtualApplicationName.Text = virtualAppName;
            if (!virtualAppName.ToLower().Equals("frond"))
            {
                // show extra panel explaining the Frond Platform
                uxVirtualAppPanel.Visible = true;
                uxVirtualApplicationName2.Text = virtualAppName;

            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

	

		private void GetDatabaseInfo()
		{
			// show database info
			BnbConfigurationInfo bonoboConfig = BnbEngine.SessionManager.GetConfigurationInfo();
			lblDatabaseServer.Text = bonoboConfig.DatabaseServer;
			lblDatabase.Text = bonoboConfig.DatabaseName;
			if (bonoboConfig.IsTestDatabase)
				lblDatabase.Text += " (Test Database)";
			else
				lblDatabase.Text += " (Live Database)";

            // show server info
            uxWebServerDesc.Text = Server.MachineName;
            uxVirtualDirDesc.Text = HttpRuntime.AppDomainAppVirtualPath;
            uxUrlDesc.Text = this.Request.Url.ToString();

			// show assembly versions
			Hashtable assemblyList = About.BonoboAssemblyVersions();

			if (assemblyList.ContainsKey("Frond"))
				lblAssemblyVersion.Text = assemblyList["Frond"].ToString();
			if (assemblyList.ContainsKey("BonoboEngine"))
				lblBonoboEngine.Text = assemblyList["BonoboEngine"].ToString();
			if (assemblyList.ContainsKey("BonoboDomainObjects"))
				lblBonoboDomainObjects.Text = assemblyList["BonoboDomainObjects"].ToString();
			if (assemblyList.ContainsKey("BonoboWebControls"))
				lblBonoboWebControls.Text = assemblyList["BonoboWebControls"].ToString();
			if (assemblyList.ContainsKey("SF2ReportsEngine"))
				lblSF2ReportsEngine.Text = assemblyList["SF2ReportsEngine"].ToString();
			

			System.Net.IPAddress clientIP=(System.Net.IPAddress.Parse(HttpContext.Current.Request.UserHostAddress));
			
			BnbIPSniffer.BnbDeriveOfficeResults ipResults = BnbIPSniffer.DeriveOfficeFromIPAddress(clientIP);
			
			lblHostIP.Text =(string) clientIP.ToString();
			lblVSOOffice.Text=ipResults.OfficeDescription;
			lblSessionAllowed.Text=ipResults.SessionTimeout.ToString() + " minutes";
				
		}

		/// <summary>
		/// Returns a name-value collection containing Assembly Names and assembly versions
		/// </summary>
		/// <returns></returns>
		public static Hashtable BonoboAssemblyVersions()
		{
			Hashtable assemblyList = new Hashtable();

			Assembly thisAssembly = Assembly.GetExecutingAssembly();

			// add current assembly (the Frond one)
			assemblyList.Add(thisAssembly.GetName().Name, thisAssembly.GetName().Version.ToString());
			foreach(AssemblyName assemLoop in thisAssembly.GetReferencedAssemblies())
			{
				switch(assemLoop.Name)
				{
					case("BonoboEngine"):
					case("BonoboDomainObjects"):
					case("BonoboWebControls"):
					case("SF2ReportsEngine"):
						assemblyList.Add(assemLoop.Name, assemLoop.Version.ToString());
					break;
		
					default:
						break;
				}
			}
			return assemblyList;
		}

		/// <summary>
		/// Returns a hashtable of developer credits
		/// </summary>
		/// <returns></returns>
		public static Hashtable BonoboDeveloperCredits()
		{
			Hashtable creditList = new Hashtable();

			Assembly[] bonoboAssemblies = About.GetBonoboAssemblies();
			foreach(Assembly assemLoop in bonoboAssemblies)
			{
				object[] creditAttribs = assemLoop.GetCustomAttributes(typeof(BnbDeveloperCreditAttribute), false);
				foreach(BnbDeveloperCreditAttribute devCredit in creditAttribs)
				{
					if (!creditList.ContainsKey(devCredit.DeveloperName))
						creditList.Add(devCredit.DeveloperName, devCredit.DeveloperName);
				}
			}

			return creditList;

		}

		/// <summary>
		/// Returns an array of Bonobo Assemblies (including this one)
		/// </summary>
		/// <returns></returns>
		private static Assembly[] GetBonoboAssemblies()
		{
			Assembly[] bonoboAssemblies = new Assembly[5];
			bonoboAssemblies[0] = Assembly.GetAssembly(typeof(BonoboEngine.BnbEditManager));
			bonoboAssemblies[1] = Assembly.GetAssembly(typeof(BonoboDomainObjects.BnbIndividual));
			bonoboAssemblies[2] = Assembly.GetAssembly(typeof(BonoboWebControls.BnbWebFormManager));
			bonoboAssemblies[3] = Assembly.GetAssembly(typeof(SF2ReportsEngine.sfRenderBroker));
			bonoboAssemblies[4] = Assembly.GetAssembly(typeof(Frond.About));
			return bonoboAssemblies;
		}

		/// <summary>
		/// Returns a hashtable of System credits
		/// Each object in the hashtable is a string array with two items - the system name and description
		/// </summary>
		/// <returns></returns>
		public static Hashtable BonoboSystemCredits()
		{
			Hashtable creditList = new Hashtable();

			Assembly[] bonoboAssemblies = About.GetBonoboAssemblies();
			foreach(Assembly assemLoop in bonoboAssemblies)
			{
				object[] creditAttribs = assemLoop.GetCustomAttributes(typeof(BnbSystemCreditAttribute), false);
				foreach(BnbSystemCreditAttribute sysCredit in creditAttribs)
				{
					if (!creditList.ContainsKey(sysCredit.SystemName))
						creditList.Add(sysCredit.SystemName, new string[]{sysCredit.SystemName, sysCredit.SystemDescription});
				}
			}

			return creditList;

		}

	
		private void GetCreditsInfo()
		{
			Hashtable credits = About.BonoboDeveloperCredits();
			ArrayList sortedCredits = new ArrayList(credits.Values);
			sortedCredits.Sort();
			foreach(string credLoop in sortedCredits)
				uxDeveloperCredits.Text += credLoop + "<br/>";

			Hashtable sysCredits = About.BonoboSystemCredits();

			StringBuilder sb = new StringBuilder();
			sb.Append("<table>");
			foreach(string[] sysCreditArray in sysCredits.Values)
			{
				sb.AppendFormat("<tr><td style=\"padding-right:10px;\">{0}</td><td>{1}</td></tr>",
					sysCreditArray[0], sysCreditArray[1]);
			}
			sb.Append("</table>");
			uxSystemCredits.Text = sb.ToString();

		}

	}
}
