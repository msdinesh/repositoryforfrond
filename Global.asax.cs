using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.Security;
using BonoboEngine;
using BonoboDomainObjects;
using BonoboWebControls;
using System.Globalization;
using System.Reflection;
using System.Diagnostics;

namespace Frond 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		public Global()
		{
			InitializeComponent();
		}	
		
		protected void Application_Start(Object sender, EventArgs e)
		{
		}
 
		protected void Session_Start(Object sender, EventArgs e)
		{
#if DEBUG
			// auto login skipped if TransferKey is passed in
			//(so that session transfer code (below) can be tested)
			string checkTransferid = HttpContext.Current.Request.QueryString["TransferKey"];
            //if (checkTransferid == null)
                //BnbWebFormManager.AutoLoginToBonobo();
#endif
			// is there a transfer key in the url?
			string transferid = HttpContext.Current.Request.QueryString["TransferKey"];
			if(transferid != null && transferid != "") 
			{
				// use transfer key to log in
				Guid transferKeyGuid = new Guid(transferid);
				bool frondPermission = BnbEngine.SessionManager.SessionlessUserHasPermission(transferKeyGuid, BnbConst.Permission_Frond);
                bool starfishPermission = BnbEngine.SessionManager.SessionlessUserHasPermission(transferKeyGuid, BnbConst.Permission_Starfish);

                if (frondPermission || starfishPermission)
				{
					bool success = BnbEngine.SessionManager.StartNewSession(transferKeyGuid);
					if (success)
					{
						string username = BnbEngine.SessionManager.GetSessionInfo().LoginName;
						FormsAuthentication.SetAuthCookie(username, false);
						FormsAuthentication.RedirectFromLoginPage(username, false);
					}
				}
			}
		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_Error(Object sender, EventArgs e)
		{
#if DEBUG
			bool logError = Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings["EnableErrorPage"]);
			if(!logError) return;
#endif
			// get the error
			Exception errorToLog = Server.GetLastError();

			try
			{
				// clear bonobo object cache after an error to avoid
				// objects crossing-over between sessions
				BnbEngine.SessionManager.ClearObjectCache();
			} 
			catch(Exception)
			{
				// ignore any probs
			}
			// log error to database
			int ticketID = this.LogErrorToDatabase(errorToLog);

			// if its a nested httpexception, we want message from inside
			Exception originalException = errorToLog;
			while((originalException.GetType() == typeof(System.Web.HttpException) ||
				originalException.GetType() == typeof(System.Web.HttpUnhandledException))
				&& originalException.InnerException != null)
			{
				originalException = originalException.InnerException;
			}

			if (originalException is BnbSessionException)
			{
				// possible problem is that browser session has continued, but server
				// session has not.
				// end server session and re-direct to menu so that user
				// is asked to log in again.
				
				// sign out the authentication and give the user a message
				// login cookies will still work.
				Response.Redirect("~/Logout.aspx?SessionTimedOut=1");
				return;
				
			}

			// re-direct to error page
			string errorPageURL = "~/Error.aspx?ErrorTicketID=" + ticketID;
			if (ticketID == -1)
				errorPageURL+="&ErrorMessage=" + errorToLog.Message;
	
			Response.Redirect(errorPageURL, true);

		}

		protected void Session_End(Object sender, EventArgs e)
		{
			BnbEngine.SessionManager.EndSession();

		}

		protected void Application_End(Object sender, EventArgs e)
		{
			this.LogApplicationEnd("Frond");
		}

		// logs error and returns ticketID
		private int LogErrorToDatabase(Exception errorToLog)
		{
			int ticketID = -1;
			string clientInfo = null;
			string pageURL = null;
			try
			{
				pageURL = this.Request.Url.ToString();
				clientInfo = this.Request.UserHostName + " " + this.Request.UserAgent;
			} 
			catch(Exception)
			{
				// ignore any probs
			}
			string applicationName = null;
			try
			{
				applicationName = BnbWebFormManager.ApplicationName();
			} 
			catch(Exception)
			{
				// ignore any probs
			}
			// try and log error to database
			try
			{
				ticketID = BnbEngine.SessionManager.LogError(applicationName, errorToLog, pageURL, clientInfo, null);
			}
			catch(Exception)
			{
				// ignore any probs
			}
			return ticketID;
		}

		/// <summary>
		/// When the web app is ending, logs some information about the reasons for the shutdown
		/// to the event log.
		/// Code based on a Microsoft Example by Scott Guthrie
		/// </summary>
		/// <param name="applicationName"></param>
		public void LogApplicationEnd(string applicationName)
		{
			string shutDownMessage = null;
			string shutDownStack = null;

			// use reflection to a private field to get a reference to the runtime
			HttpRuntime runtime = (HttpRuntime) typeof(System.Web.HttpRuntime).InvokeMember("_theRuntime",
				BindingFlags.NonPublic
				| BindingFlags.Static
				| BindingFlags.GetField,
				null,
				null,
				null);

			if (runtime != null)
			{
				// use reflection on private fields to get extra shutdown information
				shutDownMessage = (string) runtime.GetType().InvokeMember("_shutDownMessage",
					BindingFlags.NonPublic
					| BindingFlags.Instance
					| BindingFlags.GetField,
					null,
					runtime,
					null);

				shutDownStack = (string) runtime.GetType().InvokeMember("_shutDownStack",
					BindingFlags.NonPublic
					| BindingFlags.Instance
					| BindingFlags.GetField,
					null,
					runtime,
					null);
			}

			if (!EventLog.SourceExists(".NET Runtime")) 
			{
				EventLog.CreateEventSource(".NET Runtime", "Application");
			}
			EventLog log = new EventLog();
			log.Source = ".NET Runtime";
			log.WriteEntry(String.Format("\r\n\r\nApplication_End event fired for {0}\r\n\r\n_shutDownMessage={1}\r\n\r\n_shutDownStack={2}",
				applicationName,
				shutDownMessage,
				shutDownStack),
				EventLogEntryType.Error);

		}
			
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

