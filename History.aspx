<%@ Page language="c#" Codebehind="History.aspx.cs" AutoEventWireup="True" Inherits="Frond.History" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>SFHistory</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK rel="stylesheet" type="text/css" href="Css/FrondDefault.css">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:Title id="titleBar" runat="server" imagesource="Images/history24silver.gif"></uc1:Title>
						<P>
							<asp:DataGrid id="dataGridHistory" runat="server" CssClass="datagrid" AutoGenerateColumns="False">
								<HeaderStyle CssClass="datagridheader"></HeaderStyle>
								<Columns>
									<asp:BoundColumn DataField="RowNum" HeaderText="No"></asp:BoundColumn>
									<asp:HyperLinkColumn DataNavigateUrlField="RelativeURL" DataTextField="PageTitle" HeaderText="Screen/Record"></asp:HyperLinkColumn>
									<asp:BoundColumn DataField="TimeStamp" HeaderText="Last Visited" DataFormatString="{0:dd/MMM/yyyy}"></asp:BoundColumn>
								</Columns>
							</asp:DataGrid></P>
						<P>History rows:
							<asp:TextBox id="textHistoryRows" runat="server" Width="31px"></asp:TextBox>
							<asp:Button id="buttonRefresh" runat="server" Text="Refresh" onclick="buttonRefresh_Click"></asp:Button></P>
						<P>&nbsp;</P>
			</div>
		</form>
	</body>
</HTML>
