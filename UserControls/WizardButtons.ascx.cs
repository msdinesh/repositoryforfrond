namespace Frond.UserControls
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Collections;
    using System.IO;

	/// <summary>
	///		Summary description for SFWizardButtons.
	/// </summary>
	public partial class WizardButtons : System.Web.UI.UserControl
	{

		private ArrayList m_panelCollection = new ArrayList();
		public delegate void WizardEventHandler(object Sender, WizardEventArgs e);
		public delegate void WizardValidationEventHandler(object Sender, WizardValidationEventArgs e);
		public event WizardEventHandler ShowPanel;
		public event WizardEventHandler CancelWizard;
		public event WizardEventHandler FinishWizard;
		public event WizardValidationEventHandler ValidatePanel;
        private string m_firstPanelNextText = string.Empty;
        private const string VWSTATENEXTTEXT = "NEXTBUTTON_TEXT";
        //TPT Amended:PGMSP-19 - A new property included to skip the panel if needs
        public bool disablePanel;


		public class WizardEventArgs:EventArgs
		{
			public int CurrentPanel;
			public string CurrentPanelHtmlID;
			public WizardEventArgs(int currentPanel, string currentPanelHtmlID)
			{
				this.CurrentPanel = currentPanel;
				this.CurrentPanelHtmlID = currentPanelHtmlID;
			}
		}

		public class WizardValidationEventArgs:WizardEventArgs
		{
			public bool Proceed;
			public WizardValidationEventArgs(int currentPanel, string currentPanelHtmlID):base(currentPanel, currentPanelHtmlID)
			{
				Proceed = true;
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (!this.IsPostBack)
			{
                ViewState[VWSTATENEXTTEXT] = buttonNext.Text;
				this.CurrentPanel = 0;
				ShowCurrentPanel();
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		public void AddPanel(Panel pagePanel)
		{
			m_panelCollection.Add(pagePanel);
		}

		public int CurrentPanel
		{
			get
			{
				if (lblHiddenCurrentPanel.Text != "")
					return int.Parse(lblHiddenCurrentPanel.Text);
				else
					return 0;
			}
			set
			{
				lblHiddenCurrentPanel.Text = value.ToString();
			}
		}

		public string CurrentPanelHtmlID
		{
			get
			{
				int localCurrentPanel = this.CurrentPanel;
				if (localCurrentPanel>=0 && localCurrentPanel < m_panelCollection.Count)
					return ((Panel)m_panelCollection[localCurrentPanel]).ID;
				return "";
			}
		}

		public int FirstPanel
		{
			get{return 0;}
		}

		public int LastPanel
		{
			get{return m_panelCollection.Count-1;}
		}

		private void ShowCurrentPanel()
		{
			int count = 0;
			foreach(Panel panelLoop in m_panelCollection)
			{
				panelLoop.Visible =  (count == this.CurrentPanel);
				count +=1;
			}
            if (this.CurrentPanel == this.FirstPanel)
            {
                if (FirstPanelNextText.Trim() != string.Empty)
                    buttonNext.Text = FirstPanelNextText;
            }
            else
            {
                if (ViewState[VWSTATENEXTTEXT] != null)
                    buttonNext.Text = ViewState[VWSTATENEXTTEXT].ToString();
            }
			buttonPrev.Visible = (this.CurrentPanel > this.FirstPanel);
			buttonNext.Visible = (this.CurrentPanel < this.LastPanel);
			buttonFinish.Visible = (this.CurrentPanel == this.LastPanel);
			OnShowPanel();
		}

		protected void buttonPrev_Click(object sender, System.EventArgs e)
		{
			if (this.CurrentPanel > this.FirstPanel)
			{
				this.CurrentPanel -=1;
				ShowCurrentPanel();
			}
            //TPT Amended:PGMSP-19
            if (this.disablePanel == true)
            {
                this.CurrentPanel -= 1;
                ShowCurrentPanel();
            }
		}

		protected void buttonNext_Click(object sender, System.EventArgs e)
		{
			if (this.CurrentPanel < this.LastPanel)
			{
				if (this.OnValidatePanel())
				{
					this.CurrentPanel +=1;
					ShowCurrentPanel();
				}
			}
            //TPT Amended:PGMSP-19
            if (this.disablePanel == true)
            {
                this.CurrentPanel += 1;
                ShowCurrentPanel();
            }
		}

		protected virtual void OnShowPanel()
		{
			if (ShowPanel != null)
			{
				WizardEventArgs args = new WizardEventArgs(this.CurrentPanel, this.CurrentPanelHtmlID);
				ShowPanel(this, args);
			}
		}

		protected virtual bool OnValidatePanel()
		{
			WizardValidationEventArgs args = new WizardValidationEventArgs(this.CurrentPanel, this.CurrentPanelHtmlID);
			if (ValidatePanel != null)
			{
				ValidatePanel(this, args);
			}
			return args.Proceed;
		}

		protected void buttonCancel_Click(object sender, System.EventArgs e)
		{
			if (CancelWizard != null)
			{
				WizardEventArgs args = new WizardEventArgs(this.CurrentPanel, this.CurrentPanelHtmlID);
				CancelWizard(this, args);
			}
		}

		protected void buttonFinish_Click(object sender, System.EventArgs e)
		{
			if (FinishWizard != null)
			{
				WizardEventArgs args = new WizardEventArgs(this.CurrentPanel,this.CurrentPanelHtmlID );
				FinishWizard(this,args);
			}
		
		}

        public string FirstPanelNextText
        {
            get { return m_firstPanelNextText; }
            set { m_firstPanelNextText = value; }
        }

        public Button CancelButton
        {
            get { return buttonCancel; }
            set { buttonCancel = value; }
        }

        public Button PreviousButton
        {
            get { return buttonPrev; }
            set { buttonPrev = value; }
        }

        public Button FinishButton
        {
            get { return buttonFinish; }
            set { buttonFinish = value; }
        } 
					
	}
}
