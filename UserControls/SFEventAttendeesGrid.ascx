<%@ Control Language="c#" AutoEventWireup="True" Codebehind="SFEventAttendeesGrid.ascx.cs" Inherits="Frond.UserControls.SFEventAttendeesGrid" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<P>
	<TABLE id="Table1" width="80%" border="0">
		<TR>
			<TD width="50%" vAlign="top">
				<P>To add new attendees, enter a refno (or series of refnos) here and<BR>
					press 'Add attendee(s)'. Multiple refnos&nbsp;must&nbsp;be separated by commas.<BR>
					<asp:TextBox id="txtRefNos" runat="server" Width="304px"></asp:TextBox>&nbsp;
					<asp:Button id="btnAddAttendees" runat="server" Text="Add attendee(s)" Width="112px" onclick="btnAddAttendees_Click"></asp:Button><BR>
					<asp:Label id="lblMessage" runat="server" Font-Bold="True"></asp:Label></P>
			</TD>
			<asp:Panel id="pnlMarkAttendeesOutcome" runat="server">
				<TD vAlign="top" width="50%">
					<P align="right">Set marked attendees situation to:
						<asp:DropDownList id="ddlAttendanceStatus" Width="100px" runat="server"></asp:DropDownList>
						<asp:Button id="btnMarkStatus" runat="server" Text="Mark" onclick="btnMarkStatus_Click"></asp:Button><BR>
						Set marked attendees outcome to:
						<asp:DropDownList id="ddlAttendanceOutcome" Width="100px" runat="server"></asp:DropDownList>
						<asp:Button id="btnMarkOutcome" runat="server" Text="Mark" onclick="btnMarkOutcome_Click"></asp:Button></P>
				</TD>
			</asp:Panel>
		</TR>
	</TABLE>
</P>
