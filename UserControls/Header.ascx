<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Control Language="c#" AutoEventWireup="True" Codebehind="Header.ascx.cs" Inherits="Frond.UserControls.Header" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<!--<div id="waterMark" style="POSITION: absolute"><A href="javascript:window.scrollTo(0,0);" target="_self">
<img height=30 alt="Back to top" src="<%=virtualroot%>/Images/top.gif" width=26 
border=0></A></div>-->
<!--<script language="javascript" src="<%=virtualroot%>/Includes/TopAnchor.js"></script>-->
<TABLE class="<%=cssClass%>" id=Table1  border=0>
	<TR>
		<TD width="50"><img height=75 
      src="<%=virtualroot%>/Images/FrondSmall.jpg" width=75 
      style="BORDER-RIGHT: lime 1px solid; BORDER-TOP: lime 1px solid; BORDER-LEFT: lime 1px solid; BORDER-BOTTOM: lime 1px solid"
    ></TD>
	</TR>
	<tr>
		<td>
			<H2><span class="StarfishTitle">Dont use Header.ascx anymore - use MenuBar.ascx 
					instead!</span></H2>
		</td>
	</tr>
	<tr>
		<td>
			<%=appMode%>
		</td>
	</tr>
	<tr>
		<td>
			<table class="menucollapse">
				<tr>
					<td class="menuicon"><a href="<%=virtualroot%>/Menu.aspx" 
      target=_top><img class="icon24" src="<%=virtualroot%>/Images/mainmenu24silver.gif"></a>
					</td>
					<td class="menutext"><a href="<%=virtualroot%>/Menu.aspx" 
      target=_top>Menu</a>
					</td>
				</tr>
				<tr>
					<td class="menuicon">
						<a 
      href="<%=virtualroot%>/FindGeneral.aspx" target=_top 
      ><img class="icon24" src="<%=virtualroot%>/Images/find24silver.gif"></a>
					</td>
					<td class="menutext"><a href="<%=virtualroot%>/FindGeneral.aspx" 
      target=_top>Find</a>
					</td>
				</tr>
				<tr>
					<td class="menuicon">
						<a href="<%=virtualroot%>/Reports/ReportChooser.aspx" target=_top><img class="icon24" src="<%=virtualroot%>/Images/report24silver.gif"></a>
					</td>
					<td class="menutext"><a href="<%=virtualroot%>/Reports/ReportChooser.aspx" 
      target=_top>Reports</a>
					</td>
				</tr>
				<tr>
					<td class="menuicon">
						<a 
      href="<%=virtualroot%>/History.aspx" target=_top 
      ><img class="icon24" src="<%=virtualroot%>/Images/history24silver.gif"></a>
					</td>
					<td class="menutext"><a href="<%=virtualroot%>/History.aspx" 
      target=_top>History</a>
					</td>
				</tr>
				<tr>
					<td class="menuicon">
						<a href="http://vision/new/support-for-staff/support-services/it/instructions-how-to-s/frond-help-homepage"
							target="_blank"><img class="icon24" src="<%=virtualroot%>/Images/help24silver.gif"></a>
					</td>
					<td class="menutext"><asp:hyperlink id="hlHelp" runat="server" target="_blank">Help</asp:hyperlink></A>
					</td>
				</tr>
				<tr>
					<td class="menuicon">
						<a 
      href="<%=virtualroot%>/Logout.aspx" target=_top 
      ><img class="icon24" src="<%=virtualroot%>/Images/logout24silver.gif"></a>
					</td>
					<td class="menutext"><a href="<%=virtualroot%>/Logout.aspx" 
      target=_top>Logout</a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<asp:HyperLink id="hlCustom" runat="server"></asp:HyperLink>
		</td>
	</tr>
	<tr style="HEIGHT:100%">
		<td></td>
	</tr>
</TABLE>
<bnbdatagrid:BnbDataGridForQueryDataSet id="BnbDataGridForQueryDataSet3" runat="server" CssClass="datagrid"></bnbdatagrid:BnbDataGridForQueryDataSet>
