<%@ Control Language="C#" AutoEventWireup="true" Codebehind="GShowLocation.ascx.cs"
    Inherits="Frond.UserControls.GShowLocation" %>
<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.GMapControl"
    TagPrefix="cc1" %>
<table class=<%=CssName%> width="100%">
    <tr>
        <td valign="top" width="20%">
            <table  width="100%">
                <tr>
                    <td class="labelLeftBold" colspan="2">
                      <asp:Label runat ="server" ID ="lblHeading">Goegraphical details</asp:Label>  
                    </td>
                </tr>
                <tr>
                    <td class="label" >
                        *Latitude
                    </td>
                    <td>
                        <input tabindex="-1" id="txtLatitude" runat="server" readonly="readonly" type="text" /></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="label" >
                        *Longitude
                    </td>
                    <td>
                        <input tabindex="-1" id="txtLongitude" runat="server" readonly="readonly" type="text" /></td>
                    <td>
                        &nbsp;</td>
                </tr>
              
                <tr>
                    <td id="tdUserHelp" class="labelHelp" colspan="2">

                        <br />
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <span id="gErrorMessage" class="feedback"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </td>
        <td width="80%">
            <table width="100%">
                <tr>
                    <td class="labelLeft">
                        <cc1:GoogleMap ID="gmShowLocation" runat="server">
                        </cc1:GoogleMap>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
