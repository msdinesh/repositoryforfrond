<%@ Control Language="C#" AutoEventWireup="true" Codebehind="GFindLocation.ascx.cs"
    Inherits="Frond.UserControls.GFindLocation" %>
<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.DataControls"
    TagPrefix="bnbdatacontrol" %>

<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.GMapControl"
    TagPrefix="cc1" %>
<table cellpadding=0 cellspacing=0 width="100%" border="0">
    <tr>
        <td align="center">
            Country 
            &nbsp; &nbsp;<bnbdatacontrol:BnbStandaloneLookupControl id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" Height="22px" Width="144px"
									LookupTableName="lkuCountry" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" ShowBlankRow="True"></bnbdatacontrol:BnbStandaloneLookupControl>
            &nbsp; &nbsp; &nbsp;&nbsp;
                        <asp:RadioButton ID="rdoVolunteer" runat="server" GroupName="Find" Text="Volunteer"
                Checked="True" /><asp:RadioButton ID="rdoEmployer" GroupName="Find" runat="server" Text="Employer" />
               &nbsp; &nbsp;   <asp:Button ID="btnFind" runat="server" Text="Find" OnClick="btnFind_Click" /></td>
       
    </tr>
    <tr>
        <td>&nbsp;
        </td>
    </tr>
     <tr>
        <td>&nbsp;
        </td>
    </tr>
</table>
<table cellpadding=0 cellspacing=0 width="100%" border="0">
<tr>
<td align="center">
    <cc1:GoogleMap ID="GoogleMap1" runat="server" Height="380px" Width="600px">
    </cc1:GoogleMap>
</td>
</tr>
</table>