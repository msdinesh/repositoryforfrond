namespace Frond.UserControls
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using BonoboDomainObjects;
	using BonoboEngine;

	/// <summary>
	///	OrganisationAddressPanel can be used to gather Organisation (BnbCompany) address information
	///	in wizards (but not in EditPages that use the BnbWebFormManager, they need to do it differently)
	/// </summary>
	public partial class OrganisationAddressPanel : System.Web.UI.UserControl
	{

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (!this.IsPostBack)
            {
				this.FillCountriesCombo();
                //Added for role based access - google maps
                DoMapVisibility();
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			uxAddressLookup.SelectedValueChanged +=new EventHandler(uxAddressLookup_SelectedValueChanged);
			uxAddressLine1Search.Click +=new EventHandler(this.uxPostcodeSearch_Click);
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		protected void uxPostcodeSearch_Click(object sender, System.EventArgs e)
		{
			uxAddressLookup.CountryID = new Guid(uxCountryID.SelectedValue.ToString());
			uxAddressLookup.PostCode = uxPostcode.Text;
			uxAddressLookup.Line1 = uxAddressLine1.Text;
			uxAddressLookup.PerformSearch();
			uxAddressLookup.Visible = true;
		}

		private void uxAddressLookup_SelectedValueChanged(object sender, EventArgs e)
		{
			uxAddressLine1.Text = uxAddressLookup.Line1;
			uxAddressLine2.Text = uxAddressLookup.Line2;
			uxAddressLine3.Text = uxAddressLookup.Line3;
			uxAddressLine4.Text = uxAddressLookup.Town;
			uxPostcode.Text = uxAddressLookup.PostCode;
			uxCounty.Text = uxAddressLookup.County;
		}

		/// <summary>
		/// based on the field values, creates a new BnbAddress and BnbCompanyAddress object and
		/// attaches it to the BnbCompany address passed in
		/// </summary>
		/// <param name="comp"></param>
		/// <param name="em"></param>
		public BnbAddress SaveOrganisationAddressDetails(BnbCompany comp, BnbIndividual linkedIndividual, BnbEditManager em)
		{
			BnbAddress newAdd = new BnbAddress(true);
			newAdd.RegisterForEdit(em);
			newAdd.AddressLine1 = uxAddressLine1.Text;
			newAdd.AddressLine2 = uxAddressLine2.Text;
			newAdd.AddressLine3 = uxAddressLine3.Text;
			newAdd.AddressLine4 = uxAddressLine4.Text;
			newAdd.PostCode = uxPostcode.Text;
			newAdd.County = uxCounty.Text;
			newAdd.CountryID = new Guid(uxCountryID.SelectedValue.ToString());

            //Capture latitude, longitude values from Google map
            if (HasPermissionToLocateOnMap())
            {

                if (showLocation.Latitude.Trim() != "")
                    newAdd.Latitude = Convert.ToSingle(showLocation.Latitude);
                else
                    //Setting null value for validation
                    newAdd.Latitude = Convert.ToSingle(0.0);

                if (showLocation.Longitude.Trim() != "")
                    newAdd.Longitude = Convert.ToSingle(showLocation.Longitude);
                else
                    //Setting null value for validation
                    newAdd.Longitude = Convert.ToSingle(0.0);
            }
			newAdd.AddCompanyAddress();
			newAdd.CompanyAddress.RegisterForEdit(em);
			newAdd.CompanyAddress.DateFrom = DateTime.Today;
			comp.CompanyAddresses.Add(newAdd.CompanyAddress);
			// add the telephone if specified
			if (uxTelephone.Text != "")
				newAdd.SetTelephone(linkedIndividual, uxTelephone.Text);

			return newAdd;
		}

		/// <summary>
		/// StandaloneLookupControl doesnt work on UserControls so have
		/// to use a normal LookupControl instead
		/// </summary>
		private void FillCountriesCombo()
		{
			// country lookup on panel zero
			
			DataTable countryTable =  BnbEngine.LookupManager.GetLookup("lkuCountry");
			countryTable.DefaultView.RowFilter = "Exclude = 0";
			countryTable.DefaultView.Sort = "Description";
			uxCountryID.DataSource = countryTable.DefaultView;
			uxCountryID.DataTextField = "Description";
			uxCountryID.DataValueField = "GuidID";

			// default country lookup
			Guid defaultCountryID = BnbEngine.SessionManager.GetSessionInfo().DefaultCountryID;
            //Code Integrated for displaying defaultcountry only to starfish users
            if ((BnbEngine.SessionManager.GetSessionInfo().WebStyleID) == 11)
			if (defaultCountryID != Guid.Empty)
			{
				uxCountryID.SelectedValue = Convert.ToString(defaultCountryID);
				
			}
			else
			{
				ListItem blankItem = new ListItem("<Any>","");
				uxCountryID.Items.Insert(0,blankItem);
			}
			uxCountryID.DataBind();
		}

		public string CountryComboSelectedValue
		{
			get{return uxCountryID.SelectedValue;}
			set{uxCountryID.SelectedValue = value;}
        }

        #region " GMap Properties "

        public string Latitude
        {
            get
            {
                return showLocation.Latitude;
            }
            set
            {
                showLocation.Latitude = value;
            }

        }

        public string Longitude
        {
            get
            {
                return showLocation.Longitude;
            }
            set
            {
                showLocation.Longitude = value;
            }

        }

        #endregion

        /// <summary>
        /// Check permisssion to access the Google map functionality
        /// </summary>
        private bool HasPermissionToLocateOnMap()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_LocateAddressOnMap))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Added newly for role based access
        /// </summary>
        private void DoMapVisibility()
        {
            if (HasPermissionToLocateOnMap())
            {
                showLocation.Visible = true;
                btnShowLocation.Visible = true;
            }
            else
            {
                showLocation.Visible = false;
                btnShowLocation.Visible = false;
            }
        }
    }
}
