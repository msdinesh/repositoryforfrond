namespace Frond.UserControls
{
	using System;
	using System.Data;
	using System.Data.SqlClient;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using BonoboEngine;
	using BonoboEngine.Query;
	using BonoboDomainObjects;

	/// <summary>
	///		Summary description for IndividualAddressPanel.
	/// </summary>
	public partial class IndividualAddressPanel : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.Panel panelOne;
		protected System.Web.UI.WebControls.TextBox Textbox1;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (!this.IsPostBack)
			{
				FillCountriesCombo();
				this.bnbAddressLookup1.Visible = false;
                //Role based access for google map
                DoMapVisibility();
			}
			
		}

		private void FillCountriesCombo()
		{
			// country lookup on panel zero
			
			DataTable countryTable =  BnbEngine.LookupManager.GetLookup("lkuCountry");
			countryTable.DefaultView.RowFilter = "Exclude = 0";
			countryTable.DefaultView.Sort = "Description";
			drpCountry.DataSource = countryTable;
			drpCountry.DataTextField = "Description";
			drpCountry.DataValueField = "GuidID";

			// default country lookup
			Guid defaultCountryID = BnbEngine.SessionManager.GetSessionInfo().DefaultCountryID;
            //Code Integrated for displaying defaultcountry only to starfish users
            if ((BnbEngine.SessionManager.GetSessionInfo().WebStyleID) == 11)
			if (defaultCountryID != Guid.Empty)
			{
				drpCountry.SelectedValue = Convert.ToString(defaultCountryID);
				bnbAddressLookup1.CountryID = defaultCountryID;
			}
			else
			{
				ListItem blankItem = new ListItem("<Any>","");
				drpCountry.Items.Insert(0,blankItem);
			}
			drpCountry.DataBind();
		}
		public string Forename
		{
			get{return lblForename2.Text;}
			set{lblForename2.Text = value.ToString();}
		}
		public string Surname
		{
			get{return lblSurname2.Text;}
			set{lblSurname2.Text = value.ToString();}
		}
		public void saveIndividualAddressDetails(BnbIndividual objIndividual, BnbEditManager em)
		{
			objIndividual.Additional.GCContactUserID = BnbEngine.SessionManager.GetSessionInfo().UserID;
			objIndividual.Additional.RaisersEdgeID=txtRaisersEdgeId.Text;
			BnbAddress newAdd = new BnbAddress(true);
			newAdd.RegisterForEdit(em);
			newAdd.AddressLine1 = txtAddressLine1.Text;
			newAdd.AddressLine2 = txtAddressLine2.Text;
			newAdd.AddressLine3 = txtAddressLine3.Text;
			newAdd.AddressLine4 = txtAddressLine4.Text;
			newAdd.PostCode = txtPostcode.Text;
			newAdd.County = txtCounty.Text;
			newAdd.CountryID = new Guid(drpCountry.SelectedValue.ToString());

            //Capture latitude, longitude values from Google map
            if (HasPermissionToLocateOnMap())
            {
               

                if (showLocation.Latitude.Trim() != "")
                    newAdd.Latitude = Convert.ToSingle(showLocation.Latitude);
                else
                    //Setting null value for validation
                    newAdd.Latitude = Convert.ToSingle(0.0);

                if (showLocation.Longitude.Trim() != "")
                    newAdd.Longitude = Convert.ToSingle(showLocation.Longitude);
                else
                    //Setting null value for validation
                    newAdd.Longitude = Convert.ToSingle(0.0);
            }
			BnbIndividualAddress iaLink = new BnbIndividualAddress(true);
			iaLink.RegisterForEdit(em);
			// link address to indiv
			objIndividual.IndividualAddresses.Add(iaLink);
			newAdd.IndividualAddresses.Add(iaLink);

			// add the telephone if specified
			if (txtTelephone.Text != "")
				newAdd.SetTelephone( objIndividual, txtTelephone.Text);

			// add the mobile, if specified
			if (txtMobile.Text != "")
			{
				BnbContactNumber newMob = new BnbContactNumber(true);
				newMob.RegisterForEdit(em);
				objIndividual.ContactNumbers.Add(newMob);
				newMob.ContactNumberTypeID = BnbConst.ContactNumber_Mobile;
				newMob.ContactNumber = txtMobile.Text.Replace(" ","");
			}

			// add the email, if specified
			if (txtEmail.Text != "")
			{
				BnbContactNumber newEmail = new BnbContactNumber(true);
				newEmail.RegisterForEdit(em);
				objIndividual.ContactNumbers.Add(newEmail);
				newEmail.ContactNumberTypeID = BnbConst.ContactNumber_Email;
				newEmail.ContactNumber = txtEmail.Text;
			}

		
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			cmdFindAddressLine1.Click +=new EventHandler(this.cmdFind_Click);
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		protected void cmdFind_Click(object sender, System.EventArgs e)
		{
			//display the address lookup control and fill in the necessary properties
			
				bnbAddressLookup1.Visible = true;
				bnbAddressLookup1.CountryID = new Guid(drpCountry.SelectedValue);
				bnbAddressLookup1.PostCode = txtPostcode.Text;
				bnbAddressLookup1.Line1 = txtAddressLine1.Text;
				bnbAddressLookup1.PerformSearch();
				bnbAddressLookup1.Visible = true;
			
		}
	
		protected void bnbAddressLookup1_SelectedIndexChanged(object sender, System.EventArgs e)
		{

			txtAddressLine1.Text = bnbAddressLookup1.Line1;
			txtAddressLine2.Text = bnbAddressLookup1.Line2;
			txtAddressLine3.Text = bnbAddressLookup1.Line3;
			txtAddressLine4.Text = bnbAddressLookup1.Town;
			txtCounty.Text = bnbAddressLookup1.County;
			txtPostcode.Text = bnbAddressLookup1.PostCode;
        }

        #region " GMap Properties "

        public string Latitude
        {
            get
            {
                return showLocation.Latitude;
            }
            set
            {
                showLocation.Latitude = value;
            }

        }

        public string Longitude
        {
            get
            {
                return showLocation.Longitude;
            }
            set
            {
                showLocation.Longitude = value;
            }

        }

        #endregion

        /// <summary>
        /// Check permisssion to access the Google map functionality
        /// </summary>
        private bool HasPermissionToLocateOnMap()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_LocateAddressOnMap))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Added newly for role based access
        /// </summary>
        private void DoMapVisibility()
        {
            if (HasPermissionToLocateOnMap())
            {
                showLocation.Visible = true;
                btnShowLocation.Visible = true;
            }
            else
            {
                showLocation.Visible = false;
                btnShowLocation.Visible = false;
            }
        }
    }
}
