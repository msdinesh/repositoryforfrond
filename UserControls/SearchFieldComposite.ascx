<%@ Control Language="c#" AutoEventWireup="True" Codebehind="SearchFieldComposite.ascx.cs" Inherits="Frond.UserControls.SearchFieldComposite" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<tr>
	<td><asp:Button id="cmdEdit" runat="server" Text="Edit" onclick="cmdEdit_Click"></asp:Button>
		<asp:Button id="cmdUpdate" runat="server" Text="Update" onclick="cmdUpdate_Click"></asp:Button>
		<asp:Button id="cmdCancel" runat="server" Text="Cancel" onclick="cmdCancel_Click"></asp:Button>
	</td>
	<td>
		<asp:Label id="lblFieldAlias" runat="server"></asp:Label><asp:DropDownList id="cboField" runat="server" AutoPostBack="True" onselectedindexchanged="cboField_SelectedIndexChanged"></asp:DropDownList></td>
	<td><asp:Label id="lblFilterType" runat="server"></asp:Label><asp:DropDownList id="cboFilterType" runat="server"></asp:DropDownList></td>
	<td><asp:Label id="lblFilterValue" runat="server"></asp:Label><asp:TextBox id="txtTextFilter" runat="server"></asp:TextBox></td>
	<td><asp:DropDownList id="cboListFilter" runat="server"></asp:DropDownList>
		<asp:Label id="lblFieldName" runat="server" Visible="False"></asp:Label>
		<asp:Label id="lblConditionType" runat="server" Visible="False"></asp:Label>
		<asp:Label id="lblEditMode" runat="server" Visible="False"></asp:Label>
		<asp:Label id="lblViewName" runat="server" Visible="False"></asp:Label>
		<asp:Label id="lblDebug" runat="server"></asp:Label></td>
</tr>
