<%@ Control Language="c#" AutoEventWireup="True" Codebehind="NavigatePanel.ascx.cs" Inherits="Frond.UserControls.NavigatePanel" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script language="JavaScript" type="text/javascript">
<!--
	function openClose(id)
	{
		var obj = "";	

		// Check browser compatibility
		if(document.getElementById)
			obj = document.getElementById(id).style;
		else if(document.all)
			obj = document.all[id];
		else if(document.layers)
			obj = document.layers[id];
		else
			return 1;
			
		// Do the magic :)
		if(obj.display == "")
			obj.display = "none";
		else if(obj.display != "none")
			obj.display = "none";
		else
			obj.display = "block";
	}
//-->
</script>
<div class="navigatetop" onClick="openClose('navigatelinkdiv')" style="FONT-WEIGHT: bold; CURSOR: pointer">
	Navigate To ...</div>
<div class="navigatemain" id="navigatelinkdiv" style="DISPLAY:none">
	<asp:Label id="lblNavigateLinks" runat="server"></asp:Label>
</div>
