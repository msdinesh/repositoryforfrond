<%@ Control Language="c#" AutoEventWireup="True" Codebehind="WaitMessagePanel.ascx.cs" Inherits="Frond.UserControls.WaitMessagePanel" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<div id="mainWaitPanel" class="wmpMain">
<div class="wmpInner" style="<%=ExtraStyles%>">
<div class="wmpWaitIcon"><img id="waitImage" src="<%=WaitImagePath%>" /></div>
<span id="waitMessageText">Please Wait ...</span>
</div>
</div>
