namespace Frond.UserControls
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using BonoboEngine;
	using BonoboDomainObjects;
	using BonoboWebControls;
	using BonoboWebControls.DataControls;
	using System.Collections;
	using System.Web.UI;

	/// <summary>
	///		Summary description for NavigationBar.
	/// </summary>
	public partial class NavigationBar : System.Web.UI.UserControl
	{

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion

		#region Private members
		private String				m_RootDomainObject = "";
		private String				m_RootDomainObjectID = "";
		private BnbDomainObject				m_DomainObjectBase = null;

		private BnbOrganisation				m_Organisation = null;
		private BnbEmployer					m_Employer = null;
		private BnbPosition					m_Position = null;
		private BnbRole						m_Role = null;
		private BnbApplication				m_Application = null;
		private BnbIndividual				m_Volunteer = null;

		private Hashtable					m_ControlHashTable = new Hashtable();
        private DataTable m_PositionTable = null;
        private DataTable m_RoleTable = null;

		private String						m_applicationID;
		private String						m_appStatusInfo;
        private bool m_hasValueInPreviousCell = false;
        private int m_navBarStyleID;

		#endregion

		#region Properties

        private void InitialiseNavigationBarStyle()
        {
            DataRow webStyleRow = null;
            webStyleRow = BnbWorkareaManager.GetWebStyleInfoForUser();
            if (webStyleRow != null)
            {
                if (webStyleRow["NavBarStyleID"]!= DBNull.Value)
                    m_navBarStyleID = Convert.ToInt32(webStyleRow["NavBarStyleID"]);
                else
                    throw new ApplicationException("Navigation Bar style was not configured.");
            }
            
        }

		public String RootDomainObject
		{
			get 
			{ 
				if (m_RootDomainObject == "")
					m_RootDomainObject = ViewState[this.ID + "_Root"].ToString();

				return m_RootDomainObject; 
			}
			set 
			{ 
				m_RootDomainObject = value; 
				ViewState[this.ID + "_Root"] = m_RootDomainObject;
			}
		}
		public String RootDomainObjectID
		{
			get { 
				if (m_RootDomainObjectID == "")
					m_RootDomainObjectID = ViewState[this.ID + "_RootID"].ToString();
				return m_RootDomainObjectID; 
				}
			set { 
				m_RootDomainObjectID = value; 
				ViewState[this.ID + "_RootID"] = m_RootDomainObjectID; 
				}
		}

		#endregion

		// this previously public static method of wfm dissappeared so have copied it here
		private string ReplacementForBnbWebFormManagerGetGuidFromQueryString(string domainObjectName)
		{
			return HttpContext.Current.Request.QueryString[domainObjectName];
		}

		#region Overriding methods

		private void doInitialiseControl()
		{
			if(this.RootDomainObject == "") throw new Exception("The RootDomainObject property of the " + this.ID + " control in " + this.Page.ID + " has not been set");
			if(!isNavigationBarToBeDisplayed) return;
			if (ReplacementForBnbWebFormManagerGetGuidFromQueryString(this.RootDomainObject) != null)
			m_DomainObjectBase = BnbWebControlServices.GetDomainObject(this.RootDomainObject,ReplacementForBnbWebFormManagerGetGuidFromQueryString(this.RootDomainObject));
			else
			m_DomainObjectBase = BnbWebControlServices.GetDomainObject(this.RootDomainObject, this.RootDomainObjectID);
            InitialiseNavigationBarStyle();
				switch(m_DomainObjectBase.ClassName)
				{
					case "BnbIndividual":
						m_Volunteer = (BnbIndividual)m_DomainObjectBase;
						if(m_Volunteer.CurrentApplication != null) m_Application = m_Volunteer.CurrentApplication;
						if(m_Application != null && m_Application.PlacedService != null) m_Position = m_Application.PlacedService.Position;
						if(m_Position != null && m_Application.PlacedService != null) m_Role = m_Position.Role;
                        if (m_Role != null) m_Employer = m_Role.Employer;
                        if (m_Employer != null && m_Employer.OrganisationID != System.Guid.Empty) m_Organisation = (BnbOrganisation)BnbWebControlServices.GetDomainObject("BnbOrganisation", m_Employer.OrganisationID);
							break;
					case "BnbApplication":
						m_Application = (BnbApplication)m_DomainObjectBase;
						if(m_Application.Individual != null) m_Volunteer = m_Application.Individual;
						if(m_Application.PlacedService != null)
						{
							m_Position = m_Application.PlacedService.Position;
							if(m_Position.Role != null) 
                                m_Role = m_Position.Role;
						}
                        if (m_Role != null) m_Employer = m_Role.Employer;
                        if (m_Employer != null && m_Employer.OrganisationID != System.Guid.Empty) m_Organisation = (BnbOrganisation)BnbWebControlServices.GetDomainObject("BnbOrganisation", m_Employer.OrganisationID);
							break;
					case "BnbPosition":
						m_Position= (BnbPosition)m_DomainObjectBase;
						if(m_Position.Role != null) m_Role = m_Position.Role;
                        if (m_Role != null) m_Employer = m_Role.Employer;
                        if (m_Employer != null && m_Employer.OrganisationID != System.Guid.Empty) m_Organisation = (BnbOrganisation)BnbWebControlServices.GetDomainObject("BnbOrganisation", m_Employer.OrganisationID);
						if(m_Position.PlacedService != null) 
					    {
						m_Application = m_Position.PlacedService.Application;
						if(m_Position.PlacedService.Application.Individual != null) m_Volunteer = m_Position.PlacedService.Application.Individual;
					    }
						break;
                    case "BnbOrganisation":
                        m_Organisation = (BnbOrganisation)m_DomainObjectBase;
                        break;
                    case "BnbEmployer":
                        m_Employer = (BnbEmployer)m_DomainObjectBase;
                        if (m_Employer.OrganisationID != System.Guid.Empty) m_Organisation = (BnbOrganisation)BnbWebControlServices.GetDomainObject("BnbOrganisation", m_Employer.OrganisationID);
                        m_RoleTable = getDomainObjecListAsDataTable(m_Employer.Roles);
                        if (m_RoleTable != null && m_RoleTable.Rows.Count == 1) m_Role = (BnbRole)m_Employer.Roles[0];
                        if (m_Role != null) m_PositionTable = getDomainObjecListAsDataTable(m_Role.Positions);
                        if (m_PositionTable != null && m_PositionTable.Rows.Count == 1) m_Position = (BnbPosition)m_Role.Positions[0];
                        if (m_Position != null && m_Position.PlacedService != null) m_Application = m_Position.PlacedService.Application;
                        if (m_Application != null) m_Volunteer = m_Application.Individual;
                        break;
                    case "BnbRole":
                        m_Role = (BnbRole)m_DomainObjectBase;
                        if (m_Role.Employer != null) m_Employer = m_Role.Employer;
                        if (m_Employer != null && m_Employer.OrganisationID != System.Guid.Empty) m_Organisation = (BnbOrganisation)BnbWebControlServices.GetDomainObject("BnbOrganisation", m_Employer.OrganisationID);
                        if (m_Role.Positions != null) m_PositionTable = getDomainObjecListAsDataTable(m_Role.Positions);
                        if (m_PositionTable != null && m_PositionTable.Rows.Count == 1) m_Position = (BnbPosition)m_Role.Positions[0];
                        if (m_Position != null && m_Position.PlacedService != null) m_Application = m_Position.PlacedService.Application;
                        if (m_Application != null) m_Volunteer = m_Application.Individual;
                        break;

				}
					
		}

		protected override void CreateChildControls()
		{
			if(!isNavigationBarToBeDisplayed) return;
			doInitialiseControl();
			switch(m_DomainObjectBase.ClassName)
			{

				//show a dropdown list of applications on the individual page
				case "BnbIndividual" : 
                case "BnbEmployer" : 
                case "BnbRole":
                    if (m_Volunteer != null)
				    doCreateApplicationStatusList(m_Volunteer, 
					"BnbApplication",
					"ApplicationPage.aspx?BnbApplication='+this.value",
					"Application");
					break;
				//create a label of the current application	
				case "BnbPosition":
				{
					if (m_Position.PlacedService != null)
						doCreateHyperlink("BnbApplication",
							"ApplicationPage.aspx?BnbApplication=" + m_Position.PlacedService.ApplicationID,
							m_Position.PlacedService.Application.ApplicationOrderFull + " " + StatusForStatusID(m_Position.PlacedService.Application.CurrentStatus.StatusID));
				}
					break;
				case "BnbApplication":
					// if there is only 1 application then create a hyperlink 
					// else create a drop down list.
					if((m_Volunteer.Applications.Count != 1) && (Request.Url.LocalPath.EndsWith("ApplicationPage.aspx")))
					{
						doCreateApplicationStatusList(m_Volunteer, 
							"BnbApplication",
							"ApplicationPage.aspx?BnbApplication='+this.value",
							"Application",true);
					}
					else
					{
					doCreateHyperlink("BnbApplication",
					"ApplicationPage.aspx?BnbApplication=" + m_Application.ID.ToString().ToUpper(),
					m_Application.ApplicationOrderFull + " " + StatusForStatusID(m_Application.CurrentStatus.StatusID));
					}
					break;
			}
            doCreateControlForPlacementCell();
            doCreateControlForOrganisationCell();
            doCreateControlForJobCell();
            doCreateControlForEmployerCell();
			base.CreateChildControls ();
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if(!isNavigationBarToBeDisplayed) return;
			doRenderEntireTable(writer);
		}

		#endregion

		#region Private functions

		private bool isNavigationBarToBeDisplayed
		{
			// edited by idr because InNewMode seemed to disappear
			get { return (!(BnbWebFormManager.InternalPageState == BonoboWebControls.PageState.New )); }
		}

		private bool isPageStateOnQueryStringSetToNew
		{
			get 
			{
				string pagestate = HttpContext.Current.Request.QueryString["PageState"] + "";
				return (pagestate.ToLower() == "new");
			}
		}

		private void doCreateControlForEmployerCountryCell() 
		{
			if(m_Employer == null) return;
			BnbLookupControl bnb = new BnbLookupControl();
			bnb.ID = this.ID + "_EmployerCountry";
			bnb.LookupControlType = LookupControlType.ReadOnlyLabel;
			bnb.DataTextField = "Description";
			bnb.LookupTableName = "lkuCountry";
			bnb.SelectedValue = m_Employer.CountryID;
			Controls.Add(bnb);
			m_ControlHashTable.Add(bnb.ID,bnb);
		}

		private void doCreateControlForApplicationStatusCell() 
		{
			if(m_Application == null) return;
			BnbLookupControl bnb = new BnbLookupControl();
			bnb.ID = this.ID + "_BnbApplication";
			bnb.LookupTableName = "vlkuBonoboStatusFull";
			bnb.DataTextField = "Description";
			Controls.Add(bnb);
			m_ControlHashTable.Add(bnb.ID,bnb);
		}

		private void doCreateControlForOrganisationCell()
        {
            if (m_Organisation != null && m_Organisation.OrganisationName != "(None)")
                doCreateHyperlink("BnbOrganisation",
                    "OvsOrganisationPage.aspx?BnbOrganisation=" + m_Organisation.ID.ToString().ToUpper(),
                    m_Organisation.OrganisationName);
            else
                doCreateLabel("BnbOrganisation", "");
        }

        private void doCreateControlForEmployerCell()
        {
            if (m_DomainObjectBase.ClassName == "BnbEmployer")
            {
                doCreateLabel("BnbEmployer", m_Employer.EmployerName);
                return;
            }
            if (m_Employer != null)
            {
                doCreateHyperlink("BnbEmployer",
                    "EmployerPage.aspx?BnbEmployer=" + m_Employer.ID.ToString().ToUpper(),
                    m_Employer.EmployerName);
                return;
            }
            doCreateLabel("BnbEmployer", "");

        }

        private void doCreateControlForJobCell()
        {
            if (m_DomainObjectBase.ClassName == "BnbRole")
            {
                doCreateLabel("BnbRole", m_Role.RoleName);
                return;
            }
            if (m_Role != null)
            {
                doCreateHyperlink("BnbRole",
                    "JobPage.aspx?BnbEmployer=" + m_Employer.ID.ToString().ToUpper() + "&BnbRole=" + m_Role.ID.ToString().ToUpper(),
                    m_Role.RoleName);

                if (m_ControlHashTable[this.ID + "_BnbEmployer"] is DropDownList)
                    ((DropDownList)m_ControlHashTable[this.ID + "_BnbEmployer"]).SelectedValue = m_Role.EmployerID.ToString();
                return;
            }

            if (m_RoleTable != null)
            {
                doCreateDropDownList(m_RoleTable,
                    "BnbRole",
                    "JobPage.aspx?BnbEmployer=" + m_Employer.ID.ToString().ToUpper() + "&BnbRole='+ this.value",
                    "RoleName");
                return;
            }

            doCreateLabel("BnbRole", "");
        }


		private void doCreateControlForPlacementCell() 
		{
			if(m_DomainObjectBase.ClassName == "BnbPosition") 
			{
				doCreateLabel("BnbPosition",m_Position.FullPlacementReference);
				return;
			}
			if(m_Position != null) 
			{
				doCreateHyperlink("BnbPosition",
					"PlacementPage.aspx?BnbPosition=" + m_Position.ID.ToString().ToUpper(),
					m_Position.FullPlacementReference);
					
				if(m_ControlHashTable[this.ID + "_BnbRole"] is DropDownList) 
					((DropDownList)m_ControlHashTable[this.ID + "_BnbRole"]).SelectedValue = m_Position.RoleID.ToString();
				return;
			}
            if (m_PositionTable != null)
            {
                doCreateDropDownList(m_PositionTable,
                    "BnbPosition",
                    "PlacementPage.aspx?BnbRole=" + m_Role.ID.ToString().ToUpper() + "&BnbPosition=' + this.value ",
                    "FullPlacementReference");
                return;
            }
			doCreateLabel("BnbPlacement", "");
		}

		private void doCreateApplicationStatusList(BnbIndividual indv, string domainObjectClassName, string redirectUrl, string dataTextField)
		{
			DropDownList ddl = new DropDownList();
			ddl.ID = this.ID + "_" + domainObjectClassName;
			ddl.DataTextField = dataTextField;
			ddl.DataValueField = "ID";
			ddl.Attributes.Add("onchange","javascript:location.href='" + redirectUrl);
			ddl.Items.Insert(0,new ListItem("[Select...]",""));
			indv.Applications.Sort("ApplicationOrder", true);

			foreach(BnbApplication app in indv.Applications)
			{
				if(app.PlacedService != null)
					ddl.Items.Add(new ListItem(app.ApplicationOrderFull + " " + StatusForStatusID(app.CurrentStatus.StatusID)
						+ " " + app.PlacedService.Position.FullPlacementReference.ToString(),
						app.ID.ToString()));
				else
				ddl.Items.Add(new ListItem(app.ApplicationOrderFull + " " + StatusForStatusID(app.CurrentStatus.StatusID),app.ID.ToString()));
			}
			Controls.Add(ddl);
			m_ControlHashTable.Add(ddl.ID,ddl);
		}
		private void doCreateApplicationStatusList(BnbIndividual indv, string domainObjectClassName, string redirectUrl, string dataTextField, bool SelectString)
		{
			DropDownList ddl = new DropDownList();
			ddl.ID = this.ID + "_" + domainObjectClassName;
			ddl.DataTextField = dataTextField;
			ddl.DataValueField = "ID";
			ddl.Attributes.Add("onchange","javascript:location.href='" + redirectUrl);
			if (SelectString == true)
				m_applicationID = ReplacementForBnbWebFormManagerGetGuidFromQueryString("BnbApplication");
				m_appStatusInfo = ApplicationStatusForApplicationID(m_applicationID);
				ListItem newitem;
				
			indv.Applications.Sort("ApplicationOrder", true);
			foreach(BnbApplication app in indv.Applications)
			{
				if(m_applicationID.ToString().ToLower() != app.ID.ToString())
				{
					newitem = new ListItem(app.ApplicationOrderFull + " " + StatusForStatusID(app.CurrentStatus.StatusID),app.ID.ToString());
					ddl.Items.Add(newitem);
				}
				else
				{
					newitem = new ListItem(app.ApplicationOrderFull + " " + StatusForStatusID(app.CurrentStatus.StatusID),app.ID.ToString());
					newitem.Selected = true;
					ddl.Items.Add(newitem);
				}
			}
			Controls.Add(ddl);
			m_ControlHashTable.Add(ddl.ID,ddl);
		}
		private string ApplicationStatusForApplicationID(string id)
		{
			BnbApplication app = BnbApplication.Retrieve(new System.Guid(id));
			return app.ApplicationOrderFull + " " + StatusForStatusID(app.CurrentStatus.StatusID);	
		}


		private string StatusForStatusID(int statusID)
		{
			return BnbEngine.LookupManager.GetLookupItemDescription("vlkuStatusFull", statusID);
		}

		private void doCreateHyperlink(string domainObjectClassName, string redirectUrl, string linkText) 
		{
			HyperLink hlk = new HyperLink();
			hlk.ID = this.ID + "_" + domainObjectClassName;
			hlk.NavigateUrl = "../EditPages/" + redirectUrl;
			hlk.Text = HttpUtility.HtmlEncode(linkText);
			Controls.Add(hlk);
			m_ControlHashTable.Add(hlk.ID,hlk);
		}

		private void doCreateLabel(string domainObjectClassName, string text) 
		{
			Label lbl = new Label();
			lbl.ID = this.ID + "_" + domainObjectClassName;
			lbl.Text = HttpUtility.HtmlEncode(text);
			Controls.Add(lbl);
			m_ControlHashTable.Add(lbl.ID,lbl);
		}

		private void doRenderEntireTable(HtmlTextWriter writer) 
		{
			writer.AddAttribute("cellSpacing","0");
			writer.AddAttribute("cellPadding","3");
			writer.AddAttribute("border","0");
			writer.AddAttribute("class","navigationbar");
			writer.RenderBeginTag("table");
			writer.RenderBeginTag("tbody");
			doRenderFirstRow(writer);
			writer.RenderEndTag();
			writer.RenderEndTag();
		}

		private void doRenderFirstRow(HtmlTextWriter writer) 
		{
			writer.RenderBeginTag("tr");
			writer.AddAttribute("class","STATUS_BAR_HEADER");
			writer.AddAttribute("align","left");
			writer.AddAttribute("colspan","5");
			writer.RenderBeginTag("td");
			writer.AddAttribute("cellSpacing","0");
			writer.AddAttribute("cellPadding","0");
			writer.AddAttribute("border","0");
			writer.RenderBeginTag("table");
			writer.RenderBeginTag("tbody");
			writer.RenderBeginTag("tr");
			writer.AddAttribute("class","STATUS_BAR_HEADER");
			writer.AddAttribute("align","left");
			writer.RenderBeginTag("td");

                doRenderCells(writer);
				writer.RenderEndTag();
				writer.RenderEndTag();
				writer.RenderEndTag();
				writer.RenderEndTag();
				writer.RenderEndTag();
				writer.RenderEndTag();
		}

        private void doRenderCells(HtmlTextWriter writer)
        {
   
               if (m_navBarStyleID == BnbConst.NavBar_Frond)
               {
                    doRenderIndividualNameCell(writer);
                    doRenderVolunteerReferenceCell(writer);
                    doRenderApplicationStatusCell(writer);

                    if (this.RootDomainObject != "BnbIndividual")
                    {
                        doRenderPlacementReferenceCell(writer);
                        doRenderJobCell(writer);
                        doRenderEmployerCell(writer);
                        doRenderOrganisationCell(writer);
                    }
               }
               else if (m_navBarStyleID ==BnbConst.NavBar_Starfish)
               {
                    if (this.RootDomainObject != "BnbIndividual")
                    {
                        doRenderOrganisationCell(writer);
                        doRenderEmployerCell(writer);
                        doRenderJobCell(writer);
                        doRenderPlacementReferenceCell(writer);
                    }
                    doRenderApplicationStatusCell(writer);
                    doRenderVolunteerReferenceCell(writer);
                    doRenderIndividualNameCell(writer);
               }
               else
               {
                    throw new ApplicationException("Navigation Bar style is Invalid.");
               }
          
        }

		private void doRenderVolunteerReferenceCell(HtmlTextWriter writer) 
		{
            if (m_Volunteer != null)
            {
                writer.Write(" (" + (m_Volunteer.RefNo) + ")");
                m_hasValueInPreviousCell = true;
            }
		}

		private void doRenderPlacementReferenceCell(HtmlTextWriter writer) 
		{
            if (m_ControlHashTable.Contains(this.ID + "_BnbPosition"))
            {
                if(m_hasValueInPreviousCell) writer.Write(" | ");
				((WebControl)m_ControlHashTable[this.ID + "_BnbPosition"]).RenderControl(writer);
                m_hasValueInPreviousCell = true;
			}
			}

		private void doRenderIndividualNameCell(HtmlTextWriter writer) 
		{
			if(m_Volunteer != null)
			{
				if(RootDomainObject== "BnbIndividual" && Request.Url.LocalPath == "/Frond/EditPages/IndividualPage.aspx")
					doCreateLabel("BnbIndividual",m_Volunteer.FullName.ToString());
				else
				{
					doCreateHyperlink("BnbIndividual",
						"IndividualPage.aspx?BnbIndividual=" + m_Volunteer.ID.ToString().ToUpper(),
						m_Volunteer.FullName.ToString());
				}
				((WebControl)m_ControlHashTable[this.ID + "_BnbIndividual"]).RenderControl(writer);
                m_hasValueInPreviousCell = true;
			}	
		}

		private void doRenderEmployerCell(HtmlTextWriter writer) 
		{
            if (m_ControlHashTable.Contains(this.ID + "_BnbEmployer"))
            {
                if(m_hasValueInPreviousCell) writer.Write("&nbsp;|&nbsp;");
				((WebControl)m_ControlHashTable[this.ID + "_BnbEmployer"]).RenderControl(writer);
                m_hasValueInPreviousCell = true;
			}
		}

        private void doRenderOrganisationCell(HtmlTextWriter writer)
        {
            if (m_Organisation != null)
            {
                if(m_hasValueInPreviousCell) writer.Write("&nbsp;|&nbsp;");
                ((WebControl)m_ControlHashTable[this.ID + "_BnbOrganisation"]).RenderControl(writer);
                m_hasValueInPreviousCell = true;
            }
        }

        private void doRenderJobCell(HtmlTextWriter writer)
        {
            if(m_ControlHashTable.Contains(this.ID + "_BnbRole"))
            {
                if (m_hasValueInPreviousCell) writer.Write("&nbsp;|&nbsp;");
                ((WebControl)m_ControlHashTable[this.ID + "_BnbRole"]).RenderControl(writer);
                m_hasValueInPreviousCell = true;
            }
        }

        private void doRenderApplicationStatusCell(HtmlTextWriter writer)
        {
            if (m_ControlHashTable.Contains(this.ID + "_BnbApplication"))
            {
                if (m_hasValueInPreviousCell) writer.Write("&nbsp;|&nbsp;");
                ((WebControl)m_ControlHashTable[this.ID + "_BnbApplication"]).RenderControl(writer);
                m_hasValueInPreviousCell = true;
            }
        }

        private void doCreateDropDownList(DataTable domainObjectListTable, string domainObjectClassName, string redirectUrl, string dataTextField)
        {
            DropDownList ddl = new DropDownList();
            ddl.ID = this.ID + "_" + domainObjectClassName;
            ddl.Attributes.Add("onchange", "javascript:location.href='" + redirectUrl);
            ddl.DataTextField = dataTextField;
            ddl.DataValueField = "ID";
            ddl.Items.Insert(0, new ListItem("[Select...]", ""));
            //sorts the table before populating the control
            foreach (DataRow row in domainObjectListTable.Select(null, dataTextField))
            {
                ddl.Items.Add(new ListItem(row[dataTextField].ToString(), row["ID"].ToString().ToUpper()));
            }
            Controls.Add(ddl);
            m_ControlHashTable.Add(ddl.ID, ddl);
        }

        private DataTable getDomainObjecListAsDataTable(BnbDomainObjectList domainObjectList)
        {
            return domainObjectList.AsDataSet().Tables["DomainObjectData"];
        }

		#endregion
	}
}
