namespace Frond.UserControls
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using BonoboEngine.Query;
	using BonoboEngine;

	//TODO:
	// - caching of view meta-data
	// - keep track of mode (text, etc)
	// - edit mode event driven
	// - sort out edit logic

	/// <summary>
	///		Summary description for SearchFieldComposite.
	/// </summary>
	public partial class SearchFieldComposite : System.Web.UI.UserControl
	{

		private BnbQueryDataSet.ColumnInfoDataTable m_columnInfo;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			lblDebug.Text = "Debug:" + this.BindViewName; 
			
			
		}

		

		private void PopulateFieldList()
		{
			cboField.Items.Clear();
			foreach(BnbQueryDataSet.ColumnInfoRow rowLoop in this.GetColumnInfo())
			{
				if (!rowLoop.IsQueryKey && (!rowLoop.IsForeignKey 
					 || BnbEngine.LookupManager.IsLookup(rowLoop.ForeignKeyTable)))
					cboField.Items.Add(new ListItem(rowLoop.FieldUserAlias, rowLoop.ColumnName));
			}
			if (lblFieldName.Text != "")
				cboField.Items.FindByValue(lblFieldName.Text).Selected = true;

		}

		private void PopulateFilterTypeList()
		{
			string selectedField = cboField.SelectedValue;
			BnbQueryDataSet.ColumnInfoRow fieldMeta = (BnbQueryDataSet.ColumnInfoRow)this.GetColumnInfo().Rows.Find(selectedField);

			// depends on type...
			if (fieldMeta.IsForeignKey)
			{
				cboFilterType.Items.Clear();
				cboFilterType.Items.Add(new ListItem("Is One Of","isOneOf"));
				cboFilterType.Items.Add(new ListItem("Is Empty","isEmpty"));
			}
			else
			{
				if (fieldMeta.FieldTypeCode == 1 || fieldMeta.FieldTypeCode == 7)
				{
					cboFilterType.Items.Clear();
					cboFilterType.Items.Add(new ListItem("Starts With","startsWith"));
					cboFilterType.Items.Add(new ListItem("Contains","contains"));
					cboFilterType.Items.Add(new ListItem("Is","is"));
					cboFilterType.Items.Add(new ListItem("Is Empty","is empty"));
				}
				if (fieldMeta.FieldTypeCode == 2)
				{
					cboFilterType.Items.Clear();
					cboFilterType.Items.Add(new ListItem("Is Greater Than","isGreaterThan"));
					cboFilterType.Items.Add(new ListItem("Is Less Than","isLessThan"));
					cboFilterType.Items.Add(new ListItem("Is Empty","is empty"));
				}
			}
			// select value if possible?
			if (lblFilterType.Text != "")
			{
				ListItem findItem = cboFilterType.Items.FindByText(lblFilterType.Text);
				if (findItem != null)
					findItem.Selected = true;

			}
		}

		private void ShowFilterControls()
		{
			string selectedField = cboField.SelectedValue;
			BnbQueryDataSet.ColumnInfoRow fieldMeta = (BnbQueryDataSet.ColumnInfoRow)this.GetColumnInfo().Rows.Find(selectedField);

			txtTextFilter.Visible = false;
			cboListFilter.Visible = false;

			// depends on type...
			if (fieldMeta.IsForeignKey)
			{
				BnbLookupDataTable lookup = BnbEngine.LookupManager.GetLookup(fieldMeta.ForeignKeyTable);
				cboListFilter.Visible = true;
				cboListFilter.Items.Clear();
				foreach(BnbLookupRow rowLoop in lookup.Rows)
				{
					if (!rowLoop.Exclude)
						cboListFilter.Items.Add(new ListItem(rowLoop.Description, rowLoop.ID));
				}
				// attempt to select
				ListItem findItem = cboListFilter.Items.FindByValue(lblFilterValue.Text);
				if (findItem != null)
					findItem.Selected = true;
			}
			else
			{
				if (fieldMeta.FieldTypeCode == 1 || fieldMeta.FieldTypeCode == 7)
				{
					txtTextFilter.Visible = true;
					txtTextFilter.Text = lblFilterValue.Text;
				}
				if (fieldMeta.FieldTypeCode == 2)
				{
					txtTextFilter.Visible = true;
					txtTextFilter.Text = lblFilterValue.Text;
				}
			}


		}

		public string BindEditMode
		{
			get
			{
				if (lblEditMode.Text == "")
					return false.ToString();
				else
					return lblEditMode.Text;
			}
			set
			{
				bool tempValue;
				if (value == "True")
					tempValue = true;
				else
					tempValue = false;
				lblEditMode.Text = tempValue.ToString();

				if (tempValue)
				{
					this.PopulateFieldList();
					this.PopulateFilterTypeList();
					this.ShowFilterControls();

				}
			}
		}

		private bool EditMode
		{
			set{lblEditMode.Text = value.ToString();}
			get{return (BindEditMode == "True");}
		}

		public string BindFieldName
		{
			get{return lblFieldName.Text;}
			set{lblFieldName.Text = value;}
		}

		public string BindFieldAlias
		{
			get{return lblFieldAlias.Text;}
			set{lblFieldAlias.Text = value;}
		}

		public string BindConditionType
		{
			get{return lblConditionType.Text;}
			set{lblConditionType.Text = value;}
		}

		public string BindFilterType
		{
			get{return lblFilterType.Text;}
			set{lblFilterType.Text = value;}
		}

		public string BindFilterValue
		{
			get{return lblFilterValue.Text;}
			set{
				lblFilterValue.Text = value;
				
			}
		}

		public string BindViewName
		{
			get{return lblViewName.Text;}
			set{lblViewName.Text = value;}
		}

		private BnbQueryDataSet.ColumnInfoDataTable GetColumnInfo()
		{
			if (m_columnInfo == null)
			{
				BnbQuery tempQuery = new BnbQuery(this.BindViewName);
				BnbQueryDataSet queryMeta = tempQuery.GetMetaDataOnly();
				m_columnInfo = queryMeta.ColumnInfo;
			}
			return m_columnInfo;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.PreRender += new System.EventHandler(this.SearchFieldComposite_PreRender);

		}
		#endregion

		protected void cmdEdit_Click(object sender, System.EventArgs e)
		{
			this.EditMode = true;
			this.PopulateFieldList();
			this.PopulateFilterTypeList();
			this.ShowFilterControls();
		}

		protected void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.EditMode = false;
		}

		protected void SearchFieldComposite_PreRender(object sender, EventArgs e)
		{
			bool currentMode = this.EditMode;
			lblFieldAlias.Visible = !currentMode;
			cboField.Visible = currentMode;
			lblFilterType.Visible = !currentMode;
			lblFilterValue.Visible = !currentMode;
			cboFilterType.Visible = currentMode;
			if (!currentMode)
			{
				
				cboListFilter.Visible = currentMode;
				txtTextFilter.Visible = currentMode;
			}

			cmdEdit.Visible = !currentMode;
			cmdUpdate.Visible = currentMode;
			cmdCancel.Visible = currentMode;

			

		}

		protected void cmdUpdate_Click(object sender, System.EventArgs e)
		{
			lblFilterValue.Text = txtTextFilter.Text;
			lblFieldName.Text = cboField.SelectedItem.Value;
			lblFieldAlias.Text = cboField.SelectedItem.Text;
			this.EditMode = false;
		}

		protected void cboField_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.PopulateFilterTypeList();
			this.ShowFilterControls();
		}
	}
}
