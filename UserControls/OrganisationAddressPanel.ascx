<%@ Register TagPrefix="cc1" Namespace="BonoboWebControls.ServicesControls" Assembly="BonoboWebControls" %>
<%@ Control Language="c#" AutoEventWireup="True" Codebehind="OrganisationAddressPanel.ascx.cs" Inherits="Frond.UserControls.OrganisationAddressPanel" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register Src="GShowLocation.ascx" TagName="GShowLocation" TagPrefix="uc1" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<!-- usercontrol should be used inside a 2-column table -->
<TR>
	<TD class="label">Country</TD>
	<TD>
		<asp:DropDownList id="uxCountryID" runat="server" Width="224px"></asp:DropDownList></TD>
</TR>
<TR>
	<TD class="label">Postcode / ZIP</TD>
	<TD><asp:textbox id="uxPostcode" runat="server" Width="176px"></asp:textbox><asp:button id="uxPostcodeSearch" runat="server" Text="Find" onclick="uxPostcodeSearch_Click"></asp:button><cc1:bnbaddresslookupcomposite id="uxAddressLookup" CssClass="lookupcontrol" ListBoxRows="6" runat="server" Width="300px"
			Visible="False" AccountCode="vso1111111" LicenseKey="XH57-MT97-CZ47-GH56" PasswordCapscan="492267" RegistrationCodeCapscan="00003304" AutoPostBack="True"></cc1:bnbaddresslookupcomposite></TD>
</TR>
<TR>
	<TD class="label">Address Line 1</TD>
	<TD><asp:textbox id="uxAddressLine1" runat="server" Width="224px"></asp:textbox>
		<asp:Button id="uxAddressLine1Search" runat="server" Text="Find"></asp:Button>
		                        
</TD>
</TR>
<TR>
	<TD class="label">Address Line 2</TD>
	<TD><asp:textbox id="uxAddressLine2" runat="server" Width="224px"></asp:textbox></TD>
</TR>
<TR>
	<TD class="label">Address Line 3</TD>
	<TD><asp:textbox id="uxAddressLine3" runat="server" Width="224px"></asp:textbox></TD>
</TR>
<TR>
	<TD class="label">Postal Town / Line 4</TD>
	<TD><asp:textbox id="uxAddressLine4" runat="server" Width="224px"></asp:textbox></TD>
</TR>
<TR>
	<TD class="label">County / State</TD>
	<TD><asp:textbox id="uxCounty" runat="server" Width="224px"></asp:textbox></TD>
</TR>
<TR>
	<TD class="label"></TD>
	<TD><input id="btnShowLocation" runat="Server" type="button" onclick="showLocation()" value="Show on map" /></TD>
</TR>
<TR>
	<TD class="label">
		<P>&nbsp;Main Telephone</P>
	</TD>
	<TD><asp:textbox id="uxTelephone" runat="server" Width="224px"></asp:textbox></TD>
</TR>

<uc1:GShowLocation ID="showLocation" runat="server" />

