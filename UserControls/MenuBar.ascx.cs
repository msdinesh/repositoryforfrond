namespace Frond.UserControls
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using BonoboEngine;
	using BonoboWebControls;
    using BonoboDomainObjects;

	/// <summary>
	///		Summary description for MenuBar.
	/// </summary>
	public partial class MenuBar : System.Web.UI.UserControl
	{

		private string m_virtualroot = "";
		private string m_menuBarCssClass = "To Be Determined";
		private string m_appMode = "To Be Determined";
		private string m_iconFilename = null;
		private string m_virtualApplicationName = null;
		private int m_optionalFindParameter = 0;
		protected System.Web.UI.WebControls.HyperLink hlIconHelp;
		private int m_overrideWebStyleID = 0;
      

     

		protected void Page_Load(object sender, System.EventArgs e)
		{
			this.WorkOutAppMode();
			string strHelpURL = getHelpRedirect();

            h1IconChangePassword.ImageUrl = this.VirtualRoot + "/Images/myaccount24silver.gif";
            hlChangePassword.NavigateUrl = this.VirtualRoot + "/ChangePasswordPage.aspx";
            h1IconChangePassword.Visible = hlChangePassword.Visible = (Convert.ToBoolean(Session["backdoor"]) == true);
	
			hlHelp.NavigateUrl = strHelpURL;
			hlIconHelp.ImageUrl = this.VirtualRoot + "/Images/help24silver.gif";
			hlIconHelp.NavigateUrl = strHelpURL;
            // remove dummy design-time text
            hlIconHelp.Text = "";

            if (!(IsPostBack))
            {
                if (BnbEngine.SessionManager.HasBonoboSessionStarted())
                {
                    if (!BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_Wendy))
                        pnlQuickLinks.Visible = false;
                    else
                        pnlQuickLinks.Visible = true;
                }
                else
                    pnlQuickLinks.Visible = false;
                
            }
		}

		private void InitialiseWebStyleInfo()
		{
			DataRow webStyleRow = null;
			if (this.OverrideWebStyleID == 0)
				webStyleRow = BnbWorkareaManager.GetWebStyleInfoForUser();
			else
				webStyleRow = BnbWorkareaManager.GetWebStyleInfo(this.OverrideWebStyleID);

			if (webStyleRow != null)
			{
				m_iconFilename = webStyleRow["IconFilename"].ToString();
				m_virtualApplicationName = webStyleRow["VirtualApplicationName"].ToString();
			}
			if (m_iconFilename == null)
				m_iconFilename = "unknown";
			if (m_virtualApplicationName == null)
				m_virtualApplicationName = "unknown";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion



		public string VirtualRoot 
		{
			get
			{
				if(m_virtualroot != "") return m_virtualroot;
				m_virtualroot = HttpRuntime.AppDomainAppVirtualPath;
				if (m_virtualroot=="/")
					m_virtualroot="";
				return m_virtualroot;
			}
		}

		public string AppMode
		{
			get
			{
				return m_appMode;
			}
		}

		/// <summary>
		/// The menu bar has its CssClass dynamically set at run time
		/// this isn't really to do with the Style System (which sets the css file for the whole page)
		/// its to do with having different styles for 'Live' versus 'Training'
		/// </summary>
		public string MenuBarCssClass
		{
			get
			{
				return m_menuBarCssClass;
			}
		}

		public string IconFilename
		{
			get
			{
				if (m_iconFilename == null)
					this.InitialiseWebStyleInfo();
				return m_iconFilename;

			}
		}

		public string VirtualApplicationName
		{
			get
			{
				if (m_virtualApplicationName == null)
					this.InitialiseWebStyleInfo();
				return m_virtualApplicationName;
			}
		}
		
		/// <summary>
		/// Normally the MenuBar will use the WebStyleID of the current user to render.
		/// Sometimes however the WebStyleID will need to be set manually -
		/// e.g. when there is no user logged in. Set this property to do that.
		/// </summary>
		public int OverrideWebStyleID
		{
			get{return m_overrideWebStyleID;}
			set{m_overrideWebStyleID = value;}
		}
		

		/// <summary>
		/// Whether to show the 'Training' header or not depends on which database
		/// we are connected to.
		/// This routine asks BonoboEngine what sort of database we are using and caches
		/// the result.
		/// It is then used to set the CSS class and app mode variables.
		/// </summary>
		private void WorkOutAppMode()
		{
			if (Cache["AppMode"] == null )
			{
				bool isTestDatabase = BnbEngine.SessionManager.GetConfigurationInfo().IsTestDatabase;
				if (isTestDatabase)
					Cache["AppMode"] = "Training";
				else
					Cache["AppMode"] = "Live";
			}

			if ((string)Cache["AppMode"] == "Training")
			{
				m_appMode = "(Training)";
				m_menuBarCssClass = "mnMenuBarTraining";
			}
			else
			{
				m_appMode = "";
				m_menuBarCssClass = "mnMenuBar";
			}
		}
		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			//renders the appropriate page title on the browser
			writer.Write("<script language=\"JavaScript\">");
			writer.Write("document.title=\""+BnbWebFormManager.PageTitle+"\";");
			writer.Write("</script>");
			base.Render (writer);
		}

		public string CurrentPageName
		{
			get
			{
				string requestPath = Page.Request.Path;
				string rootPath = Page.Request.ApplicationPath;
				if (requestPath.StartsWith(rootPath))
					requestPath=requestPath.Substring(rootPath.Length);
				if (requestPath.StartsWith("/"))
					requestPath=requestPath.Substring(1);
				return requestPath;
			}
		}

		
		public void SetOptionalFindParameter(int findOption)
		{
			m_optionalFindParameter = findOption;
		}

		public string OptionalFindParameter
		{
			get
			{
				if (m_optionalFindParameter == 0)
					return "";
				else
					return String.Format("?FindTypeID={0}", m_optionalFindParameter);
			}
		}


		public string getHelpRedirect()
		{ 
			// a vitual path is required
			string xmlPath;
			xmlPath = "~/Xml/HelpRedirect.xml";
			

			string linkTarget = "";
			DataTable helpRedirectTable = this.GetMetaData(xmlPath).Tables["Redirect"];
			DataRow[] searchArray1 = helpRedirectTable.Select("CallingPage = '" + CurrentPageName + "'");
			if (searchArray1.Length > 0)
			{
				DataRow helpRedirectRow = searchArray1[0];
				linkTarget = helpRedirectRow["HelpPage"].ToString();
			}
			else
			{
				DataRow[] searchArray2 = helpRedirectTable.Select("CallingPage = 'Menu.aspx'");
				if (searchArray2.Length > 0)
				{
					DataRow helpRedirectRow = searchArray2[0];
					linkTarget = helpRedirectRow["HelpPage"].ToString();
				}
			}
			return linkTarget; 
		}

		private DataSet GetMetaData(string xmlPath)
		{
			DataSet helpMeta = new DataSet();
			helpMeta.ReadXml(Request.MapPath(xmlPath));
			return helpMeta;
		}

	}
}
