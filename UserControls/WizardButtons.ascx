<%@ Control Language="c#" AutoEventWireup="True" Codebehind="WizardButtons.ascx.cs" Inherits="Frond.UserControls.WizardButtons" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script type="text/javascript"> 
    function ConfirmCancel() 
    { 
        if (confirm('Are you sure you want to exit?'))
        return true;
        else
        return false;        
    }   
</script> 
<DIV class="wizardheader">
	<TABLE id="Table1" style="MARGIN: 0px" cellSpacing="0" cellPadding="0" width="100%" border="0">
		<!-- special cell layout so that next button renders before cancel button -->
		<!-- this makes sure that the Enter key actives Next instead of cancel -->
		<TR>
			<td colspan="2" width="70%" height="0px"></td>
			<td width="30%" rowspan="2" align="left" valign="bottom">
				<asp:button id="buttonFinish" Text="Finish" runat="server" onclick="buttonFinish_Click"></asp:button>
				<asp:Button id="buttonNext" Text="Next >>" runat="server" onclick="buttonNext_Click"></asp:Button>
			</td>
		</TR>
		<tr>
			<TD align="right" width="30%">
				<asp:Button id="buttonPrev" Text="<< Prev" runat="server" onclick="buttonPrev_Click" CausesValidation="false"></asp:Button></TD>
			<TD align="center" width="40%">
				<asp:button id="buttonCancel" Text="Cancel" runat="server" CausesValidation="False" onclick="buttonCancel_Click"  OnClientClick ="return ConfirmCancel()">
				</asp:button></TD>
		</tr>
	</TABLE>
</DIV>
<asp:Label id="lblHiddenCurrentPanel" runat="server" Visible="False"></asp:Label>
