using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboWebControls;

namespace Frond.UserControls
{
    public partial class MergeHeader : System.Web.UI.UserControl
    {
        protected System.Web.UI.WebControls.Image Image1;
		protected System.Web.UI.WebControls.Label Label1;

		protected System.Web.UI.WebControls.HyperLink HyperLink1;
		protected System.Web.UI.WebControls.HyperLink HyperLink2;
		protected System.Web.UI.WebControls.HyperLink HyperLink3;
		protected System.Web.UI.WebControls.HyperLink HyperLink4;

		private string m_virtualrool = "";

		public string virtualroot 
		{
			get
			{
				if(m_virtualrool != "") return m_virtualrool;
				string hosturl = System.Configuration.ConfigurationSettings.AppSettings["hosturl"];
				hosturl = (hosturl == null || hosturl == "") ? "starfish.vsoint.org" : hosturl;
				HttpRequest request = HttpContext.Current.Request;
				string url = request.Url.ToString();
				return m_virtualrool = String.Format("http{0}://{1}{2}", (url.IndexOf(hosturl)>0) ? "s" : "",request.Url.Host, request.ApplicationPath);
			}
		}
		
		public string appMode = "To Be Determined";
		protected BonoboWebControls.DataGrids.BnbDataGridForQueryDataSet BnbDataGridForQueryDataSet1;
		protected System.Web.UI.WebControls.HyperLink Hyperlink1;
		protected BonoboWebControls.DataGrids.BnbDataGridForQueryDataSet BnbDataGridForQueryDataSet2;
		public string cssClass = "To Be Determined";

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			this.WorkOutAppMode();
		//	hlHelp.NavigateUrl = getHelpRedirect();

            // TPT amended - Home link will redirect the Merge/Bulkmerge tool as per the request.
            string currentURL = this.Request.CurrentExecutionFilePath.ToString();
            if (currentURL.IndexOf("frmMergeWizard") > -1)
                pnlMergeHome.Visible = true;
            if (currentURL.IndexOf("frmBulkMerge") > -1)
                pnlBulkMerge.Visible = true;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		/// <summary>
		/// Whether to show the 'Training' header or not depends on which database
		/// we are connected to.
		/// This routine asks BonoboEngine what sort of database we are using and caches
		/// the result.
		/// It is then used to set the CSS class and app mode variables.
		/// </summary>
		private void WorkOutAppMode()
		{
			if (Cache["AppMode"] == null )
			{
				bool isTestDatabase = BnbEngine.SessionManager.GetConfigurationInfo().IsTestDatabase;
				if (isTestDatabase)
					Cache["AppMode"] = "Training";
				else
					Cache["AppMode"] = "Live";
			}

			if ((string)Cache["AppMode"] == "Training")
			{
				appMode = "Training";
				cssClass = "StarfishHeaderTraining";
			}
			else
			{
				appMode = "";
				cssClass = "StarfishHeader";
			}
		}
		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			//renders the appropriate page title on the browser
			writer.Write("<script language=\"JavaScript\">");
			writer.Write("document.title=\"Bonobo Merge\";");
			writer.Write("</script>");
			base.Render (writer);
		}

		public string CurrentPageName
		{
			get
			{
				string requestPath = Page.Request.Path;
				string rootPath = Page.Request.ApplicationPath;
				if (requestPath.StartsWith(rootPath))
					requestPath=requestPath.Substring(rootPath.Length);
				if (requestPath.StartsWith("/"))
					requestPath=requestPath.Substring(1);
				return requestPath;
			}
		}

		public string CustomLinkText
		{
			get{return hlCustom.Text;}
			set{hlCustom.Text = value;}
		}

		public string CustomLinkNavigateURL
		{
			get{return hlCustom.NavigateUrl;}
			set{hlCustom.NavigateUrl = value;}
		}


		public string getHelpRedirect()
		{ 
			// a vitual path is required
			string xmlPath;
//			string dir = "../";
//			string level = "";
//			foreach (char ch in CurrentPageName)
//			{
//				if ((ch == '/') || (ch == System.Convert.ToChar("\\")))
//					level += dir;
//			}
//
//			xmlPath = level + "Xml/HelpRedirect.xml";
			xmlPath = "~/Xml/HelpRedirect.xml";
			this.BnbDataGridForQueryDataSet3.MetaDataXmlUrl = (xmlPath);

			string linkTarget = "";
			DataTable helpRedirectTable = this.GetMetaData().Tables["Redirect"];
			try
			{
				DataRow helpRedirectRow = helpRedirectTable.Select("CallingPage = '" + CurrentPageName + "'")[0];
				linkTarget = helpRedirectRow["HelpPage"].ToString();
			}
			catch
			{
				try
				{
					DataRow helpRedirectRow = helpRedirectTable.Select("CallingPage = 'SFMenu.aspx'")[0];
					linkTarget = helpRedirectRow["HelpPage"].ToString();
				}
				catch
				{
					linkTarget = ""; 
				}
			}
			return linkTarget; 
		}

		private DataSet GetMetaData()
		{
			DataSet helpMeta = new DataSet();
			helpMeta.ReadXml(Request.MapPath(BnbDataGridForQueryDataSet3.MetaDataXmlUrl));
			return helpMeta;
		}
	
    }

}