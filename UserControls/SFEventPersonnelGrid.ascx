<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Control Language="c#" AutoEventWireup="True" Codebehind="SFEventPersonnelGrid.ascx.cs" Inherits="Frond.UserControls.SFEventPersonnelGrid" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<P>
	<TABLE id="Table1" width="80%" border="0">
		<TR>
			<TD width="50%" vAlign="top">
				<P>To add new Event Personnel, enter a refno (or series of refnos) here and press 
					'Add attendee(s)'.<br>
					Multiple refnos&nbsp;must&nbsp;be separated by commas.<BR>
					<asp:TextBox id="txtRefNos" runat="server" Width="408px"></asp:TextBox>&nbsp;
					<asp:Button id="btnAddPersonnel" runat="server" Text="Add personnel" Width="112px" onclick="btnAddPersonnel_Click"></asp:Button><BR>
					<asp:Label id="lblMessage" runat="server" Font-Bold="True"></asp:Label></P>
			</TD>
		</TR>
	</TABLE>
</P>
