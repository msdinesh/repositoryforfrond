namespace Frond.UserControls
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Text;

	/// <summary>
	///	WaitMessagePanel uses client-side JavaScript to display a 'please wait' message to the user
	///	after they click a button.
	///	To use it on a page:
	///	1) Drop the UserControl onto the page (somewhere near the bottom)
	///	2) create a new DIV on the page that wraps everything that you want to hide (usually this will be all the content of the page)
	///	3) make sure the WaitMessagePanel is NOT inside that DIV!
	///	4) In the HTML, set the DivToHide attribute of WaitMessagePanel to the html ID of the DIV
	///	5) Add this code to the page_load to attach the right javascript to the button:
	///	   cmdFind.Attributes.Add("onclick","javascript:showLoadingMessage('Searching...');");
	///	  (The message that is passed to showLoadingMessage can be anything you like) 
	///	<remark>If this control is used on a page that has SmartNavigation turned on, it might crash the browser!
	///	(don't know why) Custom-coded alternatives to SmartNav should be used to preserve scroll position between postbacks.</remark>
	/// </summary>
	public partial class WaitMessagePanel : System.Web.UI.UserControl
	{

		private string m_divToHide = null;
        private string m_widthOverride = "";

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			this.Page.RegisterClientScriptBlock("WaitMessagePanelGeneral", this.ClientSideScriptGeneral());
			this.Page.RegisterStartupScript("WaitMessagePanelStartup", this.ClientSideScriptStartup());
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion


		/// <summary>
		/// The html ID of the DIV to hide when displaying the wait message
		/// this usercontrol should not be inside that div!
		/// </summary>
		public string DivToHide
		{
			get{return m_divToHide;}
			set{m_divToHide = value;}
		}

        /// <summary>
        /// usually wait panel will be as wide as page and so text will be centred to page
        /// set width here if you want text to be more on the left
        /// </summary>
        public string WidthOverride
        {
            get { return m_widthOverride; }
            set { m_widthOverride = value; }
        }


		/// <summary>
		/// Returns client side javascript for the control
		/// </summary>
		private string ClientSideScriptGeneral()
		{
			
				StringBuilder sb = new StringBuilder();
				// Outer wrapping
				doAppend(sb,"<script language=\"javascript\">");

				doAppend(sb,"function showLoadingMessage(message) {");
				doAppend(sb,"  document.body.style.cursor = 'wait';");
				doAppend(sb,"  var coverTop = baseContent.offsetTop+48;");
				doAppend(sb,"  var coverLeft = baseContent.offsetLeft;");
				doAppend(sb,"  var coverWidth = document.body.clientWidth - coverLeft;");
				doAppend(sb,"  var coverHeight = document.body.clientHeight - coverTop;");
			    doAppend(sb,"  var divRef = document.getElementById(\"mainWaitPanel\");");
				doAppend(sb,String.Format("  var contentRef = document.getElementById(\"{0}\");",
					this.DivToHide));
				doAppend(sb,"  var msgRef = document.getElementById(\"waitMessageText\");");
				doAppend(sb,"  if (message != \"\")");
				doAppend(sb,"    msgRef.innerHTML = message;");
				doAppend(sb,"  contentRef.style.display = 'none';");
				doAppend(sb,"  divRef.style.top = coverTop;");
				doAppend(sb,"  divRef.style.left = coverLeft;");
				doAppend(sb,"  divRef.style.width = coverWidth;");
				doAppend(sb,"  divRef.style.height = coverHeight;");
				doAppend(sb,"  divRef.style.display = 'block';");
				doAppend(sb,"  // set a timeout to re-load the GIF so that animation continues after postback ");
				doAppend(sb,String.Format("  setTimeout('document.images[\"waitImage\"].src = \"{0}\"', 200);",
						this.WaitImagePath));
			
				doAppend(sb,"}");
			
				// Outer wrapping
				doAppend(sb, "</script>");
				return sb.ToString();
			
		}

		/// <summary>
		/// Returns client side script to run when the page has finished loading
		/// i.e. a startup script that will be rendered at the base of the page
		/// </summary>
		/// <returns></returns>
		private string ClientSideScriptStartup()
		{
			StringBuilder sb = new StringBuilder();
			// Outer wrapping
			doAppend(sb,"<script language=\"javascript\">");
			doAppend(sb,"document.body.style.cursor = 'default';");
			doAppend(sb,"var divRef = document.getElementById(\"mainWaitPanel\");");
			doAppend(sb,String.Format("var contentRef = document.getElementById(\"{0}\");",
				this.DivToHide));
			doAppend(sb,"contentRef.style.display = 'block';");
			doAppend(sb,"divRef.style.display = 'none';");
			
			// Outer wrapping
			doAppend(sb, "</script>");
			return sb.ToString();


		}

		/// <summary>
		/// returns the path to the wait animation
		/// </summary>
		public string WaitImagePath
		{
			get
			{
				string appPath=HttpRuntime.AppDomainAppVirtualPath; 
				if (appPath=="/") 
				   appPath=""; 

				return String.Format("{0}/Images/waitanim.gif",
					appPath);
			}
		}

        
        public string ExtraStyles
        {
            get
            {
                if (this.WidthOverride == "")
                    return "";
                else
                    return String.Format("width:{0};", this.WidthOverride);

            }
        }

		
		private void doAppend(StringBuilder sBuilder, string stringToAppend) 
		{
			sBuilder.Append(stringToAppend + Environment.NewLine);
		}
	}
}
