namespace Frond.UserControls
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using BonoboDomainObjects;
	using BonoboEngine;
	using BonoboEngine.Query;
	using System.Web.UI;
	using BonoboWebControls;
	/// <summary>
	///		Summary description for SFEventPersonnelGrid.
	/// </summary>
	public partial class SFEventPersonnelGrid : System.Web.UI.UserControl
	{
		private BnbEvent m_Event = null;

		protected Label lbl = null;

		private bool m_wasPersonnelAdded = false;
		protected System.Web.UI.WebControls.Panel pnlMarkAttendeesOutcome;
		private string m_addedPersonnelIDs = "";
		private string m_deletedPersonnelID = "";

		protected void Page_Load(object sender, System.EventArgs e)
		{
			bool wasDeleteButtonClicked = (BnbWebFormManager.EventTarget == this.ID);
			string personnelToDelete = BnbWebFormManager.EventArgument;
			// Put user code to initialize the page here
			if(wasDeleteButtonClicked) 
			{
				doDeleteEventPersonnel(new System.Guid(personnelToDelete));
				m_deletedPersonnelID = personnelToDelete;
			}
		}
		public BnbEvent Event 
		{
			get { return m_Event; }
			set { m_Event = value; }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		protected override void Render(HtmlTextWriter writer)
		{
			if(m_Event == null) throw new Exception("BnbEvent has not been set for " + this.ID);
			if(m_Event.EventPersonnel.Count > 0) 
			{
				doRenderMessageLabel(writer);
				writer.AddAttribute("class","datagrid");
				writer.RenderBeginTag("table");
				doRenderDataGridHeader(writer);
				doRenderDataGridData(writer);
				writer.RenderEndTag();
			}
			base.Render (writer);
		}

		private string getEventPersonnelRoleNames(BnbDomainObjectList roles) 
		{
			BnbEventPersonnelRole role = null;
			string toreturn = "";
			foreach(BnbDomainObject bnb in roles) 
			{
				role = (BnbEventPersonnelRole)bnb;
				if(toreturn != "") toreturn += ", ";
				toreturn += BnbEngine.LookupManager.GetLookupItemDescription("lkuEventRole",role.EventRoleID);
			}
			return toreturn;
		}

		private void doRenderDataGridHeader(HtmlTextWriter writer) 
		{
			writer.RenderBeginTag("tr");
			SFUserControlServices.RenderDataGridHeaderCell(writer,"");
			SFUserControlServices.RenderDataGridHeaderCell(writer,"");
			SFUserControlServices.RenderDataGridHeaderCell(writer,"Ref No");
			SFUserControlServices.RenderDataGridHeaderCell(writer,"Name");
			SFUserControlServices.RenderDataGridHeaderCell(writer,"Roles");
			SFUserControlServices.RenderDataGridHeaderCell(writer,"Comments");
			writer.RenderEndTag();
		}

		private void doRenderDataGridData(HtmlTextWriter writer) 
		{
			BnbIndividual ind = null;
			BnbEventPersonnel person = null;
			string personnelid = "";
			int i =0;

			foreach(BnbDomainObject bnb in m_Event.EventPersonnel) 
			{
				person = (BnbEventPersonnel)bnb;
				personnelid = person.ID.ToString();
				ind = person.Individual;
//				if (m_deletedPersonnelID.Trim().Length > 0 && personnelid.Trim().Length > 0)
//				{
					if (m_deletedPersonnelID.ToLower() != personnelid.ToLower())
					{	
						writer.RenderBeginTag("tr");
						doRenderEditHyperlink(writer,m_Event.ID.ToString(), personnelid);
						doRenderDeleteButton(writer,personnelid,i);
						SFUserControlServices.RenderDataGridDataCell(writer, getIndividualHyperlink(ind));
						SFUserControlServices.RenderDataGridDataCell(writer, ind.FullName + getAddedSign(personnelid),false);
						SFUserControlServices.RenderDataGridDataCell(writer, getEventPersonnelRoleNames(person.EventPersonnelRoles));
						SFUserControlServices.RenderDataGridDataCell(writer, person.Comments);
						writer.RenderEndTag();
						i++;
					}
//				}
			}
		}

		private void doRenderEditHyperlink(HtmlTextWriter writer, string eventid, string personnelid) 
		{
			string url=String.Format("<a href=\"EventPersonnelPage.aspx?BnbEvent={0}&BnbEventPersonnel={1}\">View</a>",eventid,personnelid);
			SFUserControlServices.RenderDataGridDataCell(writer, url);
		}

		private void doRenderDeleteButton(HtmlTextWriter writer, string personnelid, int buttonNumber)
		{
			string btn = String.Format("<input type=\"button\" id=\"{0}\" name=\"{0}\" value=\"Remove\" onclick=\"__doBonoboPostBack('{2}','{1}');\">",
				this.ID + "_DeleteButton_" + buttonNumber.ToString(),
				personnelid.ToUpper(),
				this.ID);
			writer.RenderBeginTag("td");
			writer.Write(btn);
			writer.RenderEndTag();
		}

		private void doDeleteEventPersonnel(System.Guid personnelid) 
		{
			BnbEditManager em = new BnbEditManager();
			m_Event.RegisterForEdit(em);
			BnbEventPersonnel prs = BnbEventPersonnel.Retrieve(personnelid);
			string refno = prs.Individual.RefNo;
			string fullname = prs.Individual.FullName;

			prs.RegisterForEdit(em);
			prs.MarkForDeletion();

			// create message label, to be rendered when Render() fires
			lbl = new Label();
			lbl.Font.Bold = true;

			try 
			{
				em.SaveChanges();
				lbl.ForeColor = Color.Green;
				lbl.Text = String.Format("Personnel {0} ({1}) is removed from list",fullname, refno);
			}
			catch(BnbProblemException pe) 
			{
				foreach(BnbProblem p in pe.Problems) 
				{
					if(lbl.Text != "") lbl.Text += ", ";
					lbl.Text += p.Message;
				}
			}
		}

		private void doRenderMessageLabel(HtmlTextWriter writer) 
		{
			if(lbl != null)	lbl.RenderControl(writer);
		}

		protected void btnAddPersonnel_Click(object sender, System.EventArgs e)
		{
			lblMessage.Text = "";
			if (txtRefNos.Text == "")
			{
				doAppendMessage("Please enter one or more Ref Nos, separated by commas", true);
				return;
			}
			m_wasPersonnelAdded = true;
			string[] refnos = txtRefNos.Text.Split(',');
			for(int i=0;i<refnos.Length;i++)
			{
				doAddEventPersonnel(refnos[i].ToString().ToUpper().Trim());
			}
		}

		private void doAppendMessage(string message, bool isException) 
		{
			string color = (isException) ? "red" : "green";
			message = String.Format("<span style=\"color:{0}\">{1}</span><br>",color, message);
			lblMessage.Text += message;
		}

		private void doAddEventPersonnel(string refno)
		{
			BnbIndividual ind = BnbIndividual.RetrieveByRefNo(refno);
			if(ind == null) 
			{
				doAppendMessage(String.Format("Could not find any Individuals with RefNo {0}",
					refno), true);
				return;
			}

			BnbEditManager em = new BnbEditManager();
			m_Event.RegisterForEdit(em);
			BnbEventPersonnel prs = new BnbEventPersonnel(true);
			prs.RegisterForEdit(em);
			prs.Individual = ind;
			m_Event.EventPersonnel.Add(prs);
			try 
			{
				em.SaveChanges();
				m_addedPersonnelIDs += prs.ID.ToString();
				//doAppendMessage(refno + " is added", false);
			}
			catch(BnbProblemException pe) 
			{
				string msg = "";
				foreach(BnbProblem p in pe.Problems)
				{
					if(msg != "") msg += "<br>";
					msg += p.Message;
				}
				doAppendMessage(msg,true);
				// the new evp wasn't saved, but event object stays in memory during render,
				// so remove them again so they dont display
				m_Event.EventPersonnel.RemoveAndDelete(prs);
				em.EditCompleted();
			}
		}

		private string getAddedSign(string personnelid) 
		{
			if(m_wasPersonnelAdded && m_addedPersonnelIDs.IndexOf(personnelid) != -1) return "&nbsp;<span style=\"color:green\"><b>[Added]</b></span>";
			return "";
		}

		private string getIndividualHyperlink(BnbIndividual ind)
		{
			string refno = ind.RefNo;
			string anchor = String.Format("<a href=\"../EditPages/IndividualPage.aspx?BnbIndividual={0}\" target=\"_top\">{1}</a>", ind.ID.ToString(), refno);

			return anchor;
		}
	

	}
}
