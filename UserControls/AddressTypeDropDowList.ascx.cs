namespace Frond.UserControls
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using BonoboEngine.Query;
	using BonoboEngine;

	/// <summary>
	///		Summary description for SFAddressTypeDropDowList.
	/// </summary>
	public partial class AddressTypeDropDowList : System.Web.UI.UserControl
	{

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here]
			if(!IsPostBack)
			{
				DataTable table = BnbEngine.LookupManager.GetLookup("vlkuBonoboAddressTypeWithGroup");
				table.DefaultView.RowFilter = "Exclude = 0";
				table.DefaultView.Sort = "Description";
				lckAddressType.DataSource = table;
				lckAddressType.DataTextField = "Description";
				lckAddressType.DataValueField = "IntID";
				lckAddressType.DataBind();
				lckAddressType.Items.Insert(0,new ListItem("[Select...]","-1"));
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion
	}
}
