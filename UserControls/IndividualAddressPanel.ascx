<%@ Register TagPrefix="cc1" Namespace="BonoboWebControls.ServicesControls" Assembly="BonoboWebControls" %>
<%@ Control Language="c#" AutoEventWireup="True" Codebehind="IndividualAddressPanel.ascx.cs" Inherits="Frond.UserControls.IndividualAddressPanel" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register Src="GShowLocation.ascx" TagName="GShowLocation" TagPrefix="uc1" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<TABLE class="wizardtable" id="Table4">
	<TR>
		<TD class="label" style="HEIGHT: 15px">Forenames</TD>
		<TD style="HEIGHT: 15px"><asp:label id="lblForename2" runat="server"></asp:label></TD>
	</TR>
	<TR>
		<TD class="label">Surname</TD>
		<TD><asp:label id="lblSurname2" runat="server"></asp:label></TD>
	</TR>
	<TR>
		<TD class="label">Country</TD>
		<TD><asp:dropdownlist id="drpCountry" runat="server" Width="250px" Height="20px"></asp:dropdownlist></TD>
	</TR>
	<TR>
		<TD class="label">Postcode / ZIP</TD>
		<TD><asp:textbox id="txtPostcode" runat="server" Width="174px"></asp:textbox>
			<asp:Button id="cmdFind" runat="server" Text="Find" onclick="cmdFind_Click"></asp:Button>
			<cc1:BnbAddressLookupComposite id="bnbAddressLookup1" runat="server" CssClass="lookupcontrol" AccountCode="vso1111111"
				LicenseKey="XH57-MT97-CZ47-GH56" PasswordCapscan="492267" RegistrationCodeCapscan="00003304" AutoPostBack="True"
				Width="300px" ListBoxRows="6" onselectedvaluechanged="bnbAddressLookup1_SelectedIndexChanged"></cc1:BnbAddressLookupComposite></TD>
	</TR>
	<TR>
		<TD class="label">Address Line 1</TD>
		<TD><asp:textbox id="txtAddressLine1" runat="server" Width="224px"></asp:textbox>
			<asp:Button id="cmdFindAddressLine1" runat="server" Text="Find"></asp:Button>
			                        
</TD>
	</TR>
	<TR>
		<TD class="label">Address Line 2</TD>
		<TD><asp:textbox id="txtAddressLine2" runat="server" Width="224px"></asp:textbox></TD>
	</TR>
	<TR>
		<TD class="label">Address Line 3</TD>
		<TD><asp:textbox id="txtAddressLine3" runat="server" Width="224px"></asp:textbox></TD>
	</TR>
	<TR>
		<TD class="label">Postal Town / Line 4</TD>
		<TD><asp:textbox id="txtAddressLine4" runat="server" Width="224px"></asp:textbox></TD>
	</TR>
	<TR>
		<TD class="label">County / State</TD>
		<TD>
			<asp:TextBox id="txtCounty" runat="server" Width="224px"></asp:TextBox></TD>
	</TR>
	<TR>
		<TD class="label"></TD>
		<TD><input id="btnShowLocation" runat="Server" type="button" onclick="showLocation()" value="Show on map" /></TD>
	</TR>
	<TR>
		<TD class="label">
			<P>&nbsp;Home Telephone</P>
		</TD>
		<TD><asp:textbox id="txtTelephone" runat="server" Width="224px"></asp:textbox></TD>
	</TR>
	<TR>
		<TD class="label">Mobile</TD>
		<TD><asp:textbox id="txtMobile" runat="server" Width="224px"></asp:textbox></TD>
	</TR>
	<TR>
		<TD class="label">Email</TD>
		<TD><asp:textbox id="txtEmail" runat="server" Width="224px"></asp:textbox></TD>
	</TR>
	<TR>
		<TD class="label">Raisers Edge ID</TD>
		<TD><asp:textbox id="txtRaisersEdgeId" runat="server" Width="224px"></asp:textbox></TD>
	</TR>
	
</TABLE>

<uc1:GShowLocation ID="showLocation" runat="server" />


