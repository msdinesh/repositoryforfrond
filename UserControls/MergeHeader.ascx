<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MergeHeader.ascx.cs" Inherits="Frond.UserControls.MergeHeader" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<table class="<%=cssClass%>" id="Table1" border="0">
    <tr>
        <td>
           <img height="75px" src="<%=virtualroot%>/Images/FrondSmall.jpg" width="75px" style="border-right: lime 1px solid;
                border-top: lime 1px solid; border-left: lime 1px solid; border-bottom: lime 1px solid"></td>
    </tr>
    <tr>
        <td>
           <span class="StarfishTitle">Merge</span></td>
    </tr>
    <tr>
        <td>
            (<%=appMode%>)
        </td>
    </tr>
    <tr>
        <td>
            <table class="menucollapse">
                <tr>
                    <td class="menuicon">
                        <a href="<%=virtualroot%>/EditPages/Merge/frmMergeWizard.aspx" target="_top">
                            <img class="icon24" src="<%=virtualroot%>/Images/mainmenu24silver.gif"></a>
                    </td>
                    <td class="menutext">
                        <asp:Panel ID="pnlMergeHome" runat="server" Visible="false">
                            <a href="<%=virtualroot%>/EditPages/Merge/frmMergeWizard.aspx" visible="false" target="_top">Home</a>
                        </asp:Panel>
                        <asp:Panel ID="pnlBulkMerge" runat="server" Visible="false">
                            <a href="<%=virtualroot%>/EditPages/Merge/frmBulkMerge.aspx" visible="false" target="_top">Home</a>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="menuicon">
                        <a href="<%=virtualroot%>/menu.aspx" target="_top">
                            <img class="icon24" src="<%=virtualroot%>/Images/frond_Menu.jpg"></a>
                    </td>
                    <td class="menutext">
                        <a href="<%=virtualroot%>/menu.aspx" target="_top">Frond</a>
                    </td>
                </tr>
                <tr>
                    <td class="menuicon">
                        <a href="<%=virtualroot%>/logout.aspx" target="_top">
                            <img class="icon24" src="<%=virtualroot%>/Images/logout24silver.gif" /></a>
                    </td>
                    <td class="menutext">
                        <a href="<%=virtualroot%>/logout.aspx" target="_top">Logout</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:HyperLink ID="hlCustom" runat="server"></asp:HyperLink>
        </td>
    </tr>
    <tr>
        <td style="height: 100%">
            <div style="height:600px"></div>
        </td>
    </tr>
</table>
<bnbdatagrid:BnbDataGridForQueryDataSet ID="BnbDataGridForQueryDataSet3" runat="server"
    CssClass="datagrid"></bnbdatagrid:BnbDataGridForQueryDataSet>
