<%@ Control Language="c#" AutoEventWireup="True" Codebehind="MenuBar.ascx.cs" Inherits="Frond.UserControls.MenuBar"
    TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table class="<%=MenuBarCssClass%>" id="Table1" border="0">
    <tr>
        <td class="mnAppIconCell">
            <img src="<%=VirtualRoot%>/Images/<%=IconFilename%>">
        </td>
    </tr>
    <tr>
        <td>
            <span class="mnAppTitle">
                <%=VirtualApplicationName%>
            </span>
        </td>
    </tr>
    <tr>
        <td>
            <%=AppMode%>
        </td>
    </tr>
    <tr>
        <td>
            <table class="mnMenuList">
                <tr>
                    <td class="mnMenuIcon">
                        <a href="<%=VirtualRoot%>/Menu.aspx" target="_top">
                            <img class="icon24" src="<%=VirtualRoot%>/Images/mainmenu24silver.gif"></a>
                    </td>
                    <td class="mnMenuText">
                        <a href="<%=VirtualRoot%>/Menu.aspx" target="_top">Menu</a>
                    </td>
                </tr>
                <tr>
                    <td class="mnMenuIcon">
                        <a href="<%=VirtualRoot%>/FindGeneral.aspx<%=OptionalFindParameter%>" target="_top">
                            <img class="icon24" src="<%=VirtualRoot%>/Images/find24silver.gif"></a>
                    </td>
                    <td class="mnMenuText">
                        <a href="<%=VirtualRoot%>/FindGeneral.aspx<%=OptionalFindParameter%>" target="_top">
                            Find</a>
                    </td>
                </tr>
                <tr>
                    <td class="mnMenuIcon">
                        <a href="<%=VirtualRoot%>/Query/CustomQuery.aspx" target="_top">
                            <img class="icon24" src="<%=VirtualRoot%>/Images/query24silver.gif"></a>
                    </td>
                    <td class="mnMenuText">
                        <a href="<%=VirtualRoot%>/Query/CustomQuery.aspx" target="_top">Query</a>
                    </td>
                </tr>
                <tr>
                    <td class="mnMenuIcon">
                        <a href="<%=VirtualRoot%>/Reports/ReportChooser.aspx" target="_top">
                            <img class="icon24" src="<%=VirtualRoot%>/Images/report24silver.gif"></a>
                    </td>
                    <td class="mnMenuText">
                        <a href="<%=VirtualRoot%>/Reports/ReportChooser.aspx" target="_top">Reports</a>
                    </td>
                </tr>
                <tr>
                    <td class="mnMenuIcon">
                        <a href="<%=VirtualRoot%>/History.aspx" target="_top">
                            <img class="icon24" src="<%=VirtualRoot%>/Images/history24silver.gif"></a>
                    </td>
                    <td class="mnMenuText">
                        <a href="<%=VirtualRoot%>/History.aspx" target="_top">History</a>
                    </td>
                </tr>
                <tr>
                    <td class="mnMenuIcon">
                        <asp:HyperLink ID="hlIconHelp" runat="server" Target="_blank" CssClass="icon24">lb</asp:HyperLink>
                    </td>
                    <td class="mnMenuText">
                        <asp:HyperLink ID="hlHelp" runat="server" Target="_blank">Help</asp:HyperLink>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr runat="server" id="sectionWendy" visible="true">
        <td class="mnQuicklinks">
            <asp:Panel CssClass="pnlQuickLinks" ID="pnlQuickLinks" runat="server">
                <asp:Label CssClass="lblQuickLinks" ID="lblQuickLinks" runat="server">Quicklinks</asp:Label>
                <br /><br />
                <a href="<%=VirtualRoot%>/EditPages/WizardWendy.aspx" target="_top">Wendy</a>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td>
            <table class="mnMenuList">
                <tr>
                    <td class="mnMenuIcon">
                        <asp:HyperLink ID="h1IconChangePassword" runat="server" Target="_blank" CssClass="icon24">lb</asp:HyperLink>
                    </td>
                    <td class="mnMenuText">
                        <asp:HyperLink ID="hlChangePassword" runat="server">Change password</asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td class="mnMenuIcon">
                        <a href="<%=VirtualRoot%>/Logout.aspx" target="_top">
                            <img class="icon24" src="<%=VirtualRoot%>/Images/logout24silver.gif"></a>
                    </td>
                    <td class="mnMenuText">
                        <a href="<%=VirtualRoot%>/Logout.aspx" target="_top">Logout</a>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="labelImage" runat="server" Visible="False">lb</asp:Label></td>
                    <td>
                        <asp:Label ID="labelMenuOption" runat="server" Visible="False">lb</asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="mnVertSpacer">
        <td>
        </td>
    </tr>
</table>
