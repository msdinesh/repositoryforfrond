//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.1433
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Frond.UserControls {
    
    public partial class IndividualAddressPanel {
        protected System.Web.UI.WebControls.Label lblForename2;
        protected System.Web.UI.WebControls.Label lblSurname2;
        protected System.Web.UI.WebControls.DropDownList drpCountry;
        protected System.Web.UI.WebControls.TextBox txtPostcode;
        protected System.Web.UI.WebControls.Button cmdFind;
        protected BonoboWebControls.ServicesControls.BnbAddressLookupComposite bnbAddressLookup1;
        protected System.Web.UI.WebControls.TextBox txtAddressLine1;
        protected System.Web.UI.WebControls.Button cmdFindAddressLine1;
        protected System.Web.UI.WebControls.TextBox txtAddressLine2;
        protected System.Web.UI.WebControls.TextBox txtAddressLine3;
        protected System.Web.UI.WebControls.TextBox txtAddressLine4;
        protected System.Web.UI.WebControls.TextBox txtCounty;
        protected System.Web.UI.HtmlControls.HtmlInputButton btnShowLocation;
        protected System.Web.UI.WebControls.TextBox txtTelephone;
        protected System.Web.UI.WebControls.TextBox txtMobile;
        protected System.Web.UI.WebControls.TextBox txtEmail;
        protected System.Web.UI.WebControls.TextBox txtRaisersEdgeId;
        protected Frond.UserControls.GShowLocation showLocation;
    }
}
