namespace Frond.UserControls
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Navigate panel is a client-side hidden panel which can be displayed by clicking above it.
	///		It is meant to contain a series of links to other apps and pages
	/// </summary>
	public partial class NavigatePanel : System.Web.UI.UserControl
	{

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		public void AddLink(string linkText, string linkURL)
		{
			lblNavigateLinks.Text  = lblNavigateLinks.Text + String.Format("<a href='{0}'>{1}</a><br/>",
					linkURL, linkText);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion
	}
}
