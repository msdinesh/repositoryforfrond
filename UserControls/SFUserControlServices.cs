using System;
using System.IO;
using System.Web;
using System.Web.UI;
using BonoboDomainObjects;

namespace Frond.UserControls
{
	/// <summary>
	/// Summary description for SFUserControlServices.
	/// </summary>
	public class SFUserControlServices
	{
		public SFUserControlServices()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static void RenderDataGridHeaderCell(HtmlTextWriter tw, string stringToWrite) 
		{
			tw.RenderBeginTag("th");
			tw.Write(HttpUtility.HtmlEncode(stringToWrite));
			tw.RenderEndTag();
		}

		public static void RenderDataGridDataCell(HtmlTextWriter tw, string stringtoWrite)
		{
			RenderDataGridDataCell(tw,stringtoWrite,true);
		}

		public static void RenderDataGridControlCell(HtmlTextWriter tw, Control ctl) 
		{
			tw.RenderBeginTag("td");
			ctl.RenderControl(tw);
			tw.RenderEndTag();
		}

		public static void RenderDataGridDataCell(HtmlTextWriter tw, string stringToWrite, bool encodeToHtml) 
		{
			if(stringToWrite != null && encodeToHtml && !stringToWrite.StartsWith("<a") && !stringToWrite.EndsWith("</a>")) stringToWrite = HttpUtility.HtmlEncode(stringToWrite);
			tw.RenderBeginTag("td");
			tw.Write(stringToWrite);
			tw.RenderEndTag();
		}

		public static string GetVolunteerServicePageUrl(BnbIndividual ind)
		{
			string refno = ind.RefNo;
			if(ind.CurrentApplication == null) return refno;

			string applicationid = ind.CurrentApplication.ID.ToString();
			string anchor = String.Format("<a href=\"../EditPages/ApplicationPage.aspx?BnbApplication={0}\" target=\"_top\">{1}</a>",applicationid, refno);
			return anchor;
		}

	}
}
