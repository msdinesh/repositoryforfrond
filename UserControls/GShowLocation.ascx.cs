using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Frond.UserControls
{
    /// <summary>
    /// This class is used to capture Latitude, Longitude for the given address.
    /// </summary>
    public partial class GShowLocation : System.Web.UI.UserControl
    {

        private string m_cssName = "gmaptableWizard";

        #region " GMap Properties "

        public string Latitude
        {
            get
            {
                return txtLatitude.Value;
            }
            set
            {
                txtLatitude.Value = value;
            }

        }

        public string Longitude
        {
            get
            {
                return txtLongitude.Value;
            }
            set
            {
                txtLongitude.Value = value;
            }

        }

        public bool HeadingVisibile
        {
            set
            {
                lblHeading.Visible = value;
            }
        }

        public string CssName
        {
            get
            {
                return m_cssName;
            }
            set
            {
                m_cssName = value;
            }

        }

        #endregion
    }
}