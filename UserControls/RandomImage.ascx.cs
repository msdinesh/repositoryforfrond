namespace Frond.UserControls
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Text;
	using BonoboEngine;

	/// <summary>
	///		Summary description for RandomImage.
	/// </summary>
	public partial class RandomImage : System.Web.UI.UserControl
	{
		private string m_imageLibraryXMLFilename;

		/// <summary>
		/// Remember the last image used in each thread
		/// (this prevents two identical images on a page)
		/// </summary>
		[ThreadStatic]
		private static string m_lastThreadImage = null;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (!this.IsPostBack)
			{
				RenderImage();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		public string ImageLibraryXMLFilename
		{
			get{return m_imageLibraryXMLFilename;}
			set{m_imageLibraryXMLFilename = value;}
		}

		private DataSet GetImageLibraryDataSet()
		{
			DataSet makeFromXml = new DataSet();
			makeFromXml.ReadXml(Page.MapPath(this.ImageLibraryXMLFilename), XmlReadMode.ReadSchema);
			return makeFromXml;
		}


		private void RenderImage()
		{
			DataSet imageSet = this.GetImageLibraryDataSet();
			DataTable config = imageSet.Tables["ImageConfig"];
			string imageBaseUrl = config.Rows[0]["ImageUrl"].ToString();
			// if we are running over SSL then use SSL url
			if (this.Request.IsSecureConnection)
				imageBaseUrl = config.Rows[0]["ImageUrlSsl"].ToString();

			DataTable imageList = imageSet.Tables["ImageInfo"];
			string rowFilter = "Restricted=false";

			// make sure we dont get same image as previous
			if (m_lastThreadImage != null)
				rowFilter+=String.Format(" and ImageFilename <> '{0}'",m_lastThreadImage);

			imageList.DefaultView.RowFilter = rowFilter;			
			int imageCount = imageList.DefaultView.Count;
			Random generator = new Random();

			int randomRowIndex = generator.Next(0,imageCount-1);
			DataRowView randomRow = imageList.DefaultView[randomRowIndex];

			m_lastThreadImage = randomRow["ImageFilename"].ToString();

			// now render the image
			string imageUrl = imageBaseUrl + randomRow["ImageFilename"].ToString();
			string copyright = randomRow["Copyright"].ToString();
			string info = String.Format("Volunteer: {0}, {1}",
				randomRow["VolunteerName"].ToString(),  BnbRuleUtils.TitleCase(randomRow["Country"].ToString()));
			if (randomRow["Year"].ToString() != "")
				info += String.Format(" ({0})", randomRow["Year"].ToString());
			info+= String.Format("{0}(c) {1}{0}(Image Library {2} No {3})", (char)13 ,copyright,
				randomRow["CD"].ToString(), randomRow["ImageNum"].ToString());

			uxPlaceholder.Text = String.Format("<img src=\"{0}\" alt=\"VSO Image\" title=\"{1}\" />",
							imageUrl, info);


		}

		/// <summary>
		/// for testing purposes 
		/// .. this renders all images, so that the image urls can be checked
		/// </summary>
		private void RenderAllImages()
		{
			DataSet imageSet = this.GetImageLibraryDataSet();
			DataTable config = imageSet.Tables["ImageConfig"];
			string imageBaseUrl = config.Rows[0]["ImageUrl"].ToString();
			DataTable imageList = imageSet.Tables["ImageInfo"];
			StringBuilder sb = new StringBuilder();

			foreach(DataRow rowLoop in imageList.Rows)
			{
				string imageFile = rowLoop["ImageFilename"].ToString();
				sb.AppendFormat("<img src=\"{0}\" alt=\"{1}\" />", imageBaseUrl + imageFile, imageFile);
			}
			uxPlaceholder.Text  = sb.ToString();
		}



	}
}
