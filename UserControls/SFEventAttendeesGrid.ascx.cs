namespace Frond.UserControls
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using BonoboDomainObjects;
	using BonoboEngine;
	using BonoboEngine.Query;
	using System.Web.UI;
	using BonoboWebControls;
	/// <summary>
	///		Summary description for SFEventAttendeesGrid.
	/// </summary>
	public partial class SFEventAttendeesGrid : System.Web.UI.UserControl
	{
		private BnbEvent m_Event = null;

		protected Label lbl = null;

		private bool wasMarkOutComeButtonClicked = false;
		private bool wasMarkStatusButtonClicked = false;
		private bool wasAttendeeAdded = false;

		private string m_checkedAttendeeIDs = "";
		private string m_addedAttendeeIDs = "";
		private string m_deletedAttendeeID = "";


		protected void Page_Load(object sender, System.EventArgs e)
		{
			lblMessage.Text = "";
			// Put user code to initialize the page here

			bool wasDeleteButtonClicked = (BnbWebFormManager.EventTarget == this.ID);
			string attendanceguidToDelete = BnbWebFormManager.EventArgument;
			
			//if delete button was clicked
			if(wasDeleteButtonClicked)
			{
				doDeleteEventAttendee(new System.Guid(attendanceguidToDelete));
				m_deletedAttendeeID = attendanceguidToDelete;
			}
			if(!IsPostBack) 
			{
				doBindDropDownList(ddlAttendanceStatus,"lkuAttendanceStatus");
				doBindDropDownList(ddlAttendanceOutcome,"lkuOutcome");
			}
		}

		public BnbEvent Event 
		{
			get { return m_Event; }
			set { m_Event = value; }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion
	
		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			if(m_Event == null) throw new Exception("BnbEvent has not been set for " + this.ID);
			bool hasattendees = (m_Event.Attendances.Count >0);
			pnlMarkAttendeesOutcome.Visible = hasattendees;
			if(hasattendees)
			{
				doRenderMessageLabel(writer);
				writer.AddAttribute("class","datagrid");
				writer.RenderBeginTag("table");
				doRenderDataGridHeader(writer);
				doRenderDataGridData(writer);
				writer.RenderEndTag();
			}
			base.Render(writer);
		}

		private string getCurrentAttendanceProgressString(int attendancestatusid, bool updated) 
		{
			if(attendancestatusid == -1) return "";
			string toreturn = BnbEngine.LookupManager.GetLookupItemDescription("lkuAttendanceStatus", attendancestatusid);
			if(wasMarkStatusButtonClicked && updated) toreturn += getUpdatedSign();
			return toreturn;
														  
		}

		private string getOutcomeString(int outcomeid, bool updated) 
		{
			if(outcomeid == -1) return "";
			string toreturn = BnbEngine.LookupManager.GetLookupItemDescription("lkuOutcome",outcomeid);
			if(wasMarkOutComeButtonClicked && updated) toreturn += getUpdatedSign();
			return (toreturn == "(Not Found)") ? "" : toreturn;
		}

		private void doRenderEditHyperlink(HtmlTextWriter writer, string eventguid, string attendanceguid) 
		{
			string url = String.Format("<a href=\"AttendancePage.aspx?BnbEvent={0}&BnbAttendance={1}\" target=\"_top\">View</a>",eventguid,attendanceguid);
			SFUserControlServices.RenderDataGridDataCell(writer, url);
		}

		private void doRenderDataGridHeader(HtmlTextWriter writer) 
		{
			// headers
			writer.RenderBeginTag("tr");
			SFUserControlServices.RenderDataGridHeaderCell(writer, "");
			SFUserControlServices.RenderDataGridHeaderCell(writer, "");
			SFUserControlServices.RenderDataGridHeaderCell(writer, "Name");
			SFUserControlServices.RenderDataGridHeaderCell(writer, "Ref no");
			SFUserControlServices.RenderDataGridHeaderCell(writer, "Situation");
			SFUserControlServices.RenderDataGridHeaderCell(writer, "Situation Date");
			SFUserControlServices.RenderDataGridHeaderCell(writer, "Outcome");
			SFUserControlServices.RenderDataGridHeaderCell(writer, "Comments");
			SFUserControlServices.RenderDataGridHeaderCell(writer, "Lead Skill");
			SFUserControlServices.RenderDataGridHeaderCell(writer, "");
			writer.RenderEndTag();
		}

		private void doRenderDataGridData(HtmlTextWriter writer) 
		{
			System.Globalization.CultureInfo enGB = new System.Globalization.CultureInfo("en-GB");
			BnbAttendance attendance = null;
			int i=0;
			string attendanceid = "";
			bool isMarked = false;
			foreach(BnbDomainObject bnb in m_Event.Attendances)
			{
				attendance = (BnbAttendance)bnb;
				attendanceid = attendance.ID.ToString();
				isMarked = (m_checkedAttendeeIDs.IndexOf(attendanceid) != -1);
				if (attendanceid.ToLower() != m_deletedAttendeeID.ToLower())
				{
					writer.RenderBeginTag("tr");
					doRenderEditHyperlink(writer,m_Event.ID.ToString(), attendanceid);
					doRenderDeleteButton(writer,attendance.ID,i);
					SFUserControlServices.RenderDataGridDataCell(writer, attendance.Individual.FullName + getAddedSign(attendanceid),false);
					SFUserControlServices.RenderDataGridDataCell(writer, getVolunteerOrIndividualHyperlink( attendance.Individual, attendance.Application),false);
					int situationID = -1;
					string statusDateString = "";
					if (attendance.CurrentAttendanceProgress != null)
					{
						situationID = attendance.CurrentAttendanceProgress.AttendanceStatusID;
						statusDateString = attendance.CurrentAttendanceProgress.StatusDate.ToString("dd/MMM/yyyy",enGB);
					}
					SFUserControlServices.RenderDataGridDataCell(writer, getCurrentAttendanceProgressString(situationID ,isMarked),false);
					SFUserControlServices.RenderDataGridDataCell(writer, statusDateString );
					SFUserControlServices.RenderDataGridDataCell(writer, getOutcomeString(attendance.OutcomeID, isMarked),false);
					SFUserControlServices.RenderDataGridDataCell(writer, attendance.Comments);
					string leadskill = "";
					if (attendance.Individual.LeadSkill != null)
					{
						leadskill = BnbEngine.LookupManager.GetLookupItemDescription("vlkuBonoboSkillWithGroupAbbrev", attendance.Individual.LeadSkill.SkillID);
					}
					SFUserControlServices.RenderDataGridDataCell(writer, leadskill, false);
					doRenderCheckBox(writer,attendance.ID.ToString(),i);
					writer.RenderEndTag();
					i++;
				}
			}
		}

		private void doRenderCheckBox(HtmlTextWriter writer, string attendanceguid, int checkboxNumber) 
		{
			string cbx = String.Format("<input type=\"checkbox\" id=\"{0}\" name=\"{0}\" value=\"{1}\">",this.ID + "_CheckBox_" + checkboxNumber.ToString() ,attendanceguid);
			writer.RenderBeginTag("td");
			writer.Write(cbx);
			writer.RenderEndTag();
		}

		private void doRenderMessageLabel(HtmlTextWriter writer) 
		{
			if(lbl != null)	lbl.RenderControl(writer);
		}

		private void doRenderDeleteButton(HtmlTextWriter writer, System.Guid attendanceguid, int buttonNumber)
		{
			string btn = String.Format("<input type=\"button\" id=\"{0}\" name=\"{0}\" value=\"Remove\" onclick=\"__doBonoboPostBack('{2}','{1}');\">",
				this.ID + "_DeleteButton_" + buttonNumber.ToString(),
				attendanceguid.ToString().ToUpper(),
				this.ID);
			writer.RenderBeginTag("td");
			writer.Write(btn);
			writer.RenderEndTag();
		}

		private void doDeleteEventAttendee(System.Guid attendanceguid) 
		{
			BnbEditManager em = new BnbEditManager();
			m_Event.RegisterForEdit(em);
			BnbAttendance att = BnbAttendance.Retrieve(attendanceguid);
			string refno = att.Individual.RefNo;
			string fullname = att.Individual.FullName;

			att.RegisterForEdit(em);
			att.MarkForDeletion();

			// create message label, to be rendered when Render() fires
			lbl = new Label();
			lbl.Font.Bold = true;

			try 
			{
				em.SaveChanges();
				lbl.ForeColor = Color.Green;
				lbl.Text = String.Format("Attendee {0} ({1}) is removed from list",fullname, refno);
			}
			catch(BnbProblemException pe) 
			{
				foreach(BnbProblem p in pe.Problems) 
				{
					if(lbl.Text != "") lbl.Text += ", ";
					lbl.Text += p.Message;
				}
			}
		}

		protected void btnMarkStatus_Click(object sender, System.EventArgs e)
		{
			wasMarkStatusButtonClicked = true;
			doBulkMarkAttendees(((Button)sender).ID);
		}

		private void doBindDropDownList(DropDownList ddl, string lookuptablename) 
		{
			ddl.DataSource = BnbEngine.LookupManager.GetLookup(lookuptablename).Select("Exclude = 0","Description");
			ddl.DataTextField = "Description";
			ddl.DataValueField = "IntID";
			ddl.DataBind();
			ddl.Items.Insert(0,new ListItem("","-1"));
		}

		protected void btnMarkOutcome_Click(object sender, System.EventArgs e)
		{
			wasMarkOutComeButtonClicked = true;
			doBulkMarkAttendees(((Button)sender).ID);
		}

		private void doMarkAttendee(System.Guid attendanceguid, string buttonID)
		{
			BnbEditManager em = new BnbEditManager();
			BnbAttendance att = BnbAttendance.Retrieve(attendanceguid);
			att.RegisterForEdit(em);
			if(buttonID == "btnMarkStatus")	att.AddAttendanceProgress(Convert.ToInt32(ddlAttendanceStatus.SelectedValue));
			if(buttonID == "btnMarkOutcome") att.OutcomeID = Convert.ToInt32(ddlAttendanceOutcome.SelectedValue);
			try 
			{
				em.SaveChanges();
				m_checkedAttendeeIDs += attendanceguid;
			}
			catch(BnbProblemException pe) 
			{
				foreach(BnbProblem p in pe.Problems) 
				{
					
				}
				em.EditCompleted();
			}
		}

		private void doBulkMarkAttendees(string buttonID) 
		{
			// round up all checked checkboxes and mark attendees
			string cbx = "";
			for(int i=0;i<m_Event.Attendances.Count;i++)
			{
				cbx = Request.Form[this.ID + "_CheckBox_" + i];
				if(cbx != null) 
				{
					doMarkAttendee(new System.Guid(cbx), buttonID);
				}
			}
		}

		private string getUpdatedSign() 
		{
			return "&nbsp;<span style=\"color:green\"><b>[Updated]</b></span>";
		}

		protected void btnAddAttendees_Click(object sender, System.EventArgs e)
		{
			// get individual
			// then application
			
			lblMessage.Text = "";
			if (txtRefNos.Text == "")
			{
				doAppendMessage("Please enter one or more Ref Nos, separated by commas", true);
				return;
			}
			string[] refnos = txtRefNos.Text.Split(',');
			wasAttendeeAdded = true;
			for(int i=0;i<refnos.Length;i++)
			{
				doAddEventAttendee(refnos[i].ToString().ToUpper().Trim());
			}
		}

		private void doAddEventAttendee(string refno)
		{
			BnbIndividual ind = BnbIndividual.RetrieveByRefNo(refno);
			if(ind == null) 
			{
				doAppendMessage(String.Format("Could not find any Individuals with RefNo {0}",
					refno), true);
				return;
			}

			BnbEditManager em = new BnbEditManager();
			m_Event.RegisterForEdit(em);
			BnbAttendance att = new BnbAttendance(true);
			att.RegisterForEdit(em);
			// always link via individual
			att.Individual = ind;
			// also link to current app if there is one
			if (ind.CurrentApplication != null)
				att.Application = ind.CurrentApplication;

			m_Event.Attendances.Add(att);
			try 
			{
				em.SaveChanges();
				m_addedAttendeeIDs += att.ID.ToString();
				//doAppendMessage(refno + " is added", false);
			}
			catch(BnbProblemException pe) 
			{
				string msg = "";
				foreach(BnbProblem p in pe.Problems)
				{
					if(msg != "") msg += "<br>";
					msg += p.Message;
				}
				doAppendMessage(msg,true);
				// the new attendacne wasn't saved, but event object stays in memory during render,
				// so remove them again so they dont display
				m_Event.Attendances.RemoveAndDelete(att);
				em.EditCompleted();
			}
		}

		private void doAppendMessage(string message, bool isException) 
		{
			string color = (isException) ? "red" : "green";
			message = String.Format("<span style=\"color:{0}\">{1}</span><br>",color, message);
			lblMessage.Text += message;
		}

		private string getAddedSign(string attendanceid) 
		{
			if(wasAttendeeAdded && m_addedAttendeeIDs.IndexOf(attendanceid) != -1) return "&nbsp;<span style=\"color:green\"><b>[Added]</b></span>";
			return "";
		}

		private string getVolunteerOrIndividualHyperlink(BnbIndividual ind, BnbApplication app)
		{
			string refno = ind.RefNo;
			string anchor = null;
			if (app == null)
				anchor = String.Format("<a href=\"../EditPages/IndividualPage.aspx?BnbIndividual={0}\" target=\"_top\">{1}</a>", ind.ID.ToString(), refno);
			else
				anchor = String.Format("<a href=\"../EditPages/ApplicationPage.aspx?BnbApplication={0}\" target=\"_top\">{1}</a>", app.ID.ToString(), refno);

			return anchor;
		}

	
	}
}
