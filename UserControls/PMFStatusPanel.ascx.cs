using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboEngine.Meta;
using BonoboDomainObjects;

namespace Frond.UserControls
{
    public partial class PMFStatusPanel : System.Web.UI.UserControl
    {

        private string m_virtualroot = "";

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private string m_pMFStatusID = "";
        public string PMFStatusID
        {
            get
            {
                if (m_pMFStatusID == "")
                    m_pMFStatusID = ViewState[this.ID + "_PMF"].ToString();

                return m_pMFStatusID;
            }
            set
            {
                m_pMFStatusID = value;
                ViewState[this.ID + "_PMF"] = m_pMFStatusID;
            }
        }

        private int m_projectTypeID;
        public int ProjectTypeID
        {
            get { return m_projectTypeID; }
            set { m_projectTypeID = value; }
        }

        private void doRenderFirstRow(HtmlTextWriter writer)
        {

            
            
        }

        protected override void Render(HtmlTextWriter writer)
        {
            //TPT:PGMSMIFI-126- removed IsPostback condition
             doRenderEntireTable(writer);
        }

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        private void InitializeComponent()
        {
        }

        private void doRenderEntireTable(HtmlTextWriter writer)
        {
            writer.AddAttribute("class", "pmfStatusBar");
            writer.RenderBeginTag("table");
            writer.RenderBeginTag("tr");

            string pmfLabel = "PMF";
            // get correct label for PMFStatusID field on tblProject
            BnbMetaDataSet projectMeta = BnbEngine.MetaDataManager.GetMetaData("tblProject");
            DataRow[] pmfMetaRowArray = projectMeta.FieldInfoTable.Select("FieldName = 'PMFStatusID'");
            if (pmfMetaRowArray.Length > 0)
                pmfLabel = pmfMetaRowArray[0]["FieldUserAlias"].ToString();
            


            // write label cell
            writer.AddAttribute("class", "pmfLabelCell");
            writer.RenderBeginTag("td");
            writer.Write(pmfLabel + ":");
            writer.RenderEndTag();


            BnbLookupDataTable PMFStatus = BnbEngine.LookupManager.GetLookup("lkuPMFStatus");
            int tempStatus = 0;
            bool firstBox = true;
            foreach (BnbLookupRow lookupInfo in PMFStatus.Rows)
            {
                if (firstBox)
                    firstBox = false;
                else
                {
                    if (tempStatus == 0)
                    {
                        // if not first box, show arrow before
                        writer.AddAttribute("class", "pmfArrowCell");
                        writer.RenderBeginTag("td");
                        writer.AddAttribute("src", String.Format("{0}/Images/forwardarrow16white.gif", this.VirtualRoot));
                        writer.RenderBeginTag("img");
                        writer.RenderEndTag();
                        writer.RenderEndTag();
                    }
                }

                string pmfDescription = null;
                string pmfTooltip = null;
                if (lookupInfo["ToolTip"] != DBNull.Value)
                    pmfTooltip = lookupInfo["ToolTip"].ToString();

                //TPT-Amended:PGMSMIFI-81

                if (lookupInfo.ID == BnbConst.PMFStatus_ConceptDevelopment.ToString())
                {
                    if (this.ProjectTypeID == BnbConst.ProjectType_Project)
                    {
                        pmfDescription = "Programme";
                    }
                    else
                    {
                        pmfDescription = "Programme";
                        if (this.PMFStatusID == BnbConst.PMFStatus_ConceptDevelopment.ToString())
                        {
                            writer.AddAttribute("class", "pmfSelectedCell");
                        }
                    }
                }
                else if (lookupInfo.ID == BnbConst.PMFStatus_ProposalDevelopment.ToString())
                {
                    if (this.ProjectTypeID == BnbConst.ProjectType_Project)
                    {
                        pmfDescription = "Project";
                        if (this.PMFStatusID == BnbConst.PMFStatus_ConceptDevelopment.ToString() ||
                            this.PMFStatusID == BnbConst.PMFStatus_ProposalDevelopment.ToString())
                        {
                            writer.AddAttribute("class", "pmfSelectedCell");
                        }
                    }
                    else
                    {
                        pmfDescription = "Project";
                        if (this.PMFStatusID == BnbConst.PMFStatus_ProposalDevelopment.ToString())
                        {
                            writer.AddAttribute("class", "pmfSelectedCell");
                        }
                    }
                }
                else if (lookupInfo.ID == BnbConst.PMFStatus_ContractNegotiation.ToString() || lookupInfo.ID == BnbConst.PMFStatus_ProjectStartup.ToString())
                {
                    pmfDescription = "Contract Negotiation";
                    if (this.PMFStatusID == BnbConst.PMFStatus_ContractNegotiation.ToString() ||
                           this.PMFStatusID == BnbConst.PMFStatus_ProjectStartup.ToString())
                    {
                        writer.AddAttribute("class", "pmfSelectedCell");
                    }
                    tempStatus += 1;
                    if (tempStatus < 2)
                        continue;
                    tempStatus = 0;
                }
                else if (lookupInfo.ID == BnbConst.PMFStatus_ProjectImplementation.ToString() ||
                  lookupInfo.ID == BnbConst.PMFStatus_ProjectCompletion.ToString())
                {
                    pmfDescription = "Project Implementation";
                    if (this.PMFStatusID == BnbConst.PMFStatus_ProjectImplementation.ToString() ||
                            this.PMFStatusID == BnbConst.PMFStatus_ProjectCompletion.ToString())
                    {
                        writer.AddAttribute("class", "pmfSelectedCell");
                    }
                    tempStatus += 1;
                    if (tempStatus < 2)
                        continue;
                    tempStatus = 0;
                }
                else if (lookupInfo.ID == BnbConst.PMFStatus_EndProjectCompletion.ToString())
                {
                    pmfDescription = "Project Closure";
                    if (this.PMFStatusID == BnbConst.PMFStatus_EndProjectCompletion.ToString())
                    {
                        writer.AddAttribute("class", "pmfSelectedCell");
                    }
                }  

                // insert a line break before the final space
                if (pmfDescription.LastIndexOf(" ") > -1)
                {
                    pmfDescription = String.Format("{0}<br/>{1}",
                        pmfDescription.Substring(0, pmfDescription.LastIndexOf(" ")),
                        pmfDescription.Substring(pmfDescription.LastIndexOf(" ") + 1));
                }
                                
                writer.AddAttribute("title", Server.HtmlEncode(pmfTooltip));
                writer.RenderBeginTag("td");
                writer.Write(pmfDescription);
                writer.RenderEndTag();
            }

           
            writer.RenderEndTag();
            writer.RenderEndTag();
        }

        public string VirtualRoot
        {
            get
            {
                if (m_virtualroot != "") return m_virtualroot;
                m_virtualroot = HttpRuntime.AppDomainAppVirtualPath;
                if (m_virtualroot == "/")
                    m_virtualroot = "";
                return m_virtualroot;
            }
        }

    }
}