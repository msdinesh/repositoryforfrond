<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContractReportingDatePage.aspx.cs" Inherits="Frond.EditPages.ContractReportingDatePage" %>
<%@ Register Src="../UserControls/PMFStatusPanel.ascx" TagName="PMFStatusPanel" TagPrefix="uc2" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Contract Reporting Date Page</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
    <link rel="stylesheet" href="../Css/calendar.css" type="text/css" />
</head>
<body onclick="hideCalendar(event);">
    <form id="form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/bnbaccountheader24silver.gif">
            </uc1:Title>
             <uc2:PMFStatusPanel ID="PMFStatusPanel2" runat="server" />
            &nbsp;
            <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons1" runat="server" CssClass="databuttons"
                UndoText="Undo" SaveText="Save" EditAllText="Edit All" NewText="New" HeightAllButtons="25px"
                UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" NewButtonEnabled="False">
            </bnbpagecontrol:BnbDataButtons>
            
            <bnbpagecontrol:BnbMessageBox ID="BnbMessageBox2" runat="server" CssClass="messagebox"
                SuppressRecordSavedMessage="false" PopupMessage="false"></bnbpagecontrol:BnbMessageBox><br />
                 <br>
            <table cellspacing="0px" width="100%">
                <tr style="height:20px;background-color: rgb(238,221,255); font-weight:bold; color: rgb(50,50,185) ">
                <td style="padding:5px">                    
                Contract Reports</td></tr>
            </table>
            <table border="0" class="fieldtable" width="100%">
                <tr>                   
                    <td>
                        <asp:HyperLink ID="hlGoToContractPage" runat="server">Back To Reports</asp:HyperLink></td>
                </tr>
                <tr>
                    <td class="label">
                        Report Type</td>
                    <td>
                        <bnbdatacontrol:BnbLookupControl ID="BnbClaimSubmittedBy" runat="server" CssClass="lookupcontrol"
                            Height="22px" Width="250px" DataMember="BnbGrantReport.GrantReportTypeID" DataTextField="Description"
                            ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuGrantReportType">
                        </bnbdatacontrol:BnbLookupControl>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Report Frequency</td>
                    <td>
                        <bnbdatacontrol:BnbLookupControl ID="Bnblookupcontrol1" runat="server" CssClass="lookupcontrol"
                            Height="22px" Width="250px" DataMember="BnbGrantReport.GrantReportFrequencyID"
                            DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuGrantReportFrequency">
                        </bnbdatacontrol:BnbLookupControl>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Report Status
                    </td>
                    <td>
                        <bnbdatacontrol:BnbLookupControl ID="Bnblookupcontrol2" runat="server" CssClass="lookupcontrol"
                            Height="22px" Width="250px" DataMember="BnbGrantReport.GrantReportStatusID" DataTextField="Description"
                            ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuGrantReportStatus">
                        </bnbdatacontrol:BnbLookupControl>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Report Name
                    </td>
                    <td>
                        <bnbdatacontrol:BnbTextBox ID="BnbTextBox2" runat="server" CssClass="textbox" Height="20px"
                            Width="100%" DataMember="BnbGrantReport.ReportName" MultiLine="false" Rows="1"
                            ControlType="Text" DataType="String"></bnbdatacontrol:BnbTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Date Due
                    </td>
                    <td>
                        <bnbdatacontrol:BnbDateBox ID="BnbDateBox5"  ShowCalender="true" runat="server" CssClass="datebox" Height="20px"
                            Width="90px" DataMember="BnbGrantReport.DateDue" DateCulture="en-GB" DateFormat="dd/MMM/yyyy">
                        </bnbdatacontrol:BnbDateBox>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Date Submitted
                    </td>
                    <td>
                        <bnbdatacontrol:BnbDateBox ID="BnbDateBox1"  ShowCalender="true" runat="server" CssClass="datebox" Height="20px"
                            Width="90px" DataMember="BnbGrantReport.DateSubmitted" DateCulture="en-GB" DateFormat="dd/MMM/yyyy">
                        </bnbdatacontrol:BnbDateBox>
                    </td>
                </tr>
                <tr>
                    <br />
                    
                    <asp:Panel ID="pnlReportingDates" runat="server">
                        <tr>
                            <td style="font-weight: bold; color: Silver;">
                                Reporting Dates</td>
                        </tr>
                        <td class="label">
                            Next Reporting Due Dates
                        </td>
                        <td>
                            <bnbdatagrid:BnbDataGridForDomainObjectList SortBy="ReportDueDate"  ShowDateCalender="true" ID="bnbdgReportingDate"
                                runat="server" DataGridType="Editable" AddButtonText="Add" AddButtonVisible="true"
                                DataProperties="ReportDueDate [BnbDateBox]" EditButtonText="Edit" EditButtonVisible="true"
                                CssClass="datagrid" Width="200px" DataMember="BnbGrantReport.ReportDueDates"
                                DataGridForDomainObjectListType="Editable" DeleteButtonVisible="true" />
                        </td>
                    </asp:Panel>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
