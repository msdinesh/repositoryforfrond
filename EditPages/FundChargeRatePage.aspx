<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Page language="c#" Codebehind="FundChargeRatePage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.FundChargeRatePage" %>
<%@ Register TagPrefix="bnbpagecontrols" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>FundChargeRatePage</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server"></uc1:title>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" NewWidth="55px" EditAllWidth="55px"
					SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px" NewText="New" EditAllText="Edit All" SaveText="Save"
					UndoText="Undo"></bnbpagecontrol:bnbdatabuttons>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:bnbmessagebox>
				<TABLE class="fieldtable" id="Table1" width="100%" border="0">
					<TR>
						<TD class="label" style="WIDTH: 50%">Recruitment Base
						</TD>
						<TD><bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol2" runat="server" CssClass="lookupcontrol" LookupTableName="lkuRecruitmentBase"
								LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" DataMember="BnbFundChargeRate.RecruitmentBaseId"
								Width="250px" Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 50%">VSO&nbsp;Group
						</TD>
						<TD><bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol3" runat="server" CssClass="lookupcontrol" LookupTableName="lkuVSOGroup"
								LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" DataMember="BnbFundChargeRate.VSOGroupId"
								Width="250px" Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 50%">Rate Type
						</TD>
						<TD><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" LookupTableName="lkuRateType"
								LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" DataMember="BnbFundChargeRate.RateTypeID"
								Width="250px" Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 50%">Rate
						</TD>
						<TD><bnbdatacontrol:bnbdecimalbox id="BnbDecimalBox1" runat="server" CssClass="textbox" DataMember="BnbFundChargeRate.Rate"
								Width="90px" Height="20px" FormatExpression="0.0000" TextAlign="Default"></bnbdatacontrol:bnbdecimalbox></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 50%">Valid From</TD>
						<TD><bnbdatacontrol:bnbdatebox id="BnbDateBox1" runat="server" CssClass="datebox" DataMember="BnbFundChargeRate.StartDate"
								Width="90px" Height="20px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 50%">Valid To</TD>
						<TD><bnbdatacontrol:bnbdatebox id="BnbDateBox2" runat="server" CssClass="datebox" DataMember="BnbFundChargeRate.EndDate"
								Width="90px" Height="20px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
				</TABLE>
				<P><bnbgenericcontrols:bnbhyperlink id="hlkFundChargeRateList" runat="server" NavigateUrl="FundChargeRateList.aspx">Fund Charge Rate List</bnbgenericcontrols:bnbhyperlink></P>
			</div>
		</form>
	</body>
</HTML>
