<%@ Page Language="C#" AutoEventWireup="true" Codebehind="PlacementWebTextPage.aspx.cs"
    Inherits="Frond.EditPages.PlacementWebTextPage" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register Src="../UserControls/MenuBar.ascx" TagName="MenuBar" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/Title.ascx" TagName="Title" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Programme Funding Page</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="frmPlacementWebText" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server">
            </uc1:Title>
            <uc1:navigationbar id="navigationBar" runat="server"></uc1:navigationbar>
            <div style="clear: both">
                <div style="float: left">
                    <bnbpagecontrol:BnbDataButtons ID="dataButtons" runat="server" UndoText="Undo"
                        SaveText="Save" EditAllText="Edit All" NewText="New" HeightAllButtons="25px"
                        UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" CssClass="databuttons" NewButtonEnabled="False">
                    </bnbpagecontrol:BnbDataButtons>
                    <bnbpagecontrol:BnbMessageBox ID="msgBox" runat="server" CssClass="messagebox">
                    </bnbpagecontrol:BnbMessageBox>
                    <table class="fieldtable" id="tblFieldTable" width="100%" border="0">
                        <tr>
                            <td class="label" width="50%">
                                Placement Ref</td>
                            <td width="50%">
                               <bnbdatacontrol:bnbdomainobjecthyperlink id="BnbDomainObjectHyperlinkRef" runat="server" RedirectPage="PlacementPage.aspx"
								GuidProperty="BnbPosition.FullPlacementReference" HyperlinkText="[BnbDomainObjectHyperlink]" Target="_top" DataMember="BnbPosition.FullPlacementReference"></bnbdatacontrol:bnbdomainobjecthyperlink>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="50%">
                                Language</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbLookupControl ID="cmbLanguage" runat="server" CssClass="lookupcontrol"
                                    Height="22px" Width="250px" DataTextField="Description" LookupControlType="ComboBox"
                                    LookupTableName="vlkuWebTextLanguages" DataMember="BnbPositionWebText.LanguageID"></bnbdatacontrol:BnbLookupControl>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="50%">
                                Placement Description</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbTextBox ID="txtPlacementDescription" runat="server" CssClass="textbox"
                                    Height="20px" Width="400px" DataMember="BnbPositionWebText.PlacementDescription"
                                    MultiLine="true" Rows="10" StringLength="0"></bnbdatacontrol:BnbTextBox>
                        </tr>
                        <tr>
                            <td class="label" width="50%">
                                Placement Description Link</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbTextBox ID="txtPlacementDescriptionLink" runat="server" CssClass="textbox"
                                    Height="20px" Width="250px" HyperLink="true" DataMember="BnbPositionWebText.PlacementDescriptionLink"
                                    MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:BnbTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="50%">
                                Qualifications Required</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbTextBox ID="txtQualificationsRequired" runat="server" CssClass="textbox"
                                    Height="20px" Width="400px" DataMember="BnbPositionWebText.QualificationsRequired"
                                    MultiLine="true" Rows="10" StringLength="0"></bnbdatacontrol:BnbTextBox>
                            </td>
                        </tr>
                    </table>
                </div>
    </form>
</body>
</html>
