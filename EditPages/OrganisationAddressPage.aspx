<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="cc1" Namespace="BonoboWebControls.ServicesControls" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="OrganisationAddressPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.OrganisationAddressPage" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register Src="../UserControls/GShowLocation.ascx" TagName="GShowLocation" TagPrefix="uc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML xmlns="http://www.w3.org/1999/xhtml">
	<HEAD>
		<title>OrganisationAddressPage</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		 <script language="javascript" src="../Javascripts/GoogleMap.js"></script>
	</HEAD>
	<body onload="initialize()" onunload="GUnload()">
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/bnbaddress24silver.gif"></uc1:title>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" NewWidth="55px" EditAllWidth="55px"
					SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px" NewText="New" EditAllText="Edit All" SaveText="Save"
					UndoText="Undo" NewButtonEnabled="False"></bnbpagecontrol:bnbdatabuttons>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:bnbmessagebox>
				<TABLE class="fieldtable" id="Table1" width="100%" border="0">
					<TR>
						<TD class="label" width="50%">Organisation</TD>
						<TD width="50%">
							<bnbdatacontrol:BnbDomainObjectHyperlink id="BnbDomainObjectHyperlink1" runat="server" DataMember="BnbCompany.CompanyName"
								Target="_top" HyperlinkText="[BnbDomainObjectHyperlink]" RedirectPage="OrganisationPage.aspx" GuidProperty="BnbCompany.ID"></bnbdatacontrol:BnbDomainObjectHyperlink></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Country</TD>
						<TD width="50%"><bnbdatacontrol:bnblookupcontrol id="uxCountry" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
								LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" DataMember="BnbCompanyAddress.Address.CountryID"
								LookupTableName="lkuCountry"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Postcode / ZIP</TD>
						<TD width="50%"><bnbdatacontrol:bnbtextbox id="uxPostcode" runat="server" CssClass="textbox" Width="208px" Height="20px" MultiLine="false"
								Rows="1" StringLength="0" DataMember="BnbCompanyAddress.Address.PostCode"></bnbdatacontrol:bnbtextbox>
							<asp:Button id="uxPostcodeSearch" runat="server" Text="Find" onclick="uxPostcodeSearch_Click"></asp:Button>
							<cc1:BnbAddressLookupComposite id="uxAddressLookupComposite" runat="server" CssClass="lookupcontrol" AccountCode="vso1111111"
								LicenseKey="XH57-MT97-CZ47-GH56" PasswordCapscan="492267" RegistrationCodeCapscan="00003304" AutoPostBack="True"
								Visible="False" WebFormManagerMode="True" Width="300px" Height="200px" ListBoxRows="6"></cc1:BnbAddressLookupComposite>
						</TD>
					</TR>
					<TR>
						<TD class="label" style="HEIGHT: 25px" width="50%">Line 1</TD>
						<TD style="HEIGHT: 25px" width="50%"><bnbdatacontrol:bnbtextbox id="uxLine1" runat="server" CssClass="textbox" Width="250px" Height="20px" MultiLine="false"
								Rows="1" StringLength="0" DataMember="BnbCompanyAddress.Address.AddressLine1"></bnbdatacontrol:bnbtextbox>
							<asp:Button id="uxAddressLine1Search" runat="server" Text="Find"></asp:Button>
</TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Line 2</TD>
						<TD width="50%"><bnbdatacontrol:bnbtextbox id="uxLine2" runat="server" CssClass="textbox" Width="250px" Height="20px" MultiLine="false"
								Rows="1" StringLength="0" DataMember="BnbCompanyAddress.Address.AddressLine2"></bnbdatacontrol:bnbtextbox></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Line 3</TD>
						<TD width="50%"><bnbdatacontrol:bnbtextbox id="uxLine3" runat="server" CssClass="textbox" Width="250px" Height="20px" MultiLine="false"
								Rows="1" StringLength="0" DataMember="BnbCompanyAddress.Address.AddressLine3"></bnbdatacontrol:bnbtextbox></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Postal Town / Line 4</TD>
						<TD width="50%"><bnbdatacontrol:bnbtextbox id="uxLine4" runat="server" CssClass="textbox" Width="250px" Height="20px" MultiLine="false"
								Rows="1" StringLength="0" DataMember="BnbCompanyAddress.Address.AddressLine4"></bnbdatacontrol:bnbtextbox></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">County / State</TD>
						<TD width="50%"><bnbdatacontrol:bnbtextbox id="uxCounty" runat="server" CssClass="textbox" Width="250px" Height="20px" MultiLine="false"
								Rows="1" StringLength="0" DataMember="BnbCompanyAddress.Address.County"></bnbdatacontrol:bnbtextbox></TD>
					</TR>
						<TR>
						<TD class="label" width="50%"></TD>
						<TD width="50%"> <input id="btnShowLocation" runat="Server" type="button" onclick="showLocation()" value="Show on map" /></TD>
                    </TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Date From</TD>
						<TD width="50%"><bnbdatacontrol:bnbdatebox id="BnbDateBox1" runat="server" CssClass="datebox" Width="90px" Height="20px" DateFormat="dd/mm/yyyy"
								DateCulture="en-GB" DataMember="BnbCompanyAddress.DateFrom"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Date To</TD>
						<TD width="50%"><bnbdatacontrol:bnbdatebox id="BnbDateBox2" runat="server" CssClass="datebox" Width="90px" Height="20px" DateFormat="dd/mm/yyyy"
								DateCulture="en-GB" DataMember="BnbCompanyAddress.DateTo"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">&nbsp;Main Tel</TD>
						<TD width="50%">
							<asp:TextBox id="uxTelephoneTextBox" runat="server"></asp:TextBox>
							<asp:Label id="uxTelephoneLabel" runat="server"></asp:Label></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">&nbsp;Main Fax</TD>
						<TD width="50%">
							<asp:TextBox id="uxFaxTextBox" runat="server"></asp:TextBox>
							<asp:Label id="uxFaxLabel" runat="server"></asp:Label></TD>
					</TR>
				</TABLE>
				 <p>
                <bnbpagecontrol:BnbSectionHeading ID="headingGeoDetails" runat="server" CssClass="sectionheading"
                    SectionHeading="Geographical Details" Width="100%" Height="20px" Collapsed="false">
                </bnbpagecontrol:BnbSectionHeading>
            </p>
            <table id="Table4" width="100%" border="0">
                <tr>
                    <td>
                        <uc2:GShowLocation HeadingVisibile="false" ID="showLocation" runat="server" /></td>
                   
                </tr>
                </table>
				<P></P>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons2" runat="server" CssClass="databuttons" NewWidth="55px" EditAllWidth="55px"
					SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px" NewText="New" EditAllText="Edit All" SaveText="Save"
					UndoText="Undo" NewButtonEnabled="False"></bnbpagecontrol:bnbdatabuttons>
			</div>
		</form>
	</body>
</HTML>
