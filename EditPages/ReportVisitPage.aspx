<%@ Page Language="c#" Codebehind="ReportVisitPage.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.EditPages.ReportVisitPage" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>ReportVisitPage</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet" />
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
              <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/bnbindividual24silver.gif">
            </uc1:Title> 
            <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons1" runat="server" CssClass="databuttons"
                NewWidth="55px" EditAllWidth="55px" SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px"
                NewText="New" EditAllText="Edit All" SaveText="Save" UndoText="Undo"></bnbpagecontrol:BnbDataButtons>
      
            <bnbpagecontrol:BnbMessageBox ID="BnbMessageBox1" runat="server" CssClass="messagebox">
            </bnbpagecontrol:BnbMessageBox>
            <bnbdatacontrol:BnbTextBox ID="hiddenReportOrVisitID" runat="server" CssClass="textbox"
                Height="20px" Width="250px" DataMember="BnbReportVisit.RepOrVisID" StringLength="0"
                Rows="1" MultiLine="False" ControlType="Hidden"></bnbdatacontrol:BnbTextBox>
     
       
        <div id="section1">
  
            <table id="Table1" width="100%" border="0" class="fieldtable">
                <tr>
                    <td class="label" width="50%">
                        Placement Ref</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbDomainObjectHyperlink ID="BnbDomainObjectHyperlink1" runat="server"
                            Target="_top" HyperlinkText="[BnbDomainObjectHyperlink]" RedirectPage="PlacementPage.aspx"
                            DataMember="BnbPosition.FullPlacementReference"></bnbdatacontrol:BnbDomainObjectHyperlink>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Contact</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl1" runat="server" CssClass="lookupcontrol"
                            Width="250px" Height="22px" DataMember="BnbReportVisit.ProgrammeOfficerID" DataTextField="Description"
                            ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboUser"></bnbdatacontrol:BnbLookupControl>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Report or Visit</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbLookupControl ID="lckReportOrVisit" runat="server" CssClass="lookupcontrol"
                            Width="250px" Height="22px" DataMember="BnbReportVisit.RepOrVisID" DataTextField="Description"
                            ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuReportVisit"
                            AutoPostBack="True"></bnbdatacontrol:BnbLookupControl>
                    </td>
                </tr>
            </table>
        </div>
        <div id="section2">
            <table id="Table2" width="100%" border="0" class="fieldtable">
                <tr>
                    <td class="label" width="50%" style="height: 12px">
                        Type</td>
                    <td width="50%" style="height: 12px">
                        <bnbdatacontrol:BnbLookupControl ID="bnblckRepVisTypeID" runat="server" CssClass="lookupcontrol"
                            Height="22px" Width="250px" DataMember="BnbReportVisit.RepVisTypeID" LookupTableName="lkuReportVisitType"
                            LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" ParentControlID="lckReportOrVisit"
                            ColumnRelatedToParent="ReportVisitID"></bnbdatacontrol:BnbLookupControl>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        <%=DateSentVolEmpLabel%>
                    </td>
                    <td width="50%">
                        <bnbdatacontrol:BnbDateBox ID="bnbdbxRepVisDateSentVolEmp_1" runat="server" CssClass="datebox"
                            Height="20px" Width="90px" DataMember="BnbReportVisit.RepVisDateDueVolEmp" DateFormat="dd/MMM/yyyy"
                            DateCulture="en-GB"></bnbdatacontrol:BnbDateBox>
                    </td>
                </tr>
            </table>
        </div>
        <div id="section3">
            <table id="Table3" width="100%" border="0" class="fieldtable">
                <tr>
                    <td class="label" width="50%">
                        Date received in PO</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbDateBox ID="bnbdbxRepVisDateDueVolEmp_2" runat="server" CssClass="datebox"
                            Height="20px" Width="90px" DataMember="BnbReportVisit.RepVisDateSentVolEmp" DateFormat="dd/MMM/yyyy"
                            DateCulture="en-GB"></bnbdatacontrol:BnbDateBox>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Date sent to VSOL</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbDateBox ID="bnbdbxRepVisDatSentVSOL_3" runat="server" CssClass="datebox"
                            Height="20px" Width="90px" DataMember="BnbReportVisit.RepVisDateReturned" DateFormat="dd/MMM/yyyy"
                            DateCulture="en-GB"></bnbdatacontrol:BnbDateBox>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Date received in VSOL</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbDateBox ID="bnbdbxRepVisDateReturned_4" runat="server" CssClass="datebox"
                            Height="20px" Width="90px" DataMember="BnbReportVisit.RepVisDatSentVSOL" DateFormat="dd/MMM/yyyy"
                            DateCulture="en-GB"></bnbdatacontrol:BnbDateBox>
                    </td>
                </tr>
            </table>
        </div>
        <div id="section4">
            <table id="Table4" width="100%" border="0" class="fieldtable">
                <tr>
                    <td class="label" width="50%">
                        Report filename</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbTextBox ID="bnbdbxRepVisFileName" runat="server" CssClass="textbox"
                            Height="20px" Width="250px" DataMember="BnbReportVisit.RepVisFileName" MultiLine="false"
                            Rows="1" StringLength="0"></bnbdatacontrol:BnbTextBox>
                    </td>
                </tr>
            </table>
        </div>
        <p>
            <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons2" runat="server" CssClass="databuttons"
                NewWidth="55px" EditAllWidth="55px" SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px"
                NewText="New" EditAllText="Edit All" SaveText="Save" UndoText="Undo"></bnbpagecontrol:BnbDataButtons>
        </p>

        </div>
    </form>

    <script language="javascript">
			var repvis = document.forms[0].elements['lckReportOrVisit']
			if(repvis == null) repvis = document.forms[0].elements['hiddenReportOrVisitID'];
			switch(repvis.value)
			{
				default:
					// hide all dependent controls
					document.getElementById("section2").style.display='none'
					document.getElementById("section3").style.display='none'
					document.getElementById("section4").style.display='none'
					break;
				case "1000":
					// show controls for reports only
					document.getElementById("section2").style.display='block';
					document.getElementById("section3").style.display='block';
					document.getElementById("section4").style.display='block';
					break;
				case "1001":
					// show controls for visit only
					document.getElementById("section2").style.display='block';
					document.getElementById("section3").style.display='none'
					document.getElementById("section4").style.display='block';
					break;	 
			}
    </script>

</body>
</html>
