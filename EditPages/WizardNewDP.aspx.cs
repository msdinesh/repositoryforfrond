using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using BonoboWebControls;
namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for WizardNewDP.
	/// </summary>
	public partial class WizardNewDP : System.Web.UI.Page
	{
		#region controls declaration

		protected System.Web.UI.WebControls.CompareValidator CompareValidator2;
		protected System.Web.UI.WebControls.Label lblNeedDOB;
		protected UserControls.WizardButtons wizardButtons;
		
		#endregion controls declaration
		#region page_load
		protected void Page_Load(object sender, System.EventArgs e)
		{

#if DEBUG
			if (!this.IsPostBack && this.Request.QueryString.ToString() == "")
				Response.Redirect("WizardNewDP.aspx?BnbIndividual=4B2EE430-B9B9-4466-902D-CE39BD4C92AD");
#endif
		
			BnbEngine.SessionManager.ClearObjectCache();
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);

			if (!this.IsPostBack)
			{
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				BnbWebFormManager.LogAction("New D And P Wizard", null, this);
			}

			// Put user code to initialize the page here
			if (this.Request.QueryString["BnbIndividual"] != null )
			{
					this.IndividualID = new Guid(this.Request.QueryString["BnbIndividual"]);
					
					wizardButtons.AddPanel(panelZero);
					wizardButtons.AddPanel(panelOne);
					wizardButtons.AddPanel(panelTwo);
					if (this.Request.QueryString["BnbRelatedIndividual"]!=null)
						this.RelatedIndividualID=new Guid(this.Request.QueryString["BnbRelatedIndividual"]);
			}
		}
		#endregion page_load
		#region property
		
		private Guid IndividualID
		{
			get
			{
				if (lblHiddenIndividualID.Value.Trim().Length >0)
					return new Guid(lblHiddenIndividualID.Value);
				else
					return Guid.Empty;
			}
			set{lblHiddenIndividualID.Value = value.ToString();}
		}
		
		
		private Guid RelatedIndividualID
		{
			get
			{
				if (lblHiddenRelatedIndividualID.Value.Trim().Length >0)
					return new Guid(lblHiddenRelatedIndividualID.Value);
				else
					return Guid.Empty;
			}
			set{lblHiddenRelatedIndividualID.Value = value.ToString();}
		}
		#endregion property

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			wizardButtons.ShowPanel +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
			wizardButtons.ValidatePanel +=new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
			wizardButtons.CancelWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
			wizardButtons.FinishWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dataGridDependants.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dataGridDependants_ItemDataBound);

		}
		#endregion

		#region public methods

		public void initWizard()
		{
			// event type lookup on panel zero
		}

		public void SetupPanelZero()
		{

			// nothing
			BnbIndividual indv = BnbIndividual.Retrieve(this.IndividualID);
			uxIndividualSummaryLabel1.Text = indv.InstanceDescription;

		}

		public void SetupPanelOne()
		{
			string strFirstname = txtFirstname.Text;
			string strSurname = txtSurname.Text;

			BnbCriteria dupCriteria = new BnbCriteria();
			if (strSurname.Trim().Length > 0)
			{
				BnbTextCondition surnameMatch = new BnbTextCondition("tblIndividualKeyInfo_Surname", strSurname, BnbTextCompareOptions.StartOfField);
				dupCriteria.QueryElements.Add(surnameMatch);
			}
			if (strFirstname.Trim().Length > 0)
			{
				
				if (strFirstname.Length > 5)
					strFirstname = strFirstname.Substring(0,5);
				
				BnbTextCondition forenameMatch = new BnbTextCondition("tblIndividualKeyInfo_Forename", strFirstname, BnbTextCompareOptions.StartOfField);
				dupCriteria.QueryElements.Add(forenameMatch);
			}
			if (txtRefNo.Text != "")
			{
				BnbTextCondition refNoMatch = new BnbTextCondition("tblIndividualKeyInfo_old_indv_id", txtRefNo.Text, BnbTextCompareOptions.ExactMatch);
				dupCriteria.QueryElements.Add(refNoMatch);
			}
			if (!dbDateOfBirth.IsEmpty)
			{
				BnbDateCondition dobMatch =new BnbDateCondition("tblIndividualKeyInfo_DateOfBirth", dbDateOfBirth.Date, dbDateOfBirth.Date);
				dobMatch.DateCompareOptions=BnbDateCompareOptions.FieldBetweenDates;
				dupCriteria.QueryElements.Add(dobMatch);				
			}
				
			BnbQuery dupQuery = new BnbQuery("vwBonoboIndividualDuplicateCheckFast", dupCriteria);
			BnbQueryDataSet dupDataSet = dupQuery.Execute();
			DataTable dupResults = BonoboWebControls.BnbWebControlServices.GetHtmlEncodedDataTable(dupDataSet.Tables["Results"]);
					
			if (dupResults.Rows.Count > 0)
			{
				panelDupIndividual.Visible = true;
				labelNoDuplicates.Visible = false;
				dataGridDependants.DataSource = dupResults;
				dataGridDependants.DataBind();
			}
			else
			{
				panelDupIndividual.Visible = false;
				labelNoDuplicates.Visible = true;
			}

			
		}


		/// <summary>
		/// Creates the individual
		/// </summary>
		private void SetupPanelTwo()
		{
			SetSelectedIndividual();
			cmbIntendedLocation.Visible=false;
			cmbActualLocation.Visible=false;

		
			BnbIndividual objIndividual = BnbIndividual.Retrieve(this.IndividualID);
			uxIndividualSummaryLabel2.Text = objIndividual.InstanceDescription;
			if (objIndividual.Applications.Count> 0)
			{
				cmbIntendedLocation.Visible = true;
				cmbActualLocation.Visible = true;
			}

			//If at least one of them is a volunteer show the intended and actual combos
			if (this.RelatedIndividualID!=Guid.Empty)
			{
				BnbIndividual objRelatedIndividual=BnbIndividual.Retrieve(this.RelatedIndividualID);
				uxForenameFinalTextBox.Text = objRelatedIndividual.Forename;
				uxForenameFinalTextBox.ReadOnly = true;
				uxForenameFinalTextBox.CssClass = "textboxdisabled";
				uxSurnameFinalTextBox.Text = objRelatedIndividual.Surname;
				uxSurnameFinalTextBox.ReadOnly = true;
				uxSurnameFinalTextBox.CssClass = "textboxdisabled";
				uxDateOfBirthFinal.Date = objRelatedIndividual.DateOfBirth;
				uxDateOfBirthFinal.ReadOnly = true;
				uxDateOfBirthFinal.CssClass = "textboxdisabled";
				uxPartnerRefNoLabel.Text = objRelatedIndividual.RefNo;
				if (objRelatedIndividual.Applications.Count > 0)
				{
					cmbIntendedLocation.Visible = true;
					cmbActualLocation.Visible = true;
				}
			} 
			else
			{
				// we are creating a new record, copy from first page
				uxForenameFinalTextBox.Text = txtFirstname.Text;
				uxSurnameFinalTextBox.Text = txtSurname.Text;
				uxDateOfBirthFinal.Date = dbDateOfBirth.Date;
				uxPartnerRefNoLabel.Text = "(New)";
			}
		}

		#endregion public methods

		#region private methods

		private void CreateNewDependant()
		{
			BnbIndividual objRelatedIndividual = null;
			//if no-one has been selected
			BnbEditManager em = new BnbEditManager();

			if (this.RelatedIndividualID == Guid.Empty)
			{
				//Create new individual
				objRelatedIndividual = new BnbIndividual(BnbIndividual.BnbIndividualType.KeyInfoOnly);
				objRelatedIndividual.RegisterForEdit(em);	
				objRelatedIndividual.Forename = uxForenameFinalTextBox.Text;
				objRelatedIndividual.Surname = uxSurnameFinalTextBox.Text;
				if (!uxDateOfBirthFinal.IsValid)
				{
					em.EditCompleted();
					lblPanelTwoFeedback.Text = "Date of Birth was not in 'dd/mmm/yyyy' format";
					return;
				}
				if (!uxDateOfBirthFinal.IsEmpty)
					objRelatedIndividual.DateOfBirth = uxDateOfBirthFinal.Date;
				
			}
			else
			{
				objRelatedIndividual = BnbIndividual.Retrieve(this.RelatedIndividualID);
			}
			
			BnbIndividual objIndividual = BnbIndividual.Retrieve(this.IndividualID);
			BnbRelative objRelative = new BnbRelative(true);
			objRelative.RegisterForEdit(em);
			objRelative.Individual = objIndividual;
			objRelative.RelatedIndividual = objRelatedIndividual;
			objRelative.RelationshipTypeID = (int) cmbRelationshipType.SelectedValue;
			
			if (objIndividual.Applications.Count > 0 || objRelatedIndividual.Applications.Count > 0)
			{
				//Add a relative application
				BnbRelativeApplication objRelativeApplication = new BnbRelativeApplication(true);
				objRelativeApplication.RegisterForEdit(em);
				objRelativeApplication.Relative = objRelative;
				if (objIndividual.Applications.Count > 0)
				{
					objRelativeApplication.ApplicationID = objIndividual.CurrentApplication.ID;
				}

				if (objRelatedIndividual.Applications.Count > 0)
				{
					objRelativeApplication.RelatedApplicationID = objRelatedIndividual.CurrentApplication.ID;
				}

				objRelativeApplication.IntendedLocationID = (int)cmbIntendedLocation.SelectedValue;
				objRelativeApplication.ActualLocationID = (int) cmbActualLocation.SelectedValue;
			}

			try
			{
				em.SaveChanges();
				Response.Redirect("IndividualPage.aspx?BnbIndividual=" + objIndividual.ID.ToString());
			}
			catch(BnbProblemException pe)
			{
				lblPanelTwoFeedback.Text = "Unfortunately, the dependent relationship could not be created due to the following problems: <br/>";
				foreach(BnbProblem p in pe.Problems)
					lblPanelTwoFeedback.Text += p.Message + "<br/>";
			}
		}						


		#endregion private methods

		#region events
		private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			if (e.CurrentPanel == 0)
			{
				this.SetupPanelZero();
			}
			if (e.CurrentPanel == 1)
			{
				this.SetupPanelOne();
			}
			if (e.CurrentPanel == 2)
			{
				this.SetupPanelTwo();
			}
		}

		private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			Response.Redirect("IndividualPage.aspx?BnbIndividual=" + this.IndividualID.ToString());
		}

		private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			this.CreateNewDependant();
		}

		/// <summary>
		/// </summary>
		/// <param name="Sender"></param>
		/// <param name="e"></param>
		private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
		{
			if (e.CurrentPanelHtmlID == panelZero.ID)
			{
				bool enoughCriteria = false;
				if (txtRefNo.Text != "")
					enoughCriteria = true;
				if (!dbDateOfBirth.IsEmpty && (txtFirstname.Text != "" || txtSurname.Text != ""))
					enoughCriteria = true;
				if (txtFirstname.Text != "" && txtSurname.Text != "")
					enoughCriteria = true;
				if (!enoughCriteria)
				{
					lblPanelZeroFeedback.Text = "Please enter a Ref No, or Forename and Surname";
					e.Proceed = false;
				}
			}
		}

		/// <summary>
		/// This works out which individual you've selected and sets RelatedIndividual.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public void SetSelectedIndividual() 
		{

			if (Request.Form["myradiogroup"] != null)
			{
				foreach (DataGridItem i in dataGridDependants.Items)
				{
					if (i.ItemType==ListItemType.AlternatingItem || i.ItemType==ListItemType.Item)
					{
						Label r;
						r=(Label) i.FindControl("Label2");
					
						if (r.Text.IndexOf(Request.Form["myradiogroup"])>0)
						{
							this.RelatedIndividualID = new Guid(dataGridDependants.Items[i.ItemIndex].Cells[1].Text);
							break;
						}
					}
				}
			}
		}

		#endregion events

		private void dataGridDependants_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if ((e.Item.ItemType == ListItemType.AlternatingItem) || (e.Item.ItemType == ListItemType.Item))
			{
				Label r;
				r = (Label) e.Item.FindControl("Label2");
				r.Text = "<input type=radio name='myradiogroup' value=" + e.Item.Cells[1].Text + ">";
			}
		}
	
	}
}
