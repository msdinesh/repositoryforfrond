using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using BonoboWebControls;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for FundCodeList.
	/// </summary>
	public partial class FundCodeList : System.Web.UI.Page
	{
	
		protected UserControls.Title titleBar;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			HttpContext.Current.Session["PageState"] = PageState.View;
			// Put user code to initialize the page here
			if (!this.IsPostBack)
			{
				BnbWorkareaManager.CheckPageAccess(this);
				titleBar.TitleText = "Fund Code Admin";
				ShowFundCodes();
				if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(
					BnbConst.Permission_ProgrammeFundingAdmin))
				{
					dgrFundCode.ViewLinksVisible = true;
					hlkNewFundCode.Visible = true;
				}
				else
				{
					dgrFundCode.ViewLinksVisible = false;
					hlkNewFundCode.Visible = false;
					lblNoPermission.Text = "(You do not have permission to edit Fund Codes)";
				}
				BnbWebFormManager.LogAction(titleBar.TitleText, null, this);
			}
			BnbWorkareaManager.SetPageStyleOfUser(this);

			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void ShowFundCodes()
		{
			// blank criteria
			BnbCriteria showAll = new BnbCriteria();
			// feed it to datagrid
			dgrFundCode.Criteria = showAll;
			dgrFundCode.Visible = true;
			dgrFundCode.DataBind();


		}
	}
}
