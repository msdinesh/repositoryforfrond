using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboDomainObjects;
using BonoboEngine;
using BonoboEngine.Query;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for WizardNewMonitor.
    /// </summary>
    public partial class WizardNewMonitor : System.Web.UI.Page
    {
        //protected UserControls.WizardButtons wizardButtons;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            wizardButtons.AddPanel(panelZero);
            wizardButtons.AddPanel(panelOne);

        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
            wizardButtons.CancelWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
            wizardButtons.FinishWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
            wizardButtons.ValidatePanel += new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
            wizardButtons.ShowPanel += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        private void wizardButtons_ShowPanel(object Sender, UserControls.WizardButtons.WizardEventArgs e)
        {
            if (e.CurrentPanel == 0)
            {
                if (labelPlacementRef.Text == "")
                    this.SetupPanelZero();
            }
            if (e.CurrentPanel == 1)
            {
                if (dropDownAchievement.Items.Count == 0)
                    this.SetupPanelOne();

            }
        }

        private void SetupPanelZero()
        {
            BnbPosition ownerPos = BnbPosition.Retrieve(this.PositionID);
            labelPlacementRef.Text = HttpUtility.HtmlEncode(ownerPos.FullPlacementReference);
            labelPlacementRef2.Text = HttpUtility.HtmlEncode(ownerPos.FullPlacementReference);
            if (ownerPos.PlacedService != null &&
                ownerPos.PlacedService.Application != null &&
                ownerPos.PlacedService.Application.Individual != null)
            {
                labelVolunteerName.Text = HttpUtility.HtmlEncode(ownerPos.PlacedService.Application.Individual.InstanceDescription);
                labelVolunteerName2.Text = HttpUtility.HtmlEncode(ownerPos.PlacedService.Application.Individual.InstanceDescription);
            }
            else
            {
                labelVolunteerName.Text = "(Placement not filled)";
                labelVolunteerName2.Text = "(Placement not filled)";
            }
            labelEmployerName.Text = HttpUtility.HtmlEncode(ownerPos.Role.Employer.EmployerName);
            labelEmployerName2.Text = HttpUtility.HtmlEncode(ownerPos.Role.Employer.EmployerName);

            if (ownerPos.CurrentMonitor != null)
            {
                labelPlacementInfo.Text = String.Format("Monitor and Evaluation info already exists for this Placement. "
                    + "Click <a href=\"MonitorEvaluationPage.aspx?BnbPosition={0}&BnbMonitor={1}\">here</a> to view it.",
                    ownerPos.ID.ToString(), ownerPos.CurrentMonitor.ID.ToString());

                dropDownLeadObjective.Visible = false;
                dropDownSecondaryObjective.Visible = false;
            }
            else
            {
                if (ownerPos.PositionObjectives.Count == 0)
                {
                    labelPlacementInfo.Text = "You must enter some VSO Goals for the Placement. Please select a lead and (optionally)"
                        + " a secondary VSO Goal from the following lists, and then Press Next.";
                    this.PopulateDropDown(dropDownLeadObjective, "lkuMonitorObjective");
                    this.PopulateDropDown(dropDownSecondaryObjective, "lkuMonitorObjective");
                }
                else
                {
                    labelPlacementInfo.Text = "The Placement already has the following VSO Goals. You will be able to edit them later on if you need to. Press Next to continue.";
                    dropDownLeadObjective.Visible = false;
                    dropDownSecondaryObjective.Visible = false;
                    labelLeadGoal.Text = "";
                    labelSecondaryGoals.Text = "";
                    BnbLookupDataTable objectiveLookup = BnbEngine.LookupManager.GetLookup("lkuMonitorObjective");
                    foreach (BnbPositionObjective poLoop in ownerPos.PositionObjectives)
                    {
                        string objectiveDesc = "(Unknown)";
                        BnbLookupRow foundRow = objectiveLookup.FindByID(poLoop.MonitorObjectiveID);
                        if (foundRow != null)
                            objectiveDesc = HttpUtility.HtmlEncode(foundRow.Description);

                        if (poLoop.ID == ownerPos.PositionObjectives.GetLeadID())
                            labelLeadGoal.Text = objectiveDesc;
                        else
                        {
                            if (labelSecondaryGoals.Text.Length > 0)
                                labelSecondaryGoals.Text += "<br/>";

                            labelSecondaryGoals.Text += objectiveDesc;
                        }
                    }
                    if (labelSecondaryGoals.Text == "")
                        labelSecondaryGoals.Text = "(None)";
                }
            }
        }

        private void SetupPanelOne()
        {
            BnbPosition ownerPos = BnbPosition.Retrieve(this.PositionID);
            labelPlacementRef2.Text = HttpUtility.HtmlEncode(ownerPos.FullPlacementReference);

            this.PopulateDropDown(dropDownAchievement, "lkuMonitorResponse");
            this.PopulateDropDown(dropDownSatVolPlacement, "lkuMonitorSatisfaction");
            this.PopulateDropDown(dropDownSatVolVSO, "lkuMonitorSatisfaction");
            this.PopulateDropDown(dropDownSatEmpVolunteer, "lkuMonitorSatisfaction");
            this.PopulateDropDown(dropDownSatEmpVSO, "lkuMonitorSatisfaction");
        }

        private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            Response.Redirect("../Menu.aspx");
        }

        private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            bool continueSave = false;

            if (dropDownAchievement.SelectedValue != "-1" ||
                dropDownSatEmpVolunteer.SelectedValue != "-1" ||
                dropDownSatEmpVSO.SelectedValue != "-1" ||
                dropDownSatVolPlacement.SelectedValue != "-1" ||
                dropDownSatVolVSO.SelectedValue != "-1")
                continueSave = true;
            else
            {
                labelPanelOneFeedback.Text = "You must fill in at least some of the details before pressing Finish.";
                continueSave = false;
            }

            if (continueSave)
            {
                BnbEditManager em = new BnbEditManager();
                BnbPosition ownerPos = BnbPosition.Retrieve(this.PositionID);
                try
                {
                    ownerPos.RegisterForEdit(em);
                }
                catch (BnbLockException le)
                {
                    labelPanelOneFeedback.Text = le.Message;
                    continueSave = false;
                }
                if (continueSave)
                {
                    // if drop downs were used...
                    if (dropDownLeadObjective.Items.Count > 0)
                    {
                        // must add objectives for placement
                        BnbPositionObjective newPosObj1 = new BnbPositionObjective(true);
                        newPosObj1.RegisterForEdit(em);
                        ownerPos.PositionObjectives.Add(newPosObj1);
                        newPosObj1.MonitorObjectiveID = int.Parse(dropDownLeadObjective.SelectedValue);
                        newPosObj1.Order = 1;
                        if (dropDownSecondaryObjective.SelectedValue != "-1")
                        {
                            BnbPositionObjective newPosObj2 = new BnbPositionObjective(true);
                            newPosObj2.RegisterForEdit(em);
                            ownerPos.PositionObjectives.Add(newPosObj2);
                            newPosObj2.MonitorObjectiveID = int.Parse(dropDownSecondaryObjective.SelectedValue);
                            newPosObj2.Order = 2;
                        }
                    }
                    BnbMonitor newMonitor = new BnbMonitor(true);

                    newMonitor.RegisterForEdit(em);
                    ownerPos.Monitors.Add(newMonitor);
                    newMonitor.APOVolunteerID = int.Parse(dropDownAchievement.SelectedValue);
                    newMonitor.SatVolunteerPlacementID = int.Parse(dropDownSatVolPlacement.SelectedValue);
                    newMonitor.SatVolunteerVSOID = int.Parse(dropDownSatVolVSO.SelectedValue);
                    newMonitor.SatEmployerPlacementID = int.Parse(dropDownSatEmpVolunteer.SelectedValue);
                    newMonitor.SatEmployerVSOID = int.Parse(dropDownSatEmpVSO.SelectedValue);
                    if (textComments.Text != "")
                        newMonitor.Comments = HttpUtility.HtmlEncode(textComments.Text);
                    try
                    {
                        em.SaveChanges();
                        Response.Redirect(String.Format("MonitorEvaluationPage.aspx?BnbPosition={0}&BnbMonitor={1}",
                            newMonitor.Position.ID, newMonitor.ID));
                    }
                    catch (BnbProblemException pe)
                    {
                        labelPanelOneFeedback.Text = "Sorry, the data could not be saved due to the following problems:";
                        foreach (BnbProblem p in pe.Problems)
                            labelPanelOneFeedback.Text += "<br/>" + p.Message;
                        em.EditCompleted();
                    }
                }
            }
        }

        private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
        {
            if (e.CurrentPanel == 0)
            {
                if (dropDownLeadObjective.Visible)
                {
                    if (dropDownLeadObjective.SelectedValue == "-1")
                    {
                        labelPanelZeroFeedback.Text = "You must select a lead VSO Goal before pressing Next.";
                        e.Proceed = false;
                    }
                    else
                    {
                        if (dropDownSecondaryObjective.SelectedValue == dropDownLeadObjective.SelectedValue)
                        {
                            labelPanelZeroFeedback.Text = "Lead and Secondary VSO Goal cannot be the same.";
                            e.Proceed = false;
                        }
                    }
                }
            }

        }

        public Guid PositionID
        {
            get
            {
                if (this.Request.QueryString["BnbPosition"] != null)
                    return new Guid(this.Request.QueryString["BnbPosition"]);
                else
                    return Guid.Empty;
            }
        }

        private void PopulateDropDown(DropDownList dropDown, string lookupName)
        {
            BnbLookupDataTable lookupData = BnbEngine.LookupManager.GetLookup(lookupName);
            DataView filterLookup = new DataView(lookupData);
            filterLookup.RowFilter = "Exclude = 0";
            dropDown.DataSource = filterLookup;
            dropDown.DataTextField = "Description";
            dropDown.DataValueField = "IntID";
            dropDown.DataBind();
            ListItem nullItem = new ListItem("", "-1");
            nullItem.Selected = true;
            dropDown.Items.Add(nullItem);
        }
    }
}
