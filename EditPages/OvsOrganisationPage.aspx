<%@ Page Language="c#" Codebehind="OvsOrganisationPage.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.EditPages.OvsOrganisationPage" %>

<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register Src="../UserControls/NavigationBar.ascx" TagName="NavigationBar" TagPrefix="uc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>OvsOrganisationPage</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet" />
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/bnbcompany24silver.gif">
            </uc1:Title>
            <uc2:NavigationBar ID="navigationBar" runat="server"></uc2:NavigationBar>
            <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons1" runat="server" UndoText="Undo"
                SaveText="Save" EditAllText="Edit All" NewText="New" HeightAllButtons="25px"
                UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" CssClass="databuttons">
            </bnbpagecontrol:BnbDataButtons>
            <bnbpagecontrol:BnbMessageBox ID="BnbMessageBox1" runat="server" CssClass="messagebox">
            </bnbpagecontrol:BnbMessageBox>           
            <bnbpagecontrol:BnbTabControl ID="BnbTabControl1" runat="server"></bnbpagecontrol:BnbTabControl>
            <asp:Panel ID="OrganisationDetailsTab" runat="server" CssClass="tcTab" BnbTabName="Organisation Details">
              
                <table id="Table1" width="100%" border="0" class="fieldtable">
                    <tr>
                        <td class="label" style="width: 40%">
                            Logged By</td>
                        <td>
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl1" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                DataMember="BnbOrganisation.CreatedBy" LookupTableName="vlkuBonoboUser"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width: 40%">
                            Logged On</td>
                        <td>
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox1" runat="server" CssClass="datebox" Width="90px"
                                Height="20px" DataMember="BnbOrganisation.CreatedOn" DateCulture="en-GB" DateFormat="dd/mm/yyyy">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width: 40%">
                            Country</td>
                        <td>
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl3" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                DataMember="BnbOrganisation.CountryID" LookupTableName="lkuCountry"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width: 40%">
                            Ovs Organisation</td>
                        <td>
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox1" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" StringLength="0" Rows="1" MultiLine="false" DataMember="BnbOrganisation.OrganisationName">
                            </bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width: 40%">
                            Abbreviation</td>
                        <td>
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox2" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" StringLength="0" Rows="1" MultiLine="false" DataMember="BnbOrganisation.OrganisationShortName">
                            </bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width: 40%">
                            Ovs Organisation Type</td>
                        <td>
                            <p>
                                <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList2"
                                    runat="server" CssClass="datagrid" Width="200px" DataGridType="Editable" AddButtonVisible="true"
                                    EditButtonText="Edit" AddButtonText="Add" EditButtonVisible="true" DeleteButtonVisible="True"
                                    DataMember="BnbOrganisation.OrganisationEmployerTypes" DataProperties="EmployerTypeID [BnbLookupControl:lkuEmployerType]">
                                </bnbdatagrid:BnbDataGridForDomainObjectList>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width: 40%">
                            Comments</td>
                        <td>
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox3" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" StringLength="0" Rows="3" MultiLine="True" DataMember="BnbOrganisation.Comments">
                            </bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                </table>
              
            </asp:Panel>           
            <asp:Panel ID="AddressesTab" runat="server" CssClass="tcTab" BnbTabName="Addresses">
                <p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView2" runat="server" CssClass="datagrid"
                        Width="100%" FieldName="tblOrganisation_OrganisationID" QueryStringKey="BnbOrganisation"
                        ViewName="vwBonoboOrgansisationAddress" ViewLinksText="View" NewLinkText="New Address"
                        NewLinkVisible="true" ViewLinksVisible="true" GuidKey="tblAddress_AddressID"
                        DomainObject="BnbOrganisationAddress" RedirectPage="OvsOrganisationAddressPage.aspx">
                    </bnbdatagrid:BnbDataGridForView>
                </p>
            </asp:Panel>           
            <asp:Panel ID="EmployersTab" runat="server" CssClass="tcTab" BnbTabName="Employers">
                <p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView1" runat="server" CssClass="datagrid"
                        Width="100%" ViewLinksVisible="true" NewLinkVisible="true" NewLinkText="New Employer"
                        ViewLinksText="View" ViewName="vwBonoboOrganisationEmployers" QueryStringKey="BnbOrganisation"
                        FieldName="tblEmployer_OrganisationID" RedirectPage="EmployerPage.aspx" DomainObject="BnbEmployer"
                        GuidKey="tblEmployer_EmployerID" DESIGNTIMEDRAGDROP="128"></bnbdatagrid:BnbDataGridForView>
                </p>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
