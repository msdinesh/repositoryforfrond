<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="FundCodePage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.FundCodePage" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrols" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>FundCodePage</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server"></uc1:title>
				<bnbpagecontrols:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" EditAllButtonVisibile="True"
					NewButtonVisible="True" NewWidth="55px" EditAllWidth="55px" SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px"
					NewText="New" EditAllText="Edit All" SaveText="Save" UndoText="Undo"></bnbpagecontrols:bnbdatabuttons>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:bnbmessagebox>
				<TABLE class="fieldtable" id="Table1" style="HEIGHT: 54px" width="100%" border="0">
					<TR>
						<TD class="label" style="WIDTH: 20%; HEIGHT: 23px"><P align="left">Fund Code</P>
						</TD>
						<TD class="label" style="WIDTH: 100%; HEIGHT: 23px">
							<P align="left">Fund Code Description</P>
						</TD>
					</TR>
					<TR>
						<TD><bnbdatacontrol:bnbtextbox id="BnbTextBox1" runat="server" CssClass="textbox" DataMember="BnbFundCode.FundCode"
								Width="100%" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text" DataType="String"></bnbdatacontrol:bnbtextbox></TD>
						<TD><bnbdatacontrol:bnbtextbox id="BnbTextBox2" runat="server" CssClass="textbox" DataMember="BnbFundCode.FundCodeDescription"
								Width="100%" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text" DataType="String"></bnbdatacontrol:bnbtextbox></TD>
					</TR>
				</TABLE>
				<P>
					<bnbgenericcontrols:BnbHyperlink id="hlkFundCodeList" runat="server" NavigateUrl="FundCodeList.aspx">Fund Code List</bnbgenericcontrols:BnbHyperlink></P>
			</div>
		</form>
	</body>
</HTML>
