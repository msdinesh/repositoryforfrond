using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;
using BonoboEngine;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for JobPage.
    /// </summary>
    public partial class JobPage : System.Web.UI.Page
    {
        protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox2;

        // protected UserControls.SFNavigationBar SFNavigationBar1;
        //<Integration>
        protected UserControls.NavigationBar navigationBar;
        protected UserControls.Title titleBar;
        //</Integration>
        private BnbWebFormManager bnb = null;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
#if DEBUG
            if (Request.QueryString.ToString() == "")
                BnbWebFormManager.ReloadPage("BnbEmployer=282DA8D0-5835-11D6-AD3D-000102769F09&BnbRole=282DA8D4-5835-11D6-AD3D-000102769F09");
            //"BnbEmployer=ABFEAAF2-AAE8-11D4-908F-00508BACE998&BnbRole=2cb21526-68fa-11d7-a668-00010277d9ba");
            else
                bnb = new BnbWebFormManager(this, "BnbRole");
#else
			bnb = new BnbWebFormManager(this,"BnbRole");
#endif
            // turn on logging for this page
            bnb.LogPageHistory = true;
            //<Integartion>
            bnb.PageNameOverride = "Job";
            BnbWorkareaManager.SetPageStyleOfUser(this);

            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null)
                    titleBar.TitleText = bnb.PageTitleDescriptionCurrent;

                doPlacementWizardHyperLink();
            }
            //</Integartion>
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            BnbDataButtons1.NewClick += new EventHandler(BnbDataButtons1_NewClick);
            BnbDataButtons1.SaveClick += new EventHandler(BnbDataButtons1_SaveClick);
            BnbDataButtons1.EditAllClick += new EventHandler(BnbDataButtons1_EditAllClick);
            BnbDataButtons1.UndoClick += new EventHandler(BnbDataButtons1_UndoClick);
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new EventHandler(JobPage_Init);

        }
        #endregion

        private void BnbDataButtons1_NewClick(object sender, EventArgs e)
        {
            bnblckRoleSkill.Visible = true;
        }

        private void BnbDataButtons1_UndoClick(object sender, EventArgs e)
        {
            bnblckRoleSkill.Visible = false;
        }

        private void BnbDataButtons1_SaveClick(object sender, EventArgs e)
        {
            // sets the Order attribute for the new RoleSkill object to 1
            if (bnblckRoleSkill.Visible)
                ((BnbRoleSkill)bnb.ObjectTree.DomainObjectDictionary["BnbRoleSkill"]).Order = 1;
        }

        private void BnbDataButtons1_EditAllClick(object sender, EventArgs e)
        {
            bnblckRoleSkill.Visible = false;
        }

        private void JobPage_Init(object sender, EventArgs e)
        {
            navigationBar.RootDomainObject = "BnbRole";
        }

        private void doPlacementWizardHyperLink()
        {

            string roleID = Request.QueryString["BnbRole"];
            if (roleID == null || roleID.ToLower() == "new")
            {
                // new mode: hide hyperlinks
                hypNewPlacement.Visible = false;
            }
            else
            {
                // put right guid in url
                hypNewPlacement.Visible = true;
                hypNewPlacement.NavigateUrl = String.Format("WizardNewPlacement.aspx?BnbRole={0}",
                    roleID);
            }
        }
    }
}
