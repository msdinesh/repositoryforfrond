<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls"
    Assembly="BonoboWebControls" %>

<%@ Page Language="C#" AutoEventWireup="true" Codebehind="SFApplicationPage.aspx.cs"
    Inherits="Frond.EditPages.SFApplicationPage" %>

<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PlacementPage</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="form1" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server"></uc1:Title>
        <%--    <uc1:NavigationBar ID="navigationBar" runat="server"></uc1:NavigationBar>--%>
            <div style="clear: both">
                <div style="float: left">
                    <bnbpagecontrol:BnbDataButtons ID="dataButtons" runat="server" UndoText="Undo" SaveText="Save"
                        EditAllText="Edit All" NewText="New" HeightAllButtons="25px" UndoWidth="55px"
                        SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" CssClass="databuttons" NewButtonEnabled="False">
                    </bnbpagecontrol:BnbDataButtons>
                    <bnbpagecontrol:BnbMessageBox ID="msgBox" runat="server" CssClass="messagebox"></bnbpagecontrol:BnbMessageBox>
                    <table class="fieldtable" id="Table2" cellspacing="1" cellpadding="1">
                                     <tr><td class="label" width="25%">
                                Placement Ref:</td>
                    <td colspan ="3">
                    <bnbdatacontrol:BnbDomainObjectHyperlink ID="BnbDomainObjectHyperlink1" runat="server"
                                    DataMember="BnbApplication.PlacedService.Position.FullPlacementReference" ButtonText="[BnbDomainObjectHyperlink]"
                                    RedirectPage="PlacementPage.aspx" GuidProperty="BnbApplication.PlacedService.Position.ID"
                                    Target="_top"></bnbdatacontrol:BnbDomainObjectHyperlink>
                    </td>
                    </tr>
                      <tr>
                            <td class="label" width="25%">
                                End of Service Form In
                            </td>
                            <td width="25%">
                                <bnbdatacontrol:BnbDateBox ID="BnbDateBox14" runat="server" CssClass="datebox" Height="20px"
                                    Width="90px" DataMember="BnbApplication.EOSFormInDate" DateCulture="en-GB" DateFormat="dd/MMM/yyyy">
                                </bnbdatacontrol:BnbDateBox>
                            </td>
                            <td class="label" width="25%">
                                Home Base Return</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbDateBox ID="BnbDateBox17" runat="server" CssClass="datebox" Height="20px"
                                    Width="90px" DataMember="BnbApplication.HomeReturnDate" DateCulture="en-GB" DateFormat="dd/MMM/yyyy">
                                </bnbdatacontrol:BnbDateBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%">
                                End of Service</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbDateBox ID="BnbDateBox15" runat="server" CssClass="datebox" Height="20px"
                                    Width="90px" DataMember="BnbApplication.EOSDate" DateCulture="en-GB" DateFormat="dd/MMM/yyyy">
                                </bnbdatacontrol:BnbDateBox>
                            </td>
                            <td class="label" width="25%">
                                End of Service Confimed</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox6" runat="server" CssClass="checkbox"
                                    DataMember="BnbApplication.EOSConfirmed"></bnbdatacontrol:BnbCheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%">
                                Extension</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl21" runat="server" CssClass="lookupcontrol"
                                    Height="22px" DataMember="BnbApplication.ExtensionStatusID" LookupTableName="lkuExtensionStatus"
                                    DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                            </td>
                            <td class="label" width="25%">
                                Planned/Actual Duration</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox20" runat="server" CssClass="textbox" Height="21"
                                    Width="29" DataMember="BnbApplication.PlacedService.Position.Duration" StringLength="0"
                                    Rows="1" MultiLine="false" ControlType="Text"></bnbdatacontrol:BnbTextBox>
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox21" runat="server" CssClass="textbox" Height="21"
                                    Width="29" DataMember="BnbApplication.MonthsCompleted" StringLength="0" Rows="1"
                                    MultiLine="false" ControlType="Text"></bnbdatacontrol:BnbTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%">
                                Return Code 1</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl16" runat="server" CssClass="lookupcontrol"
                                    Height="22px" DataMember="BnbApplication.ReturnCodeOneID" LookupTableName="lkuReturnCodeOne"
                                    DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" AutoPostBack="True">
                                </bnbdatacontrol:BnbLookupControl>
                            </td>
                            <td class="label" width="25%">
                                Return Code 2</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl17" runat="server" CssClass="lookupcontrol"
                                    Height="22px" DataMember="BnbApplication.ReturnCodeTwoID" LookupTableName="lkuReturnCodeTwo"
                                    DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" AutoPostBack="False"
                                    ParentControlID="BnbLookupControl16" ColumnRelatedToParent="ReturnCodeOneID"></bnbdatacontrol:BnbLookupControl>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%">
                                Suitable for Briefing?</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox5" runat="server" CssClass="checkbox"
                                    DataMember="BnbApplication.SuitableForBriefing"></bnbdatacontrol:BnbCheckBox>
                            </td>
                            <td class="label" width="25%">
                                Reference Required?</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox4" runat="server" CssClass="checkbox"
                                    DataMember="BnbApplication.ReferenceRequired"></bnbdatacontrol:BnbCheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%">
                                Reference Recieved</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbDateBox ID="BnbDateBox16" runat="server" CssClass="datebox" Height="20px"
                                    Width="90px" DataMember="BnbApplication.ReferenceReceivedDate" DateCulture="en-GB"
                                    DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbDateBox>
                            </td>
                            <td class="label" width="25%">
                                More / Less</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl20" runat="server" CssClass="lookupcontrol"
                                    Height="22px" DataMember="BnbApplication.ReturnDetailRankingID" LookupTableName="lkuReturnDetailRanking"
                                    DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" SortByDisplayOrder="True">
                                </bnbdatacontrol:BnbLookupControl>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%" colspan="2">
                                Significant Risk at Placement Assessment
                            <td style="height: 23px" colspan="3">
                                <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox10" runat="server" CssClass="checkbox"
                                    DataMember="BnbApplication.PlacementRisk"></bnbdatacontrol:BnbCheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="height: 30px" width="25%" colspan="2">
                                Vol Suitability Risks Raised by PO with Skill Team</td>
                            <td style="height: 30px" width="25%" colspan="2">
                                <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox8" runat="server" CssClass="checkbox"
                                    DataMember="BnbApplication.VolunteerRisk"></bnbdatacontrol:BnbCheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="height: 30px" width="25%" colspan="2">
                                Discussed with Volunteer?</td>
                            <td style="height: 30px" width="25%" colspan="2">
                                <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox9" runat="server" CssClass="checkbox"
                                    DataMember="BnbApplication.DiscussedWithVol"></bnbdatacontrol:BnbCheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%" colspan="1" rowspan="4">
                                <strong>Return Details</strong></td>
                            <td width="25%" colspan="4">
                                <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList7"
                                    runat="server" CssClass="datagrid" DataMember="BnbApplication.ApplicationReturnDetails"
                                    DataGridType="Editable" SortBy="ID" VisibleOnNewParentRecord="false" AddButtonVisible="true"
                                    EditButtonText="Edit" AddButtonText="Add" EditButtonVisible="true" DeleteButtonVisible="false"
                                    DataGridForDomainObjectListType="Editable" DataProperties="ReturnDetailSubID [BnbLookupControl:vlkuBonoboReturnDetailMainAndSub], ReturnDetailRatingID [BnbLookupControl:lkuReturnDetailRating]">
                                </bnbdatagrid:BnbDataGridForDomainObjectList>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>

