using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for WizardNewEmployer.
    /// </summary>
    public partial class WizardNewEmployer : System.Web.UI.Page
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            BnbEngine.SessionManager.ClearObjectCache();
            BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
            // Put user code to initialize the page here
            if (!this.IsPostBack)
                this.InitWizard();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        private void InitWizard()
        {
            panelTwo.Visible = false;
            buttonPrev.Visible = false;
            buttonFinish.Visible = false;

            BnbLookupDataTable countryLookup = BnbEngine.LookupManager.GetLookup("lkuCountry");
            DataView countryView = new DataView(countryLookup);
            countryView.RowFilter = "Exclude = 0 and ProgrammeOfficeID is not null";
            countryView.Sort = "Description";
            dropDownCountry.DataSource = countryView;
            dropDownCountry.DataValueField = "GuidID";
            dropDownCountry.DataTextField = "Description";
            dropDownCountry.DataBind();

            Guid defaultCountryID = BnbEngine.SessionManager.GetSessionInfo().DefaultCountryID;
            //Code Integrated for displaying defaultcountry only to starfish users
            if ((BnbEngine.SessionManager.GetSessionInfo().WebStyleID) == 11)
            foreach (ListItem loopItem in dropDownCountry.Items)
                if (loopItem.Value == defaultCountryID.ToString())
                    loopItem.Selected = true;
        }

        protected void buttonNext_Click(object sender, System.EventArgs e)
        {
            if (panelOne.Visible)
            {
                if (dropDownCountry.SelectedItem != null && textBoxEmployerName.Text != "")
                
                {
                    panelOne.Visible = false;
                    panelTwo.Visible = true;
                    buttonPrev.Visible = true;
                    buttonNext.Visible = false;
                    buttonFinish.Visible = true;

                    Guid countryID = new Guid(dropDownCountry.SelectedValue);
                    string employerNameBit = textBoxEmployerName.Text;
                    if (employerNameBit.Length > 8)
                        employerNameBit = employerNameBit.Substring(0, 8);

                    BnbListCondition countryMatch = new BnbListCondition("tblEmployer_CountryID", countryID);
                    BnbTextCondition nameMatch = new BnbTextCondition("tblEmployer_EmployerName", employerNameBit, BnbTextCompareOptions.StartOfField);
                    BnbCriteria dupCriteria = new BnbCriteria(countryMatch);
                    dupCriteria.QueryElements.Add(nameMatch);
                    BnbQuery dupQuery = new BnbQuery("vwBonoboEmployerDuplicateCheck", dupCriteria);
                    BnbQueryDataSet dupDataSet = dupQuery.Execute();
                    DataTable dupResults = BonoboWebControls.BnbWebControlServices.GetHtmlEncodedDataTable(dupDataSet.Tables["Results"]);

                    if (dupResults.Rows.Count > 0)
                    {
                        panelEmployerChoose.Visible = true;
                        labelNoDuplicates.Visible = false;
                        dataGridEmployers.DataSource = dupResults;
                        dataGridEmployers.DataBind();
                    }
                    else
                    {
                        panelEmployerChoose.Visible = false;
                        labelNoDuplicates.Visible = true;
                    }

                }
                else
                {
                    labelPanelOneFeedback.Text = "Please enter a Country and Employer Name";
                }





            }
        }

        protected void buttonCancel_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("../Menu.aspx");
        }

        protected void buttonPrev_Click(object sender, System.EventArgs e)
        {
            panelOne.Visible = true;
            panelTwo.Visible = false;
            buttonFinish.Visible = false;
            buttonNext.Visible = true;
            buttonPrev.Visible = false;
        }

        protected void buttonFinish_Click(object sender, System.EventArgs e)
        {
            Guid countryID = new Guid(dropDownCountry.SelectedValue);
            string employerName = textBoxEmployerName.Text;
            BnbEditManager em = new BnbEditManager();
            BnbEmployer newEmp = new BnbEmployer(true);
            newEmp.RegisterForEdit(em);
            newEmp.EmployerName = employerName;
            newEmp.CountryID = countryID;
            if (Request.QueryString["BnbOrganisation"] != null && Request.QueryString["BnbOrganisation"] != "")
            {
                Guid organisationID = new Guid(Request.QueryString["BnbOrganisation"]);
                BnbOrganisation newOrganisation = BnbOrganisation.Retrieve(organisationID);
                newEmp.Organisation = newOrganisation;
            }
            if (employerName.Length > 12)
                newEmp.EmployerShortName = employerName.Substring(0, 12);
            else
                newEmp.EmployerShortName = employerName;

            try
            {
                em.SaveChanges();
                if (Request.QueryString["BnbOrganisation"] != null && Request.QueryString["BnbOrganisation"] != "")
                    Response.Redirect(String.Format("OvsOrganisationPage.aspx?BnbOrganisation={0}",
                        newEmp.Organisation.ID.ToString()));
                else
                    Response.Redirect(String.Format("EmployerPage.aspx?BnbEmployer={0}",
                        newEmp.ID.ToString()));

            }
            catch (BnbProblemException pe)
            {
                labelPanelTwoFeedback.Text = "Unfortunately, the Employer could not be created due to the following problems: <br/>";
                foreach (BnbProblem p in pe.Problems)
                    labelPanelTwoFeedback.Text += p.Message + "<br/>";

            }

        }
    }
}
