using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using BonoboWebControls;

namespace Frond.EditPages
{
    public partial class WizardNewProposal : System.Web.UI.Page
    {
        #region " Declarations "

        protected UserControls.WizardButtons wizardButtons;
        private BnbProject project = null;        

        #endregion

        #region " Page_Load "

        protected void Page_Load(object sender, EventArgs e)
        {
            
            BnbEngine.SessionManager.ClearObjectCache();
            BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
            wizardButtons.AddPanel(panelZero);
            wizardButtons.AddPanel(panelOne);
            wizardButtons.AddPanel(panelTwo);
            wizardButtons.AddPanel(panelThree);

            Guid projID = new Guid(Request.QueryString["BnbProject"].ToString());
            project = BnbProject.Retrieve(projID);

            if (!this.IsPostBack)
            {
                BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
                if (this.Request.UrlReferrer != null)
                    txtHiddenReferrer.Text = this.Request.UrlReferrer.ToString();
                else
                    txtHiddenReferrer.Text = "~/Menu.aspx";

                fillLikelihoodCombo();
                fillGrantOfficersAndDepartmentCombo();
                fillProposalProgressCombo();
                fillCurrencyCombo();
                fillVSOFinancialYearCombo();
                Session.Add("PanelFirstLoaded", true);
            }            
            lblSubmissionErr.Text = "";
            lblOrgErr.Text = "";
        }

        #endregion

        #region  Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
            wizardButtons.ValidatePanel += new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardbuttons_ValidatePanel);
            wizardButtons.ShowPanel += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardbuttons_ShowPanel);
            wizardButtons.CancelWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardbuttons_CancelWizard);
            wizardButtons.FinishWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);            
        }

        private void InitializeComponent()
        {
        }

        #endregion

        #region " wizardbuttons_ValidatePanel "

        private void wizardbuttons_ValidatePanel(object sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
        {
            if (e.CurrentPanel == 1)
            {
                if (!(txtSubmissionDate.HasValidValue))
                {
                    lblSubmissionErr.Text = "Required field. Please specify a valid date";
                    e.Proceed = false;
                }                
                if (lblOrg.Text.ToLower() == "none")
                {
                    lblOrgErr.Text = "Required field cannot be left blank";
                    e.Proceed = false;
                }
                Page.Validate();
                if (!Page.IsValid)
                    e.Proceed = false;
            }
        }

        #endregion

        #region " wizardbuttons_ShowPanel "

        private void wizardbuttons_ShowPanel(object sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            if (e.CurrentPanel == 0)
            {
                this.SetupPanelZero();
            }
            else if (e.CurrentPanel == 1)
            {
                this.SetupPanelOne();
            }
            else if (e.CurrentPanel == 2)
            {
                this.SetupPanelTwo();
            }
            else if (e.CurrentPanel == 3)
            {
                this.SetupPanelThree();
            }
        }

        #endregion

        #region " wizardbuttons_CancelWizard "

        private void wizardbuttons_CancelWizard(object sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            Response.Redirect(txtHiddenReferrer.Text);
        }
        #endregion

        #region " wizardButtons_FinishWizard "

        private void wizardButtons_FinishWizard(object sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
                this.CreateNewProposal();
        }

        #endregion

        #region " SetupPanelZero "

        public void SetupPanelZero()
        {
        }

        #endregion

        #region " SetupPanelOne "

        public void SetupPanelOne()
        {
            bool panelFirstLoaded = Convert.ToBoolean(Session["PanelFirstLoaded"]);
            if (panelFirstLoaded)
            {
                lblProjectTitle.Text = project.ProjectTitle;
                ArrayList arr = new ArrayList();
                Session.Add("PanelFirstLoaded", false);
                for (int i = 0; i < arr.Count; i++)
                {
                    lblErrMessage.Visible = true;
                    lblErrMessage.Text += "The current " + arr[i] + " no longer exists in the system." + "<br/>";
                }
                lblErrMessage.Text += "Therefore please update by selecting the appropriate new staff member/Department name from the list.";
            }

        }

        #endregion

        #region " SetupPanelTwo "

        public void SetupPanelTwo()
        {
            txtProjectDonor.Text = project.ProjectTitle + "/" + lblOrg.Text;
            txtSubmissionDate.Text = String.Format(txtSubmissionDate.Date.ToString("dd/MMM/yyyy"));
            
        }

        #endregion

        #region " SetupPanelThree "

        public void SetupPanelThree()
        {
        }
        #endregion

        #region " saveProposalMainDetails "

        private void saveProposalMainDetails(BnbGrant objGrant, BnbEditManager em)
        {
            if (drpLikelihood.SelectedValue != "")
                objGrant.LikeliHoodID = Convert.ToInt32(drpLikelihood.SelectedValue);

            if (drpProposalLead.SelectedValue != "")
            {
                objGrant.ProposalAccountableOfficerID = new Guid(drpProposalLead.SelectedValue);
                objGrant.ProposalAccountableDepartmentID = project.GetOfficerDepartment(objGrant.ProposalAccountableOfficerID.ToString());
            }          
            if (txtSubmissionDate.Date != null)
                objGrant.SubmissionDate = txtSubmissionDate.Date;
            
            BnbGrantProgress grantProgress = new BnbGrantProgress(true);
            grantProgress.RegisterForEdit(em);
            if (drpProposalProgress.SelectedValue != "")
                grantProgress.GrantStatusID = Convert.ToInt32(drpProposalProgress.SelectedValue);
            objGrant.GrantProgresses.Add(grantProgress);

            if (drpOrganisation.SelectedValue != "")
            {
                Guid companyid = new Guid(drpOrganisation.SelectedValue);
                BnbCompany org = BnbCompany.Retrieve(companyid);
                org.Grants.Add(objGrant);
                if (org.DonorManagerID != BnbDomainObject.NullGuid)
                {
                    objGrant.GrantResponsibleOfficerID = org.DonorManagerID;
                    objGrant.GrantResponsibleDepartmentID = org.DonorManagerDepartmentID;
                }
            }
        }

        #endregion

        #region " saveProposalFinanceRiskDetails "

        private void saveProposalFinanceRiskDetails(BnbGrant objGrant, BnbEditManager em)
        {
            if (drpCurrency.SelectedValue != "")
                objGrant.CurrencyID = Convert.ToInt32(drpCurrency.SelectedValue);

            if (txtDonorRequest.Text != "")
                objGrant.AmountRequestedCurrency = Convert.ToDecimal(txtDonorRequest.Text);

            if (txtDonorBudget.Text != "")
                objGrant.DonorBudgetAmountCurrency = Convert.ToDecimal(txtDonorBudget.Text);

            if (txtProjectTotalCost.Text != "")
                objGrant.ProjectTotalCost = Convert.ToDecimal(txtProjectTotalCost.Text);

            if (txtExchangeRate.Text != "")
                objGrant.ExchangeRate = Convert.ToDecimal(txtExchangeRate.Text);

            objGrant.IncomeTypeID = BnbConst.Default_IncomeType;

            BnbProject objproject = BnbDomainObject.GeneralRetrieve(project.ID, typeof(BnbProject)) as BnbProject;
            objproject.RegisterForEdit(em);
            
        }

        #endregion

        #region " Proposal Income PipeLine "

        private void saveProposalIncomePipeline(BnbGrant objGrant, BnbEditManager em)
        {
            BnbGrantIncomePipeline grantIncomePipeline = new BnbGrantIncomePipeline(true);
            grantIncomePipeline.RegisterForEdit(em);

            if (ddlFinancialYear.SelectedValue != string.Empty)
                grantIncomePipeline.FinancialYearID = Convert.ToInt32(ddlFinancialYear.SelectedValue);

            if (txtAnnualIncomeCurrency.Text != string.Empty)
                grantIncomePipeline.ExpectedAnnualIncomeCurrency = Convert.ToDecimal(txtAnnualIncomeCurrency.Text);

            objGrant.GrantIncomePipelineList.Add(grantIncomePipeline);
        }

        #endregion

        #region " findButton_Click "
        // finds the organisation name based on the search text 

        protected void findButton_Click(object sender, EventArgs e)
        {
            BnbCriteria crit = new BnbCriteria(new BnbTextCondition("tblCompany_CompanyName", txtDonorOrg.Text, BnbTextCompareOptions.AnyPartOfField));
            BnbQuery query = new BnbQuery("vwBonoboInstitutionalDonorNames", crit);
            BnbQueryDataSet ds = query.Execute();
            // TPT amended : PGMSP-15
            DataTable orgTable = ds.Tables["Results"];            
            drpOrganisation.DataSource = orgTable;
            drpOrganisation.DataTextField = "tblCompany_CompanyName";
            drpOrganisation.DataValueField = "tblCompany_CompanyID";
            drpOrganisation.DataBind();
            drpOrganisation.Items.Insert(0, "");
            lbldrpOrg.Visible = true;
            drpOrganisation.Visible = true;
        }

        #endregion

        #region " Organisation_SelectedIndexChanged "

        protected void Organisation_SelectedIndexChanged(object sender, EventArgs e)
        {

            lblOrg.Text = drpOrganisation.SelectedItem.Text;
            drpOrganisation.Visible = false;
            txtDonorOrg.Visible = false;
            btnFind.Visible = false;
            lblFind.Visible = false;
            lbldrpOrg.Visible = false;
            addOrg.Visible = true;
            addOrg.Text = "Change";
            btnCancel.Visible = false;
        }

        #endregion

        #region " Add and Cancel button Events of organisation "

        protected void AddOrg_Click(object sender, EventArgs e)
        {
            txtDonorOrg.Visible = true;
            btnCancel.Visible = true;
            btnFind.Visible = true;
            lblFind.Visible = true;
            addOrg.Visible = false;
        }

        protected void btnCancel_Onclick(object sender, EventArgs e)
        {
            txtDonorOrg.Visible = false;
            btnCancel.Visible = false;
            btnFind.Visible = false;
            lblFind.Visible = false;
            lbldrpOrg.Visible = false;
            drpOrganisation.Visible = false;
            addOrg.Visible = true;
        }

        #endregion

         
        #region " CreateNewProposal "

        public void CreateNewProposal()
        {
            BnbEditManager em = new BnbEditManager();
            BnbGrant grant = new BnbGrant(true);
            grant.RegisterForEdit(em);
            saveProposalMainDetails(grant, em);
            saveProposalFinanceRiskDetails(grant, em);
            saveProposalIncomePipeline(grant, em);
            project.Grants.Add(grant);
            try
            {
                em.SaveChanges();
                BnbEngine.SessionManager.ClearObjectCache();
                Response.Redirect(String.Format("ProposalPage.aspx?BnbProject={0}&BnbGrant={1}", project.ID.ToString(), grant.ID.ToString()));
            }
            catch (BnbProblemException e)
            {
                lblPanelOneFeedback.Text = "Unfortunately, the Project could not be created due to the following problems: <br/>";
                foreach (BnbProblem p in e.Problems)
                    lblPanelOneFeedback.Text += p.Message + "<br/>";
            }
        }

        #endregion

        #region " Lookup's for new proposal "

        private void fillLikelihoodCombo()
        {
            DataTable likelihoodTable = BnbEngine.LookupManager.GetLookup("lkuLikeliHood");
            likelihoodTable.DefaultView.RowFilter = "Exclude = 0";
            likelihoodTable.DefaultView.Sort = "Description";
            drpLikelihood.DataSource = likelihoodTable;
            drpLikelihood.DataTextField = "Description";
            drpLikelihood.DataValueField = "IntID";
            drpLikelihood.DataBind();
            drpLikelihood.Items.Insert(0, "");
        }

        private void fillGrantOfficersAndDepartmentCombo()
        {
            DataTable grantOfficer = BnbEngine.LookupManager.GetLookup("vlkuBonoboUser");
            grantOfficer.DefaultView.RowFilter = "Exclude = 0";
            grantOfficer.DefaultView.Sort = "Description";         
            
            drpProposalLead.DataSource = grantOfficer;
            drpProposalLead.DataTextField = "Description";
            drpProposalLead.DataValueField = "GuidID";
            drpProposalLead.DataBind();
            drpProposalLead.Items.Insert(0, "");            
                        
        }

        private void fillProposalProgressCombo()
        {
            DataTable proposalProgressTable = BnbEngine.LookupManager.GetLookup("vlkuBonoboProposalStatus");
            proposalProgressTable.DefaultView.RowFilter = "Exclude=0";
            proposalProgressTable.DefaultView.Sort = "DisplayOrder";
            drpProposalProgress.DataSource = proposalProgressTable;
            drpProposalProgress.DataTextField = "Description";
            drpProposalProgress.DataValueField = "IntID";
            drpProposalProgress.DataBind();
            drpProposalProgress.Items.Insert(0, "");
        }

        private void fillCurrencyCombo()
        {
            DataTable currencyTable = BnbEngine.LookupManager.GetLookup("lkuCurrency");
            currencyTable.DefaultView.RowFilter = "Exclude=0";
            currencyTable.DefaultView.Sort = "Description";
            drpCurrency.DataSource = currencyTable;
            drpCurrency.DataTextField = "Description";
            drpCurrency.DataValueField = "IntID";
            drpCurrency.DataBind();
            drpCurrency.Items.Insert(0, "");
        }

        private void fillVSOFinancialYearCombo()
        {
            DataTable dtVSOFinancialyear = BnbEngine.LookupManager.GetLookup("lkuVSOFinancialYear");
            dtVSOFinancialyear.DefaultView.RowFilter = "Exclude=0";
            dtVSOFinancialyear.DefaultView.Sort = "DisplayOrder";
            ddlFinancialYear.DataSource = dtVSOFinancialyear;
            ddlFinancialYear.DataTextField = "Description";
            ddlFinancialYear.DataValueField = "IntID";
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, "");
        }

        #endregion



    }
}
