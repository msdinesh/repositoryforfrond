using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for PrePostCheckPage.
    /// </summary>
    public partial class PrePostCheckPage : System.Web.UI.Page
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
#if DEBUG
            BnbWebFormManager bnb = null;
            if (Request.QueryString.ToString() == "")
            {
                BnbWebFormManager.ReloadPage("BnbPosition=8874B549-26BE-11D6-9C2C-000102A32E88&BnbPrePost=new&PageState=new");
            }
            else
            {
                bnb = new BnbWebFormManager(this, "BnbPrePost");
            }
#else
			BnbWebFormManager bnb = new BnbWebFormManager(this,"BnbApplication");
#endif
            // turn on logging
            bnb.LogPageHistory = true;
            //<Integartion>
            bnb.PageNameOverride = "Pre/Post Check";
            BnbWorkareaManager.SetPageStyleOfUser(this);

            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null)
                    titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
            }
            //</Integartion>
            
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
