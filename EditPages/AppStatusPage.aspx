<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Page language="c#" Codebehind="AppStatusPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.AppStatusPage" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Benefit Page</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal><LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet"><asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent"><uc1:title id="titleBar" runat="server" imagesource="../Images/bnbapplication24silver.gif"></uc1:title>
				<uc1:NavigationBar id="NavigationBar1" runat="server"></uc1:NavigationBar>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" NewButtonEnabled="False" NewWidth="55px" EditAllWidth="55px"
					SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px" NewText="New" EditAllText="Edit All" SaveText="Save" UndoText="Undo" CssClass="databuttons"></bnbpagecontrol:bnbdatabuttons><bnbpagecontrol:bnbmessagebox id="BnbMessageBox2" runat="server" CssClass="messagebox" PopupMessage="false" SuppressRecordSavedMessage="false"></bnbpagecontrol:bnbmessagebox>
				<TABLE class="fieldtable" id="Table1" width="100%" border="0">
					<TR>
						<TD class="label" style="HEIGHT: 33px" width="25%">Volunteer</TD>
						<TD style="HEIGHT: 33px" width="25%">
							<bnbgenericcontrols:BnbHyperlink id="BnbLnkIndividual" runat="server"></bnbgenericcontrols:BnbHyperlink></TD>
					<TR>
						<TD class="label" style="HEIGHT: 33px" width="25%">Status Group</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" AutoPostBack="True"
								DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuStatusGroup" DataMember="BnbApplicationStatus.StatusGroupID"
								Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
					<TR>
					<TR>
						<TD class="label" style="HEIGHT: 33px" width="25%">Status Type</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl2" runat="server" CssClass="lookupcontrol" DataTextField="Description"
								ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuStatus" DataMember="BnbApplicationStatus.StatusID" Height="22px" ColumnRelatedToParent="StatusGroupID"
								ParentControlID="BnbLookupControl1" AutoPostBack="True"></bnbdatacontrol:bnblookupcontrol></TD>
					<TR>
					<TR>
						<TD class="label" style="HEIGHT: 33px" width="25%">Date</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnbdatebox id="BnbDateBox1" runat="server" CssClass="datebox" DataMember="BnbApplicationStatus.StatusDate"
								Height="20px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB" Width="90px"></bnbdatacontrol:bnbdatebox></TD>
					<TR>
					<TR>
						<TD class="label" style="HEIGHT: 33px" width="25%">Reason</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl3" runat="server" CssClass="lookupcontrol" DataTextField="Description"
								ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuStatusReason" DataMember="BnbApplicationStatus.StatusReasonID" Height="22px"
								ParentControlID="BnbLookupControl2" ColumnRelatedToParent="StatusID"></bnbdatacontrol:bnblookupcontrol></TD>
					<TR>
						<TD class="label" style="HEIGHT: 33px" width="25%">Comments</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnbtextbox id="BnbTextBox1" runat="server" CssClass="textbox" DataMember="BnbApplicationStatus.Comments"
								Height="20px" Width="250px" ControlType="Text" StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:bnbtextbox></TD>
					</TR>
					<TR>
					</TR>
				</TABLE>
				<P><bnbpagecontrol:bnbdatabuttons id="BnbDataButtons2" runat="server" CssClass="databuttons" UndoText="Undo" SaveText="Save"
						EditAllText="Edit All" NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px"
						NewWidth="55px" NewButtonEnabled="False"></bnbpagecontrol:bnbdatabuttons></P>
			</div>
		</form>
	</body>
</HTML>
