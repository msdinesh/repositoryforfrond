<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>

<%@ Page Language="c#" Codebehind="OvsOrganisationAddressPage.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.EditPages.OvsOrganisationAddressPage" %>

<%@ Register Src="../UserControls/GShowLocation.ascx" TagName="GShowLocation" TagPrefix="uc2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>OvsOrganisationAddresPage</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet" />
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
    <script language="javascript" src="../Javascripts/GoogleMap.js"></script>
</head>
<body onload="initialize()" onunload="GUnload()">
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/bnbaddress24silver.gif">
            </uc1:Title>
            <table style="margin: 0px" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="left" width="50%">
                        <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons1" runat="server" NewButtonEnabled="True"
                            CssClass="databuttons" NewWidth="55px" EditAllWidth="55px" SaveWidth="55px" UndoWidth="55px"
                            HeightAllButtons="25px" NewText="New" EditAllText="Edit All" SaveText="Save"
                            UndoText="Undo"></bnbpagecontrol:BnbDataButtons>
                    </td>
                </tr>
            </table>
            <bnbpagecontrol:BnbMessageBox ID="BnbMessageBox1" runat="server" CssClass="messagebox">
            </bnbpagecontrol:BnbMessageBox>
           
                <table class="fieldtable" id="Table1" width="100%" border="0">
                    <tr>
                        <td class="label" style="height: 24px" width="50%">
                            Organisation:</td>
                        <td style="height: 24px">
                            <bnbdatacontrol:BnbDomainObjectHyperlink ID="BnbDomainObjectHyperlink1" runat="server"
                                DomainObjectGuid="BnbEmployer.ID" DataMember="BnbOrganisation.OrganisationName"
                                RedirectPage="OvsOrganisationPage.aspx" HyperlinkText="[BnbDomainObjectHyperlink]"
                                DataProperty="EmployerName" DomainObject="BnbEmployer" GuidProperty="BnbOrganisation.ID"
                                Target="_top"></bnbdatacontrol:BnbDomainObjectHyperlink>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="50%">
                            Line 1:</td>
                        <td>
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox1" runat="server" CssClass="textbox" Width="250px"
                                Height="20px" DataMember="BnbOrganisationAddress.Address.AddressLine1" DataProperty="Address.AddressLine1"
                                DomainObject="BnbEmployerAddress" MultiLine="false" Rows="1"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 25px" width="50%">
                            Line 2:</td>
                        <td style="height: 25px">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox2" runat="server" CssClass="textbox" Width="250px"
                                Height="20px" DataMember="BnbOrganisationAddress.Address.AddressLine2" DataProperty="Address.AddressLine2"
                                DomainObject="BnbEmployerAddress" MultiLine="false" Rows="1"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 24px" width="50%">
                            Line 3:</td>
                        <td style="height: 24px">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox3" runat="server" CssClass="textbox" Width="250px"
                                Height="20px" DataMember="BnbOrganisationAddress.Address.AddressLine3" DataProperty="Address.AddressLine3"
                                DomainObject="BnbEmployerAddress" MultiLine="false" Rows="1"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="50%">
                            Postal Town / Line 4:</td>
                        <td>
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox4" runat="server" CssClass="textbox" Width="250px"
                                Height="20px" DataMember="BnbOrganisationAddress.Address.AddressLine4" DataProperty="Address.AddressLine4"
                                DomainObject="BnbEmployerAddress" MultiLine="false" Rows="1"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="50%">
                            County / State:</td>
                        <td>
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox5" runat="server" CssClass="textbox" Width="250px"
                                Height="20px" DataMember="BnbOrganisationAddress.Address.County" DataProperty="Address.County"
                                DomainObject="BnbEmployerAddress" MultiLine="false" Rows="1"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 25px" width="50%">
                            Postcode / ZIP:</td>
                        <td style="height: 25px">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox6" runat="server" CssClass="textbox" Width="250px"
                                Height="20px" DataMember="BnbOrganisationAddress.Address.PostCode" DataProperty="Address.PostCode"
                                DomainObject="BnbEmployerAddress" MultiLine="false" Rows="1"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 13px" width="50%">
                            Country:</td>
                        <td style="height: 13px">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl1" runat="server" CssClass="lookupcontrol"
                                Width="250px" Height="22px" DataMember="BnbOrganisationAddress.Address.CountryID"
                                DataProperty="Address.CountryID" DomainObject="BnbEmployerAddress" LookupControlType="ComboBox"
                                ControlType="ComboBox" ListBoxRows="4" DataTextField="Description" LookupTableName="lkuCountry"
                                AutoPostBack="False"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 13px" width="50%">
                        </td>
                        <td style="height: 13px">
                            <input id="btnShowLocation" runat="Server" onclick="showLocation()" type="button"
                                value="Show on map" visible="false" /></td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 24px" width="50%">
                            Contact Numbers</td>
                        <td style="height: 24px">
                            <p>
                                <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList1"
                                    runat="server" CssClass="datagrid" Width="200px" DataMember="BnbOrganisationAddress.Address.ContactNumbers"
                                    DataProperties="ContactNumberTypeID [BnbLookupControl:vlkuBonoboContactNumberTypeAddress], ContactNumber[BnbTextBox]"
                                    DataGridType="Editable" DeleteButtonVisible="false" EditButtonVisible="true"
                                    AddButtonText="Add" EditButtonText="Edit" AddButtonVisible="true"></bnbdatagrid:BnbDataGridForDomainObjectList>
                            </p>
                        </td>
                    </tr>
                </table>
           
            <p>
                <bnbpagecontrol:BnbSectionHeading ID="headingGeoDetails" runat="server" Collapsed="false"
                    CssClass="sectionheading" Height="22px" SectionHeading="Geographical Details"
                    Width="100%" />
                <uc2:GShowLocation ID="showLocation" runat="server" HeadingVisibile="false" />
                <br />
                <br />
                <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons2" runat="server" NewButtonEnabled="True"
                    CssClass="databuttons" NewWidth="55px" EditAllWidth="55px" SaveWidth="55px" UndoWidth="55px"
                    HeightAllButtons="25px" NewText="New" EditAllText="Edit All" SaveText="Save"
                    UndoText="Undo"></bnbpagecontrol:BnbDataButtons>
            </p>
        </div>
    </form>


</body>
</html>
