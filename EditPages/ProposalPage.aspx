<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProposalPage.aspx.cs" Inherits="Frond.EditPages.ProposalPage" %>
<%@ Register Src="../UserControls/PMFStatusPanel.ascx" TagName="PMFStatusPanel" TagPrefix="uc2" %>
<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.ServicesControls"
    TagPrefix="cc1" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="SFEventPersonnelGrid" Src="../UserControls/SFEventPersonnelGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Proposal Page</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<link href="../Css/calendar.css" type="text/css" rel="stylesheet" />
		<script type="text/javascript" language="javascript">
		function showHidePanel(hyperlinkID,pnlID)
		{
		var panelID= document.getElementById(pnlID);
		var hpText=document.getElementById(hyperlinkID);		
		if(panelID.style.display=='block')
		{
		panelID.style.display='none';
		hpText.innerHTML="+ Show Details";
		}
		else
		{
		panelID.style.display='block';
		hpText.innerHTML="- Hide Details";
		}
		}
		function loadEvent()
		{		
        var editClicked=document.getElementById('BnbDataButtons1_Edit');
        if(editClicked.disabled)
        {
        var panelID1= document.getElementById('ProposalID');
        panelID1.style.display='block';
        var panelID2= document.getElementById('FinanceID');
        panelID2.style.display='block';
        }        
		}
		function emailPageLink()
        {
        var str=encodeURIComponent(window.location);
        var replaceTabName = document.getElementById('BnbTabControl1_SelectedTabID').value;
        var url = decodeURIComponent(str); 
        if(url.split("&ControlID=").length== 2)
        {
        var tabName = url.split("&ControlID=")[1];
        url = url.replace(tabName,replaceTabName);
        window.open('mailto:?subject=PGMS-Page Link&body=%0d%0a%0d%0a%0d%0a%0d%0a'+encodeURIComponent(url)+''); window.focus(); return false;

        }
        else
        {
        url = url + '&ControlID=' + replaceTabName;
        window.open('mailto:?subject=PGMS-Page Link&body=%0d%0a%0d%0a%0d%0a%0d%0a'+encodeURIComponent(url)+''); window.focus(); return false;
        }    }	
    </script>
	</HEAD>
	<body onload="loadEvent()" onclick="hideCalendar(event);">
		
			<form id="Form1" method="post" runat="server" onprerender="ProposalPage_PreRender" >
				<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
				<div id="baseContent">
					<uc1:title id="titleBar" runat="server" imagesource="../images/bnbgrantproposal24silver.gif"></uc1:title>
					<uc2:PMFStatusPanel ID="PMFStatusPanel2" runat="server" />
					<table width="100%">
					<tr>
						<td><bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" NewButtonVisible="True" UndoText="Undo" SaveText="Save"
										EditAllText="Edit All" NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" CssClass="databuttons"></bnbpagecontrol:bnbdatabuttons>
						</td>
						<td align="right" style="width: 490px; padding: 5px;">
                            <a href="#" onclick="emailPageLink();return false;"><b>Email Page Link</b></a>
                        </td>
                        <td align="right"><bnbgenericcontrols:bnbhyperlink id="hlkContract" runat="server">View Contract</bnbgenericcontrols:bnbhyperlink></td>
					</tr>
				</table>
				<span id="spnViewWarning" runat="server" visible="false" class="viewwarning"></span>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox" PopupMessage="False"></bnbpagecontrol:bnbmessagebox>
                    <bnbpagecontrol:BnbTabControl ID="BnbTabControl1" runat="server" />
                
				<asp:panel id="Main" runat="server" CssClass="tcTab" BnbTabName="Main">
				<table cellspacing="0px">
                        <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlGeneral','mainID'); return false;">
                            <td style="width: 500px; padding: 5px;">
                                 General Information</td>
                            <td align="right" style="width: 450px; padding: 5px;">
                                <a href="#" id="hlGeneral">- Hide Details</a></td>
                        </tr>
                    </table>
                    <div id="mainID" style="display: block;">
                    <br />
					<TABLE id="Table1" border="0" class="fieldtable">
						<TR>
							<TD class="label">Project Title</TD>
							<TD><bnbdatacontrol:bnbdomainobjecthyperlink id="hlProjectTitle" runat="server" Target="_top" DataMember="BnbProject.ProjectTitle"
									HyperlinkText="[BnbDomainObjectHyperlink]" GuidProperty="ProjectID" RedirectPage="ProjectPage.aspx"></bnbdatacontrol:bnbdomainobjecthyperlink></TD>
							<TD class="label">Project Abbreviation</TD>
							<td><bnbdatacontrol:bnbdomainobjecthyperlink id="hlProjectAbbr" runat="server" Target="_top" DataMember="BnbProject.ProjectAbbr"
									HyperlinkText="[BnbDomainObjectHyperlink]" GuidProperty="ProjectID" RedirectPage="ProjectPage.aspx"></bnbdatacontrol:bnbdomainobjecthyperlink></td>
						</TR>
 
						<TR>
							<TD class="label">Project Code</TD>
							<TD><bnbdatacontrol:bnbdomainobjecthyperlink id="hlProjectCode" runat="server" Target="_top" DataMember="BnbProject.ProjectCode"
									HyperlinkText="[BnbDomainObjectHyperlink]" GuidProperty="ProjectID" RedirectPage="ProjectPage.aspx"></bnbdatacontrol:bnbdomainobjecthyperlink></TD>
							<TD class="label">Budget Line</TD>
							<td><bnbdatacontrol:bnbtextbox id="BnbTextBox4" runat="server" CssClass="textbox" Height="20px" Width="250px" DataMember="BnbGrant.BudgetLine"
									ControlType="Text" StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:bnbtextbox></td>
						</TR>
						<TR>
							<TD class="label">Organisation (Donor)</TD>
							<TD><bnbdatacontrol:bnbdomainobjecthyperlink id="bnbhlOrganisationDonor" runat="server" Target="_top" DataMember="BnbGrant.Company.CompanyName"
									HyperlinkText="[BnbDomainObjectHyperlink]" GuidProperty="ID" RedirectPage="OrganisationPage.aspx"></bnbdatacontrol:bnbdomainobjecthyperlink><BR>
								<bnbdatacontrol:bnbsearchdomainobjectcomposite id="bnbsdoOrganisationDonor" runat="server" DataMember="BnbGrant.Company" DomainObjectType="BnbCompany"
									BonoboViewName="vwBonoboInstitutionalDonorNames" BonoboViewFieldToBeBound="tblCompany_CompanyID" BonoboViewFieldToLookUp="tblCompany_CompanyName"></bnbdatacontrol:bnbsearchdomainobjectcomposite></TD>
							<TD class="label">Donor Reference</TD>
							<TD><bnbdatacontrol:bnbtextbox id="Bnbtextbox1" runat="server" CssClass="textbox" Height="20px" Width="250px" DataMember="BnbGrant.DonorReference"
									ControlType="Text" StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label">Proposal Lead <br /><br /> Proposal Lead Department</TD>
							<TD><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl8" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
									DataMember="BnbGrant.ProposalAccountableOfficerID" LookupTableName="vlkuBonoboUser" LookupControlType="ComboBox"
									ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:bnblookupcontrol><BR>
								 <bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol9" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
								    DataMember="BnbGrant.ProposalAccountableDepartmentID" LookupTableName="appDepartment" LookupControlType="ReadOnlyLabel"
								    ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>
						<TR>
							<TD class="label">Grant Accountable</TD>
							<TD><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
									DataMember="BnbGrant.GrantAccountableOfficerID" LookupTableName="vlkuBonoboUser" LookupControlType="ComboBox"
									ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:bnblookupcontrol><BR>
								 <bnbdatacontrol:bnblookupcontrol id="BnbLookupControl4" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
									DataMember="BnbGrant.GrantAccountableDepartmentID" LookupTableName="appDepartment" LookupControlType="ReadOnlyLabel"
									ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label">Donor Manager</TD>
							<TD><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl2" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
									DataMember="BnbGrant.GrantResponsibleOfficerID" LookupTableName="vlkuBonoboUser" LookupControlType="ReadOnlyLabel"
									DataTextField="Description"></bnbdatacontrol:bnblookupcontrol><BR>
								<bnbdatacontrol:bnblookupcontrol id="BnbLookupControl3" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
									DataMember="BnbGrant.GrantResponsibleDepartmentID" LookupTableName="appDepartment" LookupControlType="ReadOnlyLabel"
									DataTextField="Description"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>						
						<TR>
							<TD class="label">Submission Date</TD>
							<TD><bnbdatacontrol:bnbdatebox id="BnbDateBox1"  ShowCalender="true" runat="server" CssClass="datebox" Height="20px" Width="90px" DataMember="BnbGrant.SubmissionDate"
									DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
							<TD class="label">Decision Date</TD>
							<TD><bnbdatacontrol:bnbdatebox id="BnbDateBox2"  ShowCalender="true" runat="server" CssClass="datebox" Height="20px" Width="90px" DataMember="BnbGrant.DecisionDate"
									DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
						</TR>
						<TR>
							<TD class="label">No specific country</TD>
							<TD style="VERTICAL-ALIGN: top">
								<asp:Label id="lblGlobal" runat="server" OnPreRender="lblGlobal_PreRender"></asp:Label></TD>
							<TD class="label">Programme Area</TD>
							<TD>
								<bnbdatacontrol:bnblookupcontrol id="BnbLookupControl10" runat="server" CssClass="lookupcontrol" Width="250px" Height="25px"
									DataMember="BnbProject.ProgrammeAreaID" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
									LookupTableName="vlkuBonoboVSOProgrammeProgrammeOffice"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>
						<TR>
							<TD class="label">Regions</TD>
							<TD style="VERTICAL-ALIGN: top">
								<asp:Label id="lblRegions" runat="server" OnPreRender="Regions_PreRender"></asp:Label></TD>
							<asp:Panel ID="pnlDeleted" runat="server">
								<TD class="label">Deleted</TD>
								<TD>
									<bnbdatacontrol:bnbcheckbox id="BnbCheckBox1" runat="server" CssClass="checkbox" DataMember="BnbGrant.Exclude"></bnbdatacontrol:bnbcheckbox></TD>
							</asp:Panel>
						</TR>
						<TR>
							<TD class="label">Programme Offices</TD>
							<TD style="VERTICAL-ALIGN: top"><bnbdatagrid:bnbdatagridfordomainobjectlist id="bnbdgProgrammeOffices" runat="server" CssClass="datagrid_nolines" Width="200px"
									DataMember="BnbProject.ProgrammeOffices" HideColumnHeaders="True" DataGridForDomainObjectListType="Editable" DataProperties="ProgrammeOfficeID [BnbLookupControl:lkuProgrammeOffice]"
									DeleteButtonVisible="false" EditButtonVisible="False" AddButtonText="Add" EditButtonText="Edit" AddButtonVisible="true" VisibleOnNewParentRecord="false"
									SortBy="ID"></bnbdatagrid:bnbdatagridfordomainobjectlist></TD>
						</TR>
						<TR>
							<TD class="label">Last Updated By</TD>
							<TD colSpan="1"><bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol16" runat="server" CssClass="textbox" Width="250px" Height="22px"
									DataMember="BnbGrant.UpdatedBy" LookupTableName="vlkuBonoboUser" LookupControlType="ReadOnlyLabel" DataTextField="Description"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label">Last Updated On</TD>
							<TD colSpan="2"><bnbdatacontrol:bnbdatebox id="Bnbdatebox3" runat="server" CssClass="datebox" Width="90px" Height="20px" DataMember="BnbGrant.UpdatedOn"
									DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
						</TR>
						<tr>
						    <td class="label" style="width: 140px">Likelihood of success on application</td>
                             <td><bnbdatacontrol:BnbLookupControl ID="BnbLookupControl6" runat="server" CssClass="lookupcontrol"
                                     Width="250px" Height="22px" DataMember="BnbGrant.LikeliHoodID" LookupTableName="lkuLikeliHood"
                                     LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:BnbLookupControl>
                             </td>
                           <td class="label">Donor&nbsp;Contact&nbsp;name</td>
                                <td>
                                    <bnbdatacontrol:BnbTextBox ID="BnbTextBox7" runat="server" CssClass="textbox" Width="250px"
                                        Height="20px" DataMember="BnbGrant.DonorContactName" MultiLine="false" Rows="1"
                                        StringLength="0" ControlType="Text"></bnbdatacontrol:BnbTextBox>
                                </td> 
						</tr>
						<tr>
                                <td style="font-weight: bold; color: Silver;">
                                    Risk Assessment</td>
                        </tr>
						<tr>
							<td class="label">Overall Grant Risk</td>
							<td><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl5" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
									DataMember="BnbGrant.GrantRiskID" LookupTableName="lkuGrantRisk" LookupControlType="ComboBox" ListBoxRows="4"
									DataTextField="Description" SortByDisplayOrder="True"></bnbdatacontrol:bnblookupcontrol></td>
						</tr>
					</TABLE>
					</div> 
					<br />
					<asp:Panel id="Panel1" runat="server">
					 <table cellspacing="0px">
                        <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlProposal','ProposalID'); return false;">
                            <td style="width: 500px; padding: 5px;">
                                 Proposal Progress</td>
                            <td align="right" style="width: 450px; padding: 5px;">
                                <a href="#" id="hlProposal">- Hide Details</a></td>
                        </tr>
                    </table>
                <div id="ProposalID" style="display:block;"><br />
					<bnbgenericcontrols:BnbHyperlink id="hlProposalToContract" runat="server">Proposal-To-Contract</bnbgenericcontrols:BnbHyperlink>&nbsp;
					<asp:Button ID="btnProposalToContract" runat="server" Text="CONVERT PROPOSAL TO CONTRACT" Width="228px" OnClick="btnProposalToContract_Click"/>
					<bnbgenericcontrols:bnbhyperlink id="hplEditContract" runat="server" Target="_top"> Edit Contract</bnbgenericcontrols:bnbhyperlink>
				
				<P><bnbdatagrid:bnbdatagridfordomainobjectlist id="bnbdgProposalHistory" runat="server" CssClass="datagrid" ShowDateCalender="true" DataMember="BnbGrant.GrantProgresses"
						DataProperties="GrantStatusID [BnbLookupControl:vlkuBonoboProposalStatus], ProposalDate [BnbDateBox],  CreatedBy [BnbLookupControl:vlkuBonoboUser], CreatedOn [BnbDateBox]"
						DeleteButtonVisible="false" EditButtonVisible="False" AddButtonText="Add" EditButtonText="Edit" AddButtonVisible="true"
						VisibleOnNewParentRecord="false" SortBy="CreatedOn" DataGridType="Editable" LineBreaksOnColumnNames="True" OnPreRender="bnbdgProposalHistory_PreRender"></bnbdatagrid:bnbdatagridfordomainobjectlist></P>
						</div> 
			</asp:Panel> 
			   <br />	
			   <table cellspacing="0px">
                        <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlFinance','FinanceID'); return false;">
                            <td style="width: 500px; padding: 5px;">Finance</td>
                            <td align="right" style="width: 450px; padding: 5px;">
                                <a href="#" id="hlFinance">- Hide Details</a></td>
                        </tr>
                    </table>                
                <div id="FinanceID" style="display:block;">	<br />
					<TABLE id="Table4" border="0" class="fieldtable">
						<TR>
							<TD class="label">Requested (Donor Currency)</TD>
							<TD><bnbdatacontrol:bnbdecimalbox id="BnbDecimalBox1" runat="server" CssClass="textbox" Height="20px" Width="120px"
									DataMember="BnbGrant.AmountRequestedCurrency" DecimalPlaces="0"></bnbdatacontrol:bnbdecimalbox></TD>
							<TD class="label">Requested (�)</TD>
							<TD><bnbdatacontrol:bnbdecimalbox id="BnbDecimalBox4" runat="server" CssClass="textbox" Height="20px" Width="120px"
									DataMember="BnbGrant.AmountRequestedSterling" DecimalPlaces="0"></bnbdatacontrol:bnbdecimalbox></TD>
						</TR>
						<TR>
							<TD class="label">Donor Budget (Donor Currency)</TD>
							<TD><bnbdatacontrol:bnbdecimalbox id="BnbDecimalBox3" runat="server" CssClass="textbox" Height="20px" Width="120px"
									DataMember="BnbGrant.DonorBudgetAmountCurrency" DecimalPlaces="0"></bnbdatacontrol:bnbdecimalbox></TD>
							<TD class="label">Donor Budget (�)</TD>
							<TD><bnbdatacontrol:bnbdecimalbox id="bnbDecimalBoxDBSterling" runat="server" CssClass="textbox" Height="20px" Width="120px"
									DataMember="BnbGrant.DonorBudgetAmountSterling" DecimalPlaces="0"></bnbdatacontrol:bnbdecimalbox></TD>
						</TR>
						<TR>
							<TD class="label">% of Donor Budget</TD>
							<TD><bnbdatacontrol:bnbpercentbox id="BnbPercentBox1" runat="server" CssClass="textbox" Height="20px" Width="120px"
									DataMember="BnbGrant.DonorBudgetApprovedPercentage" ControlType="Text" StringLength="0" Rows="1" MultiLine="false"
									DecimalPlaces="2" DataType="String"></bnbdatacontrol:bnbpercentbox></TD>
						</TR>
								
						
						<TR>
							<TD class="label">Exchange Rate (<small>e.g. how many dollars to the pound</small>)</TD>
							<TD><bnbdatacontrol:bnbdecimalbox id="BnbDecimalBox6" runat="server" CssClass="textbox" Height="20px" Width="120px"
									DataMember="BnbGrant.ExchangeRate" ControlType="Text" StringLength="0" Rows="1" MultiLine="false" DecimalPlaces="2"
									DataType="String" DefaultValue="1" FormatExpression="#,0.000"></bnbdatacontrol:bnbdecimalbox></TD>
							<TD class="label">Donor Currency</TD>
							<TD><bnbdatacontrol:bnblookupcontrol id="bnblucCurrency" runat="server" CssClass="lookupcontrol" Height="22px" Width="120px"
									DataMember="BnbGrant.CurrencyID" LookupTableName="lkuCurrency" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"
									TextAlign="right"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>
						<TR>
						    <td class="label">Project Total Cost (Donor Currency)</td>
						    <td>
                                <bnbdatacontrol:BnbDecimalBox ID="BnbDecimalBoxPTC" runat="server" CssClass="textbox" Height="20px" Width="120px"
						        DataMember="BnbGrant.ProjectTotalCost" DecimalPlaces="0" />
						    </td>	
						    <td class="label">Project Total Cost (�)</td>
						<td>
						    <bnbdatacontrol:BnbDecimalBox ID="bnbDecimalBoxPTCSterling" runat="server" CssClass="textbox" Height="20px" Width="120px"
						        DataMember="BnbGrant.ProjectTotalCostSterling" DecimalPlaces="0" />
						</td>
						</tr>
						<tr>
						    <td class="label">Income Type</td>
						    <td><bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol7" runat="server" CssClass="lookupcontrol" Height="22px" Width="120px"
									DataMember="BnbGrant.IncomeTypeID" LookupTableName="lkuIncomeType" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"
									TextAlign="right"></bnbdatacontrol:bnblookupcontrol>
						    </td>	
						</tr>
									
					</TABLE>
					</div>
				<br />
				<asp:Panel id="Panel2" runat="server">
					 <table cellspacing="0px">
                        <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlIncomePipeline','dvIncomePipeline'); return false;">
                            <td style="width: 500px; padding: 5px;">
                                 Proposal - Phasing for income pipeline</td>
                            <td align="right" style="width: 450px; padding: 5px;">
                                <a href="#" id="hlIncomePipeline">- Hide Details</a></td>
                        </tr>
                    </table>
                    <div id="dvIncomePipeline" style="display:block;"><br />
                        <span>
                            The quarterly income pipeline report provides a picture of how much income we expect
                            to receive in current and future years from pending proposals. Using the donor�s
                            currency, please phase the total requested donor amount of <b><span id="spnDonorCurrency" runat="server"/></b>
                            across one or more VSO financial years so the report can allocate income to the
                            correct year. Where income from the proposal is expected within a single year there
                            will just be one row in the table below. Where income is expected across more than
                            one year there will be more than one row in the table. Finance will then discount
                            the phased income figures when they produce the quarterly income pipeline report
                            to reflect the fact that this is expected but not secured income.
                        </span>
                        <P><bnbdatagrid:bnbdatagridfordomainobjectlist id="bnbdgIncomePipeline" runat="server" CssClass="datagrid" Width="500px" DataMember="BnbGrant.GrantIncomePipelineList"
						DataProperties="FinancialYearID [BnbLookupControl:lkuVSOFinancialYear], ExpectedAnnualIncomeCurrency [BnbDecimalBox:150px]"
						DeleteButtonVisible="true" EditButtonVisible="true" AddButtonText="Add" EditButtonText="Edit" AddButtonVisible="true"
						VisibleOnNewParentRecord="false" SortBy="CreatedOn" DataGridType="Editable" LineBreaksOnColumnNames="True"></bnbdatagrid:bnbdatagridfordomainobjectlist></P>
                    </div>
                </asp:Panel>
					<p>
                    <asp:HyperLink ID="uxGeneratePMPGForm" runat="server">Generate Project Proposal Approval Form</asp:HyperLink>&nbsp;</p>
                     <br>
				    </asp:panel>	
				    </div> 		    
			</form>
		</body>
</html>
