using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;
using BonoboEngine.Query;
using BonoboEngine;

namespace Frond.EditPages
{
    public partial class SFApplicationPage : System.Web.UI.Page
    {
        private BnbWebFormManager bnb = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            bnb = new BnbWebFormManager(this, "BnbApplication");
            bnb.LogPageHistory = true;
            BnbWorkareaManager.SetPageStyleOfUser(this);
            string posID = Page.Request.QueryString["BnbApplication"];
            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null)
                    titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
            }
            doVisibilityReturnDetails();
        }


        private bool hasPermissionToEditEOSDetails()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().WebStyleID == 11)
            {
                if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_StarfishEditEOSDetails))
                    return true;
                else
                    return false;
            }
            return true;
        }

        private void doVisibilityReturnDetails()
        {
            if (!(hasPermissionToEditEOSDetails()))
            {
                BnbDataGridForDomainObjectList7.DisableAllButtons = true;
            }
            else
            {
                BnbDataGridForDomainObjectList7.DisableAllButtons = false;

            }
        }

    }
}
