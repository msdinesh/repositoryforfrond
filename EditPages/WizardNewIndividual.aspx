<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="IndividualAddressPanel" Src="../UserControls/IndividualAddressPanel.ascx" %>
<%@ Page language="c#" Codebehind="WizardNewIndividual.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.WizardNewIndividual" %>
<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML xmlns="http://www.w3.org/1999/xhtml">
	<HEAD>
		<title>WizardNewOrganisation</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<script language="javascript" src="../Javascripts/GoogleMap.js"></script>
	</HEAD>
	<body onload="initialize()" onunload="GUnload()">
		<FORM id="Form2" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<DIV class="wizardheader" ms_positioning="FlowLayout">
					<H3><IMG class="icon24" src="../Images/wizard24silver.gif"> New Individual Wizard</H3>
				</DIV>
				<asp:panel id="panelZero" runat="server" CssClass="wizardpanel">
					<P><EM>This wizard will help you to add a new&nbsp;Individual record.</EM>
					<P>Please enter the Surname, Postcode and Country&nbsp;of the new&nbsp;Individual 
						and press Next</P>
					<P></P>
					<P>
						<TABLE class="wizardtable" id="Table2">
							<TR>
								<TD class="label" style="HEIGHT: 15px">Forename (or Initials)</TD>
								<TD style="HEIGHT: 15px">
									<asp:TextBox id="txtForename" runat="server" Width="236px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD class="label" style="HEIGHT: 15px">Surname</TD>
								<TD style="HEIGHT: 15px">
									<asp:TextBox id="txtSurname" runat="server" Width="236px"></asp:TextBox></TD>
							</TR>
						</TABLE>
					<P>
						<asp:label id="lblPanelZeroFeedback" runat="server" CssClass="feedback"></asp:label></P>
				</asp:panel>
				<asp:panel id="panelOne" runat="server">
<asp:Panel id="panelIndividualDup" Runat="server">
<P>The following similar&nbsp;Individuals already exist. If any of them match the data you were going to 
							enter, then please use them instead by clicking on the appropriate Select 
							button.</P>
<P></P>
<P>
							<asp:DataGrid id="dataGridIndividuals" runat="server" CssClass="datagrid" AutoGenerateColumns="False">
								<Columns>
									<asp:ButtonColumn Text="Select" ButtonType="PushButton" CommandName="Select"></asp:ButtonColumn>
									<asp:BoundColumn Visible="False" DataField="tblIndividualKeyInfo_IndividualID" HeaderText="HiddenID"></asp:BoundColumn>
									<asp:BoundColumn DataField="tblIndividualKeyInfo_Surname" HeaderText="Surname"></asp:BoundColumn>
									<asp:BoundColumn DataField="tblIndividualKeyInfo_Forename" HeaderText="Forename"></asp:BoundColumn>
									<asp:BoundColumn DataField="tblIndividualKeyInfo_SurnamePrefix" HeaderText="Surname Prefix"></asp:BoundColumn>
									<asp:BoundColumn DataField="tblIndividualKeyInfo_old_indv_id" HeaderText="Ref No"></asp:BoundColumn>
									<asp:BoundColumn DataField="lkuCountry_Description" HeaderText="Country"></asp:BoundColumn>
									<asp:BoundColumn DataField="tblAddress_AddressLine1" HeaderText="Address Line 1"></asp:BoundColumn>
								</Columns>
							</asp:DataGrid></P>
<P>If none of these existing Individuals&nbsp;match, then press Next to create 
a&nbsp;new Individual. </asp:Panel>
<asp:label id="labelNoDuplicates" runat="server">No similar Individuals already exist. Press Next to continue.</asp:label></P>
<P>
						<asp:label id="lblPanelOneFeedback" runat="server" CssClass="feedback"></asp:label>
				</asp:panel>
				<asp:panel id="panelTwo" runat="server" CssClass="wizardpanel">
					<P>Please enter further details about the new Individual, then press Finish to 
						create the new record:</P>
					<P>
						<uc1:IndividualAddressPanel id="IndividualAddressPanel1" runat="server"></uc1:IndividualAddressPanel>
						<asp:Label id="lblPanelTwoFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				</asp:panel>
				<P></P>
				<P><uc1:wizardbuttons id="wizardButtons" runat="server"></uc1:wizardbuttons></P>
				<P><asp:label id="labelHiddenRequestIDUnused" runat="server" Visible="False"></asp:label>
					<asp:TextBox id="txtHiddenReferrer" runat="server" Visible="False"></asp:TextBox></P>
			</div>
		</FORM>
	</body>
</HTML>
