<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Page language="c#" Codebehind="ContactBlockPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.ContactBlockPage" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>ContactBlockPage</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal><LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet"><asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent"><uc1:title id="titleBar" runat="server" imagesource="../Images/bnbindividualcontactblock24silver.gif"></uc1:title><uc1:navigationbar id="NavigationBar1" runat="server"></uc1:navigationbar><bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" NewButtonEnabled="False" UndoText="Undo" SaveText="Save"
					EditAllText="Edit All" NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" CssClass="databuttons"></bnbpagecontrol:bnbdatabuttons><bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:bnbmessagebox>
				<TABLE class="fieldtable" id="Table1" width="100%" border="0">
					<TR>
						<TD class="label" width="40%">Individual/Volunteer</TD>
						<TD><asp:hyperlink id="hlkIndividual" runat="server"></asp:hyperlink></TD>
					</TR>
					<TR>
						<TD class="label" width="40%">Blocking Type</TD>
						<TD><bnbdatacontrol:bnblookupcontrol id="bnblckBlockingType" runat="server" CssClass="lookupcontrol" AutoPostBack="True"
								DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuContactBlockType"
								DataMember="BnbIndividualContactBlock.ContactBlockTypeID" Height="22px" Width="250px"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" width="40%">From</TD>
						<TD><bnbdatacontrol:bnbdatebox id="BnbDateBox1" runat="server" CssClass="datebox" DataMember="BnbIndividualContactBlock.FromDate"
								Height="20px" Width="90px" DateCulture="en-GB" DateFormat="dd/mm/yyyy"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
					<TR>
						<TD class="label" width="40%">To</TD>
						<TD><bnbdatacontrol:bnbdatebox id="BnbDateBox2" runat="server" CssClass="datebox" DataMember="BnbIndividualContactBlock.ToDate"
								Height="20px" Width="90px" DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
					<TR>
						<TD class="label" width="40%">Reason</TD>
						<TD><bnbdatacontrol:bnbtextbox id="BnbTextBox1" runat="server" CssClass="textbox" DataMember="BnbIndividualContactBlock.Reason"
								Height="20px" Width="250px" StringLength="0" Rows="3" MultiLine="True"></bnbdatacontrol:bnbtextbox></TD>
					</TR>
					<TR>
						<TD class="label" width="40%">User</TD>
						<TD><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl2" runat="server" CssClass="lookupcontrol" DataTextField="Description"
								ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboUser" DataMember="BnbIndividualContactBlock.BlockedByPersonID"
								Height="22px" Width="250px"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" width="40%">Dept</TD>
						<TD><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl3" runat="server" CssClass="lookupcontrol" DataTextField="Description"
								ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="appDepartment" DataMember="BnbIndividualContactBlock.BlockedByDepartmentID"
								Height="22px" Width="250px"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" width="40%">Job Title</TD>
						<TD><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl4" runat="server" CssClass="lookupcontrol" DataTextField="Description"
								ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="appJobTitle" DataMember="BnbIndividualContactBlock.BlockedByJobTitleID"
								Height="22px" Width="250px"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" width="40%">Selected Mail</TD>
						<TD>
							<P><bnbdatacontrol:bnblookupcontrol id="bnblckSelectedMail" runat="server" CssClass="lookupcontrol" DataTextField="Description"
									ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuSelectedContactType" DataMember="BnbSelectedContact.SelectedContactTypeID"
									Height="22px" Width="250px"></bnbdatacontrol:bnblookupcontrol><bnbdatagrid:bnbdatagridfordomainobjectlist id="BnbDataGridForDomainObjectList1" runat="server" CssClass="datagrid" DataMember="BnbIndividualContactBlock.SelectedContacts"
									Width="200px" DataGridType="Editable" AddButtonVisible="True" EditButtonText="Edit" AddButtonText="Add" EditButtonVisible="true" DeleteButtonVisible="True" DataProperties="SelectedContactTypeID [BnbLookupControl:lkuSelectedContactType]"></bnbdatagrid:bnbdatagridfordomainobjectlist></P>
						</TD>
					</TR>
				</TABLE>
				<p>&nbsp;</p>
			</div>
		</form>
	</body>
</HTML>
