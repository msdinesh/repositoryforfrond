//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.1433
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Frond.EditPages {
    
    public partial class XmlImportDetailsPage {
        protected System.Web.UI.WebControls.Literal literalBeforeCss;
        protected System.Web.UI.WebControls.Literal literalAfterCss;
        protected System.Web.UI.HtmlControls.HtmlForm form1;
        protected Frond.UserControls.MenuBar menuBar;
        protected Frond.UserControls.Title titleBar;
        protected System.Web.UI.WebControls.Panel panelXmlImportDetails;
        protected System.Web.UI.WebControls.Label lblRowsForImport;
        protected System.Web.UI.WebControls.Label lblSuccessRows;
        protected System.Web.UI.WebControls.Label lblFailedRows;
        protected BonoboWebControls.DataGrids.BnbDataGridForView BnbAuditView;
        protected System.Web.UI.WebControls.Button btnCancel;
    }
}
