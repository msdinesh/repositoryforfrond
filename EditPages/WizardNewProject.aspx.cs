using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;

namespace Frond.EditPages
{
    /// <summary>
    /// This class is used to create a new project using Wizard.
    /// </summary>
    public partial class WizardNewProject : System.Web.UI.Page
    {
        protected UserControls.WizardButtons wizardButtons;

        #region " Page_Load "

        protected void Page_Load(object sender, EventArgs e)
        {
            
            BnbEngine.SessionManager.ClearObjectCache();
            BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);

            wizardButtons.AddPanel(panelZero);
            wizardButtons.AddPanel(panelOne);
            wizardButtons.AddPanel(panelTwo);
            wizardButtons.AddPanel(panelThree);
            wizardButtons.AddPanel(panelFour);


            if (!this.IsPostBack)
            {
                BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
                if (this.Request.UrlReferrer != null)
                {
                    txtHiddenReferrer.Text = this.Request.UrlReferrer.ToString();
                }
                else
                    txtHiddenReferrer.Text = "~/Menu.aspx";

                fillProgrammeOfficeCombo();
                fillProjectStatusCombo();
                fillProjectOfficersAndDepartmentCombo();
                fillOverallRiskCombo();
                fillCorporateOperationalPlansCombo();
                fillMatchedFundingCombo();
                fillContractualConditionCombo();
                fillPOCapacityCombo();
                fillVolunteerRecruitabiltyCombo();
                fillVSOGoalCombo();
                fillProjectRegionCombo();
                fillProjectFeasiblityCombo();

            }
            rbtnCheckList.Attributes.Add("onclick", "javascript:showChecklist();");
        }

        #endregion

        #region " Web Form Designer generated code "

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
            wizardButtons.ValidatePanel += new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardbuttons_ValidatePanel);
            wizardButtons.ShowPanel += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardbuttons_ShowPanel);
            wizardButtons.CancelWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardbuttons_CancelWizard);
            wizardButtons.FinishWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
            
            wizardButtons.FirstPanelNextText = "BEGIN >>";
        }

        private void InitializeComponent()
        {

        }

        #endregion

        
        
        #region " SetupPanelZero "

        public void SetupPanelZero()
        {
            panelOne.Visible = false;
            panelTwo.Visible = false;
        }

        #endregion

        #region " SetupPanelOne "

        public void SetupPanelOne()
        {
            panelZero.Visible = false;
            panelOne.Visible = true;
        }

        #endregion

        #region " SetupPanelTwo "

        public void SetupPanelTwo()
        {
            lblPanelTwoFeedback.Text = "";
            string projectTitle = txtProjectTitle.Text;
            BnbTextCondition titleMatch = new BnbTextCondition("tblProject_ProjectTitle", txtProjectTitle.Text, BnbTextCompareOptions.StartOfField);
            BnbCriteria dupCriteria = new BnbCriteria(titleMatch);
            BnbQuery dupQuery = new BnbQuery("vwBonoboFindProjectTitle", dupCriteria);
            BnbQueryDataSet dupDataset = dupQuery.Execute();
            DataTable dupResults = BonoboWebControls.BnbWebControlServices.GetHtmlEncodedDataTable(dupDataset.Tables["Results"]);
            if (dupResults.Rows.Count > 0)
            {
                dupProjectTitle.Visible = true;
                labelNoDuplicates.Visible = false;
                MatchTitleGrid.DataSource = dupResults;
                MatchTitleGrid.DataBind();
            }
            else
            {

                //TPT Amended:PGMSP-19 - skipping 2nd wizard i.e "No similar records founds.Press next to continue"
                panelTwo.Visible = false;
                panelThree.Visible = true;
                if (ViewState["ProjectTitle"] != null)
                    txtProjectTitleStep3.Text = ViewState["ProjectTitle"].ToString();
                else
                    txtProjectTitleStep3.Text = txtProjectTitle.Text;
                //dupProjectTitle.Visible = false;
                //labelNoDuplicates.Visible = true;
                wizardButtons.disablePanel = true;
            }

        }

        #endregion

        #region " SetupPanelThree "

        public void SetupPanelThree()
        {
            if (ViewState["ProjectTitle"] != null)
                txtProjectTitleStep3.Text = ViewState["ProjectTitle"].ToString();
            else
                txtProjectTitleStep3.Text = txtProjectTitle.Text;
        }

        #endregion

        #region " SetupPanelFour "

        public void SetupPanelFour()
        {
            lblProjectTitleStep4.Text = txtProjectTitleStep3.Text;
            if (rbtnCheckList.SelectedValue.ToLower() == "project")
            {
                pnlRiskDetails.Visible = true;
            } 
        }

        #endregion

        #region " wizardbuttons_ValidatePanel "

        private void wizardbuttons_ValidatePanel(object sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
        {
            if (e.CurrentPanel == 0)
            {
                if (rbtnCheckList.SelectedValue == "")
                {
                    e.Proceed = false;
                    lblPanelZeroFeedback.Text = "Please Select an option";
                }
                
            }

            if (e.CurrentPanel == 1)
            {
                if (txtProjectTitle.Text.Trim() == "")
                {
                    e.Proceed = false;
                    lblPanelOneFeedback.Text = "Please enter the Project Title";
                }
            }
            if (e.CurrentPanel == 2)
            {
                foreach (DataGridItem gridItem in MatchTitleGrid.Items)
                {
                    if (((RadioButton)gridItem.FindControl("rbtnProject")).Checked == true)
                        NavigateToProjectPage();
                }

            }
            if (e.CurrentPanel == 3)
            {
                ViewState["ProjectTitle"] = txtProjectTitleStep3.Text;
                Page.Validate();
                if (!Page.IsValid)
                {
                    e.Proceed = false;
                }
            }
        }

        #endregion

        #region " wizardbuttons_ShowPanel "

        private void wizardbuttons_ShowPanel(object sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            if (e.CurrentPanel == 0)
            {
                this.SetupPanelZero();
            }
            if (e.CurrentPanel == 1)
            {
                this.SetupPanelOne();
            }
            if (e.CurrentPanel == 2)
            {
                this.SetupPanelTwo();
            }
            if (e.CurrentPanel == 3)
            {
                this.SetupPanelThree();
            }
            if (e.CurrentPanel == 4)
            {
                this.SetupPanelFour();
            }

        }

        #endregion

        #region " wizardbuttons_CancelWizard "

        private void wizardbuttons_CancelWizard(object sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            Response.Redirect(txtHiddenReferrer.Text);
        }

        #endregion

        #region " wizardButtons_FinishWizard "

        private void wizardButtons_FinishWizard(object sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
                 this.CreateNewProject();
        }

        #endregion

        #region " doViewProject "

        //allows user to select only one radiobutton at a time
        protected void doViewProject(object sender, EventArgs e)
        {

            string m_ClientID = "";
            RadioButton rbtn = (RadioButton)(sender);
            m_ClientID = rbtn.ClientID;
            foreach (DataGridItem item in MatchTitleGrid.Items)
            {
                rbtn = (RadioButton)(item.FindControl("rbtnProject"));
                rbtn.Checked = false;
                if (m_ClientID == rbtn.ClientID)
                {
                    rbtn.Checked = true;
                    ViewState["selValue"] = (item.Cells[1].Text);
                }
            }
        }

        #endregion

        #region " NavigateToProjectPage "
        //Based on the selection from the grid, the page will be redirected to the respective project

        private void NavigateToProjectPage()
        {
            if (ViewState["selValue"] != null)
                Response.Redirect("ProjectPage.aspx?BnbProject=" + ViewState["selValue"].ToString());

        }

        #endregion

        #region " btnClear_Click "
        //Clears the radiobuttons in the grid

        protected void btnClear_Click(object sender, EventArgs e)
        {
            foreach (DataGridItem item in MatchTitleGrid.Items)
            {
                RadioButton rbtn = (RadioButton)(item.FindControl("rbtnProject"));
                rbtn.Checked = false;
            }

        }

        #endregion

        #region " CreateNewProject "

        private void CreateNewProject()
        {
            BnbEditManager em = new BnbEditManager();
            BnbProject newProject = new BnbProject(true);
            newProject.RegisterForEdit(em);
            SaveProjectMainDetails(newProject, em);
            SaveProjectFinancialAndRiskDetails(newProject, em);
            try
            {
                em.SaveChanges();
                BnbEngine.SessionManager.ClearObjectCache();
                Response.Redirect("ProjectPage.aspx?BnbProject=" + newProject.ID.ToString().ToUpper());
            }
            catch (BnbProblemException e)
            {
                lblPanelFourFeedback.Text = "Unfortunately, the Project could not be created due to the following problems: <br/>";
                foreach (BnbProblem p in e.Problems)
                    lblPanelFourFeedback.Text += p.Message + "<br/>";
            }
        }

        #endregion

    }
}
