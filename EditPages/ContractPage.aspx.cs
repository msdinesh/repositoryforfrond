using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboWebControls.DataControls;
using BonoboWebControls.Collections;
using BonoboDomainObjects;
using BonoboEngine;
using BonoboEngine.Query;
using System.Collections.Specialized;
using System.Text;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for ContractPage.
    /// </summary>
    public partial class ContractPage : System.Web.UI.Page
    {
        protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox10;
        protected BonoboWebControls.DataControls.BnbTextBox Bnbtextbox3;

        private BnbWebFormManager bnb = null;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAddContractProgress;
        private BnbGrant grant = null;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here

            //<!-- PGM-204
            doDisplayDeletedCheckBox();
            //-->
 #if DEBUG
            if (Request.QueryString.ToString() == "")
            {

                //this reloads the page with an ID for an employer not sure if this will work
                BnbWebFormManager.ReloadPage("BnbGrant=b27be9c7-605a-46fd-b908-3edaa5542de0");
                return;
            }
#endif
            bnb = new BnbWebFormManager(this, "BnbGrant");

            string querystring = Page.Request.QueryString.ToString();

            grant = bnb.GetPageDomainObject("BnbGrant") as BnbGrant;

            //If Project is missing in Querystring, reload page with it using the BnbGrant.Project.ID
            if (querystring.IndexOf("BnbProject") == -1)
            {
                System.Guid projguid = ((BnbGrant)bnb.ObjectTree.DomainObjectDictionary["BnbGrant"]).Project.ID;
                BnbWebFormManager.ReloadPage("BnbProject=" + projguid + "&" + querystring);
            }

            if (Request.QueryString["Ed"] == "1")
            {
                // we need to go into edit mode in a roundabout way ....
                // to get around the way the BnbWebFormManager works
                // set session flag for next postback
                Session["NextPageInEditMode"] = 1;
                // reload page without Ed bit of querystring
                BnbWebFormManager.ReloadPage(bnb.GetQueryStringFromCurrentPageWithDomainObjectKeyValuesOnly());
            }
            // if edit flag set
            if (Session["NextPageInEditMode"] != null && Session["NextPageInEditMode"].ToString() == "1")
            {
                // clear flag
                Session.Remove("NextPageInEditMode");
                // go into edit mode
                bnb.Click_EditAll(sender, e);
            }


            
            BnbWorkareaManager.SetPageStyleOfUser(this);
            bnb.PageNameOverride = "Contract";
            bnb.LogPageHistory = true;

           
            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null && grant != null)
                {
                    StringBuilder sb = new StringBuilder(Utilities.ProjectSubHlink);
                    sb.Replace("[PROJECTID]", grant.Project.ID.ToString());
                    sb.Replace("[TITLE]", grant.Project.ProjectTitle);
                    sb.Replace("[SUBTITLE]", bnb.PageTitleDescriptionCurrent);
                    titleBar.TitleText = sb.ToString();

                }

                // TPT Amended code PGMS - 7.2.1
                //Identify the url and show the related tab when the user Click the ClaimsAndReports in contract and proposal grid
                if (this.BnbTabControl1.SelectedTabID == "")
                {
                    if (this.Request != null)
                    {
                        string url = this.Request.Url.ToString();
                        if (url.IndexOf("ControlID") > -1)
                        {
                            if (url.IndexOf("Main") > -1)
                                BnbTabControl1.SelectedTabID = Main.ID;
                            if (url.IndexOf("Progress") > -1)
                                BnbTabControl1.SelectedTabID = Progress.ID;
                            if (url.IndexOf("Claims") > -1)
                                BnbTabControl1.SelectedTabID = Claims.ID;
                            if (url.IndexOf("Reports") > -1)
                                BnbTabControl1.SelectedTabID = Reports.ID;
                        }
                    }
                }
            }
            doProposalHyperlink();
            doCurrencyLabel();
            CheckProjectStatusDate();
            doPMGPFormHyperlinks();
            doShowViewWarnings();
            NavigateReportsPage();
            doPMFStatus();
        }

        private void doShowViewWarnings()
        {
            // if its a postback, hide the warnings
            if (this.IsPostBack)
            {
                lblViewWarnings.Text = "";
                lblViewWarnings.Visible = false;
            }
            else
            {
                // call shared code to show any view warnings
                Utilities.ShowViewWarnings(bnb, lblViewWarnings);
            }
        }

        

        private void doPMGPFormHyperlinks()
        {
            BnbGrant grant = bnb.GetPageDomainObject("BnbGrant") as BnbGrant;
            if (grant != null)
            {
                // need to pass grant id to wizard
                uxGeneratePMPGContractApproval.NavigateUrl = String.Format("WizardGeneratePMPGForms.aspx?BnbProject={0}&BnbGrant={1}&DocType={2}",
                    grant.ProjectID.ToString(), grant.ID.ToString(), 2002);
                uxGeneratePMPGGrantClosure.NavigateUrl = String.Format("WizardGeneratePMPGForms.aspx?BnbProject={0}&BnbGrant={1}&DocType={2}",
                    grant.ProjectID.ToString(), grant.ID.ToString(), 2005);
            }
        }

        private void doCurrencyLabel()
        {
            BnbGrant grant = bnb.GetPageDomainObject("BnbGrant") as BnbGrant;
            if (grant != null)
            {
                BonoboEngine.Query.BnbLookupDataTable mycurr = BnbEngine.LookupManager.GetLookup("lkuCurrency");
                if (grant.CurrencyID != BnbDomainObject.NullInt && grant.CurrencyID != 0)
                    lblDonorCurrency.Text = mycurr.FindByID(grant.CurrencyID).Description;
            }
        }

        private void doProposalHyperlink()
        {
            string proposalQuerystring = bnb.GetQueryStringFromCurrentPageWithDomainObjectKeyValuesOnly();
            BnbProject project = bnb.GetPageDomainObject("BnbProject") as BnbProject;
            
            if (project != null && grant != null)
            {
                uxProposalPageHyper.Enabled = true;
                uxProposalPageHyper.NavigateUrl = String.Format("../EditPages/ProposalPage.aspx?" + proposalQuerystring + "&ControlID=proposal");
            }
            else
            {
                uxProposalPageHyper.Enabled = false;
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            BnbProject project = bnb.GetPageDomainObject("BnbProject") as BnbProject;
            if (project != null)
                Response.Redirect("ProjectPage.aspx?BnbProject=" + project.ID + "&ControlID=DMSDocuments");
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            bnbdgHistory.PreRender += new EventHandler(bnbdgHistory_PreRender);
            bnbdgClaims.PreRender += new EventHandler(bnbdgClaims_PreRender);
            //this.bnbdgReports.PreRender += new System.EventHandler(this.bnbdgReports_PreRender);
            //<-- PGM-95
            lblGlobal.PreRender += new EventHandler(lblGlobal_PreRender);
            //-->

            //<!-- PGM-233, PGM-236
            bnbdgProgrammeOffices.PreRender += new EventHandler(Regions_PreRender);
            //-->
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        private void CheckProjectStatusDate()
        {
            BnbProject objProject = (BnbProject)bnb.ObjectTree.DomainObjectDictionary["BnbProject"];
            if (objProject == null) return;

            if (objProject.EndDate <= DateTime.Today)
            {
                if (objProject.CurrentProjectStatus.ProjectStatusID != 1004)
                {
                    lblProjectStatusMessage.Text = "Please update Project End Date or close project.";
                }
            }
        }

        protected void bnbdgHistory_PreRender(object sender, EventArgs e)
        {

            bnbdgHistory.Dispose();

            //<!--PGM-334 and PGM-305 - 1046 GrantApprovalAdmin
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(1062))
            {
                bnbdgHistory.EditButtonVisible = true;
                bnbdgHistory.DeleteButtonVisible = true;
            }

            //filters the list of proposals for the grid

            BnbDomainObjectCollection bnbcol = new BnbDomainObjectCollection();

            foreach (BnbDomainObject bnbitem in bnbdgHistory.ObjectTree.AllDomainObjects)
            {
                bnbcol.Add(bnbitem);
            }

            bnbdgHistory.ObjectTree.AllDomainObjects.Clear();
            bnbdgHistory.ObjectTree.DomainObjectDictionary.Clear();

            int x = 0;
            for (int i = 0; i < bnbcol.Count; i++)
            {
                BnbGrantProgress progressitem = (BnbGrantProgress)bnbcol[i];
                if (progressitem.IsContract || progressitem.IsNew)
                {
                    x = (progressitem.IsNew) ? -1 : x;

                    bnbdgHistory.ObjectTree.AllDomainObjects.Add(progressitem);
                    bnbdgHistory.ObjectTree.DomainObjectDictionary.Add(bnbdgHistory.ID + "_r" + x, progressitem);
                    x++;
                }
            }
            //<!--PGM-334 
            //			bnbdgHistory.ObjectTree.BindDomainObjectsToDataControls();

            //<!-- PGM-206
            if (bnbdgHistory.ColumnAliases.Count != 0)
            {
                if (bnbdgHistory.ColumnAliases[3] != null)
                {
                    string currencyText = (lblDonorCurrency.Text + "" == "") ? "(Currency)" : lblDonorCurrency.Text;
                    if (lblDonorCurrency.Text + "" != "") bnbdgHistory.ColumnAliases[3] = "Grant<br>Amount(" + currencyText + ")";
                }
                //<!--PGM-350 -->
                if (bnbdgHistory.ColumnAliases[4] != null) bnbdgHistory.ColumnAliases[4] = "Exchange Rate";
                if (bnbdgHistory.ColumnAliases[5] != null) bnbdgHistory.ColumnAliases[5] = "Grant<br>Amount(�)";
                //-->
            }

            foreach (BnbDataControl bnbcontrol in bnbdgHistory.ObjectTree.AllDataControls)
            {
                if (bnbcontrol is BnbLookupControl) bnbcontrol.Width = Unit.Pixel(100);
                if (bnbcontrol is BnbTextBox) bnbcontrol.Width = Unit.Pixel(50);
                if (bnbcontrol is BnbDateBox) bnbcontrol.Width = Unit.Pixel(100);
                //<!-- PGM-206
                if (bnbcontrol is BnbDecimalBox)
                {
                    BnbDecimalBox dbx = (BnbDecimalBox)bnbcontrol;
                    dbx.FormatExpression = (dbx.DataProperty == "ExchangeRate") ? "#0.000" : "#,0";
                    dbx.Width = Unit.Pixel(100);
                }
                //-->
            }

            //Set fields for the top half
            UpdateContractAmounts();
            if (!bnbdgHistory.WasAddButtonClicked) return;

            //<!-- PGM-206
            BnbDecimalBox contractAmountCurrency = (BnbDecimalBox)this.FindControl("bnbdgHistory_r-1_c3");
            BnbDecimalBox exchangeRate = (BnbDecimalBox)this.FindControl("bnbdgHistory_r-1_c4");
            BnbDecimalBox contractAmountSterling = (BnbDecimalBox)this.FindControl("bnbdgHistory_r-1_c5");
            contractAmountCurrency.FormatExpression = "#,0";
            exchangeRate.FormatExpression = "#0.000";
            contractAmountSterling.FormatExpression = "#,0";
            //-->

            BnbLookupControl lucStatus = (BnbLookupControl)this.FindControl("bnbdgHistory_r-1_c0");
            if (lucStatus == null) return;

            //status
            lucStatus.SelectedValue = -1;

            BnbGrant grant = (BnbGrant)bnb.ObjectTree.DomainObjectDictionary["BnbGrant"];
            BnbGrantProgress progress = grant.CurrentContract;

            if (progress == null) return;

            // start date
            BnbDateBox startdate = (BnbDateBox)this.FindControl("bnbdgHistory_r-1_c1");
            startdate.Date = progress.StartDate;

            // end date
            BnbDateBox enddate = (BnbDateBox)this.FindControl("bnbdgHistory_r-1_c2");
            enddate.Date = progress.EndDate;

            //<!-- PGM-206
            //Amount Approved currency
            contractAmountCurrency.DecimalValue = progress.ContractAmountCurrency;

            //Exchange rate
            exchangeRate.DecimalValue = progress.ExchangeRate;

            //Amount approved Sterling
            contractAmountSterling.DecimalValue = progress.ContractAmountSterling;
            //-->

            //			// Contract Amount (currency)
            //			BnbDecimalBox contractamount = (BnbDecimalBox)this.FindControl("bnbdgHistory_r-1_c6");
            //			contractamount.DecimalValue=progress.ContractAmountCurrency;
            UpdateContractAmounts();


        }

        //protected void bnbdgReports_PreRender(object sender, EventArgs e)
        //{
        //    foreach (BnbDataControl control in bnbdgReports.ObjectTree.AllDataControls)
        //    {
        //        if (control is BnbLookupControl) control.Width = Unit.Pixel(150);
        //    }
        //}


        private void UpdateContractAmounts()
        {
            //Set fields for the top half
            BnbGrant grant = (BnbGrant)bnb.ObjectTree.DomainObjectDictionary["BnbGrant"];
            BnbGrantProgress progress = grant.CurrentContract;
            if (progress == null) return;

            lblContractStartDate.Text = progress.StartDate.ToLongDateString();
            lblContractEndDate.Text = progress.EndDate.ToLongDateString();
            lblContractAmountCurrency.Text = (Convert.ToDecimal(progress.ContractAmountCurrency)).ToString("#,0");
            lblContractAmountSterling.Text = (Convert.ToDecimal(progress.ContractAmountSterling)).ToString("#,0");
        }

        private void NavigateReportsPage()
        {
            BnbGrant grant = bnb.GetPageDomainObject("BnbGrant") as BnbGrant;
            if (grant != null)
                hlReports.NavigateUrl = string.Format("ContractReportingDatePage.aspx?BnbGrant={0}&BnbGrantReport=New&PageState=New&DisableRedirect=true", grant.ID.ToString());
        }


        protected void doDisplayDeletedCheckBox()
        {
            pnlDeleted.Visible = (BnbEngine.SessionManager.GetSessionInfo().HasPermission(1047));
        }

        //<!-- PGM-193
        protected void bnbdgClaims_PreRender(object sender, EventArgs e)
        {
            if (bnbdgClaims.ColumnAliases.Count == 0 || lblDonorCurrency.Text + "" == "") return;
            bnbdgClaims.ColumnAliases[5] = "Claim Amount<br>(" + lblDonorCurrency.Text + ")";
        }
        //->

        //<!-- PGM-233, PGM-236
        protected void Regions_PreRender(object sender, EventArgs e)
        {
            BnbProject project = (BnbProject)bnb.ObjectTree.DomainObjectDictionary["BnbProject"];
            if (project == null) return;

            string regions = project.GetRegionDescriptions();
            lblRegions3.Text = regions;
            lblRegions2.Text = lblRegions.Text = "(" + regions + ")";
        }
        //-->

        //<!-- PGM-95
        protected void lblGlobal_PreRender(object sender, EventArgs e)
        {
            BnbProject proj = bnb.GetPageDomainObject("BnbProject") as BnbProject;
            if (proj != null)
                lblGlobal.Text = (proj.Global) ? "Yes" : "No";
        }
        //-->

        protected void uxContractWordForm_Clicked(object sender, EventArgs e)
        {

            if (false)
            {
                string strTemplateName = "";
                string strFilename = "";
                string strView = "vwPMPGProjectForms";
                switch ("2")
                {
////                    case "1":
//                        strTemplateName = "PMPGGrantClosureForm.doc";
//                        strFilename = "PMPG GC Form.doc";
//                        //strView = "vwPMPGProjectForms";
//                        break;
                    case "2":
                        strTemplateName = "PMPGGrantContractApprovalForm.doc";
                        strFilename = "PMPG GCA Form.doc";
                        strView = "vwPMPGGrantProgressForm";
                        break;
                    default:
                        break;
                }
                

            }
        }
        
        private void doPMFStatus()
        {
            BnbProject proj = bnb.GetPageDomainObject("BnbProject") as BnbProject;
            if (proj != null)
            {
                PMFStatusPanel2.PMFStatusID = proj.PMFStatusID.ToString();
            }
        } 
       
       


    }
}
