<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%--<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.Datagrids" Assembly="BonoboWebControls" %>
--%>

<%@ Page Language="c#" Codebehind="WizardNewPlacement.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.EditPages.WizardNewPlacement" %>

<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>WizardNewPlacement</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet" />
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form2" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <div class="wizardheader" ms_positioning="FlowLayout">
                <h3>
                    <img class="icon24" src="../Images/wizard24silver.gif">
                    New Placement Wizard</h3>
            </div>
            <asp:Panel ID="panelOne" runat="server" Height="100px" CssClass="wizardpanel">
                <p>
                    <em>This wizard will help you to add a new Placement record.</em>
                    <p>
                        Please enter the Planned Start Date, Duration in months And Select Programme Area, VSO Goal, VSO Type for new Placement then press Finish</p>
                    <p>
                    </p>
                    <table class="wizardtable" id="Table2">
                        <tr>
                            <td class="label" width="40%">
                                Planned Start Date</td>
                            <td width="60%">
                                <bnbdatacontrol:BnbStandaloneDateBox ID="dtpPlannedDate" runat="server" CssClass="datebox"
                                    Width="90px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB" Height="20px"></bnbdatacontrol:BnbStandaloneDateBox>
                            </td>
                            
                        </tr>
                        <tr>
                            <td class="label" width="40%">
                                Duration in months</td>
                            <td width="60%">
                                <p>
                                    <asp:TextBox ID="txtDurationinMonths" runat="server" Width="224px"></asp:TextBox>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="height: 36px" width="40%">
                                Programme Area</td>
                            <td style="height: 36px" width="60%">
                                <p>
                                    
                                    <asp:DropDownList ID="cmbProgrammeArea" runat="server">
                                    </asp:DropDownList>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="height: 25px" width="40%">
                                VSO Goal</td>
                            <td style="height: 25px" width="60%" colspan="3">
                                <p>
                                    <asp:DropDownList ID="cmbVsoGoal" runat="server">
                                    </asp:DropDownList>
                                    <br />
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="40%">
                                VSO Type</td>
                            <td width="60%">
                                <asp:DropDownList ID="cmbVsoType" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="labelPanelOneFeedback" runat="server" CssClass="feedback"></asp:Label></p>
            </asp:Panel>
            <div class="wizardheader">
                <table id="Table1" style="margin: 0px" cellspacing="0" cellpadding="0" width="100%"
                    border="0">
                    <tr>
                        <td align="right" width="30%">
                            &nbsp;
                        </td>
                        <td align="center" width="40%">
                            <asp:Button ID="buttonCancel" runat="server" Text="Cancel" OnClick="buttonCancel_Click">
                            </asp:Button></td>
                        <td align="left" width="30%">
                            <asp:Button ID="buttonFinish" runat="server" Text="Finish" OnClick="buttonFinish_Click">
                            </asp:Button>
                        </td>
                    </tr>
                </table>
            </div>
            <p>
                &nbsp;</p>
            <asp:label id="lblHiddenUrlReferrer" runat="server" Visible="False"></asp:label>
        </div>
    </form>
</body>
</html>
