<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>

<%@ Page Language="c#" Codebehind="OrganisationPersonnelPage.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.EditPages.OrganisationPersonnelPage" %>

<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>OrganisationPersonnelPage</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet" />
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/bnbeventpersonnel24silver.gif">
            </uc1:Title>
            <table style="margin: 0px" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="left" style="width:50%" colspan="2">
                        <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons1" runat="server" NewButtonEnabled="False"
                            CssClass="databuttons" NewWidth="55px" EditAllWidth="55px" SaveWidth="55px" UndoWidth="55px"
                            HeightAllButtons="25px" NewText="New" EditAllText="Edit All" SaveText="Save"
                            UndoText="Undo"></bnbpagecontrol:BnbDataButtons>
                    </td>
                </tr>
            </table>
            <bnbpagecontrol:BnbMessageBox ID="BnbMessageBox1" runat="server" CssClass="messagebox">
            </bnbpagecontrol:BnbMessageBox>            
		<p>
                <table class="fieldtable" id="Table1" width="100%" border="0">
                    <tr>
                        <td class="label" style="width:50%">
                            Organisation</td>
                        <td style="width:50%">
                            <bnbdatacontrol:BnbDomainObjectHyperlink ID="BnbDomainObjectHyperlink1" runat="server"
                                RedirectPage="CompanyPage.aspx" GuidProperty="BnbCompany.ID" DataMember="BnbCompany.CompanyName"
                                HyperlinkText="[BnbDomainObjectHyperlink]" Target="_top"></bnbdatacontrol:BnbDomainObjectHyperlink>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width:50%">
                            Full Name</td>
                        <td style="width:50%">
                            <bnbdatacontrol:BnbTextBox ID="bnbtxtFullName" runat="server" CssClass="textbox"
                                Width="250px" Height="20px" DataMember="BnbIndividualAddress.Individual.FullName"
                                MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width:50%">
                            Ref No</td>
                        <td style="width:50%">
                            <bnbdatacontrol:BnbTextBox ID="bnbtxtRefNo" runat="server" CssClass="textbox" Width="250px"
                                Height="20px" DataMember="BnbIndividualAddress.Individual.RefNo" MultiLine="false"
                                Rows="1" StringLength="0"></bnbdatacontrol:BnbTextBox>
                            <asp:TextBox ID="txtRefNo" runat="server" Width="248px"></asp:TextBox>
                            <asp:Label ID="lblRefNo" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="label" style="width:50%">
                            Role</td>
                        <td style="width:50%">
                            <bnbdatacontrol:BnbTextBox ID="bnbtxtRole" runat="server" CssClass="textbox" Width="250px"
                                Height="20px" DataMember="BnbIndividualAddress.CompanyRole" MultiLine="false"
                                Rows="1" StringLength="0"></bnbdatacontrol:BnbTextBox>
                            <asp:TextBox ID="txtRole" runat="server" Width="248px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 24px;width:50%">
                            Dept</td>
                        <td style="height: 24px;width:50%">
                            <bnbdatacontrol:BnbTextBox ID="bnbtxtDept" runat="server" CssClass="textbox" Width="250px"
                                Height="20px" DataMember="BnbIndividualAddress.CompanyDepartment" MultiLine="false"
                                Rows="1" StringLength="0"></bnbdatacontrol:BnbTextBox>
                            <asp:TextBox ID="txtDept" runat="server" Width="248px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 24px;width:50%">
                            Address</td>
                        <td style="height: 24px;width:50%">
                            <p>
                                <bnbdatagrid:BnbDataGridForView ID="bnbvwWorkAddresses" runat="server" CssClass="datagrid"
                                    Width="360px" ViewLinksText="View" ShowOnNewButtonClick="true" NewLinkText="New"
                                    NewLinkVisible="False" ViewLinksVisible="False" DataGridType="RadioButtonList"
                                    ViewName="vwBonoboCompanyAddressesSummary" FieldName="lnkCompanyAddress_CompanyID"
                                    QueryStringKey="BnbCompany" GuidKey="tblAddress_AddressID" ColumnNames="FullAddress"
                                    DomainObject="BnbCompanyAddress"></bnbdatagrid:BnbDataGridForView>
                            </p>
                        </td>
                    </tr>
                </table>
</p>
        </div>
    </form>
</body>
</html>
