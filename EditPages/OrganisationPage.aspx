<%@ Register TagPrefix="uc1" TagName="NavigatePanel" Src="../UserControls/NavigatePanel.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Page language="c#" Codebehind="OrganisationPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.OrganisationPage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal><LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet"><asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<script type="text/javascript" language="javascript">
		function emailPageLink()
		{
		var str=encodeURIComponent(window.location);
        var replaceTabName = document.getElementById('BnbTabControl1_SelectedTabID').value;
        var url = decodeURIComponent(str);
        if(url.split("&ControlID=").length== 2)
        {
        var tabName = url.split("&ControlID=")[1];
        url = url.replace(tabName,replaceTabName);        
        window.open('mailto:?subject=PGMS-Page Link&body=%0d%0a%0d%0a%0d%0a%0d%0a'+encodeURIComponent(url)+''); window.focus(); return false;
        }
        else
        {
        url = url + '&ControlID=' + replaceTabName;        
        window.open('mailto:?subject=PGMS-Page Link&body=%0d%0a%0d%0a%0d%0a%0d%0a'+encodeURIComponent(url)+''); window.focus(); return false;
		} 
		}
		function loadEvent()
		{		
		    var editClicked=document.getElementById('BnbDataButtons1_Edit');
            if(editClicked.disabled)
            {
                var panelID1= document.getElementById('ethicalScreenID');
                panelID1.style.display='block';             
            } 
		}
		function showHidePanel(hyperlinkID,pnlID)
		{
		    var panelID= document.getElementById(pnlID);
		    var hpText=document.getElementById(hyperlinkID);		
		    if(panelID.style.display=='block')
		    {
		        panelID.style.display='none';
		        hpText.innerHTML="+ Show Details";
		    }
		    else
		    {
		        panelID.style.display='block';
		        hpText.innerHTML="- Hide Details";
		    }
		}
		</script>
	</HEAD>
	<body onload="loadEvent()">
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent" DESIGNTIMEDRAGDROP="2301"><uc1:title id="titleBar" runat="server" imagesource="../Images/bnbcompany24silver.gif"></uc1:title>
					<table><tr><td>
			<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" UndoText="Undo" SaveText="Save" EditAllText="Edit All"
					NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" NewButtonEnabled="True" CssClass="databuttons">
					</bnbpagecontrol:bnbdatabuttons></td>
					<td align="right" style="width:700px;"><a href="#" onclick="emailPageLink(); return false;"><b>Email Page Link</b></a></td></tr></table>
					<bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox">
					</bnbpagecontrol:bnbmessagebox>
					<bnbpagecontrol:bnbtabcontrol id="BnbTabControl1" runat="server"></bnbpagecontrol:bnbtabcontrol>
					<asp:panel id="MainTab" runat="server" CssClass="tcTab" BnbTabName="Main">
					<TABLE class="fieldtable" id="Table1" border="0">
						<TR>
							<TD class="label" width="25%">Organisation Name</TD>
							<TD width="50%">
								<bnbdatacontrol:bnbtextbox id="BnbTextBox1" runat="server" CssClass="textbox" DataMember="BnbCompany.CompanyName"
									MultiLine="false" Rows="1" StringLength="0" Width="250px" Height="20px"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Alias</TD>
							<TD width="50%">
								<bnbdatacontrol:bnbtextbox id="BnbTextBox2" runat="server" CssClass="textbox" DataMember="BnbCompany.CompanyShortName"
									MultiLine="false" Rows="1" StringLength="0" Width="250px" Height="20px"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Org Ref</TD>
							<TD width="50%">
								<bnbdatacontrol:bnbtextbox id="Bnbtextbox12" runat="server" CssClass="textbox" DataMember="BnbCompany.CompanyReference"
									MultiLine="false" Rows="1" StringLength="0" Width="250px" Height="20px"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">VSO Primary Contact</TD>
							<TD width="50%"><SPAN class="textbox">
									<bnbdatacontrol:bnblookupcontrol id="BnbLookupControl2" runat="server" CssClass="lookupcontrol" DataMember="BnbCompany.GCContactUserID"
										Width="250px" Height="22px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboUser"></bnbdatacontrol:bnblookupcontrol></SPAN></TD>
						</TR>
						 <tr>
                            <td class="label" width="25%">
                                VSO Primary Contact Department</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl7" runat="server" CssClass="lookupcontrol"
                                    DataMember="BnbCompany.KeyDonorContactID" DataTextField="Description" Height="22px"
                                    ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="appDepartment"
                                    Width="250px" />
                            </td>
                        </tr>
						<TR>
							<TD class="label" width="25%">Secondary Contact</TD>
							<TD width="50%">
								<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol4" runat="server" CssClass="lookupcontrol" DataMember="BnbCompany.GCSecondaryUserID"
									Width="250px" Height="22px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboUser"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>
						  <tr>
                        <td class="label" width="25%">
                            Donor Manager</td>
                        <td width="50%">
                            <bnbdatacontrol:BnbLookupControl ID="Bnblookupcontrol8" runat="server" CssClass="lookupcontrol"
                                DataMember="BnbCompany.DonorManagerID" Width="250px" Height="22px" DataTextField="Description"
                                ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboUser"></bnbdatacontrol:BnbLookupControl>
                        </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%">
                                 Donor Manager Department</td>
                            <td width="50%">
                                 <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl1" runat="server" CssClass="lookupcontrol"
                                    DataMember="BnbCompany.DonorManagerDepartmentID" DataTextField="Description"
                                    Height="22px" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="appDepartment"
                                    Width="250px" />
                            </td>
                        </tr>
						<TR>
							<TD class="label" width="25%">Web Site</TD>
							<TD width="50%">
								<bnbdatacontrol:bnbtextbox id="BnbTextBox3" runat="server" CssClass="textbox" DataMember="BnbCompany.WebHomePage"
									MultiLine="false" Rows="1" StringLength="0" Width="250px" Height="20px" HyperLink="True"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Industry Sector</TD>
							<TD width="50%">
								<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol5" runat="server" CssClass="lookupcontrol" DataMember="BnbCompany.IndustrySectorID"
									Width="250px" Height="22px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuIndustrySector"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Donor Type</TD>
							<TD width="50%">
								<bnbdatacontrol:BnbLookupControl id="BnbLookupControl3" runat="server" CssClass="lookupcontrol" DataMember="BnbCompany.FunderAgencyTypeID"
									Width="250px" Height="22px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuFunderAgencyType"></bnbdatacontrol:BnbLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Raisers Edge ID</TD>
							<TD width="50%">
								<bnbdatacontrol:bnbtextbox id="Bnbtextbox4" runat="server" CssClass="textbox" DataMember="BnbCompany.RaisersEdgeID"
									MultiLine="false" Rows="1" StringLength="0" Width="250px" Height="20px"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Relationship with VSO</TD>
							<TD width="50%">
								<bnbdatagrid:BnbDataGridForDomainObjectList id="BnbDataGridForDomainObjectList1" runat="server" CssClass="datagrid" DataMember="BnbCompany.CompanyRelationshipWithVSO"
									Width="200px" HideColumnHeaders="False" DataProperties="RelationshipWithVSOID [BnbLookupControl:lkuRelationshipWithVSO]"
									DataGridType="Editable" DataGridForDomainObjectListType="Editable" DeleteButtonVisible="True" EditButtonVisible="true" AddButtonText="Add"
									EditButtonText="Edit" AddButtonVisible="True" VisibleOnNewParentRecord="false" SortBy="ID"></bnbdatagrid:BnbDataGridForDomainObjectList></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Organisation Type</TD>
							<TD width="50%">
								<bnbdatagrid:BnbDataGridForDomainObjectList id="BnbDataGridForDomainObjectList2" runat="server" CssClass="datagrid" DataMember="BnbCompany.CompanyCompanyTypes"
									Width="200px" HideColumnHeaders="False" DataProperties="CompanyTypeID [BnbLookupControl:lkuCompanyType]" DataGridType="Editable"
									DataGridForDomainObjectListType="Editable" DeleteButtonVisible="True" EditButtonVisible="true" AddButtonText="Add" EditButtonText="Edit"
									AddButtonVisible="True" VisibleOnNewParentRecord="false" SortBy="ID"></bnbdatagrid:BnbDataGridForDomainObjectList></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Overseas Offices</TD>
							<TD width="50%">
								<bnbdatagrid:BnbDataGridForDomainObjectList id="BnbDataGridForDomainObjectList3" runat="server" CssClass="datagrid" DataMember="BnbCompany.CompanyOverseasOffices"
									Width="200px" HideColumnHeaders="False" DataProperties="CountryID [BnbLookupControl:lkuCountry]" DataGridType="Editable"
									DataGridForDomainObjectListType="Editable" DeleteButtonVisible="True" EditButtonVisible="true" AddButtonText="Add" EditButtonText="Edit"
									AddButtonVisible="True" VisibleOnNewParentRecord="false" SortBy="ID"></bnbdatagrid:BnbDataGridForDomainObjectList></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Specialisms
							</TD>
							<TD width="50%">
								<bnbdatagrid:BnbDataGridForDomainObjectList id="BnbDataGridForDomainObjectList4" runat="server" CssClass="datagrid" DataMember="BnbCompany.CompanySpecialisms"
									Width="200px" HideColumnHeaders="False" DataProperties="SpecialismID [BnbLookupControl:lkuQualification]" DataGridType="Editable"
									DataGridForDomainObjectListType="Editable" DeleteButtonVisible="True" EditButtonVisible="true" AddButtonText="Add" EditButtonText="Edit"
									AddButtonVisible="True" VisibleOnNewParentRecord="false" SortBy="ID"></bnbdatagrid:BnbDataGridForDomainObjectList></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Logged By</TD>
							<TD width="50%">
								<bnbdatacontrol:BnbLookupControl id="BnbLookupControl6" runat="server" CssClass="lookupcontrol" DataMember="BnbCompany.UpdatedBy"
									Height="22px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboUser"></bnbdatacontrol:BnbLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Logged On</TD>
							<TD width="50%">
								<bnbdatacontrol:BnbDateBox id="BnbDateBox1" runat="server" CssClass="datebox" DataMember="BnbCompany.UpdatedOn"
									Width="90px" Height="20px" DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbDateBox></TD>
						</TR>
					</TABLE>
					<br />
                <asp:Panel ID="pnlEthicallyCleared" runat="server">
                    <table cellspacing="0px">
                        <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlEthicalScreening','EthicalScreenID'); return false;">
                            <td style="width: 500px; padding: 5px;">
                                Ethical Screening</td>
                            <td align="right" style="width: 450px; padding: 5px;">
                                <a href="#" id="hlEthicalScreening">- Hide Details</a></td>
                        </tr>
                    </table>
                </asp:Panel>
                <div id="EthicalScreenID" style="display: block;">
                    <asp:Panel ID="pnlEthicalScreen" runat="server">
                        <br />
                        <table class="fieldtable" id="Table2" border="0">
                            <tr>
                                <td class="label" width="25%">
                                    Ethically Cleared?</td>
                                <td width="25%">
                                    <asp:CheckBox ID="chkBxEthicallyCleared" runat="server" Enabled="false" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label" width="25%">
                                    Ethically Cleared By</td>
                                <td width="25%">
                                    <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl9" runat="server" CssClass="lookupcontrol"
                                        DataTextField="Description" Height="22px" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
                                        LookupTableName="vlkuBonoboUser" DataMember="BnbCompany.EthicallyClearedByID" />
                                </td>
                                <td class="label" width="25%">
                                    Ethically Cleared On</td>
                                <td width="25%">
                                    <bnbdatacontrol:BnbDateBox ID="dbBxEthicallyClearedDate" runat="server" CssClass="datebox"
                                        DateCulture="en-GB" DateFormat="dd/MMM/yyyy" Height="20px" Width="90px" DataMember="BnbCompany.EthicallyClearedOn" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label" width="25%">
                                    Ethical Clearance : Decision Maker</td>
                                <td width="25%">
                                    <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl10" runat="server" CssClass="lookupcontrol"
                                        DataMember="BnbCompany.EthicalClearanceDecisionMakerID" Width="250px" Height="22px"
                                        DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboUser">
                                    </bnbdatacontrol:BnbLookupControl>
                                </td>
                            </tr>
                            <tr>
                                <td class="label" width="25%">
                                    Ethical Clearance : Last reviewed</td>
                                <td width="25%">
                                    <bnbdatacontrol:BnbDateBox ID="BnbDateBox2" runat="server" CssClass="datebox" DateCulture="en-GB"
                                        DateFormat="dd/MMM/yyyy" Height="20px" Width="90px" DataMember="BnbCompany.EthicalClearanceLastReviewed" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
					<P></P>
				</asp:panel><asp:panel id="AddressesTab" runat="server" CssClass="tcTab" BnbTabName="Addresses">
					<P>
						<bnbdatagrid:bnbdatagridforview id="BnbDataGridForView1" runat="server" CssClass="datagrid" Width="100%" ColumnNames="Custom_AddressLiveDesc, Custom_CompanyMainTelephone, Custom_CompanyMainFax, Custom_CombinedAddress"
							CriteriaParameter="lnkCompanyAddress_CompanyID" GuidKey="lnkCompanyAddress_AddressID" NewLinkText="New Address"
							NewLinkVisible="True" ViewLinksVisible="True" ViewName="vwBonoboCompanyAddresses3" QueryStringKey="BnbCompany"
							DomainObject="BnbCompanyAddress" GuidDataProperty="AddressID" RedirectPage="OrganisationAddressPage.aspx" FieldName="lnkCompanyAddress_CompanyID"
							DisplayNoResultMessageOnly="True" NoResultMessage="(No Address records for this Organisation)"></bnbdatagrid:bnbdatagridforview></P>
				</asp:panel><asp:panel id="PersonnelTab" runat="server" CssClass="tcTab" BnbTabName="Personnel">
					<asp:Hyperlink id="hypNewPersonnel" runat="server">New Personnel</asp:Hyperlink><br /><br />
					<asp:LinkButton ID="lnkBtnRemovePersonnel" runat="Server" OnClick="lnkBtnRemovePersonnel_Click">Remove Personnel</asp:LinkButton><br />
					<br />
					<asp:label id="lblPersonnelWarnings" runat="server" CssClass="messagebox"></asp:label>
					<asp:label id="lblPersonnelMessage" runat="server" ForeColor="green" Font-Bold="true"></asp:label>
					<P>
						<bnbdatagrid:bnbdatagridforview id="BnbDataGridForView2" runat="server" CssClass="datagrid" Width="100%" ColumnNames="Custom_DisplayName, tblIndividualKeyInfo_old_indv_id, Custom_AddressTelephone, Custom_Email,  lnkIndividualAddress_CompanyRole, lnkIndividualAddress_CompanyDepartment, tblAddress_AddressLine1"
							CriteriaParameter="tblCompany_CompanyID" GuidKey="tblIndividualKeyInfo_IndividualID" NewLinkText="New Personnel"
							NewLinkVisible="False" ViewLinksVisible="False" ViewName="vwBonoboCompanyPersonnel" QueryStringKey="BnbCompany"
							DomainObject="BnbIndividual" GuidDataProperty="IndividualID" RedirectPage="WizardNewPersonnel.aspx" FieldName="tblCompany_CompanyID"
							DisplayNoResultMessageOnly="True" NoResultMessage="(No Personnel records for this Organisation)"></bnbdatagrid:bnbdatagridforview></P>
				</asp:panel><asp:panel id="ActionsTab" runat="server" CssClass="tcTab" BnbTabName="Actions">
					<asp:Hyperlink id="hypNewAction" runat="server">New Action</asp:Hyperlink>
					<P>
						<bnbdatagrid:bnbdatagridforview id="BnbDataGridForView3" runat="server" CssClass="datagrid" Width="100%" CriteriaParameter="tblCompany_CompanyID"
							GuidKey="tblContactDetail_ContactDetailID" NewLinkText="New Action" NewLinkVisible="False" ViewLinksVisible="True"
							ViewName="vwBonoboCompanyContactDetails" QueryStringKey="BnbCompany" DomainObject="BnbContactDetail" GuidDataProperty="ContactDetailID"
							RedirectPage="ActionPage.aspx" FieldName="tblCompany_CompanyID" DisplayNoResultMessageOnly="True" NoResultMessage="(No Action records for this Organisation)"></bnbdatagrid:bnbdatagridforview></P>
				</asp:panel><asp:panel id="EventsTab" runat="server" CssClass="tcTab" BnbTabName="Events">
					<P>
						<bnbdatagrid:bnbdatagridforview id="uxOrgEventsGrid" runat="server" CssClass="datagrid" Width="100%" CriteriaParameter="tblCompany_CompanyID"
							GuidKey="tblAttendance_AttendanceID" NewLinkText="New Event" NewLinkVisible="False" ViewLinksVisible="True"
							ViewName="vwBonoboCompanyAttendance" QueryStringKey="BnbCompany" DomainObject="BnbAttendance" GuidDataProperty="ContactDetailID"
							RedirectPage="AttendancePage.aspx" FieldName="tblCompany_CompanyID" DisplayNoResultMessageOnly="True" NoResultMessage="(No Event Attendance records for this Organisation)"></bnbdatagrid:bnbdatagridforview></P>
				</asp:panel><asp:panel id="ResourcesTab" runat="server" CssClass="tcTab" BnbTabName="Resources">
					<P>
						<bnbdatagrid:BnbDataGridForView id="BnbDtGVResources" runat="server" CssClass="datagrid" Width="100%" ColumnNames="tblIndividualKeyInfo_RefNo,Custom_DisplayName,lkuResourceType_Description,lkuResource_Description,lnkIndividualResource_FromDate,lnkIndividualResource_ToDate,lkuResourceRecommendation_Description,lkuResourceReason_Description,appUser_FullName,lkuResourceLevelOfCommitment_Description,lkuResourceLocality_Description"
							GuidKey="lnkIndividualResource_IndividualResourceID" NewLinkText="New" NewLinkVisible="False" ViewLinksVisible="True"
							ViewName="vwBonoboOrganisationResources" QueryStringKey="BnbCompany" DomainObject="BnbIndividualResource" RedirectPage="ResourcePage.aspx"
							FieldName="tblCompany_CompanyID" DisplayNoResultMessageOnly="True" NoResultMessage="(No Resource records for this Organisation)"
							ViewLinksText="View" ShowResultsOnNewButtonClick="false" DisplayResultCount="false"></bnbdatagrid:BnbDataGridForView></P>
				</asp:panel><asp:panel id="SponsorshipTab" runat="server" CssClass="tcTab" BnbTabName="Sponsorship">
					<P>
						<bnbdatagrid:bnbdatagridforview id="BnbDataGridForView4" runat="server" CssClass="datagrid" Width="100%" ColumnNames="lkuFundingType_Description, Custom_VolName, tblIndividualKeyInfo_old_indv_id, Custom_FullPositionReference, lkuFundingStatus_Description, tblFunding_FundingValue, lkuFinancialYear_Description"
							CriteriaParameter="tblCompany_CompanyID" GuidKey="tblFunding_FundingID" NewLinkText="" NewLinkVisible="False"
							ViewLinksVisible="True" ViewName="vwBonoboOrganisationSponsorship" QueryStringKey="BnbCompany" DomainObject="BnbFunding"
							GuidDataProperty="CompanyID" RedirectPage="FundingPage.aspx" FieldName="tblFunding_CompanyID" DisplayNoResultMessageOnly="True"
							NoResultMessage="(No Sponsorship records for this Organisation)"></bnbdatagrid:bnbdatagridforview></P>
				</asp:panel><asp:panel id="SupplierTab" runat="server" CssClass="tcTab" BnbTabName="Supplier">
					<P>
						<TABLE>
						</TABLE>
					</P>
					<bnbdatagrid:BnbDataGridForView id="BnbDataGridForView6" runat="server" CssClass="datagrid" Width="100%" ColumnNames="lkuSupplierGroupType_Description, lkuSupplierRecommended_Description"
						GuidKey="lnkCompanySupplier_CompanySupplierID" NewLinkText="New Supplier" NewLinkVisible="true" ViewLinksVisible="true"
						ViewName="vwBonoboOrganisationSupplier" QueryStringKey="BnbCompany" DomainObject="BnbCompanySupplier" RedirectPage="SupplierPage.aspx"
						FieldName="lnkCompanySupplier_CompanyID" DisplayNoResultMessageOnly="True" NoResultMessage="(No Supplier records for this Organisation)"
						ViewLinksText="View" ShowResultsOnNewButtonClick="false" DisplayResultCount="false" DeleteLinksVisible="False"></bnbdatagrid:BnbDataGridForView>
				</asp:panel><asp:panel id="ProjectsTab" runat="server" CssClass="tcTab" BnbTabName="Projects">
                <br />
                <bnbdatagrid:BnbDataGridForView ID="Bnbdatagridforview5" runat="server" ColumnsToExclude="tblProject_Country"
                    CssClass="datagrid" DomainObject="BnbProject" FieldName="tblCompany_CompanyID"
                    GuidDataProperty="ProjectID" GuidKey="tblProject_ProjectID" NewLinkText="New Project"
                    NewLinkVisible="False" QueryStringKey="BnbCompany" RedirectPage="ProjectPage.aspx"
                    ViewLinksText="View" ViewLinksVisible="true" ViewName="vwBonoboCompanyProjectsOnlyActive"
                    Width="100%" />
                <br />
                </asp:panel>
            </div>
		</form>
	</body>
</HTML>
