<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Page Language="c#" Codebehind="ProgrammeComments.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.EditPages.ProgrammeCommentsPage" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>ProgrammePage</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet" />
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
     <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/bnbprogramme24silver.gif">
            </uc1:Title>
      
            <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons1" runat="server" UndoText="Undo"
                SaveText="Save" EditAllText="Edit All" NewText="New" HeightAllButtons="25px"
                UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" CssClass="databuttons">
            </bnbpagecontrol:BnbDataButtons>
            <bnbpagecontrol:BnbMessageBox ID="BnbMessageBox1" runat="server" CssClass="messagebox">
            </bnbpagecontrol:BnbMessageBox>
     
      
            <table id="Table1" width="100%" border="0" class="fieldtable">
                <tr>
                    <td class="label" width="50%" style="height: 27px">
                        Programme Ref</td>
                    <td width="50%" style="height: 27px">
                        <bnbdatacontrol:BnbTextBox ID="BnbTextBox2" runat="server" CssClass="textbox" Height="22px"
                            Width="200px" DataMember="BnbProgramme.ProgrammeRef" Rows="1" MultiLine="false"
                            ErrorMessage="Mandatory data is missing"></bnbdatacontrol:BnbTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Comment Date</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbDateBox ID="BnbDateBox1" runat="server" CssClass="datebox" Width="90px"
                            Height="20px" DateCulture="en-GB" DateFormat="dd/mm/yyyy" DataMember="BnbProgrammeComments.CommentDate">
                        </bnbdatacontrol:BnbDateBox>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Comment</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbTextBox ID="BnbTextBox1" runat="server" CssClass="textbox" Width="250px"
                            Height="20px" DataMember="BnbProgrammeComments.Comment" MultiLine="True" Rows="3"
                            StringLength="0"></bnbdatacontrol:BnbTextBox>
                    </td>
                </tr>
            </table>
      
      
            </div>
    </form>
   
</body>
</html>
