//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3053
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Frond.EditPages {
    
    public partial class WizardICT {
        protected System.Web.UI.WebControls.Literal literalBeforeCss;
        protected System.Web.UI.WebControls.Literal literalAfterCss;
        protected System.Web.UI.HtmlControls.HtmlForm Form1;
        protected Frond.UserControls.MenuBar menuBar;
        protected System.Web.UI.WebControls.Panel panelZero;
        protected System.Web.UI.WebControls.Label labelVolunteerDetails;
        protected System.Web.UI.WebControls.Label labelCurrentPlacement;
        protected System.Web.UI.WebControls.Label labelPanelZeroContinue;
        protected System.Web.UI.WebControls.Label labelPanelZeroFeedback;
        protected System.Web.UI.WebControls.Panel panelOne;
        protected System.Web.UI.WebControls.Label labelVolunteerDetails2;
        protected System.Web.UI.WebControls.Label labelCurrentPlacement2;
        protected System.Web.UI.WebControls.Label labelCountry;
        protected System.Web.UI.WebControls.TextBox textESDStart;
        protected System.Web.UI.WebControls.TextBox textESDEnd;
        protected System.Web.UI.WebControls.Button buttonRefreshPlacements;
        protected System.Web.UI.WebControls.Label labelFindFeedback;
        protected System.Web.UI.WebControls.DataGrid dataGridPlacements;
        protected System.Web.UI.WebControls.Label labelFindNotes;
        protected System.Web.UI.WebControls.TextBox txtPlacementRef;
        protected System.Web.UI.WebControls.Label labelPanelOneFeedback;
        protected System.Web.UI.WebControls.Panel panelTwo;
        protected System.Web.UI.WebControls.Label labelVolunteerDetails3;
        protected System.Web.UI.WebControls.Label labelCurrentPlacement3;
        protected System.Web.UI.WebControls.Label labelNewPlacementDetails;
        protected System.Web.UI.WebControls.TextBox textTransferDate;
        protected System.Web.UI.WebControls.Label labelPanelTwoFeedback;
        protected System.Web.UI.WebControls.CheckBox CheckBoxPanelTwo;
        protected System.Web.UI.WebControls.Label labelPanelTwoCheckBox;
        protected System.Web.UI.WebControls.Label labelHiddenCountryID;
        protected System.Web.UI.WebControls.Label labelHiddenSelectedPositionID;
    }
}
