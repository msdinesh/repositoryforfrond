using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using BonoboDomainObjects;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboWebControls;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for WizardNewWorkAddress.
	/// </summary>
	public partial class WizardNewWorkAddress : System.Web.UI.Page
	{
		protected UserControls.WizardButtons wizardButtons;
		protected UserControls.OrganisationAddressPanel uxOrganisationAddressUserControl;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
#if DEBUG
			if (!this.IsPostBack && this.Request.QueryString.ToString() == "")
				Response.Redirect("WizardNewWorkAddress.aspx?BnbIndividual=CE1934E3-1BFF-4476-8B13-A94FD81C4627&AddressTypeID=1034");
#endif

			BnbEngine.SessionManager.ClearObjectCache();
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);

			if (this.Request.UrlReferrer != null)
				uxHiddenReferrer.Text = this.Request.UrlReferrer.ToString();
			else
				uxHiddenReferrer.Text = "~/Menu.aspx";

			if (!this.IsPostBack)
			{
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				this.IndividualID = new Guid(this.Request.QueryString["BnbIndividual"]);
				this.AddressTypeID = int.Parse(this.Request.QueryString["AddressTypeID"]);
				BnbWebFormManager.LogAction("New Work Address Wizard", null, this);
			}
			wizardButtons.AddPanel(panelGetName);
			wizardButtons.AddPanel(panelSearchResults);
			if (uxOrgResultsGrid.SelectedDataRowID != Guid.Empty )
				wizardButtons.AddPanel(panelChooseOrgAddress);
			else
				panelChooseOrgAddress.Visible = false;
			if (uxAddOrgAddressCheckbox.Checked || uxOrgAddressesGrid.SelectedDataRowID  == Guid.Empty)
				wizardButtons.AddPanel(panelEnterAddress);
			else
				panelEnterAddress.Visible = false;
			wizardButtons.AddPanel(panelLinkDetails);

            //For capturing Geographical locations
            doLoadGeo();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			wizardButtons.CancelWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
			wizardButtons.FinishWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
			wizardButtons.ShowPanel +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
			wizardButtons.ValidatePanel +=new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		public Guid IndividualID
		{
			get
			{
				if (uxHiddenIndividualID.Text == "")
					return Guid.Empty;
				else
					return new Guid(uxHiddenIndividualID.Text);
			}
			set
			{
				uxHiddenIndividualID.Text = value.ToString();
			}
		}

		
		public Guid CompanyID
		{
			get
			{
				if (uxHiddenCompanyID.Text == "")
					return Guid.Empty;
				else
					return new Guid(uxHiddenCompanyID.Text);
			}
			set
			{
				uxHiddenCompanyID.Text = value.ToString();
			}
		}

		
		public Guid CompanyAddressID
		{
			get
			{
				if (uxHiddenCompanyAddressID.Text == "")
					return Guid.Empty;
				else
					return new Guid(uxHiddenCompanyAddressID.Text);
			}
			set
			{
				uxHiddenCompanyAddressID.Text = value.ToString();
			}
		}

		public int AddressTypeID
		{
			get
			{
				if (uxHiddenAddressTypeID.Text == "")
					return -1;
				else
					return int.Parse(uxHiddenAddressTypeID.Text);
			}
			set
			{
				uxHiddenAddressTypeID.Text = value.ToString();
			}
		}

		private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			Response.Redirect(uxHiddenReferrer.Text);
		}

		private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			this.CreateAddress();	
		}

		private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			if (e.CurrentPanelHtmlID == panelGetName.ID)
				this.SetupGetNamePanel();
			if (e.CurrentPanelHtmlID == panelSearchResults.ID)
				this.SetupSearchResultsPanel();
			if (e.CurrentPanelHtmlID == panelChooseOrgAddress.ID)
				this.SetupChooseOrgAddressPanel();
			if (e.CurrentPanelHtmlID == panelEnterAddress.ID)
				this.SetupEnterAddressPanel();
			if (e.CurrentPanelHtmlID == panelLinkDetails.ID)
				this.SetupLinkDetailsPanel();
		}

		private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
		{
			if (e.CurrentPanelHtmlID == panelGetName.ID)
			{
				if (uxOrgNameTextbox.Text == "")
				{
					uxPanelGetNameFeedback.Text = "Please enter an Organisation Name";
					e.Proceed = false;
				}
			}
			if (e.CurrentPanelHtmlID == panelSearchResults.ID)
			{
				if (uxOrgResultsGrid.SelectedDataRowID == Guid.Empty)
				{
					this.CompanyID = Guid.Empty;
					this.CompanyAddressID = Guid.Empty;
				}
				else
				{
					this.CompanyAddressID = uxOrgResultsGrid.SelectedDataRowID;
					BnbCompanyAddress tempCA = BnbCompanyAddress.Retrieve(this.CompanyAddressID);
					this.CompanyID = tempCA.CompanyID;
				}
			}	
			if (e.CurrentPanelHtmlID == panelChooseOrgAddress.ID)
			{
				if (uxAddOrgAddressCheckbox.Checked)
					this.CompanyAddressID = Guid.Empty;
				else
					this.CompanyAddressID = uxOrgAddressesGrid.SelectedDataRowID;
				if (this.CompanyAddressID == Guid.Empty && uxAddOrgAddressCheckbox.Checked == false)
				{
					uxPanelChooseOrgAddressFeedback.Text = "Please choose an address or check the new address checkbox";
					e.Proceed = false;
				}

			}
		}

		private void SetupGetNamePanel()
		{
			BnbIndividual indv = BnbIndividual.Retrieve(this.IndividualID);
			uxIndividualSummaryLabel1.Text = indv.InstanceDescription;
			uxAddressTypeLabel1.Text = BnbEngine.LookupManager.GetLookupItemDescription("vlkuBonoboAddressTypeWithGroup",this.AddressTypeID);
			uxOrgCountryDropDown.SelectedValue = BnbEngine.SessionManager.GetSessionInfo().DefaultCountryID;
		}

		private void SetupSearchResultsPanel()
		{
			uxOrgResultsGrid.SelectedDataRowID = Guid.Empty;
			uxOrgAddressesGrid.SelectedDataRowID = Guid.Empty;
			this.CompanyID = Guid.Empty;
			this.CompanyAddressID = Guid.Empty;
			BnbCriteria dupCriteria = new BnbCriteria();

			if (uxOrgNameTextbox.Text != "")
			{
				string companyNameBit = uxOrgNameTextbox.Text;
				if (companyNameBit.Length > 8)
					companyNameBit = companyNameBit.Substring(0,8);

				BnbTextCondition nameMatch = new BnbTextCondition("tblCompany_CompanyName", companyNameBit, BnbTextCompareOptions.AnyPartOfField);
				dupCriteria.QueryElements.Add(nameMatch);
			}
			
			if (uxOrgCountryDropDown.SelectedValue.ToString() != "")
			{
				Guid countryID = new Guid(uxOrgCountryDropDown.SelectedValue.ToString());
				BnbListCondition countryMatch = new BnbListCondition("tblAddress_CountryID", countryID);
				dupCriteria.QueryElements.Add(countryMatch);
			}
			uxOrgResultsGrid.Criteria = dupCriteria;
			uxOrgResultsGrid.MaxRows = 20;
			uxOrgResultsGrid.Visible = true;

			if (uxOrgResultsGrid.Results.Rows.Count == 0)
			{
				uxOrgResultsPanel.Visible = false;
				uxNoSimilarLabel.Visible = true;
			}
		}

		private void SetupChooseOrgAddressPanel()
		{
			uxOrgAddressesGrid.SelectedDataRowID = Guid.Empty;
			BnbIndividual indv = BnbIndividual.Retrieve(this.IndividualID);
			uxIndividualSummaryLabel2.Text = indv.InstanceDescription;
			uxAddressTypeLabel2.Text = BnbEngine.LookupManager.GetLookupItemDescription("vlkuBonoboAddressTypeWithGroup",this.AddressTypeID);
			BnbCompany comp = BnbCompany.Retrieve(this.CompanyID);
			uxOrgNameLabel2.Text = comp.CompanyName;
			uxOrgRefLabel2.Text = comp.CompanyReference;

			BnbCriteria addressCrit = new BnbCriteria(new BnbListCondition("lnkCompanyAddress_CompanyID", this.CompanyID));
			uxOrgAddressesGrid.Criteria = addressCrit;
			uxOrgAddressesGrid.Visible = true;
			uxOrgAddressesGrid.SelectedDataRowID = this.CompanyAddressID;
		}

		private void SetupEnterAddressPanel()
		{
			// do we have a chosen org?
			if (this.CompanyID != Guid.Empty)
			{
				BnbCompany comp = BnbCompany.Retrieve(this.CompanyID);
				uxOrgNameTextbox3.Visible = false;
				uxOrgNameLabel3.Text = comp.CompanyName;
				uxOrgRefLabel3.Text = comp.CompanyReference;
			}
			else
			{
				uxOrgNameTextbox3.Visible = true;
				uxOrgNameTextbox3.Text = uxOrgNameTextbox.Text;
				uxOrgRefLabel3.Visible = false;
				uxOrgRefLabel3.Text = "(New)";
			}
			if (uxOrgCountryDropDown.SelectedValue != null)
				uxOrganisationAddressUserControl.CountryComboSelectedValue = uxOrgCountryDropDown.SelectedValue.ToString();
            
		}

		private void SetupLinkDetailsPanel()
		{
			uxIgnoreWarningsCheckbox.Checked = false;
			BnbIndividual indv = BnbIndividual.Retrieve(this.IndividualID);
			uxIndividualSummaryLabel4.Text = indv.InstanceDescription;
			uxAddressTypeLabel4.Text = BnbEngine.LookupManager.GetLookupItemDescription("vlkuBonoboAddressTypeWithGroup",this.AddressTypeID);
			// do we have a chosen org?
			if (this.CompanyID != Guid.Empty)
			{
				BnbCompany comp = BnbCompany.Retrieve(this.CompanyID);
				uxOrgNameLabel4.Text = comp.CompanyName;
				uxOrgRefLabel4.Text = comp.CompanyReference;
			}
			else
			{
				uxOrgNameLabel4.Text = uxOrgNameTextbox3.Text;
				uxOrgRefLabel4.Text = "(New)";
			}
			// do we have an address?
			uxOrgFullAddressLabel.Text = "";
			if (this.CompanyAddressID != Guid.Empty)
			{
				BnbCompanyAddress ca = BnbCompanyAddress.Retrieve(this.CompanyAddressID);
				string[] addressLines = ca.Address.GetAddressLines();
				foreach(string line in addressLines)
					uxOrgFullAddressLabel.Text += line + "<br/>";
			}
			else
			{
				BnbCompany compDummy = new BnbCompany(true);
				BnbEditManager emDummy = new BnbEditManager();
				// get data from usercontrol
				// (but just to display - were not saving it yet)
				uxOrganisationAddressUserControl.SaveOrganisationAddressDetails(compDummy, null, emDummy);
				string[] addressLines = compDummy.CurrentCompanyAddress.Address.GetAddressLines();
				foreach(string line in addressLines)
					uxOrgFullAddressLabel.Text += line + "<br/>";
				emDummy.EditCompleted();
			}
			uxWorkAddressPanel.Visible = BnbIndividualAddress.AddressTypeIsWorkAddress(this.AddressTypeID);
			uxBankAddressPanel.Visible = BnbIndividualAddress.AddressTypeIsBankAddress(this.AddressTypeID);
						

		}

		private void CreateAddress()
		{

			uxPanelLinkDetailsFeedback.Text = "";
			uxIgnoreWarningsCheckbox.Visible = false;
			// create individual-address link
			BnbEditManager em = new BnbEditManager();
			BnbIndividualAddress iaLink = new BnbIndividualAddress(true);
			iaLink.RegisterForEdit(em);
			BnbIndividual indv = BnbIndividual.Retrieve(this.IndividualID);
			indv.IndividualAddresses.Add(iaLink);
			// fill in the link
			iaLink.AddressTypeID = this.AddressTypeID;
			if (iaLink.IsWorkAddress)
			{
				if (uxWorkRoleTextbox.Text != "")
					iaLink.CompanyRole = uxWorkRoleTextbox.Text;
				if (uxWorkDepartmentTextbox.Text != "")
					iaLink.CompanyDepartment = uxWorkDepartmentTextbox.Text;
			}
			if (iaLink.IsBankAddress)
			{
				if (uxAccountNameTextbox.Text != "")
					iaLink.AccountName = uxAccountNameTextbox.Text;
				if (uxAccountNumberTextbox.Text != "")
					iaLink.AccountNumber = uxAccountNumberTextbox.Text;
				if (uxSortCodeTextbox.Text != "")
					iaLink.SortCode = uxSortCodeTextbox.Text;
			}
			// do the company/address bit
			BnbCompany comp = null;
			BnbCompanyAddress compAddress = null;
			BnbAddress address = null;
			// was a company chosen?
			if (this.CompanyID != Guid.Empty)
				comp = BnbCompany.Retrieve(this.CompanyID);
			else
			{
				comp = new BnbCompany(true);
				comp.RegisterForEdit(em);
				comp.CompanyName = uxOrgNameTextbox3.Text;
				comp.GCContactUserID = BnbEngine.SessionManager.GetSessionInfo().UserID;
			}
			// was an address chosen?
			if (this.CompanyAddressID != Guid.Empty)
			{
				compAddress = comp.CompanyAddresses[this.CompanyAddressID] as BnbCompanyAddress;
				address = compAddress.Address;
			}
			else
			{
				// get it from the address user control
				address = uxOrganisationAddressUserControl.SaveOrganisationAddressDetails(comp, indv, em);
				
			}
			address.IndividualAddresses.Add(iaLink);

			// now do the save (with warnings check)
			BnbWarningCollection warns = em.GetEditWarnings();

			if(warns.Count == 0 || uxIgnoreWarningsCheckbox.Checked == true)
			{
				try
				{
					em.SaveChanges();
					Response.Redirect("./IndividualPage.aspx?BnbIndividual=" + this.IndividualID.ToString());
				}
				catch(BnbProblemException pe)
				{
					StringBuilder sb = new StringBuilder();
					sb.Append("Unfortunately, the record could not be created due to the following problems: <br/>");
					foreach(BnbProblem p in pe.Problems)
						sb.Append(p.Message + "<br/>");
					uxPanelLinkDetailsFeedback.Text = sb.ToString();

				}
			}
			else
			{
				StringBuilder sb = new StringBuilder();
				sb.Append("Please check the following warnings:<br/>");
				foreach(BnbWarning warnLoop in warns)
				{
					sb.Append(warnLoop.Message);
					sb.Append("<br/>");
				}
				uxIgnoreWarningsCheckbox.Visible = true;
				uxPanelLinkDetailsFeedback.Text = sb.ToString();
			}
		}

        //For capturing Geographical locations
        private void doLoadGeo()
        {
            if (HasPermissionToLocateOnMap())
            {
                StringBuilder script = new StringBuilder();
                script.Append("pageName ='WizardNewWorkAddress';");
                //Page mode
                script.Append("mode='new/edit';");
                if (!(uxOrganisationAddressUserControl.Latitude.Trim() == string.Empty))
                {
                    ViewState["Latittude"] = uxOrganisationAddressUserControl.Latitude;
                    ViewState["Longitude"] = uxOrganisationAddressUserControl.Longitude;
                }

                if (!(ViewState["Latittude"] == null))
                {
                    string latitude = ViewState["Latittude"].ToString();
                    string longitude = ViewState["Longitude"].ToString();
                    script.Append("latVal = " + latitude + ";lngVal = " + longitude + ";");
                }
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "WNWALoadGeo", script.ToString(), true);
            }
        }

        /// <summary>
        /// Check permisssion to access the Google map functionality
        /// </summary>
        private bool HasPermissionToLocateOnMap()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_LocateAddressOnMap))
                return true;
            else
                return false;
        }

	}
}
