using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using BonoboWebControls;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for WizardTempReturn.
    /// </summary>
    public partial class WizardTempReturn : System.Web.UI.Page
    {
        protected UserControls.WizardButtons wizardButtons;

        protected void Page_Load(object sender, System.EventArgs e)
        {
#if DEBUG
            // only for testing
            if (Request.QueryString.ToString() == "")
            {
                string url = Request.Url.ToString() + "?BnbApplication=1e79f141-518c-45fe-89f0-eaf857690e6a&BnbService=92b32529-1d18-45ac-91ce-1c2b6e7201e1";
                Response.Redirect(url);
            }
#endif
            // Put user code to initialize the page here
            wizardButtons.AddPanel(panelZero);
            wizardButtons.AddPanel(panelOne);
            wizardButtons.AddPanel(panelTwo);
        }
        public Guid ApplicationID
        {
            get
            {
#if DEBUG
                if (this.Request.QueryString["BnbApplication"] != null)
                    return new Guid(this.Request.QueryString["BnbApplication"]);
                else
                    //	return new System.Guid("46ec43dc-108b-4835-90f7-0bc209c9cc00");
                    return new System.Guid("0feea227-3f81-4dc9-9788-53c7d3201286");


#else
				if (this.Request.QueryString["BnbApplication"] != null)
					return new Guid(this.Request.QueryString["BnbApplication"]);
				else
					return Guid.Empty;
#endif
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
            wizardButtons.CancelWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
            wizardButtons.FinishWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
            wizardButtons.ShowPanel += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
            wizardButtons.ValidatePanel += new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);


        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
        private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            if (e.CurrentPanel == 0)
            {
                if (labelVolunteerDetails.Text == "")
                    this.SetupPanelZero();
            }
            if (e.CurrentPanel == 1)
            {
                labelPanelOneFeedback.Text = "";
                this.SetupPanelOne();
            }
            if (e.CurrentPanel == 2)
            {
                if (labelPanelOneFeedback.Text == "")
                {
                    labelPanelTwoFeedback.Text = "";
                    this.SetupPanelTwo();
                }
                else
                {
                    e.CurrentPanel = 1;
                    this.SetupPanelOne();
                }
            }
        }

        private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            Response.Redirect("VolunteerService.aspx?BnbApplication=" + this.ApplicationID);
        }

        private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            if (panelDetailsValid())
                if (saveTempReturnStart())
                    if (saveTempReturnEnd())
                        Response.Redirect("VolunteerService.aspx?BnbApplication=" + this.ApplicationID);
        }

        private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
        {
            if (e.CurrentPanel == 0)
            {
                e.Proceed = labelPanelZeroContinue.Visible;
            }
            if (e.CurrentPanel == 1)
            {
                labelPanelOneFeedback.Text = "";
                if (dateboxStartDate.Date == DateTime.MinValue) labelPanelOneFeedback.Text += "Start Date must be filled";
                if (dateboxEndDate.Date == DateTime.MinValue) labelPanelOneFeedback.Text += "\nEnd Date must be filled";
                if (!dateboxStartDate.IsValid) labelPanelOneFeedback.Text += "\nStart Date is invalid";
                if (!dateboxEndDate.IsValid) labelPanelOneFeedback.Text += "\nEnd Date is invalid";
                if (labelPanelOneFeedback.Text != "")
                {
                    e.Proceed = false;
                    return;
                }

                this.selectedReturnReason = this.dropDownServiceReason.SelectedItem.Text;
                this.selectedReturnReasonID = this.dropDownServiceReason.SelectedItem.Value;
                this.selectedReturnReasonID = this.dropDownServiceReason.SelectedValue.ToString();
                this.selectedStartDate = this.dateboxStartDate.Date.ToString("dd/MMM/yyyy");
                this.selectedEndDate = this.dateboxEndDate.Date.ToString("dd/MMM/yyyy");
            }
        }
        private void SetupPanelZero()
        {
            //get volunteer application details
            BnbApplication volApp = BnbApplication.Retrieve(this.ApplicationID);
            string indvDesc = volApp.Individual.InstanceDescription;
            string statusDesc = BnbEngine.LookupManager.GetLookupItemDescription("vlkuBonoboStatusFull",
                volApp.CurrentStatus.StatusID);
            string appDesc = String.Format("{0}/{1}",
                volApp.ApplicationOrder, volApp.Individual.Applications.Count);
            string volDetails = HttpUtility.HtmlEncode(indvDesc + " " + statusDesc + " " + appDesc);
            labelVolunteerDetails.Text = volDetails;
            labelVolunteerDetails2.Text = volDetails;
            labelVolunteerDetails3.Text = volDetails;

            // get volunteer placement details
            bool placementFound = false;
            string placeDesc = null;
            if (volApp.PlacedService != null)
            {
                BnbPosition placedPos = volApp.PlacedService.Position;
                placeDesc = HttpUtility.HtmlEncode(placedPos.FullPlacementReference + " " + placedPos.Role.RoleName + " (" + placedPos.Role.Employer.EmployerName + ")");
                //		this.labelHiddenSelectedReturnReasonID.Text = HttpUtility.HtmlEncode(volApp.CurrentStatus.StatusReasonID.ToString());
                placementFound = true;
            }
            else
            {
                placeDesc = "(No Placement)";
            }
            labelCurrentPlacement.Text = placeDesc;
            labelCurrentPlacement2.Text = placeDesc;
            labelCurrentPlacement3.Text = placeDesc;

            // check status
            if (volApp.CurrentStatus.StatusGroupID == BnbConst.StatusGroup_Overseas)
            {
                if (placementFound)
                {
                    labelPanelZeroContinue.Visible = true;
                }
                else
                {
                    labelPanelZeroFeedback.Text = "Bad data - Volunteer not properly placed to a Placement.";
                }

            }
            else
                labelPanelZeroFeedback.Text = "The Volunteer must have overseas status for a Temp Return. Please press cancel.";
        }

        private void SetupPanelOne()
        {
            populateReasonDropDown();

            // start date for return
            DateTime startDate;
            startDate = DateTime.Now;
            //dateboxStartDate.Text = startDate.ToString("dd/MMM/yyyy");
            dateboxStartDate.Date = startDate;
            dropDownDuration.Attributes.Add("onchange", "durationToDate();");
            //dateboxStartDate.Attributes.Add("onchange","durationToDate();");

            //			// end date for return
            //			if (dateboxEndDate.Text == "")
            //				dateboxEndDate.Text = "dd/MMM/yyyy";

            populateEndDateDropDown();
        }

        private void SetupPanelTwo()
        {
            labelReturnReason.Text = this.selectedReturnReason;
            labelStartDate.Text = this.selectedStartDate;
            labelEndDate.Text = this.selectedEndDate;
        }

        private void populateReasonDropDown()
        {
            BnbLookupDataTable serviceReasonLookup = BnbEngine.LookupManager.GetLookup("lkuServiceReason");
            DataView serviceReasonView = new DataView(serviceReasonLookup);
            //serviceReasonView.RowFilter = "Exclude = 0";
            serviceReasonView.RowFilter = "ServiceStatusID = 1005 AND Exclude = 0"; //1005 status is temp return start
            serviceReasonView.Sort = "Description";
            dropDownServiceReason.DataSource = serviceReasonView;
            dropDownServiceReason.DataTextField = "Description";
            dropDownServiceReason.DataValueField = "IntID";
            dropDownServiceReason.DataBind();
            ListItem nullItem = new ListItem("", "-1");
            nullItem.Selected = true;
            dropDownServiceReason.Items.Add(nullItem);
        }

        private void populateEndDateDropDown()
        {
            BnbLookupDataTable serviceDurationLookup = BnbEngine.LookupManager.GetLookup("lkuTempReturnDuration");
            DataView serviceDurationView = new DataView(serviceDurationLookup);
            serviceDurationView.RowFilter = "Exclude = 0";
            serviceDurationView.Sort = "DisplayOrder";
            dropDownDuration.DataSource = serviceDurationView;
            dropDownDuration.DataTextField = "Description";
            dropDownDuration.DataValueField = "DurationInDays";
            dropDownDuration.DataBind();
            ListItem nullItemSD = new ListItem("", "-1");
            nullItemSD.Selected = true;
            dropDownDuration.Items.Add(nullItemSD);
            dropDownDuration.Attributes.Add("onchange", "durationToDate(this);");
        }
        public string selectedReturnReason
        {
            get { return labelHiddenSelectedReturnReason.Text; }
            set { labelHiddenSelectedReturnReason.Text = value; }
        }
        public string selectedReturnReasonID
        {
            get { return labelHiddenSelectedReturnReasonID.Text; }
            set { labelHiddenSelectedReturnReasonID.Text = value; }
        }
        public string selectedStartDate
        {
            get { return labelHiddenSelectedStartDate.Text; }
            set { labelHiddenSelectedStartDate.Text = value; }
        }
        public string selectedEndDate
        {
            get { return labelHiddenSelectedEndDate.Text; }
            set { labelHiddenSelectedEndDate.Text = value; }
        }

        // Check that entered details are valid
        public bool panelDetailsValid()
        {
            bool panelValid = true;

            labelPanelTwoFeedback.Text = "";
            // check for reason
            //			if (selectedReturnReasonID == "-1")
            //			{
            //				labelPanelTwoFeedback.Text = "Return Reason is mandatory. ";
            //			}

            // check dates
            DateTime finalStartDate = DateTime.Now;
            try
            {
                finalStartDate = DateTime.Parse(selectedStartDate);
            }
            catch (FormatException)
            {
                labelPanelTwoFeedback.Text += "Start Date is not in the correct format. ";
                panelValid = false;
            }

            DateTime finalEndDate = DateTime.Now;
            try
            {
                finalEndDate = DateTime.Parse(selectedEndDate);
            }
            catch (FormatException)
            {
                labelPanelTwoFeedback.Text += "End Date is not in the correct format. ";
                panelValid = false;
            }

            if (finalStartDate > finalEndDate)
            {
                labelPanelTwoFeedback.Text += "End Date must be later than Start Date. ";
                panelValid = false;
            }
            return panelValid;
        }





        public bool saveTempReturnStart()
        {
            bool success = true;
            BnbEditManager em = new BnbEditManager();
            BnbApplication app = BnbApplication.Retrieve(this.ApplicationID);
            try
            {
                app.RegisterForEdit(em);
            }
            catch (BnbLockException le)
            {
                labelPanelTwoFeedback.Text = le.Message;
                success = false;
            }
            if (success)
            {
                int ReasonID = int.Parse(this.selectedReturnReasonID);
                DateTime startDate = DateTime.Parse(this.selectedStartDate);
                DateTime endDate = DateTime.Parse(this.selectedEndDate);
                app.PlacedService.RegisterForEdit(em);

                //save it
                try
                {
                    app.PlacedService.AddServiceProgress(BnbConst.ServiceStatus_TempReturnStart, ReasonID, startDate);
                    if ((startDate <= DateTime.Today) && (endDate > DateTime.Today))
                        app.CurrentStatus.StatusID = BnbConst.ServiceStatus_TempReturnStart;

                    //this works, but what if the temp return is in the future.
                    em.SaveChanges();

                }

                catch (BnbProblemException pe)
                {
                    labelPanelTwoFeedback.Text =
                        "Temp Return Wizard was not able to save service progress due to the following problems:<br/>";
                    foreach (BnbProblem p in pe.Problems)
                        labelPanelTwoFeedback.Text += "-" + p.Message + "<br/>";
                    labelPanelTwoFeedback.Text += "Please press Cancel and go to the previous screen to correct these problems. ";
                    success = false;
                    em.EditCompleted();
                }

            }
            return success;
        }
        public bool saveTempReturnEnd()
        {
            bool success = true;
            BnbEditManager em = new BnbEditManager();
            BnbApplication app = BnbApplication.Retrieve(this.ApplicationID);
            try
            {
                app.RegisterForEdit(em);
            }
            catch (BnbLockException le)
            {
                labelPanelTwoFeedback.Text = le.Message;
                success = false;
            }
            if (success)
            {
                int ReasonID = int.Parse(this.selectedReturnReasonID);
                //DateTime startDate = DateTime.Parse(this.selectedStartDate);
                DateTime endDate = DateTime.Parse(this.selectedEndDate);
                app.PlacedService.RegisterForEdit(em);

                //save it
                try
                {
                    app.PlacedService.AddServiceProgress(BnbConst.ServiceStatus_TempReturnEnd, ReasonID, endDate);
                    em.SaveChanges();
                }
                catch (BnbProblemException pe)
                {
                    if (labelPanelTwoFeedback.Text == "")
                        labelPanelTwoFeedback.Text = "Temp Return Wizard was not able to save service progress due to the following problems:<br/>";
                    foreach (BnbProblem p in pe.Problems)
                        labelPanelTwoFeedback.Text += "-" + p.Message + "<br/>";
                    labelPanelTwoFeedback.Text += "Please press Cancel and go to the previous screen to correct these problems. ";
                    success = false;
                    em.EditCompleted();
                }
            }
            return success;
        }
    }
}
