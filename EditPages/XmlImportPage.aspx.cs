using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Collections.Specialized;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using BonoboDomainObjects;
using BonoboWebControls;
using BonoboEngine;
using BonoboEngine.Query;
using Frond.Utilities;
using System.Text;

namespace Frond.EditPages
{
    public partial class XmlImportPage : System.Web.UI.Page
    {

        #region " Private declarations "

        private BnbProblemCollection problemCollection;
        private BnbProblem problem = null;
        private BnbEditManager em = null;
        private BnbPositionVolunteerRole volunteerRole = null;
        private BnbRole role = null;
        private BnbEmployer employer = null;
        private BnbRoleSkill roleSkill = null;
        private BnbPositionObjective objective = null;
        private BnbPosition position = null;
        private BnbPositionTheme theme = null;
        private PlacementParser parser = null;
        private bool hasAuditEntry = false;
        private BnbXmlImportAuditHeader auditHeader = null;
        private BnbXmlImportAuditItem auditItem = null;
        XmlNodeList placementRows = null;

        Guid auditHeaderRef = Guid.Empty;
        int recordCount = 0;
        int successCount = 0;
        int failureCount = 0;
        int rowCount = 0;
        bool recordSucceeded = false;
        Guid rowReferenceID = Guid.Empty;
        StringBuilder importDetails = null;
        int mappingID = 0;
        string validationMessage = string.Empty;

        private const string successMessage = "Row imported sucessfully";
        private const string failureMessage = "Failed to import the row";

        #endregion

        #region " Page_Load "

        protected void Page_Load(object sender, EventArgs e)
        {
            Initialize();
        }

        #endregion

        #region " Import_Click "

        protected void Import_Click(object sender, EventArgs e)
        {

            if (!(ExtractPlacementRows()))
                return;

            if (placementRows.Count > 0)
            {
                recordCount = placementRows.Count;

                foreach (XmlNode placementRow in placementRows)
                {
                    try
                    {
                        rowCount += 1;
                        importDetails = new StringBuilder();
                        rowReferenceID = Guid.Empty;
                        parser.RegisterPlacementRow(placementRow);
                        if (!(hasAuditEntry))
                            AssignAuditHeader();

                        em = new BnbEditManager();
                        MapRole();
                        MapRoleSkill();
                        MapPosition();
                        MapPositionObjective();
                        MapPositionTheme();
                        MapPositionVolunteerRole();
                        em.SaveChanges();
                        importDetails.Append(successMessage);
                        recordSucceeded = true;
                        successCount += 1;
                        rowReferenceID = position.ID;
                    }
                    catch (BnbProblemException bonoboError)
                    {
                        recordSucceeded = false;
                        failureCount += 1;
                        importDetails.Append(failureMessage + ";\r\n");
                        foreach (BnbProblem pblm in bonoboError.Problems)
                            importDetails.Append(pblm.Message + ";\r\n");

                    }
                    catch (XmlImportException importError)
                    {
                        recordSucceeded = false;
                        failureCount += 1;
                        importDetails.Append(failureMessage + ";\r\n");
                        importDetails.Append(importError.Message);
                    }
                    catch (Exception generalError)
                    {
                        recordSucceeded = false;
                        failureCount += 1;
                        importDetails.Append(failureMessage + ";\r\n");
                        importDetails.Append("Application error occured");
                    }
                    finally
                    {
                        AssignAuditItem(rowCount, importDetails.ToString(), rowReferenceID, recordSucceeded);
                    }


                    if (failureCount > 0)
                        lblScreenFeedback.Text = "Data import failed.";
                    else
                        lblScreenFeedback.Text = "Data imported successfully.";

                    panelImportResult.Visible = true;
                    lnkXmlImportDetails.NavigateUrl = "XmlImportDetailsPage.aspx?BnbXmlImportAuditHeader=" + auditHeaderRef.ToString() + "&TotalCount=" + rowCount.ToString() + "&Success=" + successCount.ToString() + "&Failure=" + failureCount.ToString();

                }
            }
            else
            {
                lblScreenFeedback.Text = "No row exists for import.";
            }
        }
    
        #endregion

        #region " rdoDefault_CheckedChanged "

        protected void rdoDefault_CheckedChanged(object sender, EventArgs e)
        {
            SetCustomSchemaVisibility();
        }

        #endregion

        #region " rdoCustom_CheckedChanged "

        protected void rdoCustom_CheckedChanged(object sender, EventArgs e)
        {
            SetCustomSchemaVisibility();
        }

        #endregion

        #region " SetCustomSchemaVisibility "

        private void SetCustomSchemaVisibility()
        {
            if (rdoCustom.Checked == true)
                trSchema.Visible = true;
            else
                trSchema.Visible = false;
        }

        #endregion

        #region " MapRoleSkill "

        private void MapRole()
        {
            BnbEmployer employer = null;
            Guid employerID = BnbDomainObject.NullGuid;

            employerID = parser.Employer;
            if (employerID == BnbDomainObject.NullGuid)
                throw new XmlImportException("Employer is missing");
            employer = BnbEmployer.Retrieve(employerID);
            role = new BnbRole(true);
            role.RegisterForEdit(em);
            role.RoleName = parser.RoleName;
            role.RoleAbbrev = parser.RoleAbbrev;
            role.Comments = parser.RoleComments;
            role.Employer = employer;
        }

        #endregion

        #region " MapRoleSkill "

        private void MapRoleSkill()
        {
            int skillCount = 0;
            int loopIdx = 0;
            Collection<int> roleSkills = parser.RoleSkills;
            Collection<int> roleSkillOrders = parser.RoleSkillOrders;
            skillCount = roleSkills.Count;

            if ((skillCount > 0))
            {
                if (skillCount == roleSkillOrders.Count)
                {
                    for (loopIdx = 0; loopIdx < skillCount; loopIdx++)
                    {
                        roleSkill = new BnbRoleSkill(true);
                        roleSkill.RegisterForEdit(em);
                        roleSkill.SkillID = roleSkills[loopIdx];
                        roleSkill.Order = roleSkillOrders[loopIdx];
                        role.RoleSkills.Add(roleSkill);
                    }
                }
                else { throw new Exception("Some of the Role skills are invalid."); }
            }
            else { throw new Exception("There should exist a role skill."); }

        }

        #endregion

        #region " MapPositionObjective "

        private void MapPosition()
        {
            position = new BnbPosition(true);
            position.RegisterForEdit(em);
            position.SummarySentDate = parser.SummarySentDate;
            position.SummaryReceivedDate = parser.SummaryReceivedDate;
            position.SummaryFilename = parser.SummaryFilename;
            position.DescriptionSentDate = parser.DescriptionSentDate;
            position.DescriptionReceivedDate = parser.DescriptionReceivedDate;
            position.DescriptionFilename = parser.DescriptionFilename;
            position.FirmDate = parser.FirmDate;
            position.TentativeDate = parser.TentativeDate;
            position.TentativeReasonID = parser.TentativeReason;
            position.EarliestStartDate = parser.EarliestStartDate;
            position.Priority = parser.Priority;
            position.OverseasLink = parser.OverseasLink;
            position.Duration = parser.Duration;
            position.ProgrammeOfficerID = parser.ProgrammeOfficer;
            position.Replacement = parser.Replacement;
            position.Comments = parser.PositionComments;
            position.FillStatus = parser.FillStatus;
            position.CancelledDate = parser.CancelledDate;
            position.CancelledReason = parser.CancelledReason;
            position.POSummaryFilename = parser.POSummaryFilename;
            position.PODescriptionFilename = parser.PODescriptionFilename;
            position.VSOTypeID = parser.VSOType;
            position.ProgrammeAreaID = parser.ProgrammeArea;
            position.RecruitmentStartDate = parser.RecruitmentStartDate;
            position.RecruitmentEndDate = parser.RecruitmentEndDate;
            role.Positions.Add(position);
        }

        #endregion

        #region " MapPositionObjective "

        private void MapPositionObjective()
        {
            int objectiveCount = 0;
            int loopIdx = 0;
            Collection<int> objectives = parser.PositionObjectives;
            Collection<int> objectiveOrders = parser.PositionObjectiveOrders;
            objectiveCount = objectives.Count;

            if ((objectiveCount > 0))
            {
                if (objectiveCount == objectiveOrders.Count)
                {
                    for (loopIdx = 0; loopIdx < objectiveCount; loopIdx++)
                    {
                        objective = new BnbPositionObjective(true);
                        objective.RegisterForEdit(em);
                        objective.MonitorObjectiveID = objectives[loopIdx];
                        objective.Order = objectiveOrders[loopIdx];
                        position.PositionObjectives.Add(objective);
                    }
                }
                else { throw new Exception("Some of the Objectives are invalid."); }

            }
        }

        #endregion

        #region " MapPositionTheme "

        private void MapPositionTheme()
        {
            int themeCount = 0;
            int loopIdx = 0;
            Collection<int> themes = parser.PositionThemes;
            Collection<int> themeOrders = parser.PositionThemeOrders;
            themeCount = themes.Count;

            if ((themeCount > 0))
            {
                if (themeCount == themeOrders.Count)
                {
                    for (loopIdx = 0; loopIdx < themeCount; loopIdx++)
                    {
                        theme = new BnbPositionTheme(true);
                        theme.RegisterForEdit(em);
                        theme.ThemeID = themes[loopIdx];
                        theme.Order = themeOrders[loopIdx];
                        position.PositionThemes.Add(theme);
                    }
                }
                else { throw new Exception("Some of the Themes are invalid."); }

            }
        }

        #endregion

        #region " MapPositionVolunteerRole "

        private void MapPositionVolunteerRole()
        {
            int volunteerRoleCount = 0;
            int loopIdx = 0;
            Collection<int> volunteerRoles = parser.PositionVolunteerRoles;
            Collection<int> volunteerRoleOrders = parser.PositionVolunteerRoleOrders;
            volunteerRoleCount = volunteerRoles.Count;

            if ((volunteerRoleCount > 0))
            {
                if (volunteerRoleCount == volunteerRoleOrders.Count)
                {
                    for (loopIdx = 0; loopIdx < volunteerRoleCount; loopIdx++)
                    {
                        volunteerRole = new BnbPositionVolunteerRole(true);
                        volunteerRole.RegisterForEdit(em);
                        volunteerRole.VolunteerRoleID = volunteerRoles[loopIdx];
                        volunteerRole.Order = volunteerRoleOrders[loopIdx];
                        position.PositionVolunteerRoles.Add(volunteerRole);
                    }
                }
                else { throw new Exception("Some of the Volunteer Roles are invalid."); }
            }
        }
        #endregion

        #region " AssignAuditHeader "

        private void AssignAuditHeader()
        {
            em = new BnbEditManager();
            auditHeader = new BnbXmlImportAuditHeader(true);
            auditHeader.RegisterForEdit(em);
            auditHeader.XmlMappingTemplateID = mappingID;
            em.SaveChanges();
            auditHeaderRef = auditHeader.ID;
            hasAuditEntry = true;
        }

        #endregion

        #region " AssignAuditItem "

        private void AssignAuditItem(int importRowNumber, string importDetails, Guid referenceID, bool isSuccess)
        {
            em = new BnbEditManager();
            auditItem = new BnbXmlImportAuditItem(true);
            auditItem.RegisterForEdit(em);
            auditItem.XmlImportAuditHeaderID = auditHeaderRef;
            auditItem.ImportDetails = importDetails;
            auditItem.ImportRowNo = importRowNumber.ToString();
            if (referenceID == Guid.Empty)
                auditItem.ReferenceID = BnbDomainObject.NullGuid;
            else
                auditItem.ReferenceID = referenceID;
            auditItem.IsSuccess = isSuccess;
            try
            {
                em.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        #endregion
        
        #region " GetXsdFile "

        private string GetXsdFile()
        {
            if (rdoCustom.Checked == true)
                return fuXmlSchema.PostedFile.FileName;
            else
                return Server.MapPath(@"XmlSchema\Placement.xsd");
        }

        #endregion

        #region " IsValidInputDetails "

        private bool IsValidInputDetails()
        {
            string validationString = string.Empty;

            if (!(ValidateXmlSchema()))
                return false;

            if (!(ValidateXml()))
                return false;

            if (!(ValidateMappingTemplate()))
                return false;

            return true;
        }

        #endregion

        #region " ValidateXmlSchema "

        private bool ValidateXmlSchema()
        {
            string xmlSchemaFile = string.Empty;
            if (rdoCustom.Checked == true)
            {
                xmlSchemaFile = fuXmlSchema.PostedFile.FileName;
                if (!(string.IsNullOrEmpty(xmlSchemaFile)))
                {
                  if(fuXmlSchema.PostedFile.ContentLength <=0)
                  {
                        validationMessage = "Provided XML schema is not a valid file.";
                        return false;
                    }

                }
                else
                {
                    validationMessage = "Please provide an XML schema.";
                    return false;
                }
            }
            return true;
        }

        #endregion

        #region " ValidateXml "

        private bool ValidateXml()
        {
            string xmlFile = fuXml.PostedFile.FileName;;
            if (!(string.IsNullOrEmpty(xmlFile)))
            {
                if (fuXml.PostedFile.ContentLength <= 0)
                {
                    validationMessage = "Provided XML is not a valid file.";
                    return false;
                }
            }
            else
            {
                validationMessage = "Please provide an XML file.";
                return false;
            }

            return true;
        }

        #endregion

        #region " ValidateMappingTemplate "

        private bool ValidateMappingTemplate()
        {
            if (cmbXmlMapping.SelectedValue.ToString() == string.Empty)
            {
                validationMessage = "Please select an Xml mapping template";
                return false;
            }
            return true;
        }

        #endregion

        #region " Initialize "

        private void Initialize()
        {
            BnbEngine.SessionManager.ClearObjectCache();
            BnbWorkareaManager.SetPageStyleOfUser(this);
            panelImportResult.Visible = false;
            if (!this.IsPostBack)
            {
                titleBar.TitleText = "Xml Import";
                BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
                BonoboWebControls.BnbWebFormManager.LogAction(titleBar.TitleText, null, this);
                FillXmlMappingCombo();
            }
        }

        #endregion

        #region " ExtractPlacementRows "

        private bool ExtractPlacementRows()
        {
               string xmlFile = null;
            string xmlSchemaFile = null;
            try
            {
                if (!(IsValidInputDetails()))
                {
                    lblScreenFeedback.Text = validationMessage;
                    return false;
                }
                xmlFile = fuXml.PostedFile.FileName;
                xmlSchemaFile = GetXsdFile();
                mappingID = Convert.ToInt32(cmbXmlMapping.SelectedValue);
                parser = new PlacementParser(mappingID);
                placementRows = parser.GetPlacementRows(xmlSchemaFile, xmlFile);
                if (placementRows == null)
                {
                    lblScreenFeedback.Text = "No row exists for import.";
                     return false;
                }

            }
            catch (XmlImportException importError)
            {
                lblScreenFeedback.Text = importError.Message;
                return false;
            }
            catch (Exception generalError)
            {
                throw new ApplicationException(generalError.Message);
               
            }

            return true;
        }

        #endregion
        
        #region " Cancel_Click "

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Menu.aspx");
        }

        #endregion

        #region " FillXmlMappingCombo "

        /// <summary>
        /// KeyPress event is not working in Standalonelookup control. So, used dropdownlist web control
        /// </summary>
        private void FillXmlMappingCombo()
        {
            // mapping template lookup 
            DataTable templateTable = BnbEngine.LookupManager.GetLookup("lkuXmlMappingTemplate");
            templateTable.DefaultView.RowFilter = "Exclude = 0";
            templateTable.DefaultView.Sort = "Description";
            cmbXmlMapping.DataSource = templateTable.DefaultView;
            cmbXmlMapping.DataTextField = "Description";
            cmbXmlMapping.DataValueField = "IntID";
            cmbXmlMapping.DataBind();
            // create empty item - To set as default selected
            ListItem blankItem = new ListItem("", "");
            cmbXmlMapping.Items.Insert(0, blankItem);

        }

        #endregion

    }

}
