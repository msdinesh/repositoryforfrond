<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="SupplierPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.SupplierPage" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>SupplierPage</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/bnbcompany24silver.gif"></uc1:title>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" NewButtonEnabled="False"
					NewWidth="55px" EditAllWidth="55px" SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px" NewText="New"
					EditAllText="Edit All" SaveText="Save" UndoText="Undo"></bnbpagecontrol:bnbdatabuttons>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox2" runat="server" PopupMessage="false" SuppressRecordSavedMessage="false"
					CssClass="messagebox"></bnbpagecontrol:bnbmessagebox>
				<TABLE class="fieldtable" id="Table1" width="100%" border="0">
					<TR>
						<TD class="label" style="HEIGHT: 33px" width="25%">Supplier Group/Type</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbcmbGroup" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
								DataMember="BnbCompanySupplier.SupplierTypeID" AutoPostBack="False" LookupTableName="vlkuSupplierGroupType" LookupControlType="ComboBox" ListBoxRows="4"
								DataTextField="Description"></bnbdatacontrol:bnblookupcontrol></TD>
					<TR>
					<TR>
						<TD class="label" style="HEIGHT: 33px" width="25%">Recommended</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLkuStatus" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
								DataMember="BnbCompanySupplier.SupplierRecommendedID" LookupTableName="lkuSupplierRecommended" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:bnblookupcontrol></TD>
					<TR>
					</TR>
				</TABLE>
				<P><bnbpagecontrol:bnbdatabuttons id="BnbDataButtons2" runat="server" CssClass="databuttons" NewButtonEnabled="False"
						NewWidth="55px" EditAllWidth="55px" SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px" NewText="New"
						EditAllText="Edit All" SaveText="Save" UndoText="Undo"></bnbpagecontrol:bnbdatabuttons></P>
			</div>
		</form>
	</body>
</HTML>
