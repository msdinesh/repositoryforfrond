<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Page language="c#" Codebehind="WizardNewOrgAction.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.WizardNewAction" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>WizardNewAction</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<DIV class="wizardheader" ms_positioning="FlowLayout">
					<H3><img class="icon24" src="../Images/wizard24silver.gif"> New Organisation Action 
						Wizard</H3>
				</DIV>
				<asp:panel id="panelZero" runat="server" CssClass="wizardpanel">
					<P><EM>This wizard will help you to add&nbsp;a new&nbsp;Action&nbsp;record to an 
							Organisation.</EM>
					<P>Please select the Personnel that you wish to add the Action to, and the type of 
						Action.</P>
					<P></P>
					<P>
						<TABLE class="wizardtable" id="Table2">
							<TR>
								<TD class="label" style="HEIGHT: 15px">Organisation Name</TD>
								<TD style="HEIGHT: 15px">
									<asp:Label id="lblCompanyName" runat="server"></asp:Label></TD>
							</TR>
							<TR>
								<TD class="label">Personnel</TD>
								<TD>
									<asp:DropDownList id="cboPersonnel" runat="server"></asp:DropDownList></TD>
							</TR>
							<TR>
								<TD class="label">Action Type</TD>
								<TD>
									<bnbdatacontrol:BnbStandaloneLookupControl id="cboActionType" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
										DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuContactType"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
							</TR>
						</TABLE>
					<P>
						<asp:label id="lblPanelZeroFeedback" runat="server" CssClass="feedback"></asp:label></P>
					<P>
				</asp:panel></P>
				<P>&nbsp;</P>
				<P>
					<asp:Panel id="panelOne" runat="server" CssClass="wizardpanel">
				</P>
				<P>Please enter further details about the Action, and then press Finish:</P>
				<P>
					<TABLE class="wizardtable" id="Table4">
						<TR>
							<TD class="label" style="HEIGHT: 15px">Organisation Name</TD>
							<TD style="HEIGHT: 15px">
								<asp:Label id="lblCompanyName2" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 25px">Personnel</TD>
							<TD style="HEIGHT: 25px">
								<asp:Label id="lblChosenPersonnel" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 25px">Action Type</TD>
							<TD style="HEIGHT: 25px">
								<asp:Label id="lblActionType" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 25px">VSO Description</TD>
							<TD style="HEIGHT: 25px">
								<asp:DropDownList id="cboVSODescription" runat="server"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 25px">Status</TD>
							<TD style="HEIGHT: 25px">
								<bnbdatacontrol:BnbStandaloneLookupControl id="cboStatus" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
									DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuContactStatus"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 25px">Information</TD>
							<TD style="HEIGHT: 25px">
								<asp:TextBox id="txtInformation" runat="server" Width="216px"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 25px">Sent Via</TD>
							<TD style="HEIGHT: 25px">
								<bnbdatacontrol:BnbStandaloneLookupControl id="cboSentVia" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
									DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuSentVia"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 25px">Reference</TD>
							<TD style="HEIGHT: 25px">
								<asp:TextBox id="txtReference" runat="server" Width="216px"></asp:TextBox></TD>
						</TR>
					</TABLE>
				</P>
				<P>
					<asp:Label id="lblPanelOneFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				<P></asp:Panel></P>
				<P>
					<uc1:WizardButtons id="wizardButtons" runat="server"></uc1:WizardButtons></P>
				<P>
					<asp:Label id="lblHiddenCompanyID" runat="server" Visible="False"></asp:Label>
					<asp:Label id="lblHiddenIndividualID" runat="server" Visible="False"></asp:Label></P>
				<P>&nbsp;</P>
			</div>
		</form>
	</body>
</HTML>
