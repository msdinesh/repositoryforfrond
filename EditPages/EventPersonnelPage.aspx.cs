using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for EventPersonnelPage.
	/// </summary>
	public partial class EventPersonnelPage : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
		protected Frond.UserControls.NavigationBar NavigationBar1;

		
		private BnbWebFormManager bnb = null;

		protected void Page_Load(object sender, System.EventArgs e)
		{

			// if new, re-direct to wizard
			if (BnbWebFormManager.IsPageDomainObjectNew("BnbEventPersonnel"))
			{
				// this must have come from individual page - get individual ID
				string indvIDasString = Request.QueryString["BnbIndividual"];
				Response.Redirect(String.Format("WizardNewAttendance.aspx?BnbIndividual={0}&EVP=1",
					indvIDasString));
			}


#if DEBUG
			
			if(Request.QueryString.ToString() == "")
			{
				BnbWebFormManager.ReloadPage("BnbEventPersonnel=15CABFD5-558E-453D-B7C6-0EAB3D82A771");
			}
			else 
			{
				bnb = new BnbWebFormManager(this,"BnbEventPersonnel");
			}
#else
			bnb = new BnbWebFormManager(this,"BnbEventPersonnel");
#endif
			// Put user code to initialize the page here
			bnb.LogPageHistory = true;
			BnbWorkareaManager.SetPageStyleOfUser(this);

			BnbEventPersonnel person = bnb.GetPageDomainObject("BnbEventPersonnel",true) as BnbEventPersonnel;
			string indv = person.IndividualID.ToString();
			NavigationBar1.RootDomainObject = "BnbIndividual";
			NavigationBar1.RootDomainObjectID= indv;

			// tweak page title settings
			bnb.PageNameOverride = "Event Personnel";
			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
			}
			/*
			if (Request.QueryString["BnbEvent"].ToString().Trim().Length > 0)
			{
				BnbHyperlink1.Visible = true;
				BnbHyperlink1.NavigateUrl = "EventPage.aspx?BnbEvent=" + Request.QueryString["BnbEvent"];
			}
			else
				BnbHyperlink1.Visible = false;*/

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
