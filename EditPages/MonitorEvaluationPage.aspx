<%@ Page Language="c#" Codebehind="MonitorEvaluationPage.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.EditPages.MonitorEvaluationPage" %>

<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>MonitorEvaluationPage</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet" />
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/bnbposition24silver.gif">
            </uc1:Title>
            <table style="margin: 0px" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td colspan="2" align="left" >
                        <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons1" runat="server" UndoText="Undo"
                            SaveText="Save" EditAllText="Edit All" NewText="New" HeightAllButtons="25px"
                            UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" CssClass="databuttons"
                            NewButtonEnabled="False"></bnbpagecontrol:BnbDataButtons>
                    </td>
                </tr>
            </table>
            <bnbpagecontrol:BnbMessageBox ID="BnbMessageBox1" runat="server" CssClass="messagebox">
            </bnbpagecontrol:BnbMessageBox>
             
                <p>
                    <table id="Table1" width="100%" border="0" class="fieldtable">
                        <tr>
                            <td class="label" width="40%">
                                Placement Ref</td>
                            <td>
                                <bnbdatacontrol:BnbDomainObjectHyperlink ID="BnbDomainObjectHyperlink1" runat="server"
                                    Target="_top" HyperlinkText="[BnbDomainObjectHyperlink]" DataMember="BnbPosition.FullPlacementReference"
                                    GuidProperty="BnbPosition.ID" RedirectPage="PlacementPage.aspx"></bnbdatacontrol:BnbDomainObjectHyperlink>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="40%">
                                Logged by</td>
                            <td>
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl1" runat="server" CssClass="lookupcontrol"
                                    Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                    DataMember="BnbMonitor.CreatedBy" LookupTableName="vlkuBonoboUser"></bnbdatacontrol:BnbLookupControl>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="40%">
                                Date</td>
                            <td>
                                <bnbdatacontrol:BnbDateBox ID="BnbDateBox1" runat="server" CssClass="datebox" Height="20px"
                                    Width="90px" DateCulture="en-GB" DateFormat="dd/mm/yyyy" DataMember="BnbMonitor.CreatedOn">
                                </bnbdatacontrol:BnbDateBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="40%">
                                Employer Type</td>
                            <td>
                                <bnbdatacontrol:BnbLookupControl ID="lucEmployerType" runat="server" CssClass="lookupcontrol"
                                    Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                    DataMember="BnbPosition.Role.Employer.EmployerTypeID" LookupTableName="lkuEmployerType">
                                </bnbdatacontrol:BnbLookupControl>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="40%">
                                VSO Goal</td>
                            <td>
                                <bnbdatacontrol:BnbLookupControl ID="bnblckVSOGoal" runat="server" CssClass="lookupcontrol"
                                    Width="250px" Height="22px" DataMember="BnbPositionObjective.MonitorObjectiveID"
                                    LookupTableName="lkuMonitorObjective" LookupControlType="ComboBox" ListBoxRows="4"
                                    DataTextField="Description" Visible="False"></bnbdatacontrol:BnbLookupControl>
                                <br>
                                <bnbdatagrid:BnbDataGridForDomainObjectList ID="bnbdtgVSOGoals" runat="server" CssClass="datagrid"
                                    Width="200px" DataGridType="Editable" DataMember="BnbPosition.PositionObjectives"
                                    AddButtonVisible="true" EditButtonText="Edit" AddButtonText="Add" EditButtonVisible="true"
                                    DeleteButtonVisible="false" DataProperties="MonitorObjectiveID [BnbLookupControl:lkuMonitorObjective]">
                                </bnbdatagrid:BnbDataGridForDomainObjectList>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="40%">
                                Theme</td>
                            <td>
                                <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList2"
                                    runat="server" CssClass="datagrid" Width="200px" DataGridType="Editable" DataMember="BnbPosition.PositionThemes"
                                    AddButtonVisible="true" EditButtonText="Edit" AddButtonText="Add" EditButtonVisible="true"
                                    DeleteButtonVisible="True" DataProperties="ThemeID [BnbLookupControl:lkuTheme]">
                                </bnbdatagrid:BnbDataGridForDomainObjectList>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="40%">
                                Achievement of placement objectives</td>
                            <td>
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl3" runat="server" CssClass="lookupcontrol"
                                    Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                    DataMember="BnbMonitor.APOVolunteerID" LookupTableName="lkuMonitorResponse"></bnbdatacontrol:BnbLookupControl>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="40%">
                                How satisfied is the volunteer with:</td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="40%">
                                Placement</td>
                            <td>
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl4" runat="server" CssClass="lookupcontrol"
                                    Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                    DataMember="BnbMonitor.SatVolunteerPlacementID" LookupTableName="lkuMonitorSatisfaction">
                                </bnbdatacontrol:BnbLookupControl>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="40%">
                                VSO</td>
                            <td>
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl5" runat="server" CssClass="lookupcontrol"
                                    Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                    DataMember="BnbMonitor.SatVolunteerVSOID" LookupTableName="lkuMonitorSatisfaction">
                                </bnbdatacontrol:BnbLookupControl>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="40%">
                                How satisfied is the employer with:</td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="40%">
                                Volunteer</td>
                            <td>
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl6" runat="server" CssClass="lookupcontrol"
                                    Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                    DataMember="BnbMonitor.SatEmployerPlacementID" LookupTableName="lkuMonitorSatisfaction">
                                </bnbdatacontrol:BnbLookupControl>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="40%">
                                VSO</td>
                            <td>
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl7" runat="server" CssClass="lookupcontrol"
                                    Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                    DataMember="BnbMonitor.SatEmployerVSOID" LookupTableName="lkuMonitorSatisfaction">
                                </bnbdatacontrol:BnbLookupControl>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="40%">
                                Comments</td>
                            <td>
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox1" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" StringLength="0" Rows="3" MultiLine="True" DataMember="BnbMonitor.Comments">
                                </bnbdatacontrol:BnbTextBox>
                            </td>
                        </tr>
                    </table>
                </p>
              <p>
                <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons2" runat="server" UndoText="Undo"
                    SaveText="Save" EditAllText="Edit All" NewText="New" HeightAllButtons="25px"
                    UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" CssClass="databuttons"
                    NewButtonEnabled="False"></bnbpagecontrol:BnbDataButtons>
            </p>
        </div>
    </form>
</body>
</html>
