using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for FundingPage.
	/// </summary>
	public partial class FundingPage : System.Web.UI.Page
	{
		protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading1;
		protected UserControls.Title titleBar;
		protected UserControls.NavigatePanel navPanel;

		private BnbWebFormManager bnb = null;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{

			// re-direct to wizard if new mode
			if (this.Request.QueryString["BnbFunding"] != null &&
				this.Request.QueryString["BnbFunding"].ToLower() == "new")
			{
				doRedirectToFundingWizard();
			}

			// Put user code to initialize the page here
			// ECD515E4-86A8-11D7-ABFC-009027882594
            //http://dev01/starfish/EditPages/ApplicationPage.aspx?SFID=D13DEB8F-BE02-11D6-909F-00508BACE998
			//http://dev01/starfish/EditPages/PlacementPage.aspx?SFID=8874B549-26BE-11D6-9C2C-000102A32E88
#if DEBUG
			if(Page.Request.QueryString.ToString() == "")
			{
				string fundingquery = "&BnbFunding=ECD515E4-86A8-11D7-ABFC-009027882594";
				BnbWebFormManager.ReloadPage("BnbIndividual=D13DEB8F-BE02-11D6-909F-00508BACE998" + fundingquery);
			}
			else
			{
				bnb = new BnbWebFormManager(this, "BnbFunding");
			}
#else
			bnb = new BnbWebFormManager(this, "BnbFunding");
#endif
			BnbWorkareaManager.SetPageStyleOfUser(this);
			bnb.LogPageHistory = true;
			doInitialiseControls();
			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;

				doNavigatePanelLinks();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void doInitialiseControls()
		{
			string fundingValue = Page.Request.QueryString["BnbFunding"];
			string appValue = Page.Request.QueryString["BnbApplication"];
			string posValue = Page.Request.QueryString["BnbPosition"];
			// if funding id specified but other two aren't, try getting from object
			if (fundingValue != null && fundingValue.ToLower() != "new")
			{
				BnbFunding tempFund = BnbFunding.Retrieve(new Guid(fundingValue));
				if (appValue == null && tempFund.ApplicationID != Guid.Empty)
					appValue = tempFund.ApplicationID.ToString();
				
				if (posValue == null && tempFund.PositionID != Guid.Empty)
					posValue = tempFund.PositionID.ToString();

			}
			bool isApp = (appValue != null && appValue.ToLower() != "new");
			bool isPos = (posValue != null && posValue.ToLower() != "new");

			if(isApp)
			{
				BnbApplication app = BnbApplication.Retrieve(new System.Guid(appValue));
				hlkVolunteer.Text = app.Individual.FullName;
				hlkVolunteer.NavigateUrl = "ApplicationPage.aspx?BnbApplication=" + app.ID.ToString().ToUpper();
				// does app have a placement?
				if (app.PlacedService != null && app.PlacedService.Position != null)
				{
					BnbPosition pos = app.PlacedService.Position;
					hlkPlacement.Text = pos.FullPlacementReference;
					hlkPlacement.NavigateUrl = "PlacementPage.aspx?BnbPosition=" + pos.ID.ToString().ToUpper();
					// if not postback, add to nav panel
					if (!this.IsPostBack)
						navPanel.AddLink("Placement", hlkPlacement.NavigateUrl);
				}
			}
			if(isPos)
			{
				BnbPosition pos = BnbPosition.Retrieve(new System.Guid(posValue));
				hlkPlacement.Text = pos.FullPlacementReference;
				hlkPlacement.NavigateUrl = "PlacementPage.aspx?BnbPosition=" + pos.ID.ToString().ToUpper();
				// does pos have an app?
				if (pos.PlacedService != null && pos.PlacedService.Application != null)
				{
					BnbApplication app = pos.PlacedService.Application;
					hlkVolunteer.Text = app.Individual.FullName;
					hlkVolunteer.NavigateUrl = "ApplicationPage.aspx?BnbApplication=" + app.ID.ToString().ToUpper();
					// if not postback, add to nav panel
					if (!this.IsPostBack)
						navPanel.AddLink("Application", hlkVolunteer.NavigateUrl);
				}
			}

		
		
		}

		/*
		private void doInitialiseFunderTypeControl()
		{
			BnbFunder funder = ((BnbFunding)bnb.ObjectTree.DomainObjectDictionary["BnbFunding"]).Funder;
			if(funder == null) bnblckFunderType.Visible = false;
			else bnblckFunderType.Visible = true;
		}
		*/

		private void doRedirectToFundingWizard()
		{
			string posID = this.Request.QueryString["BnbPosition"];
			string appID = this.Request.QueryString["BnbApplication"];
			// need to pass either posID or appID to funding wizard
			if (appID != null && appID.ToLower() != "new")
				Response.Redirect(String.Format("WizardNewFunding.aspx?BnbApplication={0}",
					appID));
			else
				if (posID != null && posID.ToLower() != "new")
					Response.Redirect(String.Format("WizardNewFunding.aspx?BnbPosition={0}",
						posID));
		}

		private void doNavigatePanelLinks()
		{
			// some links may already have been added
			// this looks for links to org or individual
			string fundingID = Request.QueryString["BnbFunding"];
			if (fundingID != "" && fundingID.ToLower() != "new")
			{
				BnbFunding tempFunding = BnbFunding.Retrieve(new Guid(fundingID));
				if (tempFunding.Company != null)
					navPanel.AddLink("Organisation (Donor)", String.Format("OrganisationPage.aspx?BnbCompany={0}",
						tempFunding.CompanyID.ToString().ToUpper()));
				if (tempFunding.DonorIndividual != null)
					navPanel.AddLink("Individual (Donor)", String.Format("IndividualPage.aspx?BnbIndividual={0}",
						tempFunding.DonorIndividualID.ToString().ToUpper()));
			}

		}
	}
}
