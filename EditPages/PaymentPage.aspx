<%@ Register TagPrefix="cc1" Namespace="BonoboWebControls.ServicesControls" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="PaymentPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.PaymentPage" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/bnbaccountheader24silver.gif"></uc1:title>
				<uc1:NavigationBar id="NavigationBar1" runat="server"></uc1:NavigationBar>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" UndoText="Undo" SaveText="Save"
					EditAllText="Edit All" NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px"
					NewWidth="55px" NewButtonEnabled="True"></bnbpagecontrol:bnbdatabuttons>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:bnbmessagebox>
				<bnbpagecontrol:bnbtabcontrol id="BnbTabControl1" runat="server"></bnbpagecontrol:bnbtabcontrol>
				<asp:panel id="MainTab" runat="server" CssClass="tcTab" Width="800px" BnbTabName="Main">
					<TABLE class="fieldtable" id="Table1" border="0">
						<TR>
							<TD class="label" style="WIDTH: 75px; HEIGHT: 5px" width="75">Voucher ID
							</TD>
							<TD style="WIDTH: 211px; HEIGHT: 5px" width="211">
								<bnbdatacontrol:bnbtextbox id="BnbTextBox1" runat="server" CssClass="textbox" Width="90px" DataMember="BnbAccountHeader.VoucherID"
									Height="19px" StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" style="WIDTH: 138px; HEIGHT: 5px" width="138">Voucher Type
							</TD>
							<TD style="WIDTH: 237px; HEIGHT: 5px" width="237">
								<bnbdatacontrol:bnblookupcontrol id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" Width="201px" DataMember="BnbAccountHeader.VoucherTypeID"
									Height="19px" LookupTableName="lkuVoucherType" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" style="WIDTH: 107px; HEIGHT: 5px" width="107">Method
							</TD>
							<TD style="WIDTH: 237px; HEIGHT: 5px" width="237">
								<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol12" runat="server" CssClass="lookupcontrol" Width="201" DataMember="BnbAccountHeader.PaymentMethodID"
									Height="19" LookupTableName="lkuPaymentMethod" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 75px; HEIGHT: 19px" width="75">To be paid By</TD>
							<TD style="WIDTH: 211px; HEIGHT: 19px" width="211">
								<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol14" runat="server" CssClass="lookupcontrol" Width="201px" DataMember="BnbAccountHeader.PaidByID"
									Height="19px" LookupTableName="vlkuBonoboUser" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" style="WIDTH: 138px; HEIGHT: 19px" width="138">To be paid On</TD>
							<TD style="WIDTH: 231px; HEIGHT: 19px" width="231">
								<bnbdatacontrol:bnbdatebox id="BnbDateBox2" runat="server" CssClass="datebox" Width="91" DataMember="BnbAccountHeader.DateDue"
									Height="21" DateCulture="en-GB" DateFormat="dd/mm/yyyy"></bnbdatacontrol:bnbdatebox></TD>
							<TD class="label" style="WIDTH: 107px; HEIGHT: 19px" width="107">Amount �
							</TD>
							<TD style="WIDTH: 237px; HEIGHT: 19px" width="237">
								<bnbdatacontrol:bnbdecimalbox id="BnbDecimalBox1" runat="server" CssClass="textbox" Width="95px" DataMember="BnbAccountHeader.AmountPayedAsDouble"
									Height="21px" FormatExpression="0.0000" TextAlign="Default" DisplayEmptyValue="False" DefaultValue="0"></bnbdatacontrol:bnbdecimalbox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 75px; HEIGHT: 16px" width="75">Pay</TD>
							<TD style="WIDTH: 211px; HEIGHT: 16px" width="211">
								<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol21" runat="server" CssClass="lookupcontrol" Width="201px" DataMember="BnbAccountHeader.PayTypeID"
									Height="19px" LookupTableName="lkuCurrencyConversionType" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" style="WIDTH: 138px; HEIGHT: 16px" width="138">Currency
							</TD>
							<TD style="WIDTH: 237px; HEIGHT: 16px" width="237">
								<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol22" runat="server" CssClass="lookupcontrol" Width="201px" DataMember="BnbAccountHeader.CurrencyID"
									Height="19px" LookupTableName="lkuCurrency" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" style="WIDTH: 107px; HEIGHT: 16px" width="107">Rate
							</TD>
							<TD style="HEIGHT: 16px" width="25%">
								<bnbdatacontrol:bnbtextbox id="Bnbtextbox01" runat="server" CssClass="textbox" Width="95px" DataMember="BnbAccountHeader.RateCurrency"
									Height="21px" StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 75px; HEIGHT: 10px" width="75">Payee Type</TD>
							<TD style="WIDTH: 211px; HEIGHT: 10px" width="211">
								<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol13" runat="server" CssClass="lookupcontrol" Width="201px" DataMember="BnbAccountHeader.PayeeTypeID"
									Height="19px" LookupTableName="lkuPayeeType" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" style="WIDTH: 138px; HEIGHT: 10px" width="138">Payee Name<BR>
							</TD>
							<TD style="WIDTH: 237px; HEIGHT: 10px" width="237">
								<bnbdatacontrol:bnbtextbox id="Bnbtextbox12" runat="server" CssClass="textbox" Width="201px" DataMember="BnbAccountHeader.PayName"
									Height="19px" StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" style="WIDTH: 107px; HEIGHT: 10px" width="107">&nbsp;Currency 
								Amount
							</TD>
							<TD style="HEIGHT: 10px" width="25%">
								<bnbdatacontrol:bnbdecimalbox id="Bnbdecimalbox29" runat="server" CssClass="textbox" Width="90px" DataMember="BnbAccountHeader.AmountCurrentAsDouble"
									Height="21px" FormatExpression="0.00" TextAlign="Default" DisplayEmptyValue="False" DefaultValue="0"></bnbdatacontrol:bnbdecimalbox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 75px; HEIGHT: 23px" width="75">Processed</TD>
							<TD style="WIDTH: 211px; HEIGHT: 23px" width="211">
								<bnbdatacontrol:bnbdatebox id="Bnbdatebox4" runat="server" CssClass="datebox" Width="91" DataMember="BnbAccountHeader.DateProcessed"
									Height="21" DateCulture="en-GB" DateFormat="dd/mm/yyyy"></bnbdatacontrol:bnbdatebox></TD>
							<TD class="label" style="WIDTH: 138px; HEIGHT: 23px" width="138">Processed By</TD>
							<TD style="WIDTH: 237px; HEIGHT: 23px" vAlign="top" width="237">
								<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol6" runat="server" CssClass="lookupcontrol" Width="201px" DataMember="BnbAccountHeader.ProcessedByID"
									Height="19px" LookupTableName="vlkuBonoboUser" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" style="WIDTH: 107px; HEIGHT: 23px" width="107"></TD>
							<TD style="HEIGHT: 23px" width="25%"></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 75px" width="75">
							</TD>
							<TD style="WIDTH: 211px" width="211">
								</TD>
							<TD class="label" style="WIDTH: 138px" width="138">Payment Location</TD>
							<TD style="WIDTH: 237px" vAlign="top" width="237">
								<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol7" runat="server" CssClass="lookupcontrol" Width="201px" DataMember="BnbAccountHeader.PaymentLocationID"
									Height="19px" LookupTableName="lkuCountry" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" style="WIDTH: 107px" width="107">Ref No (Cheque or Batch No)</TD>
							<TD width="25%">
								<P></P>
								<P>
									<bnbdatacontrol:bnbtextbox id="Bnbtextbox6" runat="server" CssClass="textbox" Width="90px" DataMember="BnbAccountHeader.PaymentID"
										Height="21px" StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:bnbtextbox></P>
							</TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 75px">Payment Address</TD>
							<TD colSpan="5">
								<P>
									<bnbdatacontrol:BnbQueryLookupControl id="queryLookupPaymentAddress" runat="server" CssClass="lookupcontrol" Width="616px"
										DataMember="BnbAccountHeader.PaymentAddressID" Height="22px" ListBoxRows="4" LookupControlType="ComboBox" BonoboViewFieldToBeBound="tblAddress_AddressID"
										BonoboViewName="vwBonoboFindAddress" BonoboViewFieldToDisplay="Custom_CombinedAddress"></bnbdatacontrol:BnbQueryLookupControl></P>
							</TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 75px">Bank Address</TD>
							<TD colSpan="5">
								<bnbdatacontrol:BnbQueryLookupControl id="queryLookupBankAddress" runat="server" CssClass="lookupcontrol" Width="616px"
									DataMember="BnbAccountHeader.PaymentBankID" Height="22px" ListBoxRows="4" LookupControlType="ComboBox" BonoboViewFieldToBeBound="tblAddress_AddressID"
									BonoboViewName="vwBonoboFindAddress" BonoboViewFieldToDisplay="Custom_CombinedAddress"></bnbdatacontrol:BnbQueryLookupControl></TD>
						</TR>
					</TABLE>
					<P></P>
					<P>
						<cc1:BnbWordFormButton id="cmdPaymentVouchers" runat="server" CssClass="button" Width="184px" Height="36px"
							DESIGNTIMEDRAGDROP="140" Text="Download Payment Voucher" onclicked="cmdPaymentVouchers_Clicked"></cc1:BnbWordFormButton></P>
					<bnbpagecontrol:BnbSectionHeading id="Bnbsectionheading7" runat="server" CssClass="sectionheading" Height="20px" Width="100%"
						Collapsed="false" SectionHeading="Payment Items"></bnbpagecontrol:BnbSectionHeading>
					<P>
						<bnbgenericcontrols:BnbHyperlink id="bonhyperGenerateRQ10" runat="server">New EOS Grant Payment Item</bnbgenericcontrols:BnbHyperlink></P>
					<P>
						<bnbdatagrid:bnbdatagridforview id="accountItemsBnbDataGridForView" runat="server" CssClass="datagrid" Width="912px"
							RedirectPage="PaymentDetailPage.aspx" NewLinkText="New Payment Item" NewLinkVisible="True" ViewLinksVisible="true"
							ViewName="vwBonoboPaymentDetails" DomainObject="BnbAccountItem" GuidKey="tblAccountItem_AccountItemID" QueryStringKey="BnbAccountHeader"
							FieldName="tblAccountItem_AccountHeaderID"></bnbdatagrid:bnbdatagridforview></P>
					<P>&nbsp;</P>
				</asp:panel>
				<asp:panel id="OverseasGrantTotalsTab" runat="server" CssClass="tcTab" BnbTabName="Volunteer Grant Totals">
					<DIV style="WIDTH: 656px; POSITION: relative; HEIGHT: 526px" ms_positioning="GridLayout">
						<DIV style="DISPLAY: inline; Z-INDEX: 101; LEFT: 88px; WIDTH: 488px; POSITION: absolute; TOP: 8px; HEIGHT: 18px"
							ms_positioning="FlowLayout">Total grant payments made to the current 
							application</DIV>
						<TABLE id="Table2" style="Z-INDEX: 102; LEFT: 32px; WIDTH: 600px; POSITION: absolute; TOP: 72px; HEIGHT: 96px"
							borderColor="black" cellSpacing="0" cellPadding="1" width="600" border="1">
							<TR align="right">
								<TD style="WIDTH: 307px; HEIGHT: 8px" colSpan="2">
									<DIV style="DISPLAY: inline; WIDTH: 128px; HEIGHT: 2px" ms_positioning="FlowLayout">At 
										today's date</DIV>
								</TD>
								<TD style="HEIGHT: 8px" colSpan="2">&nbsp;
									<asp:Label id="Label2" runat="server">Months:</asp:Label>&nbsp;&nbsp;&nbsp;
									<asp:Label id="lblGridMonthsToday" runat="server">Label</asp:Label></TD>
							</TR>
							<TR align="right">
								<TD style="WIDTH: 151px; HEIGHT: 17px">
									<P>&nbsp;</P>
									<P>
										<asp:Label id="Label16" runat="server">VG99</asp:Label></P>
									<P>
										<asp:Label id="Label15" runat="server">VG01</asp:Label></P>
									<P>
										<asp:Label id="Label14" runat="server">VP03</asp:Label></P>
								</TD>
								<TD style="WIDTH: 151px; HEIGHT: 17px">
									<P>
										<asp:Label id="Label9" runat="server">Entitlement</asp:Label></P>
									<P>
										<asp:Label id="lblGridEOSGrantAdvanceEntitlement" runat="server">Label</asp:Label></P>
									<P>
										<asp:Label id="lblGridInServiceGrantEntitlementDesc" runat="server">Label</asp:Label></P>
									<P>
										<asp:Label id="lblGridExtendersGrantEntitlementCount" runat="server">Label</asp:Label></P>
								</TD>
								<TD style="WIDTH: 139px; HEIGHT: 17px" align="right">
									<asp:Label id="Label13" runat="server">Already Paid</asp:Label>
									<P>
										<asp:Label id="lblGridEOSGrantAdvancePaid" runat="server">Label</asp:Label></P>
									<P>
										<asp:Label id="lblGridInServiceGrantPaid" runat="server">Label</asp:Label></P>
									<P>
										<asp:Label id="lblGridExtendersGrantAlreadyPaidDesc" runat="server">Label</asp:Label></P>
								</TD>
								<TD style="HEIGHT: 17px" align="right">
									<asp:Label id="Label1" runat="server">Liability</asp:Label>
									<P>
										<asp:Label id="lblGridEOSGrantAdvanceLiability" runat="server">Label</asp:Label></P>
									<P>
										<asp:Label id="lblGridInServiceGrantLiability" runat="server">Label</asp:Label></P>
									<P>
										<asp:Label id="lblGridExtendersGrantLiabilityCount" runat="server">Label</asp:Label></P>
								</TD>
							</TR>
						</TABLE>
						<TABLE id="Table3" style="Z-INDEX: 103; LEFT: 32px; WIDTH: 600px; POSITION: absolute; TOP: 32px; HEIGHT: 33px"
							cellSpacing="1" cellPadding="1" width="600" border="0">
							<TR>
								<TD style="WIDTH: 85px">
									<asp:Label id="Label34" runat="server">SOS</asp:Label></TD>
								<TD style="WIDTH: 207px">
									<asp:Label id="lblSOSWithType" runat="server">Label</asp:Label></TD>
								<TD style="WIDTH: 176px">
									<asp:Label id="Label38" runat="server">Months Today</asp:Label></TD>
								<TD>
									<asp:Label id="lblMonthsTodayNum" runat="server">Label</asp:Label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 85px">
									<asp:Label id="Label35" runat="server">EOS</asp:Label></TD>
								<TD style="WIDTH: 207px">
									<asp:Label id="lblEOSWithType" runat="server">Label</asp:Label></TD>
								<TD style="WIDTH: 176px">
									<asp:Label id="Label39" runat="server">Months at EOS</asp:Label></TD>
								<TD>
									<asp:Label id="lblMonthsAtEOSNum" runat="server">Label</asp:Label></TD>
							</TR>
						</TABLE>
						<TABLE id="Table4" style="Z-INDEX: 104; LEFT: 32px; WIDTH: 600px; POSITION: absolute; TOP: 208px; HEIGHT: 104px"
							borderColor="black" cellSpacing="0" cellPadding="1" width="600" border="1">
							<TR align="right">
								<TD style="WIDTH: 310px; HEIGHT: 8px" colSpan="2">
									<asp:Label id="Label47" runat="server">At EOS date</asp:Label></TD>
								<TD style="HEIGHT: 8px" colSpan="2">&nbsp;
									<asp:Label id="Label4" runat="server">Months:</asp:Label>&nbsp;&nbsp;&nbsp;
									<asp:Label id="lblGridMonthsAtEOS" runat="server">Label</asp:Label></TD>
							</TR>
							<TR align="right">
								<TD style="WIDTH: 152px; HEIGHT: 17px">
									<P>&nbsp;</P>
									<P>
										<asp:Label id="Label19" runat="server">VG99 (VG60)</asp:Label></P>
									<P>
										<asp:Label id="Label18" runat="server">VG01</asp:Label></P>
									<P>
										<asp:Label id="Label17" runat="server">VP03</asp:Label></P>
								</TD>
								<TD style="WIDTH: 153px; HEIGHT: 17px" align="right">
									<asp:Label id="Label20" runat="server">Entitlement</asp:Label>
									<P>
										<asp:Label id="lblGridEOSGrantFinalEntitlement" runat="server">Label</asp:Label></P>
									<P>
										<asp:Label id="lblGridInServiceGrantFinalEntitlementDesc" runat="server">Label</asp:Label></P>
									<P>
										<asp:Label id="lblGridExtendersGrantFinalEntitlement" runat="server">Label</asp:Label></P>
								</TD>
								<TD style="WIDTH: 136px; HEIGHT: 17px">
									<asp:Label id="Label29" runat="server">Already Paid</asp:Label>
									<P>
										<asp:Label id="lblGridEOSGrantFinalPaid" runat="server">Label</asp:Label></P>
									<P>
										<asp:Label id="lblGridInServiceGrantPaid2" runat="server">Label</asp:Label></P>
									<P>
										<asp:Label id="lblGridExtendersGrantAlreadyPaidDesc2" runat="server">Label</asp:Label></P>
								</TD>
								<TD style="HEIGHT: 17px">
									<asp:Label id="Label33" runat="server">Liability</asp:Label>
									<P>
										<asp:Label id="lblGridEOSGrantFinalLiability" runat="server">Label</asp:Label></P>
									<P>
										<asp:Label id="lblGridInServiceGrantFinalLiability" runat="server">Label</asp:Label></P>
									<P>
										<asp:Label id="lblGridExtendersGrantFinalLiabilityCount" runat="server">label</asp:Label></P>
								</TD>
							</TR>
						</TABLE>
						<TABLE id="Table5" style="Z-INDEX: 105; LEFT: 272px; WIDTH: 360px; POSITION: absolute; TOP: 352px; HEIGHT: 77px"
							cellSpacing="1" cellPadding="1" width="360" border="0">
							<TR>
								<TD style="WIDTH: 149px">
									<asp:Label id="Label42" runat="server">Estimated VG60 Liability</asp:Label></TD>
								<TD>
									<asp:Label id="lblEOSGrantFinalLiability2" runat="server">Label</asp:Label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 149px; HEIGHT: 18px">
									<asp:Label id="Label37" runat="server">VG01 Repayment</asp:Label></TD>
								<TD style="HEIGHT: 18px">
									<asp:Label id="lblEOSGrantCalcInServiceRepay" runat="server">Label</asp:Label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 149px">
									<asp:Label id="Label25" runat="server">VP03 Repayment</asp:Label></TD>
								<TD>
									<asp:Label id="lblEOSGrantCalcExtendersRepay" runat="server">Label</asp:Label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 149px">
									<asp:Label id="Label43" runat="server">Estimated VG60 Payment</asp:Label></TD>
								<TD>
									<asp:Label id="lblEOSGrantCalcFinal" runat="server">Label</asp:Label></TD>
							</TR>
						</TABLE>
						<asp:Label id="lblInServiceGrantRate" style="Z-INDEX: 106; LEFT: 40px; POSITION: absolute; TOP: 352px"
							runat="server" Width="216px" Height="48px">Label</asp:Label></DIV>
				</asp:panel>
			</div>
		</form>
	</body>
</HTML>
