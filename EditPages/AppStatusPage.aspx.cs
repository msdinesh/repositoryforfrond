using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboDomainObjects;
using BonoboWebControls;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for AppStatusPage.
	/// </summary>
	public partial class AppStatusPage : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
		protected Frond.UserControls.NavigationBar NavigationBar1;

	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			BnbWebFormManager bnb = null;
#if DEBUG	
			//			bnb = null;
			if (Request.QueryString.ToString() == "")
				BnbWebFormManager.ReloadPage("BnbApplicationStatus=B73A83D9-6CF6-4834-A4DA-A3015E86FC02");
			else
				bnb = new BnbWebFormManager(this,"BnbApplicationStatus");
#else	
			bnb = new BnbWebFormManager(this,"BnbApplicationStatus");
#endif
			bnb.LogPageHistory = true;
			BnbWorkareaManager.SetPageStyleOfUser(this);
			// tweak page title settings
			bnb.PageNameOverride = "Application Status";
			NavigationBar1.RootDomainObject = "BnbApplication";
			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
			}
			doIndividualHyperlink();
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	
		private string getIndividual() 
		{
			System.Guid id = new System.Guid(this.Page.Request.QueryString["BnbApplication"]);
			BnbApplication app = (BnbApplication)BonoboEngine.BnbDomainObject.GeneralRetrieve(id,typeof(BnbApplication));
			return app.IndividualID.ToString();
		}



		private void doIndividualHyperlink()
		{
			string strApplicationID = Page.Request.QueryString["BnbApplication"];
			
			if (strApplicationID != null && strApplicationID.ToLower() != "new")
			{
				BnbApplication objApplication = BnbApplication.Retrieve(new Guid(strApplicationID));
				if (objApplication.Individual.FullName != null)
					BnbLnkIndividual.Text = objApplication.Individual.FullName;
				else
					BnbLnkIndividual.Text = "Back to the application page";
				BnbLnkIndividual.NavigateUrl = "ApplicationPage.aspx?BnbApplication=" + strApplicationID;
			}
			BnbLnkIndividual.Enabled = true;
		}

	
	}
}
