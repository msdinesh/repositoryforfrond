using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;
using BonoboEngine;
using BonoboEngine.Query;
using System.Globalization;


namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for PaymentPage.
	/// </summary>
	public partial class PaymentPage : System.Web.UI.Page
	{
		protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox3;
		protected BonoboWebControls.DataControls.BnbLookupControl BnbLookupControl2;
		protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox4;
		protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox5;
		protected BonoboWebControls.DataControls.BnbLookupControl BnbLookupControl3;
		protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox9;
		protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox10;
		protected System.Web.UI.WebControls.Panel ContactNumberTab;
		protected BonoboWebControls.DataGrids.BnbDataGridForView BnbDataGridForView6;
		protected System.Web.UI.WebControls.Panel ContactBlockingTab;
		protected BonoboWebControls.DataGrids.BnbDataGridForView BnbDataGridForView8;
		protected System.Web.UI.WebControls.Panel SponsorshipTab;
		protected UserControls.Title titleBar;
		protected UserControls.MenuBar menuBar;
		protected System.Web.UI.WebControls.Panel DetailsTab;
		protected BonoboWebControls.DataControls.BnbTextBox Bnbtextbox2;
		protected BonoboWebControls.DataControls.BnbTextBox Bnbtextbox7;
		protected BonoboWebControls.DataControls.BnbTextBox Bnbtextbox8;
		protected BonoboWebControls.DataControls.BnbTextBox Bnbtextbox11;
		protected BonoboWebControls.DataControls.BnbDecimalBox BnbDecimalBox10;
		protected System.Web.UI.WebControls.Label Label3;
		
		protected BonoboWebControls.DataControls.BnbSearchIDComposite demoSearchIDComposite;
		
		protected Frond.UserControls.NavigationBar NavigationBar1;
		private BnbWebFormManager bnb;
		protected void Page_Load(object sender, System.EventArgs e)
		{

#if DEBUG			
			if(Request.QueryString.ToString() == "")
			{
				//this reloads the page with a specific ID when debugging
				BnbWebFormManager.ReloadPage("BnbAccountHeader=0152A263-00FA-11D6-ABEC-0008C7253E7E");
			}
#endif

			doEnsureApplicationInQueryString();

			// Put user code to initialize the page here
			bnb = new BnbWebFormManager(this,"BnbAccountHeader");
			bnb.LogPageHistory = true;
			bnb.PageNameOverride = "Payment";
			BnbWorkareaManager.SetPageStyleOfUser(this);
			
			BnbAccountHeader bnbAccountHeader= (BnbAccountHeader)bnb.ObjectTree.DomainObjectDictionary["BnbAccountHeader"];

            // VAF-587
            accountItemsBnbDataGridForView.NewLinkVisible = true;
            bonhyperGenerateRQ10.Visible = true;
			// pass criteria to address picker controls
			this.SetAddressPickerFilters();
			NavigationBar1.RootDomainObject = "BnbApplication";
			// fill in targeturl of generaterq10 hyper
			bonhyperGenerateRQ10.NavigateUrl = String.Format("PaymentDetailPage.aspx?BnbAccountHeader={0}&BnbAccountItem=New&PageState=New&GenerateRQ10=1",
				bnbAccountHeader.ID);
			
			BnbWorkareaManager.SetPageStyleOfUser(this);

			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;

				menuBar.SetOptionalFindParameter(1);
				//populate OverseasGrantTotalsTab

				if (!bnb.InNewMode)
				{
					Guid applicationID = Guid.Empty;
					if (Request.QueryString["BnbApplication"] != null && Request.QueryString["BnbApplication"] != "")
						applicationID = new Guid(Request.QueryString["BnbApplication"]);
					if (!BnbRuleUtils.HasValue(applicationID))
						applicationID = bnbAccountHeader.ApplicationID;
					if (!BnbRuleUtils.HasValue(applicationID))
						applicationID = bnbAccountHeader.Individual.CurrentApplication.ID;
					this.PopulateOverseasGrantTotalsTab(applicationID);
				}
			}
		}

		private void PopulateOverseasGrantTotalsTab(Guid applicationID)
		{

			DataRow dr=BnbAccountHeader.GetGrantPaymentsMade(applicationID).Rows[0];
			//set culture info (only for currency symbols)
			CultureInfo cultureInfo=new CultureInfo("en-GB");
			//header
			lblSOSWithType.Text=	dr["FormatSOSDateWithCode"].ToString();
			lblEOSWithType.Text = dr["FormatEOSDateWithCode"].ToString();
			lblMonthsTodayNum.Text = dr["MonthsToday"].ToString();
			lblMonthsAtEOSNum.Text = dr["MonthsAtEOS"].ToString();
			// first grid
			lblGridMonthsToday.Text = dr["MonthsToday"].ToString();
			lblGridEOSGrantAdvanceEntitlement.Text = string.Format("{0:F}",dr["EOSGrantAdvanceEntitlement"]);
			lblGridEOSGrantAdvancePaid.Text = string.Format("{0:F}",dr["EOSGrantAdvancePaid"]);
			lblGridEOSGrantAdvanceLiability.Text = string.Format("{0:F}",dr["EOSGrantAdvanceLiability"]);
			lblGridInServiceGrantEntitlementDesc.Text = dr["InServiceGrantEntitlementDesc"].ToString();
			lblGridInServiceGrantPaid.Text = string.Format("{0:F}",dr["InServiceGrantPaid"]);
			lblGridInServiceGrantLiability.Text = string.Format("{0:F}",dr["InServiceGrantLiability"]);
			lblGridExtendersGrantEntitlementCount.Text = dr["ExtendersGrantEntitlementCount"].ToString();
			lblGridExtendersGrantAlreadyPaidDesc.Text = dr["ExtendersGrantPaidDesc"].ToString();
			lblGridExtendersGrantLiabilityCount.Text = dr["ExtendersGrantLiabilityCount"].ToString();
			// second grid
			lblGridMonthsAtEOS.Text = dr["MonthsAtEOS"].ToString();
			lblGridEOSGrantFinalEntitlement.Text = string.Format("{0:F}",dr["EOSGrantFinalEntitlement"]);
			lblGridEOSGrantFinalPaid.Text = string.Format("{0:F}",dr["EOSGrantFinalPaid"]);
			lblGridEOSGrantFinalLiability.Text = string.Format("{0:F}",dr["EOSGrantFinalLiability"]);
			lblGridInServiceGrantFinalEntitlementDesc.Text = dr["InServiceGrantFinalEntitlementDesc"].ToString();
			lblGridInServiceGrantPaid2.Text = string.Format("{0:F}",dr["InServiceGrantPaid"]);
			lblGridInServiceGrantFinalLiability.Text = string.Format("{0:F}",dr["InServiceGrantFinalLiability"]);
			lblGridExtendersGrantFinalEntitlement.Text = dr["ExtendersGrantFinalEntitlementCount"].ToString();
			lblGridExtendersGrantAlreadyPaidDesc2.Text = dr["ExtendersGrantPaidDesc"].ToString();
			lblGridExtendersGrantFinalLiabilityCount.Text = dr["ExtendersGrantFinalLiabilityCount"].ToString();
			lblInServiceGrantRate.Text =  string.Format(cultureInfo,"{0} {1:C}",dr["InServiceGrantFinancialYear"],dr["InServiceGrantRate"]);
			lblEOSGrantFinalLiability2.Text = string.Format("{0:F}",dr["EOSGrantFinalLiability"]);
			lblEOSGrantCalcInServiceRepay.Text = string.Format("{0:F}",dr["EOSGrantCalcInServiceRepay"]);
			lblEOSGrantCalcExtendersRepay.Text = string.Format("{0:F}",dr["EOSGrantCalcExtendersRepay"]);
			lblEOSGrantCalcFinal.Text = string.Format("{0:F}",dr["EOSGrantCalcResult"]);
		}


		private void SetAddressPickerFilters()
		{
			Guid individualID = Guid.Empty;
			BnbApplication app = bnb.GetPageDomainObject("BnbApplication", true) as BnbApplication;
			
			individualID = app.IndividualID;

			BnbListCondition indvCond = new BnbListCondition("lnkIndividualAddress_IndividualID", individualID);
			BnbCriteria filterCrit = new BnbCriteria(indvCond);
			queryLookupPaymentAddress.FilterCriteria = filterCrit;
			queryLookupBankAddress.FilterCriteria = filterCrit;

		}

		protected void cmdPaymentVouchers_Clicked(object sender, System.EventArgs e)
		{
			string dir = AppDomain.CurrentDomain.BaseDirectory;
			BnbAccountHeader bnbAccHeader= (BnbAccountHeader)bnb.ObjectTree.DomainObjectDictionary["BnbAccountHeader"];	
			Guid accHeaderID = bnbAccHeader.ID;
			
			if (!BnbRuleUtils.HasValue(accHeaderID))
				accHeaderID = bnbAccHeader.ID;     
			DataTable dt = BnbAccountHeader.GetPaymentVoucherData(accHeaderID);
			if (dt.Rows.Count >0)
			{
				DataRow dr=dt.Rows[0];;

				if ((bool)dr["HasEOSGrantPayment"] ==true)
					//Set the Word Template file path to the control WordTemplate //property.
					cmdPaymentVouchers.WordTemplate = dir + @"Templates/OverseasPaymentVoucherRQ10.doc";
				else
					cmdPaymentVouchers.WordTemplate = dir + @"Templates/OverseasPaymentVoucher.doc";
				// set the control MergeData property with the created DataTable row.
				cmdPaymentVouchers.MergeData = dt;
				cmdPaymentVouchers.FilenameForUser = String.Format("PaymentVoucher{0}.doc",
					DateTime.Now.ToString("ddMMMyyyy-hhmmss"));

				// set up the section-protection - no longer required VAF-588
				//cmdPaymentVouchers.ProtectionPassword = "weeble";
				//cmdPaymentVouchers.SectionProtection = new bool[]{true, false, true, false, true};

				bool success = cmdPaymentVouchers.GenerateWordForm();
				if (success)
					BonoboWebControls.BnbWebFormManager.LogAction("Payment Page (Word Form)",
						"Generated Word Form.", this);
			}
			else
				cmdPaymentVouchers.ErrorMessage ="No payment data found";
      	}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


		private void doEnsureApplicationInQueryString()
		{	
			string appid = this.Request.QueryString["BnbApplication"];
			// if no app specified
			if (appid == null)
			{
				// then there must be an account header
				BnbAccountHeader acchead = BnbWebFormManager.StaticGetPageDomainObject("BnbAccountHeader") as BnbAccountHeader;
				BnbWebFormManager.ReloadPage(String.Format("BnbApplication={0}&BnbAccountHeader={1}",
					acchead.ApplicationID, acchead.ID));
			}
		}
	}
}
