<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WizardNewProject.aspx.cs" Inherits="Frond.EditPages.WizardNewProject" %>

<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="~/UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="~/UserControls/MenuBar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>New Project Wizard</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet" />
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
    <script type="text/javascript" language="javascript">
    function showChecklist()
    {   
        var radio = document.form1.elements['rbtnCheckList'];
        for(var i=0 ;i< radio.length;i++)
        {
        if(radio[i].checked)
        {
        var selectedvalue = radio[i].value;
        }
        }
        var programme = document.getElementById('programmeChecklist');
        var project = document.getElementById('projectChecklist'); 
        var pgmChecklist = programme.style.display;
        var pjtChecklist = project.style.display;
        if(selectedvalue == "Programme")
        {         
        if(pgmChecklist == 'none')
        programme.style.display = 'block';  
        if(pjtChecklist == 'block') 
        project.style.display = 'none';         
        }
        else
        {
        if(pjtChecklist == 'none')
        project.style.display = 'block';
        if(pgmChecklist == 'block')
        programme.style.display = 'none';
        }            
    }
    </script>
</head>
<body>
    <form id="form1" runat="server" method="post">
        <div>
            <div id="baseMenu">
                <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
            </div>
            <div id="baseContent">
                <asp:Panel ID="panelZero" runat="server">
                    <div class="wizardheader" ms_positioning="FlowLayout">
                        <h3>
                            <img class="icon24" src="../Images/wizard24silver.gif" alt="" />
                            New Project Wizard</h3>
                    </div>
                    <asp:Panel runat="server" CssClass="wizardpanel" ID="pnlCheckList">
                        <p>
                            <em>Is this project a "Programme" or a "Project"?</em></p>
                        <p>
                            <asp:RadioButtonList ID="rbtnCheckList" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Programme" Text="Programme"></asp:ListItem>
                                <asp:ListItem Value="Project" Text="Project"></asp:ListItem>
                            </asp:RadioButtonList>
                        </p>
                        <asp:Label ID="lblPanelZeroFeedback" runat="server" CssClass="feedback"></asp:Label>
                    </asp:Panel>
                    <div id="programmeChecklist" style="display:none;">
                    <asp:Panel ID="pnlProgrammeChecklist" runat="server" CssClass="wizardpanel">
                        <br />
                        <p style="text-align: center; font-size: 15px">
                            <b>New Programme Checklist</b></p>
                        <br />
                        <p class="font">
                            It will take approximately 15 minutes to initiate a project into PGMS. To assist
                            please make sure you have the following information in hand.</p>
                        <ul class="tick">
                            <li class="style">Do you have a copy of your programme plan?</li>
                            <li class="style">Do you have your programme budget available? </li>
                            <li class="style">Do you have a list of the volunteer skills planned for the programme?</li>
                        </ul>
                    </asp:Panel> 
                    </div>
                    <div id="projectChecklist" style="display:none;">
                    <asp:Panel ID="pnlProjectChecklist" runat="server" CssClass="wizardpanel">
                        <br />
                        <p style="text-align: center; font-size: 15px">
                            <b>New Project Checklist</b></p>
                        <br />
                        <p class="font">
                            It will take approximately 15 minutes to initiate a project into PGMS. To assist
                            please make sure you have the following information in hand.</p>
                        <ul class="tick">
                            <li class="style">Do you have the completed project narrative?</li>
                            <li class="style">Do you have the completed Programme budget? </li>
                            <li class="style">Do you have a list of the international volunteer placement types and numbers? </li>
                        </ul>
                    </asp:Panel>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panelOne" runat="server">
                    <div class="wizardheader" ms_positioning="FlowLayout">
                        <h3>
                            <img class="icon24" src="../Images/wizard24silver.gif" alt="" />
                            New Project Wizard</h3>
                    </div>
                    <asp:Panel ID="pnlProjectTitle" runat="server" CssClass="wizardpanel">
                        <p>
                            <em>This wizard will help generate a new project record.</em></p>
                        <table id="Table1" class="wizardtable">
                            <tr>
                                <td class="label" style="height: 20px">
                                    Project Title</td>
                                <td style="height: 20px">
                                    <asp:TextBox ID="txtProjectTitle" runat="server" Width="300px" MaxLength="50"></asp:TextBox></td>
                            </tr>
                        </table>
                        <asp:Label ID="lblPanelOneFeedback" runat="server" CssClass="feedback"></asp:Label>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="panelTwo" runat="server">
                    <div class="wizardheader" ms_positioning="FlowLayout">
                        <h3>
                            <img class="icon24" src="../Images/wizard24silver.gif" alt="" />
                            New Project Wizard</h3>
                    </div>
                    <asp:Panel ID="dupProjectTitle" runat="server">
                        <p>
                            <em>The following project name already exists. If any of the projects below <b>match
                                the new project</b> that you are going to create, then please use them instead by
                                clicking on the appropriate Radiobutton.</em></p>
                        <asp:DataGrid ID="MatchTitleGrid" runat="server" CssClass="datagrid" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:RadioButton ID="rbtnProject" runat="server" AutoPostBack="true" OnCheckedChanged="doViewProject" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="tblProject_ProjectID" Visible="false" HeaderText="HiddenID">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="tblProject_ProjectTitle" HeaderText="Project Title"></asp:BoundColumn>
                                <asp:HyperLinkColumn Text="View Project" DataNavigateUrlField="tblProject_ProjectID"
                                    DataNavigateUrlFormatString="ProjectPage.aspx?BnbProject={0}"></asp:HyperLinkColumn>
                            </Columns>
                        </asp:DataGrid>
                        <p>
                            If none of these existing projects match, then press Next to create new Project.</p>
                        <asp:Button ID="btnClear" Text="Clear" OnClick="btnClear_Click" runat="server" Font-Bold="True"
                            Height="25px" Width="75px" Font-Size="Small" />
                    </asp:Panel>
                    <p>
                        <asp:Label ID="labelNoDuplicates" runat="server">No similar Projects already exist. Press Next to continue.</asp:Label></p>
                    <asp:Label ID="lblPanelTwoFeedback" runat="server" CssClass="feedback"></asp:Label>
                </asp:Panel>
                <asp:Panel runat="server" ID="panelThree">
                    <div class="wizardheader" ms_positioning="FlowLayout">
                        <h3>
                            <img class="icon24" src="../Images/wizard24silver.gif" alt="" />
                            New Project Wizard - Project Programme</h3>
                    </div>
                    <br />
                    <table id="tblMain" class="wizardtable">
                        <tr>
                            <td class="label" style="width: 91px">
                                Project Title</td>
                            <td>
                                <asp:TextBox ID="txtProjectTitleStep3" Width="300px" runat="server" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="Required field cannot be left blank"
                                    ControlToValidate="txtProjectTitleStep3" /></td>
                            <td class="label">
                                VSO Goal</td>
                            <td>
                                <asp:DropDownList ID="drpVsoGoal" runat="server" Width="300px" Height="20px" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="Required field cannot be left blank."
                                    ControlToValidate="drpVsoGoal" /></td>
                        </tr>
                        <tr>                            
                            <td class="label" style="width: 91px">
                            Project Region</td>
                             <td>
                            <asp:DropDownList ID="drpProjectRegion" runat="server" Width="300px" Height="20px">
                            </asp:DropDownList>
                            <br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="Required field cannot be left blank."
                                ControlToValidate="drpProjectRegion" /></td>
                        </tr>
                        <tr>                           
                            <td class="label" style="height: 25px">
                                Project Status</td>
                            <td>
                                <asp:DropDownList ID="drpProjectStatus" runat="server" Width="300px" Height="20px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="Required field cannot be left blank."
                                    ControlToValidate="drpProjectStatus" /></td>
                        </tr>
                        <tr>
                            <td class="label" style="width: 91px">
                                Country Office</td>
                            <td>
                                <asp:DropDownList ID="drpProgrammeOffice" runat="server" Width="300px" Height="20px">
                                </asp:DropDownList>
                                <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="Required field cannot be left blank."
                                    ControlToValidate="drpProgrammeOffice" /></td>
                              
                        </tr>
                        <tr>
                            <td class="label" style="width: 91px">
                                Project Accountable</td>
                            <td>
                                <asp:DropDownList ID="drpProjectAccountable" runat="server" Width="300px" Height="20px">
                                </asp:DropDownList><br />
                                <asp:RequiredFieldValidator ID="validateAccountable" runat="server" ControlToValidate="drpProjectAccountable"
                                    ErrorMessage="Required field cannot be left blank" />                                
                            </td>
                            
                        </tr>
                        <tr>
                            <td class="label">
                                Narrative Summary</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtNarrative" runat="server" Width="100%" Rows="4" TextMode="MultiLine"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="Required field cannot be left blank."
                                    ControlToValidate="txtNarrative" /></td>
                        </tr>
                     
                    </table>  
                     <br />
                    <asp:ValidationSummary runat="server" ID="ValidationSummary1" CssClass="valsummary" 
                    HeaderText="It seems you have not entered the appropriate information in the mandatory fields.Please fill in these fields correctly in order to proceed." />                   
                </asp:Panel>
                <asp:Panel ID="panelFour" runat="server">
                    <div class="wizardheader" ms_positioning="FlowLayout">
                        <h3>
                            <img class="icon24" src="../Images/wizard24silver.gif" alt="" />
                            New Project Wizard - Project Programme - Financial/Risk Information</h3>
                    </div>
                    <br />
                    <asp:Panel ID="pnlPjtProgrammeFinancialInfo" runat="server">
                        <table class="wizardtable" id="tblProjectCost" runat="server" style="height: 100px">
                            <tr>
                                <th class="label" width="50%">
                                    PROJECT TITLE</th>
                                <th class="label" width="50%">
                                    PROJECT  TOTAL COST&nbsp;(�)</th>
                            </tr>
                            <tr>
                                <th width="50%" style="text-align:center">
                                    <asp:Label ID="lblProjectTitleStep4" runat="server" Width="300px"></asp:Label></th>
                                <th width="50%">
                                    <p style=" font-size:10px;"><asp:TextBox ID="txtProjectTotalCost" runat="server" Width="150px" MaxLength="20"></asp:TextBox><br />(Accepts a valid amount with two decimal places. e.g.,12345.00)</p>                                   
                                    <asp:RequiredFieldValidator ID="validateProjectTotalCost" runat="server" ControlToValidate="txtProjectTotalCost"
                                        ErrorMessage="Required field cannot be left blank" /><br />
                                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtProjectTotalCost"
                                        MinimumValue="0" Type="Currency" MaximumValue="99999999999999" ErrorMessage="Please enter a valid amount" /></th>
                            </tr>
                        </table>
                        <br />
                        <br />
                        <asp:Panel ID="pnlRiskDetails" runat="server" Visible="false" >                        
                        <table id="tblRiskCategory" runat="server" class="wizardtable" style="width: 830px">
                            <tr>
                                <th class="label">
                                    RISK CATEGORY</th>
                                <th class="label">
                                    RISK TYPE</th>
                            </tr>
                             <tr>
                                <td class="label" style="width: 40%">
                                    <p class="paragraph">
                                        <b class="bold">Match funding requirement</b>
                                    </p>
                                </td>
                                <td style="width: 60%">
                                    <asp:DropDownList ID="drpMatchFunding" runat="server" Width="100%">
                                    </asp:DropDownList><br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Required field cannot be left blank"
                                        ControlToValidate="drpMatchFunding" /></td>
                            </tr> 
                                                       
                            <tr>
                                <td class="label" style="width: 40%">
                                    <p class="paragraph">
                                        <b class="bold">Financial Feasibility (see PTC)</b></p>
                                </td>
                                <td style="width: 60%">
                                    <asp:DropDownList ID="drpProjectFeasibility" runat="server" Width="100%">
                                    </asp:DropDownList><br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required field cannot be left blank"
                                        ControlToValidate="drpProjectFeasibility" /></td>
                            </tr>
                            <tr>
                                <td class="label" style="width: 40%">
                                    <p class="paragraph">
                                        <b class="bold">Grant management experience</b>
                                    </p>
                                </td>
                                <td style="width: 60%">
                                    <asp:DropDownList ID="drpPOCapacity" runat="server" Width="100%">
                                    </asp:DropDownList><br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Required field cannot be left blank"
                                        ControlToValidate="drpPOCapacity" /></td>
                            </tr>                            
                            <tr>
                                <td class="label" style="width: 40%">
                                    <p class="paragraph">
                                        <b class="bold">Alignment to strategy</b>
                                    </p>
                                </td>
                                <td style="width: 60%">
                                    <asp:DropDownList ID="drpOperPlans" runat="server" Width="100%">
                                    </asp:DropDownList><br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required field cannot be left blank"
                                        ControlToValidate="drpOperPlans" /></td>
                            </tr>
                            <tr>
                                <td class="label" style="width: 40%; height: 25px;">
                                    <p class="paragraph">
                                        <b class="bold">Volunteer Recruitment</b>
                                    </p>
                                </td>
                                <td style="width: 60%; height: 25px;">
                                    <asp:DropDownList ID="drpVolRecruit" runat="server" Width="100%" Height="20px">
                                    </asp:DropDownList><br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Required field cannot be left blank"
                                        ControlToValidate="drpVolRecruit" /></td>
                            </tr>
                            <tr>
                                <td class="label" style="width: 40%">
                                    <p class="paragraph">
                                        <b class="bold">Contract documentation</b>
                                    </p>
                                </td>
                                <td style="width: 60%">
                                    <asp:DropDownList ID="drpContractCondn"  ToolTip="item" runat="server" Width="100%">
                                    </asp:DropDownList><br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Required field cannot be left blank"
                                        ControlToValidate="drpContractCondn" />
                                       </td>
                            </tr>
                            <tr>
                                <td class="label" style="width: 40%">
                                    <p class="paragraph">
                                        <b class="bold">Comments on risks above</b></p>
                                </td>
                                <td style="width: 60%">
                                    <asp:TextBox ID="txtRiskComments" runat="server" Width="100%" Rows="3" TextMode="multiLine"></asp:TextBox>
                                    <br />
                                    </td>
                            </tr>
                            <tr>
                                <td class="label" width="40%">
                                    <p class="paragraph">
                                        <b class="bold">Overall Project Risk Assessment </b>
                                    </p>
                                </td>
                                <td width="60%">
                                    <asp:DropDownList ID="drpOverallRisk" runat="server" Width="300px">
                                    </asp:DropDownList><br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required field cannot be left blank"
                                        ControlToValidate="drpOverallRisk" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label" width="40%">
                                    <p class="paragraph">
                                        <b class="bold">Has a more detailed risk assessment of this project been discussed with CD and RD?</b>
                                    </p>
                                </td>
                                <td width="60%">
                                    <asp:RadioButtonList id="rblRiskAssessmentCompleted" RepeatDirection="Horizontal" runat="server">
                                    <asp:ListItem Text="Yes" Value="1000"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="1001"></asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required field cannot be left blank"
                                        ControlToValidate="rblRiskAssessmentCompleted" />
                                </td>
                            </tr>
                        </table>
                        <br />  
                        </asp:Panel>  
                        <asp:ValidationSummary runat="server" ID="ValidationSummary2" CssClass="valsummary" 
                            HeaderText="It seems you have not entered the appropriate information in the mandatory fields.Please fill in these fields correctly in order to proceed." />                  
                        <asp:Label ID="lblPanelFourFeedback" runat="server" CssClass="feedback"></asp:Label>
                    </asp:Panel>
                </asp:Panel>
               <p>
                    <uc1:WizardButtons ID="wizardButtons" runat="server"></uc1:WizardButtons>
                </p>
                <p>
                    <asp:Label ID="labelHiddenRequestIDUnused" runat="server" Visible="False"></asp:Label>
                    <asp:TextBox ID="txtHiddenReferrer" runat="server" Visible="False"></asp:TextBox></p>
            </div>
        </div>
    </form>
</body>
</html>
