using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboDomainObjects;
using BonoboWebControls;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for BenefitPage.
	/// </summary>
	public partial class BenefitPage : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
		protected Frond.UserControls.NavigationBar NavigationBar1;

//		BnbWebFormManager bnb = null;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			BnbWebFormManager bnb = null;
#if DEBUG	
//			bnb = null;
			if (Request.QueryString.ToString() == "")
				BnbWebFormManager.ReloadPage("BnbIndividual=CADE27ED-C10A-4998-9B3C-CF2315B8001D&BnbIndividualBenefit=8CAE90D9-0580-47B9-BC46-06D911EFED41");
			else
				bnb = new BnbWebFormManager(this,"BnbIndividualBenefit");
#else	
			bnb = new BnbWebFormManager(this,"BnbIndividualBenefit");
#endif
			bnb.LogPageHistory = true;
			BnbWorkareaManager.SetPageStyleOfUser(this);
			// tweak page title settings
			bnb.PageNameOverride = "Benefits";
			NavigationBar1.RootDomainObject = "BnbIndividual";
			if (!this.IsPostBack)
			{
				titleBar.TitleText = "Benefits";
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
