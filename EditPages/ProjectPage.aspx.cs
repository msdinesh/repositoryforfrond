using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using BonoboWebControls;
using BonoboWebControls.GenericControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using System.Text;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for WebForm2.
	/// </summary>
	public partial class ProjectPage : System.Web.UI.Page
	{
        // removed for upgrade to .net 2
		//protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox2;
		protected BonoboWebControls.DataControls.BnbDecimalBox BnbDecimalBox1;
		protected BonoboWebControls.DataGrids.BnbDataGridForDomainObjectList BnbDataGridForDomainObjectList1;
	
		private BnbWebFormManager bnb = null;

        protected void Page_Load(object sender, System.EventArgs e)
        {
#if DEBUG
            if (Request.QueryString.ToString() == "")
            {//http://localhost/PGMS/EditPages/ProjectPage.aspx.cs
                //this reloads the page with an ID for an employer
                BnbWebFormManager.ReloadPage("BnbProject=D6D3620F-5363-46E3-A31A-0793ED15F840");
            }
#endif
            bnb = new BnbWebFormManager(this, "BnbProject");
            BnbProject objProject = bnb.GetPageDomainObject("BnbProject") as BnbProject;
            //txtduration.ErrorMessage = "Please enter the number of months in the Duration field";
            //TPT: Redirect to new project wizard in New mode
            if (this.Request.QueryString["BnbProject"] != null &&
                this.Request.QueryString["BnbProject"].ToLower() == "new")
            {
                doRedirectToProjectWizard();
            }
            
            bnb.LogPageHistory = true;
            pnlVSOGoals.Visible = !bnb.InNewMode;
            pnlNewModeProgrammeOfficeAndGoal.Visible = bnb.InNewMode;
            
            CheckProjectStatus();
            SetDMSSearch();
            doPMFStatus();
            doClaimsAndReports();
            doProposalToContractHyperlink();
            doContractReportHyperlink();
            bnb.EditModeEvent += new EventHandler(EditModeEvent_Click);
            bnb.SaveEvent += new EventHandler(SaveEvent_Click);
            chkBxPriority.Attributes.Add("Onclick", "javascript:showHidePriority();");

            //tweak logging settings
            bnb.PageNameOverride = "Project";
            bnb.LogPageHistory = true;
            BnbWorkareaManager.SetPageStyleOfUser(this);
            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null && objProject != null)
                {
                    StringBuilder sb = new StringBuilder(Utilities.ProjectHlink);
                    sb.Replace("[PROJECTID]", objProject.ID.ToString());
                    sb.Replace("[TITLE]", objProject.ProjectTitle);
                    sb.Replace("[SUBTITLE]", string.Empty);
                    titleBar.TitleText = sb.ToString();
                }
                if (objProject != null)
                    lblProjecttitle.Text = objProject.ProjectTitle;

                menuBar.SetOptionalFindParameter(17);

                // work out which tab to show based on where we came from
                if (this.Request.UrlReferrer != null)
                {
                    string referrer = this.Request.UrlReferrer.AbsolutePath;
                    if (referrer.EndsWith("WizardGeneratePMPGForms.aspx"))
                        uxTabControl.SelectedTabID = PMPGDocumentsTab.ID;
                    
                }
                // TPT Amended code PGMS - 3.3.1
                //Identify the url and show the related tab when the user email the page
                if (this.Request != null)
                {
                    string url = this.Request.Url.ToString();
                    if (url.IndexOf("ControlID") > -1)
                    {
                        if (url.IndexOf("Main") > -1)
                            uxTabControl.SelectedTabID = Main.ID;
                        if (url.IndexOf("Risk") > -1)
                            uxTabControl.SelectedTabID = ProjectRisk.ID;
                        if (url.IndexOf("Financing") > -1)
                            uxTabControl.SelectedTabID = ProjectFinancing.ID;
                        if (url.IndexOf("DMSDocuments") > -1)
                            uxTabControl.SelectedTabID = DMSDocuments.ID;
                        if (url.IndexOf("DocumentsTab") > -1)
                            uxTabControl.SelectedTabID = PMPGDocumentsTab.ID;
                    }
                }

                if ((Session["isSignOff"]) != null)
                {
                    Session["isSignOff"] = null;
                    uxTabControl.SelectedTabID = PMPGDocumentsTab.ID;
                }

                if (objProject != null)
                {
                    if (objProject.ProjectPriority)
                        chkBxPriority.Checked = objProject.ProjectPriority;
                    if (objProject.SeekingFunding)
                        chkBxSeekingFunding.Checked = objProject.SeekingFunding;
                }
                doAuditFillCombo();
                doSetLabelText("Project General");
                doSetAuditGridProperty("vwProjectGeneralAudit");
                doFillGrantCodeList();
                //PGMSMIFI-131
                doNewProposalLink();
                projectTitle();
            }

        }

        private void doFillGrantCodeList()
        {
            BnbProject project = bnb.GetPageDomainObject("BnbProject") as BnbProject;
            BnbTextCondition con = new BnbTextCondition("ProjectID", project.ID.ToString(), BnbTextCompareOptions.ExactMatch);
            BnbCriteria criteria = new BnbCriteria(con);
            BnbDGFVGrantCodelist.Criteria = criteria;
            BnbDGFVGrantCodelist.DataBind();

        }

        private void gridViewPaymentSchedule()
        {
            BnbProject objProject = bnb.GetPageDomainObject("BnbProject") as BnbProject;
            BnbTextCondition con = new BnbTextCondition("tblProject_ProjectID", objProject.ID.ToString(), BnbTextCompareOptions.ExactMatch);
            BnbCriteria criteria = new BnbCriteria(con);
            BnbQuery query = new BnbQuery("vwBonoboPaymentSchedule", criteria);
            BnbQueryDataSet ds = query.Execute();
            DataTable dt = ds.Tables["Results"];
            gvPaymentSchedule.DataSource = dt;
            gvPaymentSchedule.DataBind();
        }

        private void doRedirectToProjectWizard()
        {
            if (this.Request.QueryString["BnbProject"] != null &&
                this.Request.QueryString["BnbProject"].ToLower() == "new")
                Response.Redirect("WizardNewProject.aspx");
        }

        private void doSetAuditGridProperty(string viewName)
        {
            BnbDataGridForView7.ViewName = viewName;
        }
        private void doSetLabelText(string headingVal)
        {
            lblAuditHeading.Text = "Action: " + headingVal;
        }
        private void doAuditFillCombo()
        {
            DataTable dt = BnbEngine.LookupManager.GetLookup("lkuProjectChildReference");
            AuditSelectList1.DataSource = dt;
            dt.DefaultView.RowFilter = "Exclude = 0";
            dt.DefaultView.Sort = "DisplayOrder";
            AuditSelectList1.DataTextField = "AliasName";
            AuditSelectList1.DataValueField = "IntID";
            AuditSelectList1.DataBind();

        }

        private void doPMFStatus()
        {
            BnbProject proj = bnb.GetPageDomainObject("BnbProject") as BnbProject;
            if (proj != null)
            {
                PMFStatusPanel2.PMFStatusID = proj.PMFStatusID.ToString();
                PMFStatusPanel2.ProjectTypeID = proj.ProjectTypeID;
            }
            else
            {
                // for a new project, hide the pmfstatuspanel
                PMFStatusPanel2.Visible = false;
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			this.BnbDataButtons3.UndoClick+=new EventHandler(BnbDataButtons1_UndoClick);
			this.BnbDataGridForView1.PreRender+=new EventHandler(BnbDataGridForView1_PreRender);
			this.BnbDataGridForView2.PreRender+=new EventHandler(BnbDataGridForView2_PreRender);
            this.BnbDataGridForView4.PreRender += new EventHandler(BnbDataGridForView4_PreRender);
            this.BnbDataGridForView5.PreRender += new EventHandler(BnbDataGridForView5_PreRender);
			//<!-- PGM-197, PGM-236
			//this.bnbdgProgrammeOffices.PreRender+=new EventHandler(bnbdgProgrammeOffices_PreRender);
			//-->
			dmsSearchControl.DocumentViewClick +=new BonoboWebControls.DMSControls.BnbDMSSearchControl.DocumentEventHandler(dmsSearchControl_DocumentViewClick);
			dmsSearchControl.DocumentMoreClick +=new BonoboWebControls.DMSControls.BnbDMSSearchControl.DocumentEventHandler(dmsSearchControl_DocumentMoreClick);
			dmsSearchControl.NewDocumentClick +=new EventHandler(dmsSearchControl_NewDocumentClick);
            // second search control binds to same event handlers
            //dmsSearchControlPMPG.DocumentViewClick += new BonoboWebControls.DMSControls.BnbDMSSearchControl.DocumentEventHandler(dmsSearchControl_DocumentViewClick);
            //dmsSearchControlPMPG.DocumentMoreClick += new BonoboWebControls.DMSControls.BnbDMSSearchControl.DocumentEventHandler(dmsSearchControl_DocumentMoreClick);
            //dmsSearchControlPMPG.NewDocumentClick += new EventHandler(dmsSearchControl_NewDocumentClickPMF);
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void CheckProjectStatus()
		{
			BnbProject project = (BnbProject)bnb.ObjectTree.DomainObjectDictionary["BnbProject"];
			if(project == null) return;
			if (project.EndDate <= DateTime.Today && project.CurrentProjectStatus != null)
			{
				if (project.CurrentProjectStatus.ProjectStatusID != BnbConst.ProjectStatus_Closed &&
					project.CurrentProjectStatus.ProjectStatusID != BnbConst.ProjectStatus_Abandoned)
					lblProjectStatus.Text = "Please update Project End Date or close project.";
			}
		}

        private void SetDMSSearch()
        {
            BnbProject project = (BnbProject)bnb.ObjectTree.DomainObjectDictionary["BnbProject"];
            if (project == null) return;
            if (project.ProjectReference == null) return;

            uxGeneratePMPGHyper.NavigateUrl = String.Format("WizardGeneratePMPGForms.aspx?BnbProject={0}",
                    project.ID.ToString());

            string dmsSearchUsername = ConfigurationSettings.AppSettings["DMSSearchUsername"];
            string dmsSearchPassword = ConfigurationSettings.AppSettings["DMSSearchPassword"];

            string dmsUser = BnbEngine.SessionManager.GetSessionInfo().LoginName.ToUpper();
            string dmsDept = GetDepartmentDMSAlias();

            dmsSearchControlPMPG.ADUser = dmsSearchUsername;
            dmsSearchControlPMPG.ADPassword = dmsSearchPassword;
            dmsSearchControlPMPG.ProjectReference = project.ProjectReference;
            dmsSearchControlPMPG.DocDescription = "PMPG";
            dmsSearchControlPMPG.NewDocQueryArguments = String.Format("&ProjectRef={0}&ProjectTitle={1}&DocDescription={2}&User={3}&Dept={4}&Application={5}&DocType={6}",
                project.ProjectReference, Server.UrlEncode(project.ProjectTitle), "PMPG", dmsUser, dmsDept, "WORD", "GENERAL");
            dmsSearchControlPMPG.ShowWebDMSViewLinks = true;
            dmsSearchControlPMPG.ShowWebDMSMoreLinks = (BnbEngine.SessionManager.GetSessionInfo().HasPermission(
                BnbConst.Permission_PGMSAdditionalDMSLinks));

            dmsSearchControl.ADUser = dmsSearchUsername;
            dmsSearchControl.ADPassword = dmsSearchPassword;
            dmsSearchControl.ProjectReference = project.ProjectReference;
            dmsSearchControl.NewDocQueryArguments = String.Format("&ProjectRef={0}&ProjectTitle={1}",
                project.ProjectReference, Server.UrlEncode(project.ProjectTitle));
            dmsSearchControl.DocDescriptionSuppress = "PMPG";
            dmsSearchControl.ShowWebDMSViewLinks = true;
            dmsSearchControl.ShowWebDMSMoreLinks = (BnbEngine.SessionManager.GetSessionInfo().HasPermission(
                BnbConst.Permission_PGMSAdditionalDMSLinks));
        }

        /// <summary>
        /// for the logged in user, returns the DMS Alias from appDepartment
        /// (if specified)
        /// otherwise returns empty string
        /// </summary>
        /// <returns></returns>
        public static string GetDepartmentDMSAlias()
        {
            string dmsDept = "";
            BnbLookupDataTable deptTable = BnbEngine.LookupManager.GetLookup("appDepartment");
            BnbLookupRow deptRow = deptTable.FindByID(BnbEngine.SessionManager.GetSessionInfo().DepartmentID);
            if (deptRow != null && deptRow["DMSAlias"] != DBNull.Value)
                dmsDept = deptRow["DMSAlias"].ToString();
            return dmsDept;
        }
		
		
		private void BnbDataButtons1_UndoClick(object sender, EventArgs e)
		{
			if(Request.QueryString["DisableRedirect"] == "true") 
			{
				Response.Redirect("../FindGeneral.aspx?PageState=Undo");
				Response.End();
			}
            BnbProject objProject = bnb.GetPageDomainObject("BnbProject") as BnbProject;
            if (objProject != null)
                chkBxPriority.Checked = objProject.ProjectPriority;

            chkBxPriority.Enabled = false;
            chkBxSeekingFunding.Enabled = false;
            projectTitle();
		}

        //save event for to save Priority checkbox
        private void SaveEvent_Click(object sender, EventArgs e)
        {
            BnbProject objProject = bnb.GetPageDomainObject("BnbProject") as BnbProject;
            if (objProject != null)
            {
                objProject.ProjectPriority = chkBxPriority.Checked;
                objProject.SeekingFunding = chkBxSeekingFunding.Checked;
            }
        }
        //Resctriction to edit Project Priority checkbox(only PGMS user can edit)
        private void EditModeEvent_Click(object sender, EventArgs e)
        {
            BnbProject objProjectToEdit = bnb.GetPageDomainObject("BnbProject") as BnbProject;
            chkBxSeekingFunding.Enabled = true;
            if (objProjectToEdit != null && objProjectToEdit.AccessControl_EditProjectPriority())
                chkBxPriority.Enabled = true;
            else
            {
                BnbProject newProject = new BnbProject();
                if (newProject.IsNew && newProject.AccessControl_EditProjectPriority())
                    chkBxPriority.Enabled = true;
            }

            if (objProjectToEdit.ProjectRisk != null && (objProjectToEdit.ProjectRisk.MatchFundingID == BnbDomainObject.NullInt))
                lkupGrantMatchFunding.SelectedValue = BnbConst.RiskType_NotApplicable;

            if (objProjectToEdit.ProjectRisk != null && (objProjectToEdit.ProjectRisk.ContractualConditionsID == BnbDomainObject.NullInt))
                lkupContractualCondition.SelectedValue = BnbConst.RiskType_NotApplicable;
        }
        //Redirect to Project DocumentTab to upload new Document
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            uxTabControl.SelectedTabID = DMSDocuments.ID;
            if (chkBxPriority.Enabled)
            {
                BnbProject objProjectToEdit = bnb.GetPageDomainObject("BnbProject") as BnbProject;
                if (objProjectToEdit != null && objProjectToEdit.AccessControl_EditProjectPriority())
                    chkBxPriority.Checked = objProjectToEdit.ProjectPriority;
                chkBxPriority.Enabled = false;
            }
        }

		private void BnbDataGridForView1_PreRender(object sender, EventArgs e)
		{
			//<! PGM-206
			string colname = BnbDataGridForView1.Columns.GetColumnName("Contract Amount (�)");
			BnbDataGridForView1.Columns[colname].FormatExpression = "#,0";
			//-->
            // TPT amended PGMSSRONE-8 to show comma's in Grant amount currenty field in UI
            string grantAmount = BnbDataGridForView1.Columns.GetColumnName("Grant Amount (curr)");
            BnbDataGridForView1.Columns[grantAmount].FormatExpression = "#,0";
		}

		

		private void BnbDataGridForView2_PreRender(object sender, EventArgs e)
		{
			BnbDataGridForView2.Columns["tblGrant_AmountRequestedSterling"].FormatExpression = "#,0";
		}

        private void BnbDataGridForView4_PreRender(object sender, EventArgs e)
        {
            //<! PGMS Claim View
            BnbDataGridForView4.Columns["tblProject_TotalCost"].FormatExpression = "#,0";
            BnbDataGridForView4.Columns["Custom_DonorAmount"].FormatExpression = "#,0";            
            BnbDataGridForView4.Columns["Custom_MatchedFundingRequirement"].FormatExpression = "#,0";
            //-->
        }

        private void BnbDataGridForView5_PreRender(object sender, EventArgs e)
        {
            //<! PGMS Claim View
            BnbDataGridForView5.Columns["tblGrant_ProjectTotalCost"].FormatExpression = "#,0";
            BnbDataGridForView5.Columns["tblGrant_DonorBudgetAmountCurrency"].FormatExpression = "#,0";
            BnbDataGridForView5.Columns["tblGrant_AmountRequestedCurrency"].FormatExpression = "#,0";
            BnbDataGridForView5.Columns["Custom_MatchedFundingRequirement"].FormatExpression = "#,0";
            //-->
        }

        protected void gvPaymentSchedule_OnPreRender(object sender, EventArgs e)
        {
            GridView grid = sender as GridView;
            gridViewPaymentSchedule();
            if (grid.Rows.Count != 0)
            {
                GridViewRow row = new GridViewRow(0, 1, DataControlRowType.Header, DataControlRowState.Normal);
                TableCell cell0 = new TableHeaderCell();
                row.Cells.Add(cell0);
                //PGMSRONE-12
                TableCell cell1 = new TableHeaderCell();
                row.Cells.Add(cell1);

                TableCell cell2 = new TableHeaderCell();

                cell2.ColumnSpan = 2;
                cell2.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
                cell2.Style.Add(HtmlTextWriterStyle.FontWeight, "bold");
                cell2.Text = "Claim Amount";
                row.Cells.Add(cell2);

                TableCell cell3 = new TableHeaderCell();
                cell3.ColumnSpan = 6;
                cell3.Text = "Claim Due";
                cell3.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
                cell3.Style.Add(HtmlTextWriterStyle.FontWeight, "bold");
                row.Cells.Add(cell3);

                TableCell cell4 = new TableHeaderCell();
                cell4.ColumnSpan = 5;
                cell4.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
                cell4.Style.Add(HtmlTextWriterStyle.FontWeight, "bold");
                cell4.Text = "Amount Received";
                row.Cells.Add(cell4);

                Table t = grid.Controls[0] as Table;
                if (t != null)
                    t.Rows.AddAt(0, row);
            }
        }

		//<!-- PGM-197, PGM-236
        //private void bnbdgProgrammeOffices_PreRender(object sender, EventArgs e)
        //{
        //    BnbProject project = (BnbProject)bnb.ObjectTree.DomainObjectDictionary["BnbProject"];
        //    if(project == null) return;
        //    lblRegions.Text = project.GetRegionDescriptions();
        //}
		//-->

		private void dmsSearchControl_DocumentViewClick(object sender, string docNumber, string docVersion)
		{
			Response.Redirect(String.Format("../Redirect.aspx?TransferTo=VsoDms&N={0}&V={1}",
							docNumber,
							docVersion));
		}

		private void dmsSearchControl_DocumentMoreClick(object sender, string docNumber, string docVersion)
		{
			Response.Redirect(String.Format("../Redirect.aspx?TransferTo=VsoDms&DocNum={0}&V={1}",
				docNumber,
				docVersion));

		}

		private void dmsSearchControl_NewDocumentClick(object sender, EventArgs e)
		{
			BnbProject project = (BnbProject)bnb.ObjectTree.DomainObjectDictionary["BnbProject"];
			string projRef = project.ProjectReference;
			Response.Redirect(String.Format("../Redirect.aspx?TransferTo=VsoDms&AddDoc=1&ProjectRef={0}",
				projRef));
		}

        private void dmsSearchControl_NewDocumentClickPMF(object sender, EventArgs e)
        {
            BnbProject project = (BnbProject)bnb.ObjectTree.DomainObjectDictionary["BnbProject"];
            string projRef = project.ProjectReference;
            Response.Redirect(String.Format("../Redirect.aspx?TransferTo=VsoDms&AddDoc=1&ProjectRef={0}&DocDescription={1}",
                projRef,"PMPG"));
        }

        private void doProposalToContractHyperlink()
        {
            string s = bnb.GetQueryStringFromCurrentPageWithDomainObjectKeyValuesOnly();
            BonoboWebControls.DataGrids.BnbDataGridHyperlink hlProposalToContract = new BonoboWebControls.DataGrids.BnbDataGridHyperlink();
            hlProposalToContract.ColumnNameToRenderControl = "tblGrant_ProposalToContract";
            hlProposalToContract.HrefTemplate = "WizardProposalToContract.aspx?" + s + "&BnbGrant={0}&BnbGrantProgress=new&PageState=new&DisableRedirect=true&Wizard=True";
            hlProposalToContract.KeyColumnNames = new string[] { "tblGrant_GrantID" };
            BnbDataGridForView2.ColumnControls.Add(hlProposalToContract);
        }

        private void doClaimsAndReports()
        {
            string s = bnb.GetQueryStringFromCurrentPageWithDomainObjectKeyValuesOnly();
            BonoboWebControls.DataGrids.BnbDataGridHyperlink claimsAndReportsLink = new BonoboWebControls.DataGrids.BnbDataGridHyperlink();
            claimsAndReportsLink.ColumnNameToRenderControl = "tblGrant_ClaimsAndReports";
            claimsAndReportsLink.HrefTemplate = "ContractPage.aspx?" + s + "&BnbGrant={0}&ControlID=Claims";
            claimsAndReportsLink.KeyColumnNames = new string[] { "tblGrant_GrantID" };
            BnbDataGridForView1.ColumnControls.Add(claimsAndReportsLink);
        }

        private void doContractReportHyperlink()
        {
            string s = bnb.GetQueryStringFromCurrentPageWithDomainObjectKeyValuesOnly();
            BonoboWebControls.DataGrids.BnbDataGridHyperlink contractReportsLink = new BonoboWebControls.DataGrids.BnbDataGridHyperlink();
            contractReportsLink.ColumnNameToRenderControl = "Custom_ViewReport";
            contractReportsLink.HrefTemplate = "ContractPage.aspx?" + s + "&BnbGrant={0}&ControlID=Reports";
            contractReportsLink.KeyColumnNames = new string[] { "tblGrant_GrantID" };
            BnbDataGridForView3.ColumnControls.Add(contractReportsLink);

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            pnlSelectedAuditReport.Visible = true;
            pnlAuditTrialAllReports.Visible = false;
            string category = AuditSelectList1.SelectedItem.ToString();
            switch (category)
            {
                case "Proposals General": doSetAuditGridProperty("vwProposalGeneralAudit");
                    doSetLabelText("Proposals General"); break;
                case "Proposal Progress": doSetAuditGridProperty("vwProposalProgressAudit"); doSetLabelText("Proposal Progress"); break;
                case "Proposal Risks": doSetAuditGridProperty("vwProposalRiskAudit"); doSetLabelText("Proposal Risk"); break;
                case "Proposal Income Pipeline": doSetAuditGridProperty("vwProposalIncomePipelineAudit"); doSetLabelText("Proposal Income Pipeline"); break;
                case "Contract Claim": doSetAuditGridProperty("vwContractClaimAudit"); doSetLabelText("Contract Claim"); break;
                case "Contracts General": doSetAuditGridProperty("vwContractGeneralAudit"); doSetLabelText("Contracts General"); break;
                case "Contract Progress": doSetAuditGridProperty("vwContractProgressAudit"); doSetLabelText("Contract Progress"); break;
                case "Contract Report": doSetAuditGridProperty("vwContractReportAudit"); doSetLabelText("Contract Report"); break;
                case "Contract Report Due Date": doSetAuditGridProperty("vwContractReportDueDateAudit"); doSetLabelText("Contract Report Due Date"); break;
                case "Project General": doSetAuditGridProperty("vwProjectGeneralAudit"); doSetLabelText("Project General"); break;
                default:
                    pnlAuditTrialAllReports.Visible = true;
                    pnlSelectedAuditReport.Visible = false;
                    break;
            }
        }

        //TPT:PGMSMIFI-131
        private void doNewProposalLink()
        {
            BnbProject project = (BnbProject)bnb.ObjectTree.DomainObjectDictionary["BnbProject"];
            if (project.CurrentProjectStatus.ProjectStatusID == BnbConst.ProjectStatus_Closed)
                BnbDataGridForView2.NewLinkVisible = false;
            else
                BnbDataGridForView2.NewLinkVisible = true;
        }

        //TPT amended: PGMSP-18
        private void projectTitle()
        {
            BnbProject project = (BnbProject)bnb.ObjectTree.DomainObjectDictionary["BnbProject"];
            if (project.ProjectTitle != "" && project.ProjectTitle.Length > 50)
            {
                string projTitle = project.ProjectTitle.Remove(50);
                project.ProjectTitle = projTitle;
            }
        }
	}
}
