<%@ Page Language="C#" AutoEventWireup="true" Codebehind="WizardWendy.aspx.cs" Inherits="Frond.EditPages.WizardWendy" %>

<%@ Register Src="../UserControls/GShowLocation.ascx" TagName="GShowLocation" TagPrefix="uc2" %>
<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.ServicesControls"
    TagPrefix="cc1" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Wizard-Wendy</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>

    <script language="javascript" src="../Javascripts/GoogleMap.js"></script>

</head>
<body onload="initialize()" onunload="GUnload()">
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <div class="wizardheader" ms_positioning="FlowLayout">
                <h3>
                    <img class="icon24" src="../Images/wizard24silver.gif">
                    New Enquiry Wizard</h3>
            </div>
            <asp:Panel ID="panelGetName" runat="server" CssClass="wizardpanel">
                <p>
                    <em>This wizard will help you to add a new Enquiry.</em></p>
                <p>
                    Please enter Enquirers Forenames and Surname and then press Next</p>
                <p>
                </p>
                <p>
                    <table class="wizardtable" id="Table2">
                        <tr>
                            <td class="label" style="height: 6px">
                                Forename</td>
                            <td>
                                <asp:TextBox ID="txtFirstname" runat="server" MaxLength="50" TabIndex="1" Width="240px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="label">
                                Surname</td>
                            <td>
                                <asp:TextBox ID="txtSurname" runat="server" Width="240px" MaxLength="50" onblur="document.getElementById('WizardButtons_buttonNext').focus();"
                                    TabIndex="2"></asp:TextBox></td>
                        </tr>
                    </table>
                    <input id="lblHiddenIndividualID" type="hidden" name="lblHiddenIndividualID" runat="server">
                    <asp:Label ID="labelPanelZeroFeedback" runat="server" CssClass="feedback"></asp:Label></p>
            </asp:Panel>
            <asp:Panel ID="panelDuplicateCheck" runat="server" CssClass="wizardpanel">
                <asp:Panel ID="subpanelChoose" runat="server" Visible="True">
                    <p>
                        The following similar Individuals already exist. If any of them match the enquirer
                        you were going to enter, then please select them and press Next.</p>
                    <p>
                        <bnbdatagrid:BnbDataGridForView ID="bdgIndividuals" runat="server" CssClass="datagrid"
                            Width="100%" ViewLinksText="View" NewLinkText="New" NewLinkVisible="False" ViewLinksVisible="true"
                            ShowResultsOnNewButtonClick="false" DisplayResultCount="false" DisplayNoResultMessageOnly="false"
                            DataGridType="RadioButtonList" ColumnNames="tblIndividualKeyInfo_Forename, tblIndividualKeyInfo_Surname, tblIndividualKeyInfo_old_indv_id, tblIndividualAdditional_PreviousSurname, Custom_AppStatusForWendy, tblIndividualKeyInfo_DateOfBirth, tblAddress_AddressLine1"
                            ViewName="vwBonoboIndividualDuplicateCheckFastForWendy" GuidKey="tblIndividualKeyInfo_IndividualID">
                        </bnbdatagrid:BnbDataGridForView>
                    </p>
                    <p>
                        If none of these existing Individuals match, then press Next to create&nbsp;a new
                        Individual.</p>
                    <p>
                    </p>
                </asp:Panel>
                <asp:Label ID="lblNoDuplicates" runat="server">No similar Individuals already exist. Press Next to continue.</asp:Label>
                <p>
                    <asp:Label ID="lblPanelOneFeedback" runat="server" CssClass="feedback"></asp:Label>
            </asp:Panel>
            <asp:Panel ID="panelWendy" runat="server" CssClass="wizardpanel">
                <p>
                    Please enter further details about the new Enquirer.<br />
                    Fields marked with asterisk are Mandatory
                </p>
                <p>
                    <asp:Label ID="lblPanelEnquierDetailsFeedback" runat="server" CssClass="feedback"></asp:Label>&nbsp;</p>
                <table class="wizardtable" id="Table4">
                    <tr>
                        <td class="label">
                            * Forename</td>
                        <td>
                           <asp:TextBox ID="txtForename" TabIndex="1" MaxLength="50" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="label">
                            * Surname</td>
                        <td>
                            <asp:TextBox ID="txtSurnamePanel3" TabIndex="2" MaxLength="50" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            Enquiry date</td>
                        <td width="30%">
                            <asp:Label ID="lblEnquiryDate" runat="server"></asp:Label></td>
                        <td class="label" width="20%">
                            Enquiry Method</td>
                        <td width="30%">
                            <bnbdatacontrol:BnbStandaloneLookupControl ShowBlankRow="true" ID="cmbEnquiryMethod"
                                runat="server" CssClass="lookupcontrol" Width="230px" Height="22px" DataTextField="Description"
                                ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuEnquiryMethod"
                                TabIndex="24"></bnbdatacontrol:BnbStandaloneLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            * Title</td>
                        <td width="30%">
                            <bnbdatacontrol:BnbStandaloneLookupControl TabIndex="3" ShowBlankRow="true" ID="cmbTitle"
                                runat="server" CssClass="lookupcontrol" Width="150px" Height="22px" DataTextField="Description"
                                ListBoxRows="4" DataValueField="Description" LookupControlType="ComboBox" SortByDisplayOrder="true"
                                LookupTableName="lkuTitle"></bnbdatacontrol:BnbStandaloneLookupControl>
                            <td class="label" width="20%">
                                * Skill Code</td>
                            <td>
                                <bnbdatacontrol:BnbStandaloneLookupControl ShowBlankRow="true" ID="cmbSkillCode"
                                    runat="server" CssClass="lookupcontrol" Width="230px" Height="22px" DataTextField="Description"
                                    ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboSkillWithGroupAbbrev"
                                    TabIndex="25"></bnbdatacontrol:BnbStandaloneLookupControl>
                            </td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            * Country</td>
                        <td width="30%">
                            <bnbdatacontrol:BnbStandaloneLookupControl TabIndex="4" ShowBlankRow="true" ID="cmbCountry"
                                runat="server" CssClass="lookupcontrol" Width="230px" Height="22px" DataTextField="Description"
                                ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuCountry"></bnbdatacontrol:BnbStandaloneLookupControl>
                        </td>
                        <td class="label" width="20%">
                            Further Skill Info</td>
                        <td>
                            <asp:TextBox MaxLength="255" ID="txtFurtherSkillInfo" Width="190px" runat="server" TabIndex="26"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            * Postcode / Zip</td>
                        <td width="30%">
                            <asp:TextBox ID="txtPostcode" runat="server" Width="160px" TabIndex="5"></asp:TextBox>
                            <asp:Button ID="cmdFind" runat="server" Text="Find" OnClick="cmdFind_Click" TabIndex="6">
                            </asp:Button>
                            <cc1:BnbAddressLookupComposite ID="bnbAddressLookup1" runat="server" CssClass="lookupcontrol"
                                AccountCode="vso1111111" LicenseKey="XH57-MT97-CZ47-GH56" PasswordCapscan="492267"
                                RegistrationCodeCapscan="00003304" AutoPostBack="True" Width="230px" ListBoxRows="6"
                                OnSelectedValueChanged="bnbAddressLookup1_SelectedIndexChanged" TabIndex="6"></cc1:BnbAddressLookupComposite>
                        </td>
                        <td class="label" width="20%">
                            * VSO Type</td>
                        <td>
                            <bnbdatacontrol:BnbStandaloneLookupControl ShowBlankRow="true" ID="cmbVSOType" runat="server"
                                CssClass="lookupcontrol" Width="230px" Height="22px" DataTextField="Description"
                                ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboVSOTypeWithGroup"
                                TabIndex="27"></bnbdatacontrol:BnbStandaloneLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            * Address Line 1</td>
                        <td width="30%">
                            <asp:TextBox ID="txtAddressLine1" runat="server" Width="190px" TabIndex="7"></asp:TextBox>
                            <asp:Button ID="cmdFindAddressLine1" OnClick="cmdFind_Click" runat="server" Text="Find"
                                TabIndex="8"></asp:Button>
                        </td>
                        <td class="label" width="20%">
                            * Application Status</td>
                        <td>
                            <p>
                                <bnbdatacontrol:BnbStandaloneLookupControl ID="cmbStatusGroupEnquirer" runat="server"
                                    CssClass="lookupcontrol" Width="150px" Height="22px" DataTextField="Description"
                                    ListBoxRows="4" LookupControlType="ReadOnlyLabel" LookupTableName="lkuStatusGroup">
                                </bnbdatacontrol:BnbStandaloneLookupControl>
                                /
                                <bnbdatacontrol:BnbStandaloneLookupControl ID="cmbStatus" runat="server" CssClass="lookupcontrol"
                                    Width="150px" Height="22px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                    LookupTableName="lkuStatus" ColumnRelatedToParent="StatusGroupID" ParentControlID="cmbStatusGroupEnquirer"
                                    TabIndex="28"></bnbdatacontrol:BnbStandaloneLookupControl>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            Address Line 2
                        </td>
                        <td width="30%">
                            <asp:TextBox ID="txtAddressLine2" runat="server" Width="190px" TabIndex="9"></asp:TextBox></td>
                        <td class="label" width="20%">
                            Action</td>
                        <td>
                            <bnbdatacontrol:BnbStandaloneLookupControl ShowBlankRow="true" ID="cmbAction" runat="server"
                                CssClass="lookupcontrol" Width="230px" Height="22px" DataTextField="Description"
                                ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboContactDescriptionWithTypeForWendy"
                                TabIndex="29"></bnbdatacontrol:BnbStandaloneLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            Address Line 3</td>
                        <td width="30%">
                            <asp:TextBox ID="txtAddressLine3" runat="server" Width="190px" TabIndex="10"></asp:TextBox></td>
                        <td class="label" rowspan="2" width="20%">
                            * What is your main motivation for Volunteering?</td>
                        <td>
                            <bnbdatacontrol:BnbStandaloneLookupControl ID="cmbMainMotivation"
                                runat="server" CssClass="lookupcontrol" Width="230px" Height="22px" DataTextField="Description"
                                ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuMotivation"
                                TabIndex="30"></bnbdatacontrol:BnbStandaloneLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            * Postal / Town Line 4</td>
                        <td width="30%">
                            <asp:TextBox ID="txtAddressLine4" runat="server" Width="190px" TabIndex="11"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            County / State
                        </td>
                        <td width="30%">
                            <asp:TextBox ID="txtCounty" runat="server" Width="190px" TabIndex="12"></asp:TextBox>
                            <br />
                            <br />
                            <input id="btnShowLocation" runat="Server" type="button" onclick="showLocation()"
                                value="Show on map" tabindex="13" /></td>
                        <td class="label" width="20%">
                            * Region affliation</td>
                        <td class="tdAlignment2Wendy">
                            <bnbdatacontrol:BnbStandaloneLookupControl ID="cmbAffliation" runat="server" SelectMultiple="true"
                                CssClass="lookupcontrol" Width="230px" Height="45px" DataTextField="Description"
                                ListBoxRows="5" LookupControlType="ComboBox" LookupTableName="lkuCountryRegion"
                                SortByDisplayOrder="true" TabIndex="31"></bnbdatacontrol:BnbStandaloneLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            * Telephone Number</td>
                        <td width="30%" class="tdAlignment1Wendy">
                            <asp:TextBox ID="txtNumber" Width="190px" MaxLength="50" runat="server" TabIndex="14"></asp:TextBox></td>
                        <td class="label" width="20%">
                            * What best explains this affliation?</td>
                        <td class="tdAlignment2Wendy">
                            <bnbdatacontrol:BnbStandaloneLookupControl ID="cmbBestExplains" runat="server" CssClass="lookupcontrol"
                                Width="230px" Height="45px" DataTextField="Description" ListBoxRows="4" SelectMultiple="true"
                                LookupControlType="ComboBox" LookupTableName="lkuCountryReason" SortByDisplayOrder="true"
                                TabIndex="32"></bnbdatacontrol:BnbStandaloneLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" align="center" width="20%">
                            * Email</td>
                        <td width="30%" class="tdAlignment1Wendy">
                            <asp:TextBox ID="txtEmail" Width="190px" runat="server" TabIndex="15"></asp:TextBox></td>
                        <td class="label" width="20%">
                            * Which development issues do you feel passionate about?</td>
                        <td class="tdAlignment2Wendy">
                                <asp:ListBox ID="cmbPassionateAbout" runat="server" CssClass="lookupcontrol" 
                                SelectionMode="Multiple" Height="45px" Width="230px"></asp:ListBox>
                        </td>   
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            * Date of Birth</td>
                        <td width="30%" class="tdAlignment1Wendy">
                            <bnbdatacontrol:BnbStandaloneDateBox ID="txtDOB" runat="server" CssClass="datebox"
                                Width="90px" Height="20px" DateCulture="en-GB" DateFormat="dd/MMM/yyyy" TabIndex="16">
                            </bnbdatacontrol:BnbStandaloneDateBox>
                        </td>
                        <td class="label" width="20%">
                            * If you support other charities, what are they involved in?</td>
                        <td class="tdAlignment2Wendy">
                                <asp:ListBox  ID="cmbSupportOtherCharities" runat="server" CssClass="lookupcontrol" 
                                SelectionMode="Multiple" Height="45px" Width="230px"></asp:ListBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            * Sex</td>
                        <td class="tdAlignment1Wendy" width="30%">
                            <bnbdatacontrol:BnbStandaloneLookupControl ShowBlankRow="true" ID="cmbSex" runat="server"
                                CssClass="lookupcontrol" Width="230px" Height="22px" DataValueField="Abbreviation"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuGender"
                                TabIndex="17"></bnbdatacontrol:BnbStandaloneLookupControl>
                        </td>
                        <td class="label" width="20%">
                            * How do you support these charities?</td>
                        <td class="tdAlignment2Wendy"> 
                                <asp:ListBox ID="cmbSupportTheseCharities" runat="server" CssClass="lookupcontrol" 
                                SelectionMode="Multiple" Height="45px" Width="230px"></asp:ListBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            VSO Partnership</td>
                        <td width="30%" class="tdAlignment1Wendy">
                            <bnbdatacontrol:BnbStandaloneLookupControl ShowBlankRow="true" ID="cmbVSOPartnerships"
                                runat="server" CssClass="lookupcontrol" Width="230px" Height="22px" DataTextField="Description"
                                ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuPartnership"
                                TabIndex="18"></bnbdatacontrol:BnbStandaloneLookupControl>
                        </td>
                        <td class="label" width="20%">
                            * In what ways are you active in your community?</td>
                        <td class="tdAlignment2Wendy">
                                 <asp:ListBox ID="cmbActiveCommunity" runat="server" CssClass="lookupcontrol" 
                                SelectionMode="Multiple" Height="45px" Width="230px"></asp:ListBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            * Happy to receive range of ways?</td>
                        <td width="30%">
                            <bnbdatacontrol:BnbStandaloneLookupControl ShowBlankRow="true" ID="cmbRecieveRangeOfWays"
                                runat="server" CssClass="lookupcontrol" Width="100px" Height="22px" DataTextField="Description"
                                DataValueField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuYesNo"
                                TabIndex="19"></bnbdatacontrol:BnbStandaloneLookupControl>
                        </td>
                        <td rowspan="3" class="label" width="20%">
                            Book Events</td>
                        <td class="tdAlignment1Wendy">
                            <input type="checkbox" id="chkBookEvents" onblur="document.getElementById('WizardButtons_buttonFinish').focus();"
                                runat="server" tabindex="37" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            * Happy to receive emails?</td>
                        <td width="30%">
                            <bnbdatacontrol:BnbStandaloneLookupControl ShowBlankRow="true" ID="cmbRecieveEmails"
                                runat="server" CssClass="lookupcontrol" Width="100px" Height="22px" DataTextField="Description"
                                DataValueField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuYesNo"
                                TabIndex="20"></bnbdatacontrol:BnbStandaloneLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            * Happy to receive SMS?</td>
                        <td width="30%">
                            <bnbdatacontrol:BnbStandaloneLookupControl ShowBlankRow="true" ID="cmbRecieveSMS"
                                runat="server" CssClass="lookupcontrol" Width="100px" Height="22px" DataTextField="Description"
                                DataValueField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuYesNo"
                                TabIndex="21"></bnbdatacontrol:BnbStandaloneLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            * Sources</td>
                        <td colspan="3">
                            <bnbdatacontrol:BnbStandaloneLookupControl ShowBlankRow="true" ID="cmbSources" runat="server"
                                CssClass="lookupcontrol" Height="22px" DataTextField="Description" ListBoxRows="4"
                                LookupControlType="ComboBox" LookupTableName="vlkuBonoboSourceFull" TabIndex="22">
                            </bnbdatacontrol:BnbStandaloneLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="20%">
                            Source Other</td>
                        <td width="30%">
                            <asp:TextBox MaxLength="255" ID="txtSourceOther" Width="250px" runat="server" TabIndex="23"></asp:TextBox></td>
                        <td width="20%">
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <p>
                    <uc2:GShowLocation ID="showLocation" runat="server" />
                    &nbsp;</p>
            </asp:Panel>
            <p>
                <uc1:WizardButtons ID="wizardButtons" runat="server"></uc1:WizardButtons>
            </p>
        </div>
    </form>
</body>
</html>
