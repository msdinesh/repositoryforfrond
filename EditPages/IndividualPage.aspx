<%@ Page Language="c#" Codebehind="IndividualPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.IndividualPage" %>
<%@ Register TagPrefix="uc1" TagName="navigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html>
<head>
    <title></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
   		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server">
            </uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/bnbindividual24silver.gif">
            </uc1:Title>
            <uc1:navigationBar ID="navigationBar" runat="server">
            </uc1:navigationBar>
            <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons1" runat="server" UndoText="Undo"
                SaveText="Save" EditAllText="Edit All" NewText="New" HeightAllButtons="25px"
                UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" NewButtonEnabled="True"
                CssClass="databuttons"></bnbpagecontrol:BnbDataButtons>
            <asp:Label ID="lblViewWarnings" runat="server" CssClass="viewwarning" Visible="False"></asp:Label><bnbpagecontrol:BnbMessageBox
                ID="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:BnbMessageBox>
            <bnbpagecontrol:BnbTabControl ID="BnbTabControl1" runat="server"></bnbpagecontrol:BnbTabControl>
            <asp:Panel ID="MainTab" runat="server" CssClass="tcTab" BnbTabName="Main">
                <table class="fieldtable" id="Table1" border="0">
                    <tr>
                        <td class="label" width="25%">
                            Ref No</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox1" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.RefNo" StringLength="0" Rows="1" MultiLine="false"
                                Width="250px"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" width="25%">
                            Title</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl1" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbIndividual.Title" Width="250px" DataValueField="Description"
                                LookupTableName="lkuTitle" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                StringValueType="True" SortByDisplayOrder="True"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Surname Prefix</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl2" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbIndividual.SurnamePrefix" Width="250px" DataValueField="Description"
                                LookupTableName="lkuSurnamePrefix" DataTextField="Description" ListBoxRows="4"
                                LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                        <td class="label" width="25%">
                            Salutation</td>
                        <td>
                            <bnbdatacontrol:BnbTextBox ID="Bnbtextbox19" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.Additional.VMSMailSalutation" StringLength="0" Rows="1"
                                MultiLine="false" Width="250px"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Surname</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox3" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.Surname" StringLength="0" Rows="1" MultiLine="false"
                                Width="250px"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" width="25%">
                            Forenames</td>
                        <td>
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox4" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.Forename" StringLength="0" Rows="1" MultiLine="false"
                                Width="250px"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Initials</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox5" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.Initials" StringLength="0" Rows="1" MultiLine="false"
                                Width="250px"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" width="25%">
                            Known As</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox6" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.Additional.Salutation" StringLength="0" Rows="1" MultiLine="false"
                                Width="250px"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 24px" width="25%">
                            Email</td>
                        <td style="height: 24px" width="25%">
                            <asp:Label ID="lblEmail" runat="server" Width="184px"></asp:Label></td>
                        <td class="label" style="height: 24px" width="25%">
                            Sex</td>
                        <td style="height: 24px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl3" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbIndividual.Gender" Width="250px" DataValueField="Abbreviation"
                                LookupTableName="lkuGender" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox">
                            </bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Date of Birth</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox1" runat="server" CssClass="datebox" Height="20px"
                                DataMember="BnbIndividual.DateOfBirth" Width="90px" DateCulture="en-GB" DateFormat="dd/mm/yyyy">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            Date Of Death</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox6" runat="server" CssClass="datebox" Height="20px"
                                DataMember="BnbIndividual.Additional.DateOfDeath" Width="90px" DateCulture="en-GB"
                                DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Age</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDecimalBox ID="BnbDecimalBox1" runat="server" CssClass="textbox"
                                Height="20px" DataMember="BnbIndividual.Age" Width="90px" FormatExpression="#,0"
                                TextAlign="left"></bnbdatacontrol:BnbDecimalBox>
                        </td>
                        <td class="label" width="25%">
                            Raisers Edge ID</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox10" runat="server" CssClass="textbox" DataMember="BnbIndividual.Additional.RaisersEdgeID"
                                Width="250px"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Mobile Number</td>
                        <td width="25%">
                            <asp:Label ID="lblMobile" runat="server" Width="184px"></asp:Label></td>
                        <td class="label" width="25%">
                            Current Address Type<br>
                            <br>
                            Current Address</td>
                        <td valign="top" width="50%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl4" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbIndividual.CurrentIndividualAddress.AddressTypeID"
                                Width="250px" LookupTableName="vlkuBonoboAddressTypeWithGroup" DataTextField="Description"
                                ListBoxRows="4" LookupControlType="ReadOnlyLabel"></bnbdatacontrol:BnbLookupControl>
                            <br>
                            <br>
                            <asp:Label ID="uxCurrentAddressLabel" runat="server" Width="184px"></asp:Label><br>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Partner</td>
                        <td width="25%">
                            <bnbgenericcontrols:BnbHyperlink ID="uxPartnerHyper" runat="server"></bnbgenericcontrols:BnbHyperlink></td>
                        <td class="label" width="25%">
                        </td>
                        <td valign="top" width="50%">
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Comments</td>
                        <td colspan="3">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox9" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.Additional.Comments" StringLength="0" Rows="6" MultiLine="True"
                                Width="520px"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            RV Comments</td>
                        <td colspan="3">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox7" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.Additional.RVComments" StringLength="0" Rows="6" MultiLine="True"
                                Width="520px"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <!--More Details-->
            <asp:Panel ID="MoreDetailsTab" runat="server" CssClass="tcTab" BnbTabName="More Details">
                <table class="fieldtable" id="Table2" border="0">
                    <tr>
                        <td class="label" style="height: 25px" width="25%">
                            Occupation Group/Type</td>
                        <td style="height: 25px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl5" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbIndividual.Additional.OccupationTypeID" Width="250px"
                                LookupTableName="vlkuBonoboOccupationTypeWithGroupAbbrev" DataTextField="Description"
                                ListBoxRows="4" LookupControlType="ComboBox" AutoPostBack="True"></bnbdatacontrol:BnbLookupControl>
                        </td>
                        <td class="label" style="height: 25px" width="25%">
                            Occupation Role</td>
                        <td style="height: 25px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl8" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbIndividual.Additional.OccupationRoleID" Width="250px"
                                LookupTableName="lkuOccupationRole" DataTextField="Description" ListBoxRows="4"
                                LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 24px" width="25%">
                            ID Number
                        </td>
                        <td style="height: 24px" width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox17" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.Additional.IdentityCardNo" StringLength="0" Rows="1"
                                MultiLine="false" Width="250px" ControlType="Text"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" style="height: 24px" width="25%">
                            Alert / Working Language</td>
                        <td style="height: 24px" width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox18" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.Additional.GCAlert" StringLength="0" Rows="1" MultiLine="false"
                                Width="250px" ControlType="Text"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 24px" width="25%">
                            Public Sector Pension Contribution</td>
                        <td style="height: 24px" width="25%">
                            <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox1" runat="server" CssClass="checkbox"
                                DataMember="BnbIndividual.Additional.PublicSectorPensionContribution"></bnbdatacontrol:BnbCheckBox>
                        </td>
                        <td class="label" style="height: 24px" width="25%">
                            Key Contact</td>
                        <td style="height: 24px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl11" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbIndividual.Additional.GCContactUserID" Width="250px"
                                LookupTableName="vlkuBonoboUser" DataTextField="Description" ListBoxRows="4"
                                LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 24px" width="25%">
                            Key Contact Job Title</td>
                        <td style="height: 24px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl12" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbIndividual.Additional.GCContactJobTitleID" Width="250px"
                                LookupTableName="vlkuJobTitle" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox">
                            </bnbdatacontrol:BnbLookupControl>
                        </td>
                        <td class="label" style="height: 24px" width="25%">
                            Key Contact Dept</td>
                        <td style="height: 24px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl13" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbIndividual.Additional.GCContactDepartmentID" Width="250px"
                                LookupTableName="vlkuDepartment" DataTextField="Description" ListBoxRows="4"
                                LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 24px" width="25%">
                            Secondary Contacts</td>
                        <td style="height: 24px" colspan="3">
                            <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList8"
                                runat="server" CssClass="datagrid" DataMember="BnbIndividual.IndividualUserContacts"
                                Width="200px" DataGridType="Editable" DataGridForDomainObjectListType="Editable"
                                VisibleOnNewParentRecord="false" SortBy="ID" AddButtonText="Add" AddButtonVisible="true"
                                DataProperties="UserID [BnbLookupControl:vlkuBonoboUser]" EditButtonText="Edit"
                                EditButtonVisible="true" DeleteButtonVisible="false"></bnbdatagrid:BnbDataGridForDomainObjectList>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 24px" width="25%">
                            Directorate</td>
                        <td style="height: 24px" width="25%">
                            <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox3" runat="server" CssClass="checkbox"
                                DataMember="BnbIndividual.Additional.ConfidentialAddress"></bnbdatacontrol:BnbCheckBox>
                        </td>
                        <td class="label" width="25%">
                            Diet</td>
                        <td style="width: 345px" width="345">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl6" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbIndividual.Additional.DietID" Width="250px" LookupTableName="lkuDiet"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            &nbsp; CV&nbsp;Filename</td>
                        <td>
                            <bnbdatacontrol:BnbTextBox ID="Bnbtextbox14" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.Additional.CVFilename" StringLength="0" Rows="1" MultiLine="false"
                                Width="250px"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" style="width: 333px" width="333">
                            Blood Group</td>
                        <td style="width: 345px" width="345">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl7" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbIndividual.Additional.BloodGroupID" Width="250px"
                                LookupTableName="lkuBloodGroup" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox">
                            </bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Passport Number</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="Bnbtextbox12" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.Additional.PassportNumber" StringLength="0" Rows="1"
                                MultiLine="false" Width="250px"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" style="width: 333px" width="333">
                            Passport Expiry Date</td>
                        <td style="width: 345px" width="345">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox3" runat="server" CssClass="datebox" Height="20px"
                                DataMember="BnbIndividual.Additional.PassportExpiryDate" Width="90px" DateCulture="en-GB"
                                DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Passport Name</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="Bnbtextbox13" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.Additional.PassportName" StringLength="0" Rows="1"
                                MultiLine="false" Width="250px"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" style="width: 333px" width="333">
                            Visa Number</td>
                        <td style="width: 345px" width="345">
                            <bnbdatacontrol:BnbTextBox ID="Bnbtextbox2" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.Additional.VisaNumber" StringLength="0" Rows="1" MultiLine="false"
                                Width="250px"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Visa Expiry Date</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox4" runat="server" CssClass="datebox" Height="20px"
                                DataMember="BnbIndividual.Additional.VisaExpiryDate" Width="90px" DateCulture="en-GB"
                                DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" style="width: 333px; height: 40px" width="333">
                            &nbsp;Work Permit Number</td>
                        <td style="width: 345px; height: 40px" width="345">
                            <bnbdatacontrol:BnbTextBox ID="Bnbtextbox11" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.Additional.WorkPermitNumber" StringLength="0" Rows="1"
                                MultiLine="false" Width="250px"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 40px">
                            Work Permit Expiry Date</td>
                        <td style="height: 40px" valign="top" width="50%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox5" runat="server" CssClass="datebox" Height="20px"
                                DataMember="BnbIndividual.Additional.WorkPermitExpiryDate" Width="90px" DateCulture="en-GB"
                                DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" style="width: 333px" width="333">
                            Place of Birth</td>
                        <td style="width: 345px" width="345">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl9" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbIndividual.Additional.PlaceOfBirthID" Width="250px"
                                LookupTableName="lkuCountry" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox">
                            </bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Disability</td>
                        <td valign="top" width="50%">
                            <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox2" runat="server" CssClass="checkbox"
                                DataMember="BnbIndividual.Additional.Disability"></bnbdatacontrol:BnbCheckBox>
                        </td>
                        <td class="label" style="width: 333px" width="333">
                            Ethnic Origin</td>
                        <td style="width: 345px" width="345">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl10" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbIndividual.Additional.EthnicityID" Width="250px"
                                LookupTableName="lkuEthnicity" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                SortByDisplayOrder="True" IncludeExcluded="false"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Ethnicity Other</td>
                        <td valign="top" width="50%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox15" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.Additional.EthnicityOther" StringLength="0" Rows="1"
                                MultiLine="false" Width="250px" ControlType="Text"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" style="width: 333px" width="333">
                            Previous Surname</td>
                        <td style="width: 345px" width="345">
                            <bnbdatacontrol:BnbTextBox ID="Bnbtextbox20" runat="server" CssClass="textbox" Height="20px"
                                DataMember="BnbIndividual.Additional.PreviousSurname" StringLength="0" Rows="1"
                                MultiLine="false" Width="250px"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width: 333px" width="333">
                            Skills</td>
                        <td colspan="3">
                            <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList1"
                                runat="server" CssClass="datagrid" DataMember="BnbIndividual.IndividualSkills"
                                Width="416px" DataGridType="Editable" DataGridForDomainObjectListType="Editable"
                                VisibleOnNewParentRecord="false" SortBy="ID" AddButtonText="Add" AddButtonVisible="true"
                                DataProperties="SkillID [BnbLookupControl:vlkuBonoboSkillWithGroupAbbrev]" EditButtonText="Edit"
                                EditButtonVisible="true" DeleteButtonVisible="True"></bnbdatagrid:BnbDataGridForDomainObjectList>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width: 333px" width="333">
                            Marital Status</td>
                        <td colspan="3">
                            <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList3"
                                runat="server" CssClass="datagrid" DataMember="BnbIndividual.IndividualMaritalStatuses"
                                Width="200px" DataGridType="Editable" AddButtonText="Add" AddButtonVisible="true"
                                DataProperties="MaritalStatusID [BnbLookupControl:lkuMaritalStatus],MaritalStatusDate[BnbDateBox]"
                                EditButtonText="Edit" EditButtonVisible="true" DeleteButtonVisible="True"></bnbdatagrid:BnbDataGridForDomainObjectList>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width: 333px" width="333">
                            Nationality</td>
                        <td colspan="3">
                            <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList2"
                                runat="server" CssClass="datagrid" DataMember="BnbIndividual.IndividualNationalities"
                                Width="416px" DataGridType="Editable" AddButtonText="Add" AddButtonVisible="true"
                                DataProperties="NationalityID[BnbLookupControl:lkuNationality]" EditButtonText="Edit"
                                EditButtonVisible="true" DeleteButtonVisible="True"></bnbdatagrid:BnbDataGridForDomainObjectList>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width: 333px" width="333">
                            Vaccinations</td>
                        <td colspan="3">
                            <bnbdatagrid:BnbDataGridForDomainObjectList ID="Bnbdatagridfordomainobjectlist4"
                                runat="server" CssClass="datagrid" DataMember="BnbIndividual.IndividualVaccinations"
                                Width="416px" DataGridType="Editable" AddButtonText="Add" AddButtonVisible="true"
                                DataProperties="VaccinationID[BnbLookupControl:lkuVaccination],VaccinationDate[BnbDateBox]"
                                EditButtonText="Edit" EditButtonVisible="true" DeleteButtonVisible="True"></bnbdatagrid:BnbDataGridForDomainObjectList>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width: 333px" width="333">
                            Subscriptions</td>
                        <td colspan="3">
                            <bnbdatagrid:BnbDataGridForDomainObjectList ID="Bnbdatagridfordomainobjectlist5"
                                runat="server" CssClass="datagrid" DataMember="BnbIndividual.IndividualSubscriptions"
                                Width="416px" DataGridType="Editable" AddButtonText="Add" AddButtonVisible="true"
                                DataProperties="SubscriptionID[BnbLookupControl:lkuSubscription],DateFrom[BnbDateBox],DateTo[BnbDateBox]"
                                EditButtonText="Edit" EditButtonVisible="true" DeleteButtonVisible="True"></bnbdatagrid:BnbDataGridForDomainObjectList>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width: 333px" width="333">
                            Address Distribution</td>
                        <td colspan="3">
                            <bnbdatagrid:BnbDataGridForDomainObjectList ID="Bnbdatagridfordomainobjectlist6"
                                runat="server" CssClass="datagrid" DataMember="BnbIndividual.IndividualAddressDistributions"
                                Width="416px" DataGridType="Editable" AddButtonText="Add" AddButtonVisible="true"
                                DataProperties="AddressDistributionID[BnbLookupControl:lkuAddressDistribution], Comments[BnbTextBox]"
                                EditButtonText="Edit" EditButtonVisible="true" DeleteButtonVisible="True"></bnbdatagrid:BnbDataGridForDomainObjectList>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width: 333px" width="333">
                            Specialisms</td>
                        <td colspan="3">
                            <bnbdatagrid:BnbDataGridForDomainObjectList ID="Bnbdatagridfordomainobjectlist9"
                                runat="server" CssClass="datagrid" DataMember="BnbIndividual.IndividualQualifications"
                                Width="416px" DataGridType="Editable" AddButtonText="Add" AddButtonVisible="true"
                                DataProperties="QualificationID[BnbLookupControl:lkuQualification]" EditButtonText="Edit"
                                EditButtonVisible="true" DeleteButtonVisible="True"></bnbdatagrid:BnbDataGridForDomainObjectList>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width: 333px" width="333">
                            Allergies</td>
                        <td colspan="3">
                            <bnbdatagrid:BnbDataGridForDomainObjectList ID="Bnbdatagridfordomainobjectlist10"
                                runat="server" CssClass="datagrid" DataMember="BnbIndividual.IndividualAllergies"
                                Width="416px" DataGridType="Editable" AddButtonText="Add" AddButtonVisible="true"
                                DataProperties="AllergyID[BnbLookupControl:lkuAllergy]" EditButtonText="Edit"
                                EditButtonVisible="true" DeleteButtonVisible="True"></bnbdatagrid:BnbDataGridForDomainObjectList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="AddressTab" runat="server" CssClass="tcTab" BnbTabName="Addresses">
                <div class="sectionheading">
                    Live Addresses</div>
                <p>
                    New Address of type
                    <asp:DropDownList ID="drpNewAddressType" runat="server">
                    </asp:DropDownList>&nbsp;&nbsp;&nbsp;
                </p>
                <p>
                    <bnbdatagrid:BnbDataGridForView ID="uxAddressLiveGrid" runat="server" CssClass="datagrid"
                        Width="100%" RedirectPage="AddressPage.aspx" DomainObject="BnbIndividualAddress"
                        ViewLinksVisible="true" ViewName="vwBonoboIndividualAddressLive" ColumnNames="lnkIndividualAddress_Current, Custom_AddressType, Custom_AddressTelephone, Custom_CombinedAddress, Custom_AddressOrg, Custom_AddressMisc"
                        GuidKey="lnkIndividualAddress_IndividualAddressID" FieldName="lnkIndividualAddress_IndividualID"
                        QueryStringKey="BnbIndividual" NewLinkText="New Address" NewLinkVisible="False">
                    </bnbdatagrid:BnbDataGridForView>
                    <asp:Label ID="uxLiveAddressConfidentialLabel" runat="server" Visible="False"></asp:Label></p>
                <div class="sectionheading">
                    Old Addresses</div>
                <p>
                    <bnbdatagrid:BnbDataGridForView ID="uxAddressOldGrid" runat="server" CssClass="datagrid"
                        Width="100%" RedirectPage="AddressPage.aspx" DomainObject="BnbIndividualAddress"
                        ViewLinksVisible="true" ViewName="vwBonoboIndividualAddressOld" ColumnNames="lnkIndividualAddress_DateFrom, lnkIndividualAddress_DateTo, Custom_AddressType, Custom_AddressTelephone, Custom_CombinedAddress,  Custom_AddressOrg, Custom_AddressMisc"
                        GuidKey="lnkIndividualAddress_IndividualAddressID" FieldName="lnkIndividualAddress_IndividualID"
                        QueryStringKey="BnbIndividual" NewLinkText="New Address" NewLinkVisible="False"
                        NoResultMessage="(There are no out-of-date addresses for this Individual)" DisplayNoResultMessageOnly="True">
                    </bnbdatagrid:BnbDataGridForView>
                    <asp:Label ID="uxOldAddressConfidentialLabel" runat="server" Visible="False"></asp:Label></p>
            </asp:Panel>
            <asp:Panel ID="ContactNumberTab" runat="server" CssClass="tcTab" BnbTabName="Contact Numbers">
                <p>
                    <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList7"
                        runat="server" CssClass="datagrid" DataMember="BnbIndividual.ContactNumbers"
                        Width="100%" DataGridType="Editable" AddButtonText="Add" AddButtonVisible="true"
                        DataProperties="ContactNumberTypeID [BnbLookupControl:vlkuBonoboContactNumberTypeIndividual], ContactNumber [BnbTextBox], DateFrom [BnbDateBox], DateTo [BnbDateBox], Comments [BnbTextBox]"
                        EditButtonText="Edit" EditButtonVisible="true" DeleteButtonVisible="true" DESIGNTIMEDRAGDROP="4327">
                    </bnbdatagrid:BnbDataGridForDomainObjectList>
                    <em></em>
                </p>
            </asp:Panel>
            <asp:Panel ID="ContactBlockingTab" runat="server" CssClass="tcTab" BnbTabName="Contact Blocking">
                <p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView6" runat="server" CssClass="datagrid"
                        Width="100%" RedirectPage="ContactBlockPage.aspx" DomainObject="BnbIndividualContactBlock"
                        ViewLinksVisible="true" ViewName="vwBonoboIndividualContactBlocks" GuidKey="lnkIndividualContactBlock_IndividualContactBlockID"
                        FieldName="lnkIndividualContactBlock_IndividualID" QueryStringKey="BnbIndividual"
                        NewLinkText="New Contact Block" NewLinkVisible="true" NoResultMessage="(No Contact Block records for this Individual)"
                        DisplayNoResultMessageOnly="True" NewLinkReturnToParentPageDisabled="False"></bnbdatagrid:BnbDataGridForView>
                    <br>
                    <i>
                        <br>
                    </i>
                </p>
            </asp:Panel>
            <asp:Panel ID="DAndPTab" runat="server" CssClass="tcTab" BnbTabName="D&amp;P">
                <bnbgenericcontrols:BnbHyperlink ID="hypNewDP" runat="server">New dependant or partner</bnbgenericcontrols:BnbHyperlink>
                <br>
                <bnbdatagrid:BnbDataGridForView ID="uxRelativesGrid" runat="server" CssClass="datagrid"
                    Width="100%" RedirectPage="DepAndPartPage.aspx" DomainObject="BnbRelative" ViewLinksVisible="true"
                    ViewName="vwBonoboRelative" GuidKey="tblRelative_RelativeID" FieldName="tblRelative_IndividualID"
                    QueryStringKey="BnbIndividual" NoResultMessage="(No D+P records for this Individual)"
                    DisplayNoResultMessageOnly="True" ColumnsToExclude="IsDependent" ViewLinksText="View"
                    ShowResultsOnNewButtonClick="false" DisplayResultCount="false"></bnbdatagrid:BnbDataGridForView>
            </asp:Panel>
            <asp:Panel ID="SponsorshipTab" runat="server" CssClass="tcTab" BnbTabName="Donations">
                <p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView8" runat="server" CssClass="datagrid"
                        Width="100%" RedirectPage="FundingPage.aspx" DomainObject="BnbFunding" ViewLinksVisible="True"
                        ViewName="vwBonoboOrganisationSponsorship" ColumnNames="lkuFundingType_Description, Custom_VolName, tblIndividualKeyInfo_old_indv_id, Custom_FullPositionReference, lkuFundingStatus_Description, tblFunding_FundingValue, lkuFinancialYear_Description"
                        GuidKey="tblFunding_FundingID" FieldName="tblFunding_DonorIndividualID" QueryStringKey="BnbIndividual"
                        NewLinkText="" NewLinkVisible="False" NoResultMessage="(No donated sponsorship from this Individual)"
                        DisplayNoResultMessageOnly="True" GuidDataProperty="IndividualID" CriteriaParameter="tblFunding_DonorIndividualID">
                    </bnbdatagrid:BnbDataGridForView>
                </p>
            </asp:Panel>
            <asp:Panel ID="ActionsTab" runat="server" CssClass="tcTab" BnbTabName="Actions">
                <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView3" runat="server" CssClass="datagrid"
                    Width="100%" RedirectPage="ActionPage.aspx" DomainObject="BnbContactDetail" ViewLinksVisible="true"
                    ViewName="vwBonoboIndividualActions" ColumnNames="Custom_ActionType, lkuSentVia_Description, Custom_SentBy, tblContactDetail_DateSent, tblContactDetail_MailingReference, tblContactDetail_DateChase, Custom_ActionApplication, Custom_CombinedAddress"
                    GuidKey="tblContactDetail_ContactDetailID" FieldName="tblIndividualKeyInfo_IndividualID"
                    QueryStringKey="BnbIndividual" NewLinkText="New Action" NewLinkVisible="true"
                    NoResultMessage="(No Action records for this Individual)" DisplayNoResultMessageOnly="True"
                    NewLinkReturnToParentPageDisabled="True" ColumnsToExclude="" ViewLinksText="View"
                    ShowResultsOnNewButtonClick="false" DisplayResultCount="false" DeleteLinksVisible="False"
                    ReturnToHostedPage="True"></bnbdatagrid:BnbDataGridForView>
            </asp:Panel>
            <asp:Panel ID="EventsTab" runat="server" CssClass="tcTab" BnbTabName="Events">
                <div class="sectionheading">
                    Event Attendance</div>
                <bnbdatagrid:BnbDataGridForView ID="uxAttendanceDataGrid" runat="server" CssClass="datagrid"
                    Width="100%" RedirectPage="AttendancePage.aspx" DomainObject="BnbAttendance"
                    ViewLinksVisible="true" ViewName="vwBonoboIndividualAttendanceLong" GuidKey="tblAttendance_AttendanceID"
                    FieldName="tblIndividualKeyInfo_IndividualID" QueryStringKey="BnbIndividual"
                    NewLinkText="New Event Attendance" NewLinkVisible="true" NoResultMessage="(No Attendance records for this Individual)"
                    DisplayNoResultMessageOnly="True" ViewLinksText="View" ShowResultsOnNewButtonClick="false"
                    DisplayResultCount="false"></bnbdatagrid:BnbDataGridForView>
                <div class="sectionheading">
                    Event Personnel</div>
                <bnbdatagrid:BnbDataGridForView ID="uxEventPersonnelDataGrid" runat="server" CssClass="datagrid"
                    Width="100%" RedirectPage="EventPersonnelPage.aspx" DomainObject="BnbEventPersonnel"
                    ViewLinksVisible="true" ViewName="vwBonoboIndividualEventPersonnel" GuidKey="tblEventPersonnel_EventPersonnelID"
                    FieldName="tblEventPersonnel_IndividualID" QueryStringKey="BnbIndividual" NewLinkText="New Event Personnel"
                    NewLinkVisible="true" NoResultMessage="(No Event Personnel records for this Individual)"
                    DisplayNoResultMessageOnly="True" ViewLinksText="View" ShowResultsOnNewButtonClick="false"
                    DisplayResultCount="false"></bnbdatagrid:BnbDataGridForView>
            </asp:Panel>
            <asp:Panel ID="ResourcesTab" runat="server" CssClass="tcTab" BnbTabName="Resources">
                <bnbdatagrid:BnbDataGridForView ID="Bnbdatagridforview5" runat="server" CssClass="datagrid"
                    Width="100%" RedirectPage="ResourcePage.aspx" DomainObject="BnbIndividualResource"
                    ViewLinksVisible="true" ViewName="vwBonoboIndividualResources2" GuidKey="lnkIndividualResource_IndividualResourceID"
                    FieldName="tblIndividualKeyInfo_IndividualID" QueryStringKey="BnbIndividual"
                    NewLinkText="New Resource" NewLinkVisible="true" NoResultMessage="(No Resource records for this Individual)"
                    DisplayNoResultMessageOnly="True" ViewLinksText="View" ShowResultsOnNewButtonClick="false"
                    DisplayResultCount="false" ReturnToHostedPage="True"></bnbdatagrid:BnbDataGridForView>
            </asp:Panel>
            <asp:Panel ID="BenefitsTab" runat="server" CssClass="tcTab" BnbTabName="Benefits">
                <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView7" runat="server" CssClass="datagrid"
                    Width="100%" RedirectPage="BenefitPage.aspx" DomainObject="BnbIndividualBenefit"
                    ViewLinksVisible="true" ViewName="vwBonoboIndividualBenefit" GuidKey="lnkIndividualBenefit_IndividualBenefitID"
                    FieldName="tblIndividualKeyInfo_IndividualID" QueryStringKey="BnbIndividual"
                    NewLinkText="New Benefit Record" NewLinkVisible="true" NoResultMessage="(No Benefit records for this Individual)"
                    DisplayNoResultMessageOnly="True" ViewLinksText="View" ShowResultsOnNewButtonClick="false"
                    DisplayResultCount="false" ReturnToHostedPage="True"></bnbdatagrid:BnbDataGridForView>
            </asp:Panel>
            <asp:Panel ID="ApplicationsTab" runat="server" CssClass="tcTab" BnbTabName="Applications">
                <bnbgenericcontrols:BnbHyperlink ID="uxNewApplicationHyper" runat="server">New Enquiry or Application</bnbgenericcontrols:BnbHyperlink>
                <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView9" runat="server" CssClass="datagrid"
                    Width="100%" RedirectPage="ApplicationPage.aspx" DomainObject="BnbApplication"
                    ViewLinksVisible="true" ViewName="vwBonoboIndividualApplications" ColumnNames="Custom_ApplicationOrder, Custom_CombinedStatus, tblApplication_ApplicationDate"
                    GuidKey="tblApplication_ApplicationID" FieldName="tblIndividualKeyInfo_IndividualID"
                    QueryStringKey="BnbIndividual" NewLinkText="New" NewLinkVisible="False" NoResultMessage="(No Application records for this Individual)"
                    DisplayNoResultMessageOnly="True" ColumnsToExclude="lkuStatus_Description,lkuStatusGroup_Description"
                    ViewLinksText="View" ShowResultsOnNewButtonClick="false" DisplayResultCount="false">
                </bnbdatagrid:BnbDataGridForView>
            </asp:Panel>
            <asp:Panel ID="WorkingLanguageTab" runat="server" CssClass="tcTab" BnbTabName="Working Language">
                <bnbpagecontrol:BnbSectionHeading ID="BnbSectionHeading2" runat="server" CssClass="sectionheading"
                    SectionHeading="Working Language" Width="100%" Height="20px" Collapsed="false"></bnbpagecontrol:BnbSectionHeading>
                <p>
                    <bnbdatagrid:BnbDataGridForDomainObjectList ID="gridWorkingLanguage" runat="server"
                        CssClass="datagrid" Width="100%" DataMember="BnbIndividual.IndividualLanguages"
                        AddButtonVisible="true" EditButtonText="Edit" AddButtonText="Add" EditButtonVisible="true"
                        DeleteButtonVisible="false" DataGridType="Editable" DataProperties="LanguageID [BnbLookupControl:lkuLanguage], WrittenLevelID [BnbLookupControl:lkuLanguageLevel], OralLevelID [BnbLookupControl:lkuLanguageLevel],ComprehensionLevelID [BnbLookupControl:lkuLanguageLevel]"
                        SortBy="LanguageID"></bnbdatagrid:BnbDataGridForDomainObjectList>
                </p>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
