using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using BonoboDomainObjects;
using BonoboEngine;
namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for MailingExtractUtil.
	/// </summary>
	public partial class MailingExtractUtil : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
		protected System.Web.UI.WebControls.Label lblRunOn;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here+
			string userFullname;
			string userEmail;
			string recBase;
			int recBaseID;
			if(! IsPostBack )
			{
				titleBar.TitleText = "Mailing Extract Utility";
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				if (Request.QueryString["BnbContactHeader"]!=null)
				{
					Guid contactheaderid = new Guid(Request.QueryString["BnbContactHeader"]);
					BnbContactHeader m_contactHeader=(BnbContactHeader) BnbContactHeader.GeneralRetrieve(contactheaderid,typeof(BnbContactHeader));
					if (m_contactHeader!=null)
					{
						lblEmailSent.Visible=false;
						lblFromFind.Visible=true;
						RetrieveMailExtractInfo(m_contactHeader);
					}
					else
						lblResultsMessage.Text="Data error: This record could not be found in the database. Please contact IT Helpdesk if you require it.";
				}
				else
				{
					recBaseID=BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID;
					this.Session["RecBaseID"]=recBaseID;
					userEmail=BnbEngine.SessionManager.GetSessionInfo().EmailAddress;
					this.Session["UserEmail"]=userEmail;
					userFullname=BnbEngine.SessionManager.GetSessionInfo().FullName;
				
					recBase=BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBase;
					if (userEmail=="")
						userEmail="<unknown email address>";
					lblInstructions.Text="Please click on the postbox above to start the mailing extract for the " +recBase + " recruitment base";
					lblInstructions.Text=lblInstructions.Text+" and email it to " + userFullname + "(" + userEmail + ")";
					imageGoButton.ImageUrl="../Images/" + recBaseID.ToString() + "postbox.jpg";
					imageGoButton.Attributes.Add("onclick", "javascript:showPostbox()");
					afterpanel.Visible=false;
				}
				BonoboWebControls.BnbWebFormManager.LogAction(titleBar.TitleText, null, this);
			}
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.imageGoButton.Click += new System.Web.UI.ImageClickEventHandler(this.imageGoButton_Click);

		}
		#endregion


		private void imageGoButton_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			string rbase=Session["RecBaseID"].ToString();
			string useremail=Session["UserEmail"].ToString();
			string resultMessage = null;
			BnbContactHeader m_contactHeader;
			int irbase=-1;
			try
			{
				irbase=int.Parse(rbase);
			}
			catch
			{	
				irbase=-1;
			}

			if (irbase==-1)
			{
				resultMessage="Extract failed: You do not have a default recruitment base set.";
			}
			else if (useremail=="")
			{
				resultMessage="Extract failed: You do not have an email address set.";
			}
			else
			{
				m_contactHeader = null;
				try
				{
					m_contactHeader=BnbContactDetail.CreateMailingExtract(irbase,useremail);
				} 
				catch(ApplicationException ae)
				{
					// check for no packs message
					if (ae.Message.StartsWith("No packs"))
						resultMessage = ae.Message;
					else
						throw ae;

				}
				if (m_contactHeader != null)
				{
					RetrieveMailExtractInfo(m_contactHeader);
					resultMessage=m_contactHeader.Comments;
					// log the run in the general log
					BonoboWebControls.BnbWebFormManager.LogAction(String.Format("{0} (Batch Run {1})", 
						titleBar.TitleText,
						m_contactHeader.Old_chm_id),
						String.Format("Mailing Ref: {0} No of Records: {1} Message: {2}",
						m_contactHeader.Old_chm_id,
						m_contactHeader.NumberOfRecords.ToString(),
						m_contactHeader.Comments), this); 
				}
			}

			lblResultsMessage.Text=resultMessage;
			lblEmailSent.Visible=true;
			lblFromFind.Visible=false;

		}

		private void RetrieveMailExtractInfo(BnbContactHeader m_contactHeader)
		{
			string strOutputFolder = System.Convert.ToString(ConfigurationSettings.AppSettings["MailingExtractOutput"]);
			string xmlFilename;
			string csvFilename;
			string txtFilename;

			dbRunOn.Date=m_contactHeader.DateSent;
			lblNumberRecords.Text=m_contactHeader.NumberOfRecords.ToString();
			lblMailingReference.Text=m_contactHeader.Old_chm_id;
			lblRunDepartment.Text=BnbEngine.LookupManager.GetLookupItemDescription("appDepartment",m_contactHeader.DepartmentID);
			lblRunBy.Text=BnbEngine.LookupManager.GetLookupItemDescription("vlkuBonoboUser",m_contactHeader.SentByID);
			lblMailingType.Text=m_contactHeader.MailType;

			//Set up events for buttons
			xmlFilename=strOutputFolder+m_contactHeader.Old_chm_id+".xml";
			csvFilename=strOutputFolder+m_contactHeader.Old_chm_id+".csv";
			txtFilename=strOutputFolder+m_contactHeader.Old_chm_id+".txt";

			btnXML.Attributes.Add("onclick", "javascript:window.open('" + xmlFilename + "')");
			btnTXT.Attributes.Add("onclick", "javascript:window.open('" + txtFilename + "')");
			btnCSV.Attributes.Add("onclick", "javascript:window.open('" + csvFilename + "')");

			imageGoButton.Visible=false;
			lblResultsMessage.Visible=true;
			lblInstructions.Visible=false;
			afterpanel.Visible=true;
		}


	}
}
