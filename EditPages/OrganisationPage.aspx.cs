using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;
using BonoboWebControls.DataGrids;
using BonoboEngine;
using BonoboEngine.Query;
using System.Text;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for OrganisationDonorPage.
	/// </summary>
	public partial class OrganisationPage : System.Web.UI.Page
	{
		protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading2;
		protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading3;
		protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading5;
		protected BonoboWebControls.DataControls.BnbLookupControl BnbLookupControl1;
		protected BonoboWebControls.GenericControls.BnbHyperlink hypPersonnelWizard;
		protected BonoboWebControls.GenericControls.BnbHyperlink hypActionWizard;
		protected UserControls.Title titleBar;
		protected UserControls.MenuBar menuBar;

		private BnbWebFormManager bnb = null;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// re-direct to wizard if new mode
			if (this.Request.QueryString["BnbCompany"] != null &&
				this.Request.QueryString["BnbCompany"].ToLower() == "new")
			{
				//Response.Redirect("WizardNewOrganisation.aspx");
				//doRedirectToPersonnelWizard();
				doRedirectToOrganisationWizard();
			}
#if DEBUG
			if(Page.Request.QueryString.ToString() == "")
			{
				BnbWebFormManager.ReloadPage("BnbCompany=AFED0422-352F-11D7-ABC7-0008C78C38D4");	
			}
			else
			{
				bnb = new BnbWebFormManager(this, "BnbCompany");
			}
#else
			bnb = new BnbWebFormManager(this, "BnbCompany");
#endif
			//tweak logging settings
			bnb.PageNameOverride = "Organisation";
			bnb.LogPageHistory = true;
			BnbWorkareaManager.SetPageStyleOfUser(this);

            bnb.EditModeEvent += new EventHandler(EditModeEvent_Click);
            bnb.SaveEvent += new EventHandler(SaveEvent_Click);

			menuBar.SetOptionalFindParameter(5);
			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;

                if (this.Request.UrlReferrer != null)
                if (this.Request.UrlReferrer.Query.Contains("BnbProject"))
                {
                    string projectID = HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["BnbProject"].ToString();
                    BnbProject obj = BnbDomainObject.GeneralRetrieve(new Guid(projectID), typeof(BnbProject)) as BnbProject;
                    StringBuilder sb = new StringBuilder(Utilities.ProjectSubHlink);
                    sb.Replace("[PROJECTID]", projectID.ToString());
                    sb.Replace("[TITLE]", obj.ProjectTitle);
                    sb.Replace("[SUBTITLE]", bnb.PageTitleDescriptionCurrent);
                    titleBar.TitleText = sb.ToString();
                }

                // TPT Amended code PGMS- 3.3.1

                // Show the Related tab when the user Email the current page
                if (this.Request != null)
                {
                    string url = this.Request.Url.ToString();
                    if (url.IndexOf("ControlID") > -1)
                    {
                        if (url.IndexOf("Main") > -1)
                            BnbTabControl1.SelectedTabID = MainTab.ID;
                        if (url.IndexOf("Addresses") > -1)
                            BnbTabControl1.SelectedTabID = AddressesTab.ID;
                        if (url.IndexOf("Personnel") > -1)
                            BnbTabControl1.SelectedTabID = PersonnelTab.ID;
                        if (url.IndexOf("Actions") > -1)
                            BnbTabControl1.SelectedTabID = ActionsTab.ID;
                        if (url.IndexOf("Events") > -1)
                            BnbTabControl1.SelectedTabID = EventsTab.ID;
                        if (url.IndexOf("Resources") > -1)
                            BnbTabControl1.SelectedTabID = ResourcesTab.ID;
                        if (url.IndexOf("Sponsorship") > -1)
                            BnbTabControl1.SelectedTabID = SponsorshipTab.ID;
                        if (url.IndexOf("Supplier") > -1)
                            BnbTabControl1.SelectedTabID = SupplierTab.ID;
                        if (url.IndexOf("Projects") > -1)
                            BnbTabControl1.SelectedTabID = ProjectsTab.ID;
                    }

                }
                BnbCompany objCompany = bnb.GetPageDomainObject("BnbCompany") as BnbCompany;
                if (objCompany != null)
                {
                    if (objCompany.EthicallyCleared)
                        chkBxEthicallyCleared.Checked = objCompany.EthicallyCleared;
                }
			}
			doPersonnelWizardHyperLink();
			doActionWizardHyperLink();
			doPersonnelRefNoIndividualHyperlinks();
			doEventTabExtraHyperlinks();
		}
		
		private void doRedirectToOrganisationWizard()
		{
			if (this.Request.QueryString["BnbCompany"] != null &&
				this.Request.QueryString["BnbCompany"].ToLower() == "new")
				Response.Redirect("WizardNewOrganisation.aspx");
		}


		private void doPersonnelWizardHyperLink()
		{	
			string companyID = Request.QueryString["BnbCompany"];
			if(companyID == null || companyID.ToLower() == "new") 
			{
				// new mode: hide hyperlinks
				hypNewPersonnel.Visible = false;
			}
			else 
			{
				// put right guid in url
				hypNewPersonnel.Visible = true;
				hypNewPersonnel.NavigateUrl = String.Format("WizardNewPersonnel.aspx?BnbCompany={0}", companyID);
			}
		}
		private void doActionWizardHyperLink()
		{
			string companyID = Request.QueryString["BnbCompany"];
			if(companyID == null || companyID.ToLower() == "new") 
			{
				// new mode: hide hyperlinks
				hypNewAction.Visible = false;
			}
			else 
			{
				// put right guid in url
				hypNewAction.Visible = true;
				hypNewAction.NavigateUrl = String.Format("WizardNewOrgAction.aspx?BnbCompany={0}",	companyID);
			}
		}

		private void doPersonnelRefNoIndividualHyperlinks()
		{
		
			BnbDataGridHyperlink RefNoIndividualHyperCol = new BnbDataGridHyperlink();
			RefNoIndividualHyperCol.ColumnNameToRenderControl = "tblIndividualKeyInfo_old_indv_id";
			RefNoIndividualHyperCol.HrefTemplate = "IndividualPage.aspx?BnbIndividual={0}";
			RefNoIndividualHyperCol.KeyColumnNames = new string[] {"tblIndividualKeyInfo_IndividualID"};
			BnbDataGridForView2.ColumnControls.Add(RefNoIndividualHyperCol);
		}

		private void doEventTabExtraHyperlinks()
		{
		
			BnbDataGridHyperlink refNoHyperCol = new BnbDataGridHyperlink();
			refNoHyperCol.ColumnNameToRenderControl = "tblIndividualKeyInfo_old_indv_id";
			refNoHyperCol.HrefTemplate = "IndividualPage.aspx?BnbIndividual={0}";
			refNoHyperCol.KeyColumnNames = new string[] {"tblIndividualKeyInfo_IndividualID"};
			uxOrgEventsGrid.ColumnControls.Add(refNoHyperCol);

			BnbDataGridHyperlink eventHyperCol = new BnbDataGridHyperlink();
			eventHyperCol.ColumnNameToRenderControl = "tblEvent_EventDescription";
			eventHyperCol.HrefTemplate = "EventPage.aspx?BnbEvent={0}";
			eventHyperCol.KeyColumnNames = new string[] {"tblAttendance_EventID"};
			uxOrgEventsGrid.ColumnControls.Add(eventHyperCol);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			BnbDataButtons1.NewClick +=new EventHandler(BnbDataButtons1_NewClick);
            BnbDataButtons1.UndoClick += new EventHandler(BnbDataButtons1_UndoClick);
            this.BnbDataGridForView2.PreRender += new EventHandler(BnbDataGridForView2_PreRender);
			base.OnInit(e);
		}
			
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void BnbDataButtons1_NewClick(object sender, EventArgs e)
		{
			// re-directing in this way lets the wizard work out the urlreferrer
			this.SmartNavigation = false;
			Response.Redirect("WizardNewOrganisation.aspx");
		}

        #region Enable or Disable the Remove personnel link
        private void BnbDataGridForView2_PreRender(object sender, EventArgs e)
        {
            if (BnbDataGridForView2.ResultCount > 0)
                lnkBtnRemovePersonnel.Enabled = true;
            else
                lnkBtnRemovePersonnel.Enabled = false;
        }
        #endregion

        #region Method - Remove Personnel
        /// <summary>
        /// Method to remove the personnel form the organisation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnRemovePersonnel_Click(object sender, EventArgs e)
        {
            lblPersonnelWarnings.Text = string.Empty;
            lblPersonnelMessage.Text = string.Empty;

            if ((BnbDataGridForView2.SelectedDataRowID == null)
                                        || (BnbDataGridForView2.SelectedDataRowID == Guid.Empty))
                lblPersonnelWarnings.Text = "Please choose a personnel from the below list to remove";
            else
            {
                Guid indvlAddress = BnbDataGridForView2.SelectedDataRowID;
                BnbIndividualAddress indvLink = BnbIndividualAddress.Retrieve(indvlAddress);

                BnbIndividual indv = BnbIndividual.Retrieve(indvLink.IndividualID);
                //register the object for edit
                BnbEditManager em = new BnbEditManager();
                indvLink.RegisterForEdit(em);
                // mark the record for deletion
                indvLink.MarkForDeletion();

                foreach (BnbContactNumber cntNum in indv.IndividualContactNumbers)
                {
                    if ((cntNum.IndividualID == indvLink.IndividualID)
                                    && (cntNum.AddressID == indvLink.AddressID)
                                               && (cntNum.ContactNumberTypeID == BnbConst.ContactNumber_Telephone))
                    {
                        cntNum.RegisterForEdit(em);
                        cntNum.MarkForDeletion();
                    }
                }
                em.SaveChanges();
                lblPersonnelMessage.Text = "Record  removed";
            }
            BnbDataGridForView2.SelectedDataRowID = Guid.Empty;

        }
        #endregion

        private IList RetrieveList(Type type, string p)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// Disable the EthicallyCleared checkbox when the page moves to Undo mode event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BnbDataButtons1_UndoClick(object sender, EventArgs e)
        {
            chkBxEthicallyCleared.Enabled = false;
        }

        /// <summary>
        /// Save EthicallyCleared asp checkbox value to BnbCompany 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveEvent_Click(object sender, EventArgs e)
        {
            BnbCompany objCompany = bnb.GetPageDomainObject("BnbCompany") as BnbCompany;
            if (objCompany != null)
            {
                objCompany.EthicallyCleared = chkBxEthicallyCleared.Checked;
                if (chkBxEthicallyCleared.Checked && objCompany.EthicallyClearedOn == DateTime.MinValue)
                {
                    objCompany.EthicallyClearedByID = BnbEngine.SessionManager.GetSessionInfo().UserID;
                    objCompany.EthicallyClearedOn = DateTime.Now;
                }
                else if (chkBxEthicallyCleared.Checked == false)
                {
                    objCompany.EthicallyClearedByID = BnbDomainObject.NullGuid;
                    objCompany.EthicallyClearedOn = BnbDomainObject.NullDateTime;
                }
            }
        }

        /// <summary>
        /// Normally EthicallyCleared checkbox is disabled. Enable only when the page is in Edit mode event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditModeEvent_Click(object sender, EventArgs e)
        {
            chkBxEthicallyCleared.Enabled = true;
        }
	}
}

