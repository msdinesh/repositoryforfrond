<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="cc1" Namespace="BonoboWebControls.ServicesControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="ReportsPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.ReportsPage" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal><LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet"><asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent"><uc1:title id="titleBar" runat="server" imagesource="../Images/bnbcontactdetail24silver.gif"></uc1:title><uc1:navigationbar id="NavigationBar1" runat="server"></uc1:navigationbar><bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" NewButtonEnabled="False"
					NewWidth="55px" EditAllWidth="55px" SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px" NewText="New" EditAllText="Edit All" SaveText="Save" UndoText="Undo"></bnbpagecontrol:bnbdatabuttons><bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:bnbmessagebox>
				<TABLE class="fieldtable" id="Table1" width="100%" border="0">
					<TR>
						<TD class="label" style="WIDTH: 451px" width="451">Placement Ref</TD>
						<TD style="HEIGHT: 20px" width="50%"><bnbdatacontrol:bnbdomainobjecthyperlink id="BnbDomainObjectHyperlink2" runat="server" RedirectPage="PlacementPage.aspx"
								GuidProperty="BnbPosition.FullPlacementReference" HyperlinkText="[BnbDomainObjectHyperlink]" Target="_top" DataMember="BnbPosition.FullPlacementReference"></bnbdatacontrol:bnbdomainobjecthyperlink></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 451px" width="451">Contact</TD>
						<TD style="HEIGHT: 20px" width="50%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl5" runat="server" CssClass="lookupcontrol" DataMember="BnbReportVisit.ProgrammeOfficerID"
								LookupTableName="vlkuBonoboUser" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 451px" width="451">Report&nbsp;Type</TD>
						<TD style="HEIGHT: 20px" width="50%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl6" runat="server" CssClass="lookupcontrol" DataMember="BnbReportVisit.RepVisTypeID"
								LookupTableName="vlkuBonoboReportVisitTypeReportOnly" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 451px" width="451">Date sent to Vol/Emp</TD>
						<TD style="HEIGHT: 17px" width="50%"><bnbdatacontrol:bnbdatebox id="BnbDateBox10" runat="server" CssClass="datebox" DataMember="BnbReportVisit.RepVisDateDueVolEmp"
								Enabled="False" DateFormat="dd/MMM/yyyy" DateCulture="en-GB" Height="20px" Width="90px"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 451px" width="451">Date received in PO</TD>
						<TD style="HEIGHT: 10px" width="50%"><bnbdatacontrol:bnbdatebox id="BnbDateBox7" runat="server" CssClass="datebox" DataMember="BnbReportVisit.RepVisDateSentVolEmp"
								Enabled="False" DateFormat="dd/MMM/yyyy" DateCulture="en-GB" Height="20px" Width="90px"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 451px" width="451">Date sent to Rec Base</TD>
						<TD style="HEIGHT: 10px" width="50%"><bnbdatacontrol:bnbdatebox id="BnbDateBox8" runat="server" CssClass="datebox" DataMember="BnbReportVisit.RepVisDateReturned"
								Enabled="False" DateFormat="dd/MMM/yyyy" DateCulture="en-GB" Height="20px" Width="90px"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 451px" width="451">Date received in Rec Base</TD>
						<TD style="HEIGHT: 10px" width="50%"><bnbdatacontrol:bnbdatebox id="BnbDateBox6" runat="server" CssClass="datebox" DataMember="BnbReportVisit.RepVisDatSentVSOL"
								Enabled="False" DateFormat="dd/MMM/yyyy" DateCulture="en-GB" Height="20px" Width="90px"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 451px" width="451">Report filename</TD>
						<TD style="HEIGHT: 1px" width="50%"><bnbdatacontrol:bnbtextbox id="BnbTextBox9" runat="server" CssClass="textbox" DataMember="BnbReportVisit.RepVisFileName"
								Height="20px" DataType="String" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
					</TR>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
