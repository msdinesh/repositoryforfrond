using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboWebControls.DataControls;
using BonoboWebControls.Collections;
using BonoboDomainObjects;
using BonoboEngine;
using BonoboEngine.Query;
using System.Collections.Specialized;
using System.Text;

namespace Frond.EditPages
{
    public partial class AuditingHistoryPage : System.Web.UI.Page
    {
        private BnbWebFormManager bnb = null;
        private BnbProject project = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            bnb = new BnbWebFormManager(this, "BnbProject");
          
            BnbWorkareaManager.SetPageStyleOfUser(this);
            bnb.PageNameOverride = "Audit History";
            bnb.LogPageHistory = true;
            project = bnb.GetPageDomainObject("BnbProject") as BnbProject;
            doPMFStatus();
            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null && project != null)
                {
                    StringBuilder sb = new StringBuilder(Utilities.ProjectSubHlink);
                    sb.Replace("[PROJECTID]", project.ID.ToString());
                    sb.Replace("[TITLE]", project.ProjectTitle);
                    sb.Replace("[SUBTITLE]", bnb.PageTitleDescriptionCurrent);
                    titleBar.TitleText = sb.ToString();
                }
            }
            
            if (this.Request != null)
            {
                string url = this.Request.Url.ToString();
                if (url.IndexOf("Category") > -1)
                {
                 
                    if (url.IndexOf("ProjectGeneral") > -1)
                    {
                        doSetPropertyForGrid("vwProjectGeneralAuditHistory");
                        doSetLabelText("Project General");
                    }
                    else if (url.IndexOf("ProjectStatus") > -1)
                    {
                        doSetPropertyForGrid("vwProjectStatusAuditHistory");
                        doSetLabelText("Project Status");
                    }
                    else if (url.IndexOf("ProjectPMFStatus") > -1)
                    {
                        doSetPropertyForGrid("vwProjectPMFStatusAuditHistory");
                        doSetLabelText("Project PMFStatus");
                    }
                    else if (url.IndexOf("ProjectRisks") > -1)
                    {
                        doSetPropertyForGrid("vwProjectRiskAuditHistory");
                        doSetLabelText("Project Risks");
                    }
                    else if (url.IndexOf("ProjectGoals") > -1)
                    {
                        doSetPropertyForGrid("vwProjectGoalsAuditHistory");
                        doSetLabelText("Project Goals");
                    }
                    else if (url.IndexOf("ProjectProgrammeOfficeRegion") > -1)
                    {
                        doSetPropertyForGrid("vwProjectProgrameOfficeRegionAuditHistory");
                        doSetLabelText("Project Programme Office Region");
                    }
                    else if (url.IndexOf("ProposalsGeneral") > -1)
                    {
                        doSetPropertyForGrantGrid("vwProposalGeneralAuditHistory");
                        doSetLabelText("Proposals General");
                    }
                    else if (url.IndexOf("ProposalRisks") > -1)
                    {
                        doSetPropertyForGrantGrid("vwProposalRiskAuditHistory");
                        doSetLabelText("Proposal Risks");
                    }
                    else if (url.IndexOf("ProposalProgress") > -1)
                    {
                        doSetPropertyForGrantGrid("vwProposalProgressAuditHistory");
                        doSetLabelText("Proposal Progress");
                    }
                    else if (url.IndexOf("ProposalIncomePipeline") > -1)
                    {
                        doSetPropertyForGrantGrid("vwProposalIncomePipelineAuditHistory");
                        doSetLabelText("Proposal Income Pipeline");
                    }
                    else if (url.IndexOf("ContractsGeneral") > -1)
                    {
                        doSetPropertyForGrantGrid("vwContractGeneralAuditHistory");
                        doSetLabelText("Contracts General");
                    }
                    else if (url.IndexOf("ContractProgress") > -1)
                    {
                        doSetPropertyForGrantGrid("vwContractProgressAuditHistory");
                        doSetLabelText("Contract Progress");
                    }
                    else if (url.IndexOf("ContractClaim") > -1)
                    {
                        doSetPropertyForGrantGrid("vwContractClaimAuditHistory");
                        doSetLabelText("Contract Claim");
                    }
                    else if (url.IndexOf("ContractReports") > -1)
                    {
                        doSetPropertyForGrantGrid("vwContractReportAuditHistory");
                        doSetLabelText("Contract Report");
                    }
                    else if (url.IndexOf("ContractReportDueDates") > -1)
                    {
                        doSetPropertyForGrantReportDueDateGrid("vwContractReportDueDateAuditHistory");
                        doSetLabelText("Contract Report Due Date");
                    }
                    else if (url.IndexOf("ContractReportDueDateDelete") > -1)
                    {
                        doSetPropertyForGrantReportDueDateGridDelete("vwContractReportDueDateAuditHistory");
                        doSetLabelText("Contract Report Due Date");
                    }
                    else if (url.IndexOf("ProjectVSOGoals") > -1)
                    {
                        doSetPropertyForGrid("vwProjectVSOGoalsAuditHistory");
                        doSetLabelText("Project VSOGoals");
                    } 
                }
            } 
        }
        
        private void doSetPropertyForGrid(string viewNameVal)
        {
            BnbDataGridForView1.FieldName = "tblProjectAudit_ProjectID";
            BnbDataGridForView1.QueryStringKey = "BnbProject";
            BnbDataGridForView1.ViewName = viewNameVal;
        }
        private void doSetPropertyForGrantGrid(string viewNameVal1)
        {
            BnbDataGridForView1.FieldName = "tblprojectaudit_projectentityrowid";
            BnbDataGridForView1.QueryStringKey = "BnbGrant";
            BnbDataGridForView1.ViewName = viewNameVal1;
        }
        private void doSetPropertyForGrantReportDueDateGrid(string viewNameVal1)
        {
            BnbDataGridForView1.FieldName = "tblprojectaudit_projectentityrowid";
            BnbDataGridForView1.QueryStringKey = "BnbGrantReportDueDate";
            BnbDataGridForView1.ViewName = viewNameVal1;
        }
        private void doSetPropertyForGrantReportDueDateGridDelete(string viewNameVal1)
        {
            BnbDataGridForView1.FieldName = "tblprojectaudit_projectentityrowid";
            BnbDataGridForView1.QueryStringKey = "BnbGrantReport";
            BnbDataGridForView1.ViewName = viewNameVal1;
        }
        private void doSetLabelText(string headingVal)
        {
            lblAuditHeading.Text = headingVal;
        }

         protected void lnkGoToSignOffTab_Click(object sender, EventArgs e)
        {
            Session["isSignOff"] = 1;
            Response.Redirect("ProjectPage.aspx?BnbProject="+project.ID.ToString());
        }

        private void doPMFStatus()
        {
            BnbProject proj = bnb.GetPageDomainObject("BnbProject") as BnbProject;
            if (proj != null)
            {
                PMFStatusPanel2.PMFStatusID = proj.PMFStatusID.ToString();
            }
        }
    }

}