<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>

<%@ Page Language="c#" Codebehind="PrePostCheckPage.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.EditPages.PrePostCheckPage" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>PrePostCheckPage</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet" />
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">     
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/bnbindividual24silver.gif">
            </uc1:Title>
           
            <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons1" runat="server" CssClass="databuttons"
                UndoText="Undo" SaveText="Save" EditAllText="Edit All" NewText="New" HeightAllButtons="25px"
                UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px"></bnbpagecontrol:BnbDataButtons>
       
            <bnbpagecontrol:BnbMessageBox ID="BnbMessageBox1" runat="server" CssClass="messagebox">
            </bnbpagecontrol:BnbMessageBox>

            <table id="Table1" width="100%" border="0" class="fieldtable">
                <tr>
                    <td class="label" width="50%" style="height: 24px">
                        Placement Ref</td>
                    <td width="50%" style="height: 24px">
                        <bnbdatacontrol:BnbDomainObjectHyperlink ID="BnbDomainObjectHyperlink1" runat="server"
                            GuidProperty="BnbPosition.ID" HyperlinkText="[BnbDomainObjectHyperlink]" DataMember="BnbPosition.FullPlacementReference"
                            RedirectPage="PlacementPage.aspx"></bnbdatacontrol:BnbDomainObjectHyperlink>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Programme Officer</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl1" runat="server" CssClass="lookupcontrol"
                            Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                            DataMember="BnbPrePost.ProgramOfficerID" LookupTableName="vlkuBonoboUser"></bnbdatacontrol:BnbLookupControl>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Pre/Post</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl2" runat="server" CssClass="lookupcontrol"
                            Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                            DataMember="BnbPrePost.PrePostID" LookupTableName="lkuPrePost"></bnbdatacontrol:BnbLookupControl>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Type</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl3" runat="server" CssClass="lookupcontrol"
                            Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                            DataMember="BnbPrePost.PrePostTypeID" LookupTableName="lkuPrePostType"></bnbdatacontrol:BnbLookupControl>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Action</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl4" runat="server" CssClass="lookupcontrol"
                            Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                            DataMember="BnbPrePost.PrePostActionID" LookupTableName="lkuPrePostAction"></bnbdatacontrol:BnbLookupControl>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Date</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbDateBox ID="BnbDateBox1" runat="server" CssClass="datebox" Height="20px"
                            Width="90px" DateCulture="en-GB" DateFormat="dd/mm/yyyy" DataMember="BnbPrePost.PrePostDate">
                        </bnbdatacontrol:BnbDateBox>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Comments</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbTextBox ID="BnbTextBox1" runat="server" CssClass="textbox" Height="20px"
                            Width="250px" StringLength="0" Rows="3" MultiLine="True" DataMember="BnbPrePost.PrePostComments">
                        </bnbdatacontrol:BnbTextBox>
                    </td>
                </tr>
            </table>
    
        </div>
    </form>
    <p>
        &nbsp;</p>
</body>
</html>
