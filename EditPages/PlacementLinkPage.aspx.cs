using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboDomainObjects;
using BonoboWebControls;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for PlacementLinkPage.
	/// </summary>
	public partial class PlacementLinkPage : System.Web.UI.Page
	{
		protected BonoboWebControls.PageControls.BnbSectionLinks BnbSectionLinks1;
		protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox1;
				protected UserControls.Title titleBar;
		protected Frond.UserControls.NavigationBar NavigationBar1;

BnbWebFormManager bnb = null;
		
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// re-direct to wizard if new mode
			if (this.Request.QueryString["BnbApplication"] != null &&
				this.Request.QueryString["BnbService"].ToLower() == "new")
			{
				//Response.Redirect("WizardNewOrganisation.aspx");
				//doRedirectToPersonnelWizard();
				doRedirectToPlacementLinkWizard();
			}


#if DEBUG
			
			if(Request.QueryString.ToString() == "")
			{
				BnbWebFormManager.ReloadPage("BnbApplication=48791AAF-E64A-4E73-AFCD-EEABBFB08191&BnbService=0D279F3F-6A9D-4313-A875-C69C6959E025");
			}
			else 
			{
				bnb = new BnbWebFormManager(this,"BnbService");
			}
#else
			bnb = new BnbWebFormManager(this,"BnbService");
#endif

			BnbService serv = bnb.GetPageDomainObject("BnbService",true) as BnbService;
			string appID = serv.ApplicationID.ToString();

			NavigationBar1.RootDomainObject = "BnbApplication";
			NavigationBar1.RootDomainObjectID = appID;

			bnb.LogPageHistory = true;
			BnbWorkareaManager.SetPageStyleOfUser(this);
			// tweak page title settings
			bnb.PageNameOverride = "Placement Link";

			// call shared code to show any view warnings
			Utilities.ShowViewWarnings(bnb, lblViewWarnings);

			if (!this.IsPostBack)
			{
				titleBar.TitleText = "Placement Link";

				doOtherLinksCount();
			}
			
			if(serv.IsNew) bnbtxtDuration.Visible = false;
			else bnbtxtDuration.Visible = true;


		}
		private void doRedirectToPlacementLinkWizard()
		{
			if (this.Request.QueryString["BnbApplication"] != null &&
				this.Request.QueryString["BnbService"].ToLower() == "new")
				Response.Redirect("WizardNewPlacementLink.aspx?BnbApplication=" + this.Request.QueryString["BnbApplication"].ToString());
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void doInitialiseDurationControl()
		{
			BnbService serv = ((BnbService)bnb.ObjectTree.DomainObjectDictionary["BnbService"]);
			if(serv.IsNew) bnbtxtDuration.Visible = false;
			else bnbtxtDuration.Visible = true;
		}

		protected void bnbdbxStartDate_PreRender(object sender, EventArgs e)
		{
			if(bnb.PageState == PageState.New)
			{
				BnbService serv = ((BnbService)bnb.ObjectTree.DomainObjectDictionary["BnbService"]);
				if (serv.Application != null && serv.Application.IsInCountryTransfer) bnbdbxStartDate.AllowWrite = true;
			}
		}

		private void doOtherLinksCount()
		{
			uxOtherLinksLabel.Text = "";
			BnbService serv = bnb.GetPageDomainObject("BnbService") as BnbService ;
			if (serv!=null && serv.Position != null)
			{
				int otherCount = serv.Position.GetOtherLinkCount(serv.ID);
				uxOtherLinksLabel.Text = otherCount.ToString();
			}
		}

	}
}
