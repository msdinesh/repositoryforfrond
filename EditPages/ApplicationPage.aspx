<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="BonoboWebControls.ServicesControls" Assembly="BonoboWebControls" %>

<%@ Page Language="c#" Codebehind="ApplicationPage.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.EditPages.ApplicationPage" %>

<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html>
<head>
    <title></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/bnbapplication24silver.gif">
            </uc1:Title>
            <uc1:NavigationBar ID="navigationBar1" runat="server"></uc1:NavigationBar>
            <bnbpagecontrol:BnbDataButtons ID="BnbDaButtons1" runat="server" CssClass="databuttons"
                UndoText="Undo" SaveText="Save" EditAllText="Edit All" NewText="New" HeightAllButtons="25px"
                UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" NewButtonEnabled="True"
                OnNewClick="BnbDataButtons1_NewClick"></bnbpagecontrol:BnbDataButtons>
            <bnbpagecontrol:BnbMessageBox ID="BnbMessageBox1" runat="server" CssClass="messagebox">
            </bnbpagecontrol:BnbMessageBox>
            <asp:Label ID="lblViewWarnings" runat="server" CssClass="viewwarning" Visible="False"></asp:Label><bnbpagecontrol:BnbTabControl
                ID="uxTabControl" runat="server"></bnbpagecontrol:BnbTabControl>
            <asp:Panel ID="MainTab" runat="server" CssClass="tcTab" BnbTabName="Application">
                <bnbpagecontrol:BnbSectionHeading ID="BnbSectionHeading3" runat="server" CssClass="sectionheading"
                    SectionHeading="Individual Details" Collapsed="false" Height="20px" Width="100%">
                </bnbpagecontrol:BnbSectionHeading>
                <table class="fieldtable" id="Table1" border="0">
                    <tr>
                        <!-- main tab -->
                        <td class="label" width="25%">
                            Vol Ref</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox1" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DataMember="BnbApplication.Individual.RefNo" StringLength="0" Rows="1"
                                MultiLine="false"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" style="width: 101px" width="101">
                            Title</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl1" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataMember="BnbApplication.Individual.Title" StringValueType="True"
                                DataValueField="Description" LookupTableName="lkuTitle" DataTextField="Description"
                                ListBoxRows="4" LookupControlType="ReadOnlyLabel"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Full name</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox6" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DataMember="BnbApplication.Individual.Additional.Salutation" StringLength="0"
                                Rows="1" MultiLine="false" ControlType="Label"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" style="width: 101px" width="101">
                            Partner</td>
                        <td width="25%">
                            <bnbgenericcontrols:BnbHyperlink ID="uxPartnerHyper" runat="server"></bnbgenericcontrols:BnbHyperlink></td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Date of Birth</td>
                        <td style="height: 25px" width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox1" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbApplication.Individual.DateOfBirth" DateCulture="en-GB"
                                DateFormat="dd/mm/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" style="width: 101px; height: 25px" width="101">
                            Age</td>
                        <td style="height: 25px" width="25%">
                            <bnbdatacontrol:BnbDecimalBox ID="BnbDecimalBox3" runat="server" CssClass="textbox"
                                Height="20px" Width="90px" DataMember="BnbApplication.Individual.Age" TextAlign="left"
                                FormatExpression="#,0"></bnbdatacontrol:BnbDecimalBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Date of Death</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox2" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbApplication.Individual.Additional.DateOfDeath" DateCulture="en-GB"
                                DateFormat="dd/mm/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" style="width: 101px" width="101">
                            Sex</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl3" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataMember="BnbApplication.Individual.Gender" DataValueField="Abbreviation"
                                LookupTableName="lkuGender" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
                                TextAlign="Default"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Current Address Type<br>
                            <br>
                            Current Address</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl2" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.Individual.CurrentIndividualAddress.AddressTypeID"
                                LookupTableName="vlkuBonoboAddressTypeWithGroup " DataTextField="Description"
                                ListBoxRows="4" LookupControlType="ReadOnlyLabel"></bnbdatacontrol:BnbLookupControl>
                            <br>
                            <br>
                            <asp:Label ID="lblAddress" runat="server" Width="128px"></asp:Label></td>
                        <td class="label" style="width: 101px" width="101">
                            <br>
                            ID Card No<br>
                        </td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox2" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DataMember="BnbApplication.Individual.Additional.IdentityCardNo"
                                StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Email</td>
                        <td width="25%">
                            <asp:Label ID="lblEmail" runat="server" Width="184px"></asp:Label></td>
                        <td class="label" style="width: 101px" width="101">
                            Mobile</td>
                        <td width="25%">
                            <asp:Label ID="lblMobile" runat="server" Width="184px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="height: 36px" colspan="4">
                            <p align="left">
                                <bnbpagecontrol:BnbSectionHeading ID="BnbSectionHeading1" runat="server" CssClass="sectionheading"
                                    SectionHeading="Application Details" Collapsed="false" Height="20px" Width="100%">
                                </bnbpagecontrol:BnbSectionHeading>
                                <a href="IndividualPage.aspx"><strong></strong></a>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Current Placement</td>
                        <td width="25%">
                            <p>
                                <bnbdatacontrol:BnbDomainObjectHyperlink ID="BnbDomainObjectHyperlink1" runat="server"
                                    DataMember="BnbApplication.PlacedService.Position.FullPlacementReference" ButtonText="[BnbDomainObjectHyperlink]"
                                    RedirectPage="PlacementPage.aspx" GuidProperty="BnbApplication.PlacedService.Position.ID"
                                    Target="_top"></bnbdatacontrol:BnbDomainObjectHyperlink>
                            </p>
                        </td>
                        <td class="label" width="25%">
                            Application Count</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox19" runat="server" CssClass="textbox" Height="20px"
                                Width="48px" DataMember="BnbApplication.ApplicationOrderFull" StringLength="0"
                                Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                            <asp:HyperLink ID="hyperlinkPrevApplication" runat="server" Visible="False">Prev</asp:HyperLink>&nbsp;
                            <asp:HyperLink ID="hyperlinkNextApplication" runat="server" Visible="False">Next</asp:HyperLink></td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Status</td>
                        <td style="height: 23px" width="25%">
                            <p>
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl6" runat="server" CssClass="lookupcontrol"
                                    Height="22px" Width="250px" DataMember="BnbApplication.CurrentStatus.StatusID"
                                    LookupTableName="vlkuBonoboStatusFull" DataTextField="Description" ListBoxRows="4"
                                    LookupControlType="ReadOnlyLabel"></bnbdatacontrol:BnbLookupControl>
                            </p>
                        </td>
                        <td class="label" width="25%">
                            Months Completed</td>
                        <td style="height: 23px" width="25%">
                            <p>
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox14" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" DataMember="BnbApplication.MonthsCompleted" StringLength="0" Rows="1"
                                    MultiLine="false"></bnbdatacontrol:BnbTextBox>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 15px" width="25%">
                            Start of Service</td>
                        <td style="height: 15px" width="25%">
                            <p>
                                <bnbdatacontrol:BnbDateBox ID="BnbDateBox6" runat="server" CssClass="datebox" Height="20px"
                                    Width="101" DataMember="BnbApplication.SOSDate" DateCulture="en-GB" DateFormat="dd/mm/yyyy">
                                </bnbdatacontrol:BnbDateBox>
                            </p>
                        </td>
                        <td class="label" width="25%">
                            <p>
                                SOS&nbsp;Confirmed</p>
                        </td>
                        <td style="height: 15px" width="25%">
                            <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox2" runat="server" CssClass="checkbox"
                                DataMember="BnbApplication.SOSConfirmed"></bnbdatacontrol:BnbCheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            End of Service
                        </td>
                        <td style="height: 23px" width="25%">
                            <div style="display: inline; width: 70px; height: 15px" ms_positioning="FlowLayout">
                                <asp:Label ID="lblEOSDate" runat="server" Width="92px"></asp:Label></div>
                        </td>
                        <td class="label" width="25%">
                            EOS Confirmed</td>
                        <td style="height: 23px" width="25%">
                            <div style="display: inline; width: 70px; height: 15px" ms_positioning="FlowLayout">
                                <asp:Label ID="lblEOSConfirmed" runat="server" Width="92px"></asp:Label></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Months at EOS</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox15" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DataMember="BnbApplication.LengthOfService" StringLength="0" Rows="1"
                                MultiLine="false"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" width="25%">
                            Extension</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl7" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataMember="BnbApplication.ExtensionStatusID" LookupTableName="lkuExtensionStatus"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Enquiry&nbsp;Received By</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl8" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.EnquiryUserID" LookupTableName="vlkuBonoboUser"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                        <td class="label" width="25%">
                            Enquiry Received On</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox7" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbApplication.EnquiryDate" DateCulture="en-GB" DateFormat="dd/MMM/yyyy">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Recruitment Base</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl9" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.RecruitmentBaseID" LookupTableName="vlkuRecruitmentBase"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                        <td class="label" width="25%">
                            Recruited By</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl40" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.RecruitedByID" LookupTableName="vlkuRecruitedBy"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Available From</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox8" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbApplication.AvailabilityDate" DateCulture="en-GB"
                                DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            VSO Type</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl10" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.VSOTypeID" LookupTableName="vlkuBonoboVSOTypeWithGroup"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Enquiry Method</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl11" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.EnquiryMethodID" LookupTableName="lkuEnquiryMethod"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                        <td class="label" width="25%">
                            Application Received By</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl12" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.ApplicationUserID" LookupTableName="vlkuBonoboUser"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Application Received On</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox11" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbApplication.ApplicationDate" DateCulture="en-GB"
                                DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            Sent To</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl13" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.SkillTeamID" LookupTableName="lkuSkillTeam"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Application Contact</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl14" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.ContactUserID" LookupTableName="vlkuBonoboUser"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                        <td class="label" width="25%">
                            Posting Contact</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl25" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.PositionContactID" LookupTableName="vlkuBonoboUser"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Application Method</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl19" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.ApplicationSourceID" LookupTableName="lkuApplicationSource"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                        <%-- <td class="label" width="25%">
                            Security</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox3" runat="server" CssClass="checkbox"
                                DataMember="BnbApplication.Security"></bnbdatacontrol:BnbCheckBox>
                        </td>--%>
                    </tr>
                    <%-- <tr>
                       
                        <td class="label" width="25%">
                            Security Date
                        </td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox13" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbApplication.SecurityDate" DateCulture="en-GB" DateFormat="dd/MMM/yyyy">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            Security Clearance By&nbsp;</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl15" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.SecurityUserID" LookupTableName="vlkuBonoboUser"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
                                Enabled="False"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="label" width="25%">
                            LSO</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl18" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.LsoID" LookupTableName="lkuLSO" DataTextField="Description"
                                ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Sources</td>
                        <td style="height: 23px" colspan="3">
                            <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList6"
                                runat="server" CssClass="datagrid" DataMember="BnbApplication.ApplicationSources"
                                DataGridType="Editable" SortBy="ID" VisibleOnNewParentRecord="false" AddButtonVisible="true"
                                EditButtonText="Edit" AddButtonText="Add" EditButtonVisible="true" DeleteButtonVisible="false"
                                DataGridForDomainObjectListType="Editable"></bnbdatagrid:BnbDataGridForDomainObjectList>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Source Other</td>
                        <td width="50%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox40" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DataMember="BnbApplication.SourceOther" StringLength="0" Rows="1"
                                MultiLine="false"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                </table>
                <br>
                <cc1:BnbWordFormButton ID="cmdTravelReqWordForm" runat="server" CssClass="button"
                    AutoPostBack="True" Text="Travel Req" OnClicked="cmdTravelReqWordForm_Clicked"></cc1:BnbWordFormButton>
            </asp:Panel>
            <asp:Panel ID="ApplicationMainTab" runat="server" CssClass="tcTab" BnbTabName="Application Main">
                <bnbpagecontrol:BnbSectionHeading ID="BnbSectionHeading2" runat="server" CssClass="sectionheading"
                    SectionHeading="Individual Details" Collapsed="false" Height="20px" Width="100%">
                </bnbpagecontrol:BnbSectionHeading>
                <table class="fieldtable" id="Table5" border="0">
                    <tr>
                        <!-- main tab -->
                        <td class="label" width="25%">
                            Vol Ref</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox4" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DataMember="BnbApplication.Individual.RefNo" StringLength="0" Rows="1"
                                MultiLine="false"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" style="width: 101px" width="101">
                            Title</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl4" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataMember="BnbApplication.Individual.Title" StringValueType="True"
                                DataValueField="Description" LookupTableName="lkuTitle" DataTextField="Description"
                                ListBoxRows="4" LookupControlType="ReadOnlyLabel"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Full name</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox5" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DataMember="BnbApplication.Individual.Additional.Salutation" StringLength="0"
                                Rows="1" MultiLine="false" ControlType="Label"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" style="width: 101px" width="101">
                            Partner</td>
                        <td width="25%">
                            <bnbgenericcontrols:BnbHyperlink ID="uxPartnerHyper1" runat="server"></bnbgenericcontrols:BnbHyperlink></td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Date of Birth</td>
                        <td style="height: 25px" width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox3" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbApplication.Individual.DateOfBirth" DateCulture="en-GB"
                                DateFormat="dd/mm/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" style="width: 101px; height: 25px" width="101">
                            Age</td>
                        <td style="height: 25px" width="25%">
                            <bnbdatacontrol:BnbDecimalBox ID="BnbDecimalBox4" runat="server" CssClass="textbox"
                                Height="20px" Width="90px" DataMember="BnbApplication.Individual.Age" TextAlign="left"
                                FormatExpression="#,0"></bnbdatacontrol:BnbDecimalBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Date of Death</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox4" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbApplication.Individual.Additional.DateOfDeath" DateCulture="en-GB"
                                DateFormat="dd/mm/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" style="width: 101px" width="101">
                            Sex</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl5" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataMember="BnbApplication.Individual.Gender" DataValueField="Abbreviation"
                                LookupTableName="lkuGender" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
                                TextAlign="Default"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Current Address Type<br>
                            <br>
                            Current Address</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl22" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.Individual.CurrentIndividualAddress.AddressTypeID"
                                LookupTableName="vlkuBonoboAddressTypeWithGroup " DataTextField="Description"
                                ListBoxRows="4" LookupControlType="ReadOnlyLabel"></bnbdatacontrol:BnbLookupControl>
                            <br>
                            <br>
                            <asp:Label ID="lblAddress1" runat="server" Width="128px"></asp:Label></td>
                        <td class="label" style="width: 101px" width="101">
                            <br>
                            ID Card No<br>
                        </td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox7" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DataMember="BnbApplication.Individual.Additional.IdentityCardNo"
                                StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Email</td>
                        <td width="25%">
                            <asp:Label ID="lblEmail1" runat="server" Width="184px"></asp:Label></td>
                        <td class="label" style="width: 101px" width="101">
                            Mobile</td>
                        <td width="25%">
                            <asp:Label ID="lblMobile1" runat="server" Width="184px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="height: 36px" colspan="4">
                            <p align="left">
                                <bnbpagecontrol:BnbSectionHeading ID="BnbSectionHeading4" runat="server" CssClass="sectionheading"
                                    SectionHeading="Application Details" Collapsed="false" Height="20px" Width="100%">
                                </bnbpagecontrol:BnbSectionHeading>
                                <a href="IndividualPage.aspx"><strong></strong></a>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 25px" width="25%">
                            Current Placement</td>
                        <td style="height: 25px" width="25%">
                            <p>
                                <bnbdatacontrol:BnbDomainObjectHyperlink ID="BnbDomainObjectHyperlink2" runat="server"
                                    DataMember="BnbApplication.PlacedService.Position.FullPlacementReference" Target="_top"
                                    GuidProperty="BnbApplication.PlacedService.Position.ID" RedirectPage="PlacementPage.aspx"
                                    ButtonText="[BnbDomainObjectHyperlink]"></bnbdatacontrol:BnbDomainObjectHyperlink>
                            </p>
                        </td>
                        <td class="label" style="height: 25px" width="25%">
                            Application Count</td>
                        <td style="height: 25px" width="25%">
                            <p>
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox8" runat="server" CssClass="textbox" Width="48px"
                                    Height="20px" MultiLine="false" Rows="1" StringLength="0" DataMember="BnbApplication.ApplicationOrderFull">
                                </bnbdatacontrol:BnbTextBox>
                                &nbsp;
                                <asp:HyperLink ID="hyperlinkPrevApplication1" runat="server" Visible="False">Prev</asp:HyperLink>&nbsp;
                                <asp:HyperLink ID="hyperlinkNextApplication1" runat="server" Visible="False">Next</asp:HyperLink></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 26px" width="25%">
                            Status</td>
                        <td style="height: 26px" width="25%">
                            <p>
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl23" runat="server" CssClass="lookupcontrol"
                                    Width="250px" Height="22px" DataMember="BnbApplication.CurrentStatus.StatusID"
                                    LookupControlType="ReadOnlyLabel" ListBoxRows="4" DataTextField="Description"
                                    LookupTableName="vlkuBonoboStatusFull"></bnbdatacontrol:BnbLookupControl>
                            </p>
                        </td>
                        <td class="label" style="height: 26px" width="25%">
                            Start of Service Date</td>
                        <td style="height: 26px" width="25%">
                            <p>
                                <bnbdatacontrol:BnbDateBox ID="BnbDateBox5" runat="server" CssClass="datebox" Width="90px"
                                    Height="20px" DataMember="BnbApplication.SOSDate" DateFormat="dd/mm/yyyy" DateCulture="en-GB">
                                </bnbdatacontrol:BnbDateBox>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Start of Service Confirmed</td>
                        <td width="25%">
                            <br>
                            <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox7" runat="server" CssClass="checkbox"
                                DataMember="BnbApplication.SOSConfirmed"></bnbdatacontrol:BnbCheckBox>
                            <br>
                        </td>
                        <td class="label" width="25%">
                            End of Service Date</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox10" runat="server" CssClass="datebox" Width="90px"
                                Height="20px" DataMember="BnbApplication.EOSDate" DateFormat="dd/mm/yyyy" DateCulture="en-GB">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            End of Service Confirmed</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbCheckBox ID="checkBoxEOSConfirmed" runat="server" CssClass="checkbox"
                                DataMember="BnbApplication.EOSConfirmed"></bnbdatacontrol:BnbCheckBox>
                        </td>
                        <td class="label" width="25%">
                            Months Completed</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox9" runat="server" CssClass="textbox" Width="250px"
                                Height="20px" MultiLine="false" Rows="1" StringLength="0" DataMember="BnbApplication.MonthsCompleted">
                            </bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Months at EOS</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox10" runat="server" CssClass="textbox" Width="250px"
                                Height="20px" MultiLine="false" Rows="1" StringLength="0" DataMember="BnbApplication.LengthOfService">
                            </bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" width="25%">
                            Extension</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl24" runat="server" CssClass="lookupcontrol"
                                Width="250px" Height="22px" DataMember="BnbApplication.ExtensionStatusID" LookupControlType="ComboBox"
                                ListBoxRows="4" DataTextField="Description" LookupTableName="lkuExtensionStatus">
                            </bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="MedicalTab" runat="server" CssClass="tcTab" BnbTabName="Medical">
                <table class="fieldtable" id="Table3" cellspacing="1" cellpadding="1">
                    <tr>
                        <td class="label" valign="middle" width="25%">
                            Medical Clearance Type</td>
                        <td style="height: 70px; vertical-align: top; padding-top: 5px" colspan="3" width="25%">
                            <asp:Label ID="lblClearanceType" runat="server"></asp:Label>
                            <asp:RadioButton ID="rdoMCNone" runat="server" GroupName="ClearanceType" Text="None" /><br />
                            <asp:RadioButton ID="rdoFull" runat="server" GroupName="ClearanceType" Text="Medically cleared for any placement" /><br />
                            <asp:RadioButton ID="rdoPartial" runat="server" GroupName="ClearanceType" Text="Requires medical clearance for specific placement" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Medical Clearance Date</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbDateBox ID="dtMedicalClearance" runat="server" CssClass="datebox"
                                Height="20px" Width="90px" DataMember="BnbApplication.MedicalClearanceDate" DateCulture="en-GB"
                                DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            Medical Valid For (months)
                        </td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbDecimalBox ID="BnbDecimalBox20" runat="server" CssClass="textbox"
                                Height="20px" Width="24px" DataMember="BnbApplication.MedicalValidMonths" TextAlign="left"
                                FormatExpression="#,0"></bnbdatacontrol:BnbDecimalBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Ovs Medical clearance</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox90" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbApplication.MedicalClearanceOverseas" DateCulture="en-GB"
                                DateFormat="dd/mm/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            Ovs Medical Clearance Duration</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbDecimalBox ID="BnbDecimalBox10" runat="server" CssClass="textbox"
                                Height="20px" Width="24px" DataMember="BnbApplication.MedicalValidMonthsOverseas"
                                TextAlign="left" FormatExpression="#,0"></bnbdatacontrol:BnbDecimalBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Medical Clearance By</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="lkuClearedBy" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.MedicalUserID" LookupTableName="vlkuBonoboUser"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
                                Enabled="False"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr id="sectionMedicalReq" runat="server">
                        <td class="label" width="25%">
                            Medical Requirements</td>
                        <td colspan="3">
                            <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbGridMedicalRequirements" runat="server"
                                CssClass="datagrid" DataMember="BnbApplication.ApplicationMedicalRequirements"
                                Width="416px" DataGridType="Editable" DataGridForDomainObjectListType="Editable"
                                VisibleOnNewParentRecord="false" SortBy="ID" AddButtonText="Add" AddButtonVisible="true"
                                DataProperties="MedicalRequirementID [BnbLookupControl:lkuMedicalRequirement]"
                                EditButtonText="Edit" EditButtonVisible="true" DeleteButtonVisible="True"></bnbdatagrid:BnbDataGridForDomainObjectList>
                        </td>
                    </tr>
                </table>
                <div id="sectionPartialClearance" runat="server">
                    <p>
                        <bnbpagecontrol:BnbSectionHeading ID="BnbSectionHeading6" runat="server" CssClass="sectionheading"
                            SectionHeading="Placements for review" Collapsed="false" Height="20px" Width="100%">
                        </bnbpagecontrol:BnbSectionHeading>
                        <bnbdatagrid:BnbDataGridForView ID="bnbPlacementsForReview" runat="server" CssClass="datagrid"
                            Width="40%" RedirectPage="PlacementLinkPage.aspx" ViewLinksVisible="true" DataGridType="ViewOnly"
                            DomainObject="BnbService" GuidKey="tblService_ServiceID" FieldName="tblService_ApplicationID"
                            QueryStringKey="BnbApplication" ViewName="vwPlacementsForReview" NewLinkVisible="false"
                            ColumnNames="Custom_FullPositionReference,Custom_LinkedBy" DisplayNoResultMessageOnly="True"
                            DisplayResultCount="false" ShowResultsOnNewButtonClick="false" NoResultMessage="(No Placements available for review)">
                        </bnbdatagrid:BnbDataGridForView>
                    </p>
                    <p>
                        <bnbpagecontrol:BnbSectionHeading ID="BnbSectionHeading5" runat="server" CssClass="sectionheading"
                            SectionHeading="Final medical clearance given for placement" Collapsed="false"
                            Height="20px" Width="100%"></bnbpagecontrol:BnbSectionHeading>
                        <bnbdatagrid:BnbDataGridForView ID="BnbdatagridforviewForMedicalClearance" runat="server"
                            CssClass="datagrid" Width="100%" DomainObject="BnbServiceMedicalClearance" FieldName="tblService_ApplicationID"
                            QueryStringKey="BnbApplication" RedirectPage="AppMedicalClearancePage.aspx" ViewLinksVisible="true"
                            ViewName="vwApplicationFinalMedicalClearances" NewLinkVisible="true" NewLinkText="New Final medical clearance given for placement"
                            GuidKey="lnkServiceMedicalClearance_ServiceMedicalClearanceID" ColumnNames="Custom_FullPositionReference,lnkServiceMedicalClearance_ClearanceDate,appUser_UserName"
                            DisplayNoResultMessageOnly="True" DisplayResultCount="false" ShowResultsOnNewButtonClick="false"
                            ViewLinksText="View" NoResultMessage="(No Final medical clearance given for placement for this Application)">
                        </bnbdatagrid:BnbDataGridForView>
                    </p>
                </div>
            </asp:Panel>
            <asp:Panel ID="BackgroundTab" runat="server" CssClass="tcTab" BnbTabName="Background">
                <table class="fieldtable" id="Table6" cellspacing="1" cellpadding="1">
                    <tr>
                       <td class="label" width="25%">
                            Background Clearance Type</td>
                        <td width="25%" colspan ="3">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl15" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.SecurityClearanceTypeID" LookupTableName="lkuSecurityClearanceType"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr> <td class="label" width="25%">
                            Directorate Clearance
                        </td>
                    <td>
                     <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox1" runat="server" CssClass="checkbox"
                                DataMember="BnbApplication.DirectorateClearance"></bnbdatacontrol:BnbCheckBox>
                                </td>
                    </tr>
                    <tr>
                       
                        <td class="label" width="25%">
                            Background Clearance Date
                        </td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox13" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbApplication.SecurityDate" DateCulture="en-GB" DateFormat="dd/MMM/yyyy">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            Background Clearance By&nbsp;</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="lkuSecurityClearanceBy" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.SecurityUserID" LookupTableName="vlkuBonoboUser"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
                                Enabled="False"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
             <asp:Panel ID="TermsAndConditionTab" runat="server" CssClass="tcTab" BnbTabName="T&amp;C">
                <table class="fieldtable" id="Table7" cellspacing="1" cellpadding="1">
                    <tr>
                       <td class="label" width="25%">
                            Entitled to HCFA</td>
                        <td width="25%">
                         <asp:DropDownList runat="server" ID ="cmbTandC"></asp:DropDownList>
                         <asp:Label runat="server" ID="lblTAndC"></asp:Label>
                        </td>
                    </tr>
                    <tr> <td class="label" width="25%">
                            Start of payment
                        </td>
                     <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbDateBox ID="txtStartOfPayment" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbApplication.StartOfPayment" DateCulture="en-GB" DateFormat="dd/MMM/yyyy">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                    <tr>                       
                        <td class="label" width="25%">
                            Preparation payment status
                        </td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="cmbPaymentStatus" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.PrepPaymentStatusID" LookupTableName="lkuPrepPaymentStatus"
                                DataTextField="Description" SortByDisplayOrder =true ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="EOSTab" runat="server" CssClass="tcTab" BnbTabName="End Of Service">
                <table class="fieldtable" id="Table2" cellspacing="1" cellpadding="1">
                    <tr>
                        <td class="label" width="25%">
                            End of Service Form In
                        </td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox14" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbApplication.EOSFormInDate" DateCulture="en-GB" DateFormat="dd/MMM/yyyy">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            Home Base Return</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox17" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbApplication.HomeReturnDate" DateCulture="en-GB" DateFormat="dd/MMM/yyyy">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            End of Service</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox15" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbApplication.EOSDate" DateCulture="en-GB" DateFormat="dd/MMM/yyyy">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            End of Service Confimed</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox6" runat="server" CssClass="checkbox"
                                DataMember="BnbApplication.EOSConfirmed"></bnbdatacontrol:BnbCheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Extension</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl21" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.ExtensionStatusID" LookupTableName="lkuExtensionStatus"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                        <td class="label" width="25%">
                            Planned/Actual Duration</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox20" runat="server" CssClass="textbox" Height="21"
                                Width="29" DataMember="BnbApplication.PlacedService.Position.Duration" StringLength="0"
                                Rows="1" MultiLine="false" ControlType="Text"></bnbdatacontrol:BnbTextBox>
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox21" runat="server" CssClass="textbox" Height="21"
                                Width="29" DataMember="BnbApplication.MonthsCompleted" StringLength="0" Rows="1"
                                MultiLine="false" ControlType="Text"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Return Code 1</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl16" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.ReturnCodeOneID" LookupTableName="lkuReturnCodeOne"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" AutoPostBack="True">
                            </bnbdatacontrol:BnbLookupControl>
                        </td>
                        <td class="label" width="25%">
                            Return Code 2</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl17" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.ReturnCodeTwoID" LookupTableName="lkuReturnCodeTwo"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" AutoPostBack="False"
                                ParentControlID="BnbLookupControl16" ColumnRelatedToParent="ReturnCodeOneID"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Suitable for Briefing?</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox5" runat="server" CssClass="checkbox"
                                DataMember="BnbApplication.SuitableForBriefing"></bnbdatacontrol:BnbCheckBox>
                        </td>
                        <td class="label" width="25%">
                            Reference Required?</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox4" runat="server" CssClass="checkbox"
                                DataMember="BnbApplication.ReferenceRequired"></bnbdatacontrol:BnbCheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Reference Recieved</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox16" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbApplication.ReferenceReceivedDate" DateCulture="en-GB"
                                DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            More / Less</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl20" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbApplication.ReturnDetailRankingID" LookupTableName="lkuReturnDetailRanking"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" SortByDisplayOrder="True">
                            </bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%" colspan="2">
                            Significant Risk at Placement Assessment
                            <td style="height: 23px" colspan="3">
                                <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox10" runat="server" CssClass="checkbox"
                                    DataMember="BnbApplication.PlacementRisk"></bnbdatacontrol:BnbCheckBox>
                            </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 30px" width="25%" colspan="2">
                            Vol Suitability Risks Raised by PO with Skill Team</td>
                        <td style="height: 30px" width="25%" colspan="2">
                            <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox8" runat="server" CssClass="checkbox"
                                DataMember="BnbApplication.VolunteerRisk"></bnbdatacontrol:BnbCheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 30px" width="25%" colspan="2">
                            Discussed with Volunteer?</td>
                        <td style="height: 30px" width="25%" colspan="2">
                            <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox9" runat="server" CssClass="checkbox"
                                DataMember="BnbApplication.DiscussedWithVol"></bnbdatacontrol:BnbCheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%" colspan="1" rowspan="4">
                            <strong>Return Details</strong></td>
                        <td width="25%" colspan="4">
                            <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList7"
                                runat="server" CssClass="datagrid" DataMember="BnbApplication.ApplicationReturnDetails"
                                DataGridType="Editable" SortBy="ID" VisibleOnNewParentRecord="false" AddButtonVisible="true"
                                EditButtonText="Edit" AddButtonText="Add" EditButtonVisible="true" DeleteButtonVisible="false"
                                DataGridForDomainObjectListType="Editable" DataProperties="ReturnDetailSubID [BnbLookupControl:vlkuBonoboReturnDetailMainAndSub], ReturnDetailRatingID [BnbLookupControl:lkuReturnDetailRating]">
                            </bnbdatagrid:BnbDataGridForDomainObjectList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="CommentsTab" runat="server" CssClass="tcTab" BnbTabName="Comments">
                <table class="fieldtable" id="Table9">
                    <tr>
                        <td class="label" style="width: 281px; height: 204px">
                            <p>
                                Individual Comments</p>
                        </td>
                        <td style="height: 204px">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox3" runat="server" CssClass="textbox" Height="96px"
                                Width="432px" DataMember="BnbApplication.Individual.Additional.Comments" StringLength="0"
                                Rows="10" MultiLine="True" ControlType="Text"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width: 281px; height: 204px">
                            <p>
                                Job Comments</p>
                        </td>
                        <td>
                            <bnbdatacontrol:BnbTextBox ID="uxRoleComments" runat="server" CssClass="textbox"
                                Height="20px" Width="432px" DataMember="BnbApplication.PlacedService.Position.Role.Comments"
                                StringLength="0" Rows="10" MultiLine="True" ControlType="Label"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br>
            <asp:Panel ID="AppStatusTab" runat="server" CssClass="tcTab" BnbTabName="App Status">
                <p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView1" runat="server" CssClass="datagrid"
                        Width="100%" RedirectPage="AppStatusPage.aspx" NewLinkText="New Application Status"
                        NewLinkVisible="true" ViewLinksVisible="true" ViewName="vwBonoboApplicationStatuses"
                        DomainObject="BnbApplicationStatus" GuidKey="lnkApplicationStatus_ApplicationStatusID"
                        FieldName="lnkApplicationStatus_ApplicationID" QueryStringKey="BnbApplication"
                        ViewLinksText="View" ShowResultsOnNewButtonClick="false" DisplayResultCount="false"
                        DisplayNoResultMessageOnly="false"></bnbdatagrid:BnbDataGridForView>
                </p>
            </asp:Panel>
            <asp:Panel ID="PlacementLinksTab" runat="server" CssClass="tcTab" BnbTabName="Placement Links">
                <p>
                    <asp:HyperLink ID="hyperlinkICT" runat="server">In Country Transfer Wizard</asp:HyperLink>
                    <asp:HyperLink ID="hyperlinkNewPlacementLink" runat="server">New Placement Link</asp:HyperLink><br>
                    <br>
                    <bnbdatagrid:BnbDataGridForView ID="uxPlacementLinkDataGrid" runat="server" CssClass="datagrid"
                        Width="100%" RedirectPage="PlacementLinkPage.aspx" NewLinkText="New Placement Link"
                        NewLinkVisible="False" ViewLinksVisible="True" ViewName="vwBonoboApplicationServices2"
                        DomainObject="BnbService" GuidKey="tblService_ServiceID" FieldName="tblService_ApplicationID"
                        QueryStringKey="BnbApplication" ViewLinksText="View" ShowResultsOnNewButtonClick="false"
                        DisplayResultCount="false" DisplayNoResultMessageOnly="false" ReturnToHostedPage="True"
                        ColumnNames="tblEmployer_EmployerName, tblRole_RoleName, Custom_FullPositionReference, lkuServiceStatus_Description, tblPosition_EarliestStartDate, tblService_PlacementStartDate, tblService_PlacementEndDate">
                    </bnbdatagrid:BnbDataGridForView>
                </p>
            </asp:Panel>
            <asp:Panel ID="FundingTab" runat="server" CssClass="tcTab" BnbTabName="Funding">
                <div class="sectionheading">
                    Sponsorship</div>
                <p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView5" runat="server" CssClass="datagrid"
                        Width="100%" RedirectPage="FundingPage.aspx" NewLinkText="New Sponsorship" NewLinkVisible="True"
                        ViewLinksVisible="true" ViewName="vwBonoboApplicationFunding" DomainObject="BnbFunding"
                        GuidKey="tblFunding_FundingID" FieldName="tblFunding_ApplicationID" QueryStringKey="BnbApplication"
                        DisplayNoResultMessageOnly="True" NoResultMessage="(No Sponsorship records for this Application)">
                    </bnbdatagrid:BnbDataGridForView>
                </p>
                <table class="fieldtable" id="Table4" width="100%" border="0">
                    <tr>
                        <td class="label" style="height: 17px" width="25%">
                            Total for job</td>
                        <td style="height: 17px" width="25%">
                            <p>
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox13" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" DataMember="BnbApplication.FundingPlacementAllYears" StringLength="0"
                                    Rows="1" MultiLine="false" FormatExpression="�#,#0.00"></bnbdatacontrol:BnbTextBox>
                            </p>
                        </td>
                        <td class="label" style="height: 17px" width="25%">
                            FY&nbsp; for job&nbsp;</td>
                        <td style="height: 17px" width="25%">
                            <asp:Label ID="lblFundingPlacementSelectedYear" runat="server" DESIGNTIMEDRAGDROP="615"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            for volunteer</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox17" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DataMember="BnbApplication.FundingVolunteerAllYears" StringLength="0"
                                Rows="1" MultiLine="false" FormatExpression="�#,#0.00"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" width="25%">
                            for volunteer</td>
                        <td width="25%">
                            <p>
                                <asp:Label ID="lblFundingVolunteerSelectedYear" runat="server" DESIGNTIMEDRAGDROP="623"></asp:Label></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Both</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox18" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DataMember="BnbApplication.FundingTotalAllYears" StringLength="0"
                                Rows="1" MultiLine="false" FormatExpression="�#,#0.00"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" width="25%">
                            Both</td>
                        <td width="25%">
                        </td>
                    </tr>
                </table>
                <div class="sectionheading">
                    Programme Funding</div>
                <p>
                    <asp:HyperLink ID="hypNewProgrammeFunding" runat="server">New Programme Funding</asp:HyperLink></p>
                <p>
                    <bnbdatagrid:BnbDataGridForView ID="Bnbdatagridforview2" runat="server" CssClass="datagrid"
                        Width="100%" RedirectPage="ProgrammeFundingPage.aspx" NewLinkText="New Programme Funding"
                        NewLinkVisible="False" ViewLinksVisible="True" ViewName="vwBonoboProgrammeFunding"
                        DomainObject="BnbProgrammeFunding" GuidKey="tblProgrammeFunding_ProgrammeFundingID"
                        FieldName="tblApplication_ApplicationID" QueryStringKey="BnbApplication" ViewLinksText="View"
                        ShowResultsOnNewButtonClick="false" DisplayResultCount="false" DisplayNoResultMessageOnly="True"
                        ReturnToHostedPage="True" ColumnNames="lkuFundingType_Description, tblFundCode_FundCode,tblProgrammeFunding_FundingStartDate,tblProgrammeFunding_FundingEndDate, Custom_FullPositionReference"
                        NoResultMessage="(No Programme Funding records for this Application)"></bnbdatagrid:BnbDataGridForView>
                </p>
            </asp:Panel>
            <asp:Panel ID="PaperMoveTab" runat="server" CssClass="tcTab" BnbTabName="Paper Move">
                <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView6" runat="server" CssClass="datagrid"
                    Width="100%" RedirectPage="PaperMovePage.aspx" NewLinkText="New Paper Move" NewLinkVisible="true"
                    ViewLinksVisible="true" ViewName="vwBonoboApplicationPaperMove" DomainObject="BnbPaperMove"
                    GuidKey="tblPaperMove_PaperMoveID" FieldName="tblApplication_ApplicationID" QueryStringKey="BnbApplication"
                    ViewLinksText="View" ShowResultsOnNewButtonClick="false" DisplayResultCount="false"
                    DisplayNoResultMessageOnly="false" ColumnNames="lkuRegistryType_Description, lkuRegistryLocation_Description, tblPaperMove_old_papmove_id, Custom_BorrowedBy, tblPaperMove_LoanedDate, tblPaperMove_DueBackDate">
                </bnbdatagrid:BnbDataGridForView>
            </asp:Panel>
            <asp:Panel ID="PaymentsTab" runat="server" CssClass="tcTab" BnbTabName="Payments">
                <p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView7" runat="server" CssClass="datagrid"
                        Width="100%" RedirectPage="PaymentPage.aspx" NewLinkText="New Payment" NewLinkVisible="true"
                        ViewLinksVisible="true" ViewName="vwBonoboApplicationPayments" DomainObject="BnbAccountHeader"
                        GuidKey="tblAccountHeader_AccountHeaderID" FieldName="tblAccountHeader_ApplicationID"
                        QueryStringKey="BnbApplication" ViewLinksText="View" ShowResultsOnNewButtonClick="false"
                        DisplayResultCount="false" DisplayNoResultMessageOnly="false" NewLinkReturnToParentPageDisabled="True">
                    </bnbdatagrid:BnbDataGridForView>
                </p>
                <cc1:BnbWordFormButton ID="cmdWFrmBenefitSummary" runat="server" CssClass="button"
                    Text="Benefit Summary Advice" OnClicked="cmdWFrmBenefitSummary_Clicked"></cc1:BnbWordFormButton>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
