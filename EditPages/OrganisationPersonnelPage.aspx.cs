using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;
using BonoboEngine;
using BonoboWebControls.DataGrids;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for OrganisationPersonnelPage.
    /// </summary>
    public partial class OrganisationPersonnelPage : System.Web.UI.Page
    {

        private BnbWebFormManager bnb = null;
        private bool isnewperson = false;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here

#if DEBUG
            if (Request.QueryString.ToString() == "")
                BnbWebFormManager.ReloadPage("BnbCompany=3B97A9A4-A1DA-11D5-AB1E-0008C7E0D9D0&BnbIndividualAddress=35360584-49AA-11D7-9100-00902790DDAE");
            else
                bnb = new BnbWebFormManager(this, "BnbIndividualAddress");
#else
			bnb = new BnbWebFormManager(this,"BnbIndividualAddress");
#endif
            isnewperson = (Request.QueryString["PageState"] != null);
            bnbtxtRole.Visible = !isnewperson;
            bnbtxtRefNo.Visible = !isnewperson;
            bnbtxtDept.Visible = !isnewperson;
            txtRefNo.Visible = isnewperson;
            txtRole.Visible = isnewperson;
            txtDept.Visible = isnewperson;

              // turn on logging for this page
            bnb.LogPageHistory = true;
            //<Integartion>
            // tweak page title settings
            bnb.PageNameOverride = "Organisation Personnel";
            BnbWorkareaManager.SetPageStyleOfUser(this);

            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null)
                    titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
            }
            //</Integartion>
            
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            this.BnbDataButtons1.SaveClick += new EventHandler(BnbDataButtons_SaveClick);
            this.PreRender += new EventHandler(Page_PreRender);
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        private void BnbDataButtons_SaveClick(object sender, EventArgs e)
        {
            BnbDomainObject bnbobject = null;
            if (isnewperson)
            {
                BnbIndividual ind = null;
                ind = BnbIndividual.RetrieveByRefNo(txtRefNo.Text.Trim());
                if (ind == null)
                {
                    lblRefNo.Text = "Could not find individual";
                    Page_PreRender(this, null);
                    return;
                }


                for (int i = 0; i < bnb.ObjectTree.AllDomainObjects.Count; i++)
                {
                    bnbobject = bnb.ObjectTree.AllDomainObjects[i];
                    if (bnbobject.ClassName == "BnbIndividual") bnb.ObjectTree.AllDomainObjects[i] = ind;
                }
            }

            System.Guid guid = bnbvwWorkAddresses.SelectedDataRowID;
            if (guid == System.Guid.Empty) return;

            BnbAddress add = BnbAddress.Retrieve(guid);

            // get ID from AddressID
            for (int i = 0; i < bnb.ObjectTree.AllDomainObjects.Count; i++)
            {
                bnbobject = bnb.ObjectTree.AllDomainObjects[i];
                if (bnbobject is BnbIndividualAddress)
                {
                    ((BnbIndividualAddress)bnb.ObjectTree.AllDomainObjects[i]).Address = add;
                    return;
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            System.Guid addid = ((BnbIndividualAddress)bnb.ObjectTree.DomainObjectDictionary["BnbIndividualAddress"]).AddressID;
            switch (bnb.PageState)
            {
                case PageState.Save:
                    if (isnewperson) goto here;
                    else goto there;
                    break;
                case PageState.New:
                here:
                    bnbvwWorkAddresses.FieldName = "lnkCompanyAddress_CompanyID";
                    bnbvwWorkAddresses.QueryStringKey = "BnbCompany";
                    if (addid != System.Guid.Empty) bnbvwWorkAddresses.SelectedDataRowID = addid;
                    bnbvwWorkAddresses.DataGridType = DataGridForViewType.RadioButtonList;
                    break;

                default:
                there:
                    bnbvwWorkAddresses.DataGridType = DataGridForViewType.ViewOnly;
                    bnbvwWorkAddresses.FieldName = "lnkCompanyAddress_AddressID";

                    // get BnbAddress id from BnbIndividualID
                    bnbvwWorkAddresses.GuidToFilterResult = addid;
                    break;
            }
        }
    }
}
