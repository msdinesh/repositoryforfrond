<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Page language="c#" Codebehind="PlacementLinkPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.PlacementLinkPage" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>PlacementLinkPage</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal><LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet"><asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent"><uc1:title id="titleBar" runat="server" imagesource="../Images/bnbservice24silver.gif"></uc1:title><uc1:navigationbar id="NavigationBar1" runat="server"></uc1:navigationbar><bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" UndoText="Undo" SaveText="Save" EditAllText="Edit All"
					NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" NewButtonEnabled="False" CssClass="databuttons" DisableAllButtons="True"></bnbpagecontrol:bnbdatabuttons><bnbpagecontrol:bnbmessagebox id="BnbMessageBox2" runat="server" CssClass="messagebox" SuppressRecordSavedMessage="false"
					PopupMessage="false"></bnbpagecontrol:bnbmessagebox><asp:label id="lblViewWarnings" runat="server" CssClass="viewwarning" Visible="False"></asp:label>
				<P>
					<TABLE class="fieldtable" id="Table1" width="100%" border="0">
						<TR>
							<TD class="label" style="HEIGHT: 10px" width="50%">Volunteer</TD>
							<TD style="HEIGHT: 10px" width="50%"><bnbdatacontrol:bnbdomainobjecthyperlink id="BnbDomainObjectHyperlink2" runat="server" HyperlinkText="[BnbDomainObjectHyperlink]"
									Target="_top" DataMember="BnbService.Application.InstanceDescription" RedirectPage="ApplicationPage.aspx" GuidProperty="BnbService.Application.ID"></bnbdatacontrol:bnbdomainobjecthyperlink></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 10px" width="50%">Country</TD>
							<TD style="HEIGHT: 10px" width="50%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" Height="22px" DataMember="BnbService.Position.Role.Employer.CountryID"
									DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel" LookupTableName="lkuCountry"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>
						<TR>
							<TD class="label" width="50%">Employer</TD>
							<TD width="50%"><bnbdatacontrol:bnbtextbox id="BnbTextBox3" runat="server" CssClass="textbox" Height="20px" Width="250px" DataMember="BnbService.Position.Role.Employer.EmployerName"
									ControlType="Label" MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" width="50%">Job Title</TD>
							<TD width="50%"><bnbdatacontrol:bnbtextbox id="BnbTextBox4" runat="server" CssClass="textbox" Height="20px" Width="250px" DataMember="BnbService.Position.Role.RoleName"
									ControlType="Label" MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" width="50%">Placement Ref</TD>
							<TD width="50%">
								<P><bnbdatacontrol:bnbdomainobjecthyperlink id="BnbDomainObjectHyperlink1" runat="server" HyperlinkText="[BnbDomainObjectHyperlink]"
										Target="_top" DataMember="BnbService.Position.FullPlacementReference" RedirectPage="PlacementPage.aspx" GuidProperty="BnbService.Position.ID"></bnbdatacontrol:bnbdomainobjecthyperlink></P>
							</TD>
						</TR>
						<TR>
							<TD class="label" width="50%">Start Date</TD>
							<TD width="50%"><bnbdatacontrol:bnbdatebox id="bnbdbxStartDate" runat="server" CssClass="datebox" Height="20px" Width="90px"
									DataMember="BnbService.PlacementStartDate" DateCulture="en-GB" DateFormat="dd/mm/yyyy" OnPreRender="bnbdbxStartDate_PreRender"></bnbdatacontrol:bnbdatebox></TD>
						</TR>
						<TR>
							<TD class="label" width="50%">End Date</TD>
							<TD width="50%"><bnbdatacontrol:bnbdatebox id="BnbDateBox2" runat="server" CssClass="datebox" Height="20px" Width="90px" DataMember="BnbService.PlacementEndDate"
									DateCulture="en-GB" DateFormat="dd/mm/yyyy"></bnbdatacontrol:bnbdatebox></TD>
						</TR>
						<TR>
							<TD class="label" width="50%">Duration</TD>
							<TD width="50%"><bnbdatacontrol:BnbTextBox id="bnbtxtDuration" runat="server" CssClass="textbox" Height="20px" Width="250px"
									StringLength="0" Rows="1" MultiLine="false" ControlType="Label" DataMember="BnbService.Position.Duration" Enabled="False"></bnbdatacontrol:BnbTextBox></TD>
						</TR>
						<TR>
							<TD class="label" width="50%">Other&nbsp;Links to Placement</TD>
							<TD width="50%">
								<asp:Label id="uxOtherLinksLabel" runat="server"></asp:Label></TD>
						</TR>
					</TABLE>
				</P>
				<bnbpagecontrol:bnbsectionheading id="BnbSectionHeading2" runat="server" CssClass="sectionheading" SectionHeading="Placement Link Status"
					Width="100%" Height="20px" Collapsed="false"></bnbpagecontrol:bnbsectionheading>
				<P><bnbdatagrid:BnbDataGridForDomainObjectList id="BnbDataGridForDomainObjectList1" runat="server" CssClass="datagrid" Width="100%"
						DataMember="BnbService.ServiceProgress" AddButtonVisible="true" EditButtonText="Edit" AddButtonText="Add" EditButtonVisible="true"
						DeleteButtonVisible="false" DataGridType="Editable" DataProperties="ServiceStatusID [BnbLookupControl:lkuServiceStatus], StatusDate [BnbDateBox], ServiceReasonID [BnbLookupControl:lkuServiceReason],Rank[BnbDecimalBox],CreatedBy[BnbLookupControl:vlkuBonoboUser]"
						SortBy="StatusDate" FilterPairParentDataProperty="ServiceStatusID" FilterPairLookupColumnRelatedToParent="ServiceStatusID"
						FilterPairChildDataProperty="ServiceReasonID"></bnbdatagrid:BnbDataGridForDomainObjectList></P>
			</div>
		</form>
	</body>
</HTML>
