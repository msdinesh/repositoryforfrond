<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Page language="c#" Codebehind="FundingPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.FundingPage" %>
<%@ Register TagPrefix="uc1" TagName="NavigatePanel" Src="../UserControls/NavigatePanel.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
  <HEAD>
		<title>FundingPage</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/bnbfunding24silver.gif"></uc1:title>
				<table width="100%">
					<tr>
						<td><bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" NewWidth="55px" EditAllWidth="55px"
								SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px" NewText="New" EditAllText="Edit All" SaveText="Save"
								UndoText="Undo" NewButtonEnabled="False"></bnbpagecontrol:bnbdatabuttons></td>
						<td align="right">
							<uc1:NavigatePanel id="navPanel" runat="server"></uc1:NavigatePanel></td>
					</tr>
				</table>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:bnbmessagebox>
				<TABLE class="fieldtable" id="Table1" width="100%" border="0">
					<TR>
						<TD class="label" width="25%">Volunteer Name</TD>
						<TD width="25%">
							<P><asp:hyperlink id="hlkVolunteer" runat="server" Target="_top"></asp:hyperlink></P>
						</TD>
						<TD class="label" style="HEIGHT: 28px" width="25%">Placement Ref</TD>
						<TD style="HEIGHT: 28px" width="25%"><asp:hyperlink id="hlkPlacement" runat="server"></asp:hyperlink></TD>
					</TR>
					<TR>
						<TD class="label" width="25%">Logged By</TD>
						<TD width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" LookupTableName="vlkuBonoboUser"
								DataMember="BnbFunding.CreatedBy" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="176px"
								Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
						<TD class="label" width="25%">Logged On</TD>
						<TD width="25%"><bnbdatacontrol:bnbdatebox id="BnbDateBox1" runat="server" CssClass="datebox" DataMember="BnbFunding.CreatedOn"
								Width="90px" Height="20px" DateFormat="dd/mm/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
					<TR>
						<TD class="label" width="25%">Donor Owner</TD>
						<TD width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl2" runat="server" CssClass="lookupcontrol" LookupTableName="vlkuBonoboUser"
								DataMember="BnbFunding.ForUserID" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="176px"
								Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
						<TD class="label" width="25%"></TD>
						<TD width="25%"></TD>
					</TR>
					<TR>
						<TD class="label" style="HEIGHT: 24px" width="25%">Funding Secured By</TD>
						<TD style="HEIGHT: 24px" width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl3" runat="server" CssClass="lookupcontrol" LookupTableName="lkuFunderDepartmentType"
								DataMember="BnbFunding.FunderDepartmentID" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="176px" Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
						<TD class="label" style="HEIGHT: 24px" width="25%">Financial Year</TD>
						<TD style="HEIGHT: 24px" width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl4" runat="server" CssClass="lookupcontrol" LookupTableName="lkuFinancialYear"
								DataMember="BnbFunding.FinancialYearID" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="98px" Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" width="25%">Donor Organisation</TD>
						<TD width="25%">
							<P><bnbdatacontrol:bnbsearchdomainobjectcomposite id="BnbSearchDomainObjectComposite1" runat="server" DataMember="BnbFunding.Company"
									Width="228px" BonoboViewFieldToBeBound="tblCompany_CompanyID" BonoboViewFieldToLookUp="tblCompany_CompanyName" BonoboViewName="vwBonoboFundingCompanyLinker"
									DomainObjectType="BnbCompany" ChangeButtonVisible="False"></bnbdatacontrol:bnbsearchdomainobjectcomposite></P>
						</TD>
						<TD class="label" width="25%"></TD>
						<TD width="25%"></TD>
					</TR>
					<TR>
						<TD class="label" width="25%">Donor Individual</TD>
						<TD width="25%">
							<bnbdatacontrol:BnbSearchDomainObjectComposite id="BnbSearchDomainObjectComposite2" runat="server" DataMember="BnbFunding.DonorIndividual"
								DomainObjectType="BnbIndividual" BonoboViewName="vwBonoboFundingIndividualLinker" BonoboViewFieldToLookUp="Custom_DisplayName"
								BonoboViewFieldToBeBound="tblIndividualKeyInfo_IndividualID" ChangeButtonVisible="False"></bnbdatacontrol:BnbSearchDomainObjectComposite></TD>
						<TD class="label" width="25%"></TD>
						<TD width="25%"></TD>
					</TR>
					<TR>
						<TD class="label" width="25%">Funding Status</TD>
						<TD width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl5" runat="server" CssClass="lookupcontrol" LookupTableName="lkuFundingStatus"
								DataMember="BnbFunding.FundingStatusID" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="172px"
								Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
						<TD class="label" width="25%">Received On</TD>
						<TD width="25%"><bnbdatacontrol:bnbdatebox id="BnbDateBox2" runat="server" CssClass="datebox" DataMember="BnbFunding.ReceivedDate"
								Width="90px" Height="20px" DateFormat="dd/mm/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
					<TR>
						<TD class="label" width="25%">Value �</TD>
						<TD width="25%"><bnbdatacontrol:bnbtextbox id="BnbTextBox2" runat="server" CssClass="textbox" DataMember="BnbFunding.FundingValue"
								Width="108px" Height="20px" FormatExpression="�#,#0.00" MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:bnbtextbox></TD>
						<TD class="label" width="25%">Currency</TD>
						<TD width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl6" runat="server" CssClass="lookupcontrol" LookupTableName="lkuCurrency"
								DataMember="BnbFunding.CurrencyID" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="146px"
								Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD width="25%"></TD>
						<TD width="25%"></TD>
						<TD class="label" width="25%">Currency Value</TD>
						<TD width="25%"><bnbdatacontrol:bnbtextbox id="BnbTextBox3" runat="server" CssClass="textbox" DataMember="BnbFunding.ConvertedValue"
								Width="141px" Height="20px" MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:bnbtextbox></TD>
					</TR>
					<tr>
						<td></td>
						<td></td>
					</tr>
					<TR>
						<TD class="label" width="25%">Cost Cover:<BR>
							Accommodation</TD>
						<TD width="25%">
							<P><bnbdatacontrol:bnbcheckbox id="BnbCheckBox1" runat="server" CssClass="checkbox" DataMember="BnbFunding.CostCover_Accommodation"></bnbdatacontrol:bnbcheckbox></P>
						</TD>
						<TD class="label" style="HEIGHT: 28px" width="25%">Cost Cover:
							<BR>
							Allowance</TD>
						<TD style="HEIGHT: 28px" width="25%"><bnbdatacontrol:bnbcheckbox id="BnbCheckBox3" runat="server" CssClass="checkbox" DataMember="BnbFunding.CostCover_Allowance"></bnbdatacontrol:bnbcheckbox></TD>
					</TR>
					<TR>
						<TD class="label" width="25%">Cost Cover:<BR>
							Other Local</TD>
						<TD width="25%"><bnbdatacontrol:bnbcheckbox id="BnbCheckBox2" runat="server" CssClass="checkbox" DataMember="BnbFunding.CostCover_OtherLocal"></bnbdatacontrol:bnbcheckbox></TD>
						<TD class="label" style="HEIGHT: 28px" width="25%">Cost Cover:<BR>
							Other Rec Base</TD>
						<TD style="HEIGHT: 28px" width="25%"><bnbdatacontrol:bnbcheckbox id="BnbCheckBox4" runat="server" CssClass="checkbox" DataMember="BnbFunding.CostCover_RecruitmentBase"></bnbdatacontrol:bnbcheckbox></TD>
					</TR>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
