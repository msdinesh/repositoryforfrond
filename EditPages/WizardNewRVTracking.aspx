<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Page language="c#" Codebehind="WizardNewRVTracking.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.WizardNewRVTracking" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc2" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>WizardNewRVTracking</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<DIV class="wizardheader" ms_positioning="FlowLayout">
					<H3><IMG class="icon24" src="../Images/wizard24silver.gif"> New RV Tracking Wizard</H3>
				</DIV>
				<asp:panel id="panelZero" runat="server" CssClass="wizardpanel">
					<P><EM>This wizard will help you to add&nbsp;new&nbsp;RV Tracking record.</EM></P>
					<P></P>
					<P>
						<TABLE class="wizardtable" id="Table2">
							<TR>
								<TD class="label" style="WIDTH: 25%">RV Record Status</TD>
								<TD>
									<asp:DropDownList id="dropDownRVRecordType" runat="server" Width="136px"></asp:DropDownList></TD>
							</TR>
						</TABLE>
					<P>
						<asp:label id="lblPanelZeroFeedback" runat="server" CssClass="feedback"></asp:label></P>
					<P></P>
				</asp:panel><asp:panel id="panelOne" runat="server" CssClass="wizardpanel">
					<P>Please select the RV:</P>
					<P>Enter the Surname, Forename, Ref No and/or Country here and press Find, then 
						select one of the results:</P>
					<TABLE class="wizardtable">
						<TR>
							<TD class="label" style="WIDTH: 25%; TEXT-ALIGN: right">Ref No</TD>
							<TD>
								<asp:TextBox id="txtFindRefNo" runat="server" Width="72px"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 25%; TEXT-ALIGN: right">Surname (starts with)</TD>
							<TD>
								<asp:TextBox id="txtFindSurname" runat="server"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 25%; TEXT-ALIGN: right">Forename (starts with)</TD>
							<TD>
								<asp:TextBox id="txtFindForename" runat="server"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 25%; TEXT-ALIGN: right">EOS Date Between
							</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneDateBox id="BnbDateBox1" runat="server" CssClass="datebox" Height="20px" Width="90px" DateFormat="dd/MMM/yyyy"
									DateCulture="en-GB"></bnbdatacontrol:BnbStandaloneDateBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<bnbdatacontrol:BnbStandaloneDateBox id="BnbDateBox2" runat="server" CssClass="datebox" Height="20px" Width="90px" DateFormat="dd/MMM/yyyy"
									DateCulture="en-GB"></bnbdatacontrol:BnbStandaloneDateBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 25%; TEXT-ALIGN: right">Country</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" Height="22px" Width="144px"
									LookupTableName="lkuCountry" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" ShowBlankRow="True"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 25%; TEXT-ALIGN: right"></TD>
							<TD>
								<asp:Button id="cmdRVFind" runat="server" Text="Find" onclick="cmdIndivFind_Click"></asp:Button></TD>
						</TR>
					</TABLE>
					<P>
						<bnbdatagrid:BnbDataGridForView id="bdgRV" runat="server" CssClass="datagrid" Width="100%" GuidKey="tblApplication_ApplicationID"
							ViewName="vwBonoboFindApplication" DataGridType="RadioButtonList" DisplayNoResultMessageOnly="True" DisplayResultCount="false"
							ShowResultsOnNewButtonClick="false" ViewLinksVisible="False" NewLinkVisible="False" NewLinkText="New" ViewLinksText="View"
							Visible="False"></bnbdatagrid:BnbDataGridForView></P>
					<P></P>
					<P>
						<asp:Label id="lblSearchHint" runat="server" Visible="False">(If you cannot find the Individual you are looking for, they may not be in the database yet. If this is the case, you can add it as a new RV selecting newRV in the dropdown list using the <a href="WizardNewRV.aspx">
							</a>)</asp:Label></P>
					<P>
						<asp:Label id="lblPanelOneFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				</asp:panel><asp:panel id="panelTwo" runat="server" CssClass="wizardpanel">
					<P>Please enter further details about the new RV, then press Finish to create the 
						new record:</P>
					<TABLE class="wizardtable" id="Table4">
						<TR>
							<TD class="label" style="WIDTH: 30.1%; HEIGHT: 24px">Vol Ref</TD>
							<TD style="HEIGHT: 24px">
								<asp:Label id="lblVolRef" runat="server" Width="128px"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 30.1%">
								<P>Fore Name</P>
							</TD>
							<TD style="HEIGHT: 25px">
								<asp:TextBox id="txtForeName" runat="server"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 30.1%">Sur Name</TD>
							<TD>
								<asp:TextBox id="txtSurName" runat="server"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 30.1%">RV Record Status</TD>
							<TD>
								<asp:Label id="lblRVStatus" runat="server" Width="136px"></asp:Label></TD>
						</TR>
					</TABLE>
					<P>
						<asp:Label id="lblPanelTwoFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				</asp:panel>
				<P></P>
				<P><uc2:wizardbuttons id="wizardButtons" runat="server"></uc2:wizardbuttons></P>
				<P><asp:label id="lblHiddenApplicationID" runat="server" Visible="False"></asp:label><asp:label id="lblHiddenRVRecordTypeID" runat="server" Width="136px" Visible="False"></asp:label></P>
				<P></P>
				<P><asp:label id="lblHiddenRVRecordType" runat="server" Width="176px" Visible="False"></asp:label><asp:label id="lblHiddenRVTrackingID" runat="server" Width="136px"></asp:label></P>
			</div>
		</form>
	</body>
</HTML>
