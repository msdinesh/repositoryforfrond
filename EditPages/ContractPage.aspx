<%@ Page language="c#" Codebehind="ContractPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.ContractPage" %>
<%@ Register Src="../UserControls/PMFStatusPanel.ascx" TagName="PMFStatusPanel" TagPrefix="uc2" %>
<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.ServicesControls"
    TagPrefix="cc1" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="SFEventAttendeesGrid" Src="../UserControls/SFEventAttendeesGrid.ascx" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="SFEventPersonnelGrid" Src="../UserControls/SFEventPersonnelGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbdmssearchcontrol" Namespace="BonoboWebControls.DMSControls" Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>EventPage</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<link rel="stylesheet" href="../Css/calendar.css" type="text/css" />
		<script language="javascript" type="text/javascript">
        function emailPageLink()
        {
        var str=encodeURIComponent(window.location);
        var replaceTabName = document.getElementById('BnbTabControl1_SelectedTabID').value;
        var url = decodeURIComponent(str);
        if(url.split("&ControlID=").length== 2)
        {
        var tabName = url.split("&ControlID=")[1];
        url = url.replace(tabName,replaceTabName);
        
        window.open('mailto:?subject=PGMS-Page Link&body=%0d%0a%0d%0a%0d%0a%0d%0a'+encodeURIComponent(url)+''); window.focus(); return false;
        }
        else
        {
        url = url + '&ControlID=' + replaceTabName;
        
        window.open('mailto:?subject=PGMS-Page Link&body=%0d%0a%0d%0a%0d%0a%0d%0a'+encodeURIComponent(url)+''); window.focus(); return false;
        } 
        }
    
    </script>
	</HEAD>
	<body onclick="hideCalendar(event);">
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/bnbgrantcontract24silver.gif"></uc1:title>
				<uc2:PMFStatusPanel ID="PMFStatusPanel2" runat="server" />
				<table width="100%">
					<tr>
						<td>
				            <bnbpagecontrol:bnbdatabuttons id="BnbDataButtons3" runat="server" CssClass="databuttons" UndoText="Undo" SaveText="Save"
					        EditAllText="Edit All" NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px"
					        NewWidth="55px" NewButtonEnabled="True"></bnbpagecontrol:bnbdatabuttons>
					        </td> 
					        <td align="right" style="width: 330px; padding: 5px;">
                                <a href="#" onclick="emailPageLink();return false;"><b>Email Page Link</b></a>
                            </td>
                            <td>
                                <asp:Button ID="btnUpload" runat="server" Text="UPLOAD NEW DOCUMENT" OnClick="btnUpload_Click" /></td>
					        <td align="right">
					        <bnbgenericcontrols:BnbHyperlink ID="uxProposalPageHyper" runat="server" Text="View Proposal"></bnbgenericcontrols:BnbHyperlink>
					    </td> 
			        </tr>
			    </table>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox" PopupMessage="False"></bnbpagecontrol:bnbmessagebox><asp:label id="lblViewWarnings" runat="server" CssClass="viewwarning" Visible="False"></asp:label><asp:label id="lblProjectStatusMessage" runat="server" CssClass="viewwarning"></asp:label>
                <bnbpagecontrol:BnbTabControl ID="BnbTabControl1" runat="server" />
                <asp:panel id="Main" runat="server" CssClass="tcTab" BnbTabName="Main">
			<table id="Table1" border="0" class="fieldtable">
				<tr>
					<td class="label" style="width: 168px">Project&nbsp;Title</td>
					<td><bnbdatacontrol:bnbdomainobjecthyperlink id="BnbDomainObjectHyperlink1" runat="server" DataMember="BnbProject.ProjectTitle"
							RedirectPage="ProjectPage.aspx" Target="_top" HyperlinkText="[BnbDomainObjectHyperlink]" GuidProperty="ProjectID"></bnbdatacontrol:bnbdomainobjecthyperlink></td>
					<td class="label">Abbrev</td>
					<td><bnbdatacontrol:bnbdomainobjecthyperlink id="BnbDomainObjectHyperlink2" runat="server" DataMember="BnbProject.ProjectAbbr"
							RedirectPage="ProjectPage.aspx" Target="_top" HyperlinkText="[BnbDomainObjectHyperlink]" GuidProperty="ProjectID"></bnbdatacontrol:bnbdomainobjecthyperlink></td>
				</tr>
				<TR>
					<TD class="label" rowSpan="2" style="width: 168px">Org&nbsp;/&nbsp;Budget Line</TD>
					<TD style="VERTICAL-ALIGN: top" rowSpan="2"><bnbdatacontrol:bnbdomainobjecthyperlink id="BnbDomainObjectHyperlink3" runat="server" DataMember="BnbGrant.Company.CompanyName"
							RedirectPage="OrganisationPage.aspx" Target="_top" HyperlinkText="[BnbDomainObjectHyperlink]" GuidProperty="CompanyID"></bnbdatacontrol:bnbdomainobjecthyperlink><BR>
						<bnbdatacontrol:bnbtextbox id="BnbTextBox4" runat="server" CssClass="textbox" Height="20px" Width="250px" DataMember="BnbGrant.BudgetLine"
							MultiLine="false" Rows="1" StringLength="0" ControlType="Text" DataType="String"></bnbdatacontrol:bnbtextbox></TD>
					<td class="label">Project Code</td>
					<td>
						<P><bnbdatacontrol:bnbtextbox id="BnbTextBox8" runat="server" CssClass="textbox" Height="20px" Width="249px" DataMember="BnbProject.ProjectCode"
								MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></P>
					</td>
				</TR>
				<TR>
					<!--<TD class="label" width="25%"></TD>
					<TD width="25%"></TD>-->
					<td class="label">Grant&nbsp;Code</td>
					<td><bnbdatacontrol:bnbtextbox id="BnbTextBox6" runat="server" CssClass="textbox" Height="20px" Width="250px" DataMember="BnbGrant.OverrideFundCode"
							MultiLine="false" Rows="1" StringLength="0" ControlType="Text" DataType="String"></bnbdatacontrol:bnbtextbox></td>
				</TR>
				<TR>
					<TD class="label" style="width: 168px">Contract&nbsp;Signed&nbsp;Date</TD>
					<TD><bnbdatacontrol:bnbdatebox id="BnbDateBox1" ShowCalender="true" runat="server" CssClass="datebox" Height="20px" Width="90px" DataMember="BnbGrant.ContractSignedDate"
							DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:bnbdatebox></TD>
					<TD class="label">Contract&nbsp;Start&nbsp;/&nbsp;End</TD>
					<TD><asp:label id="lblContractStartDate" runat="server"></asp:label>&nbsp;/
						<asp:label id="lblContractEndDate" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD class="label" style="width: 168px">Donor Reference</TD>
					<td><bnbdatacontrol:bnbtextbox id="BnbTextBox5" runat="server" CssClass="textbox" Height="20px" Width="250px" DataMember="BnbGrant.DonorReference"
							MultiLine="false" Rows="1" StringLength="0" ControlType="Text" DataType="String"></bnbdatacontrol:bnbtextbox></td>
					<td class="label">Donor Currency</td>
					<td><asp:label id="lblDonorCurrency" runat="server"></asp:label></td>
				</TR>
				<TR>
					<TD class="label" style="HEIGHT: 25px; width: 168px;">
                        Grant&nbsp;Amount&nbsp;(Sterling)</TD>
					<TD><asp:label id="lblContractAmountSterling" runat="server"></asp:label></TD>
					<TD class="label">
                        Grant Amount&nbsp;(Currency)</TD>
					<TD><asp:label id="lblContractAmountCurrency" runat="server"></asp:label></TD>
				</TR>
				<TR>
				<TD class="label">Cost Share</TD>
						<TD style="width: 277px">
						    <bnbdatacontrol:BnbDecimalBox ID="BnbDecimalBox1" runat="server" CssClass="textbox"  TextAlign="left" Height="20px" Width="250px"
						        DataMember="BnbGrant.CostShare" DecimalPlaces="0" />
						</TD>
				</TR>
				<TR>
					<TD class="label" style="width: 168px">No specific<BR>
						Programme Office</TD>
					<TD style="VERTICAL-ALIGN: top"><asp:label id="lblGlobal" runat="server" OnPreRender="lblGlobal_PreRender"></asp:label></TD>
					<TD class="label" rowSpan="3">Programme Area</TD>
					<TD style="VERTICAL-ALIGN: top" rowSpan="3"><bnbdatacontrol:bnblookupcontrol id="cboProjectProgrammeArea" runat="server" CssClass="lookupcontrol" Height="22px"
							Width="250px" DataMember="BnbProject.ProgrammeAreaID" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel" LookupTableName="vlkuBonoboVSOProgrammeProgrammeOffice"></bnbdatacontrol:bnblookupcontrol></TD>
				</TR>
				<TR>
					<TD class="label" style="width: 168px">Regions</TD>
					<TD style="VERTICAL-ALIGN: top"><asp:label id="lblRegions3" runat="server" OnPreRender="Regions_PreRender"></asp:label></TD>
				</TR>
				<TR>
					<TD class="label" style="height: 28px; width: 168px;">Programme Offices</TD>
					<TD style="VERTICAL-ALIGN: top; height: 28px;"><bnbdatagrid:bnbdatagridfordomainobjectlist id="bnbdgProgrammeOffices" runat="server" CssClass="datagrid_nolines" Width="200px"
							DataMember="BnbProject.ProgrammeOffices" DataProperties="ProgrammeOfficeID [BnbLookupControl:lkuProgrammeOffice]" DeleteButtonVisible="false" EditButtonVisible="true"
							AddButtonText="Add" EditButtonText="Edit" AddButtonVisible="true" VisibleOnNewParentRecord="false" SortBy="ID" DataGridForDomainObjectListType="Editable"
							HideColumnHeaders="True"></bnbdatagrid:bnbdatagridfordomainobjectlist></TD>
				</TR>
				<TR>
					<TD class="label" style="width: 168px">Claim&nbsp;Submitted&nbsp;By</TD>
					<TD style="VERTICAL-ALIGN: top"><SPAN class="textbox"><bnbdatacontrol:bnblookupcontrol id="BnbClaimSubmittedBy" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
								DataMember="BnbGrant.ClaimSubmittedByID" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="appDepartment"></bnbdatacontrol:bnblookupcontrol></SPAN></TD>
					<TD class="label">Income&nbsp;Received&nbsp;By</TD>
					<TD style="VERTICAL-ALIGN: top"><SPAN class="textbox"><bnbdatacontrol:bnblookupcontrol id="BnbIncomeReceivedBy" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
								DataMember="BnbGrant.IncomeReceivedByID" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="appDepartment"></bnbdatacontrol:bnblookupcontrol></SPAN></TD>
				</TR>
				<TR>
				<TD class="label" style="width: 168px">Contract Signed</TD>
				    <TD>
				    <bnbdatacontrol:BnbLookupControl ID="lkupContract" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
				    DataMember="BnbGrant.ContractSignedID" DataTextField="Description" ListBoxRows="1" LookupControlType="ComboBox" LookupTableName="lkuYesNoForContract" />
                   
					</TD>
					<TD class="label">Project Total Cost (Donor Currency)</TD>
					<TD>
					    <bnbdatacontrol:BnbDecimalBox ID="BnbDecimalBoxPTC" runat="server" CssClass="textbox"  TextAlign="left" Height="20px" Width="250px"
					        DataMember="BnbGrant.ProjectTotalCost" DecimalPlaces="0" />
					</TD>
				</TR>
				<TR>
				    <TD class="label" style="width: 168px">Matched Funding Requirement (�)</TD>
					<TD><bnbdatacontrol:bnbdecimalbox id="BnbDecimalBox5" runat="server" CssClass="textbox" Width="80px" Height="20px"
							DataMember="BnbGrant.MatchedFundingRequired" DecimalPlaces="0"></bnbdatacontrol:bnbdecimalbox>
					</TD>
					<TD class="label">Project Total Cost (�)</TD>
					<TD>
						<bnbdatacontrol:BnbDecimalBox ID="bnbDecimalBoxPTCsterling" runat="server" CssClass="textbox"  TextAlign="right" Height="20px" Width="120px"
						 DataMember="BnbGrant.ProjectTotalCostSterling" DecimalPlaces="0" />
					</TD>
				</TR>
				<TR>
					<TD class="label" style="width: 168px">Conditions</TD>
					<TD style="VERTICAL-ALIGN: top" colSpan="3"><bnbdatacontrol:bnbtextbox id="BnbTextBox1" runat="server" CssClass="textbox" Height="20px" Width="100%" DataMember="BnbGrant.ContractConditions"
							MultiLine="True" Rows="5" StringLength="1000" ControlType="Text" DataType="String"></bnbdatacontrol:bnbtextbox></TD>
				</TR>
				<TR>
					<TD class="label" style="width: 168px">Comments</TD>
					<TD colSpan="3"><SPAN class="textbox">
							<bnbdatacontrol:bnbtextbox id="BnbTextBox2" runat="server" CssClass="textbox" Height="20px" Width="100%" DataMember="BnbGrant.ContractComments"
									MultiLine="True" Rows="5" StringLength="1000" ControlType="Text" DataType="String"></bnbdatacontrol:bnbtextbox>
						</SPAN>
					</TD>
				</TR>
				<asp:panel id="pnlDeleted" runat="server">
					<TR>
						<TD class="label">Deleted</TD>
						<TD>
							<bnbdatacontrol:bnbcheckbox id="BnbCheckBox6" runat="server" CssClass="checkbox" DataMember="BnbGrant.Exclude"></bnbdatacontrol:bnbcheckbox></TD>
					</TR>
					<TR>
						<TD class="label">Last Updated By</TD>
						<TD colSpan="1">
							<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol16" runat="server" CssClass="textbox" Width="250px" Height="22px"
								DataMember="BnbGrant.UpdatedBy" LookupTableName="vlkuBonoboUser" LookupControlType="ReadOnlyLabel" DataTextField="Description"></bnbdatacontrol:bnblookupcontrol></TD>
						<TD class="label">Last Updated On</TD>
						<TD colSpan="2">
							<bnbdatacontrol:bnbdatebox id="Bnbdatebox7" runat="server" CssClass="datebox" Width="90px" Height="20px" DataMember="BnbGrant.UpdatedOn"
								DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
					
				</asp:panel></table>
				<p>
                    <asp:HyperLink ID="uxGeneratePMPGContractApproval" runat="server">Generate PMPG Contract Approval Form</asp:HyperLink>&nbsp;</p>
                    <p>
                        <asp:HyperLink ID="uxGeneratePMPGGrantClosure" runat="server">Generate PMPG Grant Closure Form</asp:HyperLink>&nbsp;</p>
				</asp:panel>
<!--			<P><bnbpagecontrol:bnbsectionheading id="BnbSectionHeading5" runat="server" CssClass="sectionheading" Collapsed="false"
					Height="20px" Width="100%" SectionHeading="Authorisation"></bnbpagecontrol:bnbsectionheading></P>
			<p>
				<table id="AuthTable" border="0" class="fieldtable">
					<tr>
						<td class="label">&nbsp;</td>
						<td class="label">Name</td>
						<td class="label">Approved By</td>
						<td class="label">Approved</td>
						<td class="label">Date</td>
					</tr>
					<tr>
						<td class="label">Project Accountable</td>
						<td><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" Height="22px" Width="168px"
								DataMember="BnbProject.ProjectAccountableOfficerID" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
								LookupTableName="vlkuBonoboUser"></bnbdatacontrol:bnblookupcontrol></td>
						<td><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl4" runat="server" CssClass="lookupcontrol" Height="22px" Width="168px"
								DataMember="BnbGrant.ApprovedProjectAccountableUserID" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
								LookupTableName="vlkuBonoboUser"></bnbdatacontrol:bnblookupcontrol></td>
						<td><bnbdatacontrol:bnbcheckbox id="BnbCheckBox1" runat="server" CssClass="checkbox" DataMember="BnbGrant.ApprovedProjectAccountable"></bnbdatacontrol:bnbcheckbox></td>
						<td><bnbdatacontrol:bnbdatebox id="BnbDateBox2" runat="server" CssClass="datebox" Height="20px" Width="90px" DataMember="BnbGrant.ApprovedProjectAccountableDate"
								DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:bnbdatebox></td>
					</tr>
					<tr>
						<td class="label">Project Responsible</td>
						<td style="HEIGHT: 33px"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl2" runat="server" CssClass="lookupcontrol" Height="22px" Width="168px"
								DataMember="BnbProject.ProjectResponsibleOfficerID" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
								LookupTableName="vlkuBonoboUser"></bnbdatacontrol:bnblookupcontrol></td>
						<td style="HEIGHT: 33px"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl5" runat="server" CssClass="lookupcontrol" Height="22px" Width="168px"
								DataMember="BnbGrant.ApprovedProjectResponsibleUserID" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
								LookupTableName="vlkuBonoboUser"></bnbdatacontrol:bnblookupcontrol></td>
						<td style="HEIGHT: 33px"><bnbdatacontrol:bnbcheckbox id="BnbCheckBox2" runat="server" CssClass="checkbox" DataMember="BnbGrant.ApprovedProjectResponsible"></bnbdatacontrol:bnbcheckbox></td>
						<td style="HEIGHT: 33px"><bnbdatacontrol:bnbdatebox id="BnbDateBox3" runat="server" CssClass="datebox" Height="20px" Width="90px" DataMember="BnbGrant.ApprovedProjectResponsibleDate"
								DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:bnbdatebox></td>
					</tr>
					<tr>
						<td class="label">Grant Accountable</td>
						<td><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl3" runat="server" CssClass="lookupcontrol" Height="22px" Width="168px"
								DataMember="BnbGrant.GrantAccountableOfficerID" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
								LookupTableName="vlkuBonoboUser"></bnbdatacontrol:bnblookupcontrol></td>
						<td><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl6" runat="server" CssClass="lookupcontrol" Height="22px" Width="168px"
								DataMember="BnbGrant.ApprovedGrantAccountableUserID" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
								LookupTableName="vlkuBonoboUser"></bnbdatacontrol:bnblookupcontrol></td>
						<td><bnbdatacontrol:bnbcheckbox id="BnbCheckBox3" runat="server" CssClass="checkbox" DataMember="BnbGrant.ApprovedGrantAccountable"></bnbdatacontrol:bnbcheckbox></td>
						<td><bnbdatacontrol:bnbdatebox id="BnbDateBox4" runat="server" CssClass="datebox" Height="20px" Width="90px" DataMember="BnbGrant.ApprovedGrantAccountableDate"
								DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:bnbdatebox></td>
					</tr>
					<tr>
						<td class="label">RPDFA
							<asp:label id="lblRegions" runat="server" CssClass="label"></asp:label></td>
						<td></td>
						<td><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl7" runat="server" CssClass="lookupcontrol" Height="22px" Width="168px"
								DataMember="BnbGrant.ApprovedDevFundAdvisorUserID" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
								LookupTableName="vlkuBonoboUser"></bnbdatacontrol:bnblookupcontrol></td>
						<td><bnbdatacontrol:bnbcheckbox id="BnbCheckBox4" runat="server" CssClass="checkbox" DataMember="BnbGrant.ApprovedDevFundAdvisor"></bnbdatacontrol:bnbcheckbox></td>
						<td><bnbdatacontrol:bnbdatebox id="BnbDateBox6" runat="server" CssClass="datebox" Height="20px" Width="90px" DataMember="BnbGrant.ApprovedDevFundAdvisorDate"
								DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:bnbdatebox></td>
					</tr>
					<tr>
						<td class="label">RPM
							<asp:label id="lblRegions2" runat="server" CssClass="label"></asp:label>&nbsp;(High-risk 
							only)</td>
						<td></td>
						<td><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl8" runat="server" CssClass="lookupcontrol" Height="22px" Width="168px"
								DataMember="BnbGrant.ApprovedRegionalProgManagerUserID" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
								LookupTableName="vlkuBonoboUser"></bnbdatacontrol:bnblookupcontrol></td>
						<td><bnbdatacontrol:bnbcheckbox id="BnbCheckBox5" runat="server" CssClass="checkbox" DataMember="BnbGrant.ApprovedRegionalProgManager"></bnbdatacontrol:bnbcheckbox></td>
						<td><bnbdatacontrol:bnbdatebox id="BnbDateBox5" runat="server" CssClass="datebox" Height="20px" Width="90px" DataMember="BnbGrant.ApprovedRegionalProgManagerDate"
								DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:bnbdatebox></td>
					</tr>
				</table>
			</p>-->
            <asp:panel id="Progress" runat="server" CssClass="tcTab" BnbTabName="Progress">
			<P><bnbdatagrid:bnbdatagridfordomainobjectlist id="bnbdgHistory" ShowDateCalender="true" runat="server" CssClass="datagrid" Width="100%" DataMember="BnbGrant.GrantProgresses"
					DataProperties="GrantStatusID [BnbLookupControl:vlkuBonoboContractStatus], StartDate [BnbDateBox], EndDate [BnbDateBox], ContractAmountCurrency [BnbDecimalBox],ExchangeRate [BnbDecimalBox],ContractAmountSterling[BnbDecimalBox],CreatedBy [BnbLookupControl:vlkuBonoboUser], CreatedOn [BnbDateBox]"
					DeleteButtonVisible="False" EditButtonVisible="False" AddButtonText="Add" EditButtonText="Edit" AddButtonVisible="true"
					VisibleOnNewParentRecord="false" SortBy="CreatedOn" DataGridType="Editable" OnPreRender="bnbdgHistory_PreRender"></bnbdatagrid:bnbdatagridfordomainobjectlist></P>
			</asp:panel><asp:panel id="Claims" runat="server" CssClass="tcTab" BnbTabName="Claims">
			<P><bnbdatagrid:bnbdatagridfordomainobjectlist id="bnbdgClaims" ShowDateCalender="true" runat="server" CssClass="datagrid" Width="100%" DataMember="BnbGrant.GrantClaims" SourceHeaderColumns="ClaimAmountInSterling,AmountReceivedInSterling" DestinationHeaderColumns="Claim Amount (�),Amount Received (�)"
					DataProperties="RefNo [BnbTextBox:100px], ClaimTypeID [BnbLookupControl:lkuGrantClaimType:75px], ClaimStatusID [BnbLookupControl:lkuGrantClaimStatus:150px], ClaimDate [BnbDateBox], ClaimReceivedDate [BnbDateBox], ClaimAmountCurrency [BnbDecimalBox], AmountReceived[BnbDecimalBox],ClaimAmountInSterling [BnbDecimalBox],AmountReceivedInSterling[BnbDecimalBox]"
					DeleteButtonVisible="True" EditButtonVisible="true" AddButtonText="Add" EditButtonText="Edit" AddButtonVisible="true" VisibleOnNewParentRecord="false"
					SortBy="ClaimDate" DataGridForDomainObjectListType="Editable" DataGridType="Editable" OnPreRender="bnbdgClaims_PreRender"></bnbdatagrid:bnbdatagridfordomainobjectlist></P>
			</asp:panel><asp:panel id="Reports" runat="server" CssClass="tcTab" BnbTabName="Reports">
			<br />
			<asp:HyperLink ID="hlReports" runat="server">New Report</asp:HyperLink>
			<P><bnbdatagrid:bnbdatagridforview id="bnbdgContractReports" runat="server" CssClass="datagrid" Width="100%" GuidKey="tblGrantReport_GrantReportID"
					NewLinkReturnToParentPageDisabled="True" RedirectPage="ContractReportingDatePage.aspx" QueryStringKey="BnbGrant" FieldName="tblGrant_GrantID"
					GuidDataProperty="GrantReportID" ColumnsToExclude="Custom_ViewReport" DomainObject="BnbGrantReport" ViewName="vwBonoboProjectReports" ViewLinksVisible="true"
					NewLinkVisible="false" NewLinkText="New Proposal" ViewLinksText="View"></bnbdatagrid:bnbdatagridforview></P>
			
			
			</asp:panel>
	
		</div></form>
	</body>
</HTML>
