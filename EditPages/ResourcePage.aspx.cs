using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboDomainObjects;
using BonoboWebControls;
using BonoboEngine;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for ResourcePage.
	/// </summary>
	public partial class ResourcePage : System.Web.UI.Page
	{
		protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading1;
		protected Frond.UserControls.NavigationBar NavigationBar1;
		protected UserControls.Title titleBar;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			
			BnbWebFormManager bnb = null;
#if DEBUG	
			//			bnb = null;
			if (Request.QueryString.ToString() == "")
				BnbWebFormManager.ReloadPage("BnbIndividualResource=E8AA2762-1DCF-44CE-AA98-372F535E2BB1");
//				BnbWebFormManager.ReloadPage("BnbIndividualResource=84A21BB6-FA24-4426-8F04-4426223D26B7");

			else
				bnb = new BnbWebFormManager(this,"BnbIndividualResource");
#else	
			bnb = new BnbWebFormManager(this,"BnbIndividualResource");
#endif
			bnb.LogPageHistory = true;
			BnbWorkareaManager.SetPageStyleOfUser(this);
			// tweak page title settings
			bnb.PageNameOverride = "Resource";
			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
			}		
			BnbIndividualResource objIndividualResource = BnbWebFormManager.StaticGetPageDomainObject("BnbIndividualResource") as BnbIndividualResource;
			BnbIndividual objIndividual = BnbWebFormManager.StaticGetPageDomainObject("BnbIndividual") as BnbIndividual;
			// individual might be blank if we came from the org page
			if (objIndividual == null && objIndividualResource != null)
				objIndividual = objIndividualResource.Individual;

			string indivID = objIndividual.ID.ToString();
			NavigationBar1.RootDomainObject = "BnbIndividual";
			NavigationBar1.RootDomainObjectID = indivID;
			BnblnkIndividual.NavigateUrl = "IndividualPage.aspx?BnbIndividual=" + indivID;
			BnblnkIndividual.Text = objIndividual.FullName;

			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
