using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboDomainObjects;
using BonoboWebControls;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for FundCodePage.
	/// </summary>
	public partial class FundCodePage : System.Web.UI.Page
	{
	
		private BnbWebFormManager bnb = null;
		protected UserControls.Title titleBar;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (!this.IsPostBack)
                titleBar.TitleText = "Fund Code Admin";
					
#if DEBUG
				if(Request.QueryString.ToString() == "")
					bnb = new BnbWebFormManager("BnbFundCode=27EA5E41-72F3-4948-B407-55DEB466EDBC");
				else
					bnb = new BnbWebFormManager(this,"BnbFundCode");
#else
			bnb = new BnbWebFormManager(this,"BnbFundCode");
#endif
			bnb.LogPageHistory = true;
			if(bnb.PageState == PageState.New) HttpContext.Current.Session["ReturnUrl"] = "~/EditPages/FundCodeList.aspx";
						
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
