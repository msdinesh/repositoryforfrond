<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Page language="c#" Codebehind="ProgrammeFundingPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.ProgrammeFundingPage" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Programme Funding Page</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:Title id="titleBar" runat="server" imagesource="../Images/bnbprogrammefunding24silver.gif"></uc1:Title>
				<div style="CLEAR:both">
				<div style="FLOAT:left">
					<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" UndoText="Undo" SaveText="Save" EditAllText="Edit All"
										NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px"
										CssClass="databuttons"></bnbpagecontrol:bnbdatabuttons>
				</div>
				<div style="WIDTH:25em;TEXT-ALIGN:right"><asp:Button Runat="server" ID="btnDelete" Text="Delete" Width="55px" Height="25px" onclick="btnDelete_Click" /></div>
				</div>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:bnbmessagebox>
				<asp:label id="lblViewWarnings" runat="server" CssClass="viewwarning" Visible="False"></asp:label>
						
							<TABLE class="fieldtable" id="Table1" width="100%" style="WIDTH:100%" border="0">
								<TR>
									<TD class="label" style="HEIGHT: 28px" width="50%">
										Volunteer</TD>
									<TD style="HEIGHT: 28px" width="50%">
										<P><asp:hyperlink id="hlkVolunteer" runat="server" Target="_top"></asp:hyperlink></P>
									</TD>
								</TR>
								<TR>
									<TD class="label" width="50%">Placement Ref</TD>
									<TD width="50%">
										<asp:HyperLink id="hlkPlacement" runat="server"></asp:HyperLink></TD>
								</TR>
								<TR>
									<TD class="label" width="50%">Placement Start Date</TD>
									<TD width="50%">
										<bnbdatacontrol:BnbDateBox id="BnbDateBox1" runat="server" CssClass="datebox" Width="90px" DataMember="BnbPosition.StartDate"
											DateFormat="dd/MMM/yyyy" DateCulture="en-GB" Height="20px" Enabled="False"></bnbdatacontrol:BnbDateBox></TD>
								</TR>
								<TR>
									<TD class="label" width="50%">Placement End Date</TD>
									<TD width="50%">
										<bnbdatacontrol:BnbDateBox id="BnbDateBox4" runat="server" CssClass="datebox" Width="90px" DataMember="BnbPosition.EndDate"
											DateFormat="dd/MMM/yyyy" DateCulture="en-GB" Height="20px" Enabled="False"></bnbdatacontrol:BnbDateBox></TD>
								</TR>
								<TR>
									<TD class="label" width="50%">Project Code</TD>
									<TD width="50%">
										<P><bnbdatacontrol:bnbsearchdomainobjectcomposite id="Bnbsearchdomainobjectcomposite2" runat="server" DataMember="BnbProgrammeFunding.FundCode"
												Width="228px" BonoboViewFieldToBeBound="tblFundCode_FundCodeID" BonoboViewFieldToLookUp="Custom_FundCodeCombined" BonoboViewName="vwBonoboFindFundCode"
												DomainObjectType="BnbFundCode" ChangeButtonVisible="True" HelpMessage="<br><br>Enter any part of the Funder name or Fund Code that you require in the box below and press Find<br><br>"></bnbdatacontrol:bnbsearchdomainobjectcomposite></P>
									</TD>
								</TR>
								<TR>
									<TD class="label" style="HEIGHT: 31px" width="50%">Placement Funding Start Date</TD>
									<TD style="HEIGHT: 31px" width="50%"><bnbdatacontrol:bnbdatebox id="dbxStartDate" runat="server" CssClass="datebox" Height="20px" Width="90px" DataMember="BnbProgrammeFunding.FundingStartDate"
											DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:bnbdatebox></TD>
								</TR>
								<TR>
									<TD class="label" style="HEIGHT: 31px" width="50%">
										<P>Placement Funding End Date</P>
									</TD>
									<TD style="HEIGHT: 31px" width="50%">
										<bnbdatacontrol:BnbDateBox id="dbxEndDate" runat="server" CssClass="datebox" Width="90px" Height="20px" DataMember="BnbProgrammeFunding.FundingEndDate"
											DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:BnbDateBox></TD>
								</TR>
								<tr>
								
								    <TD class="label" style="HEIGHT: 31px" width="10%">Grant Code</TD>
								    <TD style="HEIGHT: 31px" width="50%">
								    <asp:Label ID="lblFundCode" runat="server" CssClass="textbox"></asp:Label>
								    
								    <asp:Panel ID="pnlGrantCode" runat="server">								    								    
								        <asp:DropDownList ID="ddlGrantCodes" Visible="false" runat="server"></asp:DropDownList>
								    </asp:Panel>
								    <asp:Label ID="lblErrorMsg" ForeColor="red" runat="server"></asp:Label>
								    <asp:Label ID="lblErrorMessage" runat="server" CssClass="feedback"></asp:Label>
								     <asp:GridView runat="server" HeaderStyle-BackColor="#EBD6FF"  HeaderStyle-Height="25px" 
	                                DataKeyNames="lnkGrantCodeList_GrantCode" ID="grantCodeGrid" Enabled="true" AutoGenerateColumns="false" ShowFooter ="false" EnableViewState="true">
	                                <Columns >	                
	                                <asp:TemplateField  ShowHeader="False">
	                                    <ItemTemplate> 
                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit" CausesValidation="false"></asp:LinkButton> 
                                        </ItemTemplate>  
                                        <EditItemTemplate> 
                                            <asp:LinkButton ID="lbkUpdate" runat="server" CommandName="Update" Text="Save" CausesValidation="false"></asp:LinkButton> 
                                            <asp:LinkButton ID="lnkCancel" runat="server"  CommandName="Cancel" Text="Cancel" CausesValidation="false"></asp:LinkButton>                                                
                                        </EditItemTemplate> 
                                        <FooterTemplate> 
                                            <asp:LinkButton ID="lnkAdd" runat="server" CommandName="Insert" Text="Save" CausesValidation="false"></asp:LinkButton>                                             
                                            <asp:LinkButton ID="lnkCancel" runat="server" CommandName="CancelInsert" Text="Cancel" CausesValidation="false"></asp:LinkButton>
                                        </FooterTemplate> 
                                    </asp:TemplateField> 
	                                <asp:TemplateField  HeaderText="Grant Code">
	                                    <ItemTemplate >
	                                        <asp:Label ID ="lbldesc" Text='<%# Eval("lnkGrantCodeList_GrantCode") %>' runat ="server"></asp:Label>
	                                    </ItemTemplate>
	                                    <EditItemTemplate>
	                                    
	                                        <asp:DropDownList ID="drpGrantCode" runat="server" EnableViewState="true" ></asp:DropDownList>
	                                    </EditItemTemplate> 
	                                    <FooterTemplate >
	                                        <asp:DropDownList ID="drpGrantCodeNew" runat="server" EnableViewState="true" AutoPostBack="false" ></asp:DropDownList>
	                                    </FooterTemplate> 
	                                    <HeaderStyle Font-Bold="true" HorizontalAlign ="Center" Font-Size="11px"/>                                     
	                                </asp:TemplateField>	                                                              
	                            </Columns>	                           
	                        </asp:GridView>
	                        <asp:Button ID="btnAdd" runat="server" Text="Add" Enabled="true" Visible="false" CommandName="Add" OnClick="Add_Click" CausesValidation ="false"  />
								    </TD>
								</tr>
								<tr>
								    <TD class="label" style="HEIGHT: 31px" width="50%">Fund Comments</TD>
								    <TD style="HEIGHT: 31px" width="50%">
								        <bnbdatacontrol:BnbTextBox ID="bnbTxtFundingComments" runat="server" CssClass="textbox" DataMember="BnbProgrammeFunding.FundingComments"
								        ControlType="text" Width="250px" MultiLine="True" Rows="3"/>
								</TD>
								</tr>
								
							</TABLE>
			</div>
		</form>
	</body>
</HTML>
