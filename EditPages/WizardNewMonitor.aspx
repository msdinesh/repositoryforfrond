<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Page Language="c#" Codebehind="WizardNewMonitor.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.EditPages.WizardNewMonitor" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>WizardNewMonitor</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <div class="wizardheader" ms_positioning="FlowLayout">
                <h3><IMG class="icon24" src="../Images/wizard24silver.gif">
                    New&nbsp;Monitor and Evaluation&nbsp;Wizard</h3>
            </div>
          
            <p>
                <asp:Panel ID="panelZero" runat="server" CssClass="wizardpanel">
                    <p>
                        <em>This wizard will help you enter Monitor and Evaluation information for a Placement.</em></p>
                    <p>
                    </p>
                    <p>
                        Placement Ref:
                        <asp:Label ID="labelPlacementRef" runat="server"></asp:Label></p>
                    <p>
                        Employer Name:
                        <asp:Label ID="labelEmployerName" runat="server"></asp:Label></p>
                    <p>
                        Volunteer Name:
                        <asp:Label ID="labelVolunteerName" runat="server"></asp:Label></p>
                    <p>
                        <asp:Label ID="labelPlacementInfo" runat="server"></asp:Label></p>
                    <table class="wizardtable">
                        <tr>
                            <td class="label">
                                Lead VSO Goal</td>
                            <td>
                                <asp:DropDownList ID="dropDownLeadObjective" runat="server" Width="200px">
                                </asp:DropDownList>
                                <asp:Label ID="labelLeadGoal" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="label">
                                Secondary VSO Goal</td>
                            <td>
                                <asp:DropDownList ID="dropDownSecondaryObjective" runat="server" Width="200px">
                                </asp:DropDownList>
                                <asp:Label ID="labelSecondaryGoals" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                    <p>
                        <p>
                            &nbsp;
                            <asp:Label ID="labelPanelZeroFeedback" runat="server" CssClass="feedback"></asp:Label></p>
                </asp:Panel>

                <p>
                    <asp:Panel ID="panelOne" runat="server" CssClass="wizardpanel">
                        <p>
                            Please fill in the following Monitor and Evaluation details and press Finish:
                        </p>
                        <table class="wizardtable">
                            <tr>
                                <td class="label" width="40%">
                                    Placement Ref</td>
                                <td>
                                    <asp:Label ID="labelPlacementRef2" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="label" width="40%">
                                    Employer Name</td>
                                <td>
                                    <asp:Label ID="labelEmployerName2" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="label" width="40%">
                                    Volunteer Name</td>
                                <td>
                                    <asp:Label ID="labelVolunteerName2" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="label" width="40%">
                                    Achievement of placement objectives</td>
                                <td>
                                    <asp:DropDownList ID="dropDownAchievement" runat="server" Width="251px">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td class="label" width="40%">
                                    How satisfied is the volunteer with:</td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="label" width="40%">
                                    Placement</td>
                                <td>
                                    <asp:DropDownList ID="dropDownSatVolPlacement" runat="server" Width="251px">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td class="label" width="40%">
                                    VSO</td>
                                <td>
                                    <asp:DropDownList ID="dropDownSatVolVSO" runat="server" Width="251px">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td class="label" width="40%">
                                    How satisfied is the employer with:</td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="label" width="40%">
                                    Volunteer</td>
                                <td>
                                    <asp:DropDownList ID="dropDownSatEmpVolunteer" runat="server" Width="249px">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td class="label" width="40%">
                                    VSO</td>
                                <td>
                                    <asp:DropDownList ID="dropDownSatEmpVSO" runat="server" Width="248px">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td class="label" width="40%">
                                    Comments</td>
                                <td>
                                    <asp:TextBox ID="textComments" runat="server" Width="245px" TextMode="MultiLine"
                                        Height="56px"></asp:TextBox></td>
                            </tr>
                        </table>
                        <p>
                            <asp:Label ID="labelPanelOneFeedback" runat="server" CssClass="feedback"></asp:Label></p>
                    </asp:Panel>
 
                    <p>
                        <uc1:WizardButtons ID="wizardButtons" runat="server"></uc1:WizardButtons>
                    </p>
        </div>
    </form>
</body>
</html>
