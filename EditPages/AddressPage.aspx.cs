using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboWebControls.DataControls;
using BonoboDomainObjects;
using BonoboEngine;
using System.Text;
namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for AddressPage.
	/// </summary>
	public partial class AddressPage : System.Web.UI.Page
	{
		#region controls
		protected UserControls.Title titleBar;
		protected Frond.UserControls.NavigationBar NavigationBar1;
		
		

		BnbWebFormManager bnb = null;
	
		#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
			
			
			// Put user code to initialize the page here
			// IndividualAddressID=902E1C14-7A55-11D7-B76B-0002A5D43D09
#if DEBUG
			if(Page.Request.QueryString.ToString()=="")
				bnb = new BnbWebFormManager("BnbIndividual=D13DEB7F-BE02-11D6-909F-00508BACE998&BnbIndividualAddress=D13DEB87-BE02-11D6-909F-00508BACE998");
					//"BnbIndividual=D13DEB7F-BE02-11D6-909F-00508BACE998&BnbIndividualAddress=4EB6486F-C986-425B-980A-8947A85EECFC");
					//"BnbIndividual=d13deb8f-be02-11d6-909f-00508bace998&BnbIndividualAddress=new&PageState=new&AddressTypeID=1056");
			else
				doLoadManager();
#else
			doLoadManager();
#endif
			bnb.EditModeEvent +=new EventHandler(bnb_EditModeEvent);
			bnb.ViewModeEvent +=new EventHandler(bnb_ViewModeEvent);
			bnb.SaveEvent +=new EventHandler(bnb_SaveEvent);
			NavigationBar1.RootDomainObject = "BnbIndividual";

			// tweak page title settings
			bnb.PageNameOverride = "Address";
			bnb.LogPageHistory = true;
			BnbWorkareaManager.SetPageStyleOfUser(this);
			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;

				doLoadTelephone();
                //Added newly for role based access
                DoMapVisibility();
			}

			// sort out address type and re-direction for new records
			doAddressType();
			doShowAddressPanels();
			doCompanyHyperlinks();
			// this is needed so that the postcode find button does not trigger a regular postback
			uxPostcodeSearch.Attributes.Add("onclick","javascript:__doBonoboPostBack(this.name,'keepediting');");
			uxAddressLine1Search.Attributes.Add("onclick","javascript:__doBonoboPostBack(this.name,'keepediting');");
            //For capturing Geographical locations
            AssignGeoDetails("LoadValues");
            
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			this.PreRender+=new EventHandler(AddressPage_PreRender);
						
			//bnbdgContactNumbers.Load+=new EventHandler(bnbdgContactNumbers_Load);
			base.OnInit(e);
			
			textAddressLine1.PreRender +=new EventHandler(textAddressLine1_PreRender);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private functions

		private string appid = "";

		private void doLoadManager()
		{
			string indid = Page.Request.QueryString["BnbIndividual"];
			if(indid == null) 
			{
				indid = getIndividualIDFromApplication();
				string qs = Page.Request.QueryString.ToString();
				qs = qs.Replace("BnbApplication="+appid,"BnbIndividual="+indid);
				BnbWebFormManager.ReloadPage(qs);
			}
			else 
			{
				bnb = new BnbWebFormManager(this,"BnbIndividualAddress");
			}
		}

		private string getIndividualIDFromApplication() 
		{
			appid = Page.Request.QueryString["BnbApplication"];
			BnbApplication app = BnbApplication.Retrieve(new System.Guid(appid));
			return app.IndividualID.ToString().ToUpper();
		}

		#endregion

		
		private void textAddressLine1_PreRender(object sender, EventArgs e)
		{
			// disable address lines for addresses that are linked to companies
			
			BnbIndividualAddress currentIA = (BnbIndividualAddress)bnb.ObjectTree.DomainObjectDictionary["BnbIndividualAddress"];
			if (currentIA != null && currentIA.Address != null && currentIA.Address.LinkedCompany != null)
			{
				textAddressLine1.Visible = true;
				textAddressPostcode.Visible = true;
				textAddressLine1.AlwaysReadOnly  = true;
				textAddressLine2.AlwaysReadOnly  = true;
				textAddressLine3.AlwaysReadOnly  = true;
				textAddressLine4.AlwaysReadOnly  = true;
				textAddressCounty.AlwaysReadOnly  = true;
				textAddressPostcode.AlwaysReadOnly  = true;
				dropDownAddressCountryID.AlwaysReadOnly  = true;

			}
		}

        private void AddressPage_PreRender(object sender, EventArgs e)
        {
        }

		private void doShowAddressPanels()
		{
			BnbIndividualAddress tempIALink = bnb.ObjectTree.DomainObjectDictionary["BnbIndividualAddress"] as BnbIndividualAddress;
			
			if (tempIALink != null)
			{
				// if the corresponding fields are considered 'writeable' by the domain objects,
				// the make the panels visible
				panelEmergencySection.Visible = tempIALink.IsEmergencyAddress;
				panelBankingSection.Visible = tempIALink.IsBankAddress;
				panelWorkSection.Visible = tempIALink.IsWorkAddress;
			}
			else
			{
				// if in doubt, make everything visible
				panelEmergencySection.Visible = true;
				panelBankingSection.Visible = true;
				panelWorkSection.Visible = true;
			}

		}

		

		private void doRedirectToNewAddressWizard()
		{
			if (this.Request.QueryString["BnbIndividualAddress"] != null &&
			this.Request.QueryString["BnbIndividualAddress"].ToLower() == "new")
				Response.Redirect("WizardNewAddress.aspx?BnbIndividual=" + this.Request.QueryString["BnbIndividual"]);
		}

		/* not needed after VAM-379
		private void doNewAddressFillIn()
		{
			// is address transfer filled in?
			string addressTransfer = Request.QueryString["AddressTransfer"];
			if (addressTransfer != null)
			{
				// pull the address details from the wizard out of the Session[]
				Hashtable addressTrans = (Hashtable)Session["AddressTransfer"];
				BnbAddress a = bnb.GetPageDomainObject("BnbIndividualAddress.Address",true) as BnbAddress;
				a.AddressLine1 = (string)addressTrans["Line1"];
				a.AddressLine2 = (string)addressTrans["Line2"];
				a.AddressLine3 = (string)addressTrans["Line3"];
				a.PostCode = (string)addressTrans["PostCode"];
				a.County = (string)addressTrans["County"];
				a.CountryID = (Guid)addressTrans["CountryID"];

			}

		}
		*/

		private void doAddressType()
		{
			string addresstypeidstring = Page.Request.QueryString["AddressTypeID"];
			if(addresstypeidstring != null)
			{
				int addressTypeID = Int32.Parse(addresstypeidstring);
				// re-direct to wizard if work or bank address
				if (BnbIndividualAddress.AddressTypeIsWorkAddress(addressTypeID) 
					|| BnbIndividualAddress.AddressTypeIsBankAddress(addressTypeID))
				{
					// get individualId
					string indvId = Page.Request.QueryString["BnbIndividual"];
					Response.Redirect(String.Format("./WizardNewWorkAddress.aspx?BnbIndividual={0}&AddressTypeID={1}",
						indvId, addresstypeidstring), true);

				}


				BnbIndividualAddress ia = bnb.GetPageDomainObject("BnbIndividualAddress",true) as BnbIndividualAddress;
				ia.AddressTypeID = addressTypeID;
			}

		}

		/// <summary>
		/// fires when the web form manager goes into edit mode
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void bnb_EditModeEvent(object sender, EventArgs e)
		{
			doDisplayTelephoneNumbers(true);
			// make the find button visible
			uxPostcodeSearch.Visible = true;
			uxAddressLine1Search.Visible = true;
            if (HasPermissionToLocateOnMap())
            {
                btnShowLocation.Visible = true;
            }
			// was the address list clicked?
			// Note that WebFormManagerMode must be set to true on the addresslookupcomposite for
			// this to work in the EditModeEvent
			if (BnbWebFormManager.EventTarget == "uxAddressLookupComposite")
			{
				
				textAddressLine1.OverrideText = uxAddressLookupComposite.Line1;
				textAddressLine2.OverrideText = uxAddressLookupComposite.Line2;
				textAddressLine3.OverrideText = uxAddressLookupComposite.Line3;
				textAddressLine4.OverrideText = uxAddressLookupComposite.Town;
				textAddressCounty.OverrideText = uxAddressLookupComposite.County;
				textAddressPostcode.OverrideText = uxAddressLookupComposite.PostCode;
			}
            //Set Page mode for GMap
            SetGeoMode("new/edit");
		}

		protected void uxPostcodeSearch_Click(object sender, System.EventArgs e)
		{
			// need to get postcode and country from request form
			 
			string postcode = Request.Form["textAddressPostcode"];
			string countryID = Request.Form["dropDownAddressCountryID"];
			string line1 = Request.Form["textAddressLine1"];
			
			
				// do search and activate control
				uxAddressLookupComposite.CountryID = new Guid(countryID);
				uxAddressLookupComposite.PostCode = postcode;
				uxAddressLookupComposite.Line1 = line1;
				uxAddressLookupComposite.PerformSearch();
				uxAddressLookupComposite.Visible = true;
			
		}

		

		/// <summary>
		/// fires when the web form manager goes into view mode
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void bnb_ViewModeEvent(object sender, EventArgs e)
		{
			// make the find button invisible
			uxPostcodeSearch.Visible = false;
			uxAddressLine1Search.Visible = false;
            btnShowLocation.Visible = false;
			uxAddressLookupComposite.Visible = false;

			// do tel numbers
			doDisplayTelephoneNumbers(false);
            doLoadGeographicalDetails();

            //Set Page mode for GMap
            SetGeoMode("view");
            //For capturing Geographical locations
            AssignGeoDetails("ViewValues");

		}

		/// <summary>
		/// Tel and Fax numbers are represented by separate BnbContactNumber objects.
		/// They cannot be bound using the BnbWebFormManager, so are implemented as normal
		/// ASP.NET controls on the page
		/// This displays the correct controls depending on edit mode
		/// </summary>
		private void doDisplayTelephoneNumbers(bool editMode)
		{
			uxTelephoneTextBox.Visible = editMode;
			uxFaxTextBox.Visible = editMode;
			uxTelephoneLabel.Visible = !editMode;
			uxFaxLabel.Visible = !editMode;
            //VAF-777
            uxEmailTextBox.Visible = editMode;
            uxEmailLabel.Visible = !editMode;			
		}

		/// <summary>
		/// Tel and Fax numbers are represented by separate BnbContactNumber objects.
		/// They cannot be bound using the BnbWebFormManager, so are implemented as normal
		/// ASP.NET controls on the page
		/// This finds the right ones and loads the data into the controls
		/// </summary>
		private void doLoadTelephone()
		{
			BnbIndividualAddress iaLink = bnb.GetPageDomainObject("BnbIndividualAddress") as BnbIndividualAddress;
			if (iaLink != null)
			{
				// need to know owner individual to get contact number objects
				BnbIndividual owner = iaLink.Individual;
				string tel = iaLink.Address.GetTelephone(owner);
				string fax = iaLink.Address.GetFax(owner);
                string email = iaLink.Address.GetEmail(owner);
				// there are different controls depending on edit mode
				if (tel!= null)
				{
					uxTelephoneTextBox.Text = tel;
					uxTelephoneLabel.Text = tel;
				}
				else
				{
					uxTelephoneLabel.Text = "";
					uxTelephoneTextBox.Text = "";
				}
				if (fax!=null)
				{
					uxFaxTextBox.Text = fax;
					uxFaxLabel.Text = fax;
				}
				else
				{
					uxFaxLabel.Text = "";
					uxFaxTextBox.Text = "";
				}
                //VAF-777
                if (email != null)
                {
                    uxEmailTextBox.Text = email;
                    uxEmailLabel.Text = email;
                }
                else
                {
                    uxEmailLabel.Text = "";
                    uxEmailTextBox.Text = "";
                }

			}
			else
			{
				// if no object, must be new mode
				uxTelephoneLabel.Text = "";
				uxTelephoneTextBox.Text = "";
				uxFaxLabel.Text = "";
				uxFaxTextBox.Text = "";
                //VAF-777
                uxEmailLabel.Text = "";
                uxEmailTextBox.Text = "";
			}
		}

        /// <summary>
        /// Geographical details cannot be bound using the BnbWebFormManager, so are implemented as normal
        /// ASP.NET controls on the page
        /// This finds the right ones and loads the data into the controls
        /// </summary>
        private void doLoadGeographicalDetails()
        {
            if (HasPermissionToLocateOnMap())
            {
                BnbIndividualAddress iaLink = bnb.GetPageDomainObject("BnbIndividualAddress") as BnbIndividualAddress;
                if (iaLink != null)
                {
                    float lat = iaLink.Address.Latitude;
                    float lng = iaLink.Address.Longitude;

                    showLocation.Latitude = lat.ToString();
                    showLocation.Longitude = lng.ToString();
                }
            }      
        }


		/// <summary>
		/// Tel and Fax numbers are represented by separate BnbContactNumber objects.
		/// They cannot be bound using the BnbWebFormManager, so are implemented as normal
		/// ASP.NET controls on the page
		/// This checks for data in the controls and saves the objects
		/// </summary>
		private void doSaveTelephone()
		{
			// find the main object
			BnbIndividualAddress iaLink = bnb.GetPageDomainObject("BnbIndividualAddress", true) as BnbIndividualAddress;
			// need to know owner individual to save contact number objects
			BnbIndividual owner = iaLink.Individual;
			
			if (uxTelephoneTextBox.Text != "")
				iaLink.Address.SetTelephone(owner, uxTelephoneTextBox.Text);
			else
				iaLink.Address.RemoveTelephone(owner);

			if (uxFaxTextBox.Text != "")
				iaLink.Address.SetFax(owner, uxFaxTextBox.Text);
			else
				iaLink.Address.RemoveFax(owner);

            //VAF-777
            if (uxEmailTextBox.Text != "")
                iaLink.Address.SetEmail(owner, uxEmailTextBox.Text);
            else
                iaLink.Address.RemoveEmail(owner);

		}
        /// <summary>
        /// Latitude, Longitude cannot be bound using the BnbWebFormManager, 
        /// so are implemented as normal
        /// ASP.NET controls on the page
        /// This checks for data in the controls and saves the objects
        /// </summary>
        private void doSaveGeographicalDetails()
        {
            if (HasPermissionToLocateOnMap())
            {
                // find the main object
                BnbIndividualAddress iaLink = bnb.GetPageDomainObject("BnbIndividualAddress", true) as BnbIndividualAddress;

                if (showLocation.Latitude.Trim() != "")
                    iaLink.Address.Latitude = Convert.ToSingle(showLocation.Latitude);
                else
                    //Setting null value for validation
                    iaLink.Address.Latitude = Convert.ToSingle(0.0);

                if (showLocation.Longitude.Trim() != "")
                    iaLink.Address.Longitude = Convert.ToSingle(showLocation.Longitude);
                else
                    //Setting null value for validation
                    iaLink.Address.Longitude = Convert.ToSingle(0.0);
            }

        }
		/// <summary>
		/// Fires when the BnbWebFormManager is performing a save
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void bnb_SaveEvent(object sender, EventArgs e)
		{
			this.doSaveTelephone();
            this.doSaveGeographicalDetails();
		}

		private void doCompanyHyperlinks()
		{
			// find the main object
			BnbIndividualAddress iaLink = bnb.GetPageDomainObject("BnbIndividualAddress") as BnbIndividualAddress;
			if (iaLink != null)
			{
				// if the address is linked to a company,
				// render hyperlinks in company name and bank name 
				if (iaLink.Address.CompanyAddress != null)
				{
					BnbCompany linkedCompany = iaLink.Address.CompanyAddress.Company;
					uxOrganisationNameHyper.NavigateUrl = String.Format("OrganisationPage.aspx?BnbCompany={0}", linkedCompany.ID);
					uxOrganisationNameHyper.Text = linkedCompany.CompanyName;
					uxBankNameHyper.NavigateUrl = String.Format("OrganisationPage.aspx?BnbCompany={0}", linkedCompany.ID);
					uxBankNameHyper.Text = linkedCompany.CompanyName;
				}
			}
		}

        //For capturing Geographical locations
        //Generate client script for setting Geographical locations
        private void AssignGeoDetails(string scriptID)
        {
            if (HasPermissionToLocateOnMap())
            {
                showLocation.CssName = "gmaptableNormal";
                StringBuilder script = new StringBuilder();
                script.Append("pageName ='AddressPage';");

                if (!(showLocation.Latitude.Trim() == string.Empty))
                {
                    ViewState["Latittude"] = showLocation.Latitude;
                    ViewState["Longitude"] = showLocation.Longitude;
                }

                if (!(ViewState["Latittude"] == null))
                {
                    string latitude = ViewState["Latittude"].ToString();
                    string longitude = ViewState["Longitude"].ToString();
                    script.Append("latVal = " + latitude + ";lngVal = " + longitude + ";");
                }
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), scriptID, script.ToString(), true);
            }
        }

        //For capturing Geographical locations
        //Set page mode to control GMap 
        private void SetGeoMode(string mode)
        {
            if (HasPermissionToLocateOnMap())
            {
                string strScript = "mode='" + mode + "';";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "SetMode", strScript, true);
            }

        }

        /// <summary>
        /// Check permisssion to access the Google map functionality
        /// </summary>
        private bool HasPermissionToLocateOnMap()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_LocateAddressOnMap))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Added newly for role based access
        /// </summary>
        private void DoMapVisibility()
        {
            if (HasPermissionToLocateOnMap())
            {
               
                headingGeoDetails.Visible = true;
                showLocation.Visible = true;
                btnShowLocation.Visible = true;
            }
            else
            {
                headingGeoDetails.Visible = false;
                showLocation.Visible = false;
                btnShowLocation.Visible = false;
            }
        }
	}
}

