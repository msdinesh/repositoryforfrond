<%@ Page Language="C#" AutoEventWireup="true" Codebehind="XmlImportPage.aspx.cs"
    Inherits="Frond.EditPages.XmlImportPage" %>

<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.GenericControls"
    TagPrefix="bnbgenericcontrols" %>

<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.DataGrids"
    TagPrefix="bnbdatagrid" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <head>
        <title>XmlImport</title>
        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
        <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
        <asp:literal id="literalAfterCss" runat="server"></asp:literal>
    </head>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/batchprocess24silver.gif">
            </uc1:Title>
            <div style="float: left">
                <asp:Panel ID="panelXmlImport" runat="server">
                    <p>
                    </p>
                    <table class="fieldtable" id="tblFieldTableMain" width="100%" border="0">
                   
                        <tr>
                            <td width="50%" class="label" style="height: 25px">
                                Xml Schema &nbsp;
                            </td>
                            <td style="width: 50%; height: 25px;">
                                <asp:RadioButton ID="rdoDefault" AutoPostBack="true" runat="server" Checked="true"
                                    Text="Use Default" GroupName="XmlSchema" OnCheckedChanged="rdoDefault_CheckedChanged" />
                                <asp:RadioButton ID="rdoCustom" AutoPostBack="true" runat="server" Text="Custom Schema"
                                    GroupName="XmlSchema" OnCheckedChanged="rdoCustom_CheckedChanged" /></td>
                        </tr>
                        <tr runat="server" id="trSchema" visible="false">
                            <td width="50%" class="label">
                                Load Xml Schema &nbsp;
                            </td>
                            <td style="width: 50%">
                                <asp:FileUpload ID="fuXmlSchema" runat="server" /></td>
                        </tr>
                         <tr>
                            <td width="50%" class="label">
                                Load Xml File &nbsp;
                            </td>
                            <td style="width: 50%">
                                <asp:FileUpload ID="fuXml" runat="server" /></td>
                        </tr>
                        <tr>
                            <td width="50%" class="label">
                                Xml Mapping Template &nbsp;
                            </td>
                            <td width="50%">
                             <asp:DropDownList ID="cmbXmlMapping" runat="server" Width="250px"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" width="50%">
                                <asp:Button ID="btnImportData" runat="server" Text="Import Data" OnClick="Import_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" /></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblScreenFeedback" runat="server" CssClass="feedback"></asp:Label>
                            </td>
                        </tr>
                        
                         
                    </table>
                </asp:Panel>
      <asp:Panel ID="panelImportResult" runat="server">
        <table class="fieldtable" id="tblFieldTableSub" width="100%" border="0">
        
                         <tr>
                            <td>
                                <bnbgenericcontrols:bnbhyperlink id="lnkXmlImportDetails" runat="server">View Import Details</bnbgenericcontrols:bnbhyperlink>
                            </td>
                        </tr>
         </table>
       </asp:Panel>
            </div>
        </div>
    </form>
</body>
</html>
