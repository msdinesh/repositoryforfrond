using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for WizzardNewEvent.
	/// </summary>
	public partial class WizzardNewEvent : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.RequiredFieldValidator lblValidateEventReference1;
		protected UserControls.WizardButtons wizardButtons;

		private Utilities mobjUtility = new Utilities();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			BnbEngine.SessionManager.ClearObjectCache();
			
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);

			// Put user code to initialize the page here
			wizardButtons.AddPanel(panelTwo);

			if (!this.IsPostBack)
			{
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				initWizard();
				BonoboWebControls.BnbWebFormManager.LogAction("New Event Wizard", null, this);
				if (this.Request.UrlReferrer != null)
					txtHiddenReferrer.Text = this.Request.UrlReferrer.ToString();
				else
					txtHiddenReferrer.Text = "~/Menu.aspx";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			wizardButtons.ShowPanel +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
			wizardButtons.ValidatePanel +=new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
			wizardButtons.CancelWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
			wizardButtons.FinishWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);

			

		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


		public void initWizard()
		{
			// event type lookup on panel zero
			BnbLookupDataTable eventTypeLookup = BnbEngine.LookupManager.GetLookup("lkuEventGroup");
			DataView dtvEventGroupView = new DataView(eventTypeLookup);
			dtvEventGroupView.RowFilter = "Exclude = 0";
			dtvEventGroupView.Sort = "Description";
			
//			// vaf-62: add a blank item
			ListItem blankItem = new ListItem("<Any>","");
			cmbEventType.Items.Insert(0,blankItem);

			// event type lookup on panel two
			cmbEventGroup2.DataSource = dtvEventGroupView;
			cmbEventGroup2.DataValueField = "IntID";
			cmbEventGroup2.DataTextField = "Description";
			cmbEventGroup2.DataBind();

			this.SetupPanelTwo();
		}

		
		public void SetupPanelTwo()
		{

		
		
			mobjUtility.FillCombos(cmbEventType,"lkuEventType","IntID","EventGroupID = " + cmbEventGroup2.SelectedValue.ToString(),"");
			mobjUtility.FillCombos(cmbCountry,"lkuCountry","GuidID","Exclude = 0","");
			Guid defaultCountryID = BnbEngine.SessionManager.GetSessionInfo().DefaultCountryID;
            //Code Integrated for displaying defaultcountry only to starfish users
            if ((BnbEngine.SessionManager.GetSessionInfo().WebStyleID) == 11)
			if (defaultCountryID != Guid.Empty)
			{
				cmbCountry.SelectedValue = defaultCountryID.ToString();
				mobjUtility.FillCombos(cmbEventSite, "vlkuBonoboEventSite","GuidID","CountryID = '" + Convert.ToString(cmbCountry.SelectedValue) + "'","93254B59-AAA9-11D4-908E-00508BACE998");
			}


		}

		private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
		{
			

			
		}

		private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			
		}

		private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			Response.Redirect(txtHiddenReferrer.Text);
		}

		private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			this.CreateNewEvent();
		}

		private void dataGridEvents_ItemCommand(object source, DataGridCommandEventArgs e)
		{
			TableCell idCell = e.Item.Cells[1];
			Guid guiEventID = new Guid(idCell.Text);
			//this.EnsureCompanyIsDonor(companyID);
			Response.Redirect("EventPage.aspx?BnbEvent=" + guiEventID.ToString());
		}

		/// <summary>
		/// takes the data from the various controls on the wizard and uses
		/// it to create a totally new company.
		/// and valdiation messages are shown in labelPanelTwoFeedback.
		/// if no problems, re-directs to org donor page
		/// </summary>
		private void CreateNewEvent()
		{
			BnbEditManager em = new BnbEditManager();
			BnbEvent objEvent = new BnbEvent(true);
			objEvent.RegisterForEdit(em);
			if (cmbCountry.SelectedValue.ToString().Trim().Length > 0)
				objEvent.CountryID = new Guid(cmbCountry.SelectedValue);
			objEvent.EventDescription = txtEventReference2.Text;
			if (!dtpDateFrom.IsEmpty)
				objEvent.EventStart = dtpDateFrom.Date;
			if (!dtpDateTo.IsEmpty)
				objEvent.EventEnd = dtpDateTo.Date;
			else
				objEvent.EventEnd = dtpDateFrom.Date;
			
			if (cmbEventType.SelectedValue.ToString().Trim().Length > 0)
				objEvent.EventTypeID = Convert.ToInt32(cmbEventType.SelectedValue);
			if (cmbEventGroup2.SelectedValue.ToString().Trim().Length > 0)
				objEvent.EventGroupID = Convert.ToInt32(cmbEventGroup2.SelectedValue);
			if (cmbEventSite.SelectedValue.ToString().Trim().Length > 0)
				objEvent.EventSiteID = new Guid(cmbEventSite.SelectedValue);
			else
				objEvent.EventSiteID = new Guid("93254B59-AAA9-11D4-908E-00508BACE998");
			if (cmbLocation.SelectedValue.ToString().Trim().Length > 0)
				objEvent.EventLocationID = new Guid(cmbLocation.SelectedValue);

			try
			{
				em.SaveChanges();
				Response.Redirect("EventPage.aspx?BnbEvent=" + objEvent.ID.ToString());
			}
			catch(BnbProblemException pe)
			{
				lblPanelTwoFeedback.Text = "Unfortunately, the Event could not be created due to the following problems: <br/>";
				foreach(BnbProblem p in pe.Problems)
					lblPanelTwoFeedback.Text += p.Message + "<br/>";

			}
		
		}

		protected void cmbEventGroup2_SelectedIndexChanged(object sender, System.EventArgs e)
		{

			cmbEventType.Dispose();
			mobjUtility.FillCombos(cmbEventType,"lkuEventType","IntID","EventGroupID = " + cmbEventGroup2.SelectedValue.ToString(),"");

		}

		protected void cmbCountry_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			mobjUtility.FillCombos(cmbEventSite, "vlkuBonoboEventSite","GuidID","CountryID = '" + Convert.ToString(cmbCountry.SelectedValue) + "'","93254B59-AAA9-11D4-908E-00508BACE998");
	
		}

		protected void cmbEventSite_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			cmbLocation.Dispose();
			mobjUtility.FillCombos(cmbLocation,"lkuEventLocation","GuidID","EventSiteID = '" + Convert.ToString(cmbEventSite.SelectedValue) + "'","E8B7E7C3-71CD-11D4-A3BA-00508B09C980");
		}

		



	}
}
