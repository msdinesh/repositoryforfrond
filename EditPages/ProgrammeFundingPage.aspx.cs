using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;
using BonoboEngine;
using BonoboEngine.Query;
using System.Text;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for ProgrammeFundingPage.
	/// </summary>
	public partial class ProgrammeFundingPage : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
		
	
		private BnbWebFormManager bnb = null;
		protected System.Web.UI.WebControls.Label lbldeleteMessage;
		private bool showDeleteButton = false;

		protected void Page_Load(object sender, System.EventArgs e)
		{
#if DEBUG
			if(Request.QueryString.ToString()+"" == "") 
			{
				BnbWebFormManager.ReloadPage("BnbPosition=B7841969-2905-431A-A829-EFEB0E0EAA45&BnbProgrammeFunding=74BDEFB1-6239-4BF8-A70E-7B76BE4E7E4B");
			}
#endif
			// Put user code to initialize the page here

			//<!-- This swaps the BnbApplication with BnbPosition in the querystring
			//essential for the datamembers in the controls to work properly
			string posid = Request.QueryString["BnbPosition"]+"";
			string appid = Request.QueryString["BnbApplication"]+"";
			string progfundid=Request.QueryString["BnbProgrammeFunding"]+"";
			string qs = Request.QueryString.ToString();
            if (progfundid != null && !progfundid.ToLower().Equals("new"))
            {
                BnbProgrammeFunding pf = BnbProgrammeFunding.Retrieve(new System.Guid(progfundid));
                if (pf.GrantCodeList.Count != 0)
                    btnAdd.Visible = true;
            }
			if (posid=="" && progfundid!="" && appid!="")
			{
				BnbProgrammeFunding pf = BnbProgrammeFunding.Retrieve(new System.Guid(progfundid));
				posid = pf.PositionID.ToString();
				qs = qs.Replace("BnbApplication=" + appid,"BnbPosition=" + posid);
				BnbWebFormManager.ReloadPage(qs);
			}
			else
				if(posid == "" && appid != "")  
			{
				BnbApplication app = BnbApplication.Retrieve(new System.Guid(appid));
				posid = app.PlacedService.PositionID.ToString();
				qs = qs.Replace("BnbApplication=" + appid,"BnbPosition=" + posid);
				BnbWebFormManager.ReloadPage(qs);
			}
			//-->

			bnb = new BnbWebFormManager(this, "BnbProgrammeFunding");
			bnb.LogPageHistory = true;
			BnbWorkareaManager.SetPageStyleOfUser(this);
			doInitialiseControls();
			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
                grantCodeGrid_Fill();
			}

			Utilities.ShowViewWarnings(bnb, lblViewWarnings);

            if (bnb.PageState != PageState.Edit)
            {
                btnAdd.Enabled = true;
                grantCodeGrid.Enabled = true;
                //ddlGrantCodes.Visible = false;
                grantCodeGrid.Visible = true;
            }

            bnb.EditModeEvent += new EventHandler(EditModeEvent_Click);
            bnb.SaveEvent += new EventHandler(SaveEvent_Click);
            this.BnbDataButtons1.UndoClick += new EventHandler(BnbDataButtons1_UndoClick);
            lblErrorMsg.Text = "";
			//delete button code
			showDeleteButton = Request.QueryString["delete"] != null ? Convert.ToBoolean(Request.QueryString["delete"]) : false;
			if(!showDeleteButton)
			{
				btnDelete.Visible = false;
				btnDelete.Enabled = false;
			}
			else
			{
				btnDelete.Attributes.Add("onclick","return confirm('Are you sure you want to delete this record?');");
				btnDelete.Visible = true;
				btnDelete.Enabled = true;
			}
            grantCodeGrid.RowDataBound += new GridViewRowEventHandler(grantCodeGrid_RowDataBound);
            grantCodeGrid.RowEditing += new GridViewEditEventHandler(grantCodeGrid_RowEditing);
            grantCodeGrid.RowCancelingEdit += new GridViewCancelEditEventHandler(grantCodeGrid_RowCancelingEdit);
            grantCodeGrid.RowUpdating += new GridViewUpdateEventHandler(grantCodeGrid_RowUpdating);
            grantCodeGrid.RowCommand += new GridViewCommandEventHandler(grantCodeGrid_RowCommand);

            lblErrorMessage.Visible = false;
		}

        //TPT PGMSMIFI- 97 -->
        /// <summary>
        /// To show all Grantcodes  related to the selected Project code 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditModeEvent_Click(object sender, EventArgs e)
        {
            pnlGrantCode.Visible = true;
            BnbProgrammeFunding objProgramfunding = bnb.GetPageDomainObject("BnbProgrammeFunding", true) as BnbProgrammeFunding;
            string progfundid = Request.QueryString["BnbProgrammeFunding"] + "";
            if (progfundid != null && !progfundid.ToLower().Equals("new"))
            {
                BnbProgrammeFunding pf = BnbProgrammeFunding.Retrieve(new System.Guid(progfundid));
                if (!ddlGrantCodes.Visible)
                    doFillDdlGrantCodes(pf.FundCode.FundCode.Trim());
            }
            if (!objProgramfunding.IsNew && BnbRuleUtils.HasValue(objProgramfunding.FundCodeID) && (!BnbRuleUtils.HasValue(objProgramfunding.GrantCodes)))
            {
                ddlGrantCodes.Visible = true;
                doFillDdlGrantCodes(objProgramfunding.FundCode.FundCode.Trim());
            }

            if (!objProgramfunding.IsNew)
            {
                if (objProgramfunding != null && objProgramfunding.GrantCodeList.Count > 0)
                    ddlGrantCodes.Visible = false;
                else
                    ddlGrantCodes.Visible = true;
            }
            else
                ddlGrantCodes.Visible = false;
                grantCodeGrid.Enabled = false;
                btnAdd.Enabled = false;
        }

        /// <summary>
        /// To save selected GrantCode for the particular Project code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveEvent_Click(object sender, EventArgs e)
        {
            BnbProgrammeFunding objProgramfunding = bnb.GetPageDomainObject("BnbProgrammeFunding", true) as BnbProgrammeFunding;
            if (objProgramfunding != null && !objProgramfunding.IsNew && ddlGrantCodes.Visible)
                objProgramfunding.GrantCodes = ddlGrantCodes.SelectedItem.Text;
            if (objProgramfunding.IsNew && ddlGrantCodes.Visible)
                objProgramfunding.GrantCodes = ddlGrantCodes.SelectedItem.Text;
            pnlGrantCode.Visible = true;
        }

        private void BnbDataButtons1_UndoClick(object sender, EventArgs e)
        {
            pnlGrantCode.Visible = false;
            BnbProgrammeFunding objProgramfunding = bnb.GetPageDomainObject("BnbProgrammeFunding", true) as BnbProgrammeFunding;
            if (objProgramfunding.GrantCodeList.Count.Equals(0))
            {
                DataTable dt = Session["grantCodeTable"] as DataTable;
                dt.Clear();
                grantCodeGrid_Bind(dt);
                btnAdd.Visible = false;
            }
        }

        // Fill all GrantCode related to the selected FundCode
        private void doFillDdlGrantCodes(string fundcode)
        {
            BnbProgrammeFunding funding = bnb.GetPageDomainObject("BnbProgrammeFunding") as BnbProgrammeFunding;
            if (funding == null)
            {
                bindResultQueryToDdlGrantcodes(fundcode);
            }
            string progfundid = Request.QueryString["BnbProgrammeFunding"] + "";
            if (progfundid != null && !progfundid.ToLower().Equals("new"))
            {
                bindResultQueryToDdlGrantcodes(fundcode.Trim());
            }
        }

        // Finding the paticular Project Code from the selected Fund Code list
        private void findProjectCodeFromSelectedID(Guid id)
        {
            BnbTextCondition con = new BnbTextCondition("tblFundCode_FundCodeID", Convert.ToString(id), BnbTextCompareOptions.ExactMatch);
            BnbCriteria criteria = new BnbCriteria(con);
            BnbQuery query = new BnbQuery("vwBonoboFindFundCode", criteria);
            BnbQueryDataSet ds = query.Execute();
            DataTable dt = ds.Tables["Results"];
            string fundCode = dt.Rows[0].ItemArray[1].ToString();
            Session.Add("FundCode", fundCode);
            doFillDdlGrantCodes(fundCode.Trim());
        }

        // Finding Result Query (i.e. GrantCodes)using fundcode and bind to DDLGrantCodes
        private void bindResultQueryToDdlGrantcodes(string fundcode)
        {
            BnbTextCondition con = new BnbTextCondition("ProjectCode", fundcode, BnbTextCompareOptions.ExactMatch);
            BnbCriteria criteria = new BnbCriteria(con);
            BnbQuery query = new BnbQuery("vwBonoboGrantCodeList", criteria);
            BnbQueryDataSet ds = query.Execute();
            DataTable dt = ds.Tables["Results"];
            if (dt.Rows.Count != 0)
            {
                ddlGrantCodes.DataSource = dt;
                ddlGrantCodes.DataTextField = "Description";
                ddlGrantCodes.DataValueField = "Description";
                ddlGrantCodes.DataBind();
                ddlGrantCodes.Items.Insert(0, new ListItem(string.Empty, string.Empty));
                ddlGrantCodes.SelectedIndex = 0;
            }
            else
            {
                ddlGrantCodes.Visible = false;
                this.lblErrorMsg.Text = "There is no any Grant code for this Project code";
                grantCodeGrid.Visible = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProgrammeFundingPage_PreRenderComplete(object sender, EventArgs e)
        {
            if (Session["ProgrammeFundingDeleted"] != null && Session["ProgrammeFundingDeleted"].Equals(true))
            {
                Session.Add("ProgrammeFundingDeleted", false);
                return;
            }

            BnbProgrammeFunding funding = bnb.GetPageDomainObject("BnbProgrammeFunding",true) as BnbProgrammeFunding;
            if (funding.IsNew && Bnbsearchdomainobjectcomposite2.SelectedIDValue != Guid.Empty)
            {
                Session.Add("SelectedID1", Bnbsearchdomainobjectcomposite2.SelectedIDValue.ToString());
                ddlGrantCodes.Visible = true;
                Guid id1 = Bnbsearchdomainobjectcomposite2.SelectedIDValue;
                findProjectCodeFromSelectedID(id1);
            }

            if (!funding.IsNew)
            {
                Guid id = Bnbsearchdomainobjectcomposite2.SelectedIDValue;
                if (funding.FundCodeID != Bnbsearchdomainobjectcomposite2.SelectedIDValue)
                {
                    if (BnbRuleUtils.HasValue(funding.FundingStartDate))
                        lblErrorMsg.Text = lblErrorMsg.Text + "<br>You cannot include Grant codes once Funding start is set";
                    else
                    {
                        if (!(BnbRuleUtils.HasValue(funding.Position.StartDate) && BnbRuleUtils.HasValue(funding.FundingStartDate)))
                        {
                            if (funding.GrantCodeList.Count > 0)
                                deletePreviousGrantCodeList(funding.ID);
                                grantCodeGrid.Visible = false;
                                ddlGrantCodes.Visible = true;
                                findProjectCodeFromSelectedID(id);
                        }
                    }
                }
                else
                    findProjectCodeFromSelectedID(funding.FundCodeID);
            }
        }

        private void deletePreviousGrantCodeList(Guid pgmFundingID)
        {
            BnbProgrammeFunding pf = BnbProgrammeFunding.Retrieve(pgmFundingID);
            BnbEditManager em = new BnbEditManager();
            pf.RegisterForEdit(em);
            if (pf.GrantCodeList.Count > 0)
            {
                pf.GrantCodes = string.Empty;
                foreach (BnbGrantCodeList gcl in pf.GrantCodeList)
                {
                    if (pf.ID == gcl.ProgrammeFundingID)
                    {
                        BnbGrantCodeList objGcl = BnbGrantCodeList.GeneralRetrieve(gcl.ID, typeof(BnbGrantCodeList)) as BnbGrantCodeList;
                        objGcl.RegisterForEdit(em);
                        objGcl.MarkForDeletion();
                    }
                }

                em.SaveChanges();
            }
        }

        // TPT PGMSMIFI-97

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			this.dbxStartDate.PreRender+=new EventHandler(dbxStartDate_PreRender);
			this.dbxEndDate.PreRender+=new EventHandler(dbxEndDate_PreRender);
            this.Page.PreRenderComplete += new EventHandler(ProgrammeFundingPage_PreRenderComplete);
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void doInitialiseControls()
		{
			//<!-- VAF-57
			string appValue = Page.Request.QueryString["BnbApplication"];
			//-->
			string progValue = Page.Request.QueryString["BnbProgrammeFunding"];
			string posValue = Page.Request.QueryString["BnbPosition"];
			// if funding id specified but pos isnt, try getting from object
			if (progValue != null && progValue.ToLower() != "new")
			{
				BnbProgrammeFunding tempProg = BnbProgrammeFunding.Retrieve(new Guid(progValue));
				if (posValue == null && tempProg.PositionID != Guid.Empty)
					posValue = tempProg.PositionID.ToString();

			}
			
//			<!-- VAF-57
//			if (posValue != null && posValue.ToLower() != "new")
//			{
//
//				hlkPlacement.Text = pos.FullPlacementReference;
//				hlkPlacement.NavigateUrl = String.Format("PlacementPage.aspx?BnbPosition={0}",
//					pos.ID.ToString());
//				// does pos have an app?
//				if (pos.PlacedService != null && pos.PlacedService.Application != null)
//				{
//					BnbApplication app = pos.PlacedService.Application;
//					hlkVolunteer.Text = app.Individual.FullName;
//					hlkVolunteer.NavigateUrl = "ApplicationPage.aspx?BnbApplication=" + app.ID.ToString();
//				}
//			}
			
//			//BnbPosition pos = null;
//			//BnbApplication app = null;
//
//			if(appValue != null && posValue.ToLower() != "new") 
//			{
//				app = BnbApplication.Retrieve(new System.Guid(appValue));
//				pos = app.PlacedService.Position;
//			}
//			else if(posValue != null && posValue.ToLower() != "new") 
//			{
//				pos = BnbPosition.Retrieve(new System.Guid(posValue));
//				app = pos.PlacedService.Application;
//			}
//
//			if(pos == null) return;

			BnbPosition pos = BnbPosition.Retrieve(new System.Guid(Request.QueryString["BnbPosition"]));
			BnbApplication app = null;
			
			hlkPlacement.Text = pos.FullPlacementReference;
			hlkPlacement.NavigateUrl = String.Format("PlacementPage.aspx?BnbPosition={0}", pos.ID.ToString());

			if(pos.PlacedService != null && pos.PlacedService.Application != null) app = pos.PlacedService.Application;

			if(app == null) 
			{
				hlkVolunteer.Visible = false;
				//should not be able to edit the dates as this is not allowed when the position has not been filled
				dbxStartDate.Visible=false;
				dbxEndDate.Visible = false;
			}
			else 
			{
				hlkVolunteer.Visible = true;
				hlkVolunteer.Text = app.Individual.FullName;
				hlkVolunteer.NavigateUrl = "ApplicationPage.aspx?BnbApplication=" + app.ID.ToString();
			}
			
			//-->
		}

		//<!-- VAF-75
		private void dbxStartDate_PreRender(object sender, EventArgs e)
		{
			if(bnb.PageState == PageState.New && BnbDateBox1.Text !="") 
			{
				dbxStartDate.Text = BnbDateBox1.Text;
				
			}

			if(BnbDateBox1.Text !="" && BnbDateBox1.Text != "01/Jan/0001") 
			{
				btnDelete.Visible = false;
				btnDelete.Enabled = false;

				if(showDeleteButton)
				{
					lblViewWarnings.Text = "You cannot delete programme funding if it has been allocated a start date.";
					lblViewWarnings.Visible = true;
					lblViewWarnings.Enabled = true;
				}
			}
			
		}
		//-->

		//<!-- VAF-113
		private void dbxEndDate_PreRender(object sender, EventArgs e)
		{
			if(bnb.PageState == PageState.New && BnbDateBox4.Text != "") 
			{
				dbxEndDate.Text = BnbDateBox4.Text;
			}
		}

		/// <summary>
		/// Deletes a programme funding record from the db if the Application Id is not null and there is not a valid start date
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnDelete_Click(object sender, System.EventArgs e)
		{

			lblViewWarnings.Visible = true;
			lblViewWarnings.Enabled = true;
			string appid = Request.QueryString["BnbProgrammeFunding"] != null ? Request.QueryString["BnbProgrammeFunding"]: "";
			if(appid == "" || !showDeleteButton || (BnbDateBox1.Text !="" && BnbDateBox1.Text != "01/Jan/0001"))
			{
				lblViewWarnings.Text = "This Record does not have the correct Id.";
				lblViewWarnings.Visible = true;
				lblViewWarnings.Enabled = true;
				return;
			}
			//get the funding item to delete
			BnbProgrammeFunding progFunding = BnbProgrammeFunding.Retrieve(new Guid(appid));

			//register the object for edit
			BnbEditManager em = new BnbEditManager();
			progFunding.RegisterForEdit(em);

			// mark the record for deletion
			progFunding.MarkForDeletion();

			// record actually gets deleted during SaveChanges() call:
			try
			{
				//commit the changes (in this case deletion)
				em.SaveChanges();
				//if successful then return the user to the correct page
				string returnURL = Request.QueryString["redirectURL"] != null ? Request.QueryString["redirectURL"] : "";
				string tabSelectedIndex = Request.QueryString["tab"] != null ? Request.QueryString["tab"] : "";
				if(tabSelectedIndex != "" && returnURL != "")	
				{
					returnURL = returnURL + "&tab=" + tabSelectedIndex;
				}
					
				if(returnURL != "")
				{
					Response.Redirect(returnURL,false);
				}
				else
				{
					lblViewWarnings.Text = "Record Deleted";
				}
			} 
			catch (BnbProblemException pe)
			{
				//if not successful then display the error
				StringBuilder sb = new StringBuilder();
				sb.Append("There has been a problem deleting the Programme Funding<br/><br/>");
				foreach(BnbProblem prob in pe.Problems)
				{	
					sb.Append(prob.Message);
					sb.Append("<br/>");
				}
				lblViewWarnings.Text =sb.ToString();
			}
            //TPT PGMSMIFI-97
            Session.Add("ProgrammeFundingDeleted", true);
		}
		//-->

        //<!-- PGMSMIFI-97
        //Grant code Grid

        private void grantCodeGrid_Fill()
        {
            BnbProgrammeFunding objPF = bnb.GetPageDomainObject("BnbProgrammeFunding") as BnbProgrammeFunding;
            if (objPF != null)
            {
                string progfundid = Request.QueryString["BnbProgrammeFunding"] + "";
                string fundcode = "";
                if (progfundid != null && !progfundid.ToLower().Equals("new"))
                {
                    BnbProgrammeFunding pf = BnbProgrammeFunding.Retrieve(new System.Guid(progfundid));
                    fundcode = pf.FundCode.FundCode.Trim();
                }
                BnbTextCondition con = new BnbTextCondition("tblProgrammeFunding_ProgrammeFundingID", objPF.ID.ToString(), BnbTextCompareOptions.ExactMatch);
                BnbCriteria criteria = new BnbCriteria(con);
                BnbQuery query = new BnbQuery("vwGrantCodesForProgrammeFunding", criteria);
                BnbQueryDataSet ds = query.Execute();
                DataTable table = ds.Tables["Results"];
                Session.Add("grantCodeTable", table);
                grantCodeGrid_Bind(table);
            }
        }

        public void grantCodeGrid_Bind(DataTable table)
        {
            if (table != null)
            {
                grantCodeGrid.DataSource = table;
                grantCodeGrid.DataBind();
            }
        }

        void grantCodeGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow))
            {
                DropDownList drp = (DropDownList)e.Row.FindControl("drpGrantCode");
                if (drp != null)
                {
                    string fundcode = Session["FundCode"] as string;
                    BnbTextCondition con = new BnbTextCondition("ProjectCode", fundcode.Trim(), BnbTextCompareOptions.ExactMatch);
                    BnbCriteria criteria = new BnbCriteria(con);
                    BnbQuery query = new BnbQuery("vwBonoboGrantCodeList", criteria);
                    BnbQueryDataSet ds = query.Execute();
                    DataTable dt = ds.Tables["Results"];
                    if (dt.Rows.Count != 0)
                    {
                        drp.DataSource = dt;
                        drp.DataTextField = "Description";
                        drp.DataValueField = "Description";
                        drp.DataBind();
                        drp.SelectedValue = grantCodeGrid.DataKeys[e.Row.RowIndex].Values[0].ToString();
                    }
                }
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList newGrantCode = (DropDownList)e.Row.FindControl("drpGrantCodeNew");
                if (newGrantCode != null)
                {
                    string fundcode = Session["FundCode"] as string;
                    BnbTextCondition con = new BnbTextCondition("ProjectCode", fundcode.Trim(), BnbTextCompareOptions.ExactMatch);
                    BnbCriteria criteria = new BnbCriteria(con);
                    BnbQuery query = new BnbQuery("vwBonoboGrantCodeList", criteria);
                    BnbQueryDataSet ds = query.Execute();
                    DataTable dt = ds.Tables["Results"];
                    if (dt.Rows.Count != 0)
                    {
                        newGrantCode.DataSource = dt;
                        newGrantCode.DataTextField = "Description";
                        newGrantCode.DataValueField = "Description";
                        newGrantCode.DataBind();
                    }
                }
            }


        }

        void grantCodeGrid_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grantCodeGrid.EditIndex = e.NewEditIndex;
            DataTable table = Session["grantCodeTable"] as DataTable;
            grantCodeGrid_Bind(table);
            //throw new Exception("The method or operation is not implemented.");
        }

        void grantCodeGrid_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grantCodeGrid.EditIndex = -1;
            DataTable table = Session["grantCodeTable"] as DataTable;
            grantCodeGrid_Bind(table);
            //throw new Exception("The method or operation is not implemented.");
        }
        void grantCodeGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = (GridViewRow)grantCodeGrid.Rows[e.RowIndex];
            DropDownList drdlist = (DropDownList)(grantCodeGrid.Rows[e.RowIndex].Cells[1].FindControl("drpGrantCode"));
            string selectedvalue = ((DropDownList)grantCodeGrid.Rows[e.RowIndex].FindControl("drpGrantCode")).SelectedItem.Text; //drdlist.SelectedValue.ToString();
            DataTable table = Session["grantCodeTable"] as DataTable;
            int errorlog = 0;
            foreach (DataRow dr in table.Rows)
            {
                if ((dr["lnkGrantCodeList_GrantCode"].ToString()) == selectedvalue)
                {
                    lblErrorMessage.Visible = true;
                    lblErrorMessage.Text = "Grant Code : " + selectedvalue + " already exists";
                    errorlog += 1;
                    break;
                }
            }
            if (errorlog <= 0)
            {

                table.Rows[e.RowIndex]["lnkGrantCodeList_GrantCode"] = selectedvalue;
                string gclID = table.Rows[e.RowIndex]["lnkGrantCodeList_GrantCodeListID"].ToString();
                updateGrantCodesForProgrammeFunding(gclID, selectedvalue);
            }
            grantCodeGrid.EditIndex = -1;
            grantCodeGrid.DataSource = table;
            grantCodeGrid.DataBind();

            //throw new Exception("The method or operation is not implemented.");
        }

        private void updateGrantCodesForProgrammeFunding(string gclID, string grantCode)
        {
            BnbGrantCodeList pf = BnbGrantCodeList.GeneralRetrieve(new Guid(gclID), typeof(BnbGrantCodeList)) as BnbGrantCodeList;
            BnbEditManager em = new BnbEditManager();
            pf.RegisterForEdit(em);
            pf.GrantCode = grantCode;
            try
            {
                em.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        void grantCodeGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Insert")
            {
                BnbProgrammeFunding pf = bnb.GetPageDomainObject("BnbProgrammeFunding") as BnbProgrammeFunding;
                if (pf != null)
                {
                    DropDownList newGrantCode = (DropDownList)grantCodeGrid.FooterRow.FindControl("drpGrantCodeNew");
                    string newgrantCode = ((DropDownList)grantCodeGrid.FooterRow.Controls[0].FindControl("drpGrantCodeNew")).SelectedItem.Text;
                    DataTable table = Session["grantCodeTable"] as DataTable;
                    int errorlog = 0;
                    foreach (DataRow row in table.Rows)
                    {
                        if ((row["lnkGrantCodeList_GrantCode"].ToString()) == newgrantCode)
                        {
                            lblErrorMessage.Visible = true;
                            lblErrorMessage.Text = "Grant Code : " + newgrantCode + " already exists";
                            errorlog += 1;
                            break;
                        }
                    }
                    if (errorlog <= 0)
                    {
                        table.Rows.Add(Guid.NewGuid(), newGrantCode.SelectedItem, pf.ID);
                        saveGrantCodesForProgrammeFunding();
                        grantCodeGrid.ShowFooter = false;
                    }
                    grantCodeGrid.DataSource = table;
                    grantCodeGrid.DataBind();

                }
            }

            if (e.CommandName == "CancelInsert")
            {
                DataTable table = Session["grantCodeTable"] as DataTable;
                grantCodeGrid.DataSource = table;
                grantCodeGrid.ShowFooter = false;
                grantCodeGrid.DataBind();
            }
            //throw new Exception("The method or operation is not implemented.");
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            //drpProjectObjectiveNew1.Visible = true;
            grantCodeGrid.ShowFooter = true;
            DataTable table = Session["grantCodeTable"] as DataTable;
            grantCodeGrid_Bind(table);
        }

        private void saveGrantCodesForProgrammeFunding()
        {
            //Insert and update a Project Objective
            DataTable obj = Session["grantCodeTable"] as DataTable;
            foreach (DataRow row in obj.Rows)
            {
                string grantCode = row["lnkGrantCodeList_GrantCode"].ToString();
                Guid programmeFundingID = new Guid(row["tblProgrammeFunding_ProgrammeFundingID"].ToString());
                SetGrantCodeForProgrammeFunding(grantCode, programmeFundingID);
            }
        }

        private void SetGrantCodeForProgrammeFunding(string grantCode, Guid programmeFundingID)
        {
            BnbProgrammeFunding pf = BnbProgrammeFunding.GeneralRetrieve(programmeFundingID, typeof(BnbProgrammeFunding)) as BnbProgrammeFunding;


            bool alreadyExists = false;
            foreach (BnbGrantCodeList grantCodeList in pf.GrantCodeList)
            {
                if (grantCodeList.GrantCode == grantCode)
                {
                    alreadyExists = true;
                }
            }
            if (!alreadyExists)
            {

                BnbGrantCodeList gcl = new BnbGrantCodeList(true);
                BnbEditManager em = new BnbEditManager();
                pf.RegisterForEdit(em);
                gcl.RegisterForEdit(em);
                gcl.GrantCode = grantCode.ToString().Trim();
                gcl.ProgrammeFundingID = programmeFundingID;
                pf.GrantCodeList.Add(gcl);
                try
                {
                    em.SaveChanges();
                }
                catch (BnbProblemException pe)
                {
                    new BnbProblemException("Unfortunately, the Grant Code could not be created");
                }
            }

        }

        // TPT PGMSMIFI -97 -->
	}
}
