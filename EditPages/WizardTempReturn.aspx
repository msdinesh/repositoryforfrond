<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>

<%@ Page Language="c#" Codebehind="WizardTempReturn.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.EditPages.WizardTempReturn" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>WizardTempReturn</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>

    <script type="text/javascript">
		
		function durationToDate()
		{
			//get values from text boxes & dropdowns
			var thedateboxStartDate = document.getElementById("dateboxStartDate");
			var thedateboxEndDate = document.getElementById("dateboxEndDate");
			var theDropDownDuration = document.getElementById("dropDownDuration");
			var strStartDate = thedateboxStartDate.value;
			var duration = theDropDownDuration.value;
			
			//variables for date segments			
			var day;
			var month;
			var year;
			var firstSlash;
			var secondSlash;
			
			// get date segments
			firstSlash = strStartDate.indexOf('/');
			secondSlash = strStartDate.lastIndexOf('/');
			day = strStartDate.substring(0,firstSlash);
			month = strStartDate.substring(firstSlash+1,secondSlash);
			year = strStartDate.substring(secondSlash+1,strStartDate.length);
			
			// create date from segments
			var dateOfStart = new Date(day + month + year);
			if (!isNaN(Date.parse(dateOfStart)))
			{
				var dateOfEnd = new Date(dateOfStart.getTime()+ duration*24*60*60*1000);
			}
			else
			{
				thedateboxEndDate.value = "dd/MMM/yyyy";
			}
							
			//var realMonth = (dateOfEnd.getMonth()) + 1;
			var realMonth = getMonthName(dateOfEnd.getMonth());
			
			//var realMonth = get monthName(dateOfEnd.getMonth());
			var endDateString = (dateOfEnd.getDate() + "/" + realMonth + "/" + dateOfEnd.getFullYear());	
			//alert(dateOfEnd.getate()+ "/" + realMonth + "/" + dateOfEnd.getFullYear());	
			//alert(dateOfEnd.getDate());
			
			//var temp = monthName(4);
			//alert("alert here");
			// set values on text box
			thedateboxEndDate.value = endDateString;
			
			
       	}
       	function getMonthName(monthNum)
       	{
       	switch (monthNum)
       	{
       	case 0:
       	return ("Jan");
       	break;
       	case 1:
       	return ("Feb");
       	break;
       	case 2:
       	return ("Mar");
       	break;
       	case 3:
       	return ("Apr");
       	break;
       	case 4:
       	return ("May");
       	break;
       	case 5:
       	return ("Jun");
       	break;
       	case 6:
       	return ("Jul");
       	break;
       	case 7:
       	return ("Aug");
       	break;
       	case 8:
       	return ("Sep");
       	break;
       	case 9:
       	return ("Oct");
       	break;
       	case 10:
       	return ("Nov");
       	break;
       	case 11:
       	return ("Dec");
       	break;
       	default:
       	return ("");
       	}
       	}

       	
   	
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">

        <div class="wizardheader" ms_positioning="FlowLayout">
            <h3><IMG class="icon24" src="../Images/wizard24silver.gif">
                Temp Return Wizard</h3>
        </div>
        <p>
            <asp:Panel ID="panelZero" runat="server" Height="100px" CssClass="wizardpanel">
        
        <p>
            <em>This wizard will help you to add&nbsp;new&nbsp;Temporary Returns to a Placement.</em>
            <p>
                The Temp Return will be for the volunteer shown below:</p>
            <table class="wizardtable" id="Table1">
                <tr>
                    <td class="label">
                        Volunteer Details</td>
                    <td>
                        <asp:Label ID="labelVolunteerDetails" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="label">
                        Current Placement</td>
                    <td>
                        <asp:Label ID="labelCurrentPlacement" runat="server"></asp:Label></td>
                </tr>
            </table>
            
                <asp:Label ID="labelPanelZeroContinue" runat="server" Visible="False">Press Next to continue.</asp:Label>
           
                <asp:Label ID="labelPanelZeroFeedback" runat="server" CssClass="feedback"></asp:Label>
            </asp:Panel>
            <asp:Panel ID="panelOne" runat="server" CssClass="wizardpanel">
                <table class="wizardtable" id="Table2">
                    <tr>
                        <td class="label">
                            Volunteer Details</td>
                        <td>
                            <asp:Label ID="labelVolunteerDetails2" runat="server"></asp:Label></td>
                        <tr>
                            <td class="label">
                                Current Placement</td>
                            <td>
                                <asp:Label ID="labelCurrentPlacement2" runat="server"></asp:Label></td>
                        </tr>
                </table>
                <p>
                    <strong>Please&nbsp;state the reason and dates of the Temporary Return:</strong></p>
                <table class="wizardtable" id="Table4">
                    <tr>
                        <td class="label">
                            Temp Return Reason</td>
                        <td>
                            <asp:DropDownList ID="dropDownServiceReason" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td class="label">
                            Start Date</td>
                        <td>
                            <bnbdatacontrol:BnbDateBox ID="dateboxStartDate" runat="server" CssClass="datebox"
                                Height="20px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB" Width="90px"></bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 56px">
                            Duration or End Date</td>
                        <td style="height: 56px" valign="top">
                            <p>
                                <asp:DropDownList ID="dropDownDuration" runat="server">
                                </asp:DropDownList><br>
                                <bnbdatacontrol:BnbDateBox ID="dateboxEndDate" runat="server" CssClass="datebox"
                                    Height="20px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB" Width="90px"></bnbdatacontrol:BnbDateBox>
                            </p>
                        </td>
                    </tr>
                </table>
                <p>
                    <asp:Label ID="labelPanelOneFeedback" runat="server" CssClass="feedback"></asp:Label></p>
            </asp:Panel>
            <asp:Panel ID="panelTwo" runat="server" CssClass="wizardpanel">
                <table class="wizardtable" id="Table3">
                    <tr>
                        <td class="label">
                            Volunteer Details</td>
                        <td>
                            <asp:Label ID="labelVolunteerDetails3" runat="server"></asp:Label></td>
                        <tr>
                            <td class="label">
                                Current Placement</td>
                            <td>
                                <asp:Label ID="labelCurrentPlacement3" runat="server"></asp:Label></td>
                        </tr>
                </table>
                <p>
                    <strong>Confirm the details and click Finish to save.</strong></p>
                <p>
                    <table class="wizardtable" id="Table5">
                        <tr>
                        </tr>
                        <tr>
                            <td class="label">
                                Temp Return Reason</td>
                            <td>
                                <asp:Label ID="labelReturnReason" runat="server"></asp:Label></td>
                            <tr>
                                <td class="label">
                                    Start Date</td>
                                <td>
                                    <asp:Label ID="labelStartDate" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="label">
                                    End Date</td>
                                <td>
                                    <asp:Label ID="labelEndDate" runat="server"></asp:Label></td>
                            </tr>
                    </table>
                </p>
                <p>
                    <asp:Label ID="labelPanelTwoFeedback" runat="server" CssClass="feedback"></asp:Label></p>
            </asp:Panel>
            <uc1:wizardbuttons id="wizardButtons" runat="server">
            </uc1:wizardbuttons>           
            </div>
    </form>
    <p>
        <asp:Label ID="labelHiddenSelectedReturnReason" runat="server" Visible="False"></asp:Label><asp:Label
            ID="labelHiddenSelectedStartDate" runat="server" Visible="False"></asp:Label><asp:Label
                ID="labelHiddenSelectedEndDate" runat="server" Visible="False"></asp:Label><asp:Label
                    ID="labelHiddenSelectedReturnReasonID" runat="server" Visible="False"></asp:Label></p>
</body>
</html>
