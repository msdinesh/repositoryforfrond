<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbpagecontrols" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="cc1" Namespace="BonoboWebControls.ServicesControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="RVTrackingPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.RVTrackingPage" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>RVTracking Page</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/batchprocess24silver.gif"></uc1:title>
				<bnbpagecontrols:bnbdatabuttons id="BnbDataButtons1" runat="server" NewWidth="55px" EditAllWidth="55px" SaveWidth="55px"
					UndoWidth="55px" HeightAllButtons="25px" NewText="New" EditAllText="Edit All" SaveText="Save" UndoText="Undo"
					CssClass="databuttons"></bnbpagecontrols:bnbdatabuttons>
				<bnbpagecontrols:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox" SuppressRecordSavedMessage="false"
					PopupMessage="false" Width="208px"></bnbpagecontrols:bnbmessagebox>
				<bnbpagecontrols:bnbsectionheading id="Bnbsectionheading7" runat="server" CssClass="sectionheading" Width="100%" Height="20px"
					SectionHeading="Record Details" Collapsed="false"></bnbpagecontrols:bnbsectionheading>
				<TABLE class="fieldtable" id="Table5" width="100%" border="0">
					<TR>
						<TD class="label" width="85">Record Type</TD>
						<TD width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl13" runat="server" CssClass="lookupcontrol" LookupTableName="lkuRVRecordType"
								DataMember="BnbRVTracking.RVRecordTypeID" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="136px"
								Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
						<TD class="label" width="108">Record Status</TD>
						<TD width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl14" runat="server" CssClass="lookupcontrol" LookupTableName="lkuRVRecordStatus"
								DataMember="BnbRVTracking.RVRecordStatusID" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="140px"
								Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
				</TABLE>
				<P>&nbsp;&nbsp;&nbsp;&nbsp;
					<bnbgenericcontrols:BnbHyperlink id="BnbHyperlink3" runat="server" Width="208px"></bnbgenericcontrols:BnbHyperlink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
					&nbsp;&nbsp; &nbsp;&nbsp;
					<bnbgenericcontrols:BnbHyperlink id="BnbHyperlink2" runat="server"></bnbgenericcontrols:BnbHyperlink>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<bnbgenericcontrols:BnbHyperlink id="BnbHyperlink1" runat="server" NavigateUrl="RVConversionUtil.aspx" Target="_self">RV Conversion</bnbgenericcontrols:BnbHyperlink>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<bnbgenericcontrols:BnbButton id="BnbButton1" runat="server" Width="248px" Height="28px" Text="Run Conversion for this record"
						Visible="False" onclick="BnbButton1_Click"></bnbgenericcontrols:BnbButton>
				</P>
				<bnbpagecontrols:bnbsectionheading id="BnbSectionHeading1" runat="server" CssClass="sectionheading" Width="100%" Height="20px"
						SectionHeading="Vol Details" Collapsed="false"></bnbpagecontrols:bnbsectionheading>
				
					<TABLE class="fieldtable" id="Table1" width="100%" border="0">
						<TR>
							<TD class="label" width="120">VolRef</TD>
							<TD width="25%"><bnbdatacontrol:bnbtextbox id="BnbTextBox19" runat="server" CssClass="textbox" DataMember="BnbRVTracking.VolReferenceNo"
									Width="175px" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Label"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" width="120">Forename</TD>
							<TD width="25%"><bnbdatacontrol:bnbtextbox id="BnbTextBox1" runat="server" CssClass="textbox" DataMember="BnbRVTracking.Forename"
									Width="150px" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" width="120">Title</TD>
							<TD width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" LookupTableName="lkuTitle"
									DataMember="BnbRVTracking.Title" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="104px" Height="22px"
									DataValueField="Description"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>
						<TR>
							<TD class="label" width="120">Surname Prefix</TD>
							<TD width="25%">
								<bnbdatacontrol:BnbLookupControl id="BnbLookupControl15" runat="server" CssClass="lookupcontrol" Height="22px" Width="102px"
									DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" DataMember="BnbRVTracking.SurnamePrefix"
									LookupTableName="lkuSurNamePrefix" DataValueField="Description"></bnbdatacontrol:BnbLookupControl></TD>
							<TD class="label" width="120">
								Surname</TD>
							<TD width="25%"><bnbdatacontrol:bnbtextbox id="BnbTextBox2" runat="server" CssClass="textbox" DataMember="BnbRVTracking.Surname"
									Width="168px" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" width="120">Previous Surname</TD>
							<TD><bnbdatacontrol:bnbtextbox id="BnbTextBox3" runat="server" CssClass="textbox" DataMember="BnbRVTracking.PreviousSurname"
									Width="152px" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" width="120">Sex</TD>
							<TD width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl2" runat="server" CssClass="lookupcontrol" LookupTableName="lkuGender"
									DataMember="BnbRVTracking.Gender" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="101px"
									Height="22px" DataValueField="Abbreviation"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" width="120">Ethnicity</TD>
							<TD width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl12" runat="server" CssClass="lookupcontrol" LookupTableName="lkuEthnicity"
									DataMember="BnbRVTracking.EthnicityID" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="151px"
									Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" width="120">Other</TD>
							<TD width="25%"><bnbdatacontrol:bnbtextbox id="BnbTextBox5" runat="server" CssClass="textbox" DataMember="BnbRVTracking.EthnicityOther"
									Width="168px" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" width="120">DOB</TD>
							<TD width="25%"><bnbdatacontrol:bnbdatebox id="BnbDateBox1" runat="server" CssClass="datebox" DataMember="BnbRVTracking.DateOfBirth"
									Width="90px" Height="20px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
							<TD class="label" width="120">DOD</TD>
							<TD width="25%"><bnbdatacontrol:bnbdatebox id="BnbDateBox2" runat="server" CssClass="datebox" DataMember="BnbRVTracking.DateOfDeath"
									Width="90px" Height="20px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
							<TD class="label" width="120">Status RV/</TD>
							<TD width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl7" runat="server" CssClass="lookupcontrol" LookupTableName="lkustatus"
									DataMember="BnbRVTracking.StatusID" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="170px"
									Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>
                        <tr>
                            <td class="label" width="120">
                            </td>
                            <td width="25%">
                            </td>
                            <td class="label" width="120">
                                Identity Card No</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox20" runat="server" ControlType="Text" CssClass="textbox"
                                    DataMember="BnbRVTracking.IdentityCardNo" Height="20px" MultiLine="false" Rows="1"
                                    StringLength="0" Width="250px" />
                            </td>
                            <td class="label" width="120">
                                Raisers Edge ID</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox21" runat="server" ControlType="Text" CssClass="textbox"
                                    DataMember="BnbRVTracking.RaisersEdgeID" Height="20px" MultiLine="false" Rows="1"
                                    StringLength="0" Width="250px" />
                            </td>
                        </tr>
                        <tr>
                            <td width="120">
                            </td>
                            <td width="25%">
                            </td>
                            <td class="label" width="120">
                                Marital Status</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl3" runat="server" CssClass="lookupcontrol"
                                    DataMember="BnbRVTracking.MaritalStatusID" DataTextField="Description" Height="22px"
                                    ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuMaritalStatus" />
                            </td>
                            <td class="label" width="120">
                                Marital Status Date</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbDateBox ID="BnbDateBox6" runat="server" CssClass="datebox" DataMember="BnbRVTracking.MaritalStatusDate"
                                    DateCulture="en-GB" DateFormat="dd/MMM/yyyy" Height="20px" Width="90px" />
                            </td>
                        </tr>
					</TABLE>
				
				<bnbpagecontrols:bnbsectionheading id="BnbSectionHeading6" runat="server" CssClass="sectionheading" Width="100%" Height="20px"
						SectionHeading="Nationality" Collapsed="false"></bnbpagecontrols:bnbsectionheading>
				<bnbdatagrid:bnbdatagridfordomainobjectlist id="BnbDataGridForDomainObjectList1" runat="server" CssClass="datagrid" DataMember="BnbRVTracking.Nationalities"
						Width="680px" DataProperties="NationalityID[BnbLookupControl:lkuNationality], NationalityOrder[BnbTextBox]" DataGridType="Editable"
						DataGridForDomainObjectListType="Editable" DeleteButtonVisible="false" EditButtonVisible="true" AddButtonText="Add" EditButtonText="Edit"
						AddButtonVisible="true" VisibleOnNewParentRecord="false" SortBy="ID" DESIGNTIMEDRAGDROP="1872"></bnbdatagrid:bnbdatagridfordomainobjectlist>
				<bnbpagecontrols:bnbsectionheading id="BnbSectionHeading2" runat="server" CssClass="sectionheading" Width="100%" Height="20px"
						SectionHeading="Address" Collapsed="false"></bnbpagecontrols:bnbsectionheading>
				
					<TABLE class="fieldtable" id="Table2" width="872" border="0" style="HEIGHT: 164px">
						<TR>
							<TD class="label" width="120">Address Country</TD>
							<TD width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl4" runat="server" CssClass="lookupcontrol" LookupTableName="lkuCountry"
									DataMember="BnbRVTracking.AddressCountryID" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="153px"
									Height="18px"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" style="width: 162px">PostCode</TD>
							<TD width="25%"><bnbdatacontrol:bnbtextbox id="BnbTextBox11" runat="server" CssClass="textbox" DataMember="BnbRVTracking.Postcode"
									Width="136px" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
							<TD style="WIDTH: 159px"></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD class="label" width="120">Contact Address</TD>
							<TD width="25%"><bnbdatacontrol:bnbtextbox id="BnbTextBox6" runat="server" CssClass="textbox" DataMember="BnbRVTracking.AddressLine1"
									Width="148px" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" style="width: 162px">Home Tel</TD>
							<TD width="25%"><bnbdatacontrol:bnbtextbox id="BnbTextBox12" runat="server" CssClass="textbox" DataMember="BnbRVTracking.Telephone"
									Width="140px" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
							<TD style="WIDTH: 159px"></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD style="WIDTH: 157px"><bnbdatacontrol:bnbtextbox id="BnbTextBox7" runat="server" CssClass="textbox" DataMember="BnbRVTracking.AddressLine2"
									Width="148px" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" style="width: 162px">Work Tel</TD>
							<TD width="25%"><bnbdatacontrol:bnbtextbox id="BnbTextBox13" runat="server" CssClass="textbox" DataMember="BnbRVTracking.WorkTelephone"
									Width="140px" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" width="159" style="WIDTH: 159px">Block Mail</TD>
							<TD><bnbdatacontrol:bnbcheckbox id="BnbCheckBox1" runat="server" CssClass="checkbox" DataMember="BnbRVTracking.MailBlock"
									Width="68px"></bnbdatacontrol:bnbcheckbox></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD width="25%"><bnbdatacontrol:bnbtextbox id="BnbTextBox8" runat="server" CssClass="textbox" DataMember="BnbRVTracking.AddressLine3"
									Width="148px" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" style="width: 162px">Email</TD>
							<TD style="WIDTH: 130px"><bnbdatacontrol:bnbtextbox id="BnbTextBox14" runat="server" CssClass="textbox" DataMember="BnbRVTracking.Email"
									Width="140px" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
							<TD style="WIDTH: 159px"></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 15px"></TD>
							<TD width="25%" style="HEIGHT: 15px"><bnbdatacontrol:bnbtextbox id="BnbTextBox9" runat="server" CssClass="textbox" DataMember="BnbRVTracking.AddressLine4"
									Width="148px" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" style="HEIGHT: 15px; width: 162px;">Mobile</TD>
							<TD width="25%" style="HEIGHT: 15px"><bnbdatacontrol:bnbtextbox id="BnbTextBox15" runat="server" CssClass="textbox" DataMember="BnbRVTracking.Mobile"
									Width="140px" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" width="120">Contact Via</TD>
							<TD style="HEIGHT: 15px">
								<bnbdatacontrol:BnbLookupControl id="BnbLookupControl9" runat="server" CssClass="lookupcontrol" Height="22px" Width="117px"
									DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" DataMember="BnbRVTracking.ContactUserID"
									LookupTableName="vlkuBonoboUser "></bnbdatacontrol:BnbLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" width="120">County</TD>
							<TD width="25%"><bnbdatacontrol:bnbtextbox id="BnbTextBox10" runat="server" CssClass="textbox" DataMember="BnbRVTracking.County"
									Width="148px" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" style="width: 162px">Fax No</TD>
							<TD width="25%"><bnbdatacontrol:bnbtextbox id="BnbTextBox16" runat="server" CssClass="textbox" DataMember="BnbRVTracking.Fax"
									Width="140px" Height="20px" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
							<TD style="WIDTH: 159px"></TD>
							<TD></TD>
						</TR>
                        <tr>
                            <td width="120">
                            </td>
                            <td width="25%">
                            </td>
                            <td class="label" style="width: 162px">
                                Alternate Email</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox18" runat="server" ControlType="Text" CssClass="textbox"
                                    DataMember="BnbRVTracking.AlternateEmail" Height="20px" MultiLine="false" Rows="1"
                                    StringLength="0" Width="140px" />
                            </td>
                            <td style="width: 159px">
                            </td>
                            <td>
                            </td>
                        </tr>
					</TABLE>
				
				<bnbpagecontrols:bnbsectionheading id="BnbSectionHeading3" runat="server" CssClass="sectionheading" Width="100%" Height="20px"
						SectionHeading="Address Distribution" Collapsed="false"></bnbpagecontrols:bnbsectionheading>
				<bnbdatagrid:bnbdatagridfordomainobjectlist id="BnbDataGridForDomainObjectList2" runat="server" CssClass="datagrid" DataMember="BnbRVTracking.AddressDistributions"
						Width="704px" DataProperties="AddressDistributionID[BnbLookupControl:vlkuIndvAddrDist], Comments [BnbTextBox]" DataGridType="Editable"
						DataGridForDomainObjectListType="Editable" DeleteButtonVisible="false" EditButtonVisible="true" AddButtonText="Add" EditButtonText="Edit"
						AddButtonVisible="true" VisibleOnNewParentRecord="false" SortBy="ID"></bnbdatagrid:bnbdatagridfordomainobjectlist>
				<bnbpagecontrols:bnbsectionheading id="BnbSectionHeading4" runat="server" CssClass="sectionheading" Width="100%" Height="20px"
						SectionHeading="Placement" Collapsed="false"></bnbpagecontrols:bnbsectionheading>
				
					<TABLE class="fieldtable" id="Table3" width="100%" border="0">
						<TR>
							<TD class="label" width="120">Country</TD>
							<TD width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl6" runat="server" CssClass="lookupcontrol" LookupTableName="lkuCountry"
									DataMember="BnbRVTracking.CountryID" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="192px"
									Height="16px"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" width="120">Employer</TD>
							<TD colspan="3" width="513" style="WIDTH: 513px">
								<bnbdatacontrol:BnbSearchIDComposite id="BnbSearchIDComposite1" runat="server" Width="362px" DataMember="BnbRVTracking.EmployerID"
									BonoboViewName="vwBonoboFindEmployerSmall" BonoboViewFieldToBeBound="tblEmployer_EmployerID" BonoboViewFieldToLookUp="tblEmployer_EmployerDescription"></bnbdatacontrol:BnbSearchIDComposite></TD>
						</TR>
						<TR>
							<TD class="label" width="120">Skill</TD>
							<TD width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl8" runat="server" CssClass="lookupcontrol" LookupTableName="vlkuBonoboSkillWithGroupAbbrev"
									DataMember="BnbRVTracking.SkillID" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="192px"
									Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" width="120">SOS</TD>
							<TD width="513" style="WIDTH: 513px"><bnbdatacontrol:bnbdatebox id="BnbDateBox3" runat="server" CssClass="datebox" DataMember="BnbRVTracking.SOSDate"
									Width="90px" Height="20px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
							<TD class="label" width="120">EOS</TD>
							<TD width="25%"><bnbdatacontrol:bnbdatebox id="BnbDateBox4" runat="server" CssClass="datebox" DataMember="BnbRVTracking.EOSDate"
									Width="90px" Height="20px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
						</TR>
                        <tr>
                            <td class="label" width="120">
                                Recruitment Base (optional)</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl16" runat="server" CssClass="lookupcontrol"
                                    DataMember="BnbRVTracking.RecruitmentBaseID" DataTextField="Description" Height="22px"
                                    ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuRecruitmentBase" />
                            </td>
                            <td class="label" width="120">
                                VSO Type</td>
                            <td style="width: 513px" width="513">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl17" runat="server" CssClass="lookupcontrol"
                                    DataMember="BnbRVTracking.VSOTypeID" DataTextField="Description" Height="22px"
                                    ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboVSOTypeWithGroup" />
                            </td>
                            <td width="120">
                            </td>
                            <td width="25%">
                            </td>
                        </tr>
					</TABLE>
				
				<bnbpagecontrols:bnbsectionheading id="BnbSectionHeading5" runat="server" CssClass="sectionheading" Width="100%" Height="20px"
						SectionHeading="RV Details" Collapsed="false"></bnbpagecontrols:bnbsectionheading>
				
					<TABLE class="fieldtable" id="Table4" width="100%" border="0">
						<TR>
							<TD class="label" width="72">OCP&nbsp;Group/Type</TD>
							<TD width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl10" runat="server" CssClass="lookupcontrol" LookupTableName="vlkuBonoboOccupationTypeWithGroupAbbrev"
									DataMember="BnbRVTracking.OccupationTypeID" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="288px"
									Height="30px"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" width="62">Role</TD>
							<TD width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl11" runat="server" CssClass="lookupcontrol" LookupTableName="lkuoccupationrole"
									DataMember="BnbRVTracking.OccupationRoleID" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="248px"
									Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>
						<TR>
							<TD class="label" width="62">Comments</TD>
							<TD width="25%"><bnbdatacontrol:bnbtextbox id="BnbTextBox17" runat="server" CssClass="textbox" DataMember="BnbRVTracking.RVComments"
									Width="366px" Height="62px" MultiLine="True" Rows="4" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" width="62">Validation Messages</TD>
							<TD width="25%">
								<bnbdatacontrol:BnbTextBox id="BnbTextBox4" runat="server" CssClass="textbox" Height="56px" Width="284px" DataMember="BnbRVTracking.ErrorDetails"
									ControlType="Text" StringLength="0" Rows="3" MultiLine="True"></bnbdatacontrol:BnbTextBox></TD>
						</TR>
						<TR>
							<TD class="label" width="62">CreatedOn</TD>
							<TD width="25%"><bnbdatacontrol:bnbdatebox id="BnbDateBox5" runat="server" CssClass="datebox" DataMember="BnbRVTracking.UpdatedOn"
									Width="90px" Height="20px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
							<TD class="label" width="62">Created By</TD>
							<TD width="25%">
								<!-- disabled binding, this should be a lookupcontrol --><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl5" runat="server" CssClass="lookupcontrol" LookupTableName="vlkuBonoboUser"
									DataMember="BnbRVTracking.UpdatedBy" LookupControlType="ReadOnlyLabel" ListBoxRows="4" DataTextField="Description" Width="162px" Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>
					</TABLE>

			</div>
		</form>
	</body>
</HTML>
