<%@ Page Language="c#" Codebehind="ProgrammePage.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.EditPages.ProgrammePage" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>ProgrammePage</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet" />
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/bnbprogramme24silver.gif">
            </uc1:Title>
  <bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" NewWidth="55px" EditAllWidth="55px"
					SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px" NewText="New" EditAllText="Edit All" SaveText="Save"
					UndoText="Undo"></bnbpagecontrol:bnbdatabuttons>
          
                <bnbpagecontrol:BnbMessageBox ID="BnbMessageBox1" runat="server" CssClass="messagebox">
                </bnbpagecontrol:BnbMessageBox>
                            <asp:Label ID="lblViewWarnings" runat="server" CssClass="viewwarning" Visible="False"></asp:Label>
            <bnbpagecontrol:BnbTabControl ID="BnbTabControl1" runat="server"></bnbpagecontrol:BnbTabControl>
                        <asp:Panel ID="MainTab" runat="server" CssClass="tcTab" BnbTabName="Main">
                <table class="fieldtable" id="Table1" width="100%" border="0">
                    <tr>
                        <td class="label" width="40%">
                            Programme Reference</td>
                        <td>
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox1" runat="server" CssClass="textbox" Width="250px"
                                Height="20px" DataMember="BnbProgramme.ProgrammeRef" MultiLine="false" Rows="1"
                                StringLength="0"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="40%">
                            Programme Status</td>
                        <td>
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl1" runat="server" CssClass="lookupcontrol"
                                Width="250px" Height="22px" DataMember="BnbProgramme.ProgrammeStatusID" LookupTableName="lkuProgrammeStatus"
                                LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="40%">
                            Programme Office</td>
                        <td>
                            <bnbdatacontrol:BnbLookupControl ID="lookupProgrammeOffice" runat="server" CssClass="lookupcontrol"
                                Width="250px" Height="22px" DataMember="BnbProgramme.ProgrammeOfficeID" LookupTableName="lkuProgrammeOffice"
                                LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" AutoPostBack="True">
                            </bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="40%">
                            Programme Name</td>
                        <td>
                            <bnbdatacontrol:BnbLookupControl ID="lookupProgrammeArea" runat="server" CssClass="lookupcontrol"
                                Width="250px" Height="22px" DataMember="BnbProgramme.ProgrammeAreaID" LookupTableName="vlkuBonoboProgrammeAreaUnallocated"
                                LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" ParentControlID="lookupProgrammeOffice"
                                ColumnRelatedToParent="ProgrammeOfficeID"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="40%">
                            VSO Goals</td>
                        <td>
                            <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList1"
                                runat="server" CssClass="datagrid" Width="346px" DataMember="BnbProgramme.ProgrammeObjectives"
                                AddButtonText="Add" AddButtonVisible="True" DataProperties="MonitorObjectiveID [BnbLookupControl:lkuMonitorObjective]"
                                DataGridType="Editable" DeleteButtonVisible="True" SetLeadButtonVisible="True"
                                EditButtonVisible="true" NewButtonText="New Programme" EditButtonText="Edit"
                                NewHyperlinkEnabled="True"></bnbdatagrid:BnbDataGridForDomainObjectList>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="40%">
                            VSO Themes</td>
                        <td>
                            <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList2"
                                runat="server" CssClass="datagrid" Width="339px" DataMember="BnbProgramme.ProgrammeThemes"
                                AddButtonText="Add" AddButtonVisible="True" DataProperties="ThemeID [BnbLookupControl:lkuTheme]"
                                DataGridType="Editable" DeleteButtonVisible="True" SetLeadButtonVisible="false"
                                EditButtonVisible="true" NewButtonText="New" EditButtonText="Edit" NewHyperlinkEnabled="true">
                            </bnbdatagrid:BnbDataGridForDomainObjectList>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 12px" width="40%">
                            Programme Manager</td>
                        <td style="height: 12px">
                            <p>
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl4" runat="server" CssClass="lookupcontrol"
                                    Width="250px" Height="22px" DataMember="BnbProgramme.ProgrammeManagerID" LookupTableName="vlkuBonoboUser"
                                    LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:BnbLookupControl>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="40%">
                            PAP Summary Link</td>
                        <td>
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox3" runat="server" CssClass="textbox" Width="346px"
                                Height="20px" DataMember="BnbProgramme.PAPSummaryLink" MultiLine="false" Rows="1"
                                StringLength="0" DataType="String" ControlType="Text" HyperLink="True"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="40%">
                            Programme Description</td>
                        <td>
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox2" runat="server" CssClass="textbox" Width="250px"
                                Height="20px" DataMember="BnbProgramme.ProgrammeDescription" MultiLine="True"
                                Rows="3" StringLength="0" HyperLink="False"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    
                    <tr>
                   <td class="label" width="40%">
                           VPA2</td>
                        <td>
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl25" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbProgramme.VPA2ID" LookupTableName="vlkuBonoboUser"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                </table>
        
            <p>
                To filter Programme Notes, Requests and visible Placements, enter dates here and
                press 'Filter'</p>
            <p>
                From&nbsp;
                <asp:TextBox ID="txtFilterStart" runat="server"></asp:TextBox>&nbsp;To&nbsp;
                <asp:TextBox ID="txtFilterEnd" runat="server"></asp:TextBox>&nbsp;
                <asp:Button ID="cmdFilter" runat="server" Text="Filter" OnClick="cmdFilter_Click"></asp:Button>&nbsp;
                <asp:Label ID="lblFilterMessage" runat="server" ForeColor="Red"></asp:Label></p>
           
				</asp:Panel>
           
	<asp:panel id="ProgrammeNotesTab" runat="server" CssClass="tcTab" BnbTabName="Programme Notes">
      
      
    <p>
        <bnbdatagrid:BnbDataGridForView ID="dataGridProgrammeComments" runat="server" CssClass="datagrid"
            Width="100%" ColumnNames="tblProgrammeComments_CommentDate, tblProgrammeComments_Comment"
            NewLinkText="New" NewLinkVisible="true" ViewLinksVisible="true" QueryStringKey="BnbProgramme"
            FieldName="tblProgrammeComments_ProgrammeID" DomainObject="BnbProgrammeComments"
            GuidKey="tblProgrammeComments_ProgrammeCommentsID" RedirectPage="ProgrammeComments.aspx"
            ViewName="vwBonoboProgrammeComments"></bnbdatagrid:BnbDataGridForView>
    </p>

	</asp:panel> 
				
	<asp:panel id="RequestsTab" runat="server" CssClass="tcTab" BnbTabName="Requests">
    <p>
        <bnbdatagrid:BnbDataGridForView ID="dataGridRequests" runat="server" CssClass="datagrid"
            Width="100%" NewLinkText="New Request" NewLinkVisible="true" ViewLinksVisible="true"
            QueryStringKey="BnbProgramme" FieldName="tblRequest_ProgrammeID" DomainObject="BnbRequest"
            GuidKey="tblRequest_RequestID" RedirectPage="RequestPage.aspx" ViewName="vwBonoboProgrammeRequests"
            OrderBy="tblRequest_VolStartDate desc, tblRequest_RequestDescription"></bnbdatagrid:BnbDataGridForView>
    </p>
    
	</asp:panel>
	<asp:panel id="PlacementsTab" runat="server" CssClass="tcTab" BnbTabName="Placements">
     <p>
        <bnbdatagrid:BnbDataGridForView ID="dataGridPositions" runat="server" CssClass="datagrid"
            Width="100%" NewLinkText="New Placement" NewLinkVisible="False" ViewLinksVisible="true"
            QueryStringKey="BnbProgramme" FieldName="tblProgramme_ProgrammeID" DomainObject="BnbPosition"
            GuidKey="tblPosition_PositionID" RedirectPage="PlacementPage.aspx" ViewName="vwBonoboProgrammePositions">
        </bnbdatagrid:BnbDataGridForView> </p>
</asp:panel>
      </div>
    </form>
</body>
</html>
