using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using System.Text;
using BonoboEngine.Query;
using BonoboEngine;
using BonoboDomainObjects;
using System.Collections.Specialized;

namespace Frond
{
    /// <summary>
    /// Class to Find geographical locations of Individual(s) / Employer(s) for a country
    /// Algorithm:
    /// 1. Search Volunteer(s) or Employer(s) found in the country order by 'Name' field (Ascending)
    /// 2. Select first n number of records from the search results and dump it in to a temp datatable. (n - page size configured in web.config file)
    /// 3. Sort the temp Datatable by Latitude, Longitude, Name.
    /// 4. Form  markers (build javascript)in the map (unique locations, Identical locations) for the records exists in the datatable sequentially.
    ///    Note: One marker should be shown for identical location but clicking that marker should popup all the address in that location.
    /// 5. While adding the markers. collect the mapping details to another temp Datatable.
    /// 6. Register the javascript for adding markers to map
    /// 6. Sort the later Datatable by Name
    /// 7. Display the deatils in the list
    /// </summary>
    public partial class FindGeographical : System.Web.UI.Page
    {

        #region " Private Declarations "

        // Following are used to form the name link
        private const string linkStartTag = "<a href=";
        private const string linkEndTag = "</a> <br/>";
        private string midTagPrefix = ConfigurationManager.AppSettings["NavIndividual"].ToString();
        private string markerImgPath = ConfigurationManager.AppSettings["GMarkerImagePath"].ToString();
        private string markerImgType = ConfigurationManager.AppSettings["GMarkerImageType"].ToString();
        private string linkMidTag = string.Empty;

        private string[] resultTemplateColumns = { "Marker", "Details", "Name" };

        private string viewNameForSearch = "vwFindIndividualGMap";
        private string displayName = "Volunteer(s)";

        private StringBuilder linkBuilder = null;
        private StringBuilder markerBuilder = null;
        private bool rowsExist = false;
        private BnbQueryDataSet resultSet = null;

        private DataTable displayList = null; // Exact list of search results
        private DataTable displayListCopied = null; // Required list for pagination

        private BnbColumnList colList = new BnbColumnList("Name");

        float prevLat = 0.0f;
        float prevLng = 0.0f;
        float curLat = 0.0f;
        float curLng = 0.0f;
        private string details = string.Empty; //For getting concatenated address
        DataTable dtResults = null;  //DataTable containing final result set for display
        private string nameLink = string.Empty;
        //Following are for pagination
        private int resultCount = 0;
        private int noOfPages = 0;
        private int pageSize = 0;
        private int rowStartIdx = 0;
        private int rowEndIdx = 0;
        private int currPage = 0;

        #endregion

        #region " Page_Load "
        /// <summary>
        /// Page Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Initialize();
        }

        #endregion

        #region " Initialize "
        /// <summary>
        /// Initilazation of page
        /// </summary>
        private void Initialize()
        {
            BnbEngine.SessionManager.ClearObjectCache();
            BnbWorkareaManager.SetPageStyleOfUser(this);

            if (!this.IsPostBack)
            {
                titleBar.TitleText = "Find Geographical";
                BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
                BonoboWebControls.BnbWebFormManager.LogAction(titleBar.TitleText, null, this);
                //script for showing the "Searching" screen
                btnFind.Attributes.Add("onclick", "javascript:showLoadingMessage('Searching...');");
                FillCountriesCombo();

            }
            // initialize script: Assign PageName, operation to the javascript
            string initScript = "pageName ='FindGeographical'; operation ='';";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LoadGeo", initScript, true);

        }

        #endregion

        #region " FormMarkers "
        /// <summary>
        /// This function is used to form markers to show in the map for the founded locations
        /// </summary>
        /// <param name="resultTable"></param>
        private void FormMarkers(DataTable resultTable)
        {
            int lastRowIdx = resultTable.Rows.Count - 1;
            // Loop through the rows and generate the javascript for displaying the markers over the Map.
            int identicalCount = 0;
            // Start marker Id
            string marker = "marker1";
            detailsTable.Rows.Clear();

            SetPreviousLocation(resultTable.Rows[0]);
            StartMarkerBuilder(resultTable.Rows[0]);

            for (int rowIdx = 1; rowIdx < resultTable.Rows.Count; rowIdx++)
            {
                SetCurrentLocation(resultTable.Rows[rowIdx]);
                // Check for appending the details for identical location
                if (curLat == prevLat && curLng == prevLng)
                {
                    identicalCount += 1;
                    //Append details for identical location
                    AppendLinkBuilder(resultTable.Rows[rowIdx - 1], string.Empty);
                }
                else
                {
                    // Create proper Id's '[(rowIdx) - identicalCount]' for the marker
                    marker = "marker" + ((rowIdx) - identicalCount).ToString();
                    FormMarkerScript(resultTable.Rows[rowIdx - 1], marker, prevLat, prevLng);
                    linkBuilder = new StringBuilder();
                }
                if (rowIdx < lastRowIdx)
                    SetPreviousLocation(resultTable.Rows[rowIdx]);
            }

            linkMidTag = midTagPrefix;
            // Create proper marker id '[(lastRowIdx + 1) - identicalCount]'and pass 
            // as a parameter along with the result Row
            EndMarkerBuilder(resultTable.Rows[lastRowIdx], (lastRowIdx + 1) - identicalCount);
            RegisterMarkerBuilder(marker);

        }

        #endregion

        #region " Find - Click Event "
        /// <summary>
        /// Search event: To find volunteer(s) or Employer(s) for the chosen country and display
        /// them in Map.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFind_Click(object sender, EventArgs e)
        {
            gErrorMessage.Text = string.Empty;
            DisplayPaginationControls(false);

            if (ddlCountry.SelectedValue.ToString() != string.Empty)
                // do Searching
                ProcessFind();
            else
                gErrorMessage.Text = "Please choose a country";
        }

        #endregion

        #region " ProcessFind "
        /// <summary>
        /// Core Find operation
        /// This Involves: Configure whether it's a Volunteer / Employer search
        ///                Generate and execute the Search query.
        ///                Generate markers to display over the Map.
        /// </summary>
        private void ProcessFind()
        {
            ConfigureSearchType();

            BnbQuery query = GetSearchQuery();
            try
            {
                resultSet = query.Execute();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                if (ex.Message.StartsWith("Timeout expired"))
                {
                    gErrorMessage.Text = "Your search was taking too long - Please try again. ";

                    // Log
                    if (this.LogFinds)
                        DoLogFind(displayName + " Geo", query, 0, true);
                }
                else
                    throw ex;

            }
            if (!(resultSet == null))
            {
                if (resultSet.Tables.Count > 0)
                {
                    displayList = resultSet.Tables["Results"];
                    resultCount = displayList.Rows.Count;

                    // Log
                    if (this.LogFinds)
                        DoLogFind(displayName + " Geo", query, resultCount, false);


                    if (resultCount > 0)
                    {
                        DisplayPaginationControls(true);
                        rowsExist = true;
                        ProcessResults();
                        PaginationStart();
                    }
                }
                if (!(rowsExist))
                    gErrorMessage.Text = "No " + displayName + " found in " + ddlCountry.SelectedItem.Text;
            }
        }

        #endregion

        #region " ConfigureSearchType "
        /// <summary>
        /// Configure Search attributes
        /// </summary>
        private void ConfigureSearchType()
        {
            if (rdoEmployer.Checked)
            {
                displayName = "Employer(s)";
                viewNameForSearch = "vwFindEmployerGMap";
                midTagPrefix = ConfigurationManager.AppSettings["NavEmployer"].ToString();
            }
        }

        #endregion

        #region " GetSearchQuery "
        /// <summary>
        /// Generate search query
        /// </summary>
        /// <returns></returns>
        private BnbQuery GetSearchQuery()
        {
            BnbTextCondition countryMatch = new BnbTextCondition("CountryID", ddlCountry.SelectedValue.ToString(),
                                    BnbTextCompareOptions.ExactMatch);
            BnbCriteria criteria = new BnbCriteria(countryMatch);

            // Check for Uer permission to view confidential address.
            if (!(BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_ViewConfidentialAddress)))
            {
                BnbTextCondition nonConfidential = new BnbTextCondition("Confidential", "false", BnbTextCompareOptions.ExactMatch);
                criteria.QueryElements.Add(nonConfidential);
            }

            return new BnbQuery(viewNameForSearch, criteria, colList);
        }
        #endregion

        #region " FillCountriesCombo "

        /// <summary>
        /// KeyPress event is not working in Standalonelookup control. So, used dropdownlist web control
        /// </summary>
        private void FillCountriesCombo()
        {
            // country lookup 
            DataTable countryTable = BnbEngine.LookupManager.GetLookup("lkuCountry");
            countryTable.DefaultView.RowFilter = "Exclude = 0";
            countryTable.DefaultView.Sort = "Description";
            ddlCountry.DataSource = countryTable.DefaultView;
            ddlCountry.DataTextField = "Description";
            ddlCountry.DataValueField = "GuidID";
            ddlCountry.DataBind();
            // create empty item - To set as default selected
            ListItem blankItem = new ListItem("", "");
            ddlCountry.Items.Insert(0, blankItem);

        }

        #endregion

        #region " FormMarkerScript "
        /// <summary>
        /// Generates Marker script for Identical locations
        /// </summary>
        /// <param name="resultRow"></param>
        /// <param name="marker"></param>
        /// <param name="latVal"></param>
        /// <param name="lngVal"></param>
        private void FormMarkerScript(DataRow resultRow, string marker, float latVal, float lngVal)
        {
            // Form the details
            AppendLinkBuilder(resultRow, marker);
            // Build javascript to add the markers to map.
            markerBuilder.Append("markerIcon = new GIcon();");
            markerBuilder.Append(@"markerIcon.image = """ + markerImgPath);
            markerBuilder.Append(marker + "." + markerImgType + @""";");
            markerBuilder.Append("markerIcon.iconAnchor = new GPoint(5, 34);");
            markerBuilder.Append("markerIcon.infoWindowAnchor = new GPoint(5, 2);");
            markerBuilder.Append("markerIcon.infoShadowAnchor = new GPoint(14, 25);");

            markerBuilder.Append(marker
                                 + " = new GMarker(new GLatLng("
                                 + latVal.ToString()
                                 + ","
                                 + lngVal.ToString()
                                 + "),{icon:markerIcon});");
            markerBuilder.Append("GEvent.addListener("
                                 + marker
                                 + ", 'click', function()");
            markerBuilder.Append("{");
            markerBuilder.Append(marker
                                + ".openInfoWindowHtml('"
                                + linkBuilder.ToString()
                                + "');");
            markerBuilder.Append("});");
            markerBuilder.Append("map.addOverlay(" + marker + ");");
        }

        #endregion

        #region " StartMarkerBuilder "
        /// <summary>
        /// Start generating Marker javascript
        /// </summary>
        /// <param name="resultRow"></param>
        private void StartMarkerBuilder(DataRow resultRow)
        {
            linkBuilder = new StringBuilder();
            markerBuilder = new StringBuilder();

            markerBuilder.Append("operation ='Search';");
            markerBuilder.Append("function DisplaySearch(){");
        }

        #endregion

        #region " RegisterMarkerBuilder "
        /// <summary>
        /// Register the generated marker script
        /// </summary>
        /// <param name="marker"></param>
        private void RegisterMarkerBuilder(string marker)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Buildmarker", markerBuilder.ToString(), true);
        }

        #endregion

        #region " SetPreviousLocation "

        private void SetPreviousLocation(DataRow resultRow)
        {
            prevLat = Convert.ToSingle(resultRow["Latitude"]);
            prevLng = Convert.ToSingle(resultRow["Longitude"]);
        }

        #endregion

        #region " SetCurrentLocation "

        private void SetCurrentLocation(DataRow resultRow)
        {
            curLat = Convert.ToSingle(resultRow["Latitude"]);
            curLng = Convert.ToSingle(resultRow["Longitude"]);

        }

        #endregion

        #region " AppendLinkBuilder "
        /// <summary>
        /// Append Individual / Employer name links for identical locations
        /// </summary>
        /// <param name="resultRow"></param>
        private void AppendLinkBuilder(DataRow resultRow, string marker)
        {
            // Get the address details
            details = FormDetails(resultRow);
            linkMidTag = midTagPrefix;
            linkMidTag += resultRow["ID"].ToString() + ">" + ReplaceCharacterEntities(resultRow["Name"].ToString());
            // Form the name link
            nameLink = linkStartTag + linkMidTag + linkEndTag;
            //Append the name, address Html texts to link builder
            linkBuilder.Append(nameLink);
            linkBuilder.Append(details);

            // Also add the details as a new row to the dtResults table
            DataRow drResults = dtResults.NewRow();
            drResults[resultTemplateColumns[0]] = marker;
            drResults[resultTemplateColumns[1]] = nameLink + details;
            drResults[resultTemplateColumns[2]] = ReplaceCharacterEntities(resultRow["Name"].ToString());
            dtResults.Rows.Add(drResults);

            // Fill proper marker id's for the rows that has identical locations
            if (!(marker == string.Empty))
            {
                if (dtResults.Rows.Count > 0)
                {
                    for (int rowIdx = dtResults.Rows.Count - 2; rowIdx >= 0; rowIdx--)
                    {
                        if (dtResults.Rows[rowIdx][resultTemplateColumns[0]].ToString() == string.Empty)
                            dtResults.Rows[rowIdx][resultTemplateColumns[0]] = marker;
                        else
                            return;
                    }
                }
            }
        }
        #endregion

        #region " DisplayResultList "
        /// <summary>
        /// Final display of search results
        /// </summary>
        private void DisplayResultList()
        {
            string marker = string.Empty;

            // Sort the results with Name
            dtResults.DefaultView.Sort = "Name ASC";
            DataTable tblNameSorted = dtResults.DefaultView.ToTable();
            dtResults = tblNameSorted;

            //Render the display list 
            for (int rowIdx = 0; rowIdx < dtResults.Rows.Count; rowIdx++)
            {
                marker = dtResults.Rows[rowIdx][resultTemplateColumns[0]].ToString();

                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell cellImg = new HtmlTableCell();
                HtmlTableCell cellDetails = new HtmlTableCell();

                string imgDetails = @"<img src=""" + markerImgPath + marker + "." + markerImgType 
                                    + @""" style=""cursor:hand"""; 
                cellImg.InnerHtml = imgDetails + @" onclick=""GEvent.trigger(" + marker + @", 'click');"" />";
                cellImg.VAlign = "top";
                cellDetails.VAlign = "top";
                cellDetails.InnerHtml = dtResults.Rows[rowIdx]["Details"].ToString();

                row.Cells.Add(cellImg);
                row.Cells.Add(cellDetails);
                detailsTable.Rows.Add(row);
            }
        }

        #endregion

        #region " EndMarkerBuilder "
        /// <summary>
        /// Generates final Marker javascript
        /// </summary>
        /// <param name="resultRow"></param>
        /// <param name="markerIdx"></param>
        /// <param name="marker"></param>
        private void EndMarkerBuilder(DataRow resultRow, int markerIdx)
        {
            string marker = "marker" + markerIdx.ToString();

            if (curLat == prevLat && curLng == prevLng)
                // for identical location
                FormMarkerScript(resultRow, marker, curLat, curLng);
            else
            { // For unique location
                if (curLat == 0.0f && curLng == 0.0f)
                    FormMarkerScript(resultRow, marker, prevLat, prevLng);
                else
                    FormMarkerScript(resultRow, marker, curLat, curLng);
            }
            markerBuilder.Append("}");
        }

        #endregion

        #region " LogFinds "
        /// <summary>
        /// returns true if 'LogFinds' is turned on in web.config
        /// </summary>
        private bool LogFinds
        {
            get
            {

                if (ConfigurationManager.AppSettings["LogFinds"] == null)
                    return false;
                bool checking = Boolean.Parse(ConfigurationManager.AppSettings["LogFinds"].ToString());
                return checking;
            }

        }

        #endregion

        #region " DoLogFind "
        /// <summary>
        /// Log the search details
        /// </summary>
        private void DoLogFind(string findName, BnbQuery findQuery, int resultCount, bool timedOut)
        {
            // get a description of the user criteria
            BnbQueryBuilder qb = new BnbQueryBuilder(findQuery);
            string criteriaDesc = findQuery.Criteria.AsUserDescription(qb);
            if (!timedOut)
            {
                BnbWebFormManager.LogAction(String.Format("Find - {0}", findName),
                    String.Format("Criteria: {0}  Rows Returned: {1}",
                    criteriaDesc,
                    resultCount), this);
            }
            else
            {
                BnbWebFormManager.LogAction(String.Format("Find - {0} (Timed Out)", findName),
                    String.Format("Criteria: {0}  Rows Returned: 0 (Timed Out)",
                    criteriaDesc,
                    resultCount), this);

            }
        }

        #endregion

        #region " FormDetails "
        /// <summary>
        /// Returns the filled-in lines of the address as an array of strings,
        /// </summary>
        private string FormDetails(DataRow row)
        {
            StringBuilder lines = new StringBuilder();
            string line1 = ReplaceCharacterEntities(row["AddressLine1"].ToString());
            string line2 = ReplaceCharacterEntities(row["AddressLine2"].ToString());
            string line3 = ReplaceCharacterEntities(row["AddressLine3"].ToString());
            string line4 = ReplaceCharacterEntities(row["AddressLine4"].ToString());
            string line5 = ReplaceCharacterEntities(row["county"].ToString());
            string line6 = ReplaceCharacterEntities(ddlCountry.SelectedItem.Text);
            string line7 = ReplaceCharacterEntities(row["PostCode"].ToString());

            if (BnbRuleUtils.HasValue(line1))
                lines.Append(line1 + "<br/>");
            if (BnbRuleUtils.HasValue(line2))
                lines.Append(line2 + "<br/>");
            if (BnbRuleUtils.HasValue(line3))
                lines.Append(line3 + "<br/>");
            if (BnbRuleUtils.HasValue(line4))
                lines.Append(line4 + "<br/>");
            if (BnbRuleUtils.HasValue(line5))
                lines.Append(line5 + "<br/>");
            lines.Append(line6 + "<br/>");
            if (BnbRuleUtils.HasValue(line7))
                lines.Append(line7 + "<br/>");

            return lines.ToString();
        }

        #endregion

        #region " FormResultTableTemplate "
        /// <summary>
        /// Table for storing final results - ready for display
        /// </summary>
        private void FormResultTableTemplate()
        {
            DataColumn dcResults;
            dtResults = new DataTable();
            dcResults = new DataColumn(resultTemplateColumns[0]);
            dtResults.Columns.Add(dcResults);
            dcResults = new DataColumn(resultTemplateColumns[1]);
            dtResults.Columns.Add(dcResults);
            dcResults = new DataColumn(resultTemplateColumns[2]);
            dtResults.Columns.Add(dcResults);
        }

        #endregion

        #region " ProcessResults "
        /// <summary>
        /// Process the searched results
        /// </summary>
        private void ProcessResults()
        {
            pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["GMapSearchPageSize"]);
            decimal totPage = Convert.ToDecimal(resultCount) / Convert.ToDecimal(pageSize);
            noOfPages = Convert.ToInt32(Math.Ceiling(totPage));

            ViewState["PageSize"] = pageSize;
            ViewState["NoOfPages"] = noOfPages;
            ViewState["DisplayList"] = displayList;
            ViewState["ResultCount"] = resultCount;
            currPage = 1;
            // Search results text - display
            tRow.Text = resultCount.ToString();              // Total tows
            sType.Text = displayName;                        // Search type
            countryName.Text = ddlCountry.SelectedItem.Text; // country name
        }

        #endregion

        #region " ProcessPagination "
        /// <summary>
        /// Starts processing the Pagination logic
        /// </summary>
        private void ProcessPagination()
        {
            displayList = ((DataTable)ViewState["DisplayList"]);
            pageSize = Convert.ToInt32(ViewState["PageSize"]);
            noOfPages = Convert.ToInt32(ViewState["NoOfPages"]);
            resultCount = Convert.ToInt32(ViewState["ResultCount"]);

            // Calculate Start row index and end row index for pagination
            rowStartIdx = (currPage * pageSize) - pageSize;
            if (currPage == noOfPages)
                rowEndIdx = resultCount - 1;
            else
                rowEndIdx = (currPage * pageSize) - 1;

            // Display the pagination text
            sRow.Text = Convert.ToString(rowStartIdx + 1);
            eRow.Text = Convert.ToString(rowEndIdx + 1);

            //Copy the rows of current page from the displaylist to form markers
            displayListCopied = displayList.Clone();
            for (int rowIdx = rowStartIdx; rowIdx <= rowEndIdx; rowIdx++)
            {
                displayListCopied.ImportRow(displayList.Rows[rowIdx]);
            }

            // Sort the results with Latitude, Longitude, Name
            displayListCopied.DefaultView.Sort = "Latitude, Longitude, Name ASC";
            DataTable tblLatLngSorted = displayListCopied.DefaultView.ToTable();
            displayListCopied = tblLatLngSorted;
            // Process the list for the current page
            ProcessCurrentPage();
        }

        #endregion

        #region " First_Click "
        /// <summary>
        /// Pagination - First event
        /// </summary>
        protected void First_Click(object sender, EventArgs e)
        {
            PaginationStart();
        }

        #endregion

        #region " PaginationStart "
        /// <summary>
        /// Pagination - Called for displaying first page
        /// </summary>
        private void PaginationStart()
        {
            currPage = 1;
            ViewState["CurrPage"] = currPage;
            lbtnFirst.Enabled = false;
            lbtnPrev.Enabled = false;

            if (noOfPages == 1)
            {
                lbtnNext.Enabled = false;
                lbtnLast.Enabled = false;
            }
            else
            {
                lbtnNext.Enabled = true;
                lbtnLast.Enabled = true;
            }

            ProcessPagination();
        }

        #endregion

        #region " ProcessCurrentPage "
        /// <summary>
        /// Processes the found results and displays the locations in map as well as in the list
        /// </summary>
        private void ProcessCurrentPage()
        {
            FormResultTableTemplate();
            FormMarkers(displayListCopied);
            DisplayResultList();
        }

        #endregion

        #region " Next_Click "
        /// <summary>
        /// Pagination - Next event
        /// </summary>
        protected void Next_Click(object sender, EventArgs e)
        {
            currPage = Convert.ToInt32(ViewState["CurrPage"]);
            currPage += 1;
            ViewState["CurrPage"] = currPage;

            lbtnFirst.Enabled = true;
            lbtnPrev.Enabled = true;

            if (currPage == Convert.ToInt32(ViewState["NoOfPages"]))
            {
                lbtnLast.Enabled = false;
                lbtnNext.Enabled = false;
            }
            else
            {
                lbtnLast.Enabled = true;
                lbtnNext.Enabled = true;
            }
            ProcessPagination();
        }

        #endregion

        #region " Prev_Click "
        /// <summary>
        /// Pagination - Prev event
        /// </summary>
        protected void Prev_Click(object sender, EventArgs e)
        {
            currPage = Convert.ToInt32(ViewState["CurrPage"]);
            currPage = currPage - 1;
            ViewState["CurrPage"] = currPage;

            lbtnLast.Enabled = true;
            lbtnNext.Enabled = true;

            if (currPage == 1)
            {
                lbtnFirst.Enabled = false;
                lbtnPrev.Enabled = false;
            }
            else
            {
                lbtnFirst.Enabled = true;
                lbtnPrev.Enabled = true;
            }
            ProcessPagination();
        }

        #endregion

        #region " Last_Click "
        /// <summary>
        /// Pagination - last event
        /// </summary>
        protected void Last_Click(object sender, EventArgs e)
        {
            currPage = Convert.ToInt32(ViewState["NoOfPages"]);
            ViewState["CurrPage"] = currPage;

            lbtnLast.Enabled = false;
            lbtnNext.Enabled = false;
            lbtnFirst.Enabled = true;
            lbtnPrev.Enabled = true;

            ProcessPagination();
        }

        #endregion

        #region " DisplayPaginationControls "
        /// <summary>
        /// Visibility of pagination controls
        /// </summary>
        private void DisplayPaginationControls(bool visible)
        {
            tdPagination.Visible = visible;
            tdResultText.Visible = visible;
        }

        #endregion

        #region " ReplaceCharacterEntities "
        /// <summary>
        /// Replaces HTML character entities
        /// </summary>
        /// <param name="textToReplace"></param>
        /// <returns></returns>
        private string ReplaceCharacterEntities(string textToReplace)
        {
            textToReplace = textToReplace.Replace("<", "&lt;");
            textToReplace = textToReplace.Replace(">", "&gt;");
            return textToReplace;
        }

        #endregion
    }
}
