using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;
using BonoboEngine.Query;
using BonoboEngine;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for PlacementPage.
    /// </summary>
    public partial class PlacementPage : System.Web.UI.Page
    {
        protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading2;
        protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading4;
        protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading5;
        protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading6;
        protected BonoboWebControls.DataControls.BnbLookupControl BnbLookupControl4;
        protected BonoboWebControls.DataControls.BnbDateBox BnbDateBox9;
        protected BonoboWebControls.PageControls.BnbDataButtons BnbDataButtons2;
        protected BonoboWebControls.DataGrids.BnbDataGridForView BnbDataGridForView1;
        protected BonoboWebControls.DataGrids.BnbDataGridForView BnbDataGridForView2;
        protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading1;
        protected System.Web.UI.WebControls.PlaceHolder titlePlaceHolder;
        protected UserControls.Title titleBar;
        protected UserControls.NavigationBar NavigationBar1;
        protected UserControls.NavigatePanel navPanel;

        private BnbWebFormManager bnb = null;
        private bool m_isRole = false;
        private bool m_isPos = false;
        private string m_PosID = "";
        private string m_RoleID = "";

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            m_PosID = Request.QueryString["BnbPosition"];
            m_RoleID = Request.QueryString["BnbRole"];
            m_isPos = (m_PosID != null && m_PosID != "new");
            m_isRole = (m_RoleID != null && m_RoleID != "new");

#if DEBUG
            if (Page.Request.QueryString.ToString() == "")
            {
                //BnbWebFormManager.ReloadPage("BnbPosition=102DF1AF-490C-461E-A554-117D3434DAAF");
                BnbWebFormManager.ReloadPage("BnbPosition=39189DC3-AAEC-11D4-908F-00508BACE998");
            }
            else
            {
                doLoadManager();
            }
#else
		//	bnb = new BnbWebFormManager(this, "BnbPosition");
            doLoadManager();
#endif

            //tweak logging settings
            bnb.PageNameOverride = "Placement";
            bnb.LogPageHistory = true;
            BnbWorkareaManager.SetPageStyleOfUser(this);


            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null)
                    titleBar.TitleText = bnb.PageTitleDescriptionCurrent;

                //  work out which tab to show based on where we came from
                if (this.Request.UrlReferrer != null)
                {
                    string referrer = this.Request.UrlReferrer.AbsolutePath;
                    if (referrer.EndsWith("PlacementWebTextPage.aspx"))
                        BnbTabControl1.SelectedTabID = WebTextTab.ID;
                    if (referrer.EndsWith("PrePostCheckPage.aspx"))
                        BnbTabControl1.SelectedTabID = PrePostChecklistTab.ID;
                }

                doInitialiseAdhocControls();
                doFundingWizardHyperLink();

            }
            AssignAppURL();
            NavigationBar1.RootDomainObject = "BnbPosition";

            // call shared code to show any view warnings
            Utilities.ShowViewWarnings(bnb, lblViewWarnings);

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        private void doFundingWizardHyperLink()
        {

            string posID = Request.QueryString["BnbPosition"];
            if (posID == null || posID.ToLower() == "new")
            {
                // new mode: hide hyperlinks
                hypNewFunding.Visible = false;
            }
            else
            {
                // put right guid in url
                hypNewFunding.Visible = true;
                hypNewFunding.NavigateUrl = String.Format("WizardNewFunding.aspx?BnbPosition={0}",
                    posID);
            }
        }

        private void doLoadMonitorHyperlink(BnbPosition pos)
        {
            // monitor link
            string url = "";
            string text = "";
            BonoboEngine.BnbDomainObjectList monitors = pos.Monitors;
            if (monitors.Count == 0)
            {
                //add a new monitor object
                url += "WizardNewMonitor.aspx?BnbPosition=" + pos.ID.ToString().ToUpper();
                text = "Add new Monitor and Evaluation";
            }
            else
            {
                //only expecting one Monitor record for Starfish
                if (monitors.Count == 1)
                {
                    //edit a monitor object; should only return one monitor object from the list
                    url += "MonitorEvaluationPage.aspx?BnbPosition=" + pos.ID.ToString().ToUpper() + "&BnbMonitor=" + ((BnbMonitor)monitors[0]).ID.ToString();
                    text = "Edit Monitor and Evaluation";
                }
                else
                {
                    //disable the hyperlink
                    text = "[More than one Monitor record was returned]";
                    url = "";
                }
            }
            hyperlinkMonitor.NavigateUrl = url;
            hyperlinkMonitor.Text = text;
        }

        private void doInitialiseAdhocControls()
        {
            BnbRole role = BnbRole.Retrieve(new System.Guid(m_RoleID));

            if (m_PosID == null || m_PosID.ToLower() == "new")
            {
                // new mode: hide hyperlinks
                hyperlinkRequest.Visible = false;
                hyperlinkMonitor.Visible = false;
            }
            else
            {
                BnbPosition pos = BnbPosition.Retrieve(new System.Guid(m_PosID));
                // render the correct Request Hyperlink onto the page
                if (pos.CurrentRequest == null)
                {
                    hyperlinkRequest.Text = "Link to Request";
                    hyperlinkRequest.NavigateUrl = String.Format("WizardRequestLink.aspx?BnbPosition={0}",
                        m_PosID);
                }
                else
                {
                    hyperlinkRequest.Text = "View Request";
                    hyperlinkRequest.NavigateUrl = String.Format("RequestPage.aspx?BnbProgramme={0}&BnbRequest={1}",
                        pos.CurrentRequest.ProgrammeID,
                        pos.CurrentRequest.ID);
                }
                doLoadMonitorHyperlink(pos);

                // financial year funding controls
                string yearstring = ((DropDownList)sfFinancialYearList.Controls[0]).SelectedValue;
                if (yearstring == null || yearstring == "") return;
                int yearid = Int32.Parse(yearstring);
                lblFundingTotalSelectedYear.Text = String.Format("{0:�#,##0.00}", pos.FundingTotalSelectedYear(yearid));
                lblFundingVolunteerSelectedYear.Text = String.Format("{0:�#,##0.00}", pos.FundingVolunteerSelectedYear(yearid));
                lblFundingPlacementSelectedYear.Text = String.Format("{0:�#,##0.00}", pos.FundingPlacementSelectedYear(yearid));

            }
        }

        private void doLoadManager()
        {
            if (m_isRole)
            {
                bnb = new BnbWebFormManager(this, "BnbPosition");
                return;
            }
            m_RoleID = ((BnbPosition)BnbPosition.Retrieve(new System.Guid(m_PosID))).RoleID.ToString();
            string qs = Request.QueryString.ToString() + "&BnbRole=" + m_RoleID.ToUpper();
            bnb = new BnbWebFormManager(qs);
        }

        //Adding a new placement
        protected void BnbDataButtons1_NewClick(object sender, System.EventArgs e)
        {
            doRedirectToNewPlacementWizard();

        }

        private void doRedirectToNewPlacementWizard()
        {
            string role = this.Request.QueryString["BnbRole"];
            string roleID = getRoleID(role);
            // need to pass roleID to New Placement wizard
            if (role != null && role.ToLower() != "new")
                Response.Redirect(String.Format("WizardNewPlacement.aspx?BnbRole={0}",
                    roleID));
        }

        private string getRoleID(string id)
        {
            BnbRole role = BnbRole.Retrieve(new System.Guid(id));
            //BnbApplication app = (BnbApplication)bnb.ObjectTree.DomainObjectDictionary["BnbApplication"];
            return role.ID.ToString();
        }

        private void AssignAppURL()
        {
            Guid appID = GetPlacedApplication();
            if (appID != Guid.Empty)
            {
                hlnkApplication.Enabled = true;
                hlnkApplication.NavigateUrl = String.Format("SFApplicationPage.aspx?BnbApplication={0}",
                        appID.ToString());
            }
            else
            {
                hlnkApplication.Enabled = false;
            }
        }

        private Guid GetPlacedApplication()
        {
            BnbPosition pos = BnbPosition.Retrieve(new System.Guid(m_PosID));
            Guid appID = Guid.Empty;
            if (pos != null)
            {
                if (pos.PlacedService != null)
                {
                    appID = pos.PlacedService.ApplicationID;
                }
            }

            return appID;
        }

    }
}
