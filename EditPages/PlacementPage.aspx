<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls"
    Assembly="BonoboWebControls" %>

<%@ Page Language="c#" Codebehind="PlacementPage.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.EditPages.PlacementPage" %>

<%@ Register Src="../UserControls/SFFinancialYearDropDownList.ascx" TagName="SFFinancialYearDropDownList"
    TagPrefix="uc2" %>

<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html>
<head>
    <title>PlacementPage</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/bnbposition24silver.gif">
            </uc1:Title>
            <uc1:NavigationBar ID="NavigationBar1" runat="server"></uc1:NavigationBar>
            <table width="100%">
                <tr>
                    <td>
                        <bnbpagecontrol:BnbDataButtons ID="Bnbdatabuttons3" runat="server" CssClass="databuttons"
                            NewButtonEnabled="True" NewWidth="55px" EditAllWidth="55px" SaveWidth="55px"
                            UndoWidth="55px" HeightAllButtons="25px" NewText="New" EditAllText="Edit All"
                            SaveText="Save" UndoText="Undo" DisableAllButtons="False" onnewclick="BnbDataButtons1_NewClick"></bnbpagecontrol:BnbDataButtons>
                    </td>
                   
                </tr>
            </table>
            <bnbpagecontrol:BnbMessageBox ID="Bnbmessagebox2" runat="server" CssClass="messagebox">
            </bnbpagecontrol:BnbMessageBox>
            <asp:Label ID="lblViewWarnings" runat="server" CssClass="viewwarning" Visible="False"></asp:Label>
            <bnbpagecontrol:BnbTabControl ID="BnbTabControl1" runat="server"></bnbpagecontrol:BnbTabControl>
            <asp:Panel ID="MainTab" runat="server" CssClass="tcTab" BnbTabName="Main">
                <table class="fieldtable" id="Table2" width="100%" border="0">
                    <tr>
                        <td class="label" width="25%">
                            Logged By</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="Bnblookupcontrol8" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" ControlType="ComboBox"
                                LookupControlType="ComboBox" LookupTableName="vlkuBonoboUser" DomainObject="BnbPosition"
                                DataProperty="CreatedBy" DataMember="BnbPosition.CreatedBy"></bnbdatacontrol:BnbLookupControl>
                        </td>
                        <td class="label" width="25%">
                            Logged On</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="Bnbdatebox14" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DomainObject="BnbPosition" DataProperty="CreatedOn" DataMember="BnbPosition.CreatedOn"
                                DateCulture="en-GB" DateFormat="dd/mm/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Placement Ref</td>
                        <td width="25%">
                            <p>
                                <bnbdatacontrol:BnbTextBox ID="Bnbtextbox9" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" ControlType="Text" DataMember="BnbPosition.FullPlacementReference"
                                    MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:BnbTextBox>
                            </p>
                        </td>
                        <td class="label" width="25%">
                            Job Title</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="Bnbtextbox10" runat="server" CssClass="textbox" Height="20px"
                                Width="220px" ControlType="Label" DataMember="BnbPosition.Role.RoleName" MultiLine="false"
                                Rows="1" StringLength="0" Enabled="False" DataType="String"></bnbdatacontrol:BnbTextBox>
                        </td>
                       </tr>
                    <tr>
                        <td class="label" width="25%">
                            Country</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="Bnblookupcontrol1" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
                                LookupTableName="lkuCountry" DataMember="BnbPosition.Role.Employer.CountryID"></bnbdatacontrol:BnbLookupControl>
                        </td>
                        <td class="label" width="25%">
                            Employer</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="Bnbtextbox11" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" ControlType="Label" DataMember="BnbPosition.Role.Employer.EmployerName"
                                MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Original Planned Start Date</td>
                        <td width="75%" colspan="3">
                            <bnbdatacontrol:BnbDateBox ID="Bnbdatebox13" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DomainObject="BnbPosition" DataProperty="OriginalStartDate" DataMember="BnbPosition.OriginalStartDate"
                                DateCulture="en-GB" DateFormat="dd/mm/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Planned Start Date</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox1" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DomainObject="BnbPosition" DataProperty="EarliestStartDate" DataMember="BnbPosition.EarliestStartDate"
                                DateCulture="en-GB" DateFormat="dd/mm/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            Duration in months</td>
                        <td width="25%">
                            <p>
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox8" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" ControlType="Text" DataMember="BnbPosition.Duration" MultiLine="false"
                                    Rows="1" StringLength="0" DataType="Int32"></bnbdatacontrol:BnbTextBox>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Replacement</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox1" runat="server" CssClass="checkbox"
                                DomainObject="BnbPosition" DataProperty="Replacement" DataMember="BnbPosition.Replacement">
                            </bnbdatacontrol:BnbCheckBox>
                        </td>
                        <td class="label" width="25%">
                            VSO Type</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl2" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" ControlType="ComboBox"
                                LookupControlType="ComboBox" LookupTableName="vlkuBonoboVSOTypeWithGroup" DomainObject="BnbPosition"
                                DataProperty="VSOTypeID" DataMember="BnbPosition.VSOTypeID"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Priority</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox1" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DomainObject="BnbPosition" DataProperty="Priority" DataMember="BnbPosition.Priority"
                                MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" width="25%">
                            Overseas Link</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox2" runat="server" CssClass="checkbox"
                                DomainObject="BnbPosition" DataProperty="OverseasLink" DataMember="BnbPosition.OverseasLink">
                            </bnbdatacontrol:BnbCheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 36px" width="25%">
                            Programme Area</td>
                        <td style="height: 36px" width="25%">
                            <p>
                                <bnbdatacontrol:BnbLookupControl ID="bnblckProgrammeArea" runat="server" CssClass="lookupcontrol"
                                    Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" ControlType="ComboBox"
                                    LookupControlType="ComboBox" LookupTableName="lkuProgrammeArea" DomainObject="BnbPosition"
                                    DataProperty="ProgrammeAreaID" DataMember="BnbPosition.ProgrammeAreaID" ParentControlID="bnbtxtProgrammeOfficeID"
                                    ColumnRelatedToParent="ProgrammeOfficeID"></bnbdatacontrol:BnbLookupControl>
                                <bnbdatacontrol:BnbTextBox ID="bnbtxtProgrammeOfficeID" runat="server" CssClass="textbox"
                                    Height="20px" Width="250px" ControlType="Hidden" DataMember="BnbPosition.Role.Employer.ProgrammeOfficeID"
                                    MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:BnbTextBox>
                            </p>
                        </td>
                        <td class="label" style="height: 36px" width="25%">
                            No of Links</td>
                        <td style="height: 36px" width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox7" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DomainObject="BnbPosition" DataProperty="LinkedCount" DataMember="BnbPosition.CurrentLinkCount"
                                MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Web Start Date</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="dtRecruitmentStartDate" runat="server" CssClass="datebox"
                                Height="20px" Width="90px" DomainObject="BnbPosition" DataProperty="RecruitmentStartDate"
                                DataMember="BnbPosition.RecruitmentStartDate" DateCulture="en-GB" DateFormat="dd/mm/yyyy">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            Web End Date</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="dtRecruitmentEndDate" runat="server" CssClass="datebox"
                                Height="20px" Width="90px" DomainObject="BnbPosition" DataProperty="RecruitmentEndDate"
                                DataMember="BnbPosition.RecruitmentEndDate" DateCulture="en-GB" DateFormat="dd/mm/yyyy">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Publish to Web</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbCheckBox ID="chkActiveWebRecruitment" runat="server" CssClass="checkbox"
                                DomainObject="BnbPosition" DataProperty="ActiveWebRecruitment" DataMember="BnbPosition.ActiveWebRecruitment">
                            </bnbdatacontrol:BnbCheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 22px" width="25%">
                            Volunteer Role</td>
                        <td style="height: 22px" width="25%" colspan="3" rowspan="1">
                            <p>
                                <br>
                                <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList1"
                                    runat="server" CssClass="datagrid" Width="200px" DataMember="BnbPosition.PositionVolunteerRoles"
                                    DataProperties="VolunteerRoleID [BnbLookupControl:lkuMonitorVolunteerRole]" DeleteButtonVisible="True"
                                    EditButtonVisible="true" AddButtonText="Add" EditButtonText="Edit" AddButtonVisible="true"
                                    DataGridType="Editable"></bnbdatagrid:BnbDataGridForDomainObjectList>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 25px" width="25%">
                            VSO Goal</td>
                        <td style="height: 25px" width="25%" colspan="3">
                            <p>
                                <br>
                                <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList2"
                                    runat="server" CssClass="datagrid" Width="200px" DataMember="BnbPosition.PositionObjectives"
                                    DataProperties="MonitorObjectiveID [BnbLookupControl:lkuMonitorObjective]" DeleteButtonVisible="True"
                                    EditButtonVisible="true" AddButtonText="Add" EditButtonText="Edit" AddButtonVisible="true"
                                    DataGridType="Editable" VisibleOnNewParentRecord="False"></bnbdatagrid:BnbDataGridForDomainObjectList>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            VSO Theme</td>
                        <td width="25%" colspan="3">
                            <br>
                            <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList3"
                                runat="server" CssClass="datagrid" Width="200px" DataMember="BnbPosition.PositionThemes"
                                DataProperties="ThemeID [BnbLookupControl:lkuTheme]" DeleteButtonVisible="True"
                                EditButtonVisible="true" AddButtonText="Add" EditButtonText="Edit" AddButtonVisible="true"
                                DataGridType="Editable"></bnbdatagrid:BnbDataGridForDomainObjectList>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Comments</td>
                        <td width="25%" >
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox2" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DomainObject="BnbPosition" DataProperty="Comments" DataMember="BnbPosition.Comments"
                                MultiLine="True" Rows="3" StringLength="0"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" width="25%">
                        Request Link</td>
                    <td width="25%">
                        <asp:HyperLink ID="hyperlinkRequest" runat="server"></asp:HyperLink></td>
                    </tr>
                    <tr>
			<td class="label" width="25%">
                            PS Sent</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox2" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbPosition.SummarySentDate" DateCulture="en-GB" DateFormat="dd/mm/yyyy">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            PS Sent Filename</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox3" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DataMember="BnbPosition.POSummaryFilename" MultiLine="false" Rows="1"
                                StringLength="0"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            PS Received</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox3" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbPosition.SummaryReceivedDate" DateCulture="en-GB"
                                DateFormat="dd/mm/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            PS Rec Filename</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox4" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DataMember="BnbPosition.SummaryFilename" MultiLine="false" Rows="1"
                                StringLength="0"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            PD Sent</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox4" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbPosition.DescriptionSentDate" DateCulture="en-GB"
                                DateFormat="dd/mm/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            PD Sent Filename</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox5" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DataMember="BnbPosition.PODescriptionFilename" MultiLine="false"
                                Rows="1" StringLength="0"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            PD Received</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox5" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbPosition.DescriptionReceivedDate" DateCulture="en-GB"
                                DateFormat="dd/mm/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            PD Rec Filename</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox6" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DataMember="BnbPosition.DescriptionFilename" MultiLine="false"
                                Rows="1" StringLength="0"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="width: 333px" >
                            Working Language</td>
                     <td colspan="3">
                             <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl4" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" ControlType="ComboBox"
                                LookupControlType="ComboBox" LookupTableName="vlkuWorkingLanguage" DomainObject="BnbPosition"
                                DataMember="BnbPosition.WorkingLanguageID"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                     <tr>
                <td><bnbdatacontrol:BnbDomainObjectHyperlink ID="BnbDomainObjectHyperlink1" runat="server"
                                    DataMember="BnbPosition.PlacementProgramme.DummyReference" ButtonText="[BnbDomainObjectHyperlink]"
                                    RedirectPage="#" GuidProperty="BnbPosition.ID"
                                    Target="_top"></bnbdatacontrol:BnbDomainObjectHyperlink></td>
                                    <td>
                                    <bnbdatacontrol:BnbDomainObjectHyperlink ID="BnbDomainObjectHyperlink2" runat="server"
                                    DataMember="BnbPosition.PlacedService.Application.DummyReference" ButtonText="[BnbDomainObjectHyperlink]"
                                    RedirectPage="#" GuidProperty="BnbPosition.ID"
                                    Target="_top"></bnbdatacontrol:BnbDomainObjectHyperlink></td>
                </tr>
                </table>
                <p>
                    <bnbpagecontrol:BnbSectionHeading ID="BnbSectionHeading3" runat="server" CssClass="sectionheading"
                        Height="20px" Width="100%" Collapsed="false" SectionHeading="Placement Status"></bnbpagecontrol:BnbSectionHeading>
                </p>
                <table class="fieldtable" id="Table3" width="100%" border="0">
                    <tr>
                        <td class="label" style="height: 27px" width="25%">
                            Programme Officer</td>
                        <td style="height: 27px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl7" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                LookupTableName="vlkuBonoboUser" DataMember="BnbPosition.ProgrammeOfficerID"></bnbdatacontrol:BnbLookupControl>
                        </td>
                        <td class="label" style="height: 27px" width="25%">
                            Date Tentative</td>
                        <td style="height: 27px" width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox6" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DomainObject="BnbPosition" DataProperty="TentativeDate" DataMember="BnbPosition.TentativeDate"
                                DateCulture="en-GB" DateFormat="dd/mm/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Tentative Reason</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl3" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" ControlType="ComboBox"
                                LookupControlType="ComboBox" LookupTableName="lkuTentativeReason" DomainObject="BnbPosition"
                                DataProperty="TentativeReasonID" DataMember="BnbPosition.TentativeReasonID"></bnbdatacontrol:BnbLookupControl>
                        </td>
                        <td class="label" width="25%">
                            Date Firm</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox7" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DomainObject="BnbPosition" DataProperty="FirmDate" DataMember="BnbPosition.FirmDate"
                                DateCulture="en-GB" DateFormat="dd/mm/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Firm/Tentative Status</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox12" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" ControlType="Label" DataMember="BnbPosition.TentativeFirmStatus"
                                MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:BnbTextBox>
                        </td>
                        <td class="label" width="25%">
                        </td>
                        <td width="25%">
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 18px" width="25%">
                            Fill Status</td>
                        <td style="height: 18px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl6" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                LookupTableName="lkuPositionFillStatus" DataMember="BnbPosition.FillStatus"></bnbdatacontrol:BnbLookupControl>
                        </td>
                        <td class="label" style="height: 18px" width="25%">
                        </td>
                        <td style="height: 18px" width="25%">
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 28px" width="25%">
                            Date Cancelled</td>
                        <td style="height: 28px" width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox8" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DomainObject="BnbPosition" DataProperty="CancelledDate" DataMember="BnbPosition.CancelledDate"
                                DateCulture="en-GB" DateFormat="dd/mm/yyyy"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" style="height: 28px" width="25%">
                            Cancel Reason</td>
                        <td style="height: 28px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl5" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                LookupTableName="lkuPositionCancelReason" DataMember="BnbPosition.CancelledReason">
                            </bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 28px" width="25%">
                            Posted</td>
                        <td style="height: 28px" width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox12" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbPosition.PostableDate" DateCulture="en-GB" DateFormat="dd/mm/yyyy">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" style="height: 28px" width="25%">
                        </td>
                        <td style="height: 28px" width="25%">
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Placement Start Date</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox10" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbPosition.StartDate" DateCulture="en-GB" DateFormat="dd/mm/yyyy">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            Placement End Date</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox11" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbPosition.EndDate" DateCulture="en-GB" DateFormat="dd/mm/yyyy">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                    
                     <tr>
                        <td class="label" width="25%">
                            VPA2</td>
                        <td style="height: 23px" width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl25" runat="server" CssClass="lookupcontrol"
                                Height="22px" DataMember="BnbPosition.PlacementProgramme.VPA2ID" LookupTableName="vlkuBonoboUser"
                                DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"></bnbdatacontrol:BnbLookupControl>
                        </td>
                         </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="FundingTab" runat="server" CssClass="tcTab" BnbTabName="Funding">
                <bnbpagecontrol:BnbSectionHeading ID="Bnbsectionheading7" runat="server" CssClass="sectionheading"
                    Height="20px" Width="100%" Collapsed="false" SectionHeading="Sponsorship"></bnbpagecontrol:BnbSectionHeading>
                <p>
                    <asp:HyperLink ID="hypNewFunding" runat="server">New Sponsorship</asp:HyperLink></p>
                <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView5" runat="server" CssClass="datagrid"
                    Width="100%" DomainObject="BnbFunding" FieldName="tblFunding_PositionID" QueryStringKey="BnbPosition"
                    RedirectPage="FundingPage.aspx" ViewLinksVisible="true" ViewName="vwBonoboApplicationFunding"
                    NewLinkVisible="False" NewLinkText="New Funding" GuidKey="tblFunding_FundingID"
                    DisplayNoResultMessageOnly="True" NoResultMessage="(No Sponsorship records for this Placement)">
                </bnbdatagrid:BnbDataGridForView>

                <p>
                    <table class="fieldtable" id="Table4" width="100%" border="0">
                        <tr>
                            <td class="label" style="height: 17px" width="25%">
                                Total for job</td>
                            <td style="height: 17px" width="25%">
                                <p>
                                    <bnbdatacontrol:BnbTextBox ID="BnbTextBox13" runat="server" CssClass="textbox" Height="20px"
                                        Width="250px" DataMember="BnbPosition.FundingPlacementAllYears" MultiLine="false"
                                        Rows="1" StringLength="0" FormatExpression="�#,#0.00"></bnbdatacontrol:BnbTextBox>
                                </p>
                            </td>
                            <td class="label" style="height: 17px" width="25%">
                                FY&nbsp;<uc2:SFFinancialYearDropDownList id="sfFinancialYearList" runat="server"></uc2:SFFinancialYearDropDownList>
                            or job&nbsp;</td>
                            <td style="height: 17px" width="25%">
                                <asp:Label ID="lblFundingPlacementSelectedYear" runat="server"></asp:Label></td>
                        </tr>
                         <tr>
                            <td class="label" width="25%">
                                for volunteer</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox17" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" DataMember="BnbPosition.FundingVolunteerAllYears" MultiLine="false"
                                    Rows="1" StringLength="0" FormatExpression="�#,#0.00"></bnbdatacontrol:BnbTextBox>
                            </td>
                            <td class="label" width="25%">
                                for volunteer</td>
                            <td width="25%">
                                <p>
                                    <asp:Label ID="lblFundingVolunteerSelectedYear" runat="server"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%">
                                Both</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox18" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" DataMember="BnbPosition.FundingTotalAllYears" MultiLine="false"
                                    Rows="1" StringLength="0" FormatExpression="�#,#0.00"></bnbdatacontrol:BnbTextBox>
                            </td>
                            <td class="label" width="25%">
                                Both</td>
                            <td width="25%">
                                <asp:Label ID="lblFundingTotalSelectedYear" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </p>
                <p>
                    <bnbpagecontrol:BnbSectionHeading ID="Bnbsectionheading8" runat="server" CssClass="sectionheading"
                        Height="20px" Width="100%" Collapsed="false" SectionHeading="Programme Funding">
                    </bnbpagecontrol:BnbSectionHeading>

                </p>
                <p>
            <bnbgenericcontrols:BnbHyperlink ID="BnbHyperlink1" runat="server" NavigateUrl="FundCodeList.aspx"
                Target="_top">View Fund Code List</bnbgenericcontrols:BnbHyperlink>
            <bnbgenericcontrols:BnbHyperlink ID="BnbHyperlink2" runat="server" NavigateUrl="FundChargeRateList.aspx"
                Target="_top">View Fund Charge List</bnbgenericcontrols:BnbHyperlink></p> 
                <p>
                    <bnbdatagrid:BnbDataGridForView ID="dgrProgrammeFunding" runat="server" CssClass="datagrid"
                        Width="100%" DomainObject="BnbProgrammeFunding" DataGridType="ViewAndDelete"
                        FieldName="tblProgrammeFunding_PositionID" QueryStringKey="BnbPosition" RedirectPage="ProgrammeFundingPage.aspx"
                        ViewLinksVisible="true" ViewName="vwBonoboProgrammeFunding" NewLinkVisible="true"
                        NewLinkText="New Programme Funding" GuidKey="tblProgrammeFunding_ProgrammeFundingID"
                        ReturnToHostedPage="True" DeleteLinksVisible="true" ColumnNames="lkuFundingType_Description, tblFundCode_FundCode,tblProgrammeFunding_FundingStartDate,tblProgrammeFunding_FundingEndDate"
                        DisplayNoResultMessageOnly="True" DisplayResultCount="false" ShowResultsOnNewButtonClick="false"
                        ViewLinksText="View" ReturnPageTabStripSelectedIndex="1" NoResultMessage="(No Programme Funding records for this Placement)">
                    </bnbdatagrid:BnbDataGridForView>
                </p>
            </asp:Panel>
            <asp:Panel ID="PrePostChecklistTab" runat="server" CssClass="tcTab" BnbTabName="Pre/Post Checklist">
              
                <p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView1" runat="server" CssClass="datagrid"
                        Width="100%" DomainObject="BnbPrePost" RedirectPage="PrePostCheckPage.aspx" FieldName="tblPosition_PositionID"
                        QueryStringKey="BnbPosition" ViewLinksVisible="True" ViewName="vwBonoboPositionPrePost"
                        NewLinkVisible="true" NewLinkText="New Pre/Post Check" GuidKey="tblPrePost_ArrivalID"
                        NewLinkReturnToParentPageDisabled="False"></bnbdatagrid:BnbDataGridForView>
                </p>
            </asp:Panel>

 	    <asp:Panel ID="MonitoringandEvaluationTab" runat="server" CssClass="tcTab" BnbTabName="Monitoring and Evaluation">
               
                <p>
                    <asp:HyperLink ID="hyperlinkMonitor" runat="server"></asp:HyperLink></p>
            </asp:Panel>
            <asp:Panel ID="PlacementHistoryTab" runat="server" CssClass="tcTab" BnbTabName="Placement History">
                
                <p>
                </p>
                <p>
                    <bnbdatagrid:BnbDataGridForView ID="Bnbdatagridforview3" runat="server" CssClass="datagrid"
                        Width="100%" DomainObject="BnbService" DataGridType="ViewOnly" FieldName="tblPosition_PositionID"
                        QueryStringKey="BnbPosition" RedirectPage="PlacementLinkPage.aspx" ViewLinksVisible="true"
                        ViewName="vwBonoboPlacementHistory" NewLinkVisible="False" GuidKey="tblServiceProgress_ServiceID"
                        ReturnToHostedPage="True" DeleteLinksVisible="False" ColumnNames="VolName,VolRef, VolStatus, lkuServiceStatus_Description, tblServiceProgress_StatusDate, tblServiceProgress_Rank,lkuRecruitmentBase_Description,lkuVSOType_Description, lkuServiceReason_Description,tblServiceProgress_Rank"
                        DisplayNoResultMessageOnly="True" DisplayResultCount="false" ShowResultsOnNewButtonClick="false"
                        ViewLinksText="View" ReturnPageTabStripSelectedIndex="1" NoResultMessage="(No Placement History records for this Placement)">
                    </bnbdatagrid:BnbDataGridForView>
                </p>
            </asp:Panel>

          <asp:Panel ID="ReportsandVisitsTab" runat="server" CssClass="tcTab" BnbTabName="Reports and Visits">
                            <p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView2" runat="server" CssClass="datagrid"
                        Width="100%" DomainObject="BnbReportVisit" RedirectPage="ReportVisitPage.aspx"
                        FieldName="tblPosition_PositionID" QueryStringKey="BnbPosition" ViewLinksVisible="true"
                        ViewName="vwBonoboPositionReportVisit" GuidKey="tblReportVisit_ReportVisitID"
                        NewLinkVisible="true" NewLinkText="New Report/Visit"></bnbdatagrid:BnbDataGridForView>
                </p>
            </asp:Panel>

            <asp:Panel ID="WebTextTab" runat="server" CssClass="tcTab" BnbTabName="Web Summary"
                Height="184px">

                <bnbdatagrid:BnbDataGridForView ID="BnbdatagridforviewForWebText" runat="server"
                    CssClass="datagrid" Width="100%" DomainObject="BnbPositionWebText" FieldName="lnkPositionWebText_PositionID"
                    QueryStringKey="BnbPosition" RedirectPage="PlacementWebTextPage.aspx" ViewLinksVisible="true"
                    ViewName="vwBonoboPlacementWebTexts" NewLinkVisible="true" NewLinkText="New Web Text"
                    GuidKey="lnkPositionWebText_PositionWebTextID" ReturnToHostedPage="True" ColumnNames="lnkPositionWebText_PlacementDescription,lnkPositionWebText_QualificationsRequired,lkuLanguage_Description"
                    DisplayNoResultMessageOnly="True" DisplayResultCount="false" ShowResultsOnNewButtonClick="false"
                    ViewLinksText="View" NoResultMessage="(No Placement Web Text for this Placement)">
                </bnbdatagrid:BnbDataGridForView>
            </asp:Panel>
            <asp:Panel ID="EOSTab" runat="server" CssClass="tcTab" BnbTabName="End Of Service">
                <table class="fieldtable" id="Table1" cellspacing="1" cellpadding="1">
                     <tr><td colspan = 4><asp:HyperLink ID="hlnkApplication" runat="server" Text="Click here to Edit EOS details"> </asp:HyperLink> </td></tr>
                </table>
            </asp:Panel>
            
            
        </div>
    </form>
</body>
</html>
