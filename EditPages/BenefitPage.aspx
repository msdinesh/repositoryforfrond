<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Page language="c#" Codebehind="BenefitPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.BenefitPage" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Benefit Page</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/bnbaccountheader24silver.gif"></uc1:title>&nbsp;
				<uc1:NavigationBar id="NavigationBar1" runat="server"></uc1:NavigationBar>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" UndoText="Undo" SaveText="Save"
					EditAllText="Edit All" NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px"
					NewWidth="55px" NewButtonEnabled="False"></bnbpagecontrol:bnbdatabuttons>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox2" runat="server" CssClass="messagebox" SuppressRecordSavedMessage="false"
					PopupMessage="false"></bnbpagecontrol:bnbmessagebox>
				<TABLE class="fieldtable" id="Table1" width="100%" border="0">
					<TR>
						<TD class="label" style="HEIGHT: 33px" width="25%">Benefit Type</TD>
						<TD style="HEIGHT: 33px" width="25%">
							<bnbdatacontrol:BnbLookupControl id="BnbcmbType" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
								DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuBenefitType" AutoPostBack="False"
								DataMember="BnbIndividualBenefit.BenefitTypeID"></bnbdatacontrol:BnbLookupControl></TD>
					<TR>
					<TR>
						<TD class="label" style="HEIGHT: 33px" width="25%">&nbsp;Benefit No</TD>
						<TD style="HEIGHT: 33px" width="25%">
							<bnbdatacontrol:BnbTextBox id="BnbTtNo" runat="server" CssClass="textbox" Width="250px" Height="20px" DataMember="BnbIndividualBenefit.BenefitNumber"
								ControlType="Text" StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox></TD>
					<TR>
					<TR>
						<TD class="label" style="HEIGHT: 33px" width="25%">Status</TD>
						<TD style="HEIGHT: 33px" width="25%">
							<bnbdatacontrol:BnbLookupControl id="BnbLkuStatus" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
								DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuBenefitStatus" DataMember="BnbIndividualBenefit.BenefitStatusId"></bnbdatacontrol:BnbLookupControl></TD>
					<TR>
					<TR>
						<TD class="label" style="HEIGHT: 33px" width="25%">Comments</TD>
						<TD style="HEIGHT: 33px" width="25%">
							<bnbdatacontrol:BnbTextBox id="BnbTxtReason" runat="server" CssClass="textbox" Width="250px" Height="20px"
								DataMember="BnbIndividualBenefit.BenefitReason" ControlType="Text" StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox></TD>
					<TR>
					</TR>
				</TABLE>
				<P><bnbpagecontrol:bnbdatabuttons id="BnbDataButtons2" runat="server" CssClass="databuttons" UndoText="Undo" SaveText="Save"
						EditAllText="Edit All" NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px"
						NewWidth="55px" NewButtonEnabled="False"></bnbpagecontrol:bnbdatabuttons>
				</P>
			</div>
		</form>
	</body>
</HTML>
