using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using BonoboWebControls;
using System.Text;



namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for Utilities.
	/// </summary>
	public class Utilities
	{
		public Utilities()
		{
			//
			// TODO: Add constructor logic here
			//
		}

        public const string ProjectHlink = "<div><span><a class=\"ProjectTitleUnderline\" href=\"ProjectPage.aspx?BnbProject=[PROJECTID]\">[TITLE]</a></span></div><div><span>[SUBTITLE]</span></div>";
        public const string ProjectSubHlink = "<div><span><a class=\"ProjectTitleUnderline\" href=\"ProjectPage.aspx?BnbProject=[PROJECTID]\">Project&nbsp;-&nbsp;[TITLE]</a></span></div><div><span class=\"ProjectSubTitle\">&nbsp;>>&nbsp;[SUBTITLE]</span></div>";
        public const string ProjectSubHlink2 = "<div><span><a class=\"ProjectTitleUnderline\" href=\"ProjectPage.aspx?BnbProject=[PROJECTID]\">Project&nbsp;-&nbsp;[TITLE]</a></span></div><div><span class=\"ProjectSubTitle\">&nbsp;>>&nbsp;[SUBTITLE]&nbsp;>>&nbsp;[SUBTITLE2]</span></div>";

		/// <summary>
		/// This method is used to fill the combos on the page as they are not databound with a lookup table/view.
		/// </summary>
		/// <param name="comboName"></param>
		/// <param name="tablename"></param>
		/// <param name="IDType"></param>
		/// <param name="Filter"></param>
		/// <param name="NotKnownID"></param>
		public void FillCombos(System.Web.UI.WebControls.DropDownList comboName, string tablename, string IDType, string Filter, string NotKnownID)
		{

			BnbLookupDataTable dtLookup = BnbEngine.LookupManager.GetLookup(tablename);
			DataView dtv = new DataView(dtLookup);
			dtv.RowFilter = Filter;
			dtv.Sort = "Description";
			comboName.DataSource = dtv;
			comboName.DataValueField = IDType;
			comboName.DataTextField = "Description";
			comboName.DataBind();
		
			ListItem blankItem = new ListItem("<Any>","");
			comboName.Items.Insert(0,blankItem);

			if (dtv.Count == 0 && NotKnownID.Length > 0)
			{
				ListItem NotKnownItem = new ListItem("Not Known",NotKnownID);
				comboName.Items.Insert(1,NotKnownItem);
			}
		}

		/// <summary>
		/// Checks all domain objects for view warnings, and displays them in the specified label
		/// (should move this into BnbWebFormManager and BnbMessageBox eventually)
		/// </summary>
		/// <param name="bnb"></param>
		/// <param name="warningLabel"></param>
		public static void ShowViewWarnings(BnbWebFormManager bnb, Label warningLabel)
		{
			BnbDomainObjectCollection currentObjects = bnb.ObjectTree.AllDomainObjects;
			BnbEditManager tempEM = new BnbEditManager();
			BnbWarningCollection warnings = tempEM.GetViewWarnings(currentObjects);
			StringBuilder sb = new StringBuilder();
			// wrap warnings in a div so they appear in the right place
			if (warnings.Count > 0)
			{
				sb.Append("<div>");
				foreach(BnbWarning warnLoop in warnings)
				{
					sb.Append(warnLoop.Message);
					sb.Append("<br/>");
				}
				sb.Append("</div>");
				
			}
			warningLabel.Text = sb.ToString();
			warningLabel.Visible = (warningLabel.Text != "");
		}

		/// <summary>
		/// populates address type drop down for Address tab
		/// (could be repalced by a BnbStandaloneLookupControl)
		/// </summary>
		/// <param name="atDrop"></param>
		public static void PopulateAddressTypeDropDown(DropDownList atDrop)
		{
			DataTable attable =  BnbEngine.LookupManager.GetLookup("vlkuBonoboAddressTypeWithGroup");
			attable.DefaultView.RowFilter = "Exclude = 0";
			attable.DefaultView.Sort = "Description";
			atDrop.DataSource = attable;
			atDrop.DataTextField = "Description";
			atDrop.DataValueField = "IntID";
			atDrop.DataBind();
			atDrop.Items.Insert(0,new ListItem("[select type]","-1"));
		}

		// a static method that can be called from the other pages in this app
		// could be replaced by using BnbStandaloneLookupcontrol
		public static void PopulateFinancialYearDropDown(DropDownList fyDrop)
		{
			DataTable fytable =  BnbEngine.LookupManager.GetLookup("lkuFinancialYear");
			fytable.DefaultView.RowFilter = "Exclude = 0";
			fytable.DefaultView.Sort = "DisplayOrder";
			fyDrop.DataSource = fytable;
			fyDrop.DataTextField = "Description";
			fyDrop.DataValueField = "IntID";
			fyDrop.DataBind();
			fyDrop.Items.Insert(0,new ListItem("","-1"));

			int currentFY = BnbRuleUtils.GetFinancialYearID(DateTime.Now);
			if (currentFY > 0)
			{
				ListItem defaultItem = fyDrop.Items.FindByValue(currentFY.ToString());
				if (defaultItem != null)
					defaultItem.Selected = true;
			}


		}


	}
}
