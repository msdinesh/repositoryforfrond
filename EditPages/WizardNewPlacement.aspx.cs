using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;


namespace Frond.EditPages
{
    public partial class WizardNewPlacement : System.Web.UI.Page
    {
        #region " Private declarations "
        BnbRole m_role = null;
        #endregion       

        #region " Page_Load "

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                initwizard();
                txtDurationinMonths.Text = "0";
                if (this.Request.UrlReferrer != null)
                    lblHiddenUrlReferrer.Text = this.Request.UrlReferrer.ToString();
                else
                    lblHiddenUrlReferrer.Text = "~/Menu.aspx";
            }

        }

        #endregion

        #region" initwizard "

        public void initwizard()
        {
            PopulateProgrammeArea();
            PopulateVsoGoal();
            PopulateVsoType();
        }

        #endregion

        #region " Cancel_Click "

        protected void buttonCancel_Click(object sender, EventArgs e)
        {
            this.ReturnToOriginalPage();

        }
        #endregion

        #region " Finish_Click "

        protected void buttonFinish_Click(object sender, EventArgs e)
        {
            int programmeArea;
            int vsoGoal;
            int vsoType;
            float duration;
            if (!(cmbProgrammeArea.SelectedValue.Trim() == ""))
            {
                programmeArea = Convert.ToInt32(cmbProgrammeArea.SelectedValue);
            }
            else {
                programmeArea = BnbDomainObject.NullInt;
            }
            if (!(cmbVsoGoal.SelectedValue == ""))
            {
                vsoGoal = Convert.ToInt32(cmbVsoGoal.SelectedValue);
            }
            else
            {
                vsoGoal = BnbDomainObject.NullInt;
            }
            if (!(cmbVsoType.SelectedValue == ""))
            {
                vsoType = Convert.ToInt32(cmbVsoType.SelectedValue);
            }
            else
            {
                vsoType = BnbDomainObject.NullInt;
            }
            if (txtDurationinMonths.Text.Trim() == string.Empty)
            {
                duration = Convert.ToSingle("0");
            }
            else {
                duration = Convert.ToSingle(txtDurationinMonths.Text);
            }           
            DateTime plannedDate = dtpPlannedDate.Date;        
            BnbEditManager em = new BnbEditManager();
            BnbPosition newPos = new BnbPosition(true);
            newPos.RegisterForEdit(em);
            Guid roleID = new Guid(Request.QueryString["BnbRole"]);
            m_role = BnbRole.Retrieve(roleID);
            newPos.Role = m_role;
            newPos.VSOTypeID = vsoType;
            newPos.EarliestStartDate = plannedDate;  
            newPos.ProgrammeAreaID = programmeArea;
            newPos.Duration = duration;
            //TPT Amended:VAF-1063
            newPos.TentativeDate = DateTime.Now;
            newPos.TentativeReasonID = BnbConst.TentativeReason_NoReasonSupplied;
            BnbPositionObjective newPosObjective = new BnbPositionObjective(true);
            newPosObjective.RegisterForEdit(em);
            newPosObjective.MonitorObjectiveID = vsoGoal;
            newPosObjective.Order = 1;
            newPos.PositionObjectives.Add(newPosObjective);

            try
            {
                em.SaveChanges();
                Response.Redirect(String.Format("PlacementPage.aspx?BnbPosition={0}",
                    newPos.ID.ToString()));
            }
            catch (BnbProblemException pe)
            {
                labelPanelOneFeedback.Text = "Unfortunately, the Placement could not be created due to the following problems: <br/>";
                foreach (BnbProblem p in pe.Problems)
                    labelPanelOneFeedback.Text += p.Message + "<br/>";

            }
        }
        #endregion

        #region " ReturnToOriginalPage "     

        private void ReturnToOriginalPage()
        {
            Response.Redirect(lblHiddenUrlReferrer.Text);
        }
        #endregion

        #region " PopulateProgrammeArea "

        private void PopulateProgrammeArea()
        {
            Guid roleID = new Guid(Request.QueryString["BnbRole"]);
            m_role = BnbRole.Retrieve(roleID);
            int programmeOffice = m_role.Employer.ProgrammeOfficeID;

            // Programme Area lookup 
            BnbLookupDataTable programmeAreaLookup = BnbEngine.LookupManager.GetLookup("lkuProgrammeArea");
            DataView dtvProgrammeAreaView = new DataView(programmeAreaLookup);
            dtvProgrammeAreaView.RowFilter = "Exclude = 0 And ProgrammeOfficeID = " + programmeOffice;
            dtvProgrammeAreaView.Sort = "Description";
            cmbProgrammeArea.DataSource = dtvProgrammeAreaView;
            cmbProgrammeArea.DataValueField = "IntID";
            cmbProgrammeArea.DataTextField = "Description";
            cmbProgrammeArea.DataBind();
            cmbProgrammeArea.Items.Insert(0, "");
        }
        #endregion

        #region " PopulateVsoGoal "
        private void PopulateVsoGoal()
        {
            // VSO Goal lookup 
            BnbLookupDataTable vsoGoalLookup = BnbEngine.LookupManager.GetLookup("lkuMonitorObjective");
            DataView dtvVsoGoalView = new DataView(vsoGoalLookup);
            dtvVsoGoalView.RowFilter = "Exclude = 0";
            dtvVsoGoalView.Sort = "Description";
            cmbVsoGoal.DataSource = dtvVsoGoalView;            
            cmbVsoGoal.DataValueField = "IntID";
            cmbVsoGoal.DataTextField = "Description";
            cmbVsoGoal.DataBind();
            cmbVsoGoal.Items.Insert(0, "");
        }
           
        #endregion

        #region " PopulateVsoType "
              private void PopulateVsoType()
              {
                // VSO Type lookup 
                BnbLookupDataTable vsoTypeLookup = BnbEngine.LookupManager.GetLookup("vlkuBonoboVSOTypeWithGroup");
                DataView dtvVsoTypeView = new DataView(vsoTypeLookup);
                dtvVsoTypeView.RowFilter = "Exclude = 0";
                dtvVsoTypeView.Sort = "Description"; 
                cmbVsoType.DataSource = dtvVsoTypeView;              
                cmbVsoType.DataValueField = "IntID";
                cmbVsoType.DataTextField = "Description";
                cmbVsoType.DataBind();
                cmbVsoType.Items.Insert(0, "");
              }
        #endregion
    }
}
