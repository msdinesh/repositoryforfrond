<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>

<%@ Page Language="c#" Codebehind="AddressPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.AddressPage" %>

<%@ Register Src="../UserControls/GShowLocation.ascx" TagName="GShowLocation" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="cc1" Namespace="BonoboWebControls.ServicesControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>

    <script language="javascript" src="../Javascripts/GoogleMap.js"></script>

</head>
<body onload="initialize()" onunload="GUnload()">
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/bnbaddress24silver.gif">
            </uc1:Title>
            <uc1:NavigationBar ID="NavigationBar1" runat="server"></uc1:NavigationBar>
            <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons1" runat="server" CssClass="databuttons"
                NewButtonEnabled="False" NewWidth="55px" EditAllWidth="55px" SaveWidth="55px"
                UndoWidth="55px" HeightAllButtons="25px" NewText="New" EditAllText="Edit All"
                SaveText="Save" UndoText="Undo"></bnbpagecontrol:BnbDataButtons>
            <bnbpagecontrol:BnbMessageBox ID="BnbMessageBox1" runat="server" CssClass="messagebox">
            </bnbpagecontrol:BnbMessageBox>
            <p>
                <bnbpagecontrol:BnbSectionHeading ID="BnbSectionHeading1" runat="server" CssClass="sectionheading"
                    SectionHeading="Essential Info" Width="100%" Height="20px" Collapsed="false"></bnbpagecontrol:BnbSectionHeading>
            </p>
            <p>
                <table class="fieldtable" id="Table1" width="100%" border="0">
                    <tr>
                        <td class="label" style="height: 10px" width="25%">
                            Individual/Volunteer</td>
                        <td style="height: 10px" width="25%">
                            <bnbdatacontrol:BnbDomainObjectHyperlink ID="BnbDomainObjectHyperlink1" runat="server"
                                GuidProperty="BnbIndividual.ID" DataMember="BnbIndividual.FullName" HyperlinkText="[BnbDomainObjectHyperlink]"
                                Target="_top" RedirectPage="IndividualPage.aspx"></bnbdatacontrol:BnbDomainObjectHyperlink>
                        </td>
                        <td class="label" style="height: 10px" width="25%">
                            Address Type</td>
                        <td style="height: 10px" width="25%">
                            <p>
                                <bnbdatacontrol:BnbLookupControl ID="bnblckAddressType" runat="server" CssClass="lookupcontrol"
                                    Width="250px" Height="22px" DataMember="BnbIndividualAddress.AddressTypeID" AutoPostBack="True"
                                    LookupControlType="ReadOnlyLabel" ListBoxRows="4" DataTextField="Description"
                                    LookupTableName="vlkuBonoboAddressTypeWithGroup"></bnbdatacontrol:BnbLookupControl>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Valid From</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox1" runat="server" CssClass="datebox" Width="90px"
                                Height="20px" DataMember="BnbIndividualAddress.DateFrom" DateFormat="dd/mm/yyyy"
                                DateCulture="en-GB"></bnbdatacontrol:BnbDateBox>
                        </td>
                        <td class="label" width="25%">
                            Valid To</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox3" runat="server" CssClass="datebox" Width="90px"
                                Height="20px" DataMember="BnbIndividualAddress.DateTo" DateFormat="dd/mm/yyyy"
                                DateCulture="en-GB"></bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Marked Most Current</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox1" runat="server" CssClass="checkbox"
                                DataMember="BnbIndividualAddress.Current"></bnbdatacontrol:BnbCheckBox>
                        </td>
                        <td class="label" width="25%">
                            Address Locked</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox2" runat="server" CssClass="checkbox"
                                DataMember="BnbIndividualAddress.Address.AddressLocked" Enabled="False"></bnbdatacontrol:BnbCheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="25%">
                            Logged By</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl1" runat="server" CssClass="lookupcontrol"
                                Width="196px" Height="22px" DataMember="BnbIndividualAddress.CreatedBy" LookupControlType="ReadOnlyLabel"
                                ListBoxRows="4" DataTextField="Description" LookupTableName="vlkuBonoboUserShort">
                            </bnbdatacontrol:BnbLookupControl>
                        </td>
                        <td class="label" width="25%">
                            Logged On</td>
                        <td width="25%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox2" runat="server" CssClass="datebox" Width="90px"
                                Height="20px" DataMember="BnbIndividualAddress.CreatedOn" DateFormat="dd/mm/yyyy"
                                DateCulture="en-GB"></bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                </table>
            </p>
            <asp:Panel ID="panelWorkSection" runat="server">
                <p>
                    <bnbpagecontrol:BnbSectionHeading ID="BnbSectionHeading2" runat="server" CssClass="sectionheading"
                        Collapsed="false" Height="20px" Width="100%" SectionHeading="Work Address Fields">
                    </bnbpagecontrol:BnbSectionHeading>
                </p>
                <p>
                    <table class="fieldtable" id="Table2" width="100%" border="0">
                        <tr>
                            <td class="label" width="50%">
                                Organisation</td>
                            <td width="50%">
                                <bnbgenericcontrols:BnbHyperlink ID="uxOrganisationNameHyper" runat="server"></bnbgenericcontrols:BnbHyperlink></td>
                        </tr>
                        <tr>
                            <td class="label" width="50%">
                                Role</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox1" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" DataMember="BnbIndividualAddress.CompanyRole" StringLength="0"
                                    Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="50%">
                                Department</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox2" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" DataMember="BnbIndividualAddress.CompanyDepartment" StringLength="0"
                                    Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                            </td>
                        </tr>
                    </table>
                </p>
            </asp:Panel>
            <asp:Panel ID="panelBankingSection" runat="server">
                <p>
                    <bnbpagecontrol:BnbSectionHeading ID="BnbSectionHeading6" runat="server" CssClass="sectionheading"
                        Collapsed="false" Height="20px" Width="100%" SectionHeading="Banking Address Fields">
                    </bnbpagecontrol:BnbSectionHeading>
                </p>
                <p>
                    <table class="fieldtable" id="Table6" width="100%" border="0">
                        <tr>
                            <td class="label" width="25%">
                                Bank Name</td>
                            <td width="25%">
                                <bnbgenericcontrols:BnbHyperlink ID="uxBankNameHyper" runat="server"></bnbgenericcontrols:BnbHyperlink></td>
                            <td class="label" width="25%">
                                Account Name</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox15" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" DataMember="BnbIndividualAddress.AccountName" StringLength="0"
                                    Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%">
                                Sort Code / Routing No</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox16" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" DataMember="BnbIndividualAddress.SortCode" StringLength="0" Rows="1"
                                    MultiLine="false"></bnbdatacontrol:BnbTextBox>
                            </td>
                            <td class="label" width="25%">
                                Account Number</td>
                            <td width="25%">
                                <p>
                                    <bnbdatacontrol:BnbTextBox ID="BnbTextBox17" runat="server" CssClass="textbox" Height="20px"
                                        Width="250px" DataMember="BnbIndividualAddress.AccountNumber" StringLength="0"
                                        Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                                </p>
                            </td>
                        </tr>
                    </table>
                </p>
            </asp:Panel>
            <p>
                <bnbpagecontrol:BnbSectionHeading ID="BnbSectionHeading3" runat="server" CssClass="sectionheading"
                    SectionHeading="Main Address" Width="100%" Height="20px" Collapsed="false"></bnbpagecontrol:BnbSectionHeading>
            </p>
            <table class="fieldtable" id="Table3" width="100%" border="0">
                <tr>
                    <td class="label" style="height: 10px" width="50%">
                        Country</td>
                    <td style="height: 10px" width="50%">
                        <bnbdatacontrol:BnbLookupControl ID="dropDownAddressCountryID" runat="server" CssClass="lookupcontrol"
                            Width="250px" Height="22px" DataMember="BnbIndividualAddress.Address.CountryID"
                            AutoPostBack="False" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"
                            LookupTableName="lkuCountry" onChange="javascript:storePostCode(this);"></bnbdatacontrol:BnbLookupControl>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Postcode&nbsp;/ Zip</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbTextBox ID="textAddressPostcode" runat="server" CssClass="textbox"
                            Width="200px" Height="20px" DataMember="BnbIndividualAddress.Address.PostCode"
                            Enabled="False" StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                        <asp:Button ID="uxPostcodeSearch" runat="server" Text="Find" OnClick="uxPostcodeSearch_Click">
                        </asp:Button><cc1:BnbAddressLookupComposite ID="uxAddressLookupComposite" runat="server"
                            CssClass="lookupcontrol" Width="300px" Height="200px" AutoPostBack="True" ListBoxRows="6"
                            WebFormManagerMode="True" Visible="False" RegistrationCodeCapscan="00003304"
                            PasswordCapscan="492267" LicenseKey="XH57-MT97-CZ47-GH56" AccountCode="vso1111111">
                        </cc1:BnbAddressLookupComposite>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Line 1</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbTextBox ID="textAddressLine1" runat="server" CssClass="textbox"
                            Width="250px" Height="20px" DataMember="BnbIndividualAddress.Address.AddressLine1"
                            StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                        <asp:Button ID="uxAddressLine1Search" runat="server" Text="Find" OnClick="uxPostcodeSearch_Click">
                        </asp:Button>
			</td>

                </tr>
                <tr>
                    <td class="label" width="50%">
                        Line 2</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbTextBox ID="textAddressLine2" runat="server" CssClass="textbox"
                            Width="250px" Height="20px" DataMember="BnbIndividualAddress.Address.AddressLine2"
                            StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Line 3</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbTextBox ID="textAddressLine3" runat="server" CssClass="textbox"
                            Width="250px" Height="20px" DataMember="BnbIndividualAddress.Address.AddressLine3"
                            StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Postal Town / Line 4</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbTextBox ID="textAddressLine4" runat="server" CssClass="textbox"
                            Width="250px" Height="20px" DataMember="BnbIndividualAddress.Address.AddressLine4"
                            StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        County / State</td>
                    <td width="50%">
                        <bnbdatacontrol:BnbTextBox ID="textAddressCounty" runat="server" CssClass="textbox"
                            Width="250px" Height="20px" DataMember="BnbIndividualAddress.Address.County"
                            StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                    </td>
                </tr>
		<tr>
                    <td class="label" width="50%">
                    <td width="50%"><input id="btnShowLocation" runat="Server" type="button" onclick="showLocation()" value="Show on map" /></TD>

                </tr>
                <tr>
                    <td class="label" width="50%">
                        Tel</td>
                    <td width="50%">
                        <asp:TextBox ID="uxTelephoneTextBox" runat="server"></asp:TextBox>
                        <asp:Label ID="uxTelephoneLabel" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="label" width="50%">
                        Fax</td>
                    <td width="50%">
                        <asp:TextBox ID="uxFaxTextBox" runat="server"></asp:TextBox>
                        <asp:Label ID="uxFaxLabel" runat="server"></asp:Label></td>
                </tr>
            </table>
            <asp:Panel ID="panelEmergencySection" runat="server">
                <p>
                    <bnbpagecontrol:BnbSectionHeading ID="BnbSectionHeading5" runat="server" CssClass="sectionheading"
                        Collapsed="false" Height="20px" Width="100%" SectionHeading="Emergency Address Fields">
                    </bnbpagecontrol:BnbSectionHeading>
                </p>
                <p>
                    <table class="fieldtable" id="Table5" width="100%" border="0">
                        <tr>
                            <td class="label" width="25%">
                                Emergency Contact</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox13" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" DataMember="BnbIndividualAddress.Address.ContactName" StringLength="0"
                                    Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                            </td>
                            <td class="label" width="25%">
                                Relationship</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox14" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" DataMember="BnbIndividualAddress.Address.RelativeType" StringLength="0"
                                    Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                            </td>
                        </tr>
                    <tr>
                            <td class="label" width="25%">
                                Emergency Email</td>
                            <td width="25%">
                                <asp:TextBox ID="uxEmailTextBox" Width="250px" runat="server"></asp:TextBox>
                                <asp:Label ID="uxEmailLabel" Width="250px" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </p>
            </asp:Panel>
	    <p>
                <bnbpagecontrol:BnbSectionHeading ID="headingGeoDetails" runat="server" CssClass="sectionheading"
                    SectionHeading="Geographical Details" Width="100%" Height="20px" Collapsed="false">
                </bnbpagecontrol:BnbSectionHeading>
            </p>
              <p> <uc2:GShowLocation HeadingVisibile="false" ID="showLocation" runat="server" /></p>
                            

            <p>
                <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons2" runat="server" CssClass="databuttons"
                    NewButtonEnabled="False" NewWidth="55px" EditAllWidth="55px" SaveWidth="55px"
                    UndoWidth="55px" HeightAllButtons="25px" NewText="New" EditAllText="Edit All"
                    SaveText="Save" UndoText="Undo"></bnbpagecontrol:BnbDataButtons>
            </p>
        </div>
    </form>
</body>
</html>
