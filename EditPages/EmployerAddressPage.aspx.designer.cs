//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3082
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Frond.EditPages {
    
    public partial class EmployerAddressPage {
        protected System.Web.UI.WebControls.Literal literalBeforeCss;
        protected System.Web.UI.WebControls.Literal literalAfterCss;
        protected System.Web.UI.HtmlControls.HtmlForm Form1;
        protected Frond.UserControls.MenuBar menuBar;
        protected Frond.UserControls.Title titleBar;
        protected BonoboWebControls.PageControls.BnbDataButtons BnbDataButtons1;
        protected BonoboWebControls.PageControls.BnbMessageBox BnbMessageBox1;
        protected BonoboWebControls.DataControls.BnbDomainObjectHyperlink BnbDomainObjectHyperlink1;
        protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox1;
        protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox2;
        protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox3;
        protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox4;
        protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox5;
        protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox6;
        protected BonoboWebControls.DataControls.BnbLookupControl BnbLookupControl1;
        protected System.Web.UI.HtmlControls.HtmlInputButton btnShowLocation;
        protected BonoboWebControls.DataGrids.BnbDataGridForDomainObjectList BnbDataGridForDomainObjectList1;
        protected BonoboWebControls.PageControls.BnbSectionHeading headingGeoDetails;
        protected Frond.UserControls.GShowLocation showLocation;
    }
}
