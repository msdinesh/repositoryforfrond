using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboEngine;
using BonoboDomainObjects;
using BonoboEngine.Query;

namespace Frond.EditPages
{
    /// <summary>
    /// This class is meant for adding / editiing final medical clearances. 
    /// </summary>
    public partial class AppMedicalClearancePage : System.Web.UI.Page
    {
        BnbWebFormManager bnb = null;

        #region " Page_Load "

        protected void Page_Load(object sender, EventArgs e)
        {

#if DEBUG
            //			bnb = null;
            if (Request.QueryString.ToString() == "")
                BnbWebFormManager.ReloadPage("BnbServiceMedicalClearance=B73A83D9-6CF6-4834-A4DA-A3015E86FC02");
            else
                bnb = new BnbWebFormManager(this, "BnbServiceMedicalClearance");
#else	
			bnb = new BnbWebFormManager(this,"BnbServiceMedicalClearance");
#endif
            bnb.SaveEvent += new EventHandler(bnb_SaveEvent);
            bnb.EditModeEvent += new EventHandler(bnb_EditEvent);
            bnb.ViewModeEvent += new EventHandler(bnb_ViewEvent);

            BnbWorkareaManager.SetPageStyleOfUser(this);
            // tweak page title settings
            bnb.PageNameOverride = "Application Medical Clearance";
            NavigationBar1.RootDomainObject = "BnbApplication";
            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null)
                    titleBar.TitleText = bnb.PageTitleDescriptionCurrent;

                doPopulatePlacementLink();
            }

            doIndividualHyperlink();

        }

        #endregion

        #region " doPopulatePlacementLink "

        private void doPopulatePlacementLink()
        {

            BnbServiceMedicalClearance mc = bnb.GetPageDomainObject("BnbServiceMedicalClearance", true) as BnbServiceMedicalClearance;

            if (mc != null)
            {
                if (!(mc.IsNew))
                {
                    string placementRef = BnbEngine.LookupManager.GetLookupItemDescription("vlkuPlacementsLinked", mc.ServiceID);
                    lblPlacementLink.Text = placementRef;
                }
                else if (mc.IsNew)
                {
                    string strApplicationID = Page.Request.QueryString["BnbApplication"];
                    if (strApplicationID != null && strApplicationID.ToLower() != "new")
                    {
                        BnbTextCondition applicationRef = new BnbTextCondition("tblService_ApplicationID", strApplicationID, BnbTextCompareOptions.ExactMatch);

                        BnbCriteria criteria = new BnbCriteria();
                        criteria.QueryElements.Add(applicationRef);
                        BnbQuery query = new BnbQuery("vwPlacementsForReview", criteria);
                        DataTable dt = query.Execute().Tables["Results"];

                        lkuPlacementLink.DataSource = dt;
                        lkuPlacementLink.DataTextField = "Custom_FullPositionreference";
                        lkuPlacementLink.DataValueField = "tblService_ServiceID";

                        lkuPlacementLink.DataBind();

                        lkuPlacementLink.Items.Insert(0, "");
                    }
                }

            }
        }

        #endregion

        #region " doIndividualHyperlink "

        private void doIndividualHyperlink()
        {
            string strApplicationID = Page.Request.QueryString["BnbApplication"];

            if (strApplicationID != null && strApplicationID.ToLower() != "new")
            {
                BnbApplication objApplication = BnbApplication.Retrieve(new Guid(strApplicationID));
                if (objApplication.Individual.FullName != null)
                    BnbLnkIndividual.Text = objApplication.Individual.FullName;
                else
                    BnbLnkIndividual.Text = "Back to the application page";
                BnbLnkIndividual.NavigateUrl = "ApplicationPage.aspx?BnbApplication=" + strApplicationID;
            }
            BnbLnkIndividual.Enabled = true;
        }

        #endregion

        #region " bnb_SaveEvent "

        private void bnb_SaveEvent(object sender, EventArgs e)
        {
            // Set Service ID
            BnbServiceMedicalClearance mc = bnb.GetPageDomainObject("BnbServiceMedicalClearance", true) as BnbServiceMedicalClearance;
            if (mc != null)
            {
                if (mc.IsNew)
                {
                    //TPT Amended
                    mc.Service = (BnbService)BnbDomainObject.GeneralRetrieve(new Guid(lkuPlacementLink.SelectedValue.ToString()), typeof(BnbService)); 
                    mc.ClearanceTypeID = 1002;
                }
            }
        }

        #endregion

        #region " bnb_ViewEvent "

        private void bnb_ViewEvent(object sender, EventArgs e)
        {
            lkuPlacementLink.Visible = false;
            lblPlacementLink.Visible = true;
        }

        #endregion

        #region " bnb_EditEvent "

        private void bnb_EditEvent(object sender, EventArgs e)
        {
            BnbServiceMedicalClearance mc = bnb.GetPageDomainObject("BnbServiceMedicalClearance", true) as BnbServiceMedicalClearance;
            lkuPlacementLink.Visible = false;
            lblPlacementLink.Visible = true;
            if (mc != null)
                if (mc.IsNew)
                {
                    lkuPlacementLink.Visible = true;
                    lblPlacementLink.Visible = false;
                }
        }

        #endregion

    }
}
