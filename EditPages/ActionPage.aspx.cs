using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboEngine;
using BonoboDomainObjects;
using System.IO;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for ActionPage.
	/// </summary>
	public partial class ActionPage : System.Web.UI.Page
	{
		protected BonoboWebControls.DataControls.BnbDomainObjectHyperlink BnbDomainObjectHyperlink1;
		protected BonoboWebControls.DataControls.BnbLookupControl BnbLkpActionType;
		protected BonoboWebControls.DataControls.BnbDateBox BnbDateBox2;
		protected UserControls.Title titleBar;
		protected BonoboWebControls.DataControls.BnbLookupControl Bnblookupcontrol1;
	string dir = AppDomain.CurrentDomain.BaseDirectory;
		protected Frond.UserControls.NavigationBar NavigationBar1;

		BnbWebFormManager bnb = null;

		protected void Page_Load(object sender, System.EventArgs e)
		{
#if DEBUG	
			if (Request.QueryString.ToString() == "")
				BnbWebFormManager.ReloadPage("BnbIndividual=D1DAAB24-AADF-11D4-908F-00508BACE998&BnbContactDetail=6B87D0BF-AAFC-11D4-908F-00508BACE998");
			else
				bnb = new BnbWebFormManager(this,"BnbContactDetail");
#else	
			bnb = new BnbWebFormManager(this,"BnbContactDetail");
#endif

            //If Individual is missing in Querystring, reload page with it 
            string querystringall = this.Request.QueryString.ToString();
            if (querystringall.IndexOf("BnbIndividual") == -1)
            {
                System.Guid indvID = ((BnbContactDetail)bnb.GetPageDomainObject("BnbContactDetail")).IndividualID;
                BnbWebFormManager.ReloadPage("BnbIndividual=" + indvID.ToString() + "&" + querystringall);
            }

			// subscribe to wfm events
			bnb.ViewModeEvent +=new EventHandler(bnb_ViewModeEvent);
			bnb.EditModeEvent +=new EventHandler(bnb_EditModeEvent);
			bnb.SaveEvent +=new EventHandler(bnb_SaveEvent);
			
			string individualID = Page.Request.QueryString["BnbIndividual"]; 
			// Put user code to initialize the page here
			NavigationBar1.RootDomainObject = "BnbIndividual";
			NavigationBar1.RootDomainObjectID = individualID;

			BnbWorkareaManager.SetPageStyleOfUser(this);
			BnbWordFormButton1.Visible = false;
			if (this.Request.QueryString["BnbContactDetail"] != null &&
				this.Request.QueryString["BnbContactDetail"].ToLower() != "new")
			{
				BnbContactDetail con = bnb.GetPageDomainObject("BnbContactDetail") as BnbContactDetail;
				string conDesc  = DescriptionForContactDescriptionID(con.ContactDescriptionID);

				if(File.Exists(dir +"Templates\\StandardLetters\\"+ conDesc +".doc"))
				{
					BnbWordFormButton1.Visible = true;
				}
			}
		


			bnb.LogPageHistory = true;
			// tweak page title settings
			bnb.PageNameOverride = "Actions";
			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;

				doApplicationPicker();
			}
		}



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private string DescriptionForContactDescriptionID(int contactDescriptionID)
		{
			return BnbEngine.LookupManager.GetLookupItemDescription("lkuContactDescription", contactDescriptionID);
		}
		
		protected void BnbWordFormButton1_Clicked(object sender, System.EventArgs e)
		{
		

			if (this.Request.QueryString["BnbContactDetail"] != null &&
				this.Request.QueryString["BnbContactDetail"].ToLower() != "new")
			{
				string contactID = Page.Request.QueryString["BnbContactDetail"]; 
				BnbContactDetail con = BnbContactDetail.Retrieve(new System.Guid(contactID));
				string conDesc  = DescriptionForContactDescriptionID(con.ContactDescriptionID);
				if (BnbRuleUtils.HasValue(con.ID))
				{
					DataTable dt = BnbContactDetail.GetStandardLetterData(con.ID);

					if (dt.Rows.Count >0)
					{
						DataRow dr=dt.Rows[0];;

						BnbWordFormButton1.WordTemplate = dir + "Templates\\StandardLetters\\" + conDesc +".doc";
						BnbWordFormButton1.MergeData = dt;
						BnbWordFormButton1.FilenameForUser = String.Format("StandardLetter{0}.doc",
							DateTime.Now.ToString("ddMMMyyyy-hhmmss"));
						bool success = BnbWordFormButton1.GenerateWordForm();
						if (success)
							BnbWebFormManager.LogAction("Action (Word Form)",
								"Generated Word Form ", this);
					}
					else
					{
						if (con.AddressID == Guid.Empty)
							BnbWordFormButton1.ErrorMessage ="Could not generate data for Standard Letter (No address specified for Action)";
						else
							BnbWordFormButton1.ErrorMessage ="Could not generate data for Standard Letter (check Contact Blocks for Individual)";
					}
					
				
				}
			}
		}

		private void doApplicationPicker()
		{
			BnbIndividual indv = bnb.GetPageDomainObject("BnbIndividual") as BnbIndividual;
			// add the not-linked option
			uxApplicationPicker.Items.Add(new ListItem("<None>", Guid.Empty.ToString()));
			// then add an option for each app
			indv.Applications.Sort("ApplicationOrder", true);
			foreach(BnbApplication appLoop in indv.Applications)
			{
				uxApplicationPicker.Items.Add(new ListItem(appLoop.ApplicationDescription, appLoop.ID.ToString()));
			}

			// now fill in the current value
			BnbContactDetail action = bnb.GetPageDomainObject("BnbContactDetail") as BnbContactDetail;
			if (action == null)
			{
				// in new mode - select the no link option
				uxApplicationPicker.SelectedValue = indv.CurrentApplication.ID.ToString();
//				uxApplicationPicker.SelectedValue = Guid.Empty.ToString();
			}
			else
			{
				uxApplicationPicker.SelectedValue = action.ApplicationID.ToString();
			}
			// copy selected item to label
			uxApplicationLabel.Text = Server.HtmlEncode(uxApplicationPicker.SelectedItem.Text);
			

		}

		private void bnb_ViewModeEvent(object sender, EventArgs e)
		{
			// show view version of application pickers
			uxApplicationLabel.Visible = true;
			uxApplicationPicker.Visible = false;
		}

		private void bnb_EditModeEvent(object sender, EventArgs e)
		{
			// show edit version of application pickers 
			// but only in new mode
			uxApplicationLabel.Visible = !bnb.InNewMode;
			uxApplicationPicker.Visible = bnb.InNewMode;
		}

		private void bnb_SaveEvent(object sender, EventArgs e)
		{
			// save data from application picker
			BnbContactDetail action = bnb.GetPageDomainObject("BnbContactDetail",true) as BnbContactDetail;
			if (uxApplicationPicker.SelectedValue != Guid.Empty.ToString())
			{
				BnbApplication connectedApp = BnbApplication.Retrieve(new Guid(uxApplicationPicker.SelectedValue));
				action.Application = connectedApp;
			}

		}

		protected void BnbButton1_Click(object sender, System.EventArgs e)
		{
			BnbContactDetail action = bnb.GetPageDomainObject("BnbContactDetail",true) as BnbContactDetail;

			BnbEditManager em = new BnbEditManager();
			action.RegisterForEdit(em);

			// mark the record for deletion
			action.MarkForDeletion();
			try
			{
				em.SaveChanges();
				Response.Redirect("IndividualPage.aspx?BnbIndividual=" + action.IndividualID.ToString());
			} 
			catch (BnbProblemException pe)
			{
				foreach(BnbProblem prob in pe.Problems)
					lblSaveError.Text = prob.Message.ToString();
			}
			
		}
	}
}