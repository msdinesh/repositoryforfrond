using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using BonoboWebControls;
using BonoboEngine;
using BonoboDomainObjects;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for AddressPage.
    /// </summary>
    public partial class EmployerAddressPage : System.Web.UI.Page
    {
        protected BonoboWebControls.DataControls.BnbLookupControl BnbLookupControl2;
        BnbWebFormManager bnb = null;
        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            
#if DEBUG

            if (Request.QueryString.ToString() == "")
            {
                bnb = new BnbWebFormManager("BnbEmployer=ABFEAAF2-AAE8-11D4-908F-00508BACE998&BnbEmployerAddress=8A35E4AB-AAEA-11D4-908F-00508BACE998");
            }
            else
            {
                bnb = new BnbWebFormManager(this, "BnbEmployerAddress");
            }
#else
			BnbWebFormManager bnb = new BnbWebFormManager(this,"BnbEmployerAddress");
#endif

            

            // turn on logging for this page
            bnb.LogPageHistory = true;
            //<Integartion>
            // tweak page title settings
            bnb.PageNameOverride = "Employer Address";
            BnbWorkareaManager.SetPageStyleOfUser(this);
  
            bnb.EditModeEvent += new EventHandler(bnb_EditModeEvent);
            bnb.ViewModeEvent += new EventHandler(bnb_ViewModeEvent);
            bnb.SaveEvent += new EventHandler(bnb_SaveEvent);
            if (!this.IsPostBack)
            {
                DoMapVisibility();
                if (bnb.ObjectTree != null)
                    titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
                
            }
            AssignGeoDetails("LoadValues");
        }

        /// <summary>
        /// Latitude, Longitude cannot be bound using the BnbWebFormManager, 
        /// so are implemented as normal
        /// ASP.NET controls on the page
        /// This checks for data in the controls and saves the objects
        /// </summary>
        private void doSaveGeographicalDetails()
        {
            if (HasPermissionToLocateOnMap())
            {

                    BnbEmployerAddress empAddress = bnb.GetPageDomainObject("BnbEmployerAddress", true) as BnbEmployerAddress;
           
                if (showLocation.Latitude.Trim() != "")
                    empAddress.Address.Latitude = Convert.ToSingle(showLocation.Latitude);
                else
                    //Setting null value for validation
                    empAddress.Address.Latitude = Convert.ToSingle(0.0);

                if (showLocation.Longitude.Trim() != "")
                    empAddress.Address.Longitude = Convert.ToSingle(showLocation.Longitude);
                else
                    //Setting null value for validation
                    empAddress.Address.Longitude = Convert.ToSingle(0.0);
            }

        }
        /// <summary>
        /// Added newly for role based access
        /// </summary>
        private void DoMapVisibility()
        {
            if (HasPermissionToLocateOnMap())
            {

                headingGeoDetails.Visible = true;
                showLocation.Visible = true;
                btnShowLocation.Visible = true;
            }
            else
            {
                headingGeoDetails.Visible = false;
                showLocation.Visible = false;
                btnShowLocation.Visible = false;
            }
        }
        private void doLoadGeographicalDetails()
        {
            if (HasPermissionToLocateOnMap())
            {
                BnbEmployerAddress empAdd = bnb.GetPageDomainObject("BnbEmployerAddress") as BnbEmployerAddress;
   
                {
                    float lat = empAdd.Address.Latitude;
                    float lng = empAdd.Address.Longitude;

                    showLocation.Latitude = lat.ToString();
                    showLocation.Longitude = lng.ToString();
                }
            }
        }
        /// <summary>
        /// fires when the web form manager goes into view mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bnb_ViewModeEvent(object sender, EventArgs e)
        {
            btnShowLocation.Visible = false;
            //if (HasPermissionToLocateOnMap())
            //{
            //    btnShowLocation.Visible = true;
            //}

            doLoadGeographicalDetails();

            //Set Page mode for GMap
            SetGeoMode("view");
            //For capturing Geographical locations
            AssignGeoDetails("ViewValues");

        }
        /// <summary>
        /// Fires when the BnbWebFormManager is performing a save
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bnb_SaveEvent(object sender, EventArgs e)
        {

            this.doSaveGeographicalDetails();

        }


        /// <summary>
        /// fires when the web form manager goes into edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bnb_EditModeEvent(object sender, EventArgs e)
        {

            if (HasPermissionToLocateOnMap())
            {
                btnShowLocation.Visible = true;
            }

            //Set Page mode for GMap
            SetGeoMode("new/edit");
        }
        //For capturing Geographical locations
        //Set page mode to control GMap 
        private void SetGeoMode(string mode)
        {
            if (HasPermissionToLocateOnMap())
            {
                string strScript = "mode='" + mode + "';";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "SetMode", strScript, true);
            }

        }
        //For capturing Geographical locations
        //Set page mode to control GMap 
        private void SetGeoModeForView(string mode)
        {
            if (HasPermissionToLocateOnMap())
            {
                string strScript = "mode='" + mode + "';";
                strScript += "latVal = " + showLocation.Latitude.Trim() + ";lngVal = " + showLocation.Longitude.Trim() + ";";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "SetMode", strScript, true);
            }

        }
        //Generate client script for setting Geographical locations
        private void AssignGeoDetails(string scriptID)
        {
            if (HasPermissionToLocateOnMap())
            {
                showLocation.CssName = "gmaptableNormal";
                StringBuilder script = new StringBuilder();
                script.Append("pageName ='EmployerAddressPage';");

                if (!(showLocation.Latitude.Trim() == string.Empty))
                {
                    ViewState["Latittude"] = showLocation.Latitude;
                    ViewState["Longitude"] = showLocation.Longitude;
                }

                if (!(ViewState["Latittude"] == null))
                {
                    string latitude = ViewState["Latittude"].ToString();
                    string longitude = ViewState["Longitude"].ToString();
                    script.Append("latVal = " + latitude + ";lngVal = " + longitude + ";");
                }
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), scriptID, script.ToString(), true);
            }
        }
        /// <summary>
        /// Check permisssion to access the Google map functionality
        /// </summary>
        private bool HasPermissionToLocateOnMap()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_LocateAddressOnMap))
                return true;
            else
                return false;
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        
    }
}

