<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Page language="c#" Codebehind="WizardNewPersonnel.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.WizardNewPersonnel" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>WizardNewPersonnel</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<DIV class="wizardheader" ms_positioning="FlowLayout">
					<H3><img class="icon24" src="../Images/wizard24silver.gif"> New Personnel Wizard</H3>
				</DIV>
				<asp:panel id="panelZero" runat="server" CssClass="wizardpanel">
					<P><EM>This wizard will help you to add&nbsp;a new&nbsp;Personnel record to an 
							Organisation.</EM>
					<P>Please enter the Ref No or Forename and Surname of the Individual and press Next</P>
					<P></P>
					<P>
						<TABLE class="wizardtable" id="Table2">
							<TR>
								<TD class="label" style="HEIGHT: 15px">Organisation Name</TD>
								<TD style="HEIGHT: 15px">
									<asp:Label id="lblCompanyName" runat="server"></asp:Label></TD>
							</TR>
							<TR>
								<TD class="label">Forename (or Initials)</TD>
								<TD>
									<asp:TextBox id="txtForename" runat="server" Width="236px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD class="label">Surname</TD>
								<TD>
									<asp:TextBox id="txtSurname" runat="server" Width="236px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD class="label">Ref No</TD>
								<TD>
									<asp:TextBox id="txtRefNo" runat="server"></asp:TextBox></TD>
							</TR>
						</TABLE>
					<P>
						<asp:label id="lblPanelZeroFeedback" runat="server" CssClass="feedback"></asp:label></P>
				</asp:panel>
				<asp:Panel id="panelOne" runat="server" CssClass="wizardpanel">
					<asp:Panel id="panelChoose" runat="server">
						<P>The following similar&nbsp;Individuals already exist. If any of them match the 
							data you were going to enter, then please select the appropriate table 
							row&nbsp;and press Next.</P>
						<P>
							<bnbdatagrid:BnbDataGridForView id="bdgIndividuals" runat="server" CssClass="datagrid" Width="100%" ViewLinksText="View"
								NewLinkText="New" NewLinkVisible="False" ViewLinksVisible="true" ShowResultsOnNewButtonClick="false" DisplayResultCount="false"
								DisplayNoResultMessageOnly="false" DataGridType="RadioButtonList" ViewName="vwBonoboIndividualDuplicateCheckFast"
								ColumnNames="tblIndividualKeyInfo_Surname, tblIndividualKeyInfo_Forename, tblIndividualKeyInfo_SurnamePrefix, tblIndividualKeyInfo_old_indv_id, tblAddress_AddressLine1, lkuCountry_Description"
								GuidKey="tblIndividualKeyInfo_IndividualID"></bnbdatagrid:BnbDataGridForView><BR>
							<asp:Label id="lblMaxRowsExceeded" runat="server"></asp:Label></P>
						<P>If none of the above&nbsp;Individuals match, then please check this box and 
							press Next to create&nbsp;a new Individual.</P>
						<P>
							<asp:CheckBox id="chkMakeNewIndividual" runat="server" Text="Make a new Individual"></asp:CheckBox></P>
					</asp:Panel>
					<asp:Label id="lblNoDuplicates" runat="server">No similar Individuals already exist. Press Next to continue.</asp:Label>
					<P>
						<asp:Label id="lblPanelOneFeedback" runat="server" CssClass="feedback"></asp:Label>
				</asp:Panel>
				<asp:Panel id="panelTwo" runat="server" CssClass="wizardpanel">
					<P>Please enter further details about the new Personnel, then press Finish to 
						create the new record:</P>
					<TABLE class="wizardtable" id="Table4">
						<TR>
							<TD class="label" style="HEIGHT: 15px">Organisation Name</TD>
							<TD style="HEIGHT: 15px">
								<asp:Label id="lblCompanyName2" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 25px">Org Address</TD>
							<TD style="HEIGHT: 25px">
								<asp:DropDownList id="cboCompanyAddress" runat="server" Width="224px"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD class="label">Forenames</TD>
							<TD>
								<asp:Label id="lblForename" runat="server"></asp:Label>
								<asp:TextBox id="txtForenameFinal" runat="server"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label">Surname</TD>
							<TD>
								<asp:Label id="lblSurname" runat="server"></asp:Label>
								<asp:TextBox id="txtSurnameFinal" runat="server"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label">Ref No</TD>
							<TD>
								<asp:Label id="lblRefNo" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 25px">Role</TD>
							<TD style="HEIGHT: 25px">
								<asp:TextBox id="txtRole" runat="server" Width="224px"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label">Dept</TD>
							<TD>
								<asp:TextBox id="txtDept" runat="server" Width="224px"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label">Telephone</TD>
							<TD>
								<asp:TextBox id="txtTelephone" runat="server" Width="224px"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label">Email</TD>
							<TD>
								<asp:Label id="lblEmail" runat="server"></asp:Label>
								<asp:TextBox id="txtEmail" runat="server" Width="224px"></asp:TextBox></TD>
						</TR>
					</TABLE>
					<P>
						<asp:Label id="lblPanelTwoFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				</asp:Panel>
				<P></P>
				<P>
					<uc1:WizardButtons id="wizardButtons" runat="server"></uc1:WizardButtons></P>
				<P>
					<asp:Label id="lblHiddenCompanyID" runat="server" Visible="False"></asp:Label>
					<asp:Label id="lblHiddenIndividualID" runat="server" Visible="False"></asp:Label></P>
			</div>
		</form>
	</body>
</HTML>
