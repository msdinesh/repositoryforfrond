<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="cc1" Namespace="BonoboWebControls.ServicesControls" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="WizardNewAddress.aspx.cs" AutoEventWireup="false" Inherits="Frond.EditPages.WizardNewAddress" %>
<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>WizardNewAddress</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<DIV class="wizardheader" ms_positioning="FlowLayout">
					<H3><IMG class="icon24" src="../Images/wizard24silver.gif"> New&nbsp;Address Wizard</H3>
				</DIV>
				<asp:panel id="panelZero" runat="server" Height="100px" CssClass="wizardpanel">
					<P><EM>This wizard will help you to add a new&nbsp;Address record.</EM>
					<P>To do a postcode lookup, please select the Country and&nbsp;enter the 
						PostCode&nbsp;of the new&nbsp;Address and press Find. You can then select an 
						address from the list.</P>
					<P>If you do not want to do a postcode lookup, then just select the Address Type 
						and press Finish.</P>
					<P></P>
					<P>
						<TABLE class="wizardtable" id="Table2">
							<TR>
								<TD class="label" style="HEIGHT: 6px">Individual</TD>
								<TD style="HEIGHT: 6px">
									<asp:Label id="lblIndividualFullName" runat="server"></asp:Label></TD>
							</TR>
							<TR>
								<TD class="label">Address Type</TD>
								<TD>
									<asp:DropDownList id="cmbAddressType" runat="server" Height="20px" Width="250px"></asp:DropDownList></TD>
							</TR>
							<TR>
								<TD class="label">Country</TD>
								<TD>
									<asp:DropDownList id="cmbCountry" runat="server" Height="20px" Width="250px"></asp:DropDownList></TD>
							</TR>
							<TR>
								<TD class="label">Postcode</TD>
								<TD>
									<asp:TextBox id="txtPostCode" runat="server" Width="136px"></asp:TextBox>
									<asp:Button id="cmdFind1" runat="server" Text="Find"></asp:Button></TD>
							</TR>
							<TR>
								<TD class="label"></TD>
								<TD>
									<cc1:BnbAddressLookupComposite id="uxAddressLookup" runat="server" CssClass="lookupcontrol" Width="400px" ListBoxRows="20"
										AccountCode="vso1111111" LicenseKey="XH57-MT97-CZ47-GH56" AutoPostBack="False" RegistrationCodeCapscan="00003304"
										PasswordCapscan="492267"></cc1:BnbAddressLookupComposite></TD>
							</TR>
						</TABLE>
						<INPUT id="lblHiddenIndividualID" type="hidden" name="lblHiddenIndividualID" runat="server">
						<asp:label id="labelPanelZeroFeedback" runat="server" CssClass="feedback"></asp:label><INPUT id="lblHiddenAddressTypeID" type="hidden" name="lblHiddenAddressTypeID" runat="server"></P>
				</asp:panel>
				<P></P>
				<P><uc1:wizardbuttons id="wizardButtons" runat="server"></uc1:wizardbuttons></P>
				<P><asp:label id="labelHiddenRequestIDUnused" runat="server" Visible="False"></asp:label></P>
				<p></p>
			</div>
		</form>
	</body>
</HTML>
