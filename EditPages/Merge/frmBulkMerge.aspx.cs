#region " Name Space "
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using BonoboDomainObjects;
using BonoboEngine;
using BonoboWebControls;
using System.Text.RegularExpressions;
using BonoboEngine.Utilities;
#endregion

namespace Frond.EditPages.Merge
{
    public partial class frmBulkMerge : System.Web.UI.Page
    {
        #region " Page load"

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                // check user has permission to access this page
                BnbWorkareaManager.CheckPageAccess(this);
                if (!BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Access_BulkMergeTool))
                {
                    Response.Redirect(String.Format("~/Message.aspx?Message={0}",
                 this.Page.Server.UrlEncode("You do not have access rights for this page, sorry.")), true);

                }
                BnbWorkareaManager.SetPageStyleOfUser(this);
            }
            btnMergeAll.Attributes.Add("onclick", "javascript:doHourglass();");
            lblErrorMessage.Text = string.Empty;
        }

        #endregion

        #region " Merge All Click Event "

        protected void btnMergeAll_Click(object sender, EventArgs e)
        {
            pnlBulkMergeReports.Visible = false;

            if (fuMergeFile.HasFile)
            {
                ArrayList alCSV = new ArrayList();
                alCSV = GetDuplicateRecords();
                if (alCSV != null)
                {
                    BulkMergeProcess(alCSV);
                    if (lblErrorMessage.Text == string.Empty)
                        lblErrorMessage.Text = "Bulk Merge completed.";
                }
            }
            else
                lblErrorMessage.Text = "Please upload a CSV file before Bulk Merge";
        }

        #endregion

        #region " Bulk Merge Process "

        /// <summary>
        /// Start the Bulk Merge Process & the results are bounded to Grid
        /// </summary>
        /// <param name="alCSV"></param>
        private void BulkMergeProcess(ArrayList alCSV)
        {
            string resultMessage = string.Empty;
            bool MergeSucess;

            // For Full Bulk Merge reports
            DataTable dtOutputResult = new DataTable();
            dtOutputResult.Columns.Add("Target");
            dtOutputResult.Columns.Add("Source");
            dtOutputResult.Columns.Add("Output");

            // For Full Bulk Merge Exception reports
            DataTable dtExceptionResults = new DataTable();
            dtExceptionResults.Columns.Add("Target");
            dtExceptionResults.Columns.Add("Source");
            dtExceptionResults.Columns.Add("Output");

            try
            {
                // First line of the CSV file(i.e. 0) is Header so initiate i value from 1
                for (int i = 1; i < alCSV.Count; i++)
                {
                    DataRow drResult = dtOutputResult.NewRow();
                    MergeSucess = false;

                    //get target record
                    string targetRecord = ((string[])alCSV[i])[0].ToString().Trim();
                    //get source record
                    string uwtdRecord = ((string[])alCSV[i])[1].ToString().Trim();
                    if (targetRecord == string.Empty && uwtdRecord == string.Empty)
                        continue;
                    else if (targetRecord != string.Empty && uwtdRecord != string.Empty)
                    {
                        resultMessage = MergeRules(targetRecord, uwtdRecord);
                        if (resultMessage == string.Empty)
                        {
                            BnbIndividual unwtdInd = BnbIndividual.RetrieveByRefNo(uwtdRecord);
                            GetAllReferenceIDFromUnwantedIndivdiual(unwtdInd);

                            try
                            {
                                BonoboDomainObjects.BnbIndividualMerge objMerge = new BnbIndividualMerge(true);
                                objMerge.MergeIndividuals(Session["UnwantedID"].ToString(), Session["TargetID"].ToString());
                                MergeSucess = true;
                            }
                            catch (BnbProblemException pe)
                            {
                                BonoboEngine.BnbLockManager objLockMgr = new BnbLockManager();
                                objLockMgr.ClearAllLocks();

                                foreach (BnbProblem prob in pe.Problems)
                                {
                                    // check the problem whether it is caused by Target or Unwanted with temp 'tblUnwanted' table
                                    DataTable dt = (DataTable)Session["tblUnwanted"];
                                    bool errorUwtedInd = false;
                                    int count = dt.Rows[0].ItemArray.Length;

                                    for (int j = 0; j < count; j++)
                                    {
                                        if (prob.ID.ToString() == dt.Rows[0].ItemArray[j].ToString())
                                        {
                                            resultMessage += unwtdInd.RefNo + "-" + prob.Message + "<br>";
                                            errorUwtedInd = true;
                                        }
                                    }
                                    if (!errorUwtedInd)
                                        resultMessage += targetRecord.ToString() + "-" + prob.Message + "<br>";
                                }
                            }
                            if (MergeSucess)
                                resultMessage = "Merged";
                            // nullify the current session
                            Session["tblUnwanted"] = null;
                        }
                    }
                    else
                        resultMessage = "Invalid columns identified";

                    // store the Individual details to the temp table.
                    drResult["Target"] = targetRecord;
                    drResult["Source"] = uwtdRecord;
                    drResult["Output"] = resultMessage;

                    //Bind datarow to datatable
                    dtOutputResult.Rows.Add(drResult);
                    //for Exceptions report
                    if (!MergeSucess)
                    {
                        DataRow drExcetpion = dtExceptionResults.NewRow();
                        drExcetpion["Target"] = targetRecord;
                        drExcetpion["Source"] = uwtdRecord;
                        drExcetpion["Output"] = resultMessage;
                        dtExceptionResults.Rows.Add(drExcetpion);
                    }

                    //nullify the current Individual ID's
                    Session["TargetID"] = null;
                    Session["UnwantedID"] = null;
                }
            }
            catch (Exception e)
            {
                if (dtOutputResult.Rows.Count > 0)
                {
                    if (dtExceptionResults.Rows.Count > 0)
                        Session["ExceptionDetails"] = dtExceptionResults;

                    pnlBulkMergeReports.Visible = true;

                    //bind the result to grid untill it is merged
                    gvBulkMergeResult.DataSource = dtOutputResult;
                    gvBulkMergeResult.DataBind();
                }
                lblErrorMessage.Text = "Bulk Merge Process stopped due to:</br>" + e.Message;
            }

            if (dtOutputResult.Rows.Count > 0)
            {
                // Assign Exception results to session
                Session["ExceptionDetails"] = dtExceptionResults;
                pnlBulkMergeReports.Visible = true;

                //bind Bulk Merge result to grid
                gvBulkMergeResult.DataSource = dtOutputResult;
                gvBulkMergeResult.DataBind();
            }
        }


        #endregion

        #region " Merge Rules for Individuals "

        /// <summary>
        /// Return the exceptions if found i.e. surname, forename DOB mismatch
        /// </summary>
        /// <param name="targetRefNo"></param>
        /// <param name="unWantedRefNo"></param>
        /// <returns></returns>
        private string MergeRules(string targetRefNo, string unWantedRefNo)
        {
            string strUnwantedSurname = "";
            string strTargetSurname = "";
            string strUnwantedFirstname = "";
            string strTargetFirstname = "";
            string strErrorMessage = string.Empty;

            // return message when Ref no contains more than 6 char. currently Individual RefNo restricted to 6 character, 
            //if it may vary infuture we need to change RefNo length here as well.
            if (targetRefNo.Length > 6 || unWantedRefNo.Length > 6)
                return strErrorMessage = "The Ref No must contain only 6 characters";

            if (targetRefNo != unWantedRefNo)
            {
                try
                {
                    BonoboDomainObjects.BnbIndividual objUnwantedIndividual = BnbIndividual.RetrieveByRefNo(unWantedRefNo);
                    if (objUnwantedIndividual != null)
                        Session["UnwantedID"] = objUnwantedIndividual.ID;
                    else
                        return strErrorMessage = "The Ref No " + unWantedRefNo + " is not available in the Database";

                    BonoboDomainObjects.BnbIndividual objTargetIndividual = BnbIndividual.RetrieveByRefNo(targetRefNo);

                    if (objTargetIndividual != null)
                        Session["TargetID"] = objTargetIndividual.ID;
                    else
                        return strErrorMessage = "The Ref No " + targetRefNo + " is not available in the Database";


                    if (objUnwantedIndividual.Surname != null)
                    {
                        if (objUnwantedIndividual.Surname.ToString().Length > 2)
                            strUnwantedSurname = objUnwantedIndividual.Surname.ToString().Substring(0, 3);
                    }
                    if (objTargetIndividual.Surname != null)
                    {
                        if (objTargetIndividual.Surname.ToString().Length > 2)
                            strTargetSurname = objTargetIndividual.Surname.ToString().Substring(0, 3);
                    }
                    if (objUnwantedIndividual.FirstName != null)
                        strUnwantedFirstname = objUnwantedIndividual.FirstName.ToString().Substring(0, 1);

                    if (objTargetIndividual.FirstName != null)
                        strTargetFirstname = objTargetIndividual.FirstName.ToString().Substring(0, 1);

                    if ((BnbRuleUtils.HasValue(objUnwantedIndividual.DateOfBirth.Date)) && (BnbRuleUtils.HasValue(objTargetIndividual.DateOfBirth.Date)))
                    {
                        if (objUnwantedIndividual.DateOfBirth.Date != objTargetIndividual.DateOfBirth.Date)
                        {
                            strErrorMessage = "The unwanted and target individuals must have the same date of birth";
                        }
                    }
                }
                catch (ApplicationException ae)
                {
                    strErrorMessage = ae.Message;
                }

                if (strUnwantedFirstname != strTargetFirstname)
                {
                    strErrorMessage += "<br>The unwanted and target individuals must have the same firstname first letter";
                }
                if (strUnwantedSurname != strTargetSurname)
                {
                    strErrorMessage += "<br>The unwanted and target individuals must have the same first three letters of their surname";
                }
            }
            else
                strErrorMessage = "Unable to Merge as Both Individual RefNo are same";

            return strErrorMessage;
        }

        #endregion

        #region " Collect Individual ID's to get the Error Record "

        /// <summary>
        /// Collect Unwanted details in temp table for identifying the validation exception which record caused the problem while merge.
        /// </summary>
        /// <param name="objIndividual"></param>
        private void GetAllReferenceIDFromUnwantedIndivdiual(BnbIndividual objIndividual)
        {
            BnbRelationshipInfoCollection lst = objIndividual.GetChildRelationships();
            string className;
            BnbDomainObjectList objectList = null; ;
            DataTable dt = new DataTable("tblUnwantedIndividual");
            DataRow dr = dt.NewRow();

            // collecting Individual & its child class IDs in temp table.
            dt.Columns.Add("IndividualID");
            dr["IndividualID"] = objIndividual.ID;

            if (objIndividual.Additional != null)
            {
                dt.Columns.Add("IndividualAdditionalID");
                dr["IndividualAdditionalID"] = objIndividual.Additional.ID;
            }

            bool contactCount = true;
            for (int i = 0; i < lst.Count; i++)
            {
                className = lst[i].RelatedClassName;

                switch (className)
                {
                    case ("BnbAccountHeader"):
                        objectList = objIndividual.AccountHeaders;
                        break;
                    case ("BnbAttendance"):
                        objectList = objIndividual.Attendances;
                        break;
                    case ("BnbRelative"):
                        objectList = objIndividual.Relatives;
                        break;
                    case ("BnbEventPersonnel"):
                        objectList = objIndividual.EventPersonnel;
                        break;
                    case ("BnbContactNumber"):
                        // check count of BnbContactNumber as we used twice in BnbIndividual & this should assign single time to list
                        if (contactCount)
                        {
                            objectList = objIndividual.ContactNumbers;
                            contactCount = false;
                        }
                        break;
                    case ("BnbContactDetail"):
                        objectList = objIndividual.ContactDetails;
                        break;
                    case ("BnbAddress"):
                        objectList = objIndividual.IndividualAddresses;
                        break;
                    case ("BnbApplication"):
                        objectList = objIndividual.Applications;
                        break;
                    case ("BnbIndividualAddress"):
                        objectList = objIndividual.IndividualAddresses;
                        break;
                    case ("BnbIndividualAddressDistribution"):
                        objectList = objIndividual.IndividualAddressDistributions;
                        break;
                    case ("BnbIndividualAllergy"):
                        objectList = objIndividual.IndividualAllergies;
                        break;
                    case ("BnbIndividualBenefit"):
                        objectList = objIndividual.IndividualBenefits;
                        break;
                    case ("BnbIndividualContactBlock"):
                        objectList = objIndividual.IndividualContactBlocks;
                        break;
                    case ("BnbIndividualMaritalStatus"):
                        objectList = objIndividual.IndividualMaritalStatuses;
                        break;
                    case ("BnbIndividualNationality"):
                        objectList = objIndividual.IndividualNationalities;
                        break;
                    case ("BnbIndividualQualification"):
                        objectList = objIndividual.IndividualQualifications;
                        break;
                    case ("BnbIndividualResource"):
                        objectList = objIndividual.IndividualResources;
                        break;
                    case ("BnbIndividualSkill"):
                        objectList = objIndividual.IndividualSkills;
                        break;
                    case ("BnbIndividualSubscription"):
                        objectList = objIndividual.IndividualSubscriptions;
                        break;
                    case ("BnbIndividualVaccination"):
                        objectList = objIndividual.IndividualVaccinations;
                        break;
                }
                if (objectList != null)
                {
                    for (int j = 0; j < objectList.Count; j++)
                    {
                        dt.Columns.Add(className + "_ID" + j);
                        dr[className + "_ID" + j] = objectList[j].ID;
                    }
                    objectList = null;
                }

            }
            // collecting Application and its child class IDs in temp table.
            int appIDs = 0;
            foreach (BnbApplication app in objIndividual.Applications)
            {
                for (int i = 0; i < app.GetChildRelationships().Count; i++)
                {
                    className = app.GetChildRelationships()[i].RelatedClassName;

                    switch (className)
                    {
                        case ("BnbPaperMove"):
                            objectList = app.PaperMoves;
                            break;
                        case ("BnbApplicationStatus"):
                            objectList = app.ApplicationStatuses;
                            break;
                        case ("BnbApplicationMedicalRequirement"):
                            objectList = app.ApplicationMedicalRequirements;
                            break;
                        case ("BnbFunding"):
                            objectList = app.Funding;
                            break;
                    }
                    if (objectList != null)
                    {
                        for (int j = 0; j < objectList.Count; j++)
                        {
                            dt.Columns.Add(className + "_ID" + appIDs + j);
                            dr[className + "_ID" + appIDs + j] = objectList[j].ID;
                        }
                        objectList = null;
                    }
                }
                appIDs++;
            }
            dt.Rows.Add(dr);
            Session["tblUnwanted"] = dt;

        }

        #endregion

        #region " Export Bulk Merge Reports to Excel "

        /// <summary>
        /// Export the Full Bulk Merge Report to Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbExportBulkMergeReport_OnClick(object sender, EventArgs e)
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=BulkMergeResults.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.xls";
            Response.Write("<style> .text {mso-number-format:/; } </style>");
            StringWriter sw = new StringWriter();
            HtmlTextWriter wr = new HtmlTextWriter(sw);
            if (gvBulkMergeResult.Visible)
                gvBulkMergeResult.RenderControl(wr);

            Response.Write(sw.ToString());
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that a Form control was rendered */
        }

        /// <summary>
        /// Export Bulk Merge Exception Reports only to Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbExportBulkMergeExceptionsReports_OnClick(object sender, EventArgs e)
        {
            DataTable dtException = new DataTable();
            dtException = (DataTable)Session["ExceptionDetails"];
            DataSet ds = new DataSet();
            ds.Tables.Add(dtException);

            HttpResponse response = HttpContext.Current.Response;

            // first let's clean up the response.object   
            response.Clear();
            response.Charset = "";

            // set the response mime type for excel   
            response.ContentType = "application/vnd.ms-excel";
            response.AddHeader("Content-Disposition", "attachment;filename=BulkMergeExceptions.xls");

            // create a string writer   
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    // instantiate a datagrid   
                    DataGrid dg = new DataGrid();
                    dg.DataSource = ds.Tables[0];

                    dg.DataBind();
                    dg.RenderControl(htw);
                    response.Write(sw.ToString());
                    // remove table from dataset
                    ds.Tables.Remove(dtException);
                    response.End();
                }
            }
        }

        #endregion

        #region " CSV Format Validation "

        private ArrayList GetDuplicateRecords()
        {
            string fileName = Path.GetFileName(fuMergeFile.PostedFile.FileName);
            string extension = Path.GetExtension(fileName);

            //the uploaded csv/excel file must have a column with the following headers:
            const string targetColumnHeader = "Target";
            const string unwantedColumnHeader = "Unwanted";

            //regular expression for spliting on comma, but not comma appearing between quotes
            //if parsing errors occur with a healthy csv file, the problem may be with the regular expression
            string regexPattern = ",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))";
            ArrayList csv = new ArrayList();
            try
            {
                if (extension.ToString().ToLower().Trim() != ".csv")
                    throw new CSVFormatException("A CSV file with column headers 'Target' followed by 'Unwanted' must be uploaded");

                try
                {
                    using (Stream stream = fuMergeFile.PostedFile.InputStream)
                    using (StreamReader sr = new StreamReader(stream))
                    {
                        Regex regex = new Regex(regexPattern);
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            //split the line on comma (ignoring certain commas - defined in regexPattern)
                            string[] splitLine = regex.Split(line);
                            //add this line as an array to csv, which will eventually 
                            //contain a parsed version of the complete file.
                            //trim double quotes that may be surrounding individual fields
                            csv.Add(BnbGeneralUtilities.TrimStringArrayItems(splitLine, "\"", "\""));
                        }
                    }
                }
                catch (IOException ex)
                {
                    lblErrorMessage.Text = ex.Message;
                    return null;
                }

                //location of target column
                int targetRefColumn = BnbGeneralUtilities.GetIndexOfArrayItem((string[])csv[0], targetColumnHeader);

                //location of unwanted column
                int unwantedRefColumn = BnbGeneralUtilities.GetIndexOfArrayItem((string[])csv[0], unwantedColumnHeader);
                if (targetRefColumn < 0)
                    throw new CSVFormatException("A CSV file with column headers 'Target' followed by 'Unwanted' must be uploaded");
                else
                    if (((string[])csv[0])[0].ToString().ToLower().Trim() != targetColumnHeader.ToString().ToLower().Trim())
                        throw new CSVFormatException("A CSV file with column headers 'Target' followed by 'Unwanted' must be uploaded");

                if (unwantedRefColumn < 0)
                    throw new CSVFormatException("A CSV file with column headers 'Target' followed by 'Unwanted' must be uploaded");
                else
                    if (((string[])csv[0])[1].ToString().ToLower().Trim() != unwantedColumnHeader.ToString().ToLower().Trim())
                        throw new CSVFormatException("A CSV file with column headers 'Target' followed by 'Unwanted' must be uploaded");


                //csv has no rows
                if (csv.Count < 1)
                    throw new CSVFormatException("The uploaded CSV file is empty.");
                else
                {
                    int EmptyrowsCount = 0;
                    int validDataCount = 0;
                    for (int i = 0; i < csv.Count; i++)
                    {
                        if (((string[])csv[i])[0].ToString().Trim() == string.Empty && ((string[])csv[i])[1].ToString().Trim() == string.Empty)
                            EmptyrowsCount++;
                    }

                    validDataCount = csv.Count - EmptyrowsCount;
                    //first line of the csv file includes the header count also, so we need to neglate header 
                    if (((validDataCount - 1) > 1000))
                        throw new CSVFormatException("The uploaded CSV file exceeds the limit of 1000 records");
                    //first line of the csv file includes the header count also, so we need to neglate header 
                    if (EmptyrowsCount == (csv.Count - 1))
                        throw new CSVFormatException("The uploaded CSV file is empty");
                }

                return csv;
            }
            catch (CSVFormatException ex)
            {
                lblErrorMessage.Text = ex.Message;
                return null;
            }
        }
        #endregion
    }


}
