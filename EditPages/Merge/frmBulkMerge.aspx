<%@ Page Language="C#" AutoEventWireup="true" Codebehind="frmBulkMerge.aspx.cs" Inherits="Frond.EditPages.Merge.frmBulkMerge" %>

<%@ Register TagName="Header" Src="~/UserControls/MergeHeader.ascx" TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>frmBulkMerge</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../../Css/sfStyles.css" type="text/css" rel="stylesheet" />
    <link href="../../Css/FrondDefault.css" type="text/css" rel="stylesheet" />
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>

    <script language="javascript" type="text/javascript">
        function doHourglass()
        {
        document.body.style.cursor = 'wait';
        }		
    </script>

</head>
<body>
    <form id="form1" method="post" runat="server">
        <table class="scaffold">
            <tr>
                <td>
                    <uc:Header ID="Header1" runat="server"></uc:Header>
                </td>
                <td class="maincell">
                    <table class="titlebartable">
                        <tr>
                            <td width="100%" style="height: 29px">
                                <asp:Label ID="lblTitle" Text="Bulk Merge" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                    <p>
                        <asp:Label ID="lblErrorMessage" CssClass="errorMessage" ForeColor="red" runat="server"></asp:Label></p>
                    <p>
                        <b>Instructions </b>
                    </p>
                    <br />
                    <ul>
                        <li><b>Step 1&nbsp;-&nbsp;</b>Click on browse and upload the CSV file containing the
                            duplicate data. Ensure the Target and Unwanted columns have been marked correctly.
                            <br />
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Target
                            - Individual record to retain, data from the Unwanted will be copied to this record<br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Unwanted
                            - Individual record to moved.
                            <br />
                            <br />
                        </li>
                        <li><b>Step 2&nbsp;-&nbsp;</b>You will only be able to merge the individuals if:
                            <br />
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a
                            - the selected individuals have the same date of birth.
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b
                            - first character of their firstname is identical.
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c
                            - first 3 characters of their surname are identical.</li>
                        <br />
                        <br />
                        <li><b>Step 3&nbsp;-&nbsp;</b>If the above conditions are fulfilled for all the records
                            in the bulk merge and you want to merge the individuals, click on 'Merge All'. </li>
                    </ul>
                    <br />
                    <center>
                        <table class="fieldtable">
                            <tr>
                                <td class="label">
                                    Upload CSV file with duplicate data</td>
                                <td>
                                    <asp:FileUpload ID="fuMergeFile" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Button ID="btnMergeAll" Font-Bold="true" runat="server" OnClick="btnMergeAll_Click"
                                        Text="Merge All" /></td>
                            </tr>
                        </table>
                    </center>
                    <br />
                    <table>
                    </table>
                    <asp:Panel ID="pnlBulkMergeReports" Visible="false" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <asp:LinkButton ID="lbGetExcel" runat="server" OnClick="lbExportBulkMergeReport_OnClick">ExportFullBulkMergeReport</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:LinkButton ID="lbGetExceptionReport" runat="server" OnClick="lbExportBulkMergeExceptionsReports_OnClick">ExportExceptionOnly</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table>
                            <tr>
                                <td colspan="2">
                                    <asp:GridView ID="gvBulkMergeResult" AutoGenerateColumns="false" CssClass="datagrid"
                                        runat="server">
                                        <Columns>
                                            <asp:BoundField HeaderText="Target" DataField="Target" />
                                            <asp:BoundField HeaderText="Source" DataField="Source" />
                                            <asp:BoundField HeaderText="Output" HtmlEncode="false" ItemStyle-Wrap="true" DataField="Output" />
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
