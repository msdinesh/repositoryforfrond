using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboEngine;
using BonoboDomainObjects;
using BonoboEngine.Query;



namespace BonoboMerge
{
	/// <summary>
	/// Summary description for frmMergeIndividual.
	/// </summary>
	public partial class frmMergeIndividual : System.Web.UI.Page
	{
		#region controls declaration
		protected System.Web.UI.WebControls.Label lblIndividualType;
		protected System.Web.UI.WebControls.Button cmdMerge;

		#endregion controls declaration

		#region page_load
		protected void Page_Load(object sender, System.EventArgs e)
		{
			BnbEngine.SessionManager.ClearObjectCache(); 
			
			if ((Session["LoggedIn"] == null || (!(bool)Session["LoggedIn"])))
				Response.Redirect("Login.aspx");
			
			//Check if a target or an unwanted individual have been selected and if so then 
			// Put user code to initialize the page here
			// check if the sessions have been set
			lblTitle.Text = "Merge home";
			if (!this.IsPostBack)
				BnbcmdMerge.Attributes.Add("onclick","javascript:showLoadingMessage('Merging...');");
	
			string strMergeTargetID = Session["MergeTargetID"].ToString();
			string strMergeUnwantedID=Session["MergeUnwantedID"].ToString();

			//================================DEBUG CODE to remove======================
//			Session["MergeTarget"]= "1";
//			Session["MergeUnwanted"]= "1";
//			Session["MergeTargetID"] = "13778084-55BA-4336-BBB6-F4B580F56C5F";
//			Session["MergeUnwantedID"] = "EECDF740-6448-470D-002C-AFDBF1ADEEAD";
			//================================END DEBUG CODE=======================================================================

			if (Session["MergeTarget"].ToString() == "1")
			{
				if (strMergeTargetID.Length==0)
				{
					if (Request.QueryString["BnbIndividual"] != null)
					{
						Session["MergeTargetID"] = Request.QueryString["BnbIndividual"];
						strMergeTargetID = Session["MergeTargetID"].ToString();
						DisplayIndividualDetails(strMergeTargetID,lblTargetDetails);
					}
					else
						Session["MergeTarget"] = 0;
				}
				else
					DisplayIndividualDetails(strMergeTargetID,lblTargetDetails);

			}

			if (Session["MergeUnwanted"].ToString() == "1")
			{
				if (strMergeUnwantedID.Length ==0)
				{
					if (Request.QueryString["BnbIndividual"] != null)
					{
						Session["MergeUnwantedID"] = Request.QueryString["BnbIndividual"];
						strMergeUnwantedID = Session["MergeUnwantedID"].ToString();
						DisplayIndividualDetails(strMergeUnwantedID,lblUnwantedDetails);
					}
					else
						Session["MergeUnwanted"] = 0;

				}
				else
					DisplayIndividualDetails(strMergeUnwantedID,lblUnwantedDetails);

			}

			if ((Session["MergeTarget"].ToString() == "1") && (Session["MergeUnwanted"].ToString() == "1"))
			{
				CheckApplicationStatus(Session["MergeUnwantedID"].ToString(),Session["MergeTargetID"].ToString());
				//Merge rules: 1 check that the birth dates are the same and surname first left 3 characters are identical
				if (MergeRules(Session["MergeUnwantedID"].ToString(),Session["MergeTargetID"].ToString()))
				{
					BnbcmdMerge.Visible = true;
				}
			}
			else
				BnbcmdMerge.Visible = false;
		}
		#endregion page_load

		#region private methods

		
		/// <summary>
		/// Private method. Given the unwanted and target individual IDs as string, retrieves their application status.
		/// if the application status is not of type enquirer or applicant display a warning message.
		/// </summary>
		/// <param name="UnwantedGUID">string</param>
		/// <param name="TargetGUID">string</param>
		private void CheckApplicationStatus(string UnwantedGUID, string TargetGUID)
		{
			string strUnwantedIndvApplicationStatus = "";
			string strMergeIndvApplicationStatus = "";
			string strErrorMessage = lblErrorMessage.Text + "<br>";

			//get the application status and if they are different then display a message.
			BnbListCondition lc1 = new BnbListCondition("tblIndividualKeyInfo_IndividualID", new Guid(TargetGUID));
			BnbListCondition lc2 = new BnbListCondition("tblIndividualKeyInfo_IndividualID", new Guid(UnwantedGUID));
			
			BnbQuery qryMergeIndv = new BnbQuery("vwBonoboFindIndividualGC", new BnbCriteria(lc1));
			BnbQuery qryUnwantedIndv = new BnbQuery("vwBonoboFindIndividualGC", new BnbCriteria(lc2));
			
			DataTable dtbResultsMerge = qryMergeIndv.Execute().Tables["Results"];
			if (dtbResultsMerge.Rows.Count > 0)
			{
				strMergeIndvApplicationStatus = dtbResultsMerge.Rows[0]["Custom_ApplicationStatus"].ToString();
				lblTApplicationStatus.Text = strMergeIndvApplicationStatus;
			}
			dtbResultsMerge.Clear();

			DataTable dtbResultsUnwanted = qryUnwantedIndv.Execute().Tables["Results"];
			if (dtbResultsUnwanted.Rows.Count > 0)
			{
				strUnwantedIndvApplicationStatus = dtbResultsUnwanted.Rows[0]["Custom_ApplicationStatus"].ToString();
				lblUApplicationStatus.Text = strUnwantedIndvApplicationStatus;
			}	
			dtbResultsUnwanted.Clear();
			if ((strUnwantedIndvApplicationStatus.IndexOf("Dire") > 0) || (strMergeIndvApplicationStatus.IndexOf("Dire") > 0))
				lblErrorMessage.Text = strErrorMessage + "One application has a Directorate/Special status";

			if ((strUnwantedIndvApplicationStatus.IndexOf("Enquirer") == -1) && (strUnwantedIndvApplicationStatus.IndexOf("Applicant") == -1))
				lblErrorMessage.Text = strErrorMessage + "Merge works best when Unwanted record is Enq or App";
		}

		/// <summary>
		/// Private method returning a boolean. Arguments: unwanted individual ID as string, target individual id as string. 
		/// Checks that the 2 individuals have same birthdate, first 3 characters of
		/// their surname is identical as well as the first character of their firstname. If one or all conditions not met then
		/// display error message.
		/// </summary>
		/// <param name="UnwantedGUID">string</param>
		/// <param name="TargetGUID">string</param>
		/// <returns>Boolean</returns>
		
		private bool MergeRules(string UnwantedGUID, string TargetGUID)
		{
			bool blnResult = false;
			string strUnwantedSurname = "";
			string strTargetSurname = "";
			string strUnwantedFirstname = "";
			string strTargetFirstname = "";
			Guid UnwantedID = new Guid(UnwantedGUID);
			Guid TargetID = new Guid(TargetGUID);
			string strErrorMessage = "";
            bool blnDOBCheck = false;

			if (UnwantedGUID != TargetGUID)
			{
				BonoboDomainObjects.BnbIndividual objUnwantedIndividual = BnbIndividual.Retrieve(UnwantedID);
				BonoboDomainObjects.BnbIndividual objTargetIndividual = BnbIndividual.Retrieve(TargetID);
				if (objUnwantedIndividual.Surname != null)
				{
					if (objUnwantedIndividual.Surname.ToString().Length > 2)
						strUnwantedSurname = objUnwantedIndividual.Surname.ToString().Substring(0,3);
				}
				if (objTargetIndividual.Surname != null)
				{
					if (objTargetIndividual.Surname.ToString().Length > 2)
						strTargetSurname = objTargetIndividual.Surname.ToString().Substring(0,3);
				}
				if (objUnwantedIndividual.FirstName != null)
					strUnwantedFirstname = objUnwantedIndividual.FirstName.ToString().Substring(0,1);
				
				if (objTargetIndividual.FirstName != null)
					strTargetFirstname = objTargetIndividual.FirstName.ToString().Substring(0,1);

                if ((BnbRuleUtils.HasValue(objUnwantedIndividual.DateOfBirth.Date)) && (BnbRuleUtils.HasValue(objTargetIndividual.DateOfBirth.Date)))
                {
                    if (objUnwantedIndividual.DateOfBirth.Date != objTargetIndividual.DateOfBirth.Date)
                    {
                        strErrorMessage = "The unwanted and target individuals must have the same date of birth.  Please update their date of birth if you still want to merge them.";
                    }
                }
				if (strUnwantedFirstname != strTargetFirstname)
				{
					strErrorMessage += "<br>The unwanted and target individuals must have the same firstname first letter. Please update their firstname if you still want to merge them.";
				}
				if (strUnwantedSurname == strTargetSurname)
					blnResult=true;
				else
				{
					strErrorMessage += "<br>The unwanted and target individuals must have the same first three letters of their surname. Please update their surname if you still want to merge them.";
				}

                // if both DOB is different then stop the merge process.
                // If blnDOBChek is 'true' it means that Target and unwanted DOB is diffrent.
                if (blnDOBCheck == true)
                    blnResult = false;

				lblErrorMessage.Text = strErrorMessage ;
				return blnResult;
			}
			else
			{
				lblErrorMessage.Text = "The unwanted and target individuals should be different.";
				return blnResult;
			}
		}

		/// <summary>
		/// Private method. Given an individualId and a page label, display the individual refNo and fullname in the 
		/// given label.
		/// </summary>
		/// <param name="IndividualGUID">string</param>
		/// <param name="LabelName">Label control</param>
		private void DisplayIndividualDetails(string IndividualGUID, Label LabelName)
		{
			Guid guiID = new Guid(IndividualGUID);

			BonoboDomainObjects.BnbIndividual objIndividual = BnbIndividual.Retrieve(guiID);
			LabelName.Text = objIndividual.RefNo.ToString() + "&nbsp;&nbsp;" + objIndividual.FullName.ToString();
		}
		
			
		#endregion private methods

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region events
		protected void BnbcmdTarget_Click(object sender, System.EventArgs e)
		{
			//redirect to the find contact form and pass it an ID of 1 for a target
			Session["MergeTarget"] = 1;
			Session["MergeTargetID"] = "";
			Response.Redirect("frmFindContact.aspx?Type=1");
		}

		protected void BnbcmdUnwanted_Click(object sender, System.EventArgs e)
		{
			//redirect to the find contact form and pass it an ID of 2 for a target
			Session["MergeUnwanted"]=1;
			Session["MergeUnwantedID"] = "";
			Response.Redirect("frmFindContact.aspx?Type=2");
		}

		protected void BnbcmdMerge_Click(object sender, System.EventArgs e)
		{
			string strUnwantedID = Session["MergeUnwantedID"].ToString();
			bool blnSuccess = true; 
			string strTargetID = Session["MergeTargetID"].ToString();
			string strErrorMessage = "The merge has not taken place due to the following problems: <br>";

//			GC.Collect();
//			GC.WaitForPendingFinalizers();
			BnbcmdMerge.Visible = false;

			//reset session variables.
			Session["MergeTargetID"] = "";
			Session["MergeUnwantedID"] = "";
			Session["MergeTarget"] = 0;
			Session["MergeUnwanted"]=0;

			try
			{ 
				BonoboDomainObjects.BnbIndividualMerge objMerge = new BnbIndividualMerge(true); 
				objMerge.MergeIndividuals(strUnwantedID,strTargetID); 
			} 
			catch (BnbProblemException pe) 
			{ 
				
				blnSuccess = false; 
				BonoboEngine.BnbLockManager objLockMger = new BnbLockManager();
				objLockMger.ClearAllLocks();
				
				
				lblErrorMessage.Text = ""; 
				foreach(BnbProblem prob in pe.Problems) 
					strErrorMessage += prob.Message + "<br>";
				lblErrorMessage.Text = strErrorMessage + "<br>"; 
				lblTargetDetails.Text = "";
			} 

			if (blnSuccess) 
			{
				lblErrorMessage.Text = "The merge is now completed"; 
				DisplayIndividualDetails(strTargetID,lblTargetDetails);
			}
			
			//reset all labels
			lblTApplicationStatus.Text = "";
			lblUApplicationStatus.Text = "";
			lblUnwantedDetails.Text = "";

			
		}
		#endregion events
}

	}

