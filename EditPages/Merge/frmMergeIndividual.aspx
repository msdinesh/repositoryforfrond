<%@ Register TagPrefix="uc1" TagName="Header" Src="~/UserControls/MergeHeader.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="frmMergeIndividual.aspx.cs" AutoEventWireup="True" Inherits="BonoboMerge.frmMergeIndividual" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>frmMergeIndividual</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../Css/sfStyles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
			function showLoadingMessage(message) 
			{
				document.Form1.style.visibility = 'hidden';
				document.body.style.cursor = 'wait';
				loadingmessage.innerHTML = message;
				loadingmessage.style.display = 'block';
			}

		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table class="scaffold">
				<tr>
					<td><uc1:header id="Header1" runat="server"></uc1:header></td>
					<td class="maincell">
						<P><asp:label id="lblLoginResult" runat="server" ForeColor="Red" Visible="False" CssClass="errorMessage">Label</asp:label>
							<table class="titlebartable">
								<tr>
									<td class="titleicon" style="height: 29px"><asp:label id="lblImage" runat="server"></asp:label></td>
									<td width="100%" style="height: 29px"><asp:label id="lblTitle" runat="server"></asp:label></td>
									<td style="height: 29px"></td>
								</tr>
							</table>
						</P>
						<!-- TPT: Added Hyperlink to navigate to Frond Menu -->
						<p><asp:HyperLink ID="lnkMainMenu" CssClass="link" runat="server" NavigateUrl="~/Menu.aspx" Text="<< Back to Frond menu" ForeColor="#C000C0"></asp:HyperLink></p>
						<p><b>Instructions </b>
							<br>
							<ul>
								<li>
									<b>Step 1.</b>Click on 'Target' and select the duplicate individual you 
								want to keep. After the merge, all the details of the Target individual will 
								still be on the system.
								<li>
									<b>Step 2.</b>
								Click on 'Unwanted' and select the duplicate individual you want to remove. 
								When their details have been merged into the Target individual's record they 
								will be removed from the system
								<li>
									<b>Step 3.</b> You will only be able to merge the individuals if:<br>
									&nbsp;&nbsp;&nbsp;a - the selected individuals have the same date of birth.
									<br>
									&nbsp;&nbsp;&nbsp;b - first character of their firstname is identical.
									<br>
								&nbsp;&nbsp;&nbsp;c - first 3 characters of their surname are identical.
								<li>
									<b>Step 4.</b> If the above conditions are fullfilled and you want to merge the 
									individuals, click on 'Merge'.
								</li>
							</ul>
							<br>
							<asp:label id="lblErrorMessage" runat="server" ForeColor="Red" CssClass="errorMessage"></asp:label>
							<table cellPadding="2" width="500">
								<TBODY>
									<tr>
										<td style="HEIGHT: 22px"><bnbgenericcontrols:bnbbutton id="BnbcmdTarget" runat="server" Width="90px" Text="Target" onclick="BnbcmdTarget_Click"></bnbgenericcontrols:bnbbutton></td>
										<td style="HEIGHT: 22px">&nbsp;
											<asp:label id="lblTargetDetails" runat="server"></asp:label>&nbsp;<asp:label id="lblTApplicationStatus" runat="server"></asp:label></td>
									</tr>
									<tr>
										<td><bnbgenericcontrols:bnbbutton id="BnbcmdUnwanted" runat="server" Width="90px" Text="Unwanted" onclick="BnbcmdUnwanted_Click"></bnbgenericcontrols:bnbbutton></td>
										<td>&nbsp;
											<asp:label id="lblUnwantedDetails" runat="server"></asp:label>&nbsp;<asp:label id="lblUApplicationStatus" runat="server"></asp:label></td>
					
				</tr>
				<tr>
					<td align="right" colSpan="2"><bnbgenericcontrols:bnbbutton id="BnbcmdMerge" runat="server" Visible="False" Width="90px" Text="Merge" onclick="BnbcmdMerge_Click"></bnbgenericcontrols:bnbbutton></td>
				</tr>
				</TBODY>
			</table>			
			  </table></form>
		<div class="loadingmessage" id="loadingmessage"></div>
		<script language="JavaScript">
			loadingmessage.style.position = 'absolute';
			loadingmessage.style.posTop = document.body.scrollHeight/5;
			loadingmessage.style.posLeft = document.body.scrollWidth/2;
			loadingmessage.style.display = 'none';
			document.Form1.style.visibility = 'visible';
			document.body.style.cursor = 'default';
		</script>
	</body>
</HTML>
