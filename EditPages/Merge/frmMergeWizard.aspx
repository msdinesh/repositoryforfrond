<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Merge.Master" Codebehind="frmMergeWizard.aspx.cs"
    Inherits="Contact.frmMergeWizard" %>

<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="~/UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbcriteriacontrol" Namespace="BonoboWebControls.CriteriaControls"
    Assembly="BonoboWebControls" %>
<%@ MasterType TypeName="Frond.Merge" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="holder1" runat="server">
    <div class="wizardheader">
        <h3>
            <img class="icon24" src="../../Images/wizard24silver.gif">
            Individual Merge Wizard &nbsp;&nbsp;
            <asp:Label ID="lblScreenText" runat="server"></asp:Label></h3>
    </div>
    <asp:Panel ID="panelOne" runat="server" CssClass="wizardpanel">
        <br />
        <b>Step 1 - Instructions</b><br />
        <ul>
            <li>Search and select a duplicate individual you want to keep <b>(Target)</b>. After
                the merge, all the details of the Target individual will still be on the system.<br />
                <br />
            </li>
            <li>1. Enter your individual search criteria.<br />
                <br />
            </li>
            <li>2. Choose a target individual you want to merge.</li>
        </ul>
        <br />
        <%--        <asp:Button ID="btnTargetIndv" runat="server" Text="Target" OnClick="btnTargetIndv_Click" />
--%>
        <asp:Panel ID="pnlTargetIndv" runat="server" Visible="true">
            <p align="center">
                <asp:DropDownList ID="cboFindTypeTgt" runat="server" Visible="False">
                </asp:DropDownList></p>
            <p align="left">
                <bnbcriteriacontrol:BnbCriteriaBuilderComposite ID="BnbCriteriaBuilderCompositeTgt"
                    runat="server" MetaDataXmlUrl="Xml\BonoboFindMetaDataTarget.xml">
                </bnbcriteriacontrol:BnbCriteriaBuilderComposite>
            </p>
            <p style="text-align: left; padding-left: 110px">
                <asp:Button ID="cmdFindTgt" runat="server" CssClass="button" Text="Find" OnClick="cmdFindTgt_Click">
                </asp:Button>&nbsp;</p>
            <p>
                <asp:Label ID="lblMessagesTgt" runat="server" CssClass="messagebox"></asp:Label></p>
            <p>
                <asp:Label ID="lblPanelOneFeedback" runat="server" CssClass="feedback"></asp:Label></p>
            <asp:Label ID="lblRecResultTgt" runat="server"></asp:Label>
            <p align="center">
                <asp:GridView ID="gvTarget" runat="server" CssClass="datagrid" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <asp:RadioButton ID="rbListTarget" runat="server" OnCheckedChanged="rbListTarget_CheckedChanged"
                                    AutoPostBack="true" GroupName="single" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="t_tblIndividualKeyInfo_Surname" HeaderText="Surname" />
                        <asp:BoundField DataField="t_tblIndividualKeyInfo_Forename" HeaderText="Forename" />
                        <asp:BoundField DataField="t_tblIndivdualKeyInfo_SurnamePrefix" HeaderText="Surname Prefix" />
                        <asp:BoundField DataField="t_tblIndividualKeyInfo_old_indv_id" HeaderText="Ref No" />
                        <asp:BoundField DataField="t_Custom_ApplicationStatus" HeaderText="Status" />
                        <asp:BoundField DataField="t_tblIndividualAdditional_PreviousSurname" HeaderText="Previous Surname" />
                        <asp:BoundField DataField="t_tblAddress_AddressLine1" HeaderText="Address Line 1" />
                        <asp:BoundField DataField="t_tblIndividualKeyInfo_DateOfBirth" DataFormatString="{0:d}"
                            HeaderText="Date Of Birth" />
                        <asp:BoundField DataField="t_tblIndividualKeyInfo_Gender" HeaderText="Sex" />
                        <asp:BoundField DataField="t_tblIndividualKeyInfo_Title" HeaderText="Title" />
                        <asp:BoundField DataField="t_lkuCountry_Description" HeaderText="Country" />
                    </Columns>
                </asp:GridView>
            </p>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="panelTwo" runat="server" CssClass="wizardpanel">
        <br />
        <b>Step 2 - Instructions</b><br />
        <ul>
            <li>Search and select a duplicate individual you want to remove <b>(Unwanted)</b>. When
                this <b>Unwanted</b> individual's details have been merged into the <b>Target</b>
                individual's record this <b>Unwanted</b> individual's details will be removed from
                the system<br />
                <br />
            </li>
            <li>1. Enter your individual search criteria.<br />
                <br />
            </li>
            <li>2. Choose an unwanted individual you want to merge.</li>
        </ul>
        <br />
        <p align="center">
            <asp:DropDownList ID="cboFindTypeUwtd" runat="server" Visible="False">
            </asp:DropDownList></p>
        <p align="left">
            <bnbcriteriacontrol:BnbCriteriaBuilderComposite ID="BnbCriteriaBuilderCompositeUwtd"
                runat="server" MetaDataXmlUrl="Xml\BonoboFindMetaDataUnwanted.xml">
            </bnbcriteriacontrol:BnbCriteriaBuilderComposite>
        </p>
        <p style="text-align: left; padding-left: 110px">
            <asp:Button ID="cmdFindUwtd" runat="server" CssClass="button" Text="Find" OnClick="cmdFindUwtd_Click">
            </asp:Button>&nbsp;</p>
        <p>
            <asp:Label ID="lblMessagesUwtd" runat="server" CssClass="messagebox"></asp:Label></p>
        <p>
            <asp:Label ID="lblPanelTwoFeedback" runat="server" CssClass="feedback"></asp:Label>
        </p>
        <asp:Label ID="lblRecResultUnwtd" runat="server"></asp:Label>
        <p align="center">
            <asp:GridView ID="gvUnwanted" runat="server" CssClass="datagrid" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:RadioButton ID="rbListUnwanted" runat="server" OnCheckedChanged="rbListUnwanted_CheckedChanged"
                                AutoPostBack="true" GroupName="single" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="u_tblIndividualKeyInfo_Surname" HeaderText="Surname" />
                    <asp:BoundField DataField="u_tblIndividualKeyInfo_Forename" HeaderText="Forename" />
                    <asp:BoundField DataField="u_tblIndivdualKeyInfo_SurnamePrefix" HeaderText="Surname Prefix" />
                    <asp:BoundField DataField="u_tblIndividualKeyInfo_old_indv_id" HeaderText="Ref No" />
                    <asp:BoundField DataField="u_Custom_ApplicationStatus" HeaderText="Status" />
                    <asp:BoundField DataField="u_tblIndividualAdditional_PreviousSurname" HeaderText="Previous Surname" />
                    <asp:BoundField DataField="u_tblAddress_AddressLine1" HeaderText="Address Line 1" />
                    <asp:BoundField DataField="u_tblIndividualKeyInfo_DateOfBirth" DataFormatString="{0:d}"
                        HeaderText="Date Of Birth" />
                    <asp:BoundField DataField="u_tblIndividualKeyInfo_Gender" HeaderText="Sex" />
                    <asp:BoundField DataField="u_tblIndividualKeyInfo_Title" HeaderText="Title" />
                    <asp:BoundField DataField="u_lkuCountry_Description" HeaderText="Country" />
                </Columns>
            </asp:GridView>
        </p>
    </asp:Panel>
    <asp:Panel ID="panelThree" runat="server" CssClass="wizardpanel">
    <br />
        <b>Step 3 - Instructions</b><br />    
        <ul>
        <li>You will only be able to merge the individuals if:<br>
            &nbsp;&nbsp;&nbsp;a - the selected individuals have the same date of birth.
            <br>
            &nbsp;&nbsp;&nbsp;b - first character of their firstname is identical.
            <br>
            &nbsp;&nbsp;&nbsp;c - first 3 characters of their surname are identical.<br />
            <br />
            </li>
        </ul>
            <asp:Label ID="lblErrorMessage" runat="server" ForeColor="Red" CssClass="errorMessage"></asp:Label>
            <br />
            <br />
            <p>
                <asp:Label ID="lblTargetHeader" Width="150px" runat="server" Text="Target Individual: "
                    Font-Bold="true"></asp:Label>
                <asp:Label ID="lblTargetDetails" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblTApplicationStatus"
                    runat="server"></asp:Label>
            </p>
            <p>
                <asp:Label ID="lblUnwantedHeader" Width="150px" runat="server" Text="Unwanted Individual: "
                    Font-Bold="true"></asp:Label>
                <asp:Label ID="lblUnwantedDetails" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblUApplicationStatus"
                    runat="server"></asp:Label>
            </p>
            <br />
            <bnbgenericcontrols:BnbButton ID="BnbRefresh" runat="server" Text="Refresh" OnClick="BnbRefresh_Click" />
    </asp:Panel>
    <br />
    <p>
        <uc1:WizardButtons ID="wizardButtons" runat="server"></uc1:WizardButtons>
    </p>
    <p>
        <asp:Label ID="lblHiddenReferrer" runat="server" Visible="False"></asp:Label>
    </p>
</asp:Content>
