<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="~/UserControls/MergeHeader.ascx" %>
<%@ Page language="c#" Codebehind="frmFindContact.aspx.cs" AutoEventWireup="True" Inherits="BonoboMerge.frmFindContact" %>
<%@ Register TagPrefix="bnbcriteriacontrol" Namespace="BonoboWebControls.CriteriaControls" Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>FindGeneral</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../Css/sfStyles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table class="scaffold">
				<tr>
					<td><uc1:header id="Header1" runat="server"></uc1:header></td>
					<td class="maincell">
						<P></P>
						<P><asp:label id="lblLoginResult" runat="server" CssClass="errorMessage" Visible="False" ForeColor="Red">Label</asp:label>
							<table class="titlebartable">
								<tr>
									<td class="titleicon"><asp:Label id="lblImage" runat="server"></asp:Label></td>
									<td width="100%">
										<asp:Label id="lblTitle" runat="server"></asp:Label></td>
									<td></td>
								</tr>
							</table>
						</P>
						<p><h3><b><asp:Label id="lblIndividualType" runat="server"></asp:Label></b></h3>
						</p>
						<p>
							<b>Instructions</b><br>
							<ul>
								<li>
								1. Enter your individual search criteria.
								<li>
									2. Click on 'View' to select an individual you want to merge.</li>
							</ul>
							<br>
						<P></P>
						<!--<P align="center">Get contact by VA Vol Ref
							<asp:textbox id="txtVolRef" runat="server"></asp:textbox><asp:button id="btnGetVol" runat="server" Text="Go"></asp:button>&nbsp;
						-->
						<asp:label id="lblMessage" runat="server" ForeColor="Red"></asp:label>
						<P></P>
						<P align="center"><asp:dropdownlist id="cboFindType" runat="server" Visible="False"></asp:dropdownlist></P>
						<P align="center"><bnbcriteriacontrol:bnbcriteriabuildercomposite id="BnbCriteriaBuilderComposite1" runat="server" MetaDataXmlUrl="Xml\BonoboFindMetaData.xml"></bnbcriteriacontrol:bnbcriteriabuildercomposite></P>
						<P align="center"><asp:button id="cmdFind" runat="server" CssClass="button" Text="Find" onclick="cmdFind_Click"></asp:button>&nbsp;</P>
						<P align="center"><asp:label id="lblMessages" runat="server" CssClass="messagebox"></asp:label></P>
						<P align="center"><bnbdatagrid:bnbdatagridforquerydataset id="BnbDataGridForQueryDataSet1" runat="server" CssClass="datagrid" MetaDataXmlUrl="Xml\BonoboFindMetaData.xml"></bnbdatagrid:bnbdatagridforquerydataset></P>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
