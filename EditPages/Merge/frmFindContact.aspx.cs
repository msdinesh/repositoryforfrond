using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboDomainObjects;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboWebControls;
using System.Text;

namespace BonoboMerge
{
	/// <summary>
	/// Summary description for TestFindPage2.
	/// </summary>
	public partial class frmFindContact: System.Web.UI.Page
	{
		#region page controls declaration
		#endregion page controls declaration


		#region Page_Load
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["LoggedIn"] == null || (!(bool)Session["LoggedIn"])))
				Response.Redirect("Login.aspx");
			
			string strIndividualType = Request.QueryString["Type"];
			
			lblMessages.Text = "";
			if (strIndividualType == "1")
				lblIndividualType.Text = "Find a target individual";
			if (strIndividualType == "2")
				lblIndividualType.Text = "Find an unwanted individual";

			lblTitle.Text = "Merge";

			if(!Page.IsStartupScriptRegistered("ForceDefaultToScript")) Page.RegisterStartupScript("ForceDefaultToScript",getKeyPressedScript());
			if (!this.IsPostBack)
			{
				string findid = HttpContext.Current.Request.QueryString["FindTypeID"]+"";
				cboFindType.AutoPostBack = true;
				// get the datasource for this control from the xml file
				cboFindType.DataSource = this.GetFindMetaData();
				cboFindType.DataMember = "FindOption";
				cboFindType.DataTextField = "Name";
				cboFindType.DataValueField = "ID";
				cboFindType.SelectedValue = (findid=="") ? "0" : findid;
				cboFindType.DataBind();
			}

			BnbCriteriaBuilderComposite1.FindTypeID = int.Parse(cboFindType.SelectedValue);
		}

		#endregion Page_load

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private methods

		/// <summary>
		/// Private method. Returns string containing javascript function.
		/// </summary>
		/// <returns>string</returns>
		private string getKeyPressedScript() 
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("<SCRIPT language=\"javascript\">" + Environment.NewLine);
			sb.Append("function fnTrapKeyPress(btn, event){" + Environment.NewLine);
			sb.Append("   if (event.keyCode == 13) {" + Environment.NewLine);
			sb.Append("      event.returnValue=false;" + Environment.NewLine);
			sb.Append("      event.cancel = true;" + Environment.NewLine);
			sb.Append("      btn.click();" + Environment.NewLine);
			sb.Append("   }" + Environment.NewLine);
			sb.Append("} " + Environment.NewLine);
			sb.Append("</SCRIPT>" + Environment.NewLine);
			return sb.ToString();
		}
		/// <summary>
		/// Private method. Creates a dataset from the xml file.
		/// </summary>
		/// <returns>Dataset</returns>
		private DataSet GetFindMetaData()
		{
			DataSet findMeta  = new DataSet();
			findMeta.ReadXml(Request.MapPath(BnbDataGridForQueryDataSet1.MetaDataXmlUrl));
			return findMeta;
		}

		/// <summary>
		/// Private method. Does the search and fills in the BnbDataGrid with result.
		/// </summary>
		private void PerformFind()
		{
			// get criteria from the CriteriaBuilderComposite
			try 
			{
				BnbCriteria objFindCriteria = BnbCriteriaBuilderComposite1.Criteria;
				// find view name
				DataTable objdtbFindOption = this.GetFindMetaData().Tables["FindOption"];
				DataRow objdtrFindRow = objdtbFindOption.Select("ID = " + cboFindType.SelectedValue)[0];
				string strViewName = objdtrFindRow["QueryView"].ToString();
				string strLinkTarget = objdtrFindRow["LinkTarget"].ToString();
				string strDomainObjectType = objdtrFindRow["DomainObjectType"].ToString();

				// run view
				BnbQuery  objFindQuery = new BnbQuery(strViewName, objFindCriteria);
				objFindQuery.MaxRows = 250;
				BnbQueryDataSet objFindResults = objFindQuery.Execute();

				// bind to view grid
				BnbDataGridForQueryDataSet1.IsMaxRowResultsExceeded = objFindQuery.MaxRowsExceeded;
				BnbDataGridForQueryDataSet1.LinkTarget = strLinkTarget;
				BnbDataGridForQueryDataSet1.DomainObjectType = strDomainObjectType;
				BnbDataGridForQueryDataSet1.QueryDataSet = objFindResults;
				BnbDataGridForQueryDataSet1.ViewLinkVisible = true;
			}
			catch(BnbCriteriaException ce)
			{
				lblMessages.Text = ce.Message;
			}
		}

		#endregion Private methods

		#region events

		protected void cmdFind_Click(object sender, System.EventArgs e)
		{
			this.PerformFind();
		}

		#endregion events

	}
}
