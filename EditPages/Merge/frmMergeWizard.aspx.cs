using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboEngine.Query;
using BonoboWebControls;
using BonoboDomainObjects;
using BonoboEngine;

namespace Contact
{
    public partial class frmMergeWizard : System.Web.UI.Page
    {
        #region " Page_Load "

        protected void Page_Load(object sender, EventArgs e)
        {

            if ((Session["LoggedIn"] == null || (!(bool)Session["LoggedIn"])))
                Response.Redirect("Login.aspx");

            if (this.Request.UrlReferrer != null)
                lblHiddenReferrer.Text = this.Request.UrlReferrer.ToString();
            else
                lblHiddenReferrer.Text = "~/frmMergeWizard.aspx";

            wizardButtons.AddPanel(panelOne);
            wizardButtons.AddPanel(panelTwo);
            wizardButtons.AddPanel(panelThree);

            wizardButtons.FinishButton.Enabled = false;

            wizardButtons.PreviousButton.Visible = true;
            wizardButtons.CancelButton.Visible = true;

            //string findid = HttpContext.Current.Request.QueryString["FindTypeID"] + "";
            if (!this.IsPostBack)
            {
                // TPT amended VAIND-93 - Permission setting to access this page
                BnbWorkareaManager.CheckPageAccess(this);
                if (!BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_WebMerge))
                {
                    Response.Redirect(String.Format("~/Message.aspx?Message={0}",
                 this.Page.Server.UrlEncode("You do not have access rights for this page, sorry.")), true);
                }

                cmdFindTgt.Attributes.Add("onclick", "javascript:showLoadingMessage('Searching Target Individual...');");
                cmdFindUwtd.Attributes.Add("onclick", "javascript:showLoadingMessage('Searching Unwanted Individual...');");
                wizardButtons.FinishButton.Attributes.Add("onclick", "javascript:showLoadingMessage('Merging...');");
                //******** Target **************
                cboFindTypeTgt.AutoPostBack = true;
                // get the datasource for this control from the xml file
                cboFindTypeTgt.DataSource = this.GetFindMetaDataTarget();
                cboFindTypeTgt.DataMember = "FindOption";
                cboFindTypeTgt.DataTextField = "Name";
                cboFindTypeTgt.DataValueField = "ID";
                //cboFindTypeTgt.SelectedValue = (findid == "") ? "0" : findid;
                cboFindTypeTgt.SelectedValue = "0";
                cboFindTypeTgt.DataBind();

                //******** UnWanted **************
                cboFindTypeUwtd.AutoPostBack = true;
                // get the datasource for this control from the xml file
                cboFindTypeUwtd.DataSource = this.GetFindMetaDataUnwanted();
                cboFindTypeUwtd.DataMember = "FindOption";
                cboFindTypeUwtd.DataTextField = "Name";
                cboFindTypeUwtd.DataValueField = "ID";
                //cboFindTypeUwtd.SelectedValue = (findid == "") ? "0" : findid;
                cboFindTypeUwtd.SelectedValue = "0";
                cboFindTypeUwtd.DataBind();

            }
            BnbCriteriaBuilderCompositeTgt.FindTypeID = int.Parse(cboFindTypeTgt.SelectedValue);
            BnbCriteriaBuilderCompositeUwtd.FindTypeID = int.Parse(cboFindTypeUwtd.SelectedValue);



        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            BnbCriteriaBuilderCompositeTgt.Controls.Clear();
            BnbCriteriaBuilderCompositeUwtd.Controls.Clear();
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
            wizardButtons.CancelWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
            wizardButtons.FinishWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
            wizardButtons.ShowPanel += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
            wizardButtons.ValidatePanel += new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
        }
        protected void Page_UnLoad(object sender, EventArgs e)
        {
            //tblIndividualKeyInfo_Forename.Value = "";
            //tblIndividualKeyInfo_Surname.Value = "";
        }
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //  this.cmdFind.Click += new System.EventHandler(this.cmdFind_Click);
            //this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

        #region " Field setup After Merge"
        /// <summary>
        /// Show finish buttion once the merge process over
        /// and disable the other navigation buttons
        /// </summary>
        private void SetButtonPropertiesAfterMerge()
        {
            wizardButtons.FinishButton.Text = "Finish";
            wizardButtons.FinishButton.Enabled = true;
            wizardButtons.PreviousButton.Enabled = false;
            wizardButtons.CancelButton.Enabled = false;
            BnbRefresh.Enabled = false;
        }

        #endregion

        #region " Wizard Panel Setup "
        /// <summary>
        /// Method to show wizard panel
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            GetSelectedValuesFromTheForm();

            if (e.CurrentPanel == 0)
                this.SetupPanelOne();

            if (e.CurrentPanel == 1)
                this.SetupPanelTwo();

            if (e.CurrentPanel == 2)
                this.SetupPanelThree();

        }

        private void SetupPanelOne()
        {
            lblScreenText.Text = " - Select Target Individual";
            Session["MergeCompleted"] = null;
            wizardButtons.CancelButton.Visible = false;
            Master.HiddenDivTarget.InnerHtml = string.Empty;
            CreateHiddenFieldsForUnwanted();
        }

        private void SetupPanelTwo()
        {
            lblScreenText.Text = " - Select Unwanted Individual";
            Master.HiddenDivUnwntd.InnerHtml = string.Empty;
            CreateHiddenFieldsForTarget();
        }

        private void SetupPanelThree()
        {
            wizardButtons.FinishButton.Text = "Merge";
            lblScreenText.Text = " - Merge Individuals";
            CreateHiddenFieldsForUnwanted();

            ExecuteMerge();

        }

        #endregion

        #region "Hold Search Value"
        /// <summary>
        /// Method to hold the search values in the screen
        /// </summary>
        private void GetSelectedValuesFromTheForm()
        {
            // Target Individual
            if (Request.Form["t_tblIndividualKeyInfo_Forename"] != null)
                ViewState["t_tblIndividualKeyInfo_Forename"] = Request.Form["t_tblIndividualKeyInfo_Forename"];


            if (Request.Form["t_tblIndividualKeyInfo_Surname"] != null)
                ViewState["t_tblIndividualKeyInfo_Surname"] = Request.Form["t_tblIndividualKeyInfo_Surname"];


            if (Request.Form["t_tblIndividualAdditional_PreviousSurname"] != null)
                ViewState["t_tblIndividualAdditional_PreviousSurname"] = Request.Form["t_tblIndividualAdditional_PreviousSurname"];


            if (Request.Form["t_tblIndividualKeyInfo_old_indv_id"] != null)
                ViewState["t_tblIndividualKeyInfo_old_indv_id"] = Request.Form["t_tblIndividualKeyInfo_old_indv_id"];


            if (Request.Form["t_lnkIndividualNationality_NationalityID"] != null)
                ViewState["t_lnkIndividualNationality_NationalityID"] = Request.Form["t_lnkIndividualNationality_NationalityID"];



            // Unwanted Individual
            if (Request.Form["u_tblIndividualKeyInfo_Forename"] != null)
                ViewState["u_tblIndividualKeyInfo_Forename"] = Request.Form["u_tblIndividualKeyInfo_Forename"];


            if (Request.Form["u_tblIndividualKeyInfo_Surname"] != null)
                ViewState["u_tblIndividualKeyInfo_Surname"] = Request.Form["u_tblIndividualKeyInfo_Surname"];



            if (Request.Form["u_tblIndividualAdditional_PreviousSurname"] != null)
                ViewState["u_tblIndividualAdditional_PreviousSurname"] = Request.Form["u_tblIndividualAdditional_PreviousSurname"];



            if (Request.Form["u_tblIndividualKeyInfo_old_indv_id"] != null)
                ViewState["u_tblIndividualKeyInfo_old_indv_id"] = Request.Form["u_tblIndividualKeyInfo_old_indv_id"];


            if (Request.Form["u_lnkIndividualNationality_NationalityID"] != null)
                ViewState["u_lnkIndividualNationality_NationalityID"] = Request.Form["u_lnkIndividualNationality_NationalityID"];

        }
        /// <summary>
        /// Create Hidden fields for Target Individual screen to hold Search value
        /// </summary>
        private void CreateHiddenFieldsForTarget()
        {
            Master.HiddenDivTarget.InnerHtml = string.Empty;

            if ((ViewState["t_tblIndividualKeyInfo_Forename"] != null) && (ViewState["t_tblIndividualKeyInfo_Forename"].ToString() != ""))
                Master.HiddenDivTarget.InnerHtml = @"<input type=""hidden"" name=""t_tblIndividualKeyInfo_Forename"" id=""t_tblIndividualKeyInfo_Forename"" value =""" + ViewState["t_tblIndividualKeyInfo_Forename"].ToString() + @""" />";

            if ((ViewState["t_tblIndividualKeyInfo_Surname"] != null) && (ViewState["t_tblIndividualKeyInfo_Surname"].ToString() != ""))
                Master.HiddenDivTarget.InnerHtml = @"<input type=""hidden"" id=""t_tblIndividualKeyInfo_Surname"" name=""t_tblIndividualKeyInfo_Surname"" value =""" + ViewState["t_tblIndividualKeyInfo_Surname"].ToString() + @""" />";

            if ((ViewState["t_tblIndividualAdditional_PreviousSurname"] != null) && (ViewState["t_tblIndividualAdditional_PreviousSurname"].ToString() != ""))
                Master.HiddenDivTarget.InnerHtml = @"<input type=""hidden"" id=""t_tblIndividualAdditional_PreviousSurname"" name=""t_tblIndividualAdditional_PreviousSurname"" value =""" + ViewState["t_tblIndividualAdditional_PreviousSurname"].ToString() + @""" />";

            if ((ViewState["t_tblIndividualKeyInfo_old_indv_id"] != null) && (ViewState["t_tblIndividualKeyInfo_old_indv_id"].ToString() != ""))
                Master.HiddenDivTarget.InnerHtml = @"<input type=""hidden"" id=""t_tblIndividualKeyInfo_old_indv_id"" name=""t_tblIndividualKeyInfo_old_indv_id"" value =""" + ViewState["t_tblIndividualKeyInfo_old_indv_id"].ToString() + @""" />";


            if ((ViewState["t_lnkIndividualNationality_NationalityID"] != null) && (ViewState["t_lnkIndividualNationality_NationalityID"].ToString() != "00000000-0000-0000-0000-000000000000"))
                Master.HiddenDivTarget.InnerHtml = @"<input type=""hidden"" id=""t_lnkIndividualNationality_NationalityID"" name=""t_lnkIndividualNationality_NationalityID"" value =""" + ViewState["t_lnkIndividualNationality_NationalityID"].ToString() + @""" />";

        }

        /// <summary>
        /// /// Create Hidden fields for Unwanted Individual screen to hold Search value
        /// </summary>
        private void CreateHiddenFieldsForUnwanted()
        {
            Master.HiddenDivUnwntd.InnerHtml = string.Empty;

            if ((ViewState["u_tblIndividualKeyInfo_Forename"] != null) && (ViewState["u_tblIndividualKeyInfo_Forename"].ToString() != ""))
                Master.HiddenDivUnwntd.InnerHtml = @"<input type=""hidden"" name=""u_tblIndividualKeyInfo_Forename"" id=""u_tblIndividualKeyInfo_Forename"" value =""" + ViewState["u_tblIndividualKeyInfo_Forename"].ToString() + @""" />";

            if ((ViewState["u_tblIndividualKeyInfo_Surname"] != null) && (ViewState["u_tblIndividualKeyInfo_Surname"].ToString() != ""))
                Master.HiddenDivUnwntd.InnerHtml = @"<input type=""hidden"" id=""u_tblIndividualKeyInfo_Surname"" name=""u_tblIndividualKeyInfo_Surname"" value =""" + ViewState["u_tblIndividualKeyInfo_Surname"].ToString() + @""" />";

            if ((ViewState["u_tblIndividualAdditional_PreviousSurname"] != null) && (ViewState["u_tblIndividualAdditional_PreviousSurname"].ToString() != ""))
                Master.HiddenDivUnwntd.InnerHtml = @"<input type=""hidden"" id=""u_tblIndividualAdditional_PreviousSurname"" name=""u_tblIndividualAdditional_PreviousSurname"" value =""" + ViewState["u_tblIndividualAdditional_PreviousSurname"].ToString() + @""" />";

            if ((ViewState["u_tblIndividualKeyInfo_old_indv_id"] != null) && (ViewState["u_tblIndividualKeyInfo_old_indv_id"].ToString() != ""))
                Master.HiddenDivUnwntd.InnerHtml = @"<input type=""hidden"" id=""u_tblIndividualKeyInfo_old_indv_id"" name=""u_tblIndividualKeyInfo_old_indv_id"" value =""" + ViewState["u_tblIndividualKeyInfo_old_indv_id"].ToString() + @""" />";

            if ((ViewState["u_lnkIndividualNationality_NationalityID"] != null) && (ViewState["u_lnkIndividualNationality_NationalityID"].ToString() != "00000000-0000-0000-0000-000000000000"))
                Master.HiddenDivUnwntd.InnerHtml = @"<input type=""hidden"" id=""u_lnkIndividualNationality_NationalityID"" name=""u_lnkIndividualNationality_NationalityID"" value =""" + ViewState["u_lnkIndividualNationality_NationalityID"].ToString() + @""" />";

        }

        #endregion

        #region " Target/Unwanted MetaData"

        /// <summary>
        /// Private method. Creates a dataset from the xml file.
        /// </summary>
        /// <returns>Dataset</returns>
        private DataSet GetFindMetaDataTarget()
        {
            string xmlUrl = "Xml/BonoboFindMetaDataTarget.xml";
            DataSet findMeta = new DataSet();
            findMeta.ReadXml(Request.MapPath(xmlUrl));
            return findMeta;
        }

        /// <summary>
        /// Private method. Creates a dataset from the xml file.
        /// </summary>
        /// <returns>Dataset</returns>
        private DataSet GetFindMetaDataUnwanted()
        {
            string xmlUrl = "Xml/BonoboFindMetaDataUnwanted.xml";
            DataSet findMeta = new DataSet();
            findMeta.ReadXml(Request.MapPath(xmlUrl));
            return findMeta;
        }
        #endregion

        #region " Navitate URL"
        /// <summary>
        /// Navigate to Home screen(Instruction Screen)
        /// </summary>
        private void ReturnToOriginalPage()
        {
            Response.Redirect(lblHiddenReferrer.Text);
        }
        #endregion

        #region "Screen and Wizard Events "

        /// <summary>
        /// Target Individual - Find button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmdFindTgt_Click(object sender, System.EventArgs e)
        {
            Master.HiddenDivTarget.InnerHtml = string.Empty;
            CreateHiddenFieldsForUnwanted();
            Session["TargetRefNo"] = null;
            wizardButtons.PreviousButton.Visible = false;
            wizardButtons.CancelButton.Visible = false;
            this.PerformFindTarget();
        }

        /// <summary>
        /// Unwanted Individual - Find button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmdFindUwtd_Click(object sender, System.EventArgs e)
        {
            Master.HiddenDivUnwntd.InnerHtml = string.Empty;
            CreateHiddenFieldsForTarget();
            Session["UnwantedRefNo"] = null;
            this.PerformFindUnwanted();
        }

        /// <summary>
        /// Merge Screen - Refresh button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BnbRefresh_Click(object sender, System.EventArgs e)
        {
            ExecuteMerge();
        }

        /// <summary>
        /// Wizard Control - Validate Panel when click next
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
        {
            if (e.CurrentPanel == 0)
            {

                if (Session["MergeTarget"].ToString() == "")
                {
                    e.Proceed = false;
                    wizardButtons.PreviousButton.Visible = false;
                    wizardButtons.CancelButton.Visible = false;
                    lblPanelOneFeedback.Text = "Please click on 'Find' to choose a Target Individual";
                }
                else if (Session["TargetRefNo"] == null)
                {
                    e.Proceed = false;
                    wizardButtons.PreviousButton.Visible = false;
                    wizardButtons.CancelButton.Visible = false;
                    lblPanelOneFeedback.Text = "Please Choose a Target Record to proceed";
                }

            }
            if (e.CurrentPanel == 1)
            {
                if (Session["MergeUnwanted"].ToString() == "")
                {
                    e.Proceed = false;
                    lblPanelTwoFeedback.Text = "Please click on 'Find' to choose an Unwanted Individual";
                }
                else if (Session["UnwantedRefNo"] == null)
                {
                    e.Proceed = false;
                    lblPanelTwoFeedback.Text = "Please Choose an Unwanted Record to proceed";
                }
            }
        }

        /// <summary>
        /// Wizard control - Cancel button click event
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            // clear the individuals ref no when click's on the cancel button 
            // and redirect to wizard first page.
            Session["TargetRefNo"] = null;
            Session["UnwantedRefNo"] = null;
            Session["MergeTarget"] = "";
            Session["MergeUnwanted"] = "";
            Session["MergeTargetID"] = "";
            Session["MergeUnwantedID"] = "";
            Session["tblUnwanted"] = null;
            this.ReturnToOriginalPage();
        }

        /// <summary>
        /// Wizard control - Finish/Merge button click event
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            if ((Session["MergeCompleted"] != null))
                if (Convert.ToBoolean(Session["MergeCompleted"]))
                    this.ReturnToOriginalPage();

            // When click's on the Merge Button

            if ((Session["MergeTarget"].ToString() != "0") && (Session["MergeTargetID"].ToString() != "")
                                             && (Session["MergeUnwanted"].ToString() != "0") && (Session["MergeUnwantedID"].ToString() != ""))
            {
                string strUnwantedID = Session["MergeUnwantedID"].ToString();
                bool blnSuccess = true;
                string strTargetID = Session["MergeTargetID"].ToString();
                string strErrorMessage = "The merge has not taken place due to the following problems: <br>";

                //			GC.Collect();
                //			GC.WaitForPendingFinalizers();    

                BnbIndividual unwtdInd = BnbIndividual.Retrieve(new Guid(strUnwantedID));
                GetAllReferenceIDFromUnwantedIndivdiual(unwtdInd);
                try
                {
                    BonoboDomainObjects.BnbIndividualMerge objMerge = new BnbIndividualMerge(true);
                    objMerge.MergeIndividuals(strUnwantedID, strTargetID);
                }
                catch (BnbProblemException pe)
                {
                    blnSuccess = false;
                    BonoboEngine.BnbLockManager objLockMger = new BnbLockManager();
                    objLockMger.ClearAllLocks();


                    lblErrorMessage.Text = "";
                    foreach (BnbProblem prob in pe.Problems)
                    {
                        // check the problem whether it is caused by Target or Unwanted with temp 'tblUnwanted' table
                        DataTable dt = (DataTable)Session["tblUnwanted"];
                        bool errorUwtedInd = false;
                        int count = dt.Rows[0].ItemArray.Length;
                        for (int i = 0; i < count; i++)
                        {
                            if (prob.ID.ToString() == dt.Rows[0].ItemArray[i].ToString())
                            {
                                strErrorMessage += unwtdInd.RefNo + "-" + prob.Message +  "<br>";
                                errorUwtedInd = true;
                            }
                        }
                        if(!errorUwtedInd)
                            strErrorMessage += Session["TargetRefNo"].ToString() + "-" + prob.Message + "<br>";
                    }
                    lblErrorMessage.Text = strErrorMessage + "<br>";
                    lblTargetDetails.Text = "";
                    DisplayIndividualDetailsByGUID(strTargetID, lblTargetDetails);
                    DisplayIndividualDetailsByGUID(strUnwantedID, lblUnwantedDetails);
                }

                if (blnSuccess)
                {
                    lblErrorMessage.Text = "The merge is now completed";
                    DisplayIndividualDetailsByGUID(strTargetID, lblTargetDetails);
                    lblUnwantedHeader.Text = string.Empty;
                    lblUnwantedDetails.Text = string.Empty;

                    //reset session variables.
                    Session["MergeTargetID"] = "";
                    Session["MergeUnwantedID"] = "";
                    Session["MergeTarget"] = "";
                    Session["MergeUnwanted"] = "";
                    Session["MergeCompleted"] = true;
                    Session["tblUnwanted"] = null;
                    SetButtonPropertiesAfterMerge();

                }

                //reset all labels
                lblTApplicationStatus.Text = "";
                lblUApplicationStatus.Text = "";
                // lblUnwantedDetails.Text = "";
            }
            else
                this.ReturnToOriginalPage(); // redirect to instruction page

        }

        /// <summary>
        /// VAIND-37 - TPT Amended
        /// Collect Unwanted details in temp table for identifying the validation exception which record caused the problem while merge.
        /// </summary>
        /// <param name="objIndividual"></param>
        private void GetAllReferenceIDFromUnwantedIndivdiual(BnbIndividual objIndividual)
        {
            BnbRelationshipInfoCollection lst = objIndividual.GetChildRelationships();
            string className;
            BnbDomainObjectList objectList = null; ;
            DataTable dt = new DataTable("tblUnwantedIndividual");
            DataRow dr = dt.NewRow();

            // collecting Individual & its child class IDs in temp table.
            dt.Columns.Add("IndividualID");
            dr["IndividualID"] = objIndividual.ID;

            if (objIndividual.Additional != null)
            {
                dt.Columns.Add("IndividualAdditionalID");
                dr["IndividualAdditionalID"] = objIndividual.Additional.ID;
            }

            bool contactCount = true;
            for (int i = 0; i < lst.Count; i++)
            {
                className = lst[i].RelatedClassName;

                switch (className)
                {
                    case ("BnbAccountHeader"):
                        objectList = objIndividual.AccountHeaders;
                        break;
                    case ("BnbAttendance"):
                        objectList = objIndividual.Attendances;
                        break;
                    case ("BnbRelative"):
                        objectList = objIndividual.Relatives;
                        break;
                    case ("BnbEventPersonnel"):
                        objectList = objIndividual.EventPersonnel;
                        break;
                    case ("BnbContactNumber"):
                        // check count of BnbContactNumber as we used twice in BnbIndividual & this should assign single time to list
                        if (contactCount)
                        {
                            objectList = objIndividual.ContactNumbers;
                            contactCount = false;
                        }
                        break;
                    case ("BnbContactDetail"):
                        objectList = objIndividual.ContactDetails;
                        break;
                    case ("BnbAddress"):
                        objectList = objIndividual.IndividualAddresses;
                        break;
                    case ("BnbApplication"):
                        objectList = objIndividual.Applications;
                        break;
                    case ("BnbIndividualAddress"):
                        objectList = objIndividual.IndividualAddresses;
                        break;
                    case ("BnbIndividualAddressDistribution"):
                        objectList = objIndividual.IndividualAddressDistributions;
                        break;
                    case ("BnbIndividualAllergy"):
                        objectList = objIndividual.IndividualAllergies;
                        break;
                    case ("BnbIndividualBenefit"):
                        objectList = objIndividual.IndividualBenefits;
                        break;
                    case ("BnbIndividualContactBlock"):
                        objectList = objIndividual.IndividualContactBlocks;
                        break;
                    case ("BnbIndividualMaritalStatus"):
                        objectList = objIndividual.IndividualMaritalStatuses;
                        break;
                    case ("BnbIndividualNationality"):
                        objectList = objIndividual.IndividualNationalities;
                        break;
                    case ("BnbIndividualQualification"):
                        objectList = objIndividual.IndividualQualifications;
                        break;
                    case ("BnbIndividualResource"):
                        objectList = objIndividual.IndividualResources;
                        break;
                    case ("BnbIndividualSkill"):
                        objectList = objIndividual.IndividualSkills;
                        break;
                    case ("BnbIndividualSubscription"):
                        objectList = objIndividual.IndividualSubscriptions;
                        break;
                    case ("BnbIndividualVaccination"):
                        objectList = objIndividual.IndividualVaccinations;
                        break;
                }
                if (objectList != null)
                {
                    for (int j = 0; j < objectList.Count; j++)
                    {
                        dt.Columns.Add(className + "_ID" + j);
                        dr[className + "_ID" + j] = objectList[j].ID;
                    }
                    objectList = null;
                }

            }
            // collecting Application and its child class IDs in temp table.
            int appIDs = 0;
            foreach (BnbApplication app in objIndividual.Applications)
            {
                for (int i = 0; i < app.GetChildRelationships().Count; i++)
                {
                    className = app.GetChildRelationships()[i].RelatedClassName;

                    switch (className)
                    {
                        case ("BnbPaperMove"):
                            objectList = app.PaperMoves;
                            break;
                        case ("BnbApplicationStatus"):
                            objectList = app.ApplicationStatuses;
                            break;
                        case ("BnbApplicationMedicalRequirement"):
                            objectList = app.ApplicationMedicalRequirements;
                            break;
                        case ("BnbFunding"):
                            objectList = app.Funding;
                            break;
                    }
                    if (objectList != null)
                    {
                        for (int j = 0; j < objectList.Count; j++)
                        {
                            dt.Columns.Add(className + "_ID" + appIDs + j);
                            dr[className + "_ID" + appIDs + j] = objectList[j].ID;
                        }
                        objectList = null;
                    }
                }
                appIDs++;
            }
            dt.Rows.Add(dr);
            Session["tblUnwanted"] = dt;

        }

        #endregion events

        #region " Search Result for Target / Unwanted"

        /// <summary>
        /// Private method. Does the search and fills in the DataGrid with result - Target.
        /// </summary>
        private void PerformFindTarget()
        {
            // get criteria from the CriteriaBuilderComposite
            try
            {
                lblMessagesTgt.Text = string.Empty;
                lblPanelOneFeedback.Text = string.Empty;
                lblRecResultTgt.Text = string.Empty;

                BnbCriteria objFindCriteria = BnbCriteriaBuilderCompositeTgt.Criteria;
                // find view name
                DataTable objdtbFindOption = this.GetFindMetaDataTarget().Tables["FindOption"];
                DataRow objdtrFindRow = objdtbFindOption.Select("ID = " + cboFindTypeTgt.SelectedValue)[0];
                string strViewName = objdtrFindRow["QueryView"].ToString();
                //string strLinkTarget = objdtrFindRow["LinkTarget"].ToString();
                string strDomainObjectType = objdtrFindRow["DomainObjectType"].ToString();

                // run view
                BnbQuery objFindQuery = new BnbQuery(strViewName, objFindCriteria);
                objFindQuery.MaxRows = 250;
                BnbQueryDataSet objFindResultsTgt = objFindQuery.Execute();

                DataTable dtTargetResults = BonoboWebControls.BnbWebControlServices.GetHtmlEncodedDataTable(objFindResultsTgt.Tables["Results"]);

                if (dtTargetResults.Rows.Count > 0)
                {
                    lblRecResultTgt.Text = "<p align=\"left\"><b>Your Search has returned " + dtTargetResults.Rows.Count + " records";
                    gvTarget.DataSource = dtTargetResults;
                    gvTarget.DataBind();
                    Session["MergeTarget"] = "1";
                }
                else
                {
                    lblRecResultTgt.Text = "<p align=\"left\"><b>Your Search has returned " + dtTargetResults.Rows.Count + " records";
                    gvTarget.DataSource = "";
                    gvTarget.DataBind();
                }
            }
            catch (BnbCriteriaException ce)
            {
                gvTarget.DataSource = "";
                gvTarget.DataBind();
                lblMessagesTgt.Text = ce.Message;
            }
        }


        /// <summary>
        /// Private method. Does the search and fills in the DataGrid with result - Unwanted.
        /// </summary>
        private void PerformFindUnwanted()
        {
            // get criteria from the CriteriaBuilderComposite
            try
            {
                lblMessagesUwtd.Text = string.Empty;
                lblPanelTwoFeedback.Text = string.Empty;
                lblRecResultUnwtd.Text = string.Empty;

                BnbCriteria objFindCriteria = BnbCriteriaBuilderCompositeUwtd.Criteria;
                // find view name
                DataTable objdtbFindOption = this.GetFindMetaDataUnwanted().Tables["FindOption"];
                DataRow objdtrFindRow = objdtbFindOption.Select("ID = " + cboFindTypeUwtd.SelectedValue)[0];
                string strViewName = objdtrFindRow["QueryView"].ToString();
                //string strLinkTarget = objdtrFindRow["LinkTarget"].ToString();
                string strDomainObjectType = objdtrFindRow["DomainObjectType"].ToString();

                // run view
                BnbQuery objFindQuery = new BnbQuery(strViewName, objFindCriteria);
                objFindQuery.MaxRows = 250;
                BnbQueryDataSet objFindResultsUnWtd = objFindQuery.Execute();

                DataTable dtUnwantedResults = BonoboWebControls.BnbWebControlServices.GetHtmlEncodedDataTable(objFindResultsUnWtd.Tables["Results"]);

                if (dtUnwantedResults.Rows.Count > 0)
                {
                    lblRecResultUnwtd.Text = "<p align=\"left\"><b>Your Search has returned " + dtUnwantedResults.Rows.Count + " records";
                    gvUnwanted.DataSource = dtUnwantedResults;
                    gvUnwanted.DataBind();
                    Session["MergeUnwanted"] = "1";
                }
                else
                {
                    lblRecResultUnwtd.Text = "<p align=\"left\"><b>Your Search has returned " + dtUnwantedResults.Rows.Count + " records";
                    gvUnwanted.DataSource = "";
                    gvUnwanted.DataBind();
                }
            }
            catch (BnbCriteriaException ce)
            {
                gvUnwanted.DataSource = "";
                gvUnwanted.DataBind();
                lblMessagesUwtd.Text = ce.Message;
            }
        }

        #endregion

        #region " Individuals Selection"

        /// <summary>
        /// Checked Changed Event - Target Indivudal selection in the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rbListTarget_CheckedChanged(object sender, EventArgs e)
        {
            lblPanelOneFeedback.Text = string.Empty;
            foreach (GridViewRow Trgt in gvTarget.Rows)
            {
                ((RadioButton)Trgt.FindControl("rbListTarget")).Checked = false;
            }
            RadioButton rb1 = (RadioButton)sender;
            GridViewRow trgtRow = (GridViewRow)rb1.NamingContainer;
            ((RadioButton)trgtRow.FindControl("rbListTarget")).Checked = true;
            // Cell[4] - RefNo - tblIndividualKeyInfo_old_indv_id
            Session["TargetRefNo"] = trgtRow.Cells[4].Text;

            if (panelOne.Visible)
                CreateHiddenFieldsForUnwanted();

            wizardButtons.PreviousButton.Visible = false;
            wizardButtons.CancelButton.Visible = false;
        }

        /// <summary>
        /// Checked Changed Event - unwanted Indivudal selection in the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rbListUnwanted_CheckedChanged(object sender, EventArgs e)
        {
            lblPanelTwoFeedback.Text = string.Empty;
            foreach (GridViewRow Uwtd in gvUnwanted.Rows)
            {
                ((RadioButton)Uwtd.FindControl("rbListUnwanted")).Checked = false;
            }
            RadioButton urb1 = (RadioButton)sender;
            GridViewRow uwtdRow = (GridViewRow)urb1.NamingContainer;
            ((RadioButton)uwtdRow.FindControl("rbListUnwanted")).Checked = true;
            // Cell[4] - RefNo - tblIndividualKeyInfo_old_indv_id
            Session["UnwantedRefNo"] = uwtdRow.Cells[4].Text;

            if (panelTwo.Visible)
                CreateHiddenFieldsForTarget();
        }

        #endregion

        #region  Retreive individual record

        /// <summary>
        /// Private method. Given an individualId and a page label, display the individual refNo and fullname in the 
        /// given label.
        /// </summary>
        /// <param name="IndividualGUID">string</param>
        /// <param name="LabelName">Label control</param>
        private void DisplayIndividualDetailsByGUID(string IndividualGUID, Label LabelName)
        {
            Guid guiID = new Guid(IndividualGUID);

            BonoboDomainObjects.BnbIndividual objIndividual = BnbIndividual.Retrieve(guiID);
            if (objIndividual != null)
                LabelName.Text = objIndividual.RefNo.ToString() + "&nbsp;&nbsp;" + objIndividual.FullName.ToString();
        }

        /// <summary>
        /// Retreive individual record from BnbIndividual
        /// RecordType '1' - Refers to Target Record
        /// RecordType '2' - Refers to Unwanted Record
        /// </summary>
        /// <param name="refNo"></param>
        private void DisplayIndividualDetailsByRefNo(string refNo, Label LabelName, int RecordType)
        {
            BonoboDomainObjects.BnbIndividual objIndividual = BnbIndividual.RetrieveByRefNo(refNo);
            if (objIndividual != null)
            {
                if (RecordType == 1)  // - Target Record    
                    Session["MergeTargetID"] = objIndividual.ID;
                else if (RecordType == 2) // - Unwanted Record
                    Session["MergeUnwantedID"] = objIndividual.ID;

                LabelName.Text = objIndividual.RefNo.ToString() + "&nbsp;&nbsp;" + objIndividual.FullName.ToString();
            }
        }

        #endregion

        #region "Individuals Merge Rules"
        /// <summary>
        /// Method to check the merge rules
        /// </summary>
        private void ExecuteMerge()
        {
            lblErrorMessage.Text = string.Empty;
            if (Session["TargetRefNo"] != null)
                DisplayIndividualDetailsByRefNo(Session["TargetRefNo"].ToString(), lblTargetDetails, 1);

            if (Session["UnwantedRefNo"] != null)
                DisplayIndividualDetailsByRefNo(Session["UnwantedRefNo"].ToString(), lblUnwantedDetails, 2);


            if ((Session["MergeTarget"].ToString() == "1") && (Session["MergeUnwanted"].ToString() == "1"))
            {
                if ((Session["MergeTargetID"].ToString() != "") && (Session["MergeUnwantedID"].ToString() != ""))
                {
                    CheckApplicationStatus(Session["MergeUnwantedID"].ToString(), Session["MergeTargetID"].ToString());
                    //Merge rules: 1 check that the birth dates are the same and surname first left 3 characters are identical
                    if (MergeRules(Session["MergeUnwantedID"].ToString(), Session["MergeTargetID"].ToString()))
                    {

                        wizardButtons.FinishButton.Enabled = true;
                    }
                }
            }
        }

        /// <summary>
        /// Private method. Given the unwanted and target individual IDs as string, retrieves their application status.
        /// if the application status is not of type enquirer or applicant display a warning message.
        /// </summary>
        /// <param name="UnwantedGUID">string</param>
        /// <param name="TargetGUID">string</param>
        private void CheckApplicationStatus(string UnwantedGUID, string TargetGUID)
        {
            string strUnwantedIndvApplicationStatus = "";
            string strMergeIndvApplicationStatus = "";
            string strErrorMessage = lblErrorMessage.Text + "<br>";

            //get the application status and if they are different then display a message.
            BnbListCondition lc1 = new BnbListCondition("t_tblIndividualKeyInfo_IndividualID", new Guid(TargetGUID));
            BnbListCondition lc2 = new BnbListCondition("u_tblIndividualKeyInfo_IndividualID", new Guid(UnwantedGUID));

            BnbQuery qryMergeIndv = new BnbQuery("vwBonoboFindIndividualGCMergeTarget", new BnbCriteria(lc1));
            BnbQuery qryUnwantedIndv = new BnbQuery("vwBonoboFindIndividualGCMergeUnwanted", new BnbCriteria(lc2));

            DataTable dtbResultsMerge = qryMergeIndv.Execute().Tables["Results"];
            if (dtbResultsMerge.Rows.Count > 0)
            {
                strMergeIndvApplicationStatus = dtbResultsMerge.Rows[0]["t_Custom_ApplicationStatus"].ToString();
                lblTApplicationStatus.Text = strMergeIndvApplicationStatus;
            }
            dtbResultsMerge.Clear();

            DataTable dtbResultsUnwanted = qryUnwantedIndv.Execute().Tables["Results"];
            if (dtbResultsUnwanted.Rows.Count > 0)
            {
                strUnwantedIndvApplicationStatus = dtbResultsUnwanted.Rows[0]["u_Custom_ApplicationStatus"].ToString();
                lblUApplicationStatus.Text = strUnwantedIndvApplicationStatus;
            }
            dtbResultsUnwanted.Clear();
            if ((strUnwantedIndvApplicationStatus.IndexOf("Dire") > 0) || (strMergeIndvApplicationStatus.IndexOf("Dire") > 0))
                lblErrorMessage.Text = strErrorMessage + "Warning: One application has a Directorate/Special status";

            if ((strUnwantedIndvApplicationStatus.IndexOf("Enquirer") == -1) && (strUnwantedIndvApplicationStatus.IndexOf("Applicant") == -1))
                lblErrorMessage.Text = strErrorMessage + "Warning: Merge works best when Unwanted record is Enq or App";
        }


        /// <summary>
        /// Private method returning a boolean. Arguments: unwanted individual ID as string, target individual id as string. 
        /// Checks that the 2 individuals have same birthdate, first 3 characters of
        /// their surname is identical as well as the first character of their firstname. If one or all conditions not met then
        /// display error message.
        /// </summary>
        /// <param name="UnwantedGUID">string</param>
        /// <param name="TargetGUID">string</param>
        /// <returns>Boolean</returns>

        private bool MergeRules(string UnwantedGUID, string TargetGUID)
        {
            bool blnResult = false;
            string strUnwantedSurname = "";
            string strTargetSurname = "";
            string strUnwantedFirstname = "";
            string strTargetFirstname = "";
            Guid UnwantedID = new Guid(UnwantedGUID);
            Guid TargetID = new Guid(TargetGUID);
            string strErrorMessage = lblErrorMessage.Text + "<br>";
            bool blnDOBCheck = false;

            if (UnwantedGUID != TargetGUID)
            {
                BonoboDomainObjects.BnbIndividual objUnwantedIndividual = BnbIndividual.Retrieve(UnwantedID);
                BonoboDomainObjects.BnbIndividual objTargetIndividual = BnbIndividual.Retrieve(TargetID);
                if (objUnwantedIndividual.Surname != null)
                {
                    if (objUnwantedIndividual.Surname.ToString().Length > 2)
                        strUnwantedSurname = objUnwantedIndividual.Surname.ToString().Substring(0, 3);
                }
                if (objTargetIndividual.Surname != null)
                {
                    if (objTargetIndividual.Surname.ToString().Length > 2)
                        strTargetSurname = objTargetIndividual.Surname.ToString().Substring(0, 3);
                }
                if (objUnwantedIndividual.FirstName != null)
                    strUnwantedFirstname = objUnwantedIndividual.FirstName.ToString().Substring(0, 1);

                if (objTargetIndividual.FirstName != null)
                    strTargetFirstname = objTargetIndividual.FirstName.ToString().Substring(0, 1);

                if ((BnbRuleUtils.HasValue(objUnwantedIndividual.DateOfBirth.Date)) && (BnbRuleUtils.HasValue(objTargetIndividual.DateOfBirth.Date)))
                {
                    if (objUnwantedIndividual.DateOfBirth.Date != objTargetIndividual.DateOfBirth.Date)
                    {
                        strErrorMessage = "The unwanted and target individuals must have the same date of birth.  Please update their date of birth if you still want to merge them.";
                        blnDOBCheck = true;
                    }
                }
                if (strUnwantedFirstname != strTargetFirstname)
                {
                    strErrorMessage += "<br>The unwanted and target individuals must have the same firstname first letter. Please update their firstname if you still want to merge them.";
                }
                if (strUnwantedSurname == strTargetSurname)
                    blnResult = true;
                else
                {
                    strErrorMessage += "<br>The unwanted and target individuals must have the same first three letters of their surname. Please update their surname if you still want to merge them.";
                }

                // if both DOB is different then stop the merge process.
                // If blnDOBChek is 'true' it means that Target and unwanted DOB is diffrent.
                if (blnDOBCheck == true)
                    blnResult = false;

                lblErrorMessage.Text = strErrorMessage;
                return blnResult;
            }
            else
            {
                lblErrorMessage.Text = "The unwanted and target individuals should be different.";
                return blnResult;
            }
        }

        #endregion

    }
}
