using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboWebControls.DataControls;
using BonoboWebControls.Collections;
using BonoboDomainObjects;
using BonoboEngine;
using BonoboEngine.Query;
using System.Collections.Specialized;
using System.Text;

namespace Frond.EditPages
{
    public partial class ContractReportingDatePage : System.Web.UI.Page
    {
        private BnbWebFormManager bnb = null;
        BnbGrant grant = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            bnb = new BnbWebFormManager(this, "BnbGrantReport");
            BnbGrantReport report = bnb.GetPageDomainObject("BnbGrantReport") as BnbGrantReport;
#if DEBUG
            if (Request.QueryString.ToString() == "")
            {
                //this reloads the page with an ID for an employer not sure if this will work
                BnbWebFormManager.ReloadPage("BnbGrant=b27be9c7-605a-46fd-b908-3edaa5542de0");
                return;
            }
#endif
            pnlReportingDates.Visible = !bnb.InNewMode;
            BnbWorkareaManager.SetPageStyleOfUser(this);
            bnb.PageNameOverride = "Contract Reporting Date";
            bnb.LogPageHistory = true;
            grant = bnb.GetPageDomainObject("BnbGrant") as BnbGrant;
            doPMFStatus();
            doContractPageHyperlink();

            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null && grant != null)
                {
                    string subtitle = "Contract - " + grant.Project.InstanceDescription + "/" + grant.Company.CompanyName;
                    StringBuilder sb = new StringBuilder(Utilities.ProjectSubHlink2);
                    sb.Replace("[PROJECTID]", grant.Project.ID.ToString());
                    sb.Replace("[TITLE]", grant.Project.ProjectTitle);
                    sb.Replace("[SUBTITLE]", subtitle);
                    sb.Replace("[SUBTITLE2]", bnb.PageTitleDescriptionCurrent);
                    titleBar.TitleText = sb.ToString();

                }
            }

        }

        private void doContractPageHyperlink()
        {
            hlGoToContractPage.NavigateUrl = string.Format("ContractPage.aspx?BnbProject={0}&BnbGrant={1}&ControlID=Reports", grant.ProjectID.ToString(), grant.ID.ToString());            
        }

        private void doPMFStatus()
        {
            if (grant.Project != null)
            {
                PMFStatusPanel2.PMFStatusID = grant.Project.PMFStatusID.ToString();
            }

        }
    }
}