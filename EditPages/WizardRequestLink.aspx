<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Page Language="c#" Codebehind="WizardRequestLink.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.EditPages.WizardRequestLink" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbcriteriacontrol" Namespace="BonoboWebControls.CriteriaControls"
    Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>WizardRequestLink</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">           
            <div class="wizardheader" ms_positioning="FlowLayout">
                <h3><IMG class="icon24" src="../Images/wizard24silver.gif">
                    Placement Request Link Wizard</h3>
            </div>
            <asp:Panel ID="panelOne" runat="server" Height="100px" CssClass="wizardpanel">
                <p>
                    <em>This wizard will help you link a Placement to a Request.</em></p>
                <p>
                </p>
                <p>
                    <strong>Placement Ref: </strong>
                    <asp:Label ID="labelPlacementRef" runat="server"></asp:Label></p>
                <p>
                    <strong>Programme Area: </strong>
                    <asp:Label ID="labelProgrammeArea" runat="server"></asp:Label></p>
                <p>
                    <asp:Panel ID="panelRequestChoose" runat="server">
                        <p>
                            The following Requests are un-matched and fit the Programme Area:</p>
                        <p>
                        </p>
                        <p>
                            <asp:DataGrid ID="dataGridRequests" runat="server" CssClass="datagrid" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Select">
                                        <ItemTemplate>
                                            &nbsp;
                                            <asp:Label ID="labelRadio" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="tblRequest_RequestID" HeaderText="tblRequest_RequestID">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="tblRequest_RequestDescription" HeaderText="Request Description">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="tblRequest_VolStartDate" HeaderText="Vol Start Date"
                                        DataFormatString="{0:dd/MMM/yyyy}"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid></p>
                        <p>
                        Please select one and then press Finish
                    </asp:Panel>
                </p>
                <p>
                    <asp:Label ID="labelPanelOneFeedback" runat="server" CssClass="feedback"></asp:Label></p>
            </asp:Panel>
            <div class="wizardheader">
                <table id="Table1" style="margin: 0px" cellspacing="0" cellpadding="0" width="100%"
                    border="0">
                    <tr>
                        <td align="right" width="30%">
                        </td>
                        <td align="center" width="40%">
                            <asp:Button ID="buttonCancel" runat="server" Text="Cancel" OnClick="buttonCancel_Click">
                            </asp:Button></td>
                        <td align="left" width="30%">
                            <asp:Button ID="buttonFinish" runat="server" Text="Finish" OnClick="buttonFinish_Click">
                            </asp:Button></td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="labelHiddenRequestID" runat="server" Visible="False"></asp:Label></p>
</body>
</html>
