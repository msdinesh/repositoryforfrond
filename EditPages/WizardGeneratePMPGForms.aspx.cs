using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using BonoboWebControls;

namespace Frond.EditPages
{
    public partial class WizardGeneratePMPGForms : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
#if DEBUG
            if (!this.IsPostBack && this.Request.QueryString.ToString() == "")
                Response.Redirect("WizardGeneratePMPGForms.aspx?BnbProject=9eb1943e-7a07-40d1-88af-1f6484ca86f9");
#endif
            BnbEngine.SessionManager.ClearObjectCache();

            BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);

            this.WorkOutIfGrantRequired();
            wizardButtons.AddPanel(panelZero);
            if (uxHiddenGrantRequired.Text == "1" ||
                uxHiddenGrantRequired.Text == "2")
                wizardButtons.AddPanel(panelOne);
            wizardButtons.AddPanel(panelTwo);
            wizardButtons.AddPanel(panelThree);
            
            if (!this.IsPostBack)
            {
                BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
                BonoboWebControls.BnbWebFormManager.LogAction("Generate PMPG Forms Wizard", null, this);
                this.InitWizard();
                

            }
            SetDMSSearch();
        }

        private void InitWizard()
        {
            this.GrantRequired = "0";
            if (this.Request.UrlReferrer != null)
                uxHiddenReferrer.Text = this.Request.UrlReferrer.ToString();
            else
                uxHiddenReferrer.Text = "~/Menu.aspx";

            uxHiddenProjectID.Text = this.Request.QueryString["BnbProject"];
            if (this.Request.QueryString["BnbGrant"] != null)
                uxHiddenGrantID.Text = this.Request.QueryString["BnbGrant"];
            if (this.Request.QueryString["DocType"] != null)
                uxHiddenDocType.Text = this.Request.QueryString["DocType"];

        }

        /// <summary>
        /// returns a string
        /// records whether a grant must be specified for the chosen doc type
        /// 0 = no grant
        /// 1 = proposal required
        /// 2 = contract required
        /// </summary>
        public string GrantRequired
        {
            get { return uxHiddenGrantRequired.Text; }
            set { uxHiddenGrantRequired.Text = value; }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            wizardButtons.ValidatePanel += new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
            wizardButtons.ShowPanel += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
            wizardButtons.CancelWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
            wizardButtons.FinishWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
        }

        void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            Response.Redirect(uxHiddenReferrer.Text);
        }

        void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            Response.Redirect(uxHiddenReferrer.Text);
        }

        void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            // panels are conditionally included, so check htmlID rather than index

            if (e.CurrentPanelHtmlID == "panelZero")
               this.SetupPanelZero();
            
            if (e.CurrentPanelHtmlID == "panelOne")
                this.SetupPanelOne();

            if (e.CurrentPanelHtmlID == "panelTwo")
                this.SetupPanelTwo();

            if (e.CurrentPanelHtmlID == "panelThree")
                this.SetupPanelThree();

        }

        private void SetupPanelOne()
        {
            BnbProject proj = GetSpecifiedProject();
            uxProjectName2.Text = proj.ProjectTitle;
            uxProjectRef2.Text = proj.ProjectReference;
            uxPMFStatus2.Text = this.GetPMFStatusDescription(proj);
            BnbLookupRow chosenPmpgRow = GetChosenDocLookupRow();
            uxPMPGDocChosen.Text = chosenPmpgRow.Description;

            if (this.GrantRequired == "1")
                uxGrantTypeLabel.Text = "Proposal";
            else
                uxGrantTypeLabel.Text = "Contract";
          
            // populate choose grant drop down
            uxChooseGrantDropdown.Items.Clear();
            uxChooseGrantDropdown.Items.Add(new ListItem("", "-1"));
            BnbDomainObjectList grantList = proj.Grants;
            grantList.Sort("SubmissionDate");
            foreach (BnbGrant grantLoop in grantList)
            {
                bool include = false;
                if (this.GrantRequired == "1" && !grantLoop.IsContract)
                    include = true;
                if (this.GrantRequired == "2" && grantLoop.IsContract)
                    include = true;
                if (include)
                {
                    uxChooseGrantDropdown.Items.Add(new ListItem(grantLoop.InstanceDescription, grantLoop.ID.ToString()));
                }
            }

            // if a grant was specified in querystring, select it
            if (uxHiddenGrantID.Text != "" && uxHiddenGrantID.Text != null
                && uxChooseGrantDropdown.Items.FindByValue(uxHiddenGrantID.Text) != null)
                uxChooseGrantDropdown.SelectedValue = uxHiddenGrantID.Text;
            else
                // otherwise, select blank option
                uxChooseGrantDropdown.SelectedValue = "-1";

            uxPanelOneFeedback.Text = "";
        }

        private BnbProject GetSpecifiedProject()
        {
            BnbProject proj = BnbProject.Retrieve(new Guid(uxHiddenProjectID.Text));
            return proj;
        }

        private BnbLookupRow GetChosenDocLookupRow()
        {
            int chosenDocTypeID = -1;
            if (uxChoosePMPGDocDropdown.SelectedValue != null)
                chosenDocTypeID = int.Parse(uxChoosePMPGDocDropdown.SelectedValue.ToString());
            BnbLookupDataTable pmpgDocLookup = BnbEngine.LookupManager.GetLookup("lkuPMPGDocumentType");
            BnbLookupRow chosenPmpgRow = pmpgDocLookup.FindByID(chosenDocTypeID);
            return chosenPmpgRow;
        }

        private void SetupPanelTwo()
        {
            // if no grant required, clear grantID
            if (this.GrantRequired == "0")
                uxHiddenGrantID.Text = "";

            BnbProject proj = GetSpecifiedProject();
            uxProjectName3.Text = proj.ProjectTitle;
            uxProjectRef3.Text = proj.ProjectReference;
            uxPMFStatus3.Text = this.GetPMFStatusDescription(proj);
            if (uxHiddenGrantID.Text != "")
            {
                BnbGrant grant = BnbGrant.Retrieve(new Guid(uxHiddenGrantID.Text));
                if (grant.IsContract)
                    uxGrantLabel3.Text = "Contract";
                else
                    uxGrantLabel3.Text = "Proposal";
                uxGrantDetails3.Text = grant.InstanceDescription;
            }
            else
            {
                uxGrantLabel3.Text = "";
                uxGrantDetails3.Text = "";
            }

            BnbLookupRow chosenPmpgRow = GetChosenDocLookupRow();
            uxPMPGDocChosen3.Text = chosenPmpgRow.Description;

           

        }

        private void SetupPanelThree()
        {
            BnbProject proj = GetSpecifiedProject();
            uxProjectName4.Text = proj.ProjectTitle;
            uxProjectRef4.Text = proj.ProjectReference;
            uxPMFStatus4.Text = this.GetPMFStatusDescription(proj);
            if (uxHiddenGrantID.Text != "")
            {
                BnbGrant grant = BnbGrant.Retrieve(new Guid(uxHiddenGrantID.Text));
                if (grant.IsContract)
                    uxGrantLabel4.Text = "Contract";
                else
                    uxGrantLabel4.Text = "Proposal";
                uxGrantDetails4.Text = grant.InstanceDescription;
            }
            else
            {
                uxGrantLabel4.Text = "";
                uxGrantDetails4.Text = "";
            }

            BnbLookupRow chosenPmpgRow = GetChosenDocLookupRow();
            uxPMPGDocChosen4.Text = chosenPmpgRow.Description;

            string chosenFormAbbrev = chosenPmpgRow["Abbreviation"].ToString();
            // work out suggested filename
            string country = "unknown";
            BnbProjectProgrammeOffice projPo = proj.ProgrammeOffices.GetLead() as BnbProjectProgrammeOffice;
            if (projPo != null)
                country = BnbEngine.LookupManager.GetLookupItemDescription("lkuProgrammeOffice", projPo.ProgrammeOfficeID);
            string projectAbbrev = proj.ProjectTitle;
            if (BnbRuleUtils.HasValue(proj.ProjectAbbr))
                projectAbbrev = proj.ProjectAbbr;
            uxSuggestedDocumentName.Text = String.Format("PMPG, {0}, {1}, {2}, {3}",
                country, projectAbbrev, chosenFormAbbrev, DateTime.Today.ToString("dd MMM yyyy"));

            string dmsUser = BnbEngine.SessionManager.GetSessionInfo().LoginName.ToUpper();
            string dmsDept = ProjectPage.GetDepartmentDMSAlias();

            uxUploadDocHyperlink.NavigateUrl = String.Format("../Redirect.aspx?TransferTo=VsoDms&AddDoc=1&ProjectRef={0}&ProjectTitle={1}&DocDescription={2}&User={3}&Dept={4}&Application={5}&DocType={6}",
                proj.ProjectReference, Server.UrlEncode(proj.ProjectTitle), Server.UrlEncode(uxSuggestedDocumentName.Text),
                dmsUser, dmsDept, "WORD", "GENERAL");
            uxUploadDocHyperlink.Target = "_blank";
            uxProjectPageLink.NavigateUrl = String.Format("ProjectPage.aspx?BnbProject={0}",
                proj.ID.ToString());
        }

        private void SetupPanelZero()
        {
            BnbProject proj = BnbProject.Retrieve(new Guid(uxHiddenProjectID.Text));
            uxProjectName.Text = proj.ProjectTitle;
            uxProjectRef.Text = proj.ProjectReference;
            uxPMFStatus.Text = this.GetPMFStatusDescription(proj);
            if (uxHiddenGrantID.Text != "")
            {
                BnbGrant grant = BnbGrant.Retrieve(new Guid(uxHiddenGrantID.Text));
                if (grant.IsContract)
                    uxGrantLabel.Text = "Contract";
                else
                    uxGrantLabel.Text = "Proposal";
                uxGrantDetails.Text = grant.InstanceDescription;
            }
            else
            {
                uxGrantDetails.Text = "";
                uxGrantLabel.Text = "";
            }

            // if a specific doc was requested then show it
            if (uxHiddenDocType.Text != "")
                uxChoosePMPGDocDropdown.SelectedValue = uxHiddenDocType.Text;

            SetDMSSearch();

            uxPanelZeroFeedback.Text = "";
        }

        private string GetPMFStatusDescription(BnbProject proj)
        {
            return BnbEngine.LookupManager.GetLookupItemDescription("lkuPMFStatus", proj.PMFStatusID);
        }

        void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
        {
            if (e.CurrentPanelHtmlID  == "panelZero")
            {
                if (uxChoosePMPGDocDropdown.SelectedValue.ToString() == "-1")
                {
                    uxPanelZeroFeedback.Text = "Please choose a Doc Type before pressing Next.";
                    e.Proceed = false;
                }
                // work out whether a proposal or contact is required
                this.WorkOutIfGrantRequired();
            }
            if (e.CurrentPanelHtmlID == "panelOne")
            {
                if (uxChooseGrantDropdown.SelectedValue.ToString() == "-1")
                {
                    string grantName = "Proposal";
                    if (this.GrantRequired == "2")
                        grantName = "Contract";
                    uxPanelOneFeedback.Text = String.Format("Please choose a {0} before pressing Next.", grantName);
                    e.Proceed = false;
                }
                if (e.Proceed)
                {
                    // record new GrantID
                    uxHiddenGrantID.Text = uxChooseGrantDropdown.SelectedValue;

                }
            }
        }

        private void WorkOutIfGrantRequired()
        {
            BnbLookupRow pmpgRow = this.GetChosenDocLookupRow();
            this.GrantRequired = "0";
            if (pmpgRow != null)
            {
                if ((bool)pmpgRow["IsProposalDocument"])
                    this.GrantRequired = "1";
                if ((bool)pmpgRow["IsContractDocument"])
                    this.GrantRequired = "2";
            }
        }

        public void GenerateProjectFormInWord(Guid projectID, Guid grantID, string filenameForUser, string templateName, string viewName)
        {
            BnbQueryDataSet outputData = GetMergeData(projectID, grantID, viewName);
            // send it to the word button
            uxGeneratePMPGDoc.MergeData = outputData.Tables["Results"];
            uxGeneratePMPGDoc.WordTemplate = AppDomain.CurrentDomain.BaseDirectory + @"Templates/" + templateName;
            uxGeneratePMPGDoc.FilenameForUser = filenameForUser;
            bool success = uxGeneratePMPGDoc.GenerateWordForm();
            if (success)
            {
                BnbWebFormManager.LogAction("WizardGeneratePMPGForms (Word Form)",
                    "Generated PMPG Form", this);
                uxGeneratePMPGDoc.SendResponse(null);
            }

        }

        private BnbQueryDataSet GetMergeData(Guid projectID, Guid grantID, string viewName)
        {            
            BnbQuery q = new BnbQuery(viewName);
            BnbCriteria crit = new BnbCriteria(new BnbListCondition("tblProject_ProjectID", projectID));
            if (grantID != Guid.Empty)
                crit.QueryElements.Add(new BnbListCondition("tblGrant_GrantID", grantID));

            q.Criteria = crit;
            BnbQueryDataSet outputData = q.Execute();
            return outputData;
        }

        protected void uxGeneratePMPGDoc_Clicked(object sender, EventArgs e)
        {
            int chosenDocTypeID = int.Parse(uxChoosePMPGDocDropdown.SelectedValue.ToString());
            BnbLookupDataTable pmpgDocLookup = BnbEngine.LookupManager.GetLookup("lkuPMPGDocumentType");
            BnbLookupRow chosenPmpgRow = pmpgDocLookup.FindByID(chosenDocTypeID);

            Guid projectID = new Guid(uxHiddenProjectID.Text);
            Guid grantID = Guid.Empty;
            if (uxHiddenGrantID.Text != null && uxHiddenGrantID.Text != "")
                grantID = new Guid(uxHiddenGrantID.Text);
            string templateName = chosenPmpgRow["TemplateName"].ToString();
            string viewName = chosenPmpgRow["MailMergeViewName"].ToString();
            this.GenerateProjectFormInWord(projectID, grantID, "PMPGDoc.doc", templateName, viewName);
        }

        private void SetDMSSearch()
        {
            if (uxHiddenProjectID.Text == null || uxHiddenProjectID.Text == "") return;

            BnbProject project = BnbProject.Retrieve(new Guid(uxHiddenProjectID.Text));
            if (project == null) return;
            if (project.ProjectReference == null) return;

            string dmsSearchUsername = ConfigurationSettings.AppSettings["DMSSearchUsername"];
            string dmsSearchPassword = ConfigurationSettings.AppSettings["DMSSearchPassword"];

            dmsSearchControlPMPG.ADUser = dmsSearchUsername;
            dmsSearchControlPMPG.ADPassword = dmsSearchPassword;
            dmsSearchControlPMPG.ProjectReference = project.ProjectReference;
            dmsSearchControlPMPG.DocDescription = "PMPG";
            dmsSearchControlPMPG.ShowWebDMSViewLinks = true;
            // dont show extra links because we dont want a 'new' link!
            dmsSearchControlPMPG.ShowWebDMSMoreLinks = false;

        }


    }
}
