<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="cc1" Namespace="BonoboWebControls.ServicesControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="OrganisationAddressPanel" Src="../UserControls/OrganisationAddressPanel.ascx" %>
<%@ Page language="c#" Codebehind="WizardNewOrganisation.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.WizardNewOrganisation" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML xmlns="http://www.w3.org/1999/xhtml">
	<HEAD>
		<title>WizardNewOrganisation</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../Css/PGMSInformation.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		  <script language="javascript" src="../Javascripts/GoogleMap.js"></script>
	</HEAD>
	<body onload="initialize()" onunload="GUnload()">
		<FORM id="Form2" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<DIV class="wizardheader" ms_positioning="FlowLayout">
					<H3><IMG class="icon24" src="../Images/wizard24silver.gif"> New 
						Organisation&nbsp;Wizard</H3>
				</DIV>
				<asp:panel id="panelZero" runat="server" CssClass="wizardpanel">
					<P><EM>This wizard will help you to add a new&nbsp;Organisation record.</EM>
					<P>Please enter the Country and&nbsp;Name&nbsp;of the new&nbsp;Organisation and 
						press Next</P>
					<P></P>
					
						<TABLE class="wizardtable" id="Table2">
							<TR>
								 <td class="label tooltip" style="height: 15px">                                
                                    <a id="wizardaid" class="tooltip" href="javascript:void(0)">Organisation Country <span
                                        id="wizardsid" class="wizardcustom info">
                                        <img src="../Images/Info_icon.gif" alt="Information" />
                                        Location of organisation HQ<br />
                                        </span></a>                                
                            </td>
								<TD style="HEIGHT: 15px">
									<asp:DropDownList id="dropDownCountry" runat="server" Width="229px"></asp:DropDownList></TD>
							</TR>
							<TR>
								<TD class="label">Organisation Name</TD>
								<TD>
									<asp:TextBox id="textBoxCompanyName" runat="server" Width="240px"></asp:TextBox></TD>
							</TR>
						</TABLE>
					<P>
						<asp:Label ID="labelPanelZeroFeedbackOrgCountry" runat="server" CssClass="feedback" Visible="false" Text="Please select an Organisation Country"></asp:Label><br />
						<asp:label id="labelPanelZeroFeedback" runat="server" CssClass="feedback" Visible="false" Text="Please enter an Organisation Name"></asp:label>                        
					</P>
				</asp:panel><asp:panel id="panelOne" runat="server" CssClass="wizardpanel">
<asp:panel id="panelDupOrg" runat="server">
<P>The following similar&nbsp;Organisations already exist. If any of them match the data you were going to 
							enter, then please use them instead by clicking on the appropriate Select 
							button.</P>
<P>(If the Organisation is not already a Donor then it will be made into one when you press Select)</P>
<P></P>
<P>
							<asp:DataGrid id="dataGridCompanies" runat="server" CssClass="datagrid" AutoGenerateColumns="False">
								<Columns>
									<asp:ButtonColumn Text="Select" ButtonType="PushButton" CommandName="Select"></asp:ButtonColumn>
									<asp:BoundColumn Visible="False" DataField="tblCompany_CompanyID" HeaderText="HiddenID"></asp:BoundColumn>
									<asp:BoundColumn DataField="tblCompany_CompanyName" HeaderText="Organisation Name"></asp:BoundColumn>
									<asp:BoundColumn DataField="lkuCountry_Description" HeaderText="Country"></asp:BoundColumn>
									<asp:BoundColumn DataField="tblAddress_AddressLine1" HeaderText="Address Line 1"></asp:BoundColumn>
									<asp:BoundColumn DataField="tblAddress_AddressLine2" HeaderText="Address Line 2"></asp:BoundColumn>
								</Columns>
							</asp:DataGrid></P>
<P>If none of these existing Organisations match, then press Next to create the 
new Organisation. </asp:panel>
<asp:label id="labelNoDuplicates" runat="server">No similar Organisations already exist. Press Next to continue.</asp:label>
<P>
						<asp:label id="labelPanelOneFeedback" runat="server" CssClass="feedback"></asp:label></P>
						</asp:panel>
				<asp:panel id="panelTwo" runat="server" CssClass="wizardpanel">
					<P>Please enter further details about the new Organisation, then press Finish to 
						create the new record:</P>
					<TABLE class="wizardtable" id="Table4">
						<TR>
							<TD class="label">Organisation Name</TD>
							<TD>
								<asp:TextBox id="textBoxCompanyName2" runat="server" Width="224px"></asp:TextBox></TD>
						</TR>
						<TR>
							<td class="label tooltip" style="height: 15px">
                                <a id="aid" class="tooltip" href="javascript:void(0)">Relationship to VSO <span id="sid" class="custom info">
                                    <img src="../Images/Info_icon.gif" alt="Information" />
                                    Note: Only donors will show in PGMS picklists</span></a>
                        </td>
							<TD>
								<asp:DropDownList id="cmbRelationToVSO" runat="server"></asp:DropDownList></TD>
						</TR>
							<tr>
                                <td class="label">
                                    VSO Primary Contact</td>
                                <td>
                                    <asp:DropDownList ID="drpDownPrimaryContact" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
							<TR>
							<TD class="label">VSO Primary Contact Department</TD>
							<TD>
								<asp:DropDownList id="dropDownDonorContact" runat="server"></asp:DropDownList></TD>
						</TR>
						<tr>
                        <td class="label">
                            Donor Type</td>
                        <td>
                            <asp:DropDownList ID="drpDownFunderAgencyType" runat="server">
                            </asp:DropDownList></td>
                        </tr>						
						<uc1:OrganisationAddressPanel id="uxOrganisationAddressUserControl" runat="server"></uc1:OrganisationAddressPanel></TABLE>
					<P>
						<asp:Label id="lblPanelTwoFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				</asp:panel>
				<P></P>
				<P><uc1:wizardbuttons id="wizardButtons" runat="server"></uc1:wizardbuttons></P>
				<P><asp:label id="labelHiddenRequestIDUnused" runat="server" Visible="False"></asp:label>
					<asp:TextBox id="txtHiddenReferrer" runat="server" Visible="False"></asp:TextBox></P>
			</div>
		</FORM>
	</body>
</HTML>
