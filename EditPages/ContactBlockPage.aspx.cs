using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for ContactBlockPage.
	/// </summary>
	public partial class ContactBlockPage : System.Web.UI.Page
	{
		protected BonoboWebControls.DataControls.BnbDomainObjectHyperlink BnbDomainObjectHyperlink1;
		protected UserControls.Title titleBar;
		protected Frond.UserControls.NavigationBar NavigationBar1;

		private BnbWebFormManager bnb = null;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
#if DEBUG
			if(Request.QueryString.ToString() == "")
				bnb = new BnbWebFormManager("BnbIndividual=7D0466E8-C393-4C58-8B43-0D42B5107DE7&BnbIndividualContactBlock=8CEFE4BB-101A-41CF-B163-A51263967386");
			else
                loadManager();
#else
			loadManager();
#endif
			BnbWorkareaManager.SetPageStyleOfUser(this);
			bnb.LogPageHistory = true;
			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;

			}

			doIndividualHyperlink();
			NavigationBar1.RootDomainObject = "BnbIndividual";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			//bnblckSelectedMail.PreRender+=new EventHandler(bnblckSelectedMail_PreRender);
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}

		#endregion

		private string getIndividual() 
		{
			System.Guid id = new System.Guid(this.Page.Request.QueryString["BnbApplication"]);
			BnbApplication app = (BnbApplication)BonoboEngine.BnbDomainObject.GeneralRetrieve(id,typeof(BnbApplication));
			return app.IndividualID.ToString();
		}

		private void loadManager() 
		{
			// Put user code to initialize the page here
			string qs = Page.Request.QueryString.ToString();
			string appid = Page.Request.QueryString["BnbApplication"];
			if(appid!=null)
			{
				qs = qs.Replace("BnbApplication="+appid,"BnbIndividual="+getIndividual());
				//bnb = new BnbWebFormManager(qs);
				BnbWebFormManager.ReloadPage(qs);
			}
			else 
			{
				bnb = new BnbWebFormManager(this,"BnbIndividualContactBlock");
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			// in new mode
			if (BnbWebFormManager.IsPageDomainObjectNew("BnbIndividualContactBlock"))
			{
				// show selected mail controls if combo set to selected mail
				if(bnblckBlockingType.SelectedValue != null && 
					bnblckBlockingType.SelectedValue.Equals(1001))
				{
					bnblckSelectedMail.Visible = true;
					BnbDataGridForDomainObjectList1.Visible = true; 
				}
				else
				{
					bnblckSelectedMail.Visible = false;
					BnbDataGridForDomainObjectList1.Visible = false;
				}
			}
			else
			{
				// set default visibility
				BnbDataGridForDomainObjectList1.Visible = true;
				bnblckSelectedMail.Visible = false;
				// if not new mode, check the value of the data
				BnbIndividualContactBlock icb = bnb.GetPageDomainObject("BnbIndividualContactBlock") as BnbIndividualContactBlock;
				// if not selected mail then hide grid too
				if (icb != null && icb.ContactBlockTypeID != BnbConst.ContactBlockType_AllowSelectedMail)
				{
					BnbDataGridForDomainObjectList1.Visible = false;
				}
				
			}

			base.OnPreRender (e);
		}

		private void doIndividualHyperlink()
		{
			string indvValue = Page.Request.QueryString["BnbIndividual"];
			
			if (indvValue != null && indvValue.ToLower() != "new")
			{
				BnbIndividual tempIndv = BnbIndividual.Retrieve(new Guid(indvValue));
				hlkIndividual.Text = tempIndv.FullName;
				hlkIndividual.NavigateUrl = String.Format("IndividualPage.aspx?BnbIndividual={0}",
						tempIndv.ID.ToString().ToLower());
			}
		}

		
	}
}
