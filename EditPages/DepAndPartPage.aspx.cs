using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboDomainObjects;
using BonoboWebControls;
using BonoboEngine;


namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for DepAndPartPage.
	/// </summary>
	public partial class DepAndPartPage : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
		protected Frond.UserControls.NavigationBar NavigationBar1;
		private BnbWebFormManager bnb=null;
		protected void Page_Load(object sender, System.EventArgs e)
		{

			// this must be done before the webformmanager is set up
			doCommutativeBinding();

#if DEBUG	
			if (Request.QueryString.ToString() == "")
				BnbWebFormManager.ReloadPage("BnbIndividual=D72BFDD9-AADF-11D4-908F-00508BACE998&BnbRelative=94C586B4-6492-409C-AFE7-6E00DADFE655");
			else
				bnb = new BnbWebFormManager(this,"BnbRelative");
#else	
			bnb = new BnbWebFormManager(this,"BnbRelative");
#endif
			bnb.LogPageHistory = true;
			bnb.PageNameOverride = "Dependants and Partners";
			BnbWorkareaManager.SetPageStyleOfUser(this);

			NavigationBar1.RootDomainObject = "BnbIndividual";
			
			if (!this.IsPostBack)
			{
				titleBar.TitleText = bnb.PageNameOverride;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			this.PreRender +=new EventHandler(DepAndPartPage_PreRender);
			base.OnInit(e);

		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void DepAndPartPage_PreRender(object sender, EventArgs e)
		{
			doDisableIndividualFields();
		}

		private void doDisableIndividualFields()
		{
			BnbRelative relative = bnb.GetPageDomainObject("BnbRelative") as BnbRelative ;
			// is the relative a proper record with additional data?
			if (relative.RelatedIndividual.Additional != null)
			{
				// disable forename, surname, dob because it can be edited on the Individual page instead
				uxForename.ControlType  = BonoboWebControls.DataControls.TextBoxControlType.Label;
				uxSurname.ControlType  = BonoboWebControls.DataControls.TextBoxControlType.Label;
				uxDOB.Visible = false;
				uxDOBLabel.Visible  = true;
			}

		}

		/// <summary>
		/// BnbRelative links to two individuals.
		/// The one we want to bind the fields on depends on which Individual page we came from
		/// (which can be worked out be looking at the BnbIndividual queryparam)
		/// So the datacontrols are bound by this routine to the appropriate Individual.
		/// </summary>
		private void doCommutativeBinding()
		{
			BnbEngine.SessionManager.ClearObjectCache();
			Guid individualID = new Guid(Request.QueryString["BnbIndividual"]);
			Guid relativeID = new Guid(Request.QueryString["BnbRelative"]);
			BnbRelative relative =BnbRelative.Retrieve(relativeID);		
			// by default we show the BnbRelative.RelatedIndividual
			string displayIndividual = "RelatedIndividual";
			BnbIndividual fromIndv = relative.Individual;
			BnbIndividual toIndv = relative.RelatedIndividual;
			// but if we came from them, we show BnbRelative.Individual instead
			if (relative.RelatedIndividual.ID.ToString() == individualID.ToString())
			{
				displayIndividual = "Individual";
				fromIndv = relative.RelatedIndividual;
				toIndv = relative.Individual;
			}
			
			// do hyperlinks
			uxRelativeFromHyper.NavigateUrl="./IndividualPage.aspx?BnbIndividual=" + fromIndv.ID;
			uxRelativeFromHyper.Text = fromIndv.InstanceDescription;

			uxRelativeToHyper.Text = toIndv.InstanceDescription;
			// only set hyperlink target if the indv is a proper record with additional data
			if (toIndv.Additional != null)
				uxRelativeToHyper.NavigateUrl="./IndividualPage.aspx?BnbIndividual=" + toIndv.ID;
				
			// set binding on controls
			uxForename.DataMember = String.Format("BnbRelative.{0}.Forename",displayIndividual);
			uxSurname.DataMember = String.Format("BnbRelative.{0}.Surname", displayIndividual);
			uxDOB.DataMember = String.Format("BnbRelative.{0}.DateOfBirth",displayIndividual);
			// also copy DOB to hidden label
			uxDOBLabel.Text = toIndv.DateOfBirth.ToString("dd/MMM/yyyy");
			uxAge.DataMember = String.Format("BnbRelative.{0}.Age",displayIndividual);

			if (relative.MostRecentRelativeApplication!=null)
			{
				uxIntendedLocation.DataMember="BnbRelative.MostRecentRelativeApplication.IntendedLocationID";
				uxActualLocation.DataMember="BnbRelative.MostRecentRelativeApplication.ActualLocationID";
				uxIntendedLocation.Visible=true;
				uxActualLocation.Visible=true;
			}
			else
			{
				uxIntendedLocation.DataMember="";
				uxIntendedLocation.Visible=false;
				uxActualLocation.DataMember="";
				uxActualLocation.Visible=false;
			}


		}
	}
}
