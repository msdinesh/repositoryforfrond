using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;


namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for NIBatchDetailPage.
	/// </summary> 
	public partial class NIBatchDetailPage : System.Web.UI.Page
	{

		protected Frond.UserControls.NavigationBar NavigationBar1;
		protected UserControls.Title titleBar;
		private BnbWebFormManager bnb;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			bnb = new BnbWebFormManager(this,"BnbNIBatchDetail");
			bnb.LogPageHistory = true;
			BnbWorkareaManager.SetPageStyleOfUser(this);
			BnbNIBatchDetail nid = bnb.GetPageDomainObject("BnbNIBatchDetail",true) as BnbNIBatchDetail;
			if (nid != null)
			{
				string appID = nid.ApplicationID.ToString();
				NavigationBar1.RootDomainObject = "BnbApplication";		
				NavigationBar1.RootDomainObjectID = appID;
			}
			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;

			}

			
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
