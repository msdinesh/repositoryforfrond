<%@ Page Language="C#" AutoEventWireup="true" Codebehind="FindGeographical.aspx.cs"
    Inherits="Frond.FindGeographical" %>

<%@ Register Src="../UserControls/WaitMessagePanel.ascx" TagName="WaitMessagePanel"
    TagPrefix="uc1" %>
<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.GenericControls"
    TagPrefix="bnbgenericcontrols" %>
<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.DataGrids"
    TagPrefix="bnbdatagrid" %>
<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.DataControls"
    TagPrefix="bnbdatacontrol" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.GMapControl"
    TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Find Geographical</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
    <script language="javascript" src="../Javascripts/GoogleMap.js"></script>
</head>
<body onload="initialize()" onunload="GUnload()">

    <form defaultbutton="btnFind" id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/batchprocess24silver.gif">
            </uc1:Title>
            <div style="float: left" id="find">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td colspan="2">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td width="25%">
                        </td>
                        <td align="left" width="75%">
                            Country &nbsp; &nbsp;
                            <asp:DropDownList ID="ddlCountry" runat="server" Width="200px">
                            </asp:DropDownList>
                            &nbsp; &nbsp; &nbsp;&nbsp;
                            <asp:RadioButton ID="rdoVolunteer" runat="server" GroupName="Find" Text="Volunteer"
                                Checked="True" />
                            <asp:RadioButton ID="rdoEmployer" GroupName="Find" runat="server" Text="Employer" />
                            &nbsp; &nbsp;
                            <asp:Button ID="btnFind" runat="server" Text="Find" OnClick="btnFind_Click" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="25%">
                        </td>
                        <td align="left" width="75%">
                            <asp:Label ID="gErrorMessage" runat="server" CssClass="feedback"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="tdResultText" runat="server" visible="false">
                            Results
                            <asp:Label CssClass="boldText" ID="sRow" runat="server"></asp:Label>
                            -
                            <asp:Label CssClass="boldText" ID="eRow" runat="server"></asp:Label>
                            of about
                            <asp:Label CssClass="boldText" ID="tRow" runat="server"></asp:Label>
                            <asp:Label CssClass="boldText" ID="sType" runat="server"></asp:Label>
                            found in
                            <asp:Label CssClass="boldText" ID="countryName" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2" id="tdPagination" runat="server" visible="false">
                            <br />
                            <asp:LinkButton runat="server" ID="lbtnFirst" Text="First" Enabled="false" OnClick="First_Click"></asp:LinkButton>&nbsp;&nbsp;
                            <asp:LinkButton runat="server" ID="lbtnNext" Text="Next" OnClick="Next_Click"></asp:LinkButton>&nbsp;&nbsp;
                            <asp:LinkButton runat="server" ID="lbtnPrev" Text="Prev" Enabled="false" OnClick="Prev_Click"></asp:LinkButton>&nbsp;&nbsp;
                            <asp:LinkButton runat="server" ID="lbtnLast" Text="Last" OnClick="Last_Click"></asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td class="slist">
                            <div class="divScroll">
                                <table id="detailsTable" runat="server">
                                </table>
                            </div>
                        </td>
                        <td class="sMap">
                            <cc1:GoogleMap runat="server" ID="googleMap" Height="450px">
                            </cc1:GoogleMap>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
            <uc1:WaitMessagePanel ID="uxWaitMessagePanel" runat="server" DivToHide="find"></uc1:WaitMessagePanel>
        </div>
    </form>
</body>
</html>
