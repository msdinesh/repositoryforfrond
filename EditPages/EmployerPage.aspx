<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>

<%@ Page Language="c#" Codebehind="EmployerPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.EmployerPage"
    Trace="false" %>

<%@ Register Src="../UserControls/NavigationBar.ascx" TagName="NavigationBar" TagPrefix="uc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/bnbemployer24silver.gif">
            </uc1:Title>
            <uc2:NavigationBar ID="navigationBar" runat="server"></uc2:NavigationBar>

                <table style="margin: 0px" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td align="left" width="50%">
                            <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons1" runat="server" NewButtonEnabled="True"
                                CssClass="databuttons" UndoText="Undo" SaveText="Save" EditAllText="Edit All"
                                NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px"
                                NewWidth="55px"></bnbpagecontrol:BnbDataButtons>
                        </td>       
                        <td align="right" width="50%">
                            <a href="../FindGeneral.aspx?FindTypeID=0" target="_top">Find Employer</a></td>
                    </tr>
                </table>

                <bnbpagecontrol:BnbMessageBox ID="BnbMessageBox1" runat="server" CssClass="messagebox">
                </bnbpagecontrol:BnbMessageBox>
        
            <bnbpagecontrol:BnbTabControl ID="BnbTabControl1" runat="server"></bnbpagecontrol:BnbTabControl>
            <asp:Panel ID="EmployerDetailsTab" runat="server" CssClass="tcTab" BnbTabName="Employer Details">
                <p>
                    <table id="Table1" width="100%" border="0" class="fieldtable">
                        <tr>
                            <td class="label" style="height: 11px" width="25%">
                                Country</td>
                            <td style="height: 11px" width="25%">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl1" runat="server" CssClass="lookupcontrol"
                                    Width="250px" Height="22px" DataMember="BnbEmployer.CountryID" IsBoundToChildLookupControl="True"
                                    AutoPostBack="True" LookupControlType="ComboBox" ControlType="ComboBox" ListBoxRows="4"
                                    LookupTableName="lkuCountry" DataProperty="BnbEmployer.CountryID" DomainObject="BnbEmployer"
                                    DataTextField="Description"></bnbdatacontrol:BnbLookupControl>
                            </td>
                            <td class="label" style="height: 11px" width="25%">
                                Province</td>
                            <td style="height: 11px" width="25%">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl3" runat="server" CssClass="lookupcontrol"
                                    Width="250px" Height="22px" DataMember="BnbEmployer.ProvinceID" LookupControlType="ComboBox"
                                    ControlType="ComboBox" ListBoxRows="4" LookupTableName="lkuProvince" DataProperty="BnbEmployer.ProvinceID"
                                    DomainObject="BnbEmployer" DataTextField="Description" ColumnRelatedToParent="CountryID"
                                    ParentControlID="BnbLookupControl1" ParentLookupControlID="BnbLookupControl1"></bnbdatacontrol:BnbLookupControl>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%">
                                Employer Name</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox1" runat="server" CssClass="textbox" Width="250px"
                                    Height="20px" DataMember="BnbEmployer.EmployerName" DataProperty="BnbEmployer.EmployerName"
                                    DomainObject="BnbEmployer" ReadOnly="False" MultiLine="False" Rows="0"></bnbdatacontrol:BnbTextBox>
                            </td>
                            <td class="label" width="25%">
                                Employer Ref</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox4" runat="server" CssClass="textbox" Width="250px"
                                    Height="20px" DataMember="BnbEmployer.FullEmployerReference" DataProperty="BnbEmployer.FullEmployerReference"
                                    DomainObject="BnbEmployer" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="height: 38px" width="25%">
                                Organisation</td>
                            <td style="height: 38px" width="25%">
                                <p>
                                    <bnbdatacontrol:BnbSearchDomainObjectComposite ID="BnbSearchDomainObjectComposite1"
                                        runat="server" Width="240px" DataMember="BnbEmployer.Organisation" BonoboViewFieldToLookUp="tblOrganisation_OrganisationName"
                                        BonoboViewFieldToBeBound="tblOrganisation_OrganisationID" BonoboViewName="vwBonoboOrganisationLinker"
                                        DomainObjectType="BnbOrganisation" ChangeButtonVisible="True" AddButtonVisible="True">
                                    </bnbdatacontrol:BnbSearchDomainObjectComposite>
                                </p>
                            </td>
                            <td class="label" style="height: 38px" width="25%">
                                Employer Type</td>
                            <td style="height: 38px" width="25%">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl2" runat="server" CssClass="lookupcontrol"
                                    Width="250px" Height="22px" DataMember="BnbEmployer.EmployerTypeID" LookupControlType="ComboBox"
                                    ControlType="ComboBox" ListBoxRows="4" LookupTableName="lkuEmployerType" DataProperty="BnbEmployer.EmployerTypeID"
                                    DomainObject="BnbEmployer" DataTextField="Description"></bnbdatacontrol:BnbLookupControl>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="height: 9px" width="25%">
                                Employer Short Name</td>
                            <td style="height: 9px" width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox2" runat="server" CssClass="textbox" Width="250px"
                                    Height="20px" DataMember="BnbEmployer.EmployerShortName" DataProperty="BnbEmployer.EmployerShortName"
                                    DomainObject="BnbEmployer" MultiLine="false"></bnbdatacontrol:BnbTextBox>
                            </td>
                            <td class="label" style="height: 9px" width="25%">
                            </td>
                            <td style="height: 9px" width="25%">
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%">
                                Employer Comments</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox3" runat="server" CssClass="textbox" Width="250px"
                                    Height="22px" DataMember="BnbEmployer.Comments" DataProperty="BnbEmployer.Comments"
                                    DomainObject="BnbEmployer" MultiLine="True" Rows="3"></bnbdatacontrol:BnbTextBox>
                            </td>
                            <td class="label" width="25%">
                            </td>
                            <td width="25%">
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%">
                                Logged By</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl4" runat="server" CssClass="lookupcontrol"
                                    Width="250px" Height="22px" DataMember="BnbEmployer.CreatedBy" LookupControlType="ComboBox"
                                    ControlType="ComboBox" ListBoxRows="4" LookupTableName="vlkuBonoboUser" DataProperty="BnbEmployer.CreatedBy"
                                    DomainObject="BnbEmployer" DataTextField="Description"></bnbdatacontrol:BnbLookupControl>
                            </td>
                            <td class="label" width="25%">
                                Logged On</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbDateBox ID="BnbDateBox1" runat="server" CssClass="datebox" Width="90px"
                                    Height="22px" DataMember="BnbEmployer.CreatedOn" DataProperty="BnbEmployer.CreatedOn"
                                    DomainObject="BnbEmployer" ReadOnly="True" DateCulture="en-GB" DateFormat="dd/mm/yyyy"
                                    AlwaysReadOnly="False"></bnbdatacontrol:BnbDateBox>
                            </td>
                        </tr>
                    </table>
                </p>
            </asp:Panel>
            <asp:Panel ID="AddressesTab" runat="server" CssClass="tcTab" BnbTabName="Addresses">
                <p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView1" runat="server" CssClass="datagrid"
                        Width="100%" DomainObject="BnbEmployerAddress" QueryStringKey="BnbEmployer" FieldName="tblEmployer_EmployerID"
                        ViewLinksVisible="True" NewLinkText="New Address" NewLinkEnabled="True" HrefForViewHLink="EmployerAddressPage.aspx?BnbEmployerAddress="
                        ViewHLinkGuid="EmployerAddressID" ViewName="vwBonoboEmployerAddress" ViewLinkGuid="AddressID"
                        UrlForViewLink="EmployerAddressPage?BnbEmployerAddress=" QueryStringGuid="BnbEmployer"
                        CssClassForTableHeading="datagridviewforheading" CssClassForHeaderRow="datagridforviewheaderrow"
                        CssClassForDataRows="datagridforviewdatarows" Guid="EmployerAddressID" GuidQueryString="BnbEmployer"
                        GuidKey="lnkEmployerAddress_EmployerAddressID" Href="EmployerAddressPage.aspx"
                        RedirectPage="EmployerAddressPage.aspx" NewLinkVisible="True"></bnbdatagrid:BnbDataGridForView>
                </p>
            </asp:Panel>
            <asp:Panel ID="JobsTab" runat="server" CssClass="tcTab" BnbTabName="Jobs">
                <p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView2" runat="server" CssClass="datagrid"
                        Width="100%" DomainObject="BnbRole" QueryStringKey="BnbEmployer" FieldName="tblEmployer_EmployerID"
                        ViewLinksVisible="true" NewLinkText="New Job..." ViewName="vwBonoboEmployerRole"
                        GuidKey="tblRole_RoleID" RedirectPage="JobPage.aspx" NewLinkVisible="true" DisableRedirectToParentPage="True"
                        NewLinkReturnToParentPageDisabled="True"></bnbdatagrid:BnbDataGridForView>
                </p>
            </asp:Panel>
            <p>
                &nbsp;</p>
        </div>
    </form>
</body>
</html>
