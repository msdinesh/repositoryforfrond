using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboWebControls;
using BonoboDomainObjects;
using BonoboWebControls.DataControls;
using BonoboWebControls.PageControls;
using BonoboWebControls.GenericControls;
using BonoboWebControls.DataGrids;
using BonoboWebControls.Collections;
using System.Text;

namespace Frond.EditPages
{
    public partial class ProposalPage : System.Web.UI.Page
    {
        private BnbGrant m_grant = null;
        private BnbWebFormManager bnb = null;
        private BnbProject m_project = null;


        protected void Page_Load(object sender, EventArgs e)
        {
            #if DEBUG
			if(Request.QueryString.ToString() == "")
			{
				BnbWebFormManager.ReloadPage("BnbGrant=631572ea-e308-401f-ad70-42f9bb3c2a7a");
			}
#endif
			bnb = new BnbWebFormManager(this,"BnbGrant");

			string querystring = Page.Request.QueryString.ToString();

			//If Project is missing in Querystring, reload page with it using the BnbGrant.Project.ID
			if(querystring.IndexOf("BnbProject") == -1) 
			{
				System.Guid projguid = ((BnbGrant)bnb.ObjectTree.DomainObjectDictionary["BnbGrant"]).Project.ID;
				BnbWebFormManager.ReloadPage("BnbProject="+projguid+"&"+querystring);
			}
            // if new, re-direct to wizard
            if (BnbWebFormManager.IsPageDomainObjectNew("BnbGrant"))
            {
                string projectID = Request.QueryString["BnbProject"];
                Response.Redirect(String.Format("WizardNewProposal.aspx?BnbProject={0}", projectID));
            } 
            

			bnb.PageNameOverride = "Proposal";
			bnb.LogPageHistory=true;
            BnbWorkareaManager.SetPageStyleOfUser(this);

			//<!-- PGM-204
			doDisplayDeletedCheckBox();
			//-->
            m_grant = (BnbGrant)bnb.ObjectTree.DomainObjectDictionary["BnbGrant"];

            doContractHyperlinks();
            doPMPGHyperlink();
            doPMFStatus();
            doNewProposal();

            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null && m_grant != null)
                {
                    StringBuilder sb = new StringBuilder(Utilities.ProjectSubHlink);
                    string projID = "#";
                    string projDesc = string.Empty;
                    string projAbbr = string.Empty;

                    if (m_grant.Project == null)
                    {
                        BnbProject proj = (BnbProject)bnb.GetPageDomainObject("BnbProject");
                        projID = proj.ID.ToString();
                        projDesc = m_grant.Project.ProjectTitle;
                        projAbbr = m_grant.Project.ProjectAbbr;
                    }
                    else
                    {
                        projID = m_grant.Project.ID.ToString();
                        projDesc = m_grant.Project.ProjectTitle;
                        projAbbr = m_grant.Project.ProjectAbbr;
                    }

                    sb.Replace("[PROJECTID]", projID);
                    sb.Replace("[TITLE]", projDesc);
                    string trimPageTitle = bnb.PageTitleDescriptionCurrent.Replace(projDesc + '/', "");
                    string pageTitle = trimPageTitle.Replace(projAbbr + '/', "");
                    sb.Replace("[SUBTITLE]", pageTitle);
                    titleBar.TitleText = sb.ToString();
                    spnDonorCurrency.InnerText = m_grant.AmountRequestedCurrency.ToString("n0");
                }
                //Identify the url and show the related tab when the user Click the ClaimsAndReports in contract and proposal grid
                if (this.BnbTabControl1.SelectedTabID == "")
                {
                    if (this.Request != null)
                    {
                        string url = this.Request.Url.ToString();
                        if (url.IndexOf("ControlID") > -1)
                        {
                            if (url.IndexOf("Main") > -1)
                                BnbTabControl1.SelectedTabID = Main.ID;
                        }
                    }
                }
                
            }

           
		}

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            this.Page.PreRenderComplete += new EventHandler(ProposalPage_PreRenderComplete);
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        private void doPMPGHyperlink()
        {
            BnbGrant grant = BnbWebFormManager.StaticGetPageDomainObject("BnbGrant") as BnbGrant;
            if (grant != null)
            {
                // need to pass grant id to wizard
                uxGeneratePMPGForm.NavigateUrl = String.Format("WizardGeneratePMPGForms.aspx?BnbProject={0}&BnbGrant={1}&DocType={2}",
                    grant.ProjectID.ToString(), grant.ID.ToString(), 2001);
            }
        }

        private void doContractHyperlinks()
        {
            string s = bnb.GetQueryStringFromCurrentPageWithDomainObjectKeyValuesOnly();

            m_project = (BnbProject)bnb.ObjectTree.DomainObjectDictionary["BnbProject"];
           
            BnbCompany org = m_grant.Company;

            bnbhlOrganisationDonor.Visible = (org != null);
            bnbsdoOrganisationDonor.Visible = (org == null);

            hplEditContract.NavigateUrl = "./ContractPage.aspx?" + s + "&Ed=1";
            btnProposalToContract.Visible = !m_grant.IsContract;
            hlProposalToContract.Visible = !m_grant.IsContract;
            hlProposalToContract.NavigateUrl = "WizardProposalToContract.aspx?" + s + "&BnbGrantProgress=new&PageState=new&DisableRedirect=true&Wizard=True";

            if (m_grant.IsContract)
            {
                hlkContract.Visible = true;
                hlkContract.NavigateUrl = "./ContractPage.aspx?" + s;
            }
            else
            {
                hlkContract.Visible = false;
            }
        }


        protected void bnbdgProposalHistory_PreRender(object sender, EventArgs e)
        {
            bool HasContract = false;
            //filters the list of proposals for the grid
            BnbDomainObjectCollection bnbcol = new BnbDomainObjectCollection();
            BnbGrant grant = (BnbGrant)bnb.ObjectTree.DomainObjectDictionary["BnbGrant"];

            foreach (BnbDomainObject bnbitem in bnbdgProposalHistory.ObjectTree.AllDomainObjects)
            {
                bnbcol.Add(bnbitem);
            }

            bnbdgProposalHistory.ObjectTree.AllDomainObjects.Clear();
            bnbdgProposalHistory.ObjectTree.DomainObjectDictionary.Clear();

            int x = 0;
            for (int i = 0; i < bnbcol.Count; i++)
            {
                BnbGrantProgress progressitem = (BnbGrantProgress)bnbcol[i];
                if (progressitem.IsProposal || progressitem.IsNew)
                {
                    x = (progressitem.IsNew) ? -1 : x;
                    bnbdgProposalHistory.ObjectTree.AllDomainObjects.Add(progressitem);
                    bnbdgProposalHistory.ObjectTree.DomainObjectDictionary.Add(bnbdgProposalHistory.ID + "_r" + x, progressitem);
                    x++;
                }
                if (progressitem.IsContract)
                {
                    HasContract = true;
                }
            }

            bnbdgProposalHistory.ObjectTree.BindDomainObjectsToDataControls();

            if (HasContract)
            {
                hlkContract.Visible = true;
                hplEditContract.Visible = true;
            }

            if (!bnbdgProposalHistory.WasAddButtonClicked) return;

            BnbLookupControl lucStatus = (BnbLookupControl)this.FindControl("bnbdgProposalHistory_r-1_c0");
            if (lucStatus == null) return;

            //status
            lucStatus.SelectedValue = -1;
            BnbGrantProgress progress = grant.CurrentProposal;

            if (progress == null) return;

            //start date
            BnbDateBox dbStartDate = (BnbDateBox)this.FindControl("bnbdgProposalHistory_r-1_c1");
            dbStartDate.Date = progress.StartDate;

        }

        
        protected void ProposalPage_PreRender(object sender, EventArgs e)
        {
            IfContractReDirectToContractPage();   
            hplEditContract.Visible = false;

            bnbsdoOrganisationDonor.AllowWrite = true;
        }

        private void doDisplayDeletedCheckBox()
        {
            pnlDeleted.Visible = (BnbEngine.SessionManager.GetSessionInfo().HasPermission(1047));
        }

        //<!-- PGM-236
        protected void Regions_PreRender(object sender, EventArgs e)
        {
            lblRegions.Text = m_project.GetRegionDescriptions();
        }
        //-->
        protected void ProposalPage_PreRenderComplete(object sender, EventArgs e)
        {
            CheckExpectedAnnualIncome(m_grant);
        }
        //<!-- PGM-95
        protected void lblGlobal_PreRender(object sender, EventArgs e)
        {
            lblGlobal.Text = (m_project.Global) ? "Yes" : "No";
        }

       
        //-->

        private void IfContractReDirectToContractPage()
        {
            string s = bnb.GetQueryStringFromCurrentPageWithDomainObjectKeyValuesOnly();
            m_grant = (BnbGrant)bnb.ObjectTree.DomainObjectDictionary["BnbGrant"];

            if (m_grant.IsContract)
            {
                string proposal = this.Request.QueryString.ToString();
                if (!(proposal.IndexOf("Main") > -1))
                {
                    if (!(proposal.IndexOf("proposal") > -1))
                        Response.Redirect("./ContractPage.aspx?" + s);
                }
            }

        }

        private void doPMFStatus()
        {
            BnbProject proj = bnb.GetPageDomainObject("BnbProject") as BnbProject;
            if (proj != null)
            {
                PMFStatusPanel2.PMFStatusID = proj.PMFStatusID.ToString();
            }

        }

        //PGMSMIFI-131
        private void doNewProposal()
        {
            if (m_project.CurrentProjectStatus.ProjectStatusID == BnbConst.ProjectStatus_Closed)
                BnbDataButtons1.NewButtonEnabled = false;
            else
                BnbDataButtons1.NewButtonEnabled = true;
        }

        /// <summary>
        /// To show comma's in Expected Annual Income Currency in the Proposal income pipeline section
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bnbdgIncomePipeline_PreRender(object sender, EventArgs e)
        {
            BnbDecimalBox expectedAnnualIncomeCurrency = (BnbDecimalBox)this.FindControl("bnbdgIncomePipeline_r-1_c1");
            expectedAnnualIncomeCurrency.FormatExpression = "#,0";
        }

        /// <summary>
        /// Show view warning message if total of all expected annual income currency is less than the 'Requested (Donor Currency)'
        /// </summary>
        /// <param name="grant"></param>
        private void CheckExpectedAnnualIncome(BnbGrant grant)
        {
            Decimal totalAnnualIncome = Decimal.Zero;
            foreach (BnbGrantIncomePipeline gip in grant.GrantIncomePipelineList)
                totalAnnualIncome += gip.ExpectedAnnualIncomeCurrency;

            if (totalAnnualIncome != grant.AmountRequestedCurrency)
            {
                spnViewWarning.Visible = true;
                spnViewWarning.InnerText = "The rows in the Phasing section do not add up to the total requested budget.  Please go back and correct this in the Phasing section";
            }
            else
                spnViewWarning.Visible = false;
        }

        protected void btnProposalToContract_Click(object sender, EventArgs e)
        {
            string s = bnb.GetQueryStringFromCurrentPageWithDomainObjectKeyValuesOnly();
            m_project = (BnbProject)bnb.ObjectTree.DomainObjectDictionary["BnbProject"];
            BnbCompany org = m_grant.Company;
            string NavigateUrl = "WizardProposalToContract.aspx?" + s + "&BnbGrantProgress=new&PageState=new&DisableRedirect=true&Wizard=True";
            Response.Redirect(NavigateUrl);
        }

       
    }    
}

