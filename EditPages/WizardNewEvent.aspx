<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Page language="c#" Codebehind="WizardNewEvent.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.WizzardNewEvent" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Wizard New Event</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<DIV class="wizardheader" ms_positioning="FlowLayout">
					<H3><IMG class="icon24" src="../Images/wizard24silver.gif"> New Event Wizard</H3>
				</DIV>
				<asp:panel id="panelTwo" runat="server" CssClass="wizardpanel">
					<P>Please enter details about the new Event, then press Finish to create the new 
						record:
						<asp:ValidationSummary id="lblValidationSummary1" runat="server"></asp:ValidationSummary></P>
					<TABLE class="wizardtable" id="Table4">
						<TR>
							<TD class="label">Event Reference</TD>
							<TD>
								<asp:TextBox id="txtEventReference2" runat="server" Width="224px"></asp:TextBox>
								<asp:RequiredFieldValidator id="lblReference2Validation" runat="server" ErrorMessage="Event reference is required"
									ControlToValidate="txtEventReference2" Display="Dynamic">*</asp:RequiredFieldValidator></TD>
						</TR>
						<TR>
							<TD class="label">Event Group</TD>
							<TD>
								<asp:DropDownList id="cmbEventGroup2" runat="server" AutoPostBack="True" onselectedindexchanged="cmbEventGroup2_SelectedIndexChanged"></asp:DropDownList>
								<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ErrorMessage="Event group required"
									ControlToValidate="cmbEventGroup2">*</asp:RequiredFieldValidator></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 21px">Event Type</TD>
							<TD style="HEIGHT: 21px">
								<asp:DropDownList id="cmbEventType" runat="server"></asp:DropDownList>
								<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ErrorMessage="Event Type required" ControlToValidate="cmbEventType"
									Display="Dynamic">*</asp:RequiredFieldValidator></TD>
						</TR>
						<TR>
							<TD class="label">Event Country</TD>
							<TD>
								<asp:DropDownList id="cmbCountry" runat="server" Width="224px" AutoPostBack="True" onselectedindexchanged="cmbCountry_SelectedIndexChanged"></asp:DropDownList>
								<asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" ErrorMessage="Event Country required"
									ControlToValidate="cmbCountry" Display="Dynamic">*</asp:RequiredFieldValidator></TD>
						</TR>
						<TR>
							<TD class="label">Site</TD>
							<TD>
								<asp:DropDownList id="cmbEventSite" runat="server" AutoPostBack="True" onselectedindexchanged="cmbEventSite_SelectedIndexChanged"></asp:DropDownList>
								<asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" ErrorMessage="Site required" ControlToValidate="cmbEventSite"
									Display="Dynamic">*</asp:RequiredFieldValidator></TD>
						</TR>
						<TR>
							<TD class="label">Location</TD>
							<TD>
								<asp:DropDownList id="cmbLocation" runat="server"></asp:DropDownList>
								<asp:RequiredFieldValidator id="RequiredFieldValidator5" runat="server" ErrorMessage="Location required" ControlToValidate="cmbLocation"
									Display="Dynamic">*</asp:RequiredFieldValidator></TD>
						</TR>
						<TR>
							<TD class="label">Date From</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneDateBox id="dtpDateFrom" runat="server" CssClass="datebox" Width="90px" DateFormat="dd/MMM/yyyy"
									DateCulture="en-GB" Height="20px"></bnbdatacontrol:BnbStandaloneDateBox></TD>
						</TR>
						<TR>
							<TD class="label">Date To</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneDateBox id="dtpDateTo" runat="server" CssClass="datebox" Width="90px" DateFormat="dd/MMM/yyyy"
									DateCulture="en-GB" Height="20px"></bnbdatacontrol:BnbStandaloneDateBox></TD>
						</TR>
					</TABLE>
					<P>
						<asp:Label id="lblPanelTwoFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				</asp:panel>
				<P></P>
				<P><uc1:wizardbuttons id="wizardButtons" runat="server"></uc1:wizardbuttons></P>
				<P><asp:label id="labelHiddenRequestIDUnused" runat="server" Visible="False"></asp:label>
					<asp:TextBox id="txtHiddenReferrer" runat="server" Visible="False"></asp:TextBox></P>
				<p></p>
			</div>
		</form>
	</body>
</HTML>
