<%@ Register TagPrefix="cc1" Namespace="BonoboWebControls.ServicesControls" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="RegistryLabelsUtil.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.RegistryLabelsUtil" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="bnbcriteriacontrol" Namespace="BonoboWebControls.CriteriaControls" Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Registry Labels</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent"><uc1:title id="titleBar" runat="server" imagesource="../Images/batchprocess24silver.gif"></uc1:title>
			<p>Use the 'Find Volunteers' tab to find the volunteers that you want to print labels for. Press the 'Select' button next to each one that you want to select. The list of selected volunteers
			will be shown in the 'Labels List' tab. Press the 'Generate Registry Labels' button to download the labels in a Word document.</p>
			<bnbpagecontrol:bnbtabcontrol id="BnbTabControl1" runat="server" IgnoreWorkareas="true"></bnbpagecontrol:bnbtabcontrol>
			<asp:panel id="LabelsListTab" runat="server" Height="238px" Width="800px" BnbTabName="Labels List"
					CssClass="tcTab">
					<P>
						<asp:DataGrid id="MailMergeDataGrid" runat="server" CssClass="DataGrid" AutoGenerateColumns="False">
							<HeaderStyle CssClass="datagridheader"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn>
									<ItemTemplate>
										<asp:Button id="RemoveButton" runat="server" Text="Remove" CausesValidation="false" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"ApplicationID")%>'>
										</asp:Button>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="Forename" HeaderText="Forename"></asp:BoundColumn>
								<asp:BoundColumn DataField="Surname" HeaderText="Surname"></asp:BoundColumn>
								<asp:BoundColumn DataField="ReferenceNo" HeaderText="Vol Ref"></asp:BoundColumn>
								<asp:BoundColumn DataField="Salutation" HeaderText="Salutation"></asp:BoundColumn>
								<asp:BoundColumn DataField="Partner" HeaderText="Partner"></asp:BoundColumn>
								<asp:BoundColumn DataField="RecruitmentBase" HeaderText="Recruitment Base"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid></P>
					<cc1:BnbWordFormButton id="BnbWordFormButton1" runat="server" CssClass="button" Text="Generate Registry Labels" onclicked="BnbWordFormButton1_Clicked"></cc1:BnbWordFormButton>
				</asp:panel><asp:panel id="FindVolunteersTab" runat="server" Height="562px" Width="912px" BnbTabName="Find Volunteers"
					CssClass="tcTab">
					<TABLE class="fieldtable" id="Table1" border="0">
						<TR>
							<TD class="label" style="WIDTH: 85px">Forename</TD>
							<TD>
								<asp:TextBox id="ForenameTextBox" runat="server"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 85px">Surname</TD>
							<TD>
								<asp:TextBox id="SurnameTextBox" runat="server"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 85px">Vol Ref</TD>
							<TD>
								<asp:TextBox id="VolRefTextBox" runat="server"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 85px">Status</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="StatusLookupControl" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									LookupTableName="VlkuBonoboStatusFull" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" ShowBlankRow="True"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 85px">Recruitment base</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="RecruitmentBaseLookupControl" runat="server" CssClass="lookupcontrol" Width="250px"
									Height="22px" LookupTableName="lkuRecruitmentBase" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
					</TABLE>
					<P>
						<asp:Button id="Button1" runat="server" Width="60px" Text="Find" onclick="Button1_Click"></asp:Button></P>
					<P>
						<asp:Label id="MessageLabel" runat="server" ForeColor="Blue"></asp:Label></P>
					<P>
						<asp:Label id="ErrorLabel" runat="server" ForeColor="Red"></asp:Label></P>
					<DIV>
						<asp:DataGrid id="FindResultsDataGrid" runat="server" CssClass="DataGrid" AutoGenerateColumns="False">
							<HeaderStyle CssClass="datagridheader"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn>
									<ItemTemplate>
										<asp:Button id="SelectButton" runat="server" Text="Select" CausesValidation="false" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"tblApplication_ApplicationID")%>'>
										</asp:Button>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="tblIndividualKeyInfo_Surname" HeaderText="Surname"></asp:BoundColumn>
								<asp:BoundColumn DataField="tblIndividualKeyInfo_Forename" HeaderText="Forename"></asp:BoundColumn>
								<asp:BoundColumn DataField="tblIndividualKeyInfo_old_indv_id" HeaderText="Vol Ref"></asp:BoundColumn>
								<asp:BoundColumn DataField="Custom_ApplicationOrder" HeaderText="App Order"></asp:BoundColumn>
								<asp:BoundColumn DataField="Custom_ApplicationStatus" HeaderText="Status"></asp:BoundColumn>
								<asp:BoundColumn DataField="tblIndividualKeyInfo_DateOfBirth" HeaderText="Date of Birth"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid></DIV>
				</asp:panel>
				
		
		</DIV>
		</form>
	</body>
</HTML>
