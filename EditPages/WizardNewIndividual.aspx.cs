using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using System.Text;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for WizardNewIndividual.
	/// </summary>
	public partial class WizardNewIndividual : System.Web.UI.Page
	{
		protected UserControls.WizardButtons wizardButtons;
		protected UserControls.IndividualAddressPanel IndividualAddressPanel1;

	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			BnbEngine.SessionManager.ClearObjectCache();
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
			// Put user code to initialize the page here
			wizardButtons.AddPanel(panelZero);
			wizardButtons.AddPanel(panelOne);
			wizardButtons.AddPanel(panelTwo);

			if (!this.IsPostBack)
			{
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				initWizard();
				BonoboWebControls.BnbWebFormManager.LogAction("New Individual Wizard", null, this);
				if (this.Request.UrlReferrer != null)
					txtHiddenReferrer.Text = this.Request.UrlReferrer.ToString();
				else
					txtHiddenReferrer.Text = "~/Menu.aspx";
			}
            //For capturing Geographical locations
            doLoadGeo();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			wizardButtons.ShowPanel +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
			wizardButtons.ValidatePanel +=new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
			wizardButtons.CancelWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
			wizardButtons.FinishWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);

			dataGridIndividuals.ItemCommand += new DataGridCommandEventHandler(dataGridCompanies_ItemCommand);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


		private void initWizard()
		{
//			// default country lookup
//			Guid defaultCountryID = BnbEngine.SessionManager.GetSessionInfo().DefaultCountryID;
//			if (defaultCountryID != Guid.Empty)
//				drpCountry.SelectedValue = defaultCountryID;

		}

		public void SetupPanelZero()
		{

			// nothing

		}

		public void SetupPanelOne()
		{
			lblPanelOneFeedback.Text = "";
			string forenameBit = txtForename.Text;
			if (forenameBit.Length > 5)
				forenameBit = forenameBit.Substring(0,5);

			BnbTextCondition surnameMatch = new BnbTextCondition("tblIndividualKeyInfo_Surname", txtSurname.Text, BnbTextCompareOptions.StartOfField);
			BnbTextCondition forenameMatch = new BnbTextCondition("tblIndividualKeyInfo_Forename", forenameBit, BnbTextCompareOptions.StartOfField);

			BnbCriteria dupCriteria = new BnbCriteria(surnameMatch);
			dupCriteria.QueryElements.Add(forenameMatch);
			BnbQuery dupQuery = new BnbQuery("vwBonoboIndividualDuplicateCheckFast", dupCriteria);
			BnbQueryDataSet dupDataSet = dupQuery.Execute();
			DataTable dupResults = BonoboWebControls.BnbWebControlServices.GetHtmlEncodedDataTable(dupDataSet.Tables["Results"]);
					
			if (dupResults.Rows.Count > 0)
			{	
				
				panelIndividualDup.Visible = true;
				labelNoDuplicates.Visible = false;
				dataGridIndividuals.DataSource = dupResults;
				dataGridIndividuals.DataBind();
			}
			else
			{
				panelIndividualDup.Visible = false;
				labelNoDuplicates.Visible = true;
			}



		}
		
		public void SetupPanelTwo()
		{
			// copy country and company name from panel zero
			//lblForename2.Text = txtForename.Text;
			//lblSurname2.Text = txtSurname.Text;
			IndividualAddressPanel1.Forename = txtForename.Text;
			IndividualAddressPanel1.Surname = txtSurname.Text;
		}

		

		private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
		{
			if (e.CurrentPanel == 0)
			{
				if (txtSurname.Text == "")
				{
					e.Proceed = false;
					lblPanelZeroFeedback.Text = "Please fill in the Surname";
				}
				if (txtForename.Text == "")
				{
					e.Proceed = false;
					lblPanelZeroFeedback.Text = "Please fill in the Forename/Initials";
				}
			}
//			if (e.CurrentPanel == 2)
//				e.Proceed = AddressPanelValidate();
		}

		private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			if (e.CurrentPanel == 0)
			{
				this.SetupPanelZero();
			}
			if (e.CurrentPanel == 1)
			{
				this.SetupPanelOne();
			}
			if (e.CurrentPanel == 2)
			{
				this.SetupPanelTwo();
			}
		}

		private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			Response.Redirect(txtHiddenReferrer.Text);
		}

		private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			if (AddressPanelValidate())
				this.CreateNewIndividual();
		}

		private void dataGridCompanies_ItemCommand(object source, DataGridCommandEventArgs e)
		{
			TableCell idCell = e.Item.Cells[1];
			Guid indID = new Guid(idCell.Text);
			Response.Redirect("IndividualPage.aspx?BnbIndividual=" + indID.ToString().ToUpper());
		}


		private bool AddressPanelValidate()
		{
         
			BnbIndividual dummy = new BnbIndividual(true);
			BnbEditManager em = new BnbEditManager();
			dummy.RegisterForEdit(em);
			dummy.Forename = txtForename.Text;
			dummy.Surname = txtSurname.Text;
			IndividualAddressPanel1.saveIndividualAddressDetails(dummy,em);

			// only do warnings if checkbox not checked
				BnbWarningCollection warnColl = em.GetEditWarnings();
				if (warnColl.Count > 0)
				{
					lblPanelTwoFeedback.Text = "Please check the following warnings:<br/>";
					foreach(BnbWarning warn in warnColl)
						lblPanelTwoFeedback.Text += warn.Message + "<br/>";
					return false;
				}

			// if no warnings, do validation
			try
			{
				em.Validate();
			} 
			catch(BnbProblemException pe)
			{
				lblPanelTwoFeedback.Text = "Please check the following problems:<br/>";
				foreach(BnbProblem p in pe.Problems)
					lblPanelTwoFeedback.Text += p.Message + "<br/>";
				return false;
			}
			return true;
		}
		
		
		/// <summary>
		/// takes the data from the various controls on the wizard and uses
		/// it to create a totally new individual
		/// and valdiation messages are shown in lblPanelTwoFeedback.
		/// if no problems, re-directs to ind donor page
		/// </summary>
		private void CreateNewIndividual()
		{
			BnbEditManager em = new BnbEditManager();
			BnbIndividual newIndv = new BnbIndividual(true);
			newIndv.RegisterForEdit(em);
			
			IndividualAddressPanel1.saveIndividualAddressDetails(newIndv,em);

			newIndv.Surname = txtSurname.Text;
			newIndv.Forename = txtForename.Text;
			newIndv.Additional.GCContactUserID = BnbEngine.SessionManager.GetSessionInfo().UserID;
			
			try
			{
				em.SaveChanges();
				BnbEngine.SessionManager.ClearObjectCache();
				Response.Redirect("IndividualPage.aspx?BnbIndividual=" + newIndv.ID.ToString().ToUpper());
			}
			catch(BnbProblemException pe)
			{
				lblPanelTwoFeedback.Text = "Unfortunately, the Individual could not be created due to the following problems: <br/>";
				foreach(BnbProblem p in pe.Problems)
					lblPanelTwoFeedback.Text += p.Message + "<br/>";

			}
		
		}

        //For capturing Geographical locations
        private void doLoadGeo()
        {
            if (HasPermissionToLocateOnMap())
            {
                StringBuilder script = new StringBuilder();
                script.Append("pageName ='WizardNewIndividual';");
                // Page mode
                script.Append("mode='new/edit';");
                if (!(IndividualAddressPanel1.Latitude.Trim() == string.Empty))
                {
                    ViewState["Latittude"] = IndividualAddressPanel1.Latitude;
                    ViewState["Longitude"] = IndividualAddressPanel1.Longitude;
                }

                if (!(ViewState["Latittude"] == null))
                {
                    string latitude = ViewState["Latittude"].ToString();
                    string longitude = ViewState["Longitude"].ToString();
                    script.Append("latVal = " + latitude + ";lngVal = " + longitude + ";");
                }
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "WNILoadGeo", script.ToString(), true);
            }
        }
        /// <summary>
        /// Check permisssion to access the Google map functionality
        /// </summary>
        private bool HasPermissionToLocateOnMap()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_LocateAddressOnMap))
                return true;
            else
                return false;
        }


	}
}
