<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="FundChargeRateList.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.FundChargeRateList" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>FundingPage</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server"></uc1:title>
				<P>
					<asp:HyperLink id="hlkNewFundChargeRate" runat="server" NavigateUrl="FundChargeRatePage.aspx?BnbFundChargeRate=new&amp;PageState=New">New Fund Charge Rate</asp:HyperLink><asp:label id="lblNoPermission" runat="server"></asp:label></P>
				<P><bnbdatagrid:bnbdatagridforview id="dgrFundChargeRate" runat="server" Visible="False" RedirectPage="FundChargeRatePage.aspx"
						GuidKey="tblFundChargeRate_FundChargeRateID" DomainObject="BnbFundChargeRate" ColumnNames="lkuRateType_Description, Custom_FormattedRate, tblFundChargeRate_StartDate, tblFundChargeRate_EndDate,lkuVSOGroup_Description,Custom_RecruitmentBase"
						ViewName="vwBonoboFindFundChargeRate" DisplayNoResultMessageOnly="false" DisplayResultCount="false" ShowResultsOnNewButtonClick="false"
						ViewLinksVisible="true" NewLinkVisible="False" NewLinkText="New" ViewLinksText="View" Width="100%" CssClass="datagrid"
						OrderBy="Custom_RecruitmentBase DESC,lkuRateType_Description,lkuVSOGroup_Description"></bnbdatagrid:bnbdatagridforview></P>
			</div>
		</form>
	</body>
</HTML>
