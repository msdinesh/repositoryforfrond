using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboEngine;
using BonoboDomainObjects;
using System.IO;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for ActionPage.
	/// </summary>
	public partial class ReportsPage : System.Web.UI.Page
	{
		protected BonoboWebControls.DataControls.BnbDomainObjectHyperlink BnbDomainObjectHyperlink1;
		protected BonoboWebControls.DataControls.BnbLookupControl BnbLkpActionType;
		protected BonoboWebControls.DataControls.BnbDateBox BnbDateBox2;
		protected UserControls.Title titleBar;
		protected BonoboWebControls.DataControls.BnbLookupControl Bnblookupcontrol1;
		protected Frond.UserControls.NavigationBar NavigationBar1;
		
		BnbWebFormManager bnb = null;

		protected void Page_Load(object sender, System.EventArgs e)
		{
#if DEBUG	
			if (Request.QueryString.ToString() == "")
				BnbWebFormManager.ReloadPage("BnbPosition=76DC2AA7-E3CE-4E8F-BE0E-1F6D0565F7F0&BnbReportVisit=55887F3B-3FE6-4247-B921-C4D6210916EA");
			else
				bnb = new BnbWebFormManager(this,"BnbReportVisit");
#else	
		bnb = new BnbWebFormManager(this,"BnbReportVisit");
#endif
			
			BnbReportVisit obj = bnb.GetPageDomainObject("BnbReportVisit",true) as BnbReportVisit;
			
			obj.RepOrVisID = 1000;
			string posID = Page.Request.QueryString["BnbPosition"]; 
			// Put user code to initialize the page here
			NavigationBar1.RootDomainObject = "BnbPosition";
			NavigationBar1.RootDomainObjectID = posID;

			BnbWorkareaManager.SetPageStyleOfUser(this);

			bnb.LogPageHistory = true;
			// tweak page title settings
			bnb.PageNameOverride = "Placement Reports";
			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;


			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		}




}