using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboWebControls;
using BonoboDomainObjects;
using BonoboEngine.Query;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for WizardNewAddress.
	/// </summary>
	public class WizardNewAddress : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label labelPanelZeroFeedback;
		protected System.Web.UI.WebControls.Panel panelZero;
		protected System.Web.UI.WebControls.Label labelHiddenRequestIDUnused;
		protected System.Web.UI.WebControls.TextBox txtPostCode;
		protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox3;
		protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading3;
		protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading4;
		protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading5;
		protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading6;
		protected System.Web.UI.WebControls.Button cmdFind1;
		protected System.Web.UI.WebControls.DropDownList cmbCountry;
		protected System.Web.UI.WebControls.DropDownList cmbAddressType;
		protected System.Web.UI.HtmlControls.HtmlInputHidden lblHiddenIndividualID;
		protected UserControls.WizardButtons wizardButtons;
		protected System.Web.UI.WebControls.Label lblIndividualFullName;
		protected System.Web.UI.HtmlControls.HtmlInputHidden lblHiddenAddressTypeID;
		protected System.Web.UI.WebControls.Literal literalBeforeCss;
		protected BonoboWebControls.ServicesControls.BnbAddressLookupComposite uxAddressLookup;
		protected System.Web.UI.WebControls.Literal literalAfterCss;
//		BnbWebFormManager bnb;
	
		#region Page_Load
		private void Page_Load(object sender, System.EventArgs e)
		{
			//BnbIndividual=D1DAAB24-AADF-11D4-908F-00508BACE998
			// Put user code to initialize the page here
			BnbEngine.SessionManager.ClearObjectCache();
			// Put user code to initialize the page here
#if DEBUG	
			
			if (Request.QueryString.ToString() == "")
				Response.Redirect("WizardNewAddress.aspx?BnbIndividual=6607554A-11EC-4961-A29E-8C8BC48462E9");
#endif
					
			BnbWorkareaManager.SetPageStyleOfUser(this);
			if (!this.IsPostBack)
			{
				if (this.Request.QueryString["BnbIndividual"] != null)
					this.IndividualID = new Guid(this.Request.QueryString["BnbIndividual"]);
				else
					throw new ApplicationException("WizardNewAddress.aspx expects a BnbIndividual parameter in the querystring");

				this.initWizard();
				BnbWebFormManager.LogAction("New Address Wizard", null, this);
			}

			// Put user code to initialize the page here
			wizardButtons.AddPanel(panelZero);
			
			
		}

		/// <summary>
		/// stores the individual that the action is being added to
		/// </summary>
		public Guid IndividualID
		{
			get{return new Guid(lblHiddenIndividualID.Value);}
			set{lblHiddenIndividualID.Value = value.ToString();}
		}
		/// <summary>
		/// stores the addressType that the action is being added to
		/// </summary>
		public int AddressTypeID
		{
			get{return Convert.ToInt32(lblHiddenAddressTypeID.Value);}
			set{lblHiddenAddressTypeID.Value = value.ToString();}
		}
		#endregion Page_Load

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);


			wizardButtons.CancelWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
			wizardButtons.FinishWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.cmdFind1.Click += new System.EventHandler(this.cmdFind1_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region private methods

		public void initWizard()
		{
			Utilities objUtilities = new Utilities();
			uxAddressLookup.Visible = false;

			BnbIndividual objIndividual = BnbIndividual.Retrieve(this.IndividualID);
			lblIndividualFullName.Text = objIndividual.FullName;


			// address type lookup on panel zero
			BnbLookupDataTable tblAddressType = BnbEngine.LookupManager.GetLookup("vlkuBonoboAddressTypeWithGroup");
			DataView dtvAddressTypeView = new DataView(tblAddressType);
			dtvAddressTypeView.RowFilter = "Exclude = 0";
			dtvAddressTypeView.Sort = "Description";
			cmbAddressType.DataSource = dtvAddressTypeView;
			cmbAddressType.DataValueField = "IntID";
			cmbAddressType.DataTextField = "Description";
			cmbAddressType.DataBind();
			//			// vaf-62: add a blank item
			//ListItem blankItem = new ListItem("<Any>","");
			//cmbAddressType.Items.Insert(0,blankItem);
			// default to perm/home
			cmbAddressType.SelectedValue = BnbConst.AddressType_PermanentHome.ToString();

			//country combos default.
			objUtilities.FillCombos(cmbCountry,"lkuCountry","GuidID","Exclude = 0","");
			cmbCountry.SelectedValue = BnbEngine.SessionManager.GetSessionInfo().DefaultCountryID.ToString();
			
			
			uxAddressLookup.CountryID = BnbEngine.SessionManager.GetSessionInfo().DefaultCountryID;


		}


		#endregion 


		#region events

	

		private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			Response.Redirect("../FindGeneral.aspx");
		}

		private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			this.PassAddressToAddressPage();
		}

	
	

		

		private void cmdFind1_Click(object sender, System.EventArgs e)
		{
			//display the address lookup control and fill in the necessary properties
			if (txtPostCode.Text.Length > 0 && cmbCountry.SelectedValue.ToString().Length > 0)
			{
				uxAddressLookup.Visible = true;
				uxAddressLookup.CountryID = new Guid(cmbCountry.SelectedValue);
				uxAddressLookup.PostCode = txtPostCode.Text;
				uxAddressLookup.PerformSearch();
				uxAddressLookup.Visible = true;
			}
		}
		
		

		private void PassAddressToAddressPage()
		{
			if (uxAddressLookup.Visible && uxAddressLookup.SelectedValue != null)
			{
				// get the selected address
				uxAddressLookup.CopySelectedAddressToProperties();
				// copy the data to a hashtable
				Hashtable addressTransfer = new Hashtable();
				addressTransfer.Add("Line1", uxAddressLookup.Line1);
				addressTransfer.Add("Line2", uxAddressLookup.Line2);
				addressTransfer.Add("Line3", uxAddressLookup.Line3);
				addressTransfer.Add("PostCode", uxAddressLookup.PostCode);
				addressTransfer.Add("County", uxAddressLookup.County);
				addressTransfer.Add("CountryID", uxAddressLookup.CountryID);
				// put it in the session
				this.Session["AddressTransfer"] = addressTransfer;
				// redirect to address page
				Response.Redirect(String.Format("AddressPage.aspx?BnbIndividual={0}&BnbIndividualAddress=new&PageState=new&AddressTypeID={1}&AddressTransfer=1",
					this.IndividualID, cmbAddressType.SelectedValue));
				this.Session["ReturnUrl"] = String.Format("IndividualPage.aspx?BnbIndividual={0}",
					this.IndividualID);
			}
			else
			{
				// just re-direct with address type
				Response.Redirect(String.Format("AddressPage.aspx?BnbIndividual={0}&BnbIndividualAddress=new&PageState=new&AddressTypeID={1}",
					this.IndividualID, cmbAddressType.SelectedValue));
				this.Session["ReturnUrl"] = String.Format("IndividualPage.aspx?BnbIndividual={0}",
					this.IndividualID);
			}



		}

		#endregion

	

		
	}
}
