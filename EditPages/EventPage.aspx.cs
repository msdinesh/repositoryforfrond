using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;
namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for EventPage.
	/// </summary>
	public partial class EventPage : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
		
		protected UserControls.SFEventAttendeesGrid SFEventAttendeesGrid1;
		protected UserControls.SFEventPersonnelGrid SFEventPersonnelGrid1;
		protected System.Web.UI.WebControls.Panel SFAttendeesResultGrid;

		BnbWebFormManager bnb = null;

		protected void Page_Load(object sender, System.EventArgs e)
		{

			// if new, re-direct to wizard
			// also see newclick event
			if (BnbWebFormManager.IsPageDomainObjectNew("BnbEvent"))
			{
				Response.Redirect("WizardNewEvent.aspx");
			}

#if DEBUG
			
			if(Request.QueryString.ToString() == "")				
//				BnbWebFormManager.ReloadPage("BnbEvent=046D6877-9DFF-4AC5-8212-B082B185E01C");
				BnbWebFormManager.ReloadPage("BnbEvent=b87ff2f6-6aee-41c0-af06-9a36b8140978");
			else
				bnb = new BnbWebFormManager(this,"BnbEvent");
#else			
			bnb = new BnbWebFormManager(this,"BnbEvent");
#endif
			bnb.LogPageHistory = true;
			// tweak page title settings
			bnb.PageNameOverride = "Event";
			
			BnbWorkareaManager.SetPageStyleOfUser(this);

			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;

				// work out which tab to show based on where we came from
				if (this.Request.UrlReferrer != null)
				{
					string referrer = this.Request.UrlReferrer.AbsolutePath;
					if (referrer.EndsWith("AttendancePage.aspx"))
						BnbTabControl1.SelectedTabID = AttendeesTab.ID;
					if (referrer.EndsWith("EventPersonnelPage.aspx"))
						BnbTabControl1.SelectedTabID = EventPersonnelTab.ID;
					if (referrer.EndsWith("WizardNewEvent.aspx"))
						BnbTabControl1.SelectedTabID = MainTab.ID;
					if (referrer.EndsWith("WizardSelectionResults.aspx")
						|| referrer.EndsWith("AttendanceResultPage.aspx"))
						BnbTabControl1.SelectedTabID = AttendeesResultsTab.ID;
					if (referrer.EndsWith("WizardNewAttendance.aspx"))
					{
						// depends what mode the wizard was in
						if (this.Request.UrlReferrer.Query.IndexOf("EVP") > -1)
							BnbTabControl1.SelectedTabID = EventPersonnelTab.ID;
						else
							BnbTabControl1.SelectedTabID = AttendeesTab.ID;

					}
				}
				doHideResultsTab();
			}
			SFEventPersonnelGrid1.Event = 
				SFEventAttendeesGrid1.Event = 
				(BnbEvent)bnb.ObjectTree.DomainObjectDictionary["BnbEvent"];
			
			this.doNewAttendeeHyperlinks();
		}

		private void doNewAttendeeHyperlinks()
		{
			BnbEvent ev = bnb.GetPageDomainObject("BnbEvent") as BnbEvent;
			if (ev != null)
			{
				uxNewAttendanceHyper.NavigateUrl = String.Format("WizardNewAttendance.aspx?BnbEvent={0}",
					ev.ID);
				uxNewEventPersonnelHyper.NavigateUrl = String.Format("WizardNewAttendance.aspx?BnbEvent={0}&EVP=1",
					ev.ID);				
			}
		}

		private void doHideResultsTab()
		{
			BnbEvent ev =BnbWebFormManager.StaticGetPageDomainObject("BnbEvent") as BnbEvent;
			if (ev != null)
			{
				uxResultsGridPanel.Visible = ev.IsAttendanceResultsEvent;
				uxNoAttendeeResultsLabel.Visible = !ev.IsAttendanceResultsEvent;
			}
			else
			{
				// hide both if new mode
				uxResultsGridPanel.Visible = false;
				uxNoAttendeeResultsLabel.Visible = false;
				
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			BnbDataButtons1.NewClick +=new EventHandler(BnbDataButtons1_NewClick);
			base.OnInit(e);

		
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void BnbDataButtons1_NewClick(object sender, EventArgs e)
		{
			// doing the new-redirect here in the NewClick event ensures that
			// the wizard knows the UrlReferrer
			this.SmartNavigation = false;
			Response.Redirect("WizardNewEvent.aspx");
		}
	}
}
