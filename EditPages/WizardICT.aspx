<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>

<%@ Page Language="c#" Codebehind="WizardICT.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.WizardICT" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>WizardICT</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet" />
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server">
            </uc1:MenuBar>
        </div>
        <div id="baseContent">
          
            <div class="wizardheader" ms_positioning="FlowLayout">
                <h3><IMG class="icon24" src="../Images/wizard24silver.gif">
                    In Country Transfer&nbsp;Wizard</h3>
            </div>
            <p>
                <asp:Panel ID="panelZero" runat="server" CssClass="wizardpanel">
                    <p>
                        <em>This wizard will help you to perform an In Country Transfer. That is, to transfer
                            an Overseas volunteer into a new Placement.</em></p>
                    <p>
                        The In Country Transfer will be for the Volunteer shown below.</p>
                    <p>
                    </p>
                    <table class="wizardtable" id="Table1">
                        <tr>
                            <td class="label">
                                Volunteer Details</td>
                            <td>
                                <asp:Label ID="labelVolunteerDetails" runat="server"></asp:Label></td>
                            <tr>
                                <td class="label">
                                    Current Placement</td>
                                <td>
                                    <asp:Label ID="labelCurrentPlacement" runat="server"></asp:Label></td>
                            </tr>
                    </table>
                    <p>
                        <p>
                            <asp:Label ID="labelPanelZeroContinue" runat="server" Visible="False">Press Next to continue.</asp:Label>&nbsp;
                        </p>
                        <p>
                            <asp:Label ID="labelPanelZeroFeedback" runat="server" CssClass="feedback"></asp:Label></p>
                </asp:Panel>
                <asp:Panel ID="panelOne" runat="server" CssClass="wizardpanel">
                    <table class="wizardtable" id="Table2">
                        <tr>
                            <td class="label">
                                Volunteer Details</td>
                            <td>
                                <asp:Label ID="labelVolunteerDetails2" runat="server"></asp:Label></td>
                            <tr>
                                <td class="label">
                                    Current Placement</td>
                                <td>
                                    <asp:Label ID="labelCurrentPlacement2" runat="server"></asp:Label></td>
                            </tr>
                    </table>
                    <p>
                        <strong>Please choose the new Placement for the Volunteer:</strong></p>
                    <p>
                    </p>
                    <p>
                        Unfilled
                        <asp:Label ID="labelCountry" runat="server"></asp:Label>&nbsp;Placements with Earliest
                        Start Date between
                        <asp:TextBox ID="textESDStart" runat="server" Width="88px"></asp:TextBox>&nbsp;and
                        <asp:TextBox ID="textESDEnd" runat="server" Width="77px"></asp:TextBox>&nbsp;
                        <asp:Button ID="buttonRefreshPlacements" runat="server" Text="Refresh" OnClick="buttonRefreshPlacements_Click">
                        </asp:Button>&nbsp;
                        <asp:Label ID="labelFindFeedback" runat="server" CssClass="feedback"></asp:Label></p>
                    <p>
                        <asp:DataGrid ID="dataGridPlacements" runat="server" CssClass="datagrid" AutoGenerateColumns="False">
                            <Columns>
                                <asp:TemplateColumn HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:Label ID="labelRadio" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn Visible="False" DataField="tblPosition_PositionID" HeaderText="HiddenPositionID">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="Custom_FullPositionReference" HeaderText="Placement Ref">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="tblRole_RoleName" HeaderText="Job Title"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tblEmployer_EmployerName" HeaderText="Employer"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tblPosition_EarliestStartDate" HeaderText="ESD" DataFormatString="{0:dd/MMM/yyyy}">
                                </asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid></p>
                    <p>
                        <asp:Label ID="labelFindNotes" runat="server" EnableViewState="False"></asp:Label></p>
                    <p>
                        <strong>Or enter the Placement Reference number if known:
                            <asp:TextBox ID="txtPlacementRef" runat="server" Width="184px"></asp:TextBox>
                        </strong>
                    </p>
                    <p>
                        <asp:Label ID="labelPanelOneFeedback" runat="server" CssClass="feedback"></asp:Label></p>
                </asp:Panel>
                <asp:Panel ID="panelTwo" runat="server" CssClass="wizardpanel">
                    You have selected to transfer the Volunteer from the current placement to the new
                    placement shown below:
                    <p>
                    </p>
                    <p>
                    </p>
                    <p>
                    </p>
                    <p>
                        <table class="wizardtable" id="Table3">
                            <tr>
                                <td class="label">
                                    Volunteer Details</td>
                                <td>
                                    <asp:Label ID="labelVolunteerDetails3" runat="server"></asp:Label></td>
                                <tr>
                                    <td class="label">
                                        Current Placement</td>
                                    <td>
                                        <asp:Label ID="labelCurrentPlacement3" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        New Placement</td>
                                    <td>
                                        <asp:Label ID="labelNewPlacementDetails" runat="server"></asp:Label></td>
                                </tr>
                        </table>
                    </p>
                    <p>
                        The date that the Volunteer will start the new Placement will default to today.
                        Change the date in the box below if you wish the Volunteer to transfer on a different
                        date:</p>
                    <p>
                    </p>
                    <p>
                    </p>
                    <p>
                    </p>
                    <p>
                        <table class="wizardtable" id="Table4">
                            <tr>
                                <td class="label">
                                    Transfer Date</td>
                                <td>
                                    <asp:TextBox ID="textTransferDate" runat="server"></asp:TextBox></td>
                            </tr>
                        </table>
                    </p>
                    <p>
                        Press Finish to save the In Country Transfer.
                        <p>
                            <asp:Label ID="labelPanelTwoFeedback" runat="server" CssClass="feedback"></asp:Label><br>
                            <asp:CheckBox ID="CheckBoxPanelTwo" runat="server" Visible="False"></asp:CheckBox>
                            <asp:Label ID="labelPanelTwoCheckBox" runat="server" CssClass="feedback"></asp:Label></p>
                </asp:Panel>
                <p>
                    <uc1:WizardButtons ID="wizardButtons" runat="server"></uc1:WizardButtons>
                </p>
                <p>
                    <asp:Label ID="labelHiddenCountryID" runat="server" Visible="False"></asp:Label>
                    <asp:Label ID="labelHiddenSelectedPositionID" runat="server" Visible="False"></asp:Label></p>
        </div>
    </form>
</body>
</html>
