<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Page language="c#" Codebehind="PaymentDetailPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.PaymentDetailPage" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/bnbaccountheader24silver.gif"></uc1:title>
				<uc1:NavigationBar id="NavigationBar1" runat="server"></uc1:NavigationBar>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" UndoText="Undo" SaveText="Save"
					EditAllText="Edit All" NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px"
					NewWidth="55px" NewButtonEnabled="True"></bnbpagecontrol:bnbdatabuttons>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:bnbmessagebox><BR>
				<TABLE class="fieldtable" id="Table1" border="0">
					<TR>
						<TD class="label" style="WIDTH: 126px; HEIGHT: 5px" width="126">Voucher ID</TD>
						<TD style="WIDTH: 211px; HEIGHT: 5px" width="211" colSpan="5">
							<bnbdatacontrol:BnbDomainObjectHyperlink id="BnbDomainObjectHyperlink1" runat="server" DataMember="BnbAccountItem.AccountHeader.VoucherID"
								Target="_top" HyperlinkText="[BnbDomainObjectHyperlink]" GuidProperty="BnbAccountItem.AccountHeaderID" RedirectPage="PaymentPage.aspx"></bnbdatacontrol:BnbDomainObjectHyperlink></TD>
						
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 126px; HEIGHT: 5px" width="126">
							<P>Purpose&nbsp;</P>
						</TD>
						<TD style="WIDTH: 211px; HEIGHT: 5px" width="211">
							<bnbdatacontrol:bnbtextbox id="BnbTextBox1" runat="server" CssClass="textbox" Width="90px" DataMember="BnbAccountItem.Purpose"
								Height="19px" StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:bnbtextbox></TD>
						<TD class="label" style="WIDTH: 138px; HEIGHT: 5px" width="138">Cost Centre&nbsp;</TD>
						<TD style="WIDTH: 237px; HEIGHT: 5px" width="237">
							<bnbdatacontrol:bnblookupcontrol id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" Width="201px" DataMember="BnbAccountItem.CostCentreID"
								Height="19px" LookupTableName="lkuCostCentre" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
						<TD class="label" style="WIDTH: 107px; HEIGHT: 5px" width="107">Accounts Code&nbsp;</TD>
						<TD style="WIDTH: 237px; HEIGHT: 5px" width="237">
							<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol12" runat="server" CssClass="lookupcontrol" Width="201" DataMember="BnbAccountItem.AccountsCodeID"
								Height="19" LookupTableName="lkuAccountsCode" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 126px; HEIGHT: 19px" width="126">Fund Code</TD>
						<TD style="WIDTH: 211px; HEIGHT: 19px" width="211">
							<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol13" runat="server" CssClass="lookupcontrol" Width="201px" DataMember="BnbAccountItem.FundCodeID"
								Height="19px" LookupTableName="vlkuBonoboFundCode" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
						<TD class="label" style="WIDTH: 138px; HEIGHT: 19px" width="138">T4 Vol. Info.</TD>
						<TD style="WIDTH: 231px; HEIGHT: 19px" width="231">
							<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol21" runat="server" CssClass="lookupcontrol" Width="201px" DataMember="BnbAccountItem.T4VolunteerCodeID"
								Height="19px" LookupTableName="lkuT4Code" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
						<TD class="label" style="WIDTH: 107px; HEIGHT: 19px" width="107">Amount&nbsp;</TD>
						<TD style="WIDTH: 237px; HEIGHT: 19px" width="237">
							<bnbdatacontrol:bnbdecimalbox id="BnbDecimalBox1" runat="server" CssClass="textbox" Width="95px" DataMember="BnbAccountItem.ItemAmount"
								Height="21px" FormatExpression="0.00" TextAlign="Default" DisplayEmptyValue="False" DefaultValue="0"></bnbdatacontrol:bnbdecimalbox></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 126px; HEIGHT: 16px" width="126">Benefit</TD>
						<TD style="WIDTH: 211px; HEIGHT: 16px" width="211">
							<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol14" runat="server" CssClass="lookupcontrol" Width="201px" DataMember="BnbAccountItem.BenefitTypeID"
								Height="19px" LookupTableName="lkuBenefitType" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
						<TD class="label" style="WIDTH: 138px; HEIGHT: 16px" width="138">Benefit Number</TD>
						<TD style="WIDTH: 237px; HEIGHT: 16px" width="237">
							<bnbdatacontrol:bnbtextbox id="Bnbtextbox12" runat="server" CssClass="textbox" Width="95px" DataMember="BnbAccountItem.BenefitNumber"
								Height="21px" StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:bnbtextbox></TD>
						<TD class="label" style="WIDTH: 107px; HEIGHT: 16px" width="107">Benefit 
							Class&nbsp;</TD>
						<TD style="HEIGHT: 16px" width="25%">
							<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol22" runat="server" CssClass="lookupcontrol" Width="201px" DataMember="BnbAccountItem.BenefitClassID"
								Height="19px" LookupTableName="lkuBenefitStatus" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 126px; HEIGHT: 10px" width="126">
							<P>Benefit Year</P>
						</TD>
						<TD style="WIDTH: 211px; HEIGHT: 10px" width="211">
							<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol8" runat="server" CssClass="lookupcontrol" Width="201px" DataMember="BnbAccountItem.BenefitYearID"
								Height="19px" LookupTableName="lkuFinancialYear" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
						<TD class="label" style="WIDTH: 138px; HEIGHT: 10px" width="138">Benefit 
							Salary&nbsp;</TD>
						<TD style="WIDTH: 237px; HEIGHT: 10px" width="237">
							<bnbdatacontrol:bnbdecimalbox id="Bnbdecimalbox29" runat="server" CssClass="textbox" Width="90px" DataMember="BnbAccountItem.BenefitSalary"
								Height="21px" TextAlign="Default" DisplayEmptyValue="False" DefaultValue="0"></bnbdatacontrol:bnbdecimalbox></TD>
						<TD class="label" style="WIDTH: 107px; HEIGHT: 10px" width="107">&nbsp;Benefit No. 
							Weeks&nbsp;</TD>
						<TD style="HEIGHT: 10px" width="25%">
							<bnbdatacontrol:bnbtextbox id="Bnbtextbox01" runat="server" CssClass="textbox" Width="95px" DataMember="BnbAccountItem.BenefitNoWeeks"
								Height="21px" StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:bnbtextbox></TD>
					</TR>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
