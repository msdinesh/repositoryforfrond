using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using BonoboWebControls;
using BonoboDomainObjects;
using BonoboEngine;


namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for AttendancePage.
	/// </summary>
	public partial class AttendanceResultPage : System.Web.UI.Page
	{
		protected BonoboWebControls.PageControls.BnbDataButtons Bnbdatabuttons2;
		protected BonoboWebControls.PageControls.BnbSectionLinks BnbSectionLinks1;
		protected BonoboWebControls.PageControls.BnbMessageBox BnbMessageBox1;
		protected BonoboWebControls.PageControls.BnbSectionHeading Bnbsectionheading2;

		protected UserControls.Title titleBar;
		protected Frond.UserControls.NavigationBar NavigationBar1;

		protected BnbWebFormManager bnb;

		protected void Page_Load(object sender, System.EventArgs e)
		{

#if DEBUG	
			
			if (Request.QueryString.ToString() == "")
				BnbWebFormManager.ReloadPage("BnbAttendance=DE520D5B-429A-49BE-BF56-EF7524709108");
			else
				bnb = new BnbWebFormManager(this,"BnbAttendance");
#else	
			bnb = new BnbWebFormManager(this,"BnbAttendance");
#endif

			// if this page is called with a BnbAttendance that has
			// no BnbAttendanceResult yet, we want to re-direct to wizard.
			BnbAttendance att = BnbWebFormManager.StaticGetPageDomainObject("BnbAttendance") as BnbAttendance;
			if (att.CurrentAttendanceResult == null)
				Response.Redirect(String.Format("WizardSelectionResults.aspx?BnbAttendance={0}",
					att.ID), true);		

			bnb.LogPageHistory = true;
			BnbWorkareaManager.SetPageStyleOfUser(this);
			BnbAttendance attendance = bnb.GetPageDomainObject("BnbAttendance",true) as BnbAttendance;
			string indv = attendance.IndividualID.ToString();
			NavigationBar1.RootDomainObject = "BnbIndividual";
			NavigationBar1.RootDomainObjectID= indv;
	

			// tweak page title settings
			bnb.PageNameOverride = "Attendance Result";
			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;

			}
			doRejectionCodesEnable();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void doRejectionCodesEnable()
		{
			// disable the rejection codes grid if user does not have permission
			// (looks tidier that way)
			if (!BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_AssessmentResults))
			{
				uxRejectionCodeGrid.DataGridType = BonoboWebControls.DataGrids.DataGridForDomainObjectListType.ViewOnly;
			}
		}
	}
}

