using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboEngine.Query;
using Gentle.Framework;
using System.Data.SqlClient;
using System.Reflection;
using BonoboDomainObjects;
using System.Collections.Specialized;
using BonoboEngine;
namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for NIBatchPage.
	/// </summary>
	public partial class NIBatchPage : System.Web.UI.Page
	{
		#region Control Declarations
		protected System.Web.UI.WebControls.Label lblViewWarnings;
		protected BonoboWebControls.DataControls.BnbDecimalBox Bnbdecimalbox29;
		protected BonoboWebControls.DataControls.BnbDateBox BnbDateBox1;
		protected BonoboWebControls.DataControls.BnbLookupControl Bnblookupcontrol56;
		protected System.Web.UI.WebControls.Panel Panel1;
		#endregion

		private BnbNIBatchHeader bnbNIBatchHeader;
		private BnbWebFormManager bnb;
		protected UserControls.Title titleBar;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
#if DEBUG			
			if(Request.QueryString.ToString() == "")
			{
				//this reloads the page with a specific ID when debugging
				BnbWebFormManager.ReloadPage("BnbNIBatchHeader=30831F1C-F606-4A11-95A0-3FABF7168480");
			}
			//something like the following line can be used for testing
			//BnbNIBatchHeader.DeleteRecordsForGivenYearDescription("74/75");
#endif
			bnb = new BnbWebFormManager(this,"BnbNIBatchHeader");
			bnb.PageNameOverride = "NI Batch";
			bnb.LogPageHistory = true;
			BnbWorkareaManager.SetPageStyleOfUser(this);
			bnbNIBatchHeader = (BnbNIBatchHeader)bnb.ObjectTree.DomainObjectDictionary["BnbNIBatchHeader"];	
			if(!IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
				Session["State"]="Default";
				PopulateCountryCombo();			
			}
			SetEnablednessOfCertainControls(true);
			ApplyCountryFilter();
			if(ChequeNumberTextBox.Text!=string.Empty)
				ChequeNumberWarningLabel.Visible=false;
		}


		private void PopulateCountryCombo()
		{
			BnbListCondition condition = new BnbListCondition("tblNIBatchDetail_NIBatchHeaderID",bnbNIBatchHeader.ID );
			BnbQuery query = new BnbQuery("vwBonoboNIBatchDetailCountryTotals", new BnbCriteria(condition));
			CountriesDropDownList.DataTextField="Custom_CountryWithRecordCount";
			CountriesDropDownList.DataValueField="tblNIBatchDetail_CountryID";
			CountriesDropDownList.DataSource =query.Execute();
			CountriesDropDownList.DataMember ="Results";
			CountriesDropDownList.DataBind();
		}

		private void ApplyCountryFilter()
		{
			if(CountriesDropDownList.SelectedValue !=string.Empty)
			{
				BnbListCondition condition1 = new BnbListCondition("CountryID",new Guid(CountriesDropDownList.SelectedValue ));
				BnbListCondition condition2 = new BnbListCondition("NIBatchHeaderID",new Guid(Request.QueryString["BnbNIBatchHeader"]));
				accountItemsBnbDataGridForView.Criteria=new BnbCriteria(condition1);
				accountItemsBnbDataGridForView.Criteria.QueryElements.Add(condition2);
			}
		}
		protected void ApplyCountryFilterButton_Click(object sender, System.EventArgs e)
		{
			ApplyCountryFilter();
		}

		protected void RunBatchButton_Click(object sender, System.EventArgs e)
		{
			//Run a BnbQuery against vwNIBatchDetailDescriptive to see if 
			//any of the rows have UserOverride set to 1
			BnbListCondition condition1 = new BnbListCondition("NIBatchHeaderID",bnbNIBatchHeader.ID);
			BnbListCondition condition2 = new BnbListCondition("UserOverride",1);
			BnbCriteria criteria=new BnbCriteria(condition1);
			criteria.QueryElements.Add(condition2);
			BnbQuery query = new BnbQuery("vwNIBatchDetailDescriptive",criteria);
			

			if(query.Execute().Tables["Results"].Rows.Count>0)
			{ 
				Session["State"]="PreRunBatch";
				ConditionalPanelLabel.Text="Some ad-hoc changes have been applied to the list. Re-running the batch job will cause those changes to be lost. Are you sure you want to run the batch job?";
				ConditionallyVisibleOnUserOverridePanel .Visible=true;
				SetEnablednessOfCertainControls(false);
			}
			else
				RunBatch();

		}

		private void SetEnablednessOfCertainControls(bool enable)
		{

			BnbDataButtons1.Enabled=enable;
			ActionButtonsPanel.Enabled=enable;
		}

		protected void FreezeDataButton_Click(object sender, System.EventArgs e)
		{
			Session["State"]="PreFreezeData";
			ConditionalPanelLabel.Text="Freezing the data will allow you to produce the final report to send to the DSS. However, you will not be able to re-run the batch or change any data. Are you sure you want to freeze?";
			ConditionallyVisibleOnUserOverridePanel .Visible=true;
			SetEnablednessOfCertainControls(false);
		}

		protected void ApproveDataButton_Click(object sender, System.EventArgs e)
		{
			if(ChequeNumberTextBox.Text==string.Empty)
				ChequeNumberWarningLabel.Visible=true;
			else
			{
				Session["State"]="PreApproveData";
				ConditionalPanelLabel.Text="Approving the data will create payment records for all the volunteers. This should only be done once the DSS have approved the data. Are you sure you want to approve?";
				ConditionallyVisibleOnUserOverridePanel .Visible=true;
				SetEnablednessOfCertainControls(false);
			}
		}

		protected void YesContinueButton_Click(object sender, System.EventArgs e)
		{
			SetEnablednessOfCertainControls(true);
			ConditionallyVisibleOnUserOverridePanel.Visible=false;
			switch(Session["State"].ToString())
			{
				case "PreRunBatch":
					RunBatch();
					break;
				case "PreFreezeData":
					FreezeData();
					break;
				case "PreApproveData":
					ApproveData();
					break;
			}
		}

		private void ApproveData()
		{

			/*
			 * -	Do a BnbQuery against vwNIBatchDetailDescriptive to get all NIBatchDetailIDs 
			 * for the current NIBatchHeaderID which have Include flag set to 1.
			-	For each NIBatchDetailID, do the following:

			o	Retrieve the BnbNIBatchDetail object that corresponds to the row in vwNIBatchDetailDescriptive.
			o	If BnbNIBatchDetail.AccountHeaderID is null, then do the following:
			o	create a BnbEditManager
			o	Register the BnbNIBatchDetail object for edit
			o	create a new BnbAccountHeader, register it, and fill it in as follows:


			 * */

			BnbListCondition condition1 = new BnbListCondition("NIBatchHeaderID",bnbNIBatchHeader.ID);
			BnbListCondition condition2 = new BnbListCondition("Include",1);
			BnbCriteria criteria=new BnbCriteria(condition1);
			criteria.QueryElements.Add(condition2);
			BnbQuery query = new BnbQuery("vwNIBatchDetailDescriptive",criteria);
			BnbEditManager editManager;
			foreach(DataRow row in query.Execute().Tables["Results"].Rows )
			{
				
				BnbNIBatchDetail bnbNIBatchDetail	=BnbNIBatchDetail.Retrieve(new Guid(row["NIBatchDetailID"].ToString())); 
				
				if(bnbNIBatchDetail.AccountHeaderID==Guid.Empty)
				{
					editManager=new BnbEditManager();
					bnbNIBatchDetail.RegisterForEdit(editManager);
					BnbAccountHeader bnbAccountHeader=new BnbAccountHeader(true);
					bnbAccountHeader.RegisterForEdit(editManager);
					bnbAccountHeader.Individual =  BnbIndividual.Retrieve(bnbNIBatchDetail.IndividualID);
					bnbAccountHeader.Application=  BnbApplication.Retrieve(bnbNIBatchDetail.ApplicationID);
					bnbAccountHeader.DeptReqID=BnbConst.Dept_Accounts ;
					bnbAccountHeader.DateDue=DateTime.Now;
					bnbAccountHeader.CurrencyID =bnbNIBatchHeader.CurrencyID;
					bnbAccountHeader.PayeeTypeID =BnbConst.PayeeType_Representative;
					bnbAccountHeader.PaymentMethodID=BnbConst.PaymentMethod_Cheque;
					bnbAccountHeader.VoucherTypeID =BnbConst.VoucherType_Payment;
					bnbAccountHeader.PaidByID =BnbEngine.SessionManager.GetSessionInfo().UserID;
					bnbAccountHeader.RateCurrency="1.0";
					bnbAccountHeader.PayName ="DSS";
					bnbAccountHeader.Comments="Automatically added by NI Batch";
					bnbAccountHeader.PaymentID=bnbNIBatchHeader.ChequeNumber;
					bnbAccountHeader.PayTypeID=BnbConst.Currency_Sterling;
					/*
					o	then create a new BnbAccountItem, register it, 
					relate it to the BnbAccountHeader, and fill it in as follows:
					*/					
					BnbAccountItem bnbAccountItem=new BnbAccountItem(true);
					bnbAccountItem.RegisterForEdit(editManager);
					bnbAccountItem.AccountHeader=bnbAccountHeader;
					bnbAccountItem.AccountsCodeID =BnbConst.AccountCode_BE01SocialSecurity ;
					bnbAccountItem.ItemAmount =(decimal)bnbNIBatchDetail.Value;
					bnbAccountItem.BenefitClassID=BnbConst.BenefitClass_VDW;
					bnbAccountItem.BenefitNoWeeks=bnbNIBatchDetail.NoOfWeeks;
					bnbAccountItem.BenefitYearID =bnbNIBatchHeader.FinancialYearID;
					bnbAccountItem.CostCentreID=BnbConst .CostCentre_75POB;
					bnbAccountItem.BenefitNumber=bnbNIBatchDetail.NINumber;
					bnbAccountItem.BenefitTypeID =bnbNIBatchHeader.BenefitTypeID;
					bnbAccountItem.Purpose ="UK Social Securiy (NI Batch)";
					/*o	Update the BnbNIBatchDetail object so 
					 * that BnbNIBatchDetail.AccountHeaderID = the ID of the new BnbAccountHeader
					o	Call BnbEditManager.SaveChanges() to save the changes. 
					No validation messages are expected to occur, but if they do, an error will be automatically thrown anyway.
							*/
					bnbNIBatchDetail.AccountHeaderID=bnbAccountHeader.ID;
					editManager.SaveChanges();
				}  
			}
			editManager=new BnbEditManager();
			bnbNIBatchHeader.RegisterForEdit(editManager);
			bnbNIBatchHeader.Approved=true;
			editManager.SaveChanges();
			ApproveDataButton.Enabled=false;
		}
		private void FreezeData()
		{
			BnbEditManager editManager=new BnbEditManager();
			bnbNIBatchHeader.RegisterForEdit(editManager);
			bnbNIBatchHeader.FreezeData=true;
			editManager.SaveChanges();
			FreezeDataButton.Enabled=false;		
		}
		private void RunBatch()
		{
			bnbNIBatchHeader.RunBatch();
		}
		protected void NoContinueButton_Click(object sender, System.EventArgs e)
		{
			SetEnablednessOfCertainControls(true);
			ConditionallyVisibleOnUserOverridePanel.Visible=false;
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.PreRender += new System.EventHandler(this.NIBatchPage_PreRender);

		}
		#endregion
		protected void NIBatchPage_PreRender(object sender, EventArgs e)
		{
			if(ActionButtonsPanel.Enabled)
			{
				RunBatchButton.Enabled = !bnbNIBatchHeader.FreezeData && !bnb.InEditMode && !bnb.InNewMode ;
				FreezeDataButton.Enabled = !bnbNIBatchHeader.FreezeData && !bnb.InEditMode&& !bnb.InNewMode ;
				ApproveDataButton.Enabled = bnbNIBatchHeader.FreezeData && !bnb.InEditMode && !bnb.InNewMode && !bnbNIBatchHeader.Approved;
				
			}
		}
	}
}
