<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc2" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="WizardNewPlacementLink.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.WizardNewPlacementLink" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>WizardNewPlacementLink</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal><LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet"><asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent">
				<DIV class="wizardheader" ms_positioning="FlowLayout">
					<H3><IMG class="icon24" src="../Images/wizard24silver.gif"> New&nbsp;Placement 
						link&nbsp;Wizard</H3>
				</DIV>
				<asp:panel id="panelZero" runat="server" CssClass="wizardpanel">
					<P>This wizard will help you select a Placement to link the Volunteer to.</P>
					<TABLE class="wizardtable">
						<TR>
							<TD class="label" style="WIDTH: 32.85%; TEXT-ALIGN: right">Volunteer
							</TD>
							<TD>
								<asp:Label id="uxVolSummary" runat="server" Width="200px"></asp:Label></TD>
						</TR>
						<TR>
							<TD colSpan="2">
								<P>Enter the&nbsp;Placement Ref No, Jobtitle, employer&nbsp;and/or Country here and 
									press Find, then select one of the results:</P>
							</TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 32.85%; TEXT-ALIGN: right">Placement Ref</TD>
							<TD>
								<asp:TextBox id="txtFindPlacementRefNo" runat="server" Width="136px"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 32.85%; TEXT-ALIGN: right">Job&nbsp;Title&nbsp;(starts 
								with)</TD>
							<TD>
								<asp:TextBox id="txtFindJobTitle" runat="server"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 32.85%; TEXT-ALIGN: right">Employer&nbsp;(starts 
								with)</TD>
							<TD>
								<asp:TextBox id="txtFindEmployer" runat="server"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 32.85%; TEXT-ALIGN: right">Planned Start&nbsp;Date 
								Between
							</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneDateBox id="uxPSDStart" runat="server" CssClass="datebox" Width="90px" DateCulture="en-GB"
									DateFormat="dd/MMM/yyyy" Height="20px"></bnbdatacontrol:BnbStandaloneDateBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<bnbdatacontrol:BnbStandaloneDateBox id="uxPSDEnd" runat="server" CssClass="datebox" Width="90px" DateCulture="en-GB"
									DateFormat="dd/MMM/yyyy" Height="20px"></bnbdatacontrol:BnbStandaloneDateBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 32.85%; TEXT-ALIGN: right">Country</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" Width="144px" Height="22px"
									ShowBlankRow="True" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuCountry"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 32.85%; TEXT-ALIGN: right">Unfilled</TD>
							<TD>
								<asp:CheckBox id="chkUnFilled" runat="server" Checked="True"></asp:CheckBox></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 32.85%; TEXT-ALIGN: left">Max Results:
								<asp:DropDownList id="uxMaxResultsDropdown" runat="server">
									<asp:ListItem Value="100" Selected="True">100</asp:ListItem>
									<asp:ListItem Value="500">500</asp:ListItem>
									<asp:ListItem Value="1000">1000</asp:ListItem>
								</asp:DropDownList></TD>
							<TD>
								<asp:Button id="btnFindPlacement" runat="server" Text="Find" onclick="btnFindPlacement_Click"></asp:Button></TD>
						</TR>
					</TABLE>
					<P>
						<bnbdatagrid:BnbDataGridForView id="bdgPlacement" runat="server" CssClass="datagrid" Width="100%" Visible="False"
							ViewLinksText="View" NewLinkText="New" NewLinkVisible="False" ViewLinksVisible="False" ShowResultsOnNewButtonClick="false"
							DisplayResultCount="false" DisplayNoResultMessageOnly="True" DataGridType="RadioButtonList" ViewName="vwBonoboFindPosition"
							GuidKey="tblPosition_PositionID"></bnbdatagrid:BnbDataGridForView></P>
					<P>
						<asp:Label id="lblPanelZeroFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				</asp:panel><asp:panel id="panelOne" runat="server" CssClass="wizardpanel">
					<P>Please select the Placement link, then press Finish to create the new record:</P>
					<TABLE class="wizardtable" id="Table4">
						<TR>
							<TD class="label" style="WIDTH: 42.75%; HEIGHT: 24px">Volunteer Ref</TD>
							<TD style="HEIGHT: 24px">
								<asp:Label id="lblVolRef1" runat="server" Width="264px"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 42.75%">
								<P>&nbsp;Name</P>
							</TD>
							<TD style="HEIGHT: 25px">
								<asp:Label id="lblVolName1" runat="server" Width="264px"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 42.75%">Current Status</TD>
							<TD>
								<asp:Label id="lblCurrentStatus1" runat="server" Width="272px"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 42.75%">Placement Ref No</TD>
							<TD>
								<asp:Label id="lblPlacementRef" runat="server" Width="272px"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 42.75%">Job title</TD>
							<TD>
								<asp:Label id="lblJobtitle" runat="server" Width="272px"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 42.75%">Employer</TD>
							<TD>
								<asp:Label id="lblEmployer" runat="server" Width="272px"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 42.75%">Initial Placement Link</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="uxServiceStatusLookup" runat="server" CssClass="lookupcontrol" Width="250px"
									Height="22px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboServiceStatusLinking"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
					</TABLE>
					<P>
						<asp:label id="lblViewWarnings" runat="server" CssClass="viewwarning"></asp:label>
						<asp:Label id="lblPanelTwoFeedback" runat="server" CssClass="feedback"></asp:Label><BR>
						<asp:CheckBox id="uxIgnoreWarningsCheckbox" runat="server"></asp:CheckBox>
						<asp:label id="lblIgnoreWarnings" runat="server" CssClass="viewwarning"></asp:label></P>
				</asp:panel>
				<p><uc2:wizardbuttons id="wizardButtons" runat="server"></uc2:wizardbuttons><asp:label id="lblHiddenPositionID" runat="server" Visible="False"></asp:label><asp:label id="lblHiddenApplicationID" runat="server" Width="136px" Visible="False"></asp:label>
					<asp:TextBox id="uxHiddenReferrer" runat="server" Visible="False"></asp:TextBox></p>
			</div>
		</form>
	</body>
</HTML>
