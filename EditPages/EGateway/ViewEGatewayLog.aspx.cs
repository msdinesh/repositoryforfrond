using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using BonoboEngine;
using BonoboEngine.Query;

namespace Frond.EditPages.EGateway
{
    public partial class ViewEGatewayLog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.RenderLogMain();
                this.SetToggleButton();
                titleBar.TitleText = "View Form Detail";
            }
        }

        private void RenderLogMain()
        {
            string source = this.Request.QueryString["DataSet"];
            string appFormIDString = this.Request.QueryString["BnbApplicationForm"];
            uxHiddenApplicationFormID.Text = appFormIDString;
            uxHiddenShowFinalOnly.Text = "0";
            BnbQueryDataSet qds = null;
            switch (source)
            {
                case "RegForm":
                    qds = BnbFormsProcessUtils.StoredRegistrationFormData.QueryDataSet;
                    uxBackHyperlink.NavigateUrl = "ImportRegistrationForm.aspx";
                    uxBackHyperlink.Text = "Back to Import Registration Forms page";
                    break;
                case "AppForm":
                    qds = BnbFormsProcessUtils.StoredApplicationFormData.QueryDataSet;
                    uxBackHyperlink.NavigateUrl = "ImportApplicationForm.aspx";
                    uxBackHyperlink.Text = "Back to Import Application Forms page";
                    break;
                case "SearchForm":
                    qds = BnbFormsProcessUtils.StoredSearchFormData.QueryDataSet;
                    uxBackHyperlink.NavigateUrl = "SearchForms.aspx";
                    uxBackHyperlink.Text = "Back to Search Forms page";
                    break;
            }

            // find row - we want to display a few fields from the app form data
            DataRow[] findRows = qds.Tables["results"].Select(String.Format("tblApplicationForm_ApplicationFormID = '{0}'", appFormIDString));
            if (findRows.Length == 1)
            {
                DataRow displayRow = findRows[0];
                uxFormRefLabel.Text = displayRow["tblApplicationForm_FormReference"].ToString();
                uxForenameLabel.Text = displayRow["tblRegistrationPage1_FirstName"].ToString();
                uxSurnameLabel.Text = displayRow["tblRegistrationPage1_Surname"].ToString();
            }

            this.RenderLogTable(new Guid(appFormIDString), false);
        }

        private void RenderLogTable(Guid applicationFormID, bool finalStepsOnly)
        {
            BnbCriteria crit = new BnbCriteria(new BnbListCondition("tblEGatewayLog_ApplicationFormID", applicationFormID));
            if (finalStepsOnly)
                crit.QueryElements.Add(new BnbBooleanCondition("tblEGatewayLog_IsFinalStep", true));

            BnbQuery q = new BnbQuery("vwBonoboEGatewayLog", crit);
            q.NoLocks = true;

            BnbQueryDataSet logqds = q.Execute();

            // render the data as a table
            StringWriter sw = new StringWriter();
            HtmlTextWriter writer = new HtmlTextWriter(sw);
            writer.AddAttribute("class", "datagrid");
            writer.RenderBeginTag("table");
            // col headers
            writer.AddAttribute("class", "datagridheader");
            writer.RenderBeginTag("tr");
            writer.RenderBeginTag("td");
            writer.Write("Process Timestamp");
            writer.RenderEndTag();
            writer.RenderBeginTag("td");
            writer.Write("Step No");
            writer.RenderEndTag();
            writer.RenderBeginTag("td");
            writer.Write("Message");
            writer.RenderEndTag();
            writer.RenderBeginTag("td");
            writer.Write("User");
            writer.RenderEndTag();
            writer.RenderEndTag();
            // render rows
            foreach (DataRow rowLoop in logqds.Tables["Results"].Rows)
            {
                bool finalStep = ((bool)rowLoop["tblEGatewayLog_IsFinalStep"]);
                writer.RenderBeginTag("tr");
                writer.RenderBeginTag("td");
                writer.Write(((DateTime)rowLoop["tblEGatewayLog_ProcessTimestamp"]).ToString("dd/MMM/yyyy HH:mm:ss"));
                writer.RenderEndTag();
                writer.RenderBeginTag("td");
                writer.Write(rowLoop["tblEGatewayLog_StepNo"].ToString());
                writer.RenderEndTag();
                writer.RenderBeginTag("td");
                // bold if last step
                if (finalStep)
                    writer.RenderBeginTag("b");

                writer.Write(Server.HtmlEncode(rowLoop["tblEGatewayLog_ProgressMessage"].ToString()));
                if (finalStep)
                    writer.RenderEndTag();

                writer.RenderEndTag();
                writer.RenderBeginTag("td");
                writer.Write(Server.HtmlEncode(rowLoop["appUser_FullName"].ToString()));
                writer.RenderEndTag();
                writer.RenderEndTag();
            }

            uxLogTable.Text = sw.ToString();

        }

        private void SetToggleButton()
        {
            if (uxHiddenShowFinalOnly.Text == "1")
                uxToggleFinalSteps.Text = "Show all steps";
            else
                uxToggleFinalSteps.Text = "Show final steps only";

        }

        protected void uxToggleFinalSteps_Click(object sender, EventArgs e)
        {
            bool newShowFinalStepsOnly = false;
            if (uxHiddenShowFinalOnly.Text == "0")
                newShowFinalStepsOnly = true;

            Guid appFormID = new Guid(uxHiddenApplicationFormID.Text);
            this.RenderLogTable(appFormID, newShowFinalStepsOnly);

            if (newShowFinalStepsOnly)
                uxHiddenShowFinalOnly.Text = "1";
            else
                uxHiddenShowFinalOnly.Text = "0";

            this.SetToggleButton();
        }
    }
}
