<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportRegistrationForm.aspx.cs" Inherits="Frond.EditPages.EGateway.ImportRegistrationForm" %>

<%@ Register Src="../../UserControls/WaitMessagePanel.ascx" TagName="WaitMessagePanel"
    TagPrefix="uc2" %>

<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.DataGrids"
    TagPrefix="bnbdatagrid" %>

<%@ Register TagPrefix="uc1" TagName="Title" Src="../../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
  <HEAD>
		<title>Import Registration Forms</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<script language="javascript">
   // code to remember scroll-bar position between postbacks
   // (like SmartNavigation, but without the bugs)
   function AltSmartNav_GetCoords()
   {
      var scrollX, scrollY;
      
      if (document.all)
      {
         if (!document.documentElement.scrollLeft)
            scrollX = document.body.scrollLeft;
         else
            scrollX = document.documentElement.scrollLeft;
               
         if (!document.documentElement.scrollTop)
            scrollY = document.body.scrollTop;
         else
            scrollY = document.documentElement.scrollTop;
      }   
      else
      {
         scrollX = window.pageXOffset;
         scrollY = window.pageYOffset;
      }
   
      document.forms["Form1"].hiddenXCoord.value = scrollX;
      document.forms["Form1"].hiddenYCoord.value = scrollY;
   }
   
   function AltSmartNav_Scroll()
   {
      var x = document.forms["Form1"].hiddenXCoord.value;
      var y = document.forms["Form1"].hiddenYCoord.value;
      window.scrollTo(x, y);
   }
   
   window.onload = AltSmartNav_Scroll;
   window.onscroll = AltSmartNav_GetCoords;
   window.onkeypress = AltSmartNav_GetCoords;
   window.onclick = AltSmartNav_GetCoords;
		</script>
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../../Images/batchprocess24silver.gif"></uc1:title>
				<div id="pageContentDiv">
				<p><a href="EGatewayUtil.aspx">E-Gateway Homepage</a></p>
                <p>This is a list of Completed Registration Forms. <asp:Label ID="uxEmailCopyLabel" runat="server"></asp:Label></p>
                <bnbdatagrid:BnbSmartGridWithControls ID="uxRegSmartGrid" runat="server" ShowExportButton = "False" OnColumnButtonClicked="uxRegSmartGrid_ColumnButtonClicked" OnExportClicked="uxRegSmartGrid_ExportClicked" OnOptionsChanged="uxRegSmartGrid_OptionsChanged" SortColumnsByViewPosition="True" />
                <br />
                <table width="800px"><tr>
                <td><asp:Button ID="uxBulkImportButton" runat="server" Text="Bulk Import" OnClick="uxBulkImportButton_Click" />
                (This will import all 'No Match' and 'Email Match' rows)<br />
                    <asp:Label ID="uxBulkimportFeedback" runat="server" Text=""></asp:Label></td>
                <td><asp:Button ID="uxExcelExport" runat="server" OnClick="uxExcelExport_Click" Text="Excel Export" />
                    <asp:CheckBox ID="uxExportAllColumnsCheckbox" runat="server" Text="Export all columns" /></td>
                    </tr>
                <tr><td>Data fetched at 
                    <asp:Label ID="uxDataFetchedTime" runat="server" Text=""></asp:Label><asp:Button
                        ID="uxRefreshData" runat="server" Text="Refresh" OnClick="uxRefreshData_Click" /></td>
                        <td>&nbsp;
                    </td>
                    </tr></table>
                    <asp:Panel ID="uxTestModeEmailPanel" runat="server" width="500px" Visible="False" BorderStyle="Dashed" BorderWidth="1px" >
                    <p><b>Test Mode Emailing</b></p>
                    <asp:Panel ID="uxTestModeEmailOn" runat="server" width="480px">
                    <p>Enabled. To avoid accidental emailing of real people during testing, all invite/decline emails will be sent to: 
                        <asp:Label ID="uxTestModeEmailToLabel" runat="server"></asp:Label></p>
                    <p>To disable Test Mode Emailing, click here: 
                        <asp:Button ID="uxDisableTestEmailModeButton" runat="server" Text="Disable" OnClick="uxDisableTestEmailModeButton_Click" />
                        </asp:Panel>
                        <asp:Panel ID="uxTestModeEmailOff" runat="server" width="480px" Visible="False">
                        <p>Currently Disabled - Emails will go to the normal addresses.</p>
                        <p>To re-enable, click here:<asp:Button ID="uxTestModeEmailEnable" runat="server" Text="Enable" OnClick="uxTestModeEmailEnable_Click" /></p>
                        </asp:Panel>
                    </asp:Panel>
                <p>
                    <asp:GridView ID="uxImportHistoryGrid" runat="server" AutoGenerateColumns="False"
                        CssClass="datagrid" Width="800px" OnRowCommand="uxImportHistoryGrid_RowCommand">
                        <Columns>
                            <asp:BoundField DataField="ImportedDate" DataFormatString="{0:HH:mm:ss}" HeaderText="Import Time" />
                            <asp:BoundField DataField="ImportedSuccessMessage" HeaderText="Success" />
                            <asp:BoundField DataField="FormReference" HeaderText="Form Ref" />
                            <asp:HyperLinkField DataNavigateUrlFields="MatchedApplicationID" DataNavigateUrlFormatString="../ApplicationPage.aspx?BnbApplication={0}"
                                DataTextField="ApplicationDescription" HeaderText="Imported App" />
                            <asp:BoundField DataField="ImportMessages" HeaderText="Import Messages" />
                            <asp:TemplateField HeaderText="View Log" ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="ViewLog"
                                        Text="View Log" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"ApplicationFormID") %>' ></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    
                    </p>
                    </div>
                <uc2:WaitMessagePanel ID="uxWaitMessagePanel" runat="server" DivToHide="pageContentDiv" WidthOverride="800px" />
            </div>
   					<asp:TextBox id="hiddenXCoord" runat="server" style="VISIBILITY:hidden"></asp:TextBox>
					<asp:TextBox id="hiddenYCoord" runat="server" style="VISIBILITY:hidden"></asp:TextBox>

		</form>
	</body>
</HTML>