<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestUtilities.aspx.cs" Inherits="Frond.EditPages.EGateway.TestUtilities" %>

<%@ Register TagPrefix="uc1" TagName="Title" Src="../../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
  <HEAD>
		<title>Import Forms Test Utilities</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server" ></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../../Images/batchprocess24silver.gif"></uc1:title>
				<p><a href="EGatewayUtil.aspx">E-Gateway Homepage</a></p>
				<p>This page has utilities to help with the testing of the Online Applications Imports.</p>
                <p>
                    <asp:Label ID="uxLiveModeLabel" runat="server" Text="" Visible="False"></asp:Label>&nbsp;</p>
                <p>
                    <asp:Panel ID="uxPanelTestMode" runat="server" Width="100%">
                    <table class="fieldtable">
                    <tr>
                    <td colspan = "4" class="label" style="text-align:left;font-weight:normal;"><p><b>Add 5 Completed Registation Forms</b></p>
                    <p>This can be used to generate test daya for the Registration Forms Import.</p>
                    <p>Please enter 5 Titles, Forenames and Surnames below (Please use just Mr, Mrs, Miss or Ms for the title).</p>
                    <p>All the form data will be the same except for the names, but its a good way to test the Bulk Import.</p>
                    <p>For realistic example names, online petitions such as these <a target=_blank href="http://petitions.pm.gov.uk/list/open?sort=signers">E-Petitions</a> are a good source.</p>
                    </td>
                    </tr>
                    <tr>
                    <td class="label"></td>
                    <td class="label" style="text-align:left;">Title</td>
                    <td class="label" style="text-align:left;">Forename</td>
                    <td class="label" style="text-align:left;">Surname</td>
                    </tr>
                    <tr><td class="label">Name 1</td><td>
                        <asp:TextBox ID="uxTitle1" runat="server" Width="80px"></asp:TextBox></td><td>
                        <asp:TextBox ID="uxForename1" runat="server"></asp:TextBox></td><td>
                        <asp:TextBox ID="uxSurname1" runat="server"></asp:TextBox></td></tr>
                    <tr><td class="label">Name 2</td><td>
                        <asp:TextBox ID="uxTitle2" runat="server" Width="80px"></asp:TextBox></td><td>
                        <asp:TextBox ID="uxForename2" runat="server"></asp:TextBox></td><td>
                        <asp:TextBox ID="uxSurname2" runat="server"></asp:TextBox></td></tr>
                    <tr><td class="label">Name 1</td><td>
                        <asp:TextBox ID="uxTitle3" runat="server" Width="80px"></asp:TextBox></td><td>
                        <asp:TextBox ID="uxForename3" runat="server"></asp:TextBox></td><td>
                        <asp:TextBox ID="uxSurname3" runat="server"></asp:TextBox></td></tr>
                    <tr><td class="label">Name 1</td><td>
                        <asp:TextBox ID="uxTitle4" runat="server" Width="80px"></asp:TextBox></td><td>
                        <asp:TextBox ID="uxForename4" runat="server"></asp:TextBox></td><td>
                        <asp:TextBox ID="uxSurname4" runat="server"></asp:TextBox></td></tr>
                    <tr><td class="label">Name 1</td><td>
                        <asp:TextBox ID="uxTitle5" runat="server" Width="80px"></asp:TextBox></td><td>
                        <asp:TextBox ID="uxForename5" runat="server"></asp:TextBox></td><td>
                        <asp:TextBox ID="uxSurname5" runat="server"></asp:TextBox></td></tr>
                    <tr><td colspan="4">
                        <asp:Button ID="uxCreateData" runat="server" OnClick="uxCreateData_Click" Text="Create Data" /><br />
                        <asp:Label ID="uxCreateDataFeedback" runat="server"></asp:Label></td></tr>
                    
                    </table>
                        <br />
                        <br />
                        <br />
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                        <asp:Button ID="uxEmailTest" runat="server" OnClick="uxEmailTest_Click" Text="Email Test" Visible="False" />
                        <asp:Label ID="uxEmailTestLabel" runat="server" Visible="False"></asp:Label></asp:Panel>
                    &nbsp;</p>
				
            </div>
		</form>
	</body>
</HTML>
