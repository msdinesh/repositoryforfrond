using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboEngine;

namespace Frond.EditPages.EGateway
{
    public partial class TestUtilities : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                titleBar.TitleText = "Import Forms Test Utilties";
                BnbConfigurationInfo config = BnbEngine.SessionManager.GetConfigurationInfo();
                if (config.IsTestDatabase)
                {
                    uxPanelTestMode.Visible = true;
                    uxLiveModeLabel.Visible = false;
                }
                else
                {
                    uxPanelTestMode.Visible = false;
                    uxLiveModeLabel.Visible = true;
                    uxLiveModeLabel.Text = "Frond is running on a Live database so these utilities are not available";
                }


            }
        }

        protected void uxCreateData_Click(object sender, EventArgs e)
        {
            this.CreateTestData();
        }

        private void CreateTestData()
        {
            ArrayList testRecords = new ArrayList();

            // create the test record array
            WsOnlineForms.BnbTestDataValues test1 = new WsOnlineForms.BnbTestDataValues();
            test1.Title = uxTitle1.Text;
            test1.Forename = uxForename1.Text;
            test1.Surname = uxSurname1.Text;
            if (test1.Forename != "")
                testRecords.Add(test1);

            WsOnlineForms.BnbTestDataValues test2 = new WsOnlineForms.BnbTestDataValues();
            test2.Title = uxTitle2.Text;
            test2.Forename = uxForename2.Text;
            test2.Surname = uxSurname2.Text;
            if (test2.Forename != "")
                testRecords.Add(test2);

            WsOnlineForms.BnbTestDataValues test3 = new WsOnlineForms.BnbTestDataValues();
            test3.Title = uxTitle3.Text;
            test3.Forename = uxForename3.Text;
            test3.Surname = uxSurname3.Text;
            if (test3.Forename != "")
                testRecords.Add(test3);

            WsOnlineForms.BnbTestDataValues test4 = new WsOnlineForms.BnbTestDataValues();
            test4.Title = uxTitle4.Text;
            test4.Forename = uxForename4.Text;
            test4.Surname = uxSurname4.Text;
            if (test4.Forename != "")
                testRecords.Add(test4);

            WsOnlineForms.BnbTestDataValues test5 = new WsOnlineForms.BnbTestDataValues();
            test5.Title = uxTitle5.Text;
            test5.Forename = uxForename5.Text;
            test5.Surname = uxSurname5.Text;
            if (test5.Forename != "")
                testRecords.Add(test5);

            // make it into an array
            WsOnlineForms.BnbTestDataValues[] testDataArray = new WsOnlineForms.BnbTestDataValues[testRecords.Count];
            testRecords.CopyTo(testDataArray);

            // call the web service
            string castanetUsername = ConfigurationSettings.AppSettings["CastanetAdminUsername"].ToString();
            string castanetPassword = ConfigurationSettings.AppSettings["CastanetAdminPassword"].ToString();

            try
            {
                WsOnlineForms.FrondIntegration fi = new WsOnlineForms.FrondIntegration();
                fi.Timeout = 30 * 1000;
                WsOnlineForms.BnbWebServiceProcessInfo wsProcessInfo = fi.CreateTestRegistrationForms(castanetUsername, castanetPassword,
                    testDataArray);

                string reportBack = null;
                if (wsProcessInfo.Success)
                    reportBack = "Successful<br/>";
                else
                {
                    reportBack = "Failed<br/><br/>";
                    reportBack += String.Join("<br/>", wsProcessInfo.ProgressMessages);
                }
                uxCreateDataFeedback.Text = reportBack;
            }
            catch (Exception e)
            {
                uxCreateDataFeedback.Text = e.Message;
            }

            

        }

        protected void uxEmailTest_Click(object sender, EventArgs e)
        {
            //BnbEmailManager.SendEmailViaWebDAV("mainmail.vsoint.org/exchange", "Ian.Richardson", "vsouk", "richari", "(password)", "ian.richardson@vso.org.uk", "test webdav", "a test email sent using webdav", false, false);

            BnbEmailManager.SendEmailViaWebDAV("mainmail.vsoint.org/exchange", "Ian.Richardson", "vsouk", "richari", "watmm33", "ian.richardson@vso.org.uk", "test webdav", "a test email sent using webdav",true);

            uxEmailTestLabel.Text = "ok";
        }

    }
}
