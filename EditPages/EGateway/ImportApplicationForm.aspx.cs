using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;

namespace Frond.EditPages.EGateway
{
    public partial class ImportApplicationForm : System.Web.UI.Page
    {
        EGatewayUtil eg;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                titleBar.TitleText = "Import Application Forms";
                BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
                this.PopulateApplicationFormGrid(false);
                this.PopulateImportHistoryGrid();
            }
            uxBulkimportFeedback.Text = "";
            uxRefreshData.Attributes.Add("onclick", "javascript:showLoadingMessage('Re-fetching Forms Data ...');");
            uxBulkImportButton.Attributes.Add("onclick", "javascript:showLoadingMessage('Bulk Importing Data ...');");
        }

        public BnbFormsProcessUtils.BnbCachedDataSet GetApplicationFormData(bool forceRefresh)
        {
            BnbFormsProcessUtils.BnbCachedDataSet cachedData = BnbFormsProcessUtils.StoredApplicationFormData;
            if (cachedData != null && !forceRefresh)
                return cachedData;

            WsOnlineForms.FrondIntegration affi = new WsOnlineForms.FrondIntegration();
            if (System.Configuration.ConfigurationManager.AppSettings["WebServiceDefaultCredentials"] != null)
            {
                try
                {
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["WebServiceDefaultCredentials"]) == true)
                    {
                        affi.PreAuthenticate = true;
                        affi.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    }
                }
                catch
                {
                    throw new ApplicationException("Please configure a valid 'WebServiceDefaultCredentials' in Appsettings");
                }
            }
            else
                throw new ApplicationException("Please configure 'WebServiceDefaultCredentials' in Appsettings");
            string castanetUsername = ConfigurationSettings.AppSettings["CastanetAdminUsername"].ToString();
            string castanetPassword = ConfigurationSettings.AppSettings["CastanetAdminPassword"].ToString();

            string recBaseID = Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID].ToString();

            DataSet regDataSet = affi.GetCompletedApplications(castanetUsername, castanetPassword, 0, recBaseID);
            //TPT Ammended:Jira call VAF-1058:Exclude the columns as per the Recruitement base while fetching the application form 
            if (recBaseID.ToUpper() != "ALL")
            {
                string columns = GetExcludeColumnsFromXml();
                string[] columnsList = columns.Split(',');

                for (int i = 0; i < regDataSet.Tables[0].Rows.Count; i++)
                {
                    string columnText = regDataSet.Tables[0].Rows[i]["ColumnName"].ToString();
                    for (int j = 0; j < columnsList.Length; j++)
                    {
                        // trim spaces, carriage returns and tabs from start and end
                        string columnsListValue = columnsList[j].Trim(new char[] { (char)13, (char)10, (char)32, (char)9 });
                        if (columnText == columnsListValue)
                        {
                            regDataSet.Tables[0].Rows[i].Delete();
                        }
                    }
                }
                regDataSet.AcceptChanges();
                regDataSet.Tables[0].AcceptChanges();
            } 
            // convert it to a BnbQueryDataSet
            BnbQueryDataSet qds = BnbQueryUtils.ConvertToBnbQueryDataSet(regDataSet);

            // add new columns
            qds.Tables["results"].Columns.Add("Extra_Import", typeof(string));
            BnbQueryUtils.AddColumnToBnbQueryDataSetColumnInfo(qds.ColumnInfo,
                "Extra_Import", "Import", 1);

            qds.Tables["results"].Columns.Add("Extra_ImportResults", typeof(string));
            BnbQueryUtils.AddColumnToBnbQueryDataSetColumnInfo(qds.ColumnInfo,
                "Extra_ImportResults", "Import Results", 1);

            qds.Tables["results"].Columns.Add("Extra_SkillID", typeof(int));
            BnbQueryUtils.AddColumnToBnbQueryDataSetColumnInfo(qds.ColumnInfo,
                "Extra_SkillID", "Skill", 3);

            // turn appID field into a foreign key column
            BnbQueryUtils.SetBnbQueryDataSetForeignKeyInfo(qds, "tblApplicationForm_MappedApplicationID", "tblApplication");

            // default lookup columns
            foreach (DataRow rowLoop in qds.Tables["results"].Rows)
            {
                rowLoop["Extra_SkillID"] = -1;
            }

            BnbFormsProcessUtils.SetStoredApplicationFormData(qds);

            return BnbFormsProcessUtils.StoredApplicationFormData;
        }

        //TPT Ammended:Jira Call VAF-1058:Exclude the columns from xml file as per the Recruitement base
        public string GetExcludeColumnsFromXml()
        {
            string excludeColumns = "";
            string recruitBaseID = Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID].ToString();
            DataSet dataset = new DataSet();
            string path1 = AppDomain.CurrentDomain.BaseDirectory + @"/EditPages/EGateway/ImportFormExcludeColumns.xml";
            dataset.ReadXml(path1, XmlReadMode.InferSchema);
            DataTable dataTable = dataset.Tables["ExcludeColumns"];
            DataRow[] rows = dataTable.Select("RecBaseID=" + recruitBaseID);
            try
            {
                excludeColumns = rows[0]["ExcludeColumnNames"].ToString();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("This Recruitment Base mapping is missing for 'Exclude Columns'");
            }
            return excludeColumns;
        }


        public void PopulateApplicationFormGrid(bool forceRefresh)
        {
            BnbFormsProcessUtils.BnbCachedDataSet cachedData = this.GetApplicationFormData(forceRefresh);
            BnbQueryDataSet appDataSet = cachedData.QueryDataSet;

            uxDataFetchedTime.Text = cachedData.FetchedDate.ToString("HH:mm:ss");

            uxAppSmartGrid.ClearSettings();
            // set hyperlink map
            DataTable map = uxAppSmartGrid.MakeEmptyHyperlinkMap();
            DataRow mapRow = map.NewRow();
            mapRow["TableName"] = "tblApplication";
            mapRow["HyperlinkUrlPattern"] = "../ApplicationPage.aspx?BnbApplication={0}";
            map.Rows.Add(mapRow);
            uxAppSmartGrid.HyperlinkMap = map;

            // set column controls
            uxAppSmartGrid.AddControlColumnHyperlinkButton("tblApplicationForm_ApplicationFormID", "View");
            uxAppSmartGrid.AddControlColumnButton("Extra_Import", "Import", "Imported, Failed", "javascript:showLoadingMessage('Importing Row ...');");
            uxAppSmartGrid.AddControlColumnLookup("Extra_SkillID", "vlkuBonoboSkillWithGroupAbbrev", true, true);
            uxAppSmartGrid.QueryDataSet = appDataSet;
            uxAppSmartGrid.ColumnDisplayList = this.GetColumnDisplayList();
            if (Session["EGateway_ImportAppForm_SortColumn"] != null)
                uxAppSmartGrid.SortColumn = Session["EGateway_ImportAppForm_SortColumn"].ToString();
            if (Session["EGateway_ImportAppForm_SortDescending"] != null)
                uxAppSmartGrid.SortDescending = (bool)Session["EGateway_ImportAppForm_SortDescending"];
            uxAppSmartGrid.Visible = true;
        }

        protected void uxAppSmartGrid_ColumnButtonClicked(object sender, EventArgs e, string columnName, string rowID)
        {
            if (columnName == "Extra_Import")
            {
                // find row
                DataTable appTable = uxAppSmartGrid.QueryDataSet.Tables["results"];
                DataRow[] findRows = appTable.Select(String.Format("tblApplicationForm_ApplicationFormID = '{0}'", rowID));
                if (findRows.Length == 1)
                {
                    this.ImportApplicationFormRow(findRows[0], uxAppSmartGrid.QueryDataSet);   
                }

            }
            if (columnName == "tblApplicationForm_ApplicationFormID")
            {
                // use rowID to re-direct to viewformdetail
                Response.Redirect(String.Format("ViewFormDetail.aspx?BnbApplicationForm={0}&DataSet=AppForm", rowID));
            }
        }

        private void PopulateImportHistoryGrid()
        {
            DataView dv = BnbFormsProcessUtils.ImportApplicationFormsHistoryDataView;
            if (dv.Count == 0)
                uxImportHistoryGrid.Visible = false;
            else
            {
                uxImportHistoryGrid.DataSource = dv;
                uxImportHistoryGrid.DataBind();
                uxImportHistoryGrid.Visible = true;

            }
        }

        private void ImportApplicationFormRow(DataRow importRow, BnbQueryDataSet qds)
        {
            BnbFormsProcessUtils.ImportApplicationFormProcess(importRow, qds);
            this.PopulateImportHistoryGrid();
        }

        protected void uxRefreshData_Click(object sender, EventArgs e)
        {
            this.PopulateApplicationFormGrid(true);
        }

        protected void uxExcelExport_Click(object sender, EventArgs e)
        {
            uxAppSmartGrid.DownloadExcelExport(uxExportAllColumnsCheckbox.Checked);
        }

        

        protected void uxBulkImportButton_Click(object sender, EventArgs e)
        {
            this.BulkImport();
        }

        public void BulkImport()
        {
            BonoboWebControls.BnbWebFormManager.LogAction("Starting Bulk Import...", null, this);
            ArrayList progressMessages = new ArrayList();
            try
            {
                InternalBulkImport(progressMessages);
            }
            catch (Exception e)
            {
                int errorTicketID = BonoboWebControls.BnbWebFormManager.LogErrorToDatabase(e);
                progressMessages.Add(String.Format("An error occured: {0} (ErrorTicketID {1})",
                    e.Message, errorTicketID));
            }
            string[] progressMessagesArray = new string[progressMessages.Count];
            progressMessages.CopyTo(progressMessagesArray);
            uxBulkimportFeedback.Text = String.Join("<br/>", progressMessagesArray);
            BonoboWebControls.BnbWebFormManager.LogAction("Finished Bulk Import.", null, this);
        }

        private void InternalBulkImport(ArrayList progressMessages)
        {
            progressMessages.Add("Starting Bulk Import ...");

            BnbQueryDataSet qds = uxAppSmartGrid.QueryDataSet;
            int totalRows = qds.Tables["results"].Rows.Count;           
            int importableCount = 0;
            int alreadyImportedCount = 0;
            ArrayList importableRows = new ArrayList();
            foreach (DataRow rowLoop in qds.Tables["results"].Rows)
            {
                string importedInfo = (string)BnbQueryUtils.ReplaceDBNull(rowLoop["Extra_Import"], null);
                if (importedInfo != null && importedInfo != "")
                    alreadyImportedCount += 1;
                else 
                {
                    importableCount += 1;
                    importableRows.Add(rowLoop);
                }
            }
         
            progressMessages.Add(String.Format("Starting bulk import of {0} rows...", importableCount));
            foreach (DataRow importRow in importableRows)
            {
                BnbFormsProcessUtils.ImportApplicationFormProcess(importRow, qds);   
            }
            this.PopulateImportHistoryGrid();
            progressMessages.Add(String.Format("Finished. See results below."));
            
        }

        protected void uxAppSmartGrid_OptionsChanged(object sender, EventArgs e)
        {
            Session["EGateway_ImportAppForm_ColumnDisplayList"] = uxAppSmartGrid.ColumnDisplayList;
            Session["EGateway_ImportAppForm_SortColumn"] = uxAppSmartGrid.SortColumn;
            Session["EGateway_ImportAppForm_SortDescending"] = uxAppSmartGrid.SortDescending;

        }
        private string GetDefaultColumnDisplayList()
        {
            DataSet ds = BnbFormsProcessUtils.GetImportFormsSettings();
            DataTable generalSettings = ds.Tables["GeneralSettings"];
            DataRow settingRow = generalSettings.Rows[0];
            return settingRow["ImportAppFormColumns"].ToString();
        }

        private string GetColumnDisplayList()
        {
            if (Session["EGateway_ImportAppForm_ColumnDisplayList"] != null)
                return Session["EGateway_ImportAppForm_ColumnDisplayList"].ToString();
            else
                return this.GetDefaultColumnDisplayList();
        }

        protected void uxAppSmartGrid_ExportClicked(object sender, BonoboWebControls.DataGrids.BnbSmartGrid.BnbSmartGridExportEventArgs e)
        {
            // try a different suggested filename
            e.SuggestedFilename = String.Format("appcompleted-{0}.xls", DateTime.Now.ToString("ddMMMyyyy-HHmmss"));
            // add some header lines
            e.HeaderLines.Add("Online Applications - Completed Application Forms");
            e.HeaderLines.Add(String.Format("Exported at: {0}", DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss")));
        }

        protected void uxImportHistoryGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ViewLog")
            {
                string appFormID = e.CommandArgument.ToString();
                Response.Redirect(String.Format("ViewEGatewayLog.aspx?BnbApplicationForm={0}&DataSet=AppForm", appFormID));
            }
        }

    }
}
