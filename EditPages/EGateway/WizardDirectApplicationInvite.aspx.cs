using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;

namespace Frond.EditPages.EGateway
{
    public partial class WizardDirectApplicationInvite : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
                this.SetupPanelZero();
            }
            wizardButtons.AddPanel(panelZero);
            wizardButtons.AddPanel(panelOne);
            wizardButtons.CancelWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
            wizardButtons.FinishWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
            wizardButtons.ShowPanel += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
            wizardButtons.ValidatePanel += new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
            //uxResultsSmartGrid.UpdateDataTableFromControls();
        }

        void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
        {
            if (e.CurrentPanelHtmlID == "panelZero")
            {
                if (uxResultsSmartGrid.SelectedRadioButton == null)
                {
                    e.Proceed = false;
                    uxPanelZeroFeedback.Text = "Please search and select a row before pressing Next";
                }
            }
        }

        void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            if (e.CurrentPanelHtmlID == "panelZero")
                this.SetupPanelZero();
            if (e.CurrentPanelHtmlID == "panelOne")
                this.SetupPanelOne();

        }

        private void SetupPanelOne()
        {
            uxPanelOneFeedback.Text = "";
            uxInviteButton.Enabled = true;
            // get individualID from grid
            uxHiddenIndividualID.Text = uxResultsSmartGrid.SelectedRadioButton;
            // load record and display details
            Guid individualID = new Guid(uxHiddenIndividualID.Text);
            BnbIndividual indv = BnbIndividual.Retrieve(individualID);
            uxChosenRefNoLabel.Text = indv.RefNo;
            uxChosenNameLabel.Text = indv.FullName;
            // get lead skill and display
            uxLeadSkill.Text = "";
            if (indv.IndividualSkills.Count > 0)
            {
                BnbIndividualSkill leadSkill = indv.IndividualSkills.GetLead() as BnbIndividualSkill;
                if (leadSkill != null)
                {
                    string skillDesc = BnbEngine.LookupManager.GetLookupItemDescription("vlkuBonoboSkillWithGroupAbbrev", leadSkill.SkillID);
                    uxLeadSkill.Text = skillDesc;
                }
            }


            if (indv.Email != null)
                uxChosenEmailLabel.Text = indv.Email.ContactNumber;
            else
            {
                uxChosenEmailLabel.Text = "*No Email Address set*";
                uxPanelOneFeedback.Text = "Cannot invite this Individual because they do not have an Email address. Please press Cancel or go Back and choose a different Individual";
                uxInviteButton.Enabled = false;
            }

            uxFinishSummary.Text = "";
            uxInviteProgress.Text = "";

            uxSkillAdvicePanel.Visible = false;
            uxIndividualLink.NavigateUrl = String.Format("../IndividualPage.aspx?BnbIndividual={0}", indv.ID.ToString());

            this.DisplayEmailTestMode();
        }

        void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            if (uxFinishSummary.Text == "")
            {
                uxPanelOneFeedback.Text = "Please press Invite before pressing Finish";
                return;
            }
            Response.Redirect("EGatewayUtil.aspx");
        }

        void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            Response.Redirect("EGatewayUtil.aspx");
        }

        public void SetupPanelZero()
        {
            uxResultsSmartGrid.ClearSettings();
            uxResultsSmartGrid.Visible = false;

            uxQueryBuilderComposite.ViewName = "vwBonoboQueryIndividual";
            // TPT amended: PGMSSRONE-8 included ExcludeColumns property 
            uxQueryBuilderComposite.ExcludeColumns = "";
            BnbQueryBuilder qb = new BnbQueryBuilder(uxQueryBuilderComposite.ViewName);
            string defaultCriteriaXML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                            +"<BnbCriteria xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><LogicalOperator>And</LogicalOperator><QueryElements>"
                            +"<BnbTextCondition><LogicalOperator>And</LogicalOperator><Fieldname>tblIndividualKeyInfo_old_indv_id</Fieldname><TextCompareOption>ExactMatch</TextCompareOption><CompareText /></BnbTextCondition>"
                            +"<BnbTextCondition><LogicalOperator>And</LogicalOperator><Fieldname>tblIndividualKeyInfo_Forename</Fieldname><TextCompareOption>StartOfField</TextCompareOption><CompareText /></BnbTextCondition>"
                            +"<BnbTextCondition><LogicalOperator>And</LogicalOperator><Fieldname>tblIndividualKeyInfo_Surname</Fieldname><TextCompareOption>StartOfField</TextCompareOption><CompareText /></BnbTextCondition>"
                            +"<BnbTextCondition><LogicalOperator>And</LogicalOperator><Fieldname>Custom_Email</Fieldname><TextCompareOption>StartOfField</TextCompareOption><CompareText /></BnbTextCondition></QueryElements></BnbCriteria>";
            BnbCriteria defaultCrit = BnbCriteria.BuildFromXml(defaultCriteriaXML);
            qb.Query.Criteria = defaultCrit;
            DataTable defaultWhere = qb.CriteriaAsWhereTable();
            uxQueryBuilderComposite.WhereTable = defaultWhere;
            

           



        }

        protected void uxFindButton_Click(object sender, EventArgs e)
        {
            this.FindIndividuals();
        }

        private void FindIndividuals()
        {
            uxPanelZeroFeedback.Text = "";
            if (!uxQueryBuilderComposite.CheckFilter())
                return;
			

            BnbQueryBuilder qb = uxQueryBuilderComposite.GetQueryBuilder(true);
            if (qb.Query.Criteria.QueryElements.Count == 0)
            {
                uxPanelZeroFeedback.Text = "Please enter some search criteria";
                return;
            }

            qb.Query.NoLocks = true;
            BnbQueryDataSet queryResults = null;
            try
            {
                queryResults = qb.Query.Execute();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                if (ex.Message.StartsWith("Timeout expired"))
                {
                    uxPanelZeroFeedback.Text = "Your query was taking too long - please narrow your search. ";
                    return;
                }
                else
                    throw ex;

            }
            if (queryResults != null)
            {
                // apply rules to results
                BonoboDomainObjects.BnbQueryRules.ApplyRulesToQueryData(queryResults);

                // if no column list then generate default
                if (qb.ColumnDisplayList == null || qb.ColumnDisplayList == "")
                    qb.ColumnDisplayList = "Extra_Choose, tblIndividualKeyInfo_old_indv_id, Custom_DisplayName, Custom_Email";

                // add a radio button column
                queryResults.Tables["results"].Columns.Add("Extra_Choose", typeof(string));
                BnbQueryUtils.AddColumnToBnbQueryDataSetColumnInfo(queryResults.ColumnInfo,
                    "Extra_Choose", "Choose", 1);
                //APPN-41 - direct Invite fix for Netherland
                string columnList = uxResultsSmartGrid.ColumnDisplayList;
                if (columnList == null)
                {
                    uxResultsSmartGrid.AddControlColumnRadioButton("Extra_Choose");
                }

                // bind to smart grid
                uxResultsSmartGrid.QueryDataSet = queryResults;
                uxResultsSmartGrid.ColumnDisplayList = qb.ColumnDisplayList;
                uxResultsSmartGrid.SortColumn = qb.SortColumn;
                uxResultsSmartGrid.SortDescending = qb.SortDescending;
                uxResultsSmartGrid.Visible = true;
                
            }
        }

        protected void uxInviteButton_Click(object sender, EventArgs e)
        {
            PerformInviteAndShowFeedback();
        }

        private void PerformInviteAndShowFeedback()
        {
            uxPanelOneFeedback.Text = "";
            Guid individualID = new Guid(uxHiddenIndividualID.Text);
            BnbFormsProcessUtils.BnbProcessInfo processInfo = BnbFormsProcessUtils.InviteDirectApplicationProcess(individualID);
            uxInviteProgress.Text = String.Join("<br/>", processInfo.ProgressMessages);
            if (processInfo.Success)
            {
                uxFinishSummary.Text = "Invititation Process Completed. Please press Finish";

                // if they dont have a lead skill, show some advice
                if (uxLeadSkill.Text == "")
                    uxSkillAdvicePanel.Visible = true;
            }
            else
            {
                uxFinishSummary.Text = "";
                uxPanelOneFeedback.Text = "Invitation Process Failed";
            }
        }

        /// <summary>
        /// displays or toggles mode of the Test Mode Email panel
        /// </summary>
        private void DisplayEmailTestMode()
        {
            if (BnbFormsProcessUtils.TestModeEmail)
            {
                uxTestModeEmailPanel.Visible = true;
                uxTestModeEmailToLabel.Text = BnbFormsProcessUtils.TestModeEmailTo;
                uxTestModeEmailOn.Visible = BnbFormsProcessUtils.TestModeEmailEnabled;
                uxTestModeEmailOff.Visible = !BnbFormsProcessUtils.TestModeEmailEnabled;
            }
        }

        
    }
}
