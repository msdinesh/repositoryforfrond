<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchForms.aspx.cs" Inherits="Frond.EditPages.EGateway.SearchForms" %>
<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.ServicesControls" TagPrefix="cc1" %>
<%@ Register Src="../../UserControls/WaitMessagePanel.ascx" TagName="WaitMessagePanel" TagPrefix="uc2" %>
<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.DataGrids" TagPrefix="bnbdatagrid" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
  <HEAD>
		<title>Search Online Application Forms</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<script language="javascript">
   // code to remember scroll-bar position between postbacks
   // (like SmartNavigation, but without the bugs)
   function AltSmartNav_GetCoords()
   {
      var scrollX, scrollY;
      
      if (document.all)
      {
         if (!document.documentElement.scrollLeft)
            scrollX = document.body.scrollLeft;
         else
            scrollX = document.documentElement.scrollLeft;
               
         if (!document.documentElement.scrollTop)
            scrollY = document.body.scrollTop;
         else
            scrollY = document.documentElement.scrollTop;
      }   
      else
      {
         scrollX = window.pageXOffset;
         scrollY = window.pageYOffset;
      }
   
      document.forms["Form1"].hiddenXCoord.value = scrollX;
      document.forms["Form1"].hiddenYCoord.value = scrollY;
   }
   
   function AltSmartNav_Scroll()
   {
      var x = document.forms["Form1"].hiddenXCoord.value;
      var y = document.forms["Form1"].hiddenYCoord.value;
      window.scrollTo(x, y);
   }
   
   window.onload = AltSmartNav_Scroll;
   window.onscroll = AltSmartNav_GetCoords;
   window.onkeypress = AltSmartNav_GetCoords;
   window.onclick = AltSmartNav_GetCoords;
		</script>
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server" OnLoad="menuBar_Load"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../../Images/batchprocess24silver.gif"></uc1:title>
				<div id="pageContentDiv">
				<p><a href="EGatewayUtil.aspx">E-Gateway Homepage</a></p>
                <p>This page can be used to search through previously imported forms.</p>
                <table class="fieldtable">
                <tr><td class="label">Form Status</td><td style="width: 7px">
                    <asp:DropDownList ID="uxFormStatusLookup" runat="server">
                    </asp:DropDownList></td>
                    <td style="width: 251px">
                    </td>
                    <td style="width: 251px">
                    </td>
                </tr>
                <tr><td class="label">Imported From</td><td style="width: 7px">
                    <bnbdatacontrol:BnbStandaloneDateBox ID="uxImportedFromDateBox" runat="server" CssClass="datebox"
                        DateCulture="en-GB" DateFormat="dd/MMM/yyyy" Height="20px" Width="90px" />
                </td>
                    <td style="width: 251px">
                    </td>
                    <td style="width: 251px">
                    </td>
                </tr>
                <tr><td class="label">Imported To</td><td style="width: 7px">
                    <bnbdatacontrol:BnbStandaloneDateBox ID="uxImportedToDateBox" runat="server" CssClass="datebox"
                        DateCulture="en-GB" DateFormat="dd/MMM/yyyy" Height="20px" Width="90px" />
                </td>
                    <td style="width: 251px">
                    </td>
                    <td style="width: 251px">
                    </td>
                </tr>
                    <tr>
                        <td class="label">
                            Created From</td>
                        <td style="width: 7px">
                        <bnbdatacontrol:BnbStandaloneDateBox ID="uxCreatedFromDateBox" runat="server" CssClass="datebox"
                        DateCulture="en-GB" DateFormat="dd/MMM/yyyy" Height="20px" Width="90px" />
                        </td>
                        <td style="width: 251px">
                        </td>
                        <td style="width: 251px">
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Created To</td>
                        <td style="width: 7px">
                        <bnbdatacontrol:BnbStandaloneDateBox ID="uxCreatedToDateBox" runat="server" CssClass="datebox"
                        DateCulture="en-GB" DateFormat="dd/MMM/yyyy" Height="20px" Width="90px" />
                        </td>
                        <td style="width: 251px">
                        </td>
                        <td style="width: 251px">
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Form Ref</td>
                        <td style="width: 7px">
                            <asp:TextBox ID="uxFormReferenceTextbox" runat="server"></asp:TextBox></td>
                        <td style="width: 251px">
                            (e.g. FRM001234)</td>
                        <td style="width: 251px">
                        </td>
                    </tr>
                <tr><td class="label">Ref No</td><td style="width: 7px">
                    <asp:TextBox ID="uxRefNoTextbox" runat="server"></asp:TextBox></td>
                    <td style="width: 251px">
                        (will only work for Application Form stage)</td>
                    <td style="width: 251px">
                    </td>
                </tr>
                <tr><td class="label">Forename</td><td style="width: 7px">
                    <asp:TextBox ID="uxForenameTextbox" runat="server"></asp:TextBox></td>
                    <td style="width: 251px">
                        (Starts with)</td>
                    <td style="width: 251px">
                    </td>
                </tr>
                <tr><td class="label">Surname</td><td style="width: 7px">
                    <asp:TextBox ID="uxSurnameTextbox" runat="server"></asp:TextBox></td>
                    <td style="width: 251px">
                        (Starts with)</td>
                    <td style="width: 251px">
                        <asp:Button ID="uxClearAll" runat="server" OnClick="uxClearAll_Click" Text="Clear All" /></td>
                </tr>
                </table>
                <br />
                <asp:Label ID="uxFetchSummaryLabel" runat="server"></asp:Label><br />
                <br />
                <asp:Button ID="uxFetchForms" runat="server" Text="Fetch Forms" OnClick="uxFetchForms_Click" /><br />
                    <asp:Panel ID="uxResultsPanel" runat="server" Width="100%" Visible="False" >
                    
                    <br />
                <br />
                <bnbdatagrid:BnbSmartGridWithControls ID="uxResultsSmartGrid" runat="server" ShowExportButton = "False" OnExportClicked="uxResultsSmartGrid_ExportClicked" OnColumnButtonClicked="uxResultsSmartGrid_ColumnButtonClicked" OnOptionsChanged="uxResultsSmartGrid_OptionsChanged" SortColumnsByViewPosition="True" />
                    <p><asp:Button ID="uxExcelExport" runat="server"  Text="Excel Export" OnClick="uxExcelExport_Click" />
                    <asp:CheckBox ID="uxExportAllColumnsCheckbox" runat="server" Text="Export all columns" />
                        
                    </p>
                    <p>
                        <cc1:BnbWordFormButton ID="uxWordFormExport" runat="server" CssClass="button" OnClicked="uxWordFormExport_Clicked"
                            Text="Word Export" /> (All rows)</p>
                            
                            </asp:Panel>
                        
                
                
                </div>
                <uc2:WaitMessagePanel ID="WaitMessagePanel1" runat="server" DivToHide="pageContentDiv" WidthOverride="800px" />
            </div>
            <asp:TextBox id="hiddenXCoord" runat="server" style="VISIBILITY:hidden"></asp:TextBox>
					<asp:TextBox id="hiddenYCoord" runat="server" style="VISIBILITY:hidden"></asp:TextBox>

		</form>
	</body>
</HTML>