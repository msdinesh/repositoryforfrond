using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;

namespace Frond.EditPages.EGateway
{
    public partial class SearchForms : System.Web.UI.Page
    {

        private bool m_wordExportDone = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                titleBar.TitleText = "Search Online Application Forms";
                BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
                uxResultsSmartGrid.ClearSettings();
                uxResultsSmartGrid.Visible = false;
                this.SetupPage();
                this.RecallSearchCriteria();
                this.RecallSearchResults();
            }
            uxFetchForms.Attributes.Add("onclick", "javascript:showLoadingMessage('Fetching Forms Data ...');");
        }

        private void RecallSearchResults()
        {
            if (BnbFormsProcessUtils.StoredSearchFormData != null)
            {
                BnbFormsProcessUtils.BnbCachedDataSet cached = BnbFormsProcessUtils.StoredSearchFormData;
                this.ShowResults(cached.QueryDataSet);

            }
        }

        private void SetupPage()
        {
            // setup the form status lookup
            WsOnlineForms.FrondIntegration affi = new WsOnlineForms.FrondIntegration();
            if (System.Configuration.ConfigurationManager.AppSettings["WebServiceDefaultCredentials"] != null)
            {
                try
                {
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["WebServiceDefaultCredentials"]) == true)
                    {
                        affi.PreAuthenticate = true;
                        affi.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    }
                }
                catch
                {
                    throw new ApplicationException("Please configure a valid 'WebServiceDefaultCredentials' in Appsettings");
                }
            }
            else
                throw new ApplicationException("Please configure 'WebServiceDefaultCredentials' in Appsettings");
            string castanetUsername = ConfigurationSettings.AppSettings["CastanetAdminUsername"].ToString();
            string castanetPassword = ConfigurationSettings.AppSettings["CastanetAdminPassword"].ToString();
            DataTable formStatusLookup = affi.GetLookupTable(castanetUsername, castanetPassword, "lkuApplicationFormStatus");
            // add a blank option
            DataRow newRow = formStatusLookup.NewRow();
            newRow["IntID"] = -1;
            newRow["Description"] = "<Any>";
            newRow["Exclude"] = false;
            formStatusLookup.Rows.Add(newRow);
            formStatusLookup.DefaultView.Sort = "Description";
            formStatusLookup.DefaultView.RowFilter = "Exclude = 0";

            uxFormStatusLookup.DataSource = formStatusLookup.DefaultView;
            uxFormStatusLookup.DataValueField = "IntID";
            uxFormStatusLookup.DataTextField = "Description";
            uxFormStatusLookup.DataBind();
        }

        protected void menuBar_Load(object sender, EventArgs e)
        {
            
        }

        private void FetchForms()
        {
            uxFetchSummaryLabel.Text = "";
            // first check the input
            int formStatusID = int.Parse(uxFormStatusLookup.SelectedValue);
            if (formStatusID == -1 && uxImportedFromDateBox.Date == DateTime.MinValue &&
                uxImportedToDateBox.Date == DateTime.MinValue 
                && uxFormReferenceTextbox.Text == ""
                && uxRefNoTextbox.Text == ""
                && uxForenameTextbox.Text == "" && uxSurnameTextbox.Text == "" &&
                uxCreatedFromDateBox.Date == DateTime.MinValue && uxCreatedToDateBox.Date == DateTime.MinValue)
            {
                uxFetchSummaryLabel.Text = "Please fill in some of the search criteria before pressing the Fetch button";
                return;
            }
            this.RememberSearchCriteria();
            // call the w.s.
            WsOnlineForms.FrondIntegration affi = new WsOnlineForms.FrondIntegration();
            if (System.Configuration.ConfigurationManager.AppSettings["WebServiceDefaultCredentials"] != null)
            {
                try
                {
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["WebServiceDefaultCredentials"]) == true)
                    {
                        affi.PreAuthenticate = true;
                        affi.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    }
                }
                catch
                {
                    throw new ApplicationException("Please configure a valid 'WebServiceDefaultCredentials' in Appsettings");
                }
            }
            else
                throw new ApplicationException("Please configure 'WebServiceDefaultCredentials' in Appsettings");
            string castanetUsername = ConfigurationSettings.AppSettings["CastanetAdminUsername"].ToString();
            string castanetPassword = ConfigurationSettings.AppSettings["CastanetAdminPassword"].ToString();
            //TPT:Call to Set RecruitmentBaseID method,Assign value to string - Support Call dated 10/05/2011
            SetRecBaseIDInSession();
            string recBaseID = Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID].ToString();
            //TPT:Add recBaseID as parameter
            DataSet searchResultsRaw = affi.SearchForms(castanetUsername, castanetPassword,
                formStatusID, uxImportedFromDateBox.Date, uxImportedToDateBox.Date, 
                uxCreatedFromDateBox.Date, uxCreatedToDateBox.Date ,
                uxFormReferenceTextbox.Text, uxRefNoTextbox.Text, uxForenameTextbox.Text, uxSurnameTextbox.Text, recBaseID);
            //TPT Ammended:Jira Call VAF-1058:Exclude the columns as per the Recruitement base while searching reg/app forms
            if (recBaseID.ToUpper() != "ALL")
            {
                string columns = GetExcludeColumnsFromXml();
                string[] columnsList = columns.Split(',');

                for (int i = 0; i < searchResultsRaw.Tables[0].Rows.Count; i++)
                {
                    string columnText = searchResultsRaw.Tables[0].Rows[i]["ColumnName"].ToString();
                    for (int j = 0; j < columnsList.Length; j++)
                    {
                        // trim spaces, carriage returns and tabs from start and end
                        string columnsListValue = columnsList[j].Trim(new char[] { (char)13, (char)10, (char)32, (char)9 });
                        if (columnText == columnsListValue)
                        {
                            searchResultsRaw.Tables[0].Rows[i].Delete();
                        }
                    }
                }
                searchResultsRaw.AcceptChanges();
                searchResultsRaw.Tables[0].AcceptChanges();
            }  
            // convert it to a BnbQueryDataSet
            BnbQueryDataSet qds = BnbQueryUtils.ConvertToBnbQueryDataSet(searchResultsRaw);

            // turn appID field into a foreign key column
            BnbQueryDataSet.ColumnInfoRow appIDRow = qds.ColumnInfo.Rows.Find("tblApplicationForm_MappedApplicationID") as BnbQueryDataSet.ColumnInfoRow;
            if (appIDRow != null)
                appIDRow.ForeignKeyTable = "tblApplication";

            // add column for word export
            qds.Tables["results"].Columns.Add("Extra_WordExport", typeof(string));
            BnbQueryUtils.AddColumnToBnbQueryDataSetColumnInfo(qds.ColumnInfo,
                "Extra_WordExport", "Word Export", 1);
            // add column for log view
            qds.Tables["results"].Columns.Add("Extra_ViewLog", typeof(string));
            BnbQueryUtils.AddColumnToBnbQueryDataSetColumnInfo(qds.ColumnInfo,
                "Extra_ViewLog", "View Log", 1);

            // cache the search result so the ViewFormDetail page works
            BnbFormsProcessUtils.SetStoredSearchFormData(qds);
            this.ShowResults(qds);
        }

        //TPT Ammended:Jira Call VAF-1058:Exclude the columns from xml file as per the Recruitement base
        public string GetExcludeColumnsFromXml()
        {
            string excludeColumns = "";
            string recruitBaseID = Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID].ToString();
            DataSet dataset = new DataSet();
            string path1 = AppDomain.CurrentDomain.BaseDirectory + @"/EditPages/EGateway/ImportFormExcludeColumns.xml";
            dataset.ReadXml(path1, XmlReadMode.InferSchema);
            DataTable dataTable = dataset.Tables["ExcludeColumns"];
            DataRow[] rows = dataTable.Select("RecBaseID=" + recruitBaseID);
            try
            {
                excludeColumns = rows[0]["ExcludeColumnNames"].ToString();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("This Recruitment Base mapping is missing for 'Exclude Columns'");
            }
            return excludeColumns;
        }

        //TPT: Methods added to set RecruitmentBaseID & Check Permission - Support Call dated 10/05/2011
        private void SetRecBaseIDInSession()
        {
            if (HasPermissionToViewAllRegOrAppForms())
                Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID] = "ALL";
            else
                Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID] = BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID.ToString();
        }

        private bool HasPermissionToViewAllRegOrAppForms()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_ViewAllRegOrAppForms))
                return true;
            else
                return false;
        }
        //TPT: Code Change ends

        private void RememberSearchCriteria()
        {
            Session["EGateway_SearchForms_FormStatusID"] = uxFormStatusLookup.SelectedValue;
            Session["EGateway_SearchForms_ImportedFrom"] = uxImportedFromDateBox.Date;
            Session["EGateway_SearchForms_ImportedTo"] = uxImportedToDateBox.Date;
            Session["EGateway_SearchForms_CreatedFrom"] = uxCreatedFromDateBox.Date;
            Session["EGateway_SearchForms_CreatedTo"] = uxCreatedToDateBox.Date;
            Session["EGateway_SearchForms_FormRef"] = uxFormReferenceTextbox.Text;
            Session["EGateway_SearchForms_RefNo"] = uxRefNoTextbox.Text;
            Session["EGateway_SearchForms_Forename"] = uxForenameTextbox.Text;
            Session["EGateway_SearchForms_Surname"] = uxSurnameTextbox.Text;
        }

        private void RecallSearchCriteria()
        {
            if (Session["EGateway_SearchForms_FormStatusID"] != null)
                uxFormStatusLookup.SelectedValue = Session["EGateway_SearchForms_FormStatusID"].ToString();
            if (Session["EGateway_SearchForms_ImportedFrom"] != null)
                uxImportedFromDateBox.Date = (DateTime)Session["EGateway_SearchForms_ImportedFrom"];
            if (Session["EGateway_SearchForms_ImportedTo"] != null)
                uxImportedToDateBox.Date = (DateTime)Session["EGateway_SearchForms_ImportedTo"];
            if (Session["EGateway_SearchForms_CreatedFrom"] != null)
                uxCreatedFromDateBox.Date = (DateTime)Session["EGateway_SearchForms_CreatedFrom"];
            if (Session["EGateway_SearchForms_CreatedTo"] != null)
                uxCreatedToDateBox.Date = (DateTime)Session["EGateway_SearchForms_CreatedTo"];
            if (Session["EGateway_SearchForms_FormRef"] != null)
                uxFormReferenceTextbox.Text = Session["EGateway_SearchForms_FormRef"].ToString();
            if (Session["EGateway_SearchForms_RefNo"] != null)
                uxRefNoTextbox.Text = Session["EGateway_SearchForms_RefNo"].ToString();
            if (Session["EGateway_SearchForms_Forename"] != null)
                uxForenameTextbox.Text = Session["EGateway_SearchForms_Forename"].ToString();
            if (Session["EGateway_SearchForms_Surname"] != null)
                uxSurnameTextbox.Text = Session["EGateway_SearchForms_Surname"].ToString();
        }

        private void ShowResults(BnbQueryDataSet qds)
        {
            uxResultsSmartGrid.ClearSettings();
            // set hyperlink map
            DataTable map = uxResultsSmartGrid.MakeEmptyHyperlinkMap();
            DataRow mapRow = map.NewRow();
            mapRow["TableName"] = "tblApplicationForm";
            mapRow["HyperlinkUrlPattern"] = "ViewFormDetail.aspx?BnbApplicationForm={0}&DataSet=SearchForm";
            map.Rows.Add(mapRow);
            mapRow = map.NewRow();
            mapRow["TableName"] = "tblApplication";
            mapRow["HyperlinkUrlPattern"] = "../ApplicationPage.aspx?BnbApplication={0}";
            map.Rows.Add(mapRow);
            uxResultsSmartGrid.HyperlinkMap = map;
           
            uxResultsSmartGrid.AddControlColumnButton("Extra_WordExport", "Word Export", null);
            uxResultsSmartGrid.AddControlColumnHyperlinkButton("Extra_ViewLog", "View Log");
            uxResultsSmartGrid.QueryDataSet = qds;
            uxResultsSmartGrid.ColumnDisplayList = this.GetColumnDisplayList();
            if (Session["EGateway_SearchForms_SortColumn"] != null)
                uxResultsSmartGrid.SortColumn = Session["EGateway_SearchForms_SortColumn"].ToString();
            if (Session["EGateway_SearchForms_SortDescending"] != null)
                uxResultsSmartGrid.SortDescending = (bool)Session["EGateway_SearchForms_SortDescending"];
            uxResultsSmartGrid.Visible = true;
            uxResultsPanel.Visible = true;
        }

        protected void uxExcelExport_Click(object sender, EventArgs e)
        {
            uxResultsSmartGrid.DownloadExcelExport(uxExportAllColumnsCheckbox.Checked);
        }

        

        protected void uxWordFormExport_Clicked(object sender, EventArgs e)
        {
            this.DoWordMailMerge();
        }

        public void DoWordMailMerge()
        {
            this.DoWordMailMerge(Guid.Empty);
        }

        public void DoWordMailMerge(Guid rowID)
        {
            if (m_wordExportDone)
                return;

            DataTable dataForWord = uxResultsSmartGrid.QueryDataSet.Tables["results"];
            if (rowID != Guid.Empty)
            {
                dataForWord.DefaultView.RowFilter = String.Format("tblApplicationForm_ApplicationFormID = '{0}'", rowID);
                // copy that row to a new table
                DataTable singleRowClone = dataForWord.Clone();
                DataRow singlerow = singleRowClone.NewRow();
                foreach (DataColumn colLoop in singleRowClone.Columns)
                    singlerow[colLoop] = dataForWord.DefaultView[0][colLoop.ColumnName];
                singleRowClone.Rows.Add(singlerow);
                // switch them round
                dataForWord = singleRowClone;
            }
            string dir = AppDomain.CurrentDomain.BaseDirectory;
            uxWordFormExport.WordTemplate = String.Format("{0}\\Templates\\AppForm.doc", dir);
            uxWordFormExport.MergeData = dataForWord;
            bool success = uxWordFormExport.GenerateWordForm();
            if (success)
            {
                // great!
            }
            m_wordExportDone = true;
        }

        protected void uxResultsSmartGrid_ColumnButtonClicked(object sender, EventArgs e, string columnName, string rowID)
        {
            if (columnName == "Extra_WordExport")
            {
                Guid rowIDGuid = new Guid(rowID);
                this.DoWordMailMerge(rowIDGuid);
            }
            if (columnName == "Extra_ViewLog")
            {
                Response.Redirect(String.Format("ViewEGatewayLog.aspx?BnbApplicationForm={0}&DataSet=SearchForm", rowID));
            }
        }

        protected void uxFetchForms_Click(object sender, EventArgs e)
        {
            this.FetchForms();
        }

        protected void uxResultsSmartGrid_OptionsChanged(object sender, EventArgs e)
        {
            Session["EGateway_SearchForms_ColumnDisplayList"] = uxResultsSmartGrid.ColumnDisplayList;
            Session["EGateway_SearchForms_SortColumn"] = uxResultsSmartGrid.SortColumn;
            Session["EGateway_SearchForms_SortDescending"] = uxResultsSmartGrid.SortDescending;
        }

        private string GetDefaultColumnDisplayList()
        {
            DataSet ds = BnbFormsProcessUtils.GetImportFormsSettings();
            DataTable generalSettings = ds.Tables["GeneralSettings"];
            DataRow settingRow = generalSettings.Rows[0];
            return settingRow["SearchFormsColumns"].ToString();
        }

        private string GetColumnDisplayList()
        {
            if (Session["EGateway_SearchForms_ColumnDisplayList"] != null)
                return Session["EGateway_SearchForms_ColumnDisplayList"].ToString();
            else
                return this.GetDefaultColumnDisplayList();
        }

        protected void uxResultsSmartGrid_ExportClicked(object sender, BonoboWebControls.DataGrids.BnbSmartGrid.BnbSmartGridExportEventArgs e)
        {
            if (e.ExportData.Columns.Contains("Extra_WordExport"))
                e.ExportData.Columns.Remove("Extra_WordExport");
            // try a different suggested filename
            e.SuggestedFilename = String.Format("formsearch-{0}.xls", DateTime.Now.ToString("ddMMMyyyy-HHmmss"));
            // add some header lines
            e.HeaderLines.Add("Online Applications - Search Forms");
            e.HeaderLines.Add(String.Format("Exported at: {0}", DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss")));

        }

        protected void uxClearAll_Click(object sender, EventArgs e)
        {
            Session["EGateway_SearchForms_FormStatusID"] = null;
            Session["EGateway_SearchForms_ImportedFrom"] = null;
            Session["EGateway_SearchForms_ImportedTo"] = null;
            Session["EGateway_SearchForms_CreatedFrom"] = null;
            Session["EGateway_SearchForms_CreatedTo"] = null;
            Session["EGateway_SearchForms_FormRef"] = null;
            Session["EGateway_SearchForms_RefNo"] = null;
            Session["EGateway_SearchForms_Forename"] = null;
            Session["EGateway_SearchForms_Surname"] = null;
            // apparently only way to clear controls is to reload the whole page
            Response.Redirect("SearchForms.aspx");
        }



    }
}
