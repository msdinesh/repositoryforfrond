<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewFormDetail.aspx.cs" Inherits="Frond.EditPages.EGateway.ViewFormDetail" %>



<%@ Register TagPrefix="uc1" TagName="Title" Src="../../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="NavigatePanel" Src="../../UserControls/NavigatePanel.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
  <HEAD>
		<title>View Form Detail</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../../Images/batchprocess24silver.gif"></uc1:title>
                <p><a href="EGatewayUtil.aspx">E-Gateway Homepage</a></p>
                <p><asp:HyperLink ID="uxBackHyperlink" runat="server"></asp:HyperLink></p>
                
                <asp:Label ID="uxDetailTable" runat="server" Text=""></asp:Label>
				</div>
		</form>
	</body>
</HTML>