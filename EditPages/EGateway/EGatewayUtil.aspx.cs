using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboEngine;
using BonoboDomainObjects;

namespace Frond.EditPages.EGateway
{
    public partial class EGatewayUtil : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                titleBar.TitleText = "E-Gateway Homepage";
                BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
                WsOnlineForms.FrondIntegration affi = new WsOnlineForms.FrondIntegration();
                if (System.Configuration.ConfigurationManager.AppSettings["WebServiceDefaultCredentials"] != null)
                {
                    try
                    {
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["WebServiceDefaultCredentials"]) == true)
                        {
                            affi.PreAuthenticate = true;
                            affi.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        }
                    }
                    catch
                    {
                        throw new ApplicationException("Please configure a valid 'WebServiceDefaultCredentials' in Appsettings");
                    }
                }
                else
                    throw new ApplicationException("Please configure 'WebServiceDefaultCredentials' in Appsettings");
                uxWsApplicationFormUrl.Text = affi.Url.ToString();

                CheckAndDispalyRegAndAppFormdetails();

                string response = "";
                try
                {
                    string castanetUsername = ConfigurationSettings.AppSettings["CastanetAdminUsername"].ToString();
                    string castanetPassword = ConfigurationSettings.AppSettings["CastanetAdminPassword"].ToString();

                    string recBaseID = BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID.ToString();

                    WsOnlineForms.BnbFormSummaryInfo summInfo = affi.GetFormSummaryInfo(castanetUsername, castanetPassword, recBaseID);
                    if (System.Configuration.ConfigurationManager.AppSettings["WebServiceDefaultCredentials"] != null)
                    {
                        try
                        {
                            if (Convert.ToBoolean(ConfigurationManager.AppSettings["WebServiceDefaultCredentials"]) == true)
                            {
                                affi.PreAuthenticate = true;
                                affi.Credentials = System.Net.CredentialCache.DefaultCredentials;
                            }
                        }
                        catch
                        {
                            throw new ApplicationException("Please configure a valid 'WebServiceDefaultCredentials' in Appsettings");
                        }
                    }
                    else
                        throw new ApplicationException("Please configure 'WebServiceDefaultCredentials' in Appsettings");
                    if (summInfo != null)
                    {
                        uxWsResponding.Text = "OK";
                        if (summInfo.IsTestDatabase)
                            uxWsResponding.Text += " (Test Database)";
                        uxCompletedRegForms.Text = summInfo.CompletedRegistrationForms.ToString();
                        uxCompletedAppForms.Text = summInfo.CompletedApplicationForms.ToString();
                        if (summInfo.LastRegistrationFormImport == DateTime.MinValue)
                            uxLastRegFormImport.Text = "(no imports yet)";
                        else
                            uxLastRegFormImport.Text = summInfo.LastRegistrationFormImport.ToString("dd/MMM/yyyy HH:mm:ss");
                        if (summInfo.LastApplicationFormImport == DateTime.MinValue)
                            uxLastAppFormsImport.Text = "(no imports yet)";
                        else
                            uxLastAppFormsImport.Text = summInfo.LastApplicationFormImport.ToString("dd/MMM/yyyy HH:mm:ss");

                        // live vs test test
                        BnbConfigurationInfo config = BnbEngine.SessionManager.GetConfigurationInfo();
                        if (config.IsTestDatabase && !summInfo.IsTestDatabase)
                            uxLiveModeWarning.Text = "Warning: Frond is running on a Test database but seems to be connecting to the Live Online Applications database. Be careful!";
                        if (!config.IsTestDatabase && summInfo.IsTestDatabase)
                            uxLiveModeWarning.Text = "Warning: Live Frond seems to be connecting to a Test version of the Online Applications Database. Be careful!";

                        if (config.IsTestDatabase)
                            uxTestUtilitiesPanel.Visible = true;

                        // only show batch link is more than batch size waiting
                        if (summInfo.CompletedRegistrationForms > BnbFormsProcessUtils.CONST_FetchBatchSize)
                        {
                            uxFetchRegFormsSmallerBatchLink.Text = String.Format("Fetch {0} Reg Forms", BnbFormsProcessUtils.CONST_FetchBatchSize);
                            if (!(HasPermissionToViewAllRegOrAppForms()))
                                uxFetchRegFormsSmallerBatchLink.Text += " for " + BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBase;
                            uxFetchRegFormsSmallerBatchLink.Visible = true;
                        }

                        DisplayTextForNullRecBase();
                    }
                }
                catch (Exception err)
                {
                    int ticketID = BnbWebFormManager.LogErrorToDatabase(err);
                    string errorExplanation = err.Message;
                    // do some error translating
                    if (err is System.Net.WebException)
                    {
                        if (err.Message.StartsWith("The operation has timed out"))
                            errorExplanation = "Call to the Online Apps Server timed out.";
                        if (err.Message.StartsWith("Unable to connect to the remote server") ||
                            err.Message.StartsWith("The request failed with HTTP status 404"))
                            errorExplanation = "Frond has lost contact with the Online Apps server.";
                    }
                    if (err is System.Web.Services.Protocols.SoapException)
                    {
                        if (err.Message.IndexOf("The server is not operational.") > -1
                            && err.Message.IndexOf("at System.DirectoryServices.DirectoryEntry.Bind") > -1)
                            errorExplanation = "Frond could not log into the Online Apps server because the External Active Directory server is not responding.";
                    }
                    response = String.Format("Status: Error: {0} (TicketID {1})",
                        errorExplanation, ticketID.ToString());



                    uxWsResponding.Text = response;
                    uxLiveModeWarning.Text = "Error connecting to remote server. <a href=\"./EGatewayUtil.aspx\">Click Here</a> to try again.";
                }

            }

            uxFetchRegFormsLink.Attributes.Add("onclick", "javascript:showLoadingMessage('Fetching Forms Data and Matching against database ...');");
            uxFetchRegFormsSmallerBatchLink.Attributes.Add("onclick", "javascript:showLoadingMessage('Fetching Forms Data and Matching against database ...');");
            uxFetchAppFormsLink.Attributes.Add("onclick", "javascript:showLoadingMessage('Fetching Forms Data ...');");
        }

        protected void uxFetchRegFormsLink_Click(object sender, EventArgs e)
        {
            SetRecBaseIDInSessionForRegForms();
            Response.Redirect("ImportRegistrationForm.aspx");
        }

        protected void uxEmailTest_Click(object sender, EventArgs e)
        {
        }

        protected void uxFetchAppFormsLink_Click(object sender, EventArgs e)
        {
            SetRecBaseIDInSessionForAppForms();
            Response.Redirect("ImportApplicationForm.aspx");
        }

        protected void uxFetchRegFormsSmallerBatchLink_Click(object sender, EventArgs e)
        {
            SetRecBaseIDInSessionForRegForms();
            Response.Redirect(String.Format("ImportRegistrationForm.aspx?BatchSize={0}",
                BnbFormsProcessUtils.CONST_FetchBatchSize));
        }

        #region " HasPermissionToViewAllRegOrAppForms "
        /// <summary>
        /// Check permisssion whether the user is able to view all Reg/App forms
        /// </summary>
        private bool HasPermissionToViewAllRegOrAppForms()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_ViewAllRegOrAppForms))
                return true;
            else
                return false;

        }

        #endregion

        #region " HasRecruitmentBase "
        /// <summary>
        /// Check permisssion whether the user is able to view all Reg/App forms
        /// </summary>
        private bool HasRecruitmentBase()
        {
            return BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID != -1;
        }

        #endregion

        #region " FillRecruitmentBase "

        /// <summary>
        /// KeyPress event is not working in Standalonelookup control. So, used dropdownlist web control
        /// </summary>
        private void FillRecruitmentBase()
        {
            // mapping template lookup 
            DataTable recruitmentBaseTable = BnbEngine.LookupManager.GetLookup("vlkuRecruitmentBase");
            recruitmentBaseTable.DefaultView.RowFilter = "Exclude = 0";
            recruitmentBaseTable.DefaultView.Sort = "Description";
            cmbRecruitmentBaseReg.DataSource = recruitmentBaseTable.DefaultView;
            cmbRecruitmentBaseApp.DataSource = recruitmentBaseTable.DefaultView;

            cmbRecruitmentBaseReg.DataTextField = "Description";
            cmbRecruitmentBaseApp.DataTextField = "Description";

            cmbRecruitmentBaseReg.DataValueField = "IntID";
            cmbRecruitmentBaseApp.DataValueField = "IntID";

            cmbRecruitmentBaseReg.DataBind();
            cmbRecruitmentBaseApp.DataBind();
            // create empty item - To set as default selected
            ListItem blankItem = new ListItem("All", "All");
            cmbRecruitmentBaseReg.Items.Insert(0, blankItem);
            cmbRecruitmentBaseApp.Items.Insert(0, blankItem);

        }

        #endregion

        #region " CheckAndDispalyRegAndAppFormdetails "

        private void CheckAndDispalyRegAndAppFormdetails()
        {
            if (HasPermissionToViewAllRegOrAppForms())
            {
                trRecruitmentBaseForReg.Visible = true;
                trRecruitmentBaseForApp.Visible = true;
                FillRecruitmentBase();
            }
            else
            {
                trRecruitmentBaseForReg.Visible = false;
                trRecruitmentBaseForApp.Visible = false;

                if (HasRecruitmentBase())
                {
                    uxFetchRegFormsLink.Visible = true;
                    uxFetchAppFormsLink.Visible = true;
                    uxFetchRegFormsLink.Text = "Fetch Reg Forms for " + BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBase;
                    uxFetchAppFormsLink.Text = "Fetch App Forms for " + BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBase;

                }
                else
                {
                    uxFetchRegFormsLink.Visible = false;
                    uxFetchAppFormsLink.Visible = false;
                }
            }

        }

        #endregion

        #region " DisplayTextForNullRecBase "

        /// <summary>
        /// If the user recruitment base is Null then the text "[Data not available]" will
        /// be shown wherever necessary.
        /// </summary>
        private void DisplayTextForNullRecBase()
        {
            if (!(HasRecruitmentBase()))
            {
                uxCompletedRegForms.Text = "[Data not available]";
                uxCompletedAppForms.Text = "[Data not available]";
                uxLastRegFormImport.Text = "[Data not available]";
                uxLastAppFormsImport.Text = "[Data not available]";
            }
        }
        #endregion

        #region " SetRecBaseIDInSessionForRegForms "

        private void SetRecBaseIDInSessionForRegForms()
        {
            if (HasPermissionToViewAllRegOrAppForms())
                Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID] = cmbRecruitmentBaseReg.SelectedValue.ToString();
            else
                Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID] = BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID.ToString();

        }
        #endregion

        #region " SetRecBaseIDInSessionForAppForms "

        private void SetRecBaseIDInSessionForAppForms()
        {
            if (HasPermissionToViewAllRegOrAppForms())
                Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID] = cmbRecruitmentBaseApp.SelectedValue.ToString();
            else
                Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID] = BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID.ToString();

        }
        #endregion

    }
}
