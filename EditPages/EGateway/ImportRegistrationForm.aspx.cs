using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using BonoboEngine.Query;
using BonoboDomainObjects;
using BonoboEngine;

namespace Frond.EditPages.EGateway
{
    public partial class ImportRegistrationForm : System.Web.UI.Page
    {
        EGatewayUtil eg;
        protected void Page_Load(object sender, EventArgs e)
        {
            BnbEngine.SessionManager.ClearObjectCache();
            if (!this.IsPostBack)
            {
                titleBar.TitleText = "Import Registration Forms";
                BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
                int batchSize = 0;
                if (this.Request.QueryString["BatchSize"] != null)
                    batchSize = int.Parse(this.Request.QueryString["BatchSize"]);
                this.PopulateRegistrationGrid(false, batchSize);
                this.PopulateImportHistoryGrid();
                this.ShowEmailCopyAddress();
                this.DisplayEmailTestMode();
            }
            else
            {
                uxRegSmartGrid.UpdateDataTableFromControls();
            }
            uxBulkimportFeedback.Text = "";
            uxRefreshData.Attributes.Add("onclick", "javascript:showLoadingMessage('Re-fetching Forms Data ...');");
            uxBulkImportButton.Attributes.Add("onclick", "javascript:showLoadingMessage('Bulk Importing Data ...');");
        }

        /// <summary>
        /// displays or toggles mode of the Test Mode Email panel
        /// </summary>
        private void DisplayEmailTestMode()
        {
            if (BnbFormsProcessUtils.TestModeEmail)
            {
                uxTestModeEmailPanel.Visible = true;
                uxTestModeEmailToLabel.Text = BnbFormsProcessUtils.TestModeEmailTo;
                uxTestModeEmailOn.Visible = BnbFormsProcessUtils.TestModeEmailEnabled;
                uxTestModeEmailOff.Visible = !BnbFormsProcessUtils.TestModeEmailEnabled;
            }
        }

        private void ShowEmailCopyAddress()
        {
            DataSet ds = BnbFormsProcessUtils.GetImportFormsSettings();
            DataTable generalSettings = ds.Tables["GeneralSettings"];
            DataRow row = generalSettings.Rows[0];
            bool blindCopyToCurrentUser = Convert.ToBoolean(generalSettings.Rows[0]["BlindCopyToCurrentUser"].ToString());
            //string blindCopyToFixedAddress = generalSettings.Rows[0]["BlindCopyToFixedAddress"].ToString();
            
             //TPT : Code to Display BCC address based on recruitment Base in UI - Support Call Dated 10/05/2011
            string recBaseID = Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID].ToString();

            string blindCopyToFixedAddress = null;
            if (recBaseID.ToUpper() != "ALL")
            {
                string recBaseName = string.Empty;
                if (recBaseID == "1000")
                    recBaseName = "VSOUK";
                else if (recBaseID == "1009")
                    recBaseName = "VSOIreland";
                else if (recBaseID == "1004")
                    recBaseName = "VSOJitolee";
                else if (recBaseID == "1005")
                    recBaseName = "VSOBahaginan";
                else if (recBaseID == "1003")
                    recBaseName = "VSONetherland";

                blindCopyToFixedAddress = generalSettings.Rows[0]["OverrideBCCAddress" + recBaseName].ToString();

                if (blindCopyToCurrentUser)
                    uxEmailCopyLabel.Text = String.Format("(Emails will be Bcc'd to your address {0})", BnbEngine.SessionManager.GetSessionInfo().EmailAddress);
                if (blindCopyToFixedAddress != null && blindCopyToFixedAddress != "")
                    uxEmailCopyLabel.Text = String.Format("(Emails will be Bcc'd to fixed address {0})", blindCopyToFixedAddress);
            }
            else
            {
                uxEmailCopyLabel.Text = "(Emails will be Bcc'd to fixed address of the corresponding Recruitment Base)" + this.WriteBCCAddress(row );
            }

        }
        private string WriteBCCAddress(DataRow row)
        {
            StringWriter sw = new StringWriter();
            HtmlTextWriter writer = new HtmlTextWriter(sw);
            writer.WriteBreak(); writer.WriteBreak();
            writer.AddAttribute("border", "1");
            writer.AddAttribute("cellpadding", "3");
            writer.AddAttribute("cellspacing", "3");
            writer.RenderBeginTag("table");
            writer.RenderBeginTag("tr");
            writer.RenderBeginTag("td");
            writer.Write("VSOUK");
            writer.RenderEndTag();
            writer.RenderBeginTag("td");            
            writer.Write(row ["OverrideBCCAddressVSOUK"].ToString ());
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderBeginTag("tr");
            writer.RenderBeginTag("td");
            writer.Write("VSOIreland");
            writer.RenderEndTag();
            writer.RenderBeginTag("td");
            writer.Write(row["OverrideBCCAddressVSOIreland"].ToString());
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderBeginTag("tr");
            writer.RenderBeginTag("td");
            writer.Write("VSOJitolee");
            writer.RenderEndTag();
            writer.RenderBeginTag("td");
            writer.Write(row["OverrideBCCAddressVSOJitolee"].ToString());
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderBeginTag("tr");
            writer.RenderBeginTag("td");
            writer.Write("VSOBahaginan");
            writer.RenderEndTag();
            writer.RenderBeginTag("td");
            writer.Write(row["OverrideBCCAddressVSOBahaginan"].ToString());
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderBeginTag("tr");
            writer.RenderBeginTag("td");
            writer.Write("VSONetherland");
            writer.RenderEndTag();
            writer.RenderBeginTag("td");
            writer.Write(row["OverrideBCCAddressVSONetherland"].ToString());
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            return sw.ToString();

        }


        private void PopulateImportHistoryGrid()
        {
            DataView dv = BnbFormsProcessUtils.ImportRegistrationFormsHistoryDataView;
            if (dv.Count == 0)
                uxImportHistoryGrid.Visible = false;
            else
            {
                uxImportHistoryGrid.DataSource = dv;
                uxImportHistoryGrid.DataBind();
                uxImportHistoryGrid.Visible = true;

            }
        }


        public BnbFormsProcessUtils.BnbCachedDataSet GetRegistrationData(bool forceRefresh, int batchSize)
        {
            BnbFormsProcessUtils.BnbCachedDataSet cachedData = BnbFormsProcessUtils.StoredRegistrationFormData;
            if (cachedData != null && !forceRefresh)
                return cachedData;

            // otherwise, get it from web service

            WsOnlineForms.FrondIntegration affi = new WsOnlineForms.FrondIntegration();
            if (System.Configuration.ConfigurationManager.AppSettings["WebServiceDefaultCredentials"] != null)
            {
                try
                {
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["WebServiceDefaultCredentials"]) == true)
                    {
                        affi.PreAuthenticate = true;
                        affi.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    }
                }
                catch
                {
                    throw new ApplicationException("Please configure a valid 'WebServiceDefaultCredentials' in Appsettings");
                }
            }
            else
                throw new ApplicationException("Please configure 'WebServiceDefaultCredentials' in Appsettings");
            string castanetUsername = ConfigurationSettings.AppSettings["CastanetAdminUsername"].ToString();
            string castanetPassword = ConfigurationSettings.AppSettings["CastanetAdminPassword"].ToString();

            string recBaseID = Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID].ToString();

            DataSet regDataSet = affi.GetCompletedRegistrations(castanetUsername, castanetPassword, batchSize, recBaseID);
            //TPT Ammended:Jira call VAF-1058:Exclude the columns as per the Recruitement base while fetching the Registration form 
            if (recBaseID.ToUpper() != "ALL")
            {
                string columns = GetExcludeColumnsFromXml();
                string[] columnsList = columns.Split(',');

                for (int i = 0; i < regDataSet.Tables[0].Rows.Count; i++)
                {
                    string columnText = regDataSet.Tables[0].Rows[i]["ColumnName"].ToString();
                    for (int j = 0; j < columnsList.Length; j++)
                    {
                        // trim spaces, carriage returns and tabs from start and end
                        string columnsListValue = columnsList[j].Trim(new char[] { (char)13, (char)10, (char)32, (char)9 });
                        if (columnText == columnsListValue)
                        {
                            regDataSet.Tables[0].Rows[i].Delete();
                        }
                    }
                }
                regDataSet.AcceptChanges();
                regDataSet.Tables[0].AcceptChanges();
            }
            // convert it to a BnbQueryDataSet
            BnbQueryDataSet qds = BnbQueryUtils.ConvertToBnbQueryDataSet(regDataSet);

            // add new columns
            qds.Tables["results"].Columns.Add("Extra_Import", typeof(string));
            BnbQueryUtils.AddColumnToBnbQueryDataSetColumnInfo(qds.ColumnInfo,
                "Extra_Import", "Import", 1);

            qds.Tables["results"].Columns.Add("Extra_ImportResults", typeof(string));
            BnbQueryUtils.AddColumnToBnbQueryDataSetColumnInfo(qds.ColumnInfo,
                "Extra_ImportResults", "Import Results", 1);

            qds.Tables["results"].Columns.Add("Extra_DecisionID", typeof(int));
            BnbQueryUtils.AddColumnToBnbQueryDataSetColumnInfo(qds.ColumnInfo,
                "Extra_DecisionID", "Choose", 3);

            qds.Tables["results"].Columns.Add("Extra_SkillID", typeof(int));
            BnbQueryUtils.AddColumnToBnbQueryDataSetColumnInfo(qds.ColumnInfo,
                "Extra_SkillID", "Skill", 3);

            // for dup search results
            qds.Tables["results"].Columns.Add("Extra_DuplicateCheck", typeof(string));
            BnbQueryUtils.AddColumnToBnbQueryDataSetColumnInfo(qds.ColumnInfo,
                "Extra_DuplicateCheck", "Matching", 1);

            qds.Tables["results"].Columns.Add("Extra_MatchIndividualID", typeof(Guid));
            BnbQueryUtils.AddColumnToBnbQueryDataSetColumnInfo(qds.ColumnInfo,
                "Extra_MatchIndividualID", "MatchIndividualID", 4);

            // turn match ID field into a foreign key column
            BnbQueryUtils.SetBnbQueryDataSetForeignKeyInfo(qds, "Extra_MatchIndividualID", "tblIndividualKeyInfo");


            // do dup check
            // possiblity of skipping dup check not used at present, but should work ok
            // (dup check gets done at import time if not done now)
            if (this.Request.QueryString["NoDupCheck"] != "1")
            {
                foreach (DataRow rowLoop in qds.Tables["results"].Rows)
                {
                    this.ProcessRegistrationRowForDuplicates(rowLoop, qds);
                }
            }

            // default lookup columns
            foreach (DataRow rowLoop in qds.Tables["results"].Rows)
            {
                rowLoop["Extra_DecisionID"] = -1;
                rowLoop["Extra_SkillID"] = -1;
            }

            // cache it for later use
            BnbFormsProcessUtils.SetStoredRegistrationFormData(qds, batchSize);

            return BnbFormsProcessUtils.StoredRegistrationFormData;
        }

        //TPT Ammended:Jira call VAF-1058:Exclude the columns from xml file as per the Recruitement base
        public string GetExcludeColumnsFromXml()
        {
            string excludeColumns = "";
            string recruitBaseID = Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID].ToString();
            DataSet dataset = new DataSet();
            string path1 = AppDomain.CurrentDomain.BaseDirectory + @"/EditPages/EGateway/ImportFormExcludeColumns.xml";
            dataset.ReadXml(path1, XmlReadMode.InferSchema);
            DataTable dataTable = dataset.Tables["ExcludeColumns"];
            DataRow[] rows = dataTable.Select("RecBaseID=" + recruitBaseID);
            try
            {
                excludeColumns = rows[0]["ExcludeColumnNames"].ToString();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("This Recruitment Base mapping is missing for 'Exclude Columns'");
            }
            return excludeColumns;
        }

        public void PopulateRegistrationGrid(bool forceRefresh, int batchSize)
        {
            BnbFormsProcessUtils.BnbCachedDataSet cachedData = this.GetRegistrationData(forceRefresh, batchSize);
            BnbQueryDataSet regDataSet = cachedData.QueryDataSet;

            //set the views for Decision based on the current Recruitment Base id
            string viewDecision = "";
            string recruitmentBaseID = Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID].ToString();
            DataSet dset = BnbFormsProcessUtils.GetImportFormsSettingsViewDecisions();
            DataTable recruitmentBaseDetails = dset.Tables["Decision"];
            DataRow[] rows = recruitmentBaseDetails.Select("RecBaseID ='" + recruitmentBaseID + "'");
            try
            {
                viewDecision = rows[0]["ViewName"].ToString();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("This Recruitment Base mapping is missing for 'Decisions'");
            }
            
            // show data fetched time
            uxDataFetchedTime.Text = cachedData.FetchedDate.ToString("HH:mm:ss");

            if (cachedData.BatchSize > 0)
            {
                uxRefreshData.Text = String.Format("Refresh (next {0})", cachedData.BatchSize);
                uxRefreshData.CommandArgument = cachedData.BatchSize.ToString();
            }

            uxRegSmartGrid.ClearSettings();
           
            // set column controls
            uxRegSmartGrid.AddControlColumnHyperlinkButton("tblApplicationForm_ApplicationFormID", "View");
            uxRegSmartGrid.AddControlColumnButton("Extra_Import", "Import", "Imported, Failed", "javascript:showLoadingMessage('Importing Row ...');");
            //uxRegSmartGrid.AddControlColumnLookup("Extra_DecisionID", "vlkuBonoboRegistrationFormDecision", false, true);
            uxRegSmartGrid.AddControlColumnLookup("Extra_DecisionID", viewDecision, false, true);
            uxRegSmartGrid.AddControlColumnLookup("Extra_SkillID", "vlkuBonoboSkillWithGroupAbbrev", true, true);
            uxRegSmartGrid.QueryDataSet = regDataSet;
            uxRegSmartGrid.ColumnDisplayList = this.GetColumnDisplayList();
             if (Session["EGateway_ImportRegForm_SortColumn"] != null)
                uxRegSmartGrid.SortColumn = Session["EGateway_ImportRegForm_SortColumn"].ToString();
            if (Session["EGateway_ImportRegForm_SortDescending"] != null)
                uxRegSmartGrid.SortDescending = (bool)Session["EGateway_ImportRegForm_SortDescending"];
            uxRegSmartGrid.Visible = true;
        }

        private string GetDefaultColumnDisplayList()
        {
            DataSet ds = BnbFormsProcessUtils.GetImportFormsSettings(); 
            DataTable generalSettings = ds.Tables["GeneralSettings"];
            DataRow settingRow = generalSettings.Rows[0];
            return settingRow["ImportRegFormColumns"].ToString();
        }

        private string GetColumnDisplayList()
        {
            if (Session["EGateway_ImportRegForm_ColumnDisplayList"] != null)
                return Session["EGateway_ImportRegForm_ColumnDisplayList"].ToString();
            else
                return this.GetDefaultColumnDisplayList();
        }

        private void ProcessRegistrationRowForDuplicates(DataRow rowCheck, BnbQueryDataSet regDataSet)
        {

            string email = rowCheck["tblRegistrationPage1_Email"].ToString();
            string surname = rowCheck["tblRegistrationPage1_Surname"].ToString();
            string forename = rowCheck["tblRegistrationPage1_FirstName"].ToString();
            BnbQueryDataSet matchResultsSet = null;
            try
            {
                matchResultsSet = BnbFormsProcessUtils.CheckForDuplicates(email, surname, forename);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                if (ex.Message.StartsWith("Timeout expired"))
                {
                    rowCheck["Extra_DuplicateCheck"] = "(Timed Out)";
                    return;
                }
                throw ex;
            }
            DataTable matchResults = matchResultsSet.Tables["results"];
            // if one result and email matches then use it
            bool emailMatchFound = false;
            if (matchResults.Rows.Count == 1)
            {
                if (matchResults.Rows[0]["Custom_Email"].ToString().Equals(email))
                {
                    rowCheck["Extra_MatchIndividualID"] = matchResults.Rows[0]["tblIndividualKeyInfo_IndividualID"];
                    emailMatchFound = true;
                }
            }
            // fill in match result
            if (matchResults.Rows.Count == 0)
                rowCheck["Extra_DuplicateCheck"] = "No Matches";
            else
            {
                if (emailMatchFound)
                    rowCheck["Extra_DuplicateCheck"] = "Email Match";
                else
                    rowCheck["Extra_DuplicateCheck"] = String.Format("{0} Matches", matchResults.Rows.Count);
            }
        }



        protected void uxRegSmartGrid_ColumnButtonClicked(object sender, EventArgs e, string columnName, string rowID)
        {
            if (columnName == "Extra_Import")
            {
                // find row
                DataTable regTable = uxRegSmartGrid.QueryDataSet.Tables["results"];
                DataRow[] findRows = regTable.Select(String.Format("tblApplicationForm_ApplicationFormID = '{0}'", rowID));
                if (findRows.Length == 1)
                {
                    // check an option is specified
                    if (findRows[0]["Extra_DecisionID"].ToString() != "-1"
                        && findRows[0]["Extra_SkillID"].ToString() != "-1")
                        this.ImportRegistrationRow(findRows[0], uxRegSmartGrid.QueryDataSet);
                    else
                        findRows[0]["Extra_Import"] = "Choose options";
                    
                }

            }
            if (columnName == "tblApplicationForm_ApplicationFormID")
            {
                // use rowID to re-direct to viewformdetail
                Response.Redirect(String.Format("ViewFormDetail.aspx?BnbApplicationForm={0}&DataSet=RegForm", rowID));
            }
        }

        



        public void ImportRegistrationRow(DataRow importRow, BnbQueryDataSet qds)
        {
            // check dup match
            string dupCheck = (string)BnbQueryUtils.ReplaceDBNull(importRow["Extra_DuplicateCheck"], null);
            // if not done yet, then do it
            if (dupCheck == null || dupCheck == "")
            {
                this.ProcessRegistrationRowForDuplicates(importRow, qds);
                dupCheck = (string)importRow["Extra_DuplicateCheck"];
            }

            // what is the match?
            bool noMatches = false;
            bool emailMatch = false;
            Guid matchIndividualID = Guid.Empty;
            if (dupCheck == "No Matches")
                noMatches = true;
            else if (dupCheck == "Email Match")
            {
                emailMatch = true;
                matchIndividualID = (Guid)importRow["Extra_MatchIndividualID"];
            }





            // if no match, or an email match, do the import without user interaction
            // VAF-745 we were asked to proceed the user to the wizard even when there is a
            // email address match.
            if (noMatches)
            {
                BnbFormsProcessUtils.ImportRegistrationFormProcess(matchIndividualID, importRow, qds);
                this.PopulateImportHistoryGrid();
            }
            else
            {
                // otherwise, re-direct to wizard
                Response.Redirect(String.Format("WizardImportRegistration.aspx?BnbApplicationForm={0}", importRow["tblApplicationForm_ApplicationFormID"].ToString()));

            }

        }

        protected void uxExcelExport_Click(object sender, EventArgs e)
        {
            uxRegSmartGrid.DownloadExcelExport(uxExportAllColumnsCheckbox.Checked);
        }



        protected void uxTestModeEmailEnable_Click(object sender, EventArgs e)
        {
            BnbFormsProcessUtils.TestModeEmailEnabled = true;
            this.DisplayEmailTestMode();
        }

        protected void uxDisableTestEmailModeButton_Click(object sender, EventArgs e)
        {
            BnbFormsProcessUtils.TestModeEmailEnabled = false;
            this.DisplayEmailTestMode();
        }

        protected void uxRefreshData_Click(object sender, EventArgs e)
        {
            int batchSize = 0;
            Button senderButton = (Button)sender;
            if (senderButton.CommandArgument != null && senderButton.CommandArgument != "")
                batchSize = int.Parse(senderButton.CommandArgument);

            this.PopulateRegistrationGrid(true, batchSize);
        }

        protected void uxBulkImportButton_Click(object sender, EventArgs e)
        {
            this.BulkImport();
        }

        public void BulkImport()
        {
            BonoboWebControls.BnbWebFormManager.LogAction("Starting Bulk Import...", null, this);
            ArrayList progressMessages = new ArrayList();
            try
            {
                InternalBulkImport(progressMessages);
            }
            catch (Exception e)
            {
                int errorTicketID = BonoboWebControls.BnbWebFormManager.LogErrorToDatabase(e);
                progressMessages.Add(String.Format("An error occured: {0} (ErrorTicketID {1})",
                    e.Message, errorTicketID));
            }
            string[] progressMessagesArray = new string[progressMessages.Count];
            progressMessages.CopyTo(progressMessagesArray);
            uxBulkimportFeedback.Text = String.Join("<br/>", progressMessagesArray);
            BonoboWebControls.BnbWebFormManager.LogAction("Finished Bulk Import.", null, this);
        }

        private void InternalBulkImport(ArrayList progressMessages)
        {
            progressMessages.Add("Starting Bulk Import ...");
            
            BnbQueryDataSet qds = uxRegSmartGrid.QueryDataSet;
            int totalRows = qds.Tables["results"].Rows.Count;
            int blankCount = 0;
            int importableCount = 0;
            int duplicatesCount = 0;
            int alreadyImportedCount = 0;
            ArrayList importableRows = new ArrayList();
            foreach (DataRow rowLoop in qds.Tables["results"].Rows)
            {
                string importedInfo = (string)BnbQueryUtils.ReplaceDBNull(rowLoop["Extra_Import"], null);
                string matchInfo = (string)BnbQueryUtils.ReplaceDBNull(rowLoop["Extra_DuplicateCheck"], null);
                if (importedInfo != null && importedInfo != "")
                    alreadyImportedCount += 1;
                else if (matchInfo == null || matchInfo == "")
                    blankCount += 1;
                else if (matchInfo == "No Matches" || matchInfo == "Email Match")
                {
                    importableCount += 1;
                    importableRows.Add(rowLoop);
                }
                else
                    duplicatesCount += 1;
            }
            if (blankCount == totalRows)
            {
                progressMessages.Add("Matching information is blank so cannot Bulk Import.");
                return;
            }
            progressMessages.Add(String.Format("Found {0} importable rows, {1} rows with possible duplicates",
                importableCount, duplicatesCount));
            if (blankCount > 0)
                progressMessages.Add(String.Format("Found {0} rows with blank matching information",
                blankCount));
            if (importableCount > 0)
            {
                progressMessages.Add(String.Format("Starting bulk import of {0} rows...", importableCount));
                foreach (DataRow importRow in importableRows)
                {
                    string dupCheck = (string)importRow["Extra_DuplicateCheck"];
                    Guid matchIndividualID = Guid.Empty;
                    if (dupCheck == "Email Match")
                    {
                        matchIndividualID = (Guid)importRow["Extra_MatchIndividualID"];
                    }
                    BnbFormsProcessUtils.ImportRegistrationFormProcess(matchIndividualID, importRow, qds);
                }
                this.PopulateImportHistoryGrid();
                progressMessages.Add(String.Format("Finished. See results below."));
            }
        }

        protected void uxRegSmartGrid_OptionsChanged(object sender, EventArgs e)
        {
            Session["EGateway_ImportRegForm_ColumnDisplayList"] = uxRegSmartGrid.ColumnDisplayList;
            Session["EGateway_ImportRegForm_SortColumn"] = uxRegSmartGrid.SortColumn;
            Session["EGateway_ImportRegForm_SortDescending"] = uxRegSmartGrid.SortDescending;
        }

        protected void uxRegSmartGrid_ExportClicked(object sender, BonoboWebControls.DataGrids.BnbSmartGrid.BnbSmartGridExportEventArgs e)
        {
            // strip the lookup columns out of the exportTable
            if (e.ExportData.Columns.Contains("Extra_DecisionID"))
                e.ExportData.Columns.Remove("Extra_DecisionID");

            // try a different suggested filename
            e.SuggestedFilename = String.Format("regcompleted-{0}.xls", DateTime.Now.ToString("ddMMMyyyy-HHmmss"));
            // add some header lines
            e.HeaderLines.Add("Online Applications - Completed Registration Forms");
            e.HeaderLines.Add(String.Format("Exported at: {0}", DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss")));

        }

        protected void uxImportHistoryGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ViewLog")
            {
                string appFormID = e.CommandArgument.ToString();
                Response.Redirect(String.Format("ViewEGatewayLog.aspx?BnbApplicationForm={0}&DataSet=RegForm", appFormID));
            }
        }

        
    }
}
