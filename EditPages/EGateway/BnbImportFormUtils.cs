using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;


namespace Frond.EditPages.EGateway
{
    /// <summary>
    /// static methods for importing forms data into main database
    /// </summary>
    public class BnbImportFormUtils
    {

        public const string SESSION_EGATEWAYRECBASEID = "EGATEWAYRECBASEID";

        public static BnbImportFormResults SaveRegistrant(Guid individualID,
            string title,
            string firstname,
            string surname,
            string address1,
            string address2,
            string address3,
            string postaltown,
            string county,
            string postcode,
            string countryname,
            string daytimephone,
            string emailaddress,
            string fax,
            DateTime dob,
            string nationality,
            DateTime availableFrom,
            string sex,
            string ethnicity,
            string ethnicityOther,
            bool disability,
            bool contactVolunteeringOptions,
            bool contactByEmail,
            int enquirerStatusID,
            int actionPackID,
            Guid sourceID,
            int vsoTypeID,
            int skillID,
            string comments,
            //bool pension,
            string regHeardFromVsoOther)
        {
            BnbEditManager em = new BnbEditManager();

            // either load individual or create new one
            BnbIndividual indv = GetIndividual(em, individualID, title, firstname, surname, dob, sex);

            //additional
            UpdateIndividualAdditional(em, indv, ethnicity, ethnicityOther, disability, comments, false);


            SetNationalityToIndividual(em, indv, nationality);

            // address bits including telephonenumber
            SetAddressToIndividual(em, indv, BnbConst.AddressType_PermanentHome, address1, address2, address3, postaltown, county, postcode, countryname, daytimephone, fax);

            // add email if specified and not already one recorded
            SetContactNumberToIndividual(em, indv, BnbConst.ContactNumber_Email, emailaddress);

            // enquiry rec
            BnbApplication newEnq = CreateApplicationAfterRegistration(em, indv, BnbConst.ApplicationSource_OnlineEnquiry, enquirerStatusID, availableFrom, vsoTypeID, regHeardFromVsoOther);
            // source
            SetSourceOfApplication(em, newEnq, sourceID);


            //recruitmentbase
            SetRecruitmentBase(em, newEnq, countryname);

            if (skillID > 0)
                SetSkill(em, indv, skillID);
            // actions
            // TPT - commented. As requested in the call APPj-19
            //if (actionPackID != -1)
            //    CreateActionPack(em, indv, newEnq, actionPackID);
            // contact block selected contacts
            if (contactVolunteeringOptions)
                ApplySelectedContact(em, indv, 1019);
            //TPT Ammended:Jira call VAF-1056            
            else
                ApplySelectedContact(em, indv, 1012);
            if (contactByEmail)
                ApplySelectedContact(em, indv, 1023);
            else
                ApplySelectedContact(em, indv, 1024);

            BnbImportFormResults impResults = CommitAndGetResults(em, newEnq);
            return impResults;

        }
        /// <summary>
        /// Adds the right recruitment base id to correspond to the correct mapping in
        /// lkuRecruitmentArea
        /// </summary>
        /// <param name="em"></param>
        /// <param name="app"></param>
        /// <param name="countryname"></param>
        private static void SetRecruitmentBase(BnbEditManager em, BnbApplication app, string countryName)
        {
            ////work out the right countryid
            //Guid convertedCountryID = GetGuidLookupID(countryName, "lkuCountry");

            //BnbLookupDataTable recruitmentAreaLookup = BnbEngine.LookupManager.GetLookup("vlkuRecruitmentArea");

            //DataRow[] firstSearch = recruitmentAreaLookup.Select("CountryID =" + "'" + convertedCountryID + "'");

            //if (firstSearch.Length > 0)
            //    app.RecruitmentBaseID = ((int)Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID]);
            //else
            //    app.RecruitmentBaseID = 1000;
            //TPT commented- Added rec base id, based on which recruitment base the form has been imported from.
            //As requested in the call APPJ-18, APPJ-23
            if (HttpContext.Current.Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID] != null && (HttpContext.Current.Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID].ToString().ToUpper() != "ALL"))
            {
                if (Convert.ToInt32(HttpContext.Current.Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID]) != -1)
                {
                    app.RecruitmentBaseID = (Convert.ToInt32(HttpContext.Current.Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID]));
                    app.RecruitedByID = (Convert.ToInt32(HttpContext.Current.Session[BnbImportFormUtils.SESSION_EGATEWAYRECBASEID]));
                }
                else
                {
                    app.RecruitmentBaseID = 1000;
                    app.RecruitedByID = 1000;
                }
            }
            else
            {
                app.RecruitmentBaseID = 1000;
                app.RecruitedByID = 1000;
            }
        }





        /// <summary>
        /// Adds the source specified if it doesnt already exist
        /// </summary>
        /// <param name="em"></param>
        /// <param name="app"></param>
        /// <param name="sourceID"></param>
        private static void SetSourceOfApplication(BnbEditManager em, BnbApplication app, Guid sourceID)
        {
            bool sourceAlreadyExists = false;
            // loop through existing sources
            foreach (BnbApplicationSource sourceLoop in app.ApplicationSources)
            {
                if (sourceLoop.SourceID.Equals(sourceID))
                    sourceAlreadyExists = true;
            }
            if (sourceAlreadyExists)
                return;
            // otherwise, add it
            BnbApplicationSource newSource = new BnbApplicationSource(true);
            newSource.RegisterForEdit(em);
            app.ApplicationSources.Add(newSource);
            newSource.SourceID = sourceID;
        }


        /// <summary>
        /// Adds working languages for the individual
        /// </summary>
        /// <param name="em"></param>
        /// <param name="app"></param>
        /// <param name="sourceID"></param>
        private static void SetWorkingLanguagesForIndividual(BnbEditManager em, BnbApplication app, string language1, int lang1AbilityID, string language2, int lang2AbilityID)
        {
            if (app != null)
            {
                BnbIndividual indv = app.Individual;
                if (indv != null)
                {
                        int language1ID = BnbDomainObject.NullInt;
                        int language2ID = BnbDomainObject.NullInt;

                        if (language1 != null)
                            if (language1.Trim() != string.Empty)
                                language1ID = GetIntLookupID(language1, "lkuLanguage");

                        if (language2 != null)
                            if (language2.Trim() != string.Empty)
                                language2ID = GetIntLookupID(language2, "lkuLanguage");

                        if (language1ID != BnbDomainObject.NullInt && lang1AbilityID != BnbDomainObject.NullInt)
                        {
                            //Add language 1
                            BnbIndividualLanguage indvLang1 = new BnbIndividualLanguage(true);
                            indvLang1.RegisterForEdit(em);
                            indvLang1.LanguageID = language1ID;
                            indvLang1.ComprehensionLevelID = GetMappedLanguageLevelID(lang1AbilityID);
                            indvLang1.OralLevelID = BnbConst.Language_Level_None;
                            indvLang1.WrittenLevelID = BnbConst.Language_Level_None;
                            app.Individual.IndividualLanguages.Add(indvLang1);
                        }

                        if (language2ID != BnbDomainObject.NullInt && language1ID != language2ID && lang2AbilityID != BnbDomainObject.NullInt)
                        {
                            //Add language 2
                            BnbIndividualLanguage indvLang2 = new BnbIndividualLanguage(true);
                            indvLang2.RegisterForEdit(em);
                            indvLang2.LanguageID = language2ID;
                            indvLang2.ComprehensionLevelID = GetMappedLanguageLevelID(lang2AbilityID);
                            indvLang2.OralLevelID = BnbConst.Language_Level_None;
                            indvLang2.WrittenLevelID = BnbConst.Language_Level_None;
                            app.Individual.IndividualLanguages.Add(indvLang2);
                        }                    
                }
            }
        }

        private static void CreateActionPack(BnbEditManager em, BnbIndividual indv, BnbApplication app, int actionPackID)
        {
            BnbContactDetail newPack = new BnbContactDetail(true);
            newPack.RegisterForEdit(em);
            // link pack to application if specified
            if (app != null)
                app.ContactDetails.Add(newPack);
            // otherwise link to indv
            else
                indv.ContactDetails.Add(newPack);
            // fill in pack details
            newPack.ContactDescriptionID = actionPackID;
            newPack.SentViaID = BnbConst.SentVia_MailingHouse;

        }

        /// <summary>
        /// adds the specified contact block type to the specified indvidiual
        /// also adds a new 'Allow Selected Mail' contact block if necessary
        /// </summary>
        /// <param name="em"></param>
        /// <param name="indv"></param>
        /// <param name="newSelectedContactTypeID"></param>
        private static void ApplySelectedContact(BnbEditManager em, BnbIndividual indv, int newSelectedContactTypeID)
        {
            // if found, do nothing
            if (SelectedContactFound(indv, newSelectedContactTypeID))
                return;


            // if no contact block yet, or wrong type, we'll have to add one
            BnbIndividualContactBlock blockToAddTo = null;
            if (indv.CurrentContactBlock == null ||
                 indv.CurrentContactBlock.ContactBlockTypeID != BnbConst.ContactBlockType_AllowSelectedMail)
            {
                BnbIndividualContactBlock newCB = new BnbIndividualContactBlock(true);
                newCB.RegisterForEdit(em);
                indv.IndividualContactBlocks.Add(newCB);
                newCB.Reason = "Added automatically by Online Applications Import";
                newCB.ContactBlockTypeID = BnbConst.ContactBlockType_AllowSelectedMail;
                blockToAddTo = newCB;
            }
            else
            {
                blockToAddTo = indv.CurrentContactBlock;
            }

            // add the item to the right object
            BnbSelectedContact newSelCont = new BnbSelectedContact(true);
            newSelCont.RegisterForEdit(em);
            blockToAddTo.SelectedContacts.Add(newSelCont);
            newSelCont.SelectedContactTypeID = newSelectedContactTypeID;


        }

        /// <summary>
        /// returns true if the specified indv has the specified contact block type
        /// </summary>
        /// <param name="indv"></param>
        /// <param name="lookForSelectedContactTypeID"></param>
        /// <returns></returns>
        private static bool SelectedContactFound(BnbIndividual indv, int lookForSelectedContactTypeID)
        {
            if (indv.CurrentContactBlock == null)
                return false;

            if (indv.CurrentContactBlock.ContactBlockTypeID != BnbConst.ContactBlockType_AllowSelectedMail)
                return false;

            bool found = false;
            foreach (BnbSelectedContact contactLoop in indv.CurrentContactBlock.SelectedContacts)
                if (contactLoop.SelectedContactTypeID == lookForSelectedContactTypeID)
                {
                    found = true;
                    break;
                }

            return found;
        }

        private static void UpdateIndividualAdditional(BnbEditManager em, BnbIndividual indv, string ethnicity, string ethnicityOther, bool disability, string comments, bool pension)
        {
            indv.Additional.RegisterForEdit(em);
            indv.Additional.Comments = comments;
            int ethnicityID = GetIntLookupID(ethnicity, "lkuEthnicity");
            indv.Additional.EthnicityID = ethnicityID;
            if (ethnicityID == BnbConst.Ethnicity_Other)
                indv.Additional.EthnicityOther = ethnicityOther;
            indv.Additional.Disability = disability;
            //VAF-348
            indv.Additional.PublicSectorPensionContribution = pension;
        }

        public static BnbImportFormResults SaveApplicant(Guid individualID,
            Guid applicationID,
            string title,
            string firstname,
            string surname,
            string address1,
            string address2,
            string address3,
            string postaltown,
            string county,
            string postcode,
            string countryname,
            string daytimephone,
            string emailaddress,
            string fax,
            DateTime dob,
            string nationality,
            DateTime availableFrom,
            string sex,
            string ethnicity,
            string ethnicityOther,
            bool disability,
            bool contactVolunteeringOptions,
            bool contactByEmail,
            string maritalStatus,
            DateTime maritalStatusDate,
            int skillID,
            Guid sourceID,
            string comments,
            bool pension,
            string appHeardFromVsoOther, string OtherLanguage1, int OtherLang1AbilityID, string OtherLanguage2, int OtherLang2AbilityID)
        {
            BnbEditManager em = new BnbEditManager();

            // load existing individual and update as necessary
            BnbIndividual indv = GetIndividual(em, individualID, title, firstname, surname, dob, sex);
            //additional
            UpdateIndividualAdditional(em, indv, ethnicity, ethnicityOther, disability, comments, pension);
            SetNationalityToIndividual(em, indv, nationality);
            // address bits including telephonenumber
            SetAddressToIndividual(em, indv, BnbConst.AddressType_PermanentHome, address1, address2, address3, postaltown, county, postcode, countryname, daytimephone, fax);
            // add/update email 
            SetContactNumberToIndividual(em, indv, BnbConst.ContactNumber_Email, emailaddress);
            // update app
            BnbApplication appUpdated = UpdateApplicationAfterApplicationForm(em, indv, applicationID, BnbConst.Status_ApplicantApplicant, availableFrom, appHeardFromVsoOther);

            // source
            SetSourceOfApplication(em, appUpdated, sourceID);

            SetWorkingLanguagesForIndividual(em, appUpdated, OtherLanguage1, OtherLang1AbilityID, OtherLanguage2, OtherLang2AbilityID);
            // marital status
            SetMaritalStatus(em, indv, maritalStatus, maritalStatusDate);

            if (skillID > 0)
                SetSkill(em, indv, skillID);
            // contact block selected contacts
            if (contactVolunteeringOptions)
                ApplySelectedContact(em, indv, 1019);
            //TPT Ammended:Jira Call VAF-1056
            else
                ApplySelectedContact(em, indv, 1012);
            if (contactByEmail)
                ApplySelectedContact(em, indv, 1023);
            else
                ApplySelectedContact(em, indv, 1024);
            BnbImportFormResults impResults = CommitAndGetResults(em, appUpdated);
            return impResults;

        }

        private static void SetMaritalStatus(BnbEditManager em, BnbIndividual indv, string maritalStatus, DateTime maritalStatusDate)
        {
            int maritalStatusConverted = GetIntLookupID(maritalStatus, "lkuMaritalStatus");

            if (maritalStatusConverted == BnbDomainObject.NullInt)
                throw new ApplicationException("Marital status - " + maritalStatus + " is missing in maritalstatus Lookup");
            bool alreadyExists = false;
            if (indv.IndividualMaritalStatuses.Count > 0)
            {
                indv.IndividualMaritalStatuses.Sort("MaritalStatusDate", true);
                BnbIndividualMaritalStatus currentMS = indv.IndividualMaritalStatuses[0] as BnbIndividualMaritalStatus;
                if (currentMS.MaritalStatusID == maritalStatusConverted)
                    alreadyExists = true;
            }
            if (!alreadyExists)
            {
                BnbIndividualMaritalStatus newMS = new BnbIndividualMaritalStatus(true);
                newMS.RegisterForEdit(em);
                newMS.MaritalStatusID = maritalStatusConverted;
                newMS.MaritalStatusDate = maritalStatusDate;
                indv.IndividualMaritalStatuses.Add(newMS);

            }
        }

        private static void SetSkill(BnbEditManager em, BnbIndividual indv, int skillID)
        {
            bool alreadyExists = false;
            bool existingLead = (indv.IndividualSkills.GetLead() != null);
            foreach (BnbIndividualSkill skillLoop in indv.IndividualSkills)
                if (skillLoop.SkillID == skillID)
                    alreadyExists = true;

            if (!alreadyExists)
            {
                BnbIndividualSkill newSkill = new BnbIndividualSkill(true);
                newSkill.RegisterForEdit(em);
                newSkill.SkillID = skillID;
                // set lead if this is first skill
                if (existingLead)
                    newSkill.Order = 2;
                else
                    newSkill.Order = 1;
                indv.IndividualSkills.Add(newSkill);

            }


        }

        private static BnbImportFormResults CommitAndGetResults(BnbEditManager em, BnbApplication appToGetIDs)
        {
            BnbImportFormResults impResults = new BnbImportFormResults();
            try
            {
                em.SaveChanges();
                impResults.Imported = true;
                impResults.ApplicationID = appToGetIDs.ID;
                impResults.IndividualID = appToGetIDs.IndividualID;
                impResults.IndividualRefNo = appToGetIDs.Individual.RefNo;
            }
            catch (BnbProblemException pe)
            {
                em.EditCompleted();
                impResults.Imported = false;
                impResults.Problems = pe.Problems;

            }
            catch (Exception e)
            {
                BonoboWebControls.BnbWebFormManager.LogErrorToDatabase(e);
                impResults.Imported = false;
                impResults.ImportException = e;
                em.EditCompleted();
            }
            return impResults;

        }

        private static BnbIndividual GetIndividual(BnbEditManager em, Guid indID, string title, string firstname, string surname, DateTime dob, string sex)
        {
            BnbIndividual indv = null;
            if (indID == Guid.Empty)
            {
                indv = new BnbIndividual(true);
                indv.RegisterForEdit(em);
            }
            else
            {
                indv = BnbIndividual.Retrieve(indID);
                indv.RegisterForEdit(em);
            }
            indv.Title = ReverseNull(title);
            indv.Forename = ReverseNull(firstname);
            indv.Surname = BnbIndividual.ExtractSurnameRemainder(surname);
            indv.SurnamePrefix = BnbIndividual.ExtractSurnamePrefix(surname);
            if (sex.ToLower().StartsWith("m"))
                indv.Gender = "M";
            if (sex.ToLower().StartsWith("f"))
                indv.Gender = "F";

            indv.DateOfBirth = dob;
            return indv;
        }

        private static void SetNationalityToIndividual(BnbEditManager em, BnbIndividual indv, string nationality)
        {
            bool properNationalitySpecified = (!(nationality.ToLower() == "unknown" || nationality == ""));
            // if there is a default nationality of unknown then remove it
            if (indv.IndividualNationalities.Count == 1)
            {
                BnbIndividualNationality leadnat = indv.IndividualNationalities.GetLead() as BnbIndividualNationality;
                if (leadnat.NationalityID == BnbConst.Nationality_Unknown && properNationalitySpecified)
                {
                    leadnat.RegisterForEdit(em);
                    indv.IndividualNationalities.RemoveAndDelete(leadnat);
                }
            }
            if (nationality == "")
                nationality = "Unknown";
            Guid nationalityID = GetGuidLookupID(nationality, "lkuNationality");
            if (nationalityID == Guid.Empty)
                nationalityID = BnbConst.Nationality_Unknown;
            // check if that nationality already exists
            bool nationalityFound = false;
            foreach (BnbIndividualNationality natLoop in indv.IndividualNationalities)
                if (natLoop.NationalityID == nationalityID)
                    nationalityFound = true;

            if (nationalityFound)
                return;

            bool leadAlreadyExists = (indv.IndividualNationalities.GetLead() != null);

            BnbIndividualNationality nat = new BnbIndividualNationality(true);
            nat.RegisterForEdit(em);
            nat.NationalityID = nationalityID;
            if (leadAlreadyExists)
                nat.NationalityOrder = 2;
            else
                nat.NationalityOrder = 1;
            indv.IndividualNationalities.Add(nat);
        }

        /// <summary>
        /// checks address details against existing current address for indvidual
        /// and adds a new address if necessary
        /// also updates tel and fax numbers
        /// </summary>
        /// <param name="em"></param>
        /// <param name="indv"></param>
        /// <param name="addresstypeid"></param>
        /// <param name="address1"></param>
        /// <param name="address2"></param>
        /// <param name="address3"></param>
        /// <param name="postaltown"></param>
        /// <param name="county"></param>
        /// <param name="postcode"></param>
        /// <param name="countryname"></param>
        /// <param name="telephonenumber"></param>
        /// <param name="faxnumber"></param>
        private static void SetAddressToIndividual(
            BnbEditManager em,
            BnbIndividual indv,
            int addresstypeid,
            string address1,
            string address2,
            string address3,
            string postaltown,
            string county,
            string postcode,
            string countryname,
            string telephonenumber,
            string faxnumber)
        {
            Guid convertedCountryID = GetGuidLookupID(countryname, "lkuCountry");
            bool addNewAddress = true;

            // if country is ROI, make sure County is specified
            if (convertedCountryID.Equals(BnbConst.Country_RepIreland))
            {
                if (county == null || county == "")
                    county = ".";
            }

            // if current address exists, check it
            if (indv.CurrentIndividualAddress != null &&
                indv.CurrentIndividualAddress.Address != null)
            {
                BnbAddress currentAddress = indv.CurrentIndividualAddress.Address;
                // add new address if different
                addNewAddress = currentAddress.AddressIsDifferent(address1, address2, address3, postaltown, county,
                    postcode, convertedCountryID);
            }
            BnbAddress addressForTelephone = null;
            if (addNewAddress)
            {
                BnbIndividualAddress newAddressLink = new BnbIndividualAddress(true);
                newAddressLink.RegisterForEdit(em);
                newAddressLink.AddressTypeID = addresstypeid;
                BnbAddress newAddress = new BnbAddress(true);
                newAddress.RegisterForEdit(em);
                newAddress.AddressLine1 = ReverseNull(address1);
                newAddress.AddressLine2 = ReverseNull(address2);
                newAddress.AddressLine3 = ReverseNull(address3);
                newAddress.AddressLine4 = ReverseNull(postaltown);
                newAddress.County = ReverseNull(county);
                newAddress.PostCode = postcode;

                //Include default latitude, longitude value during import
                newAddress.Latitude = Convert.ToSingle(9999);
                newAddress.Longitude = Convert.ToSingle(9999);

                newAddress.CountryID = convertedCountryID;
                newAddress.IndividualAddresses.Add(newAddressLink);
                indv.IndividualAddresses.Add(newAddressLink);
                addressForTelephone = newAddress;
            }
            else
                addressForTelephone = indv.CurrentIndividualAddress.Address;

            if (telephonenumber != "")
            {
                addressForTelephone.RegisterForEdit(em);
                addressForTelephone.SetTelephone(indv, telephonenumber);
            }
            if (faxnumber != "" && faxnumber != null)
            {
                addressForTelephone.RegisterForEdit(em);
                addressForTelephone.SetFax(indv, faxnumber);
            }


        }

        /// <summary>
        /// adds or updates specified contact number
        /// </summary>
        /// <param name="em"></param>
        /// <param name="indv"></param>
        /// <param name="contactnumbertype"></param>
        /// <param name="contactnumber"></param>
        private static void SetContactNumberToIndividual(BnbEditManager em, BnbIndividual indv, int contactnumbertype, string contactnumber)
        {
            contactnumber += "";
            if (contactnumber != "")
            {
                BnbContactNumber existing = indv.ContactNumbers.FindContactNumberType(contactnumbertype);
                if (existing == null)
                {
                    BnbContactNumber cn = new BnbContactNumber(true);
                    cn.RegisterForEdit(em);
                    cn.ContactNumberTypeID = contactnumbertype;
                    cn.ContactNumber = contactnumber;
                    cn.DateFrom = DateTime.Today;
                    indv.ContactNumbers.Add(cn);
                }
                else
                {
                    existing.RegisterForEdit(em);
                    existing.ContactNumber = contactnumber;
                }
            }
        }

        private static BnbApplication CreateApplicationAfterRegistration(BnbEditManager em, BnbIndividual indv, int enquirymenthodID, int statusID, DateTime availabilityDate, int vsoTypeID, string regSourceOther)
        {
            BnbApplication newEnq = new BnbApplication(true);
            newEnq.RegisterForEdit(em);
            newEnq.EnquiryMethodID = BnbConst.ApplicationSource_OnlineEnquiry;
            //newEnq.EnquiryUserID = BnbConst.User_OnlineEnquiry;

            newEnq.ApplicationSourceID = BnbDomainObject.NullInt;
            //newEnq.ApplicationSourceID = applicationsourceID;
            newEnq.AddApplicationStatus(statusID);
            if (availabilityDate != DateTime.MinValue)
                newEnq.AvailabilityDate = availabilityDate;

            if (vsoTypeID > -1)
            {
                newEnq.VSOTypeID = vsoTypeID;
                BnbLookupDataTable vsoTypeTable = BnbEngine.LookupManager.GetLookup("lkuVSOType");
                BnbLookupRow foundRow = vsoTypeTable.FindByID(vsoTypeID);
                if (foundRow != null)
                    newEnq.VSOGroupID = (int)foundRow["VSOGroupID"];
            }

            newEnq.SourceOther = regSourceOther;

            indv.Applications.Add(newEnq);

            //also return it
            return newEnq;
        }

        private static BnbApplication UpdateApplicationAfterApplicationForm(BnbEditManager em, BnbIndividual indv, Guid applicationID, int newStatusID, DateTime availabilityDate, string appSourceOther)
        {
            // look for specified application
            BnbApplication app = null;
            if (applicationID != Guid.Empty)
                app = indv.Applications[applicationID] as BnbApplication;
            // if not found, add a new one
            if (app == null)
            {
                app = new BnbApplication(true);
                app.RegisterForEdit(em);
                indv.Applications.Add(app);
                app.AddApplicationStatus(newStatusID);
            }
            else
                app.RegisterForEdit(em);

            // update app status
            if (app.CurrentStatus != null && app.CurrentStatus.StatusID != newStatusID)
                app.AddApplicationStatus(newStatusID);

            // update availability
            if (availabilityDate != DateTime.MinValue)
                app.AvailabilityDate = availabilityDate;

            app.ApplicationSourceID = 1008;

            //update SourceOther
            app.SourceOther = appSourceOther;

            // also return the app
            return app;

        }



        private static string ReverseNull(string maybeEmpty)
        {
            if (maybeEmpty == "")
                return null;
            else
                return maybeEmpty;
        }



        private static Guid GetGuidLookupID(string description, string lookupTableName)
        {
            if (description == null || description == "")
                return Guid.Empty;

            BnbLookupDataTable lookupTable = BnbEngine.LookupManager.GetLookup(lookupTableName);
            string select = String.Format("Description = '{0}'", description);
            if (lookupTable.Columns.Contains("Exclude"))
                select += " and Exclude = 0";
            DataRow[] findRows = lookupTable.Select(select);
            if (findRows.Length > 0)
                return new Guid(findRows[0]["GuidID"].ToString());
            else
                return Guid.Empty;
        }


        private static int GetIntLookupID(string description, string lookupTableName)
        {
            if (description == null || description == "")
                return BnbDomainObject.NullInt;

            BnbLookupDataTable lookupTable = BnbEngine.LookupManager.GetLookup(lookupTableName);
            string select = String.Format("Description = '{0}'", description);
            if (lookupTable.Columns.Contains("Exclude"))
                select += " and Exclude = 0";
            DataRow[] findRows = lookupTable.Select(select);
            if (findRows.Length > 0)
                return (int)findRows[0]["IntID"];
            else
                return BnbDomainObject.NullInt;
        }

        /// <summary>
        /// get mapped online forms language ability mapping id with frond language level id
        /// </summary>
        /// <param name="formsLanguageAbilityID"></param>
        /// <returns></returns>
        private static int GetMappedLanguageLevelID(int formsLanguageAbilityID)
        {
            // get source mapping
            DataSet ds = new DataSet();
            string path = AppDomain.CurrentDomain.BaseDirectory + @"/EditPages/EGateway/LanguageLevelMapping.xml";
            ds.ReadXml(path, XmlReadMode.InferSchema);

            DataTable abilityMap = ds.Tables["LanguageMapping"];
            DataRow[] findMappedRows = abilityMap.Select(String.Format("FormsLanguageAbilityID = '{0}'", formsLanguageAbilityID));
            if (findMappedRows.Length > 1)
                throw new ApplicationException(String.Format("Found too many language ability mapping rows for FormsLanguageAbilityID {0}", formsLanguageAbilityID));
            else if (findMappedRows.Length <= 0)
                throw new ApplicationException(String.Format("No language ability mapping specified for FormsLanguageAbilityID {0}", formsLanguageAbilityID));
            else
                return Convert.ToInt32(findMappedRows[0]["FrondLanguageLevelID"].ToString());
        }


    }


    public class BnbImportFormResults
    {
        public bool Imported;
        public Guid IndividualID;
        public string IndividualRefNo;
        public Guid ApplicationID;
        public BnbProblemCollection Problems;
        public Exception ImportException;
    }
}
