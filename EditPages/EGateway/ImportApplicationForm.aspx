<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportApplicationForm.aspx.cs" Inherits="Frond.EditPages.EGateway.ImportApplicationForm" %>

<%@ Register Src="../../UserControls/WaitMessagePanel.ascx" TagName="WaitMessagePanel"
    TagPrefix="uc2" %>

<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.DataGrids"
    TagPrefix="bnbdatagrid" %>

<%@ Register TagPrefix="uc1" TagName="Title" Src="../../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
  <HEAD>
		<title>Import Application Forms</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<script language="javascript">
   // code to remember scroll-bar position between postbacks
   // (like SmartNavigation, but without the bugs)
   function AltSmartNav_GetCoords()
   {
      var scrollX, scrollY;
      
      if (document.all)
      {
         if (!document.documentElement.scrollLeft)
            scrollX = document.body.scrollLeft;
         else
            scrollX = document.documentElement.scrollLeft;
               
         if (!document.documentElement.scrollTop)
            scrollY = document.body.scrollTop;
         else
            scrollY = document.documentElement.scrollTop;
      }   
      else
      {
         scrollX = window.pageXOffset;
         scrollY = window.pageYOffset;
      }
   
      document.forms["Form1"].hiddenXCoord.value = scrollX;
      document.forms["Form1"].hiddenYCoord.value = scrollY;
   }
   
   function AltSmartNav_Scroll()
   {
      var x = document.forms["Form1"].hiddenXCoord.value;
      var y = document.forms["Form1"].hiddenYCoord.value;
      window.scrollTo(x, y);
   }
   
   window.onload = AltSmartNav_Scroll;
   window.onscroll = AltSmartNav_GetCoords;
   window.onkeypress = AltSmartNav_GetCoords;
   window.onclick = AltSmartNav_GetCoords;
		</script>
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../../Images/batchprocess24silver.gif"></uc1:title>
				<div id="pageContentDiv">
				<p><a href="EGatewayUtil.aspx">E-Gateway Homepage</a></p>
                <p>This is a list of Completed Application Forms.</p>
                <bnbdatagrid:BnbSmartGridWithControls ID="uxAppSmartGrid" runat="server" ShowExportButton = "False" OnColumnButtonClicked="uxAppSmartGrid_ColumnButtonClicked" OnExportClicked="uxAppSmartGrid_ExportClicked" OnOptionsChanged="uxAppSmartGrid_OptionsChanged" SortColumnsByViewPosition="True" />
                <br />
                <br />
                <table width="800px">
                <tr><td><asp:Button ID="uxBulkImportButton" runat="server" Text="Bulk Import" OnClick="uxBulkImportButton_Click" />
                    <asp:Label ID="uxBulkimportFeedback" runat="server"></asp:Label></td>
                <td>
                    <asp:Button ID="uxExcelExport" runat="server"  Text="Excel Export" OnClick="uxExcelExport_Click" />
                    <asp:CheckBox ID="uxExportAllColumnsCheckbox" runat="server" Text="Export all columns" /></td>
                </tr>
                <tr>
                <td>Data fetched at 
                    <asp:Label ID="uxDataFetchedTime" runat="server" Text=""></asp:Label><asp:Button
                        ID="uxRefreshData" runat="server" Text="Refresh" OnClick="uxRefreshData_Click" /></td>
                <td></td>
                </tr>
                </table>
                <p>
                    <asp:GridView ID="uxImportHistoryGrid" runat="server" AutoGenerateColumns="False"
                        CssClass="datagrid" Width="800px" OnRowCommand="uxImportHistoryGrid_RowCommand">
                        <Columns>
                            <asp:BoundField DataField="ImportedDate" DataFormatString="{0:HH:mm:ss}" HeaderText="Import Time" />
                            <asp:BoundField DataField="ImportedSuccessMessage" HeaderText="Success" />
                            <asp:BoundField DataField="FormReference" HeaderText="Form Ref" />
                            <asp:HyperLinkField DataNavigateUrlFields="MatchedApplicationID" DataNavigateUrlFormatString="../ApplicationPage.aspx?BnbApplication={0}"
                                DataTextField="ApplicationDescription" HeaderText="Imported App" />
                            <asp:BoundField DataField="ImportMessages" HeaderText="Import Messages" />
                            <asp:TemplateField HeaderText="View Log" ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="ViewLog"
                                        Text="View Log" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"ApplicationFormID") %>' ></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    
                    </p>
                </div>
                <uc2:WaitMessagePanel ID="uxWaitMessagePanel" runat="server" DivToHide="pageContentDiv" WidthOverride="800px"/>
            </div>
            <asp:TextBox id="hiddenXCoord" runat="server" style="VISIBILITY:hidden"></asp:TextBox>
					<asp:TextBox id="hiddenYCoord" runat="server" style="VISIBILITY:hidden"></asp:TextBox>

		</form>
	</body>
</HTML>