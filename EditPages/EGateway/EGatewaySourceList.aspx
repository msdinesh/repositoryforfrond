<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EGatewaySourceList.aspx.cs" Inherits="Frond.EditPages.EGateway.EGatewaySourceList" %>

<%@ Register TagPrefix="uc1" TagName="Title" Src="../../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="NavigatePanel" Src="../../UserControls/NavigatePanel.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
  <HEAD>
		<title>EGateway Source List</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../../Images/batchprocess24silver.gif"></uc1:title>
				<br />
                <a href="EGatewayUtil.aspx">E-Gateway Homepage</a>
                <p>
                    This page shows how E-Gateway maps the 'Heard About VSO' options in the Registration/Application
                    form map to Sources in Frond. The first three columns show the options in the forms,
                    and the Source Group/Type and Source columns show the corresponding Frond Sources.
                    </p>
                    <p>'Heard About VSO' options that ask the user to specify additional text information are shown in <b>bold</b>.</p>
                    <p>Mapping problems will be listed in the 'Notes' column.</p>
                <p>
                    <asp:Button ID="uxExportSourceListButton" runat="server" Text="Export List to Excel" OnClick="uxExportSourceListButton_Click" />&nbsp;</p>
                
                <asp:Label ID="uxSourceListTable" runat="server" Text=""></asp:Label>
				</div>
		</form>
	</body>
</HTML>