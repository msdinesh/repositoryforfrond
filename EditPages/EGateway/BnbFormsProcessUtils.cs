using System;
using System.Data;
using System.Configuration;
using System.Net;
using System.Net.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Net.Mail;

using BonoboEngine;
using BonoboDomainObjects;
using BonoboEngine.Query;

namespace Frond.EditPages.EGateway
{
    /// <summary>
    /// static methods for the main E-Gateway processes
    /// e.g. import registration form, import application form
    /// </summary>
    public class BnbFormsProcessUtils
    {

        public static readonly int CONST_FetchBatchSize = 20;

        /// <summary>
        /// returns the 'completed registration form' data from the Session cache
        /// </summary>
        public static BnbCachedDataSet StoredRegistrationFormData
        {
            get { return HttpContext.Current.Session["RegistrationFormData"] as BnbCachedDataSet; }
        }

        /// <summary>
        /// Adds a 'completed registration form' dataset to the Session cache
        /// </summary>
        /// <param name="qds"></param>
        public static void SetStoredRegistrationFormData(BnbQueryDataSet qds, int batchSize)
        {
            BnbCachedDataSet cds = new BnbCachedDataSet();
            cds.QueryDataSet = qds;
            cds.FetchedDate = DateTime.Now;
            cds.BatchSize = batchSize;
            HttpContext.Current.Session["RegistrationFormData"] = cds;
        }

        /// <summary>
        /// returns the 'completed application form' data from the Session cache
        /// </summary>
        public static BnbCachedDataSet StoredApplicationFormData
        {
            get { return HttpContext.Current.Session["ApplicationFormData"] as BnbCachedDataSet; }
        }

        /// <summary>
        /// Adds a 'completed application form' dataset to the Session cache
        /// </summary>
        /// <param name="qds"></param>
        public static void SetStoredApplicationFormData(BnbQueryDataSet qds)
        {
            BnbCachedDataSet cds = new BnbCachedDataSet();
            cds.QueryDataSet = qds;
            cds.FetchedDate = DateTime.Now;
            HttpContext.Current.Session["ApplicationFormData"] = cds;
        }

        /// <summary>
        /// Returns the last form search results from the Session cache
        /// </summary>
        public static BnbCachedDataSet StoredSearchFormData
        {
            get { return HttpContext.Current.Session["SearchFormData"] as BnbCachedDataSet; }
        }

        /// <summary>
        /// Adds a 'form search results' dataset to the session cache
        /// </summary>
        /// <param name="qds"></param>
        public static void SetStoredSearchFormData(BnbQueryDataSet qds)
        {
            BnbCachedDataSet cds = new BnbCachedDataSet();
            cds.QueryDataSet = qds;
            cds.FetchedDate = DateTime.Now;
            HttpContext.Current.Session["SearchFormData"] = cds;
        }

        public static DataTable ImportProcessHistoryDataTable
        {
            get
            {
                DataTable dt = HttpContext.Current.Session["FormsImportProcessHistoryDataTable"] as DataTable;
                if (dt == null)
                {
                    dt = MakeNewImportProcessHistory();
                    HttpContext.Current.Session["FormsImportProcessHistoryDataTable"] = dt;
                }
                return dt;
            }
        }

        public static DataView ImportRegistrationFormsHistoryDataView
        {
            get
            {
                DataView dv = new DataView(ImportProcessHistoryDataTable);
                dv.Sort = "ImportedDate DESC";
                dv.RowFilter = "ImportType = 'RegImport'";
                return dv;
            }
        }


        public static DataView ImportApplicationFormsHistoryDataView
        {
            get
            {
                DataView dv = new DataView(ImportProcessHistoryDataTable);
                dv.Sort = "ImportedDate DESC";
                dv.RowFilter = "ImportType = 'AppImport'";
                return dv;
            }
        }

        /// <summary>
        /// Controls whether the test mode email box is shown on the reg form import
        /// </summary>
        public static bool TestModeEmail
        {
            get
            {
                DataSet ds = GetImportFormsSettings();
                DataTable generalSettings = ds.Tables["GeneralSettings"];
                bool testModeAllow = (generalSettings.Rows[0]["TestModeEmailing"].ToString().ToLower() == "true");
                return testModeAllow;

            }
        }

        /// <summary>
        /// tracks whether the user has enabled or disabled test mode email
        /// </summary>
        public static bool TestModeEmailEnabled
        {
            get
            {
                if (HttpContext.Current.Session["TestModeEmailEnabled"] == null)
                    HttpContext.Current.Session["TestModeEmailEnabled"] = TestModeEmail;
                return (bool)HttpContext.Current.Session["TestModeEmailEnabled"];
            }
            set{
                HttpContext.Current.Session["TestModeEmailEnabled"] = value;
            }
        }

        public static string TestModeEmailTo
        {
            get
            {
                DataSet ds = GetImportFormsSettings();
                DataTable generalSettings = ds.Tables["GeneralSettings"];
                return (generalSettings.Rows[0]["TestModeEmailTo"].ToString());
            }
        }

        private static DataTable MakeNewImportProcessHistory()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ImportType", typeof(string));
            dt.Columns.Add("ApplicationFormID", typeof(Guid));
            dt.Columns.Add("FormReference", typeof(string));
            dt.Columns.Add("ImportedDate", typeof(DateTime));
            dt.Columns.Add("ImportedSuccess", typeof(bool));
            dt.Columns.Add("ImportedSuccessMessage", typeof(string));
            dt.Columns.Add("NewIndividual", typeof(bool));
            dt.Columns.Add("MatchedIndividualID", typeof(Guid));
            dt.Columns.Add("MatchedApplicationID", typeof(Guid));
            dt.Columns.Add("ApplicationDescription", typeof(string));
            dt.Columns.Add("ImportMessages", typeof(string));
            return dt;
        }

        /// <summary>
        /// adds summary info about an import to the in-session log
        /// </summary>
        private static void AddImportProcessHistory(string importType,
            Guid applicationFormID,
            string formReference,
            DateTime importedDate,
            bool importedSuccess,
            bool newIndividual,
            Guid applicationID,
            string importMessages)
        {
            Guid individualID = Guid.Empty;
            string appDesc = "";
            if (applicationID != Guid.Empty)
            {
                BnbApplication importedApp = BnbApplication.Retrieve(applicationID);
                individualID = importedApp.IndividualID;
                appDesc = importedApp.InstanceDescription;
            }
            
            DataTable dt = ImportProcessHistoryDataTable;
            DataRow newRow = dt.NewRow();
            newRow["ImportType"] = importType;
            newRow["ApplicationFormID"] = applicationFormID;
            newRow["FormReference"] = formReference;
            newRow["ImportedDate"] = importedDate;
            newRow["ImportedSuccess"] = importedSuccess;
            if (importedSuccess)
                newRow["ImportedSuccessMessage"] = "Imported";
            else
                newRow["ImportedSuccessMessage"] = "Failed";
            newRow["NewIndividual"] = newIndividual;
            newRow["MatchedIndividualID"] = individualID;
            newRow["MatchedApplicationID"] = applicationID;
            newRow["ApplicationDescription"] = appDesc;
            if (importedSuccess)
                newRow["ImportMessages"] = "";
            else 
                newRow["ImportMessages"] = importMessages;
            dt.Rows.Add(newRow);

        }

        public static BnbQueryDataSet CheckForDuplicates(string email, string surname, string forename)
        {
            // first go - look for matching email 
            BnbCriteria crit1 = new BnbCriteria(new BnbTextCondition("Custom_Email", email, BnbTextCompareOptions.ExactMatch));
            BnbQuery q1 = new BnbQuery("vwBonoboIndividualDuplicateCheckImportUtility", crit1);
            BnbQueryDataSet res1 = q1.Execute();
            // if this finds 1 or more rows, it will do
            if (res1.Tables["results"].Rows.Count > 0)
                return res1;
            // otherwise, search on surname and start of forename
            BnbCriteria crit2 = new BnbCriteria(new BnbTextCondition("tblIndividualKeyInfo_Surname", surname, BnbTextCompareOptions.ExactMatch));
            string startOfForename = forename;
            if (startOfForename.Length > 3)
                startOfForename = startOfForename.Substring(0, 3);
            crit2.QueryElements.Add(new BnbTextCondition("tblIndividualKeyInfo_Forename", startOfForename, BnbTextCompareOptions.StartOfField));
            BnbQuery q2 = new BnbQuery("vwBonoboIndividualDuplicateCheckImportUtility", crit2);
            BnbQueryDataSet res2 = q2.Execute();
            return res2;
        }

        public static void ImportRegistrationFormProcess(Guid matchIndividualID, DataRow importRow, BnbQueryDataSet qds)
        {
            
            // object to keep track of progress
            BnbProcessInfo processInfo = new BnbProcessInfo();
            try
            {
                InternalImportRegistrationFormProcess(processInfo, matchIndividualID, importRow, qds);
            }
            catch (Exception e)
            {
                // any exceptions should be logged and then added to progress
                int errorTicketID = BonoboWebControls.BnbWebFormManager.LogErrorToDatabase(e);
                processInfo.AddProgressMessage(String.Format("Error: {0} (ErrorTicketID {1})", e.Message, errorTicketID));
                processInfo.Success = false;

                // check for common communication failure errors
                if (e is WebException)
                {
                    if (e.Message.StartsWith("The operation has timed out"))
                        processInfo.AddProgressMessage("Failure Diagnosis: A call to the Online Apps Server timed out. Please try again later.");
                    if (e.Message.StartsWith("Unable to connect to the remote server") ||
                        e.Message.StartsWith("The request failed with HTTP status 404"))
                        processInfo.AddProgressMessage("Failure Diagnosis: Frond has lost contact with the Online Apps server.");
                }
                if (e is System.Web.Services.Protocols.SoapException)
                {
                    if (e.Message.IndexOf("The server is not operational.") > -1 
                        && e.Message.IndexOf("at System.DirectoryServices.DirectoryEntry.Bind") > -1)
                        processInfo.AddProgressMessage("Failure Diagnosis: Frond could not log into the Online Apps server because the External Active Directory server is not responding");
                }
            }

            // save process messages to log table
            try
            {
                LogProcessMessages(processInfo);
            }
            catch (Exception e)
            {
                // any exceptions should be logged and then added to progress
                int errorTicketID = BonoboWebControls.BnbWebFormManager.LogErrorToDatabase(e);
                processInfo.AddProgressMessage(String.Format("Failed to log steps to tblEGatewayLog. Error: {0} (ErrorTicketID {1})", e.Message, errorTicketID));             
            }
            
            // update dataset based on success or otherwise
            // just add final message
            if (processInfo.Success)
            {
                importRow["Extra_Import"] = "Imported";
                importRow["Extra_ImportResults"] = processInfo.GetFinalMessage();
            }
            else
            {
                importRow["Extra_Import"] = "Failed";
                importRow["Extra_ImportResults"] = processInfo.GetFinalMessage();
            }
            // log all messages to an action log
            BonoboWebControls.BnbWebFormManager.LogAction("ImportRegistrationFormProcess", String.Join(", ", processInfo.ProgressMessages), null);
            BnbEngine.SessionManager.ClearObjectCache();
            // add the row into the in-session progress log
            // just add final message
            bool newIndividual = (matchIndividualID == Guid.Empty);
            Guid appFormID = new Guid(importRow["tblApplicationForm_ApplicationFormID"].ToString());
            string formRef = importRow["tblApplicationForm_FormReference"].ToString();
            BnbFormsProcessUtils.AddImportProcessHistory("RegImport",appFormID, formRef, DateTime.Now, processInfo.Success,
                newIndividual, processInfo.ApplicationID, processInfo.GetFinalMessage());
           

        }

        private static void InternalImportRegistrationFormProcess(BnbProcessInfo processInfo, Guid matchIndividualID, DataRow importRow, BnbQueryDataSet qds)
        {
            
            Guid appFormID = new Guid(importRow["tblApplicationForm_ApplicationFormID"].ToString());
            processInfo.ApplicationFormID = appFormID;
            string formRef = importRow["tblApplicationForm_FormReference"].ToString();
            processInfo.FormReference = formRef;
            processInfo.AddProgressMessage(String.Format("ImportRegistrationFormProcess for Form Ref {0}", formRef));
            int decisionID = (int)importRow["Extra_DecisionID"];
            string decisionDesc = BnbEngine.LookupManager.GetLookupItemDescription("vlkuBonoboRegistrationFormDecision", decisionID);
            processInfo.AddProgressMessage(String.Format("DecisionID {0} - {1}", decisionID, decisionDesc));
            // work out invited, etc
            BnbLookupDataTable decisionTable = BnbEngine.LookupManager.GetLookup("vlkuBonoboRegistrationFormDecision");
            BnbLookupRow decisionRow = decisionTable.FindByID(decisionID);
            if (decisionRow == null)
            {
                processInfo.AddProgressMessage("Could not find row in vlkuBonoboRegistrationFormDecision");
                if (decisionID == -1)
                    processInfo.AddProgressMessage("Failure Diagnosis: No import decision was specified (invite, decline, etc)");
                processInfo.Success = false;
                return;
            }
            int skillID = (int)importRow["Extra_SkillID"];
            string skillDesc = BnbEngine.LookupManager.GetLookupItemDescription("vlkuBonoboSkillWithGroupAbbrev", skillID);
            processInfo.AddProgressMessage(String.Format("SkillID {0} - {1}", skillID, skillDesc));
            if (skillID == -1)
            {
                processInfo.AddProgressMessage("Failure Diagnosis: No Skill was specified");
                processInfo.Success = false;
                return;
            }

            bool invited = (bool)decisionRow["Invite"];
            int statusID = (int)decisionRow["StatusID"];
            int actionPackID = -1;
            if (decisionRow["ContactDescriptionID"] != DBNull.Value)
                actionPackID = (int)decisionRow["ContactDescriptionID"];
            int vsoTypeID = -1;
            if (decisionRow["VSOTypeID"] != DBNull.Value)
                vsoTypeID = (int)decisionRow["VSOTypeID"];

            string recBase = (string)importRow["tblApplicationForm_RecruitmentBaseID"];
            bool stage1Success = RegStage1GetImportLock(processInfo, appFormID, recBase);
            if (!stage1Success)
            {
                processInfo.Success = false;
                return;
            }

            BnbImportFormResults stage2Results = RegStage2ImportToMainDatabase(processInfo, matchIndividualID,
                importRow, qds, statusID, actionPackID, vsoTypeID);

            if (!stage2Results.Imported)
            {
                processInfo.Success = false;
                return;
            }

            bool stage3Success = RegStage3ConfigureUserAccount(processInfo, invited, appFormID,
                stage2Results.IndividualID, stage2Results.IndividualRefNo, stage2Results.ApplicationID, false,recBase);

            if (!stage3Success)
            {
                processInfo.Success = false;
                return;
            }

            string email = importRow["tblRegistrationPage1_Email"].ToString();
            bool stage4Success = RegStage4SendInviteOrDeclineEmail(processInfo, decisionID, null, email,
                processInfo.VsoOnlineAccountExists, processInfo.ADUsername, processInfo.ApplicationFormUserID, invited, recBase);

            // if here, it must have been ok
            processInfo.Success = true;
            // add final message
            processInfo.AddProgressMessage(String.Format("Registration Form {0} imported successfully", formRef));
            
            return;

        }

        private static bool RegStage1GetImportLock(BnbProcessInfo processInfo, Guid appFormID, string recBaseID)
        {
            processInfo.AddProgressMessage("Calling Web Service to get Import Lock ...");
            string castanetUsername = ConfigurationManager.AppSettings["CastanetAdminUsername"].ToString();
            string castanetPassword = ConfigurationManager.AppSettings["CastanetAdminPassword"].ToString();
            bool success;

            if (recBaseID.Trim().ToUpper()== "IRE")
            {
                WsApplicationFormIreland.FrondIntegration fi = new WsApplicationFormIreland.FrondIntegration();
                if (System.Configuration.ConfigurationManager.AppSettings["WebServiceDefaultCredentials"] != null)
                {
                    try
                    {
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["WebServiceDefaultCredentials"]) == true)
                        {
                            fi.PreAuthenticate = true;
                            fi.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        }
                    }
                    catch
                    {
                        throw new ApplicationException("Please configure a valid 'WebServiceDefaultCredentials' in Appsettings");
                    }
                }
                else
                    throw new ApplicationException("Please configure 'WebServiceDefaultCredentials' in Appsettings");
                WsApplicationFormIreland.BnbWebServiceProcessInfo wsProcessIreland = fi.GetRegistrationImportLock(castanetUsername, castanetPassword,
                    appFormID);
                success=LogRegErrors(processInfo, wsProcessIreland.Success, wsProcessIreland.ProgressMessages, wsProcessIreland.CastanetErrorTicketID);
            }
            else 
            {
                WsOnlineForms.FrondIntegration onlineForms = new WsOnlineForms.FrondIntegration();
                if (System.Configuration.ConfigurationManager.AppSettings["WebServiceDefaultCredentials"] != null)
                {
                    try
                    {
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["WebServiceDefaultCredentials"]) == true)
                        {
                            onlineForms.PreAuthenticate = true;
                            onlineForms.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        }
                    }
                    catch
                    {
                        throw new ApplicationException("Please configure a valid 'WebServiceDefaultCredentials' in Appsettings");
                    }
                }
                else
                    throw new ApplicationException("Please configure 'WebServiceDefaultCredentials' in Appsettings");
                WsOnlineForms.BnbWebServiceProcessInfo wsProcessOnlineForms = onlineForms.GetRegistrationImportLock(castanetUsername, castanetPassword,
                    appFormID);
                success = LogRegErrors(processInfo, wsProcessOnlineForms.Success, wsProcessOnlineForms.ProgressMessages, wsProcessOnlineForms.CastanetErrorTicketID);
            }
            return success;
        }

        private static bool LogRegErrors(BnbProcessInfo processInfo, bool processSuccess, string [] progressMessages, int castanetErrorTicketID)
        {
            if (!processSuccess)
            {
                // add progress messages from web service into our own list:
                processInfo.AddProgressMessage("Web Service call to get Import Lock failed. Details follow:");
                bool alreadyImported = false;
                bool lockActive = false;
                foreach (string message in progressMessages)
                {
                    processInfo.AddProgressMessage(message);
                    if (message.IndexOf("may have been imported already") > -1)
                        alreadyImported = true;
                    if (message.IndexOf("ImportLock set to") > -1)
                        lockActive = true;
                }

                if (castanetErrorTicketID > 0)
                    processInfo.AddProgressMessage(String.Format("ErrorTicketID in ApplicationForm database: {0}", castanetErrorTicketID));

                // add final diagnostic message if reason for failure known
                if (alreadyImported)
                    processInfo.AddProgressMessage(String.Format("Failure Diagnosis: The Form appears to have been imported already."));
                if (lockActive)
                    processInfo.AddProgressMessage(String.Format("Failure Diagnosis: The Form is 'Import Locked' after a previous Import attempt. Please try again in about 5 minutes"));

                return false;
            }
            else
            {
                processInfo.AddProgressMessage("Import Lock obtained OK");
                return true;
            }
        }

        private static BnbImportFormResults RegStage2ImportToMainDatabase(BnbProcessInfo processInfo, Guid matchIndividualID, DataRow importRow, BnbQueryDataSet qds,
            int statusID, int actionPackID, int vsoTypeID)
        {
            DateTime availableFrom = DateTime.MinValue; // CalculateAvailableFrom(importRow);
            bool disability = ConvertYesNoToBool(importRow, "Custom_HasDisability");
            bool contactVolunteeringOptions = ConvertYesNoToBool(importRow, "Custom_HasVolunteeringOptions");
            bool contactByEmail = ConvertYesNoToBool(importRow, "Custom_HasEmailSMS");
            //VAF-348 - Importing pension field to tblIndividualAdditional
            //bool pension = ConvertYesNoToBool(importRow, "tblApplicationFormPage5_Pension");
            // work out source
            int heardAboutVSOID = (int)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage4_HeardFromVSOID"],-1);
            int heardAboutVSODetailID = (int)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage4_HeardFromVSODetailID"], -1);
            int heardAboutVSOFurtherDetailID = (int)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage4_HeardFromVSOFurtherDetailID"], -1);
            Guid mappedSourceID = BnbFormsProcessUtils.GetMappedSourceID(heardAboutVSOID, heardAboutVSODetailID, heardAboutVSOFurtherDetailID);
            int skillID = (int)BnbQueryUtils.ReplaceDBNull(importRow["Extra_SkillID"], -1);

            processInfo.AddProgressMessage("Import Reg Form Data into main DB ...");

            //TPT Commented-change request dated(10-5-2011):addressline1 is non mandatory for jitolee i.e. for recruitment base id KEN
            string importRegAddressLine1 = "";
            string recBaseIDImport = (string)importRow["tblApplicationForm_RecruitmentBaseID"];
            if (recBaseIDImport.Trim().ToUpper() == "KEN")
            {
                importRegAddressLine1 = "N/A";
            }
            else
                importRegAddressLine1 = (string)importRow["tblRegistrationPage1_AddressLine1"];

            BnbImportFormResults impResults = BnbImportFormUtils.SaveRegistrant(matchIndividualID,
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["lkuTitle_Description"], null),
                    (string)importRow["tblRegistrationPage1_FirstName"],
                    (string)importRow["tblRegistrationPage1_Surname"],
                    importRegAddressLine1,
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage1_AddressLine2"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage1_AddressLine3"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage1_PostalTown"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage1_County"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage1_PostCode"], null),
                    (string)importRow["Custom_AddressCountry"],
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage1_DaytimePhone"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage1_Email"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage1_FaxNumber"], null),
                    (DateTime)importRow["tblRegistrationPage1_DateOfBirth"],
                    (string)importRow["lkuNationality_Description"],
                    availableFrom,
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["lkuGender_Description"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["lkuEthnicity_Description"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage4_EthnicityOther"], null),
                    disability,
                    contactVolunteeringOptions,
                    contactByEmail,
                    statusID,
                    actionPackID,
                    mappedSourceID,
                    vsoTypeID,
                    skillID,
                    null,
                    //pension,
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage4_HeardFromVSOOther"], null));

            if (!impResults.Imported)
            {
                string failReason = "";
                if (impResults.Problems != null)
                {
                    failReason = "due to following validation messages: ";
                    foreach (BnbProblem prob in impResults.Problems)
                    {
                        failReason += prob.Message + " ";
                    }
                }
                if (impResults.ImportException != null)
                {
                    failReason = "due to an unexpected error: " + impResults.ImportException.Message;
                }
                processInfo.AddProgressMessage(String.Format("Failure Diagnosis: Saving the data to Frond DB failed {0}",
                    failReason));
                return impResults;
            }
            else
            {
                processInfo.ApplicationID = impResults.ApplicationID;
                processInfo.IndividualID = impResults.IndividualID;
                string message = "";
                if (matchIndividualID == Guid.Empty)
                    message = String.Format("Created Individual record {0}", impResults.IndividualRefNo);
                else
                    message = String.Format("Updated Individual record {0}", impResults.IndividualRefNo);
                processInfo.AddProgressMessage(String.Format("Import to main DB OK. {0}", message));
                return impResults;
            }

        }

        private static bool ConvertYesNoToBool(DataRow importRow, string fieldName)
        {
            bool yesNo = false;
            string yesNoString = importRow[fieldName].ToString();
            if (yesNoString.ToLower() == "yes")
                yesNo = true;
            else if (yesNoString.ToLower() == "no")
                yesNo = false;
            else if (yesNoString.ToLower() == "")
                yesNo = false;
            else
                throw new ApplicationException(String.Format("Could not translate field {0} to boolean. Value: {1}", fieldName, yesNoString));
            return yesNo;
        }

        private static DateTime CalculateAvailableFrom(DataRow importRow)
        {
            // work out availablefrom
            DateTime availableFrom = DateTime.MinValue;
            // if they have specified a From date, use that
            //Commented TPT- for adding availability from date for bahaginan rec base since other info page is not there
            //
            //if (importRow["tblRegistrationPage0_AvailableFromDate"] != DBNull.Value)
            //    availableFrom = (DateTime)importRow["tblRegistrationPage0_AvailableFromDate"];
            //else
            //{
                // otherwise, base it on the picklist they chose
                int availabilityRange = (int)importRow["tblRegistrationPage0_AvailabilityLength"];
                int addMonths = 12;
                switch (availabilityRange)
                {
                    case 4:
                        // within 6 months - say 3?
                        addMonths = 3;
                        break;
                    case 7:
                        // 6-12 months, say 9?
                        addMonths = 9;
                        break;
                    case 8:
                        // more than 12, say 15?
                        addMonths = 15;
                        break;
                }
                availableFrom = DateTime.Today.AddMonths(addMonths);
            //}
            return availableFrom;
        }

        public static Guid GetMappedSourceID(int heardAboutVSOID, int heardAboutVSODetailID, int heardAboutVSOFurtherDetailID)
        {
            // get source mapping
            DataSet ds = new DataSet();
            string path = AppDomain.CurrentDomain.BaseDirectory + @"/EditPages/EGateway/SourceMapping.xml";
            ds.ReadXml(path, XmlReadMode.InferSchema);
            
            // level 3 table
            DataTable sourceMap = ds.Tables["SourceMapping"];
            DataRow[] findSourceRows = sourceMap.Select(String.Format("HeardAboutVSOFurtherDetailID = '{0}'", heardAboutVSOFurtherDetailID));
            if (findSourceRows.Length > 1)
                throw new ApplicationException(String.Format("Found too many source mapping rows for HeardAboutVSOFurtherDetailID {0}", heardAboutVSOFurtherDetailID));
            if (findSourceRows.Length == 1)
            {
                // return the sourceID
                return new Guid(findSourceRows[0]["SourceID"].ToString());
            }

            // if level 3 didn't match, look for level 2
            DataTable sourceTypeMap = ds.Tables["SourceTypeMapping"];
            DataRow[] findSourceTypeRows = sourceTypeMap.Select(String.Format("HeardAboutVSODetailID = '{0}'", heardAboutVSODetailID));
            if (findSourceTypeRows.Length > 1)
                throw new ApplicationException(String.Format("Found too many source mapping rows for HeardAboutVSODetailID {0}", heardAboutVSODetailID));
            if (findSourceTypeRows.Length == 1)
            {
                // return the sourceID
                return new Guid(findSourceTypeRows[0]["SourceID"].ToString());
            }

            // if level 2 didn't match, try level 1
            DataTable sourceGroupMap = ds.Tables["SourceGroupMapping"];
            DataRow[] findSourceGroupRows = sourceGroupMap.Select(String.Format("HeardAboutVSOID = '{0}'", heardAboutVSOID));
            if (findSourceGroupRows.Length > 1)
                throw new ApplicationException(String.Format("Found too many source mapping rows for HeardAboutVSOID {0}", heardAboutVSOID));
            if (findSourceGroupRows.Length == 1)
            {
                // return the sourceID
                return new Guid(findSourceGroupRows[0]["SourceID"].ToString());
            }
            throw new ApplicationException(String.Format("Could not find any source mapping for HeardAboutVSOID {0} /DetailID {1} /FurtherDetailID {2}",
                heardAboutVSOID, heardAboutVSODetailID, heardAboutVSOFurtherDetailID));
        }

        private static bool RegStage3ConfigureUserAccount(BnbProcessInfo processInfo, bool invited, Guid appFormID, 
            Guid individualID, string refNo, Guid applicationID, bool createNewApplicationForm, string recBaseID)
        {
            // otherwise we imported ok
            DateTime importDate = DateTime.Now;
            bool success;
            // does the user have a vsoonline account?
            processInfo.AddProgressMessage("Checking if they have VSOOnline Account ...");
            string existingUsername = BnbRegistrationManager.GetUsernameIfRegistered(individualID);
            bool alreadyHasOnlineAccount = (existingUsername != null);
            if (alreadyHasOnlineAccount)
                processInfo.AddProgressMessage(String.Format("VSOOnline Account Check returned Username {0}", existingUsername));
            else
                processInfo.AddProgressMessage("VSOOnline Account Check returned No Username");

            // call web service to update app form data
            string castanetUsername = ConfigurationManager.AppSettings["CastanetAdminUsername"].ToString();
            string castanetPassword = ConfigurationManager.AppSettings["CastanetAdminPassword"].ToString();

            if (recBaseID.Trim().ToUpper() == "IRE")
            {
                WsApplicationFormIreland.BnbRegistrationImportedInfo wsImportedInfo = null;
                WsApplicationFormIreland.FrondIntegration fi = new WsApplicationFormIreland.FrondIntegration();
                if (System.Configuration.ConfigurationManager.AppSettings["WebServiceDefaultCredentials"] != null)
                {
                    try
                    {
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["WebServiceDefaultCredentials"]) == true)
                        {
                            fi.PreAuthenticate = true;
                            fi.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        }
                    }
                    catch
                    {
                        throw new ApplicationException("Please configure a valid 'WebServiceDefaultCredentials' in Appsettings");
                    }
                }
                else
                    throw new ApplicationException("Please configure 'WebServiceDefaultCredentials' in Appsettings");
                fi.Timeout = 30 * 1000;
                processInfo.ApplicationID = applicationID;
                if (createNewApplicationForm)
                {
                    processInfo.AddProgressMessage("Calling Web Service to Create Direct Application Form ...");
                    BnbIndividual indv = BnbIndividual.Retrieve(individualID);
                    wsImportedInfo = fi.CreateDirectApplicationForm(castanetUsername, castanetPassword,
                        individualID, refNo, applicationID, alreadyHasOnlineAccount, existingUsername, recBaseID, indv.FirstName, indv.Surname);
                    // get the new applicationformID which is returned from that web method
                    processInfo.ApplicationFormID = wsImportedInfo.ApplicationFormID;
                }
                else
                {
                    processInfo.AddProgressMessage("Calling Web Service to update Form Data ...");
                    wsImportedInfo = fi.RegistrationFormImported(castanetUsername, castanetPassword,
                        appFormID, invited, importDate,
                        individualID, refNo, applicationID,
                        alreadyHasOnlineAccount, existingUsername, recBaseID);
                }
                success = LogRegConfigureErrors(processInfo, wsImportedInfo.Success, wsImportedInfo.ProgressMessages, wsImportedInfo.CastanetErrorTicketID,
                     alreadyHasOnlineAccount, existingUsername, wsImportedInfo.OwnerUserID);
            }
            else
            {
                //added a comment line
                WsOnlineForms.BnbRegistrationImportedInfo wsImportedInfo = null;
                WsOnlineForms.FrondIntegration fi = new WsOnlineForms.FrondIntegration();
                if (System.Configuration.ConfigurationManager.AppSettings["WebServiceDefaultCredentials"] != null)
                {
                    try
                    {
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["WebServiceDefaultCredentials"]) == true)
                        {
                            fi.PreAuthenticate = true;
                            fi.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        }
                    }
                    catch
                    {
                        throw new ApplicationException("Please configure a valid 'WebServiceDefaultCredentials' in Appsettings");
                    }
                }
                else
                    throw new ApplicationException("Please configure 'WebServiceDefaultCredentials' in Appsettings");
                fi.Timeout = 30 * 1000;
                processInfo.ApplicationID = applicationID;
                if (createNewApplicationForm)
                {
                    processInfo.AddProgressMessage("Calling Web Service to Create Direct Application Form ...");
                    BnbIndividual indv = BnbIndividual.Retrieve(individualID);
                    wsImportedInfo = fi.CreateDirectApplicationForm(castanetUsername, castanetPassword,
                        individualID, refNo, applicationID, alreadyHasOnlineAccount, existingUsername, recBaseID, indv.FirstName, indv.Surname);
                    // get the new applicationformID which is returned from that web method
                    processInfo.ApplicationFormID = wsImportedInfo.ApplicationFormID;
                }
                else
                {
                    processInfo.AddProgressMessage("Calling Web Service to update Form Data ...");
                    wsImportedInfo = fi.RegistrationFormImported(castanetUsername, castanetPassword,
                        appFormID, invited, importDate,
                        individualID, refNo, applicationID,
                        alreadyHasOnlineAccount, existingUsername, recBaseID);
                }
                success = LogRegConfigureErrors(processInfo, wsImportedInfo.Success, wsImportedInfo.ProgressMessages, wsImportedInfo.CastanetErrorTicketID,
                     alreadyHasOnlineAccount, existingUsername, wsImportedInfo.OwnerUserID);
            }
            return success;

        }

        private static bool LogRegConfigureErrors(BnbProcessInfo processInfo, bool importSuccess, string[] progressMessages,
            int castanetErrorTicketID, bool alreadyHasOnlineAccount, string existingUsername, Guid OwnerUserID)
        {
            if (!importSuccess)
            {
                // add progress messages from web service into our own list:
                processInfo.AddProgressMessage("Web Service call failed. Details follow:");

                foreach (string message in progressMessages)
                    processInfo.AddProgressMessage(message);

                if (castanetErrorTicketID > 0)
                    processInfo.AddProgressMessage(String.Format("ErrorTicketID in ApplicationForm database: {0}", castanetErrorTicketID));

                return false;
            }
            else
            {
                // keep the info that needs to be passed on to the next stage
                processInfo.VsoOnlineAccountExists = alreadyHasOnlineAccount;
                processInfo.ApplicationFormUserID = OwnerUserID;
                processInfo.ADUsername = existingUsername;
                processInfo.AddProgressMessage("Web Service called OK");
            }
            return true;
        }


        internal static DataSet GetImportFormsSettings()
        {
            DataSet ds = new DataSet();
            string path = AppDomain.CurrentDomain.BaseDirectory + @"/EditPages/EGateway/ImportFormsSettings.xml";
            ds.ReadXml(path, XmlReadMode.InferSchema);
            doCheckEmailUrl();
            return ds;
        }

        public static void doCheckEmailUrl()
        {
            string appFormBaseUrl = "https://www.vso.org.uk/volunteering/applicationform";
            //Guid applicationFormUserID = new Guid("A745D6C3-530A-4FB2-9161-83A738D13FF4");
            string applicationFormUserID = "AF7B5A6A-CEA6-43F9-A343-C841ACC2686C";
                //"faf23e24-3b92-4e1b-a862-0712009f0ac3";
            string toAddress = "dinesh.sivalingam@tenthplanettechnologies.com";

            // build the url template for account creation
            string urlTemplate = appFormBaseUrl + "/CreateAccount.aspx?UserID={0}&Email={1}";
            // put the encrypted and encoded parameters into the url
            string url = String.Format(urlTemplate,
                HttpUtility.UrlEncode(BnbCrypter.Encrypt(applicationFormUserID.ToLower())),
                HttpUtility.UrlEncode(BnbCrypter.Encrypt(toAddress)));
            //TPT Commented-change request dated on 10-5-2011:Removing the English words in Dutch emails
            //if (recBaseID.Trim().ToUpper() == "NED")
            //    inviteSentenceTemplate = "{0}";
            // put the url into the sentence template
            //inviteSentence = String.Format(inviteSentenceTemplate, url);

        }

        //To Read the xml file for setting the view decisions
        internal static DataSet GetImportFormsSettingsViewDecisions()
        {
            DataSet dataset = new DataSet();
            string path1 = AppDomain.CurrentDomain.BaseDirectory + @"/EditPages/EGateway/RecruitmentBaseDetails.xml";
            dataset.ReadXml(path1, XmlReadMode.InferSchema);
            return dataset;
        }

        private static bool RegStage4SendInviteOrDeclineEmail(BnbProcessInfo processInfo, int decisionID, string emailCode, string toAddress, bool accountExists,
            string adUsername, Guid applicationFormUserID, bool invited, string recBaseID)
        {
            processInfo.AddProgressMessage(String.Format("Checking to see if Email needed ..."));
            // if no emailcode specified then get it using decisionID
            if (emailCode == null || emailCode == "")
            {
                emailCode = null;
                BnbLookupDataTable decisionTable = BnbEngine.LookupManager.GetLookup("vlkuBonoboRegistrationFormDecision");
                BnbLookupRow findRow = decisionTable.FindByID(decisionID);
                if (findRow["EmailCode"] != DBNull.Value) 
                    emailCode = findRow["EmailCode"].ToString();
            }
            // if no emailcode then no email
            if (emailCode == null)
            {
                // no email to send
                processInfo.AddProgressMessage(String.Format("No EmailCode specified for decisionID {0}.", decisionID));
                processInfo.AddProgressMessage("No Email sent.");
                return true; 

            }
            DataSet ds = GetImportFormsSettings();
            DataTable generalSettings = ds.Tables["GeneralSettings"];
            DataTable packEmailTemplates = ds.Tables["PackEmailTemplate"];

            // get app form url settings
            string overrideApplicationFormUrl = null;
            //Get Link to Application form, based on what their recruitment base id is.
           // BnbApplication app= BnbApplication.Retrieve(processInfo.ApplicationID);
            string recbasename = string.Empty;
            if (recBaseID.Trim().ToUpper() == "UK")
                recbasename = "VSOUK";
            else if (recBaseID.Trim().ToUpper() == "IRE")
                recbasename = "VSOIreland";
            else if (recBaseID.Trim().ToUpper() == "KEN")
            {
                recbasename = "VSOJitolee";
                emailCode = "KEN_" + emailCode;
            }
            else if (recBaseID.Trim().ToUpper() == "PHI")
            {
                recbasename = "VSOBahaginan";
                emailCode = "PHI_" + emailCode;
            }
            else if (recBaseID.Trim().ToUpper() == "NED")
            {
                recbasename = "VSONetherland";
                emailCode = "NED_" + emailCode;
            }
            else if (recBaseID.Trim().ToUpper() == "CUSO")
            {
                recbasename = "VSOCUSO";
                emailCode = "CUSO_" + emailCode;
            }
 
                    
            overrideApplicationFormUrl = generalSettings.Rows[0]["OverrideApplicationFormUrl"+recbasename].ToString();
            // find row that corresponds to email code
            DataRow[] findRows = packEmailTemplates.Select(String.Format("EmailCode = '{0}'", emailCode));
            if (findRows.Length == 0)
            {
                // no email to send then thats a problem
                throw new ApplicationException(String.Format("No Email Template found for EmailCode {0}", emailCode));
            }


            bool isInviteEmail = Convert.ToBoolean(findRows[0]["IsInviteEmail"].ToString());
            string emailBodyTemplate = findRows[0]["EmailBodyTemplate"].ToString();
            string emailSubjectTemplate = findRows[0]["EmailSubject"].ToString();
            string appFormBaseUrl = null;
            // work out ref no
            BnbIndividual tempIndv = BnbIndividual.Retrieve(processInfo.IndividualID);
            string refNo = tempIndv.RefNo;
            // check isinviteemail matches flag passed in
            // (just a safety check)
            if (invited != isInviteEmail)
                throw new ApplicationException(String.Format("Invited is {0} but Email Template for EmailCode {1} has IsInviteEmail set to {2}",
                    invited, emailCode, isInviteEmail));
            
            appFormBaseUrl = overrideApplicationFormUrl;
            
            string emailBody = null;
            string emailSubject = null;
            if (isInviteEmail)
            {
                processInfo.AddProgressMessage("Arranging Invitation text ...");
                string inviteSentence = null;
                // we need to put the invite sentence in
                if (accountExists)
                {
                    string inviteSentenceTemplate = generalSettings.Rows[0]["InviteSentenceHasAccount"].ToString();
                    string url = appFormBaseUrl + "/Default.aspx";
                    inviteSentence = String.Format(inviteSentenceTemplate, url, adUsername);
                }
                else
                {
                    string inviteSentenceTemplate = generalSettings.Rows[0]["InviteSentenceNoAccount"].ToString();
                    // build the url template for account creation
                    string urlTemplate = appFormBaseUrl + "/CreateAccount.aspx?UserID={0}&Email={1}";
                    // put the encrypted and encoded parameters into the url
                    string url = String.Format(urlTemplate,
                        HttpUtility.UrlEncode(BnbCrypter.Encrypt(applicationFormUserID.ToString())),
                        HttpUtility.UrlEncode(BnbCrypter.Encrypt(toAddress)));
                    //TPT Commented-change request dated on 10-5-2011:Removing the English words in Dutch emails
                    if (recBaseID.Trim().ToUpper() == "NED")
                        inviteSentenceTemplate = "{0}";
                    // put the url into the sentence template
                    inviteSentence = String.Format(inviteSentenceTemplate, url);
                    //doCheckEmailUrl(appFormBaseUrl, applicationFormUserID.ToString(), toAddress);
                }
                // put the sentence into the email
                emailBody = String.Format(emailBodyTemplate, refNo,inviteSentence);
                emailSubject = String.Format(emailSubjectTemplate, refNo);
                processInfo.AddProgressMessage("Invitation text done.");
            }
            else
            {
                processInfo.AddProgressMessage("Email is a Decline.");
                // otherwise, just substitute refno
                emailBody = String.Format(emailBodyTemplate, refNo);
                emailSubject = String.Format(emailSubjectTemplate, refNo);
                processInfo.AddProgressMessage("Invitation text done.");

            }

            processInfo.AddProgressMessage("Getting Email Settings ...");
            string smtpHost = generalSettings.Rows[0]["SmtpServer"].ToString();
            string smtpLogin = generalSettings.Rows[0]["SmtpLogin"].ToString();
            if (smtpLogin == "")
                smtpLogin = null;
            string smtpPassword = generalSettings.Rows[0]["SmtpPassword"].ToString();
            string emailApparentlyFrom = generalSettings.Rows[0]["ApparentlyFrom"].ToString();
            string emailApparentlyFromDescription = generalSettings.Rows[0]["ApparentlyFromDescription"].ToString();
            

            // re-directing emails during testing
            if (BnbFormsProcessUtils.TestModeEmailEnabled)
            {
                processInfo.AddProgressMessage(String.Format("Test Mode Email: sending to {0} instead of {1}", BnbFormsProcessUtils.TestModeEmailTo, toAddress));
                toAddress = BnbFormsProcessUtils.TestModeEmailTo;
            }


            bool smtpSSL = Convert.ToBoolean(generalSettings.Rows[0]["SmtpSSL"].ToString());
            bool blindCopyToCurrentUser = Convert.ToBoolean(generalSettings.Rows[0]["BlindCopyToCurrentUser"].ToString());
            //string blindCopyToFixedAddress = generalSettings.Rows[0]["BlindCopyToFixedAddress"].ToString();
            //TPT: Code changed to set BCC Address based on recruitment Base - Support Call Dated 10/05/2011
            string blindCopyToFixedAddress = null;

            string recBaseName = string.Empty;
            if (recBaseID.Trim().ToUpper() == "UK")
                recBaseName = "VSOUK";
            else if (recBaseID.Trim().ToUpper() == "IRE")
                recBaseName = "VSOIreland";
            else if (recBaseID.Trim().ToUpper() == "KEN")
                recBaseName = "VSOJitolee";
            else if (recBaseID.Trim().ToUpper() == "PHI")
                recBaseName = "VSOBahaginan";
            else if (recBaseID.Trim().ToUpper() == "NED")
                recBaseName = "VSONetherland";

            blindCopyToFixedAddress = generalSettings.Rows[0]["OverrideBCCAddress" + recBaseName].ToString();
            //TPT:code change ends

            string bccAddress = null;
            if (blindCopyToCurrentUser)
            {
                bccAddress = BnbEngine.SessionManager.GetSessionInfo().EmailAddress;
            }
            if (blindCopyToFixedAddress != null && blindCopyToFixedAddress != "")
                bccAddress = blindCopyToFixedAddress;

            processInfo.AddProgressMessage("Sending Email ...");
            BnbEmailManager.SendEmailViaSMTP(smtpHost, smtpLogin, smtpPassword, smtpSSL, toAddress,
                bccAddress, emailApparentlyFrom, emailApparentlyFromDescription, emailSubject, emailBody);
            string sentMessage = "Email Sent.";
            if (bccAddress != null)
                sentMessage += String.Format("Bcc'd to email {0}", bccAddress);
            processInfo.AddProgressMessage(sentMessage);
            // log email
            
            BnbEmailManager.LogEmailSent(null, emailBody, toAddress, 1004, smtpHost, null, null, processInfo.IndividualID);

            return true;
        }

        

        public static void ImportApplicationFormProcess(DataRow importRow, BnbQueryDataSet qds)
        {
            // object to keep track of progress
            BnbProcessInfo processInfo = new BnbProcessInfo();
            try
            {
                InternalImportApplicationFormProcess(processInfo, importRow, qds);
            }
            catch (Exception e)
            {
                // any exceptions should be logged and then added to progress
                int errorTicketID = BonoboWebControls.BnbWebFormManager.LogErrorToDatabase(e);
                processInfo.AddProgressMessage(String.Format("Error: {0} (ErrorTicketID {1})", e.Message, errorTicketID));
                processInfo.Success = false;

                // check for common communication failure errors
                if (e is WebException)
                {
                    if (e.Message.StartsWith("The operation has timed out"))
                        processInfo.AddProgressMessage("Failure Diagnosis: A call to the Online Apps Server timed out. Please try again later.");
                    if (e.Message.StartsWith("Unable to connect to the remote server") ||
                        e.Message.StartsWith("The request failed with HTTP status 404"))
                        processInfo.AddProgressMessage("Failure Diagnosis: Frond has lost contact with the Online Apps server.");
                }
                if (e is System.Web.Services.Protocols.SoapException)
                {
                    if (e.Message.IndexOf("The server is not operational.") > -1
                        && e.Message.IndexOf("at System.DirectoryServices.DirectoryEntry.Bind") > -1)
                        processInfo.AddProgressMessage("Failure Diagnosis: Frond could not log into the Online Apps server because the External Active Directory server is not responding");
                }
            }

            // save process messages to log table
            try
            {
                LogProcessMessages(processInfo);
            }
            catch (Exception e)
            {
                // any exceptions should be logged and then added to progress
                int errorTicketID = BonoboWebControls.BnbWebFormManager.LogErrorToDatabase(e);
                processInfo.AddProgressMessage(String.Format("Failed to log steps to tblEGatewayLog. Error: {0} (ErrorTicketID {1})", e.Message, errorTicketID));
            }

            // update dataset based on success or otherwise
            // final message only
            if (processInfo.Success)
            {
                importRow["Extra_Import"] = "Imported";
                importRow["Extra_ImportResults"] = processInfo.GetFinalMessage();
            }
            else
            {
                importRow["Extra_Import"] = "Failed";
                importRow["Extra_ImportResults"] = processInfo.GetFinalMessage();
            }

            BonoboWebControls.BnbWebFormManager.LogAction("ImportApplicationFormProcess", String.Join(", ", processInfo.ProgressMessages), null);
            BnbEngine.SessionManager.ClearObjectCache();
            // add the row into the in-session progress log
            // final message only
            Guid appFormID = new Guid(importRow["tblApplicationForm_ApplicationFormID"].ToString());
            string formRef = importRow["tblApplicationForm_FormReference"].ToString();
            BnbFormsProcessUtils.AddImportProcessHistory("AppImport",appFormID, formRef, DateTime.Now, processInfo.Success,
                false, processInfo.ApplicationID, processInfo.GetFinalMessage());
           
        }

        private static void InternalImportApplicationFormProcess(BnbProcessInfo processInfo, DataRow importRow, BnbQueryDataSet qds)
        {

            Guid appFormID = new Guid(importRow["tblApplicationForm_ApplicationFormID"].ToString());
            string formRef = importRow["tblApplicationForm_FormReference"].ToString();
            processInfo.ApplicationFormID = appFormID;
            processInfo.FormReference = formRef;
            processInfo.AddProgressMessage(String.Format("ImportApplicationFormProcess for ApplicationForm Ref {0}", formRef));

            string recBaseID = (string)importRow["tblApplicationForm_RecruitmentBaseID"];

            bool stage1Success = AppStage1GetImportLock(processInfo, appFormID, recBaseID);
            if (!stage1Success)
            {
                processInfo.Success = false;
                return;
            }

            BnbImportFormResults stage2Results = AppStage2ImportToMainDatabase(processInfo, importRow, qds);

            if (!stage2Results.Imported)
            {
                processInfo.Success = false;
                return;
            }

            bool stage3Success = AppStage3ChangeFormStatus(processInfo, appFormID, recBaseID);

            if (!stage3Success)
            {
                processInfo.Success = false;
                return;
            }

            // if here, it must have been ok
            processInfo.Success = true;
            // add final message
            processInfo.AddProgressMessage(String.Format("Application Form {0} imported successfully", formRef));
            
            return;

        }

        private static bool AppStage1GetImportLock(BnbProcessInfo processInfo, Guid appFormID, string recBaseID)
        {
            processInfo.AddProgressMessage("Calling Web Service to get Import Lock ...");
            bool success;
            string castanetUsername = ConfigurationSettings.AppSettings["CastanetAdminUsername"].ToString();
            string castanetPassword = ConfigurationSettings.AppSettings["CastanetAdminPassword"].ToString();


            if (recBaseID.Trim().ToUpper()== "IRE")
            {
                WsApplicationFormIreland.FrondIntegration fi = new WsApplicationFormIreland.FrondIntegration();
                if (System.Configuration.ConfigurationManager.AppSettings["WebServiceDefaultCredentials"] != null)
                {
                    try
                    {
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["WebServiceDefaultCredentials"]) == true)
                        {
                            fi.PreAuthenticate = true;
                            fi.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        }
                    }
                    catch
                    {
                        throw new ApplicationException("Please configure a valid 'WebServiceDefaultCredentials' in Appsettings");
                    }
                }
                else
                    throw new ApplicationException("Please configure 'WebServiceDefaultCredentials' in Appsettings");
                WsApplicationFormIreland.BnbWebServiceProcessInfo wsProcess = fi.GetApplicationImportLock(castanetUsername, castanetPassword,
                    appFormID);
                success = LogAppImportMessages(processInfo, wsProcess.Success, wsProcess.ProgressMessages, wsProcess.CastanetErrorTicketID);

                return success;
            }
            else
            {
                WsOnlineForms.FrondIntegration fi = new WsOnlineForms.FrondIntegration();
                if (System.Configuration.ConfigurationManager.AppSettings["WebServiceDefaultCredentials"] != null)
                {
                    try
                    {
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["WebServiceDefaultCredentials"]) == true)
                        {
                            fi.PreAuthenticate = true;
                            fi.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        }
                    }
                    catch
                    {
                        throw new ApplicationException("Please configure a valid 'WebServiceDefaultCredentials' in Appsettings");
                    }
                }
                else
                    throw new ApplicationException("Please configure 'WebServiceDefaultCredentials' in Appsettings");
                WsOnlineForms.BnbWebServiceProcessInfo wsProcess = fi.GetApplicationImportLock(castanetUsername, castanetPassword,
                    appFormID);
                success = LogAppImportMessages(processInfo, wsProcess.Success, wsProcess.ProgressMessages, wsProcess.CastanetErrorTicketID);

                return success;
            }

        }

        private static bool LogAppImportMessages(BnbProcessInfo processInfo, bool success, string[] progressMessages, int castanetErrorTicketID)
        {
            if (!success)
            {
                // add progress messages from web service into our own list:
                processInfo.AddProgressMessage("Web Service call to get Import Lock failed. Details follow:");
                bool alreadyImported = false;
                bool lockActive = false;
                foreach (string message in progressMessages)
                {
                    processInfo.AddProgressMessage(message);
                    if (message.IndexOf("may have been imported already") > -1)
                        alreadyImported = true;
                    if (message.IndexOf("ImportLock set to") > -1)
                        lockActive = true;
                }

                if (castanetErrorTicketID > 0)
                    processInfo.AddProgressMessage(String.Format("ErrorTicketID in ApplicationForm database: {0}", castanetErrorTicketID));

                // add final diagnostic message if reason for failure known
                if (alreadyImported)
                    processInfo.AddProgressMessage(String.Format("Failure Diagnosis: The Form appears to have been imported already."));
                if (lockActive)
                    processInfo.AddProgressMessage(String.Format("Failure Diagnosis: The Form is 'Import Locked' after a previous Import attempt. Please try again in about 5 minutes"));

                return false;
            }
            else
            {
                processInfo.AddProgressMessage("Import Lock obtained OK");
                return true;
            }

        }


        private static BnbImportFormResults AppStage2ImportToMainDatabase(BnbProcessInfo processInfo, DataRow importRow, BnbQueryDataSet qds)
        {
            
            bool disability = ConvertYesNoToBool(importRow, "Custom_HasDisability");
            bool contactVolunteeringOptions = ConvertYesNoToBool(importRow, "Custom_HasVolunteeringOptions");
            bool contactByEmail = ConvertYesNoToBool(importRow, "Custom_HasEmailSMS");
            //VAF-348
            bool pension = ConvertYesNoToBool(importRow, "custom_Pension");
            // work out source
            int heardAboutVSOID = (int)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage4_HeardFromVSOID"], -1);
            int heardAboutVSODetailID = (int)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage4_HeardFromVSODetailID"], -1);
            int heardAboutVSOFurtherDetailID = (int)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage4_HeardFromVSOFurtherDetailID"], -1);
            Guid mappedSourceID = BnbFormsProcessUtils.GetMappedSourceID(heardAboutVSOID, heardAboutVSODetailID, heardAboutVSOFurtherDetailID);
           

            processInfo.AddProgressMessage("Import App Form Data into main DB ...");
            //TPT Commented-change request dated(10-5-2011):addressline1 is non mandatory for jitolee i.e. for recruitment base id KEN
            string importAppAddressLine1 = "";
            string recBaseIDImport = (string)importRow["tblApplicationForm_RecruitmentBaseID"];
            if (recBaseIDImport.Trim().ToUpper() == "KEN")
            {
                importAppAddressLine1 = "N/A";
            }
            else
                importAppAddressLine1 = (string)importRow["tblRegistrationPage1_AddressLine1"];

            Guid individualID = new Guid(importRow["tblApplicationForm_MappedIndividualID"].ToString());
            Guid applicationID = new Guid(BnbQueryUtils.ReplaceDBNull(importRow["tblApplicationForm_MappedApplicationID"], Guid.Empty).ToString());
            BnbImportFormResults impResults = BnbImportFormUtils.SaveApplicant(individualID, applicationID,
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["lkuTitle_Description"], null),
                    (string)importRow["tblRegistrationPage1_FirstName"],
                    (string)importRow["tblRegistrationPage1_Surname"],
                    importAppAddressLine1,
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage1_AddressLine2"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage1_AddressLine3"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage1_PostalTown"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage1_County"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage1_PostCode"], null),
                    (string)importRow["Custom_AddressCountry"],
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage1_DaytimePhone"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage1_Email"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage1_FaxNumber"], null),
                    (DateTime)importRow["tblRegistrationPage1_DateOfBirth"],
                    (string)importRow["lkuNationality_Description"],
                    (DateTime)BnbQueryUtils.ReplaceDBNull(importRow["tblApplicationFormPage3_AvailableFrom"], CalculateAvailableFrom(importRow)),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["lkuGender_Description"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["lkuEthnicity_Description"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage4_EthnicityOther"], null),
                    disability,
                    contactVolunteeringOptions,
                    contactByEmail,
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["Custom_MaritalStatus"], null),
                    (DateTime)BnbQueryUtils.ReplaceDBNull(importRow["tblApplicationFormPage4_RelationshipSinceDate"], DateTime.MinValue),
                    -1,
                    mappedSourceID,
                    null,
                    pension,
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["tblRegistrationPage4_HeardFromVSOOther"], null),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["Custom_Language1"], null),
                    (int)BnbQueryUtils.ReplaceDBNull(importRow["tblApplicationFormPage2_Ability1"], -1),
                    (string)BnbQueryUtils.ReplaceDBNull(importRow["Custom_Language2"], null),
                    (int)BnbQueryUtils.ReplaceDBNull(importRow["tblApplicationFormPage2_Ability2"], -1));
            if (!impResults.Imported)
            {
                string failReason = "";
                if (impResults.Problems != null)
                {
                    failReason = "due to following validation messages: ";
                    foreach (BnbProblem prob in impResults.Problems)
                    {
                        failReason += prob.Message + " ";
                    }
                }
                if (impResults.ImportException != null)
                {
                    failReason = "due to an unexpected error: " + impResults.ImportException.Message;
                }
                processInfo.AddProgressMessage(String.Format("Failure Diagnosis: Saving the data to Frond DB failed {0}",
                    failReason));
                
                return impResults;
            }
            else
            {
                string message = String.Format("Updated Individual record {0}", impResults.IndividualRefNo);
                
                processInfo.ApplicationID = impResults.ApplicationID;
                processInfo.IndividualID = impResults.IndividualID;
                processInfo.AddProgressMessage(String.Format("Import to main DB OK. {0}", message));
                return impResults;
            }

        }

        private static bool AppStage3ChangeFormStatus(BnbProcessInfo processInfo, Guid appFormID, string recBaseID)
        {
            
            DateTime importDate = DateTime.Now;
            
            
            // call web service to update app form data
            string castanetUsername = ConfigurationManager.AppSettings["CastanetAdminUsername"].ToString();
            string castanetPassword = ConfigurationManager.AppSettings["CastanetAdminPassword"].ToString();

            processInfo.AddProgressMessage("Calling Web Service to update Form Data ...");

            if (recBaseID.Trim().ToUpper() == "IRE")
            {
                WsApplicationFormIreland.FrondIntegration fi = new WsApplicationFormIreland.FrondIntegration();
                if (System.Configuration.ConfigurationManager.AppSettings["WebServiceDefaultCredentials"] != null)
                {
                    try
                    {
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["WebServiceDefaultCredentials"]) == true)
                        {
                            fi.PreAuthenticate = true;
                            fi.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        }
                    }
                    catch
                    {
                        throw new ApplicationException("Please configure a valid 'WebServiceDefaultCredentials' in Appsettings");
                    }
                }
                else
                    throw new ApplicationException("Please configure 'WebServiceDefaultCredentials' in Appsettings");
                fi.Timeout = 30 * 1000;
                WsApplicationFormIreland.BnbWebServiceProcessInfo wsImportedInfo = fi.ApplicationFormImported(castanetUsername, castanetPassword,
                    appFormID, importDate);
                return LogAppStatusChangeMessages(processInfo, wsImportedInfo.Success, wsImportedInfo.ProgressMessages, wsImportedInfo.CastanetErrorTicketID);
            }
            else
            {
                WsOnlineForms.FrondIntegration fi = new WsOnlineForms.FrondIntegration();
                if (System.Configuration.ConfigurationManager.AppSettings["WebServiceDefaultCredentials"] != null)
                {
                    try
                    {
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["WebServiceDefaultCredentials"]) == true)
                        {
                            fi.PreAuthenticate = true;
                            fi.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        }
                    }
                    catch
                    {
                        throw new ApplicationException("Please configure a valid 'WebServiceDefaultCredentials' in Appsettings");
                    }
                }
                else
                    throw new ApplicationException("Please configure 'WebServiceDefaultCredentials' in Appsettings");
                fi.Timeout = 30 * 1000;
                WsOnlineForms.BnbWebServiceProcessInfo wsImportedInfo = fi.ApplicationFormImported(castanetUsername, castanetPassword,
                    appFormID, importDate);
                return LogAppStatusChangeMessages(processInfo, wsImportedInfo.Success, wsImportedInfo.ProgressMessages, wsImportedInfo.CastanetErrorTicketID);
            }
        }

        private static bool LogAppStatusChangeMessages(BnbProcessInfo processInfo, bool success, string[] progressMessages, int castanetErrorTicketID)
        {
            if (!success)
            {
                // add progress messages from web service into our own list:
                processInfo.AddProgressMessage("Web Service call failed. Details follow:");

                foreach (string message in progressMessages)
                    processInfo.AddProgressMessage(message);

                if (castanetErrorTicketID > 0)
                    processInfo.AddProgressMessage(String.Format("ErrorTicketID in ApplicationForm database: {0}", castanetErrorTicketID));

                return false;
            }
            else
            {
                processInfo.AddProgressMessage("Web Service called OK");
            }
            return true;
        }

        /// <summary>
        /// For use when an individual is invited directly to apply without filling in a Registration Form first
        /// </summary>
        /// <param name="individualID"></param>
        public static BnbProcessInfo InviteDirectApplicationProcess(Guid individualID)
        {
            // object to keep track of progress
            string recBaseID;
            if (BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID == BnbConst.RecruitmentBase_UK)
                recBaseID = "UK";
            else if (BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID == BnbConst.RecruitmentBase_IRELAND)
                recBaseID = "IRE";
            else if (BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID == BnbConst.RecruitmentBase_JITOLEE)
                recBaseID = "KEN";
            else if (BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID == BnbConst.RecruitmentBase_BAHAGINAN)
                recBaseID = "PHI";
            else if (BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID == BnbConst.RecruitmentBase_Netherlands)
                recBaseID = "NED";
            else if (BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID == BnbConst.RecruitmentBase_CUSO)
                recBaseID = "CUSO";
            else
                recBaseID = "UK";
            
            BnbProcessInfo processInfo = new BnbProcessInfo();
            try
            {
                InternalInviteDirectApplicationProcess(processInfo, individualID, recBaseID);
            }
            catch (Exception e)
            {
                // any exceptions should be logged and then added to progress
                int errorTicketID = BonoboWebControls.BnbWebFormManager.LogErrorToDatabase(e);
                processInfo.AddProgressMessage(String.Format("Error: {0} (ErrorTicketID {1})", e.Message, errorTicketID));
                processInfo.Success = false;

                // check for common communication failure errors
                if (e is WebException)
                {
                    if (e.Message.StartsWith("The operation has timed out"))
                        processInfo.AddProgressMessage("Failure Diagnosis: A call to the Online Apps Server timed out. Please try again later.");
                    if (e.Message.StartsWith("Unable to connect to the remote server") ||
                        e.Message.StartsWith("The request failed with HTTP status 404"))
                        processInfo.AddProgressMessage("Failure Diagnosis: Frond has lost contact with the Online Apps server.");

                }
            }

            // if we have an appformID then save process messages to log table
            if (processInfo.ApplicationFormID != Guid.Empty)
            {
                try
                {
                    LogProcessMessages(processInfo);
                }
                catch (Exception e)
                {
                    // any exceptions should be logged and then added to progress
                    int errorTicketID = BonoboWebControls.BnbWebFormManager.LogErrorToDatabase(e);
                    processInfo.AddProgressMessage(String.Format("Failed to log steps to tblEGatewayLog. Error: {0} (ErrorTicketID {1})", e.Message, errorTicketID));
                }
            }

            // log all messages to action log
            BonoboWebControls.BnbWebFormManager.LogAction("ImportApplicationFormProcess", String.Join(", ", processInfo.ProgressMessages), null);
            BnbEngine.SessionManager.ClearObjectCache();
            return processInfo;
            
        }

        private static void InternalInviteDirectApplicationProcess(BnbProcessInfo processInfo, Guid individualID, string recBaseID)
        {
            
            // get ref no
            BnbIndividual indv = BnbIndividual.Retrieve(individualID);
            string refNo = indv.RefNo;
            string email = indv.Email.ContactNumber;

            processInfo.AddProgressMessage(String.Format("InviteDirectApplicationProcess for Individual {0}", refNo));

            // call stage with with createNewApplicationForm = true
            bool stage3Success = RegStage3ConfigureUserAccount(processInfo, true, Guid.Empty,
                individualID, refNo, Guid.Empty, true, recBaseID);

            if (!stage3Success)
            {
                processInfo.Success = false;
                return;
            }

            processInfo.IndividualID = individualID;
            // no pack is sent, but this ID affects what email they get
            
            bool stage4Success = RegStage4SendInviteOrDeclineEmail(processInfo, 0, "DIRECT", email,
                processInfo.VsoOnlineAccountExists, processInfo.ADUsername, processInfo.ApplicationFormUserID, true, recBaseID);

            // if here, it must have been ok
            processInfo.Success = true;
            // add final message
            processInfo.AddProgressMessage(String.Format("Direct Invite for {0} completed successfully", refNo));
           
            return;

        }

        /// <summary>
        /// saves the progress messages in the BnbProcessInfo object
        /// to the tblEGatewayLog table.
        /// </summary>
        /// <param name="processInfo"></param>
        private static void LogProcessMessages(BnbProcessInfo processInfo)
        {
            int messageCount = processInfo.ProgressMessages.Length;
            DateTime processTimestamp = DateTime.Now;
            BnbSessionInfo seshInfo = BnbEngine.SessionManager.GetSessionInfo();
            BnbEditManager logEm = new BnbEditManager();
            int stepNo = 1;
            foreach (string message in processInfo.ProgressMessages)
            {
                BnbEGatewayLog logEntry = new BnbEGatewayLog(true);
                logEntry.RegisterForEdit(logEm);
                logEntry.ApplicationFormID = processInfo.ApplicationFormID;
                logEntry.FormReference = processInfo.FormReference;
                logEntry.ProcessTimestamp = processTimestamp;
                logEntry.StepNo = stepNo;
                if (stepNo == messageCount)
                    logEntry.IsFinalStep = true;
                logEntry.ProgressMessage = message;
                logEntry.UserId = seshInfo.UserID;
                logEntry.SessionID = seshInfo.BonoboSessionID;

                stepNo += 1;
            }
            // now save them all
            try
            {
                logEm.SaveChanges();
            }
            catch (BnbProblemException pe)
            {
                logEm.EditCompleted();
                processInfo.AddProgressMessage(String.Format("Could not save log to tblEGatewayLog due to validation messages {0}", pe.Message));


            }


        }

        /// <summary>
        /// an object to represnt the progress of a process
        /// contains success flag and an array of progress messages
        /// </summary>
        public class BnbProcessInfo
        {
            public bool Success;
            public string[] ProgressMessages;
            public int CastanetErrorTicketID;
            public Guid ApplicationID;
            public Guid IndividualID;
            public Guid ApplicationFormID;
            public string FormReference;
            public bool VsoOnlineAccountExists;
            public Guid ApplicationFormUserID;
            public string ADUsername;

            private ArrayList m_progressArray = new ArrayList();

            public void AddProgressMessage(string message)
            {
                m_progressArray.Add(message);
                // copy to array
                this.ProgressMessages = new string[m_progressArray.Count];
                m_progressArray.CopyTo(this.ProgressMessages);
            }

            public string GetFinalMessage()
            {
                if (this.ProgressMessages.Length == 0)
                    return null;

                return (this.ProgressMessages[this.ProgressMessages.Length -1]);
            }

            
        }

        

        public class BnbCachedDataSet
        {
            public BnbQueryDataSet QueryDataSet;
            public DateTime FetchedDate;
            public int BatchSize;

        }


    }
}
