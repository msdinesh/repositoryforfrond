<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WizardImportRegistration.aspx.cs" Inherits="Frond.EditPages.EGateway.WizardImportRegistration" %>

<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.DataGrids"
    TagPrefix="bnbdatagrid" %>

<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../../UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Import Registration Form Wizard</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<DIV class="wizardheader" ms_positioning="FlowLayout">
					<H3><IMG class="icon24" src="../../Images/wizard24silver.gif">Import Registration Form Wizard</H3>
				</DIV>
				<asp:panel id="panelZero" runat="server" CssClass="wizardpanel">
                    <p><i>This wizard will help you import a Completed Registration Form that has possible
                    matching records already in the database.</i></p>
                    
				<table class="fieldtable">
				<tr><td class="label">Registration Form Ref</td><td style="width: 7px">
                    <asp:Label ID="uxRegFormReferenceLabel" runat="server"></asp:Label></td><td class="label">Reg Completed Date</td><td>
                    <asp:Label ID="uxRegCompletedDateLabel" runat="server"></asp:Label></td></tr>
				<tr><td class="label">Forename</td><td style="width: 7px">
                    <asp:Label ID="uxForenameLabel" runat="server"></asp:Label></td><td class="label">Surname</td><td>
                    <asp:Label ID="uxSurnameLabel" runat="server"></asp:Label></td></tr>
				<tr><td class="label">Date of Birth</td><td style="width: 7px">
                    <asp:Label ID="uxDateOfBirthLabel" runat="server"></asp:Label></td><td class="label">Email</td><td>
                    <asp:Label ID="uxEmailLabel" runat="server"></asp:Label></td></tr>
				<tr><td class="label">Address Line 1</td><td style="width: 7px">
                    <asp:Label ID="uxAddressLine1Label" runat="server"></asp:Label></td><td class="label">Address Country</td><td>
                    <asp:Label ID="uxAddressCountryLabel" runat="server"></asp:Label></td></tr>
				
				</table>
				<p>The following matching record were found in the database. Please select one, or if none of them match, check the 'new record' checkbox:</p>
                    <p>
                        <bnbdatagrid:BnbSmartGridWithControls ID="uxMatchingGrid" runat="server" OnColumnButtonClicked="uxMatchingGrid_ColumnButtonClicked" />
                        </p>
                    <p>
                        <asp:Label ID="uxMappingSummary" runat="server"></asp:Label>&nbsp;</p>
                    <p>
                        <asp:CheckBox ID="uxNewRecordCheckbox" runat="server" Text="Create a new record" />&nbsp;</p>
                    <p>
                        <asp:Label ID="uxPanelZeroFeedback" runat="server" CssClass="feedback"></asp:Label>&nbsp;</p>
								</asp:panel>
                &nbsp;
				<P></P>
				<P><uc1:wizardbuttons id="wizardButtons" runat="server"></uc1:wizardbuttons></P>
				<P><asp:TextBox id="txtHiddenReferrer" runat="server" Visible="False"></asp:TextBox><asp:textbox ID="uxHiddenApplicationFormID" visible="False" runat="server"></asp:textbox>
                    <asp:TextBox ID="uxHiddenIndividualID" runat="server" Visible="False"></asp:TextBox></P>
				<p></p>
			</div>
		</form>
	</body>
</HTML>

