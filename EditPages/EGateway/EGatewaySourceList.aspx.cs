using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using BonoboEngine;
using BonoboEngine.Query;

namespace Frond.EditPages.EGateway
{
    public partial class EGatewaySourceList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                titleBar.TitleText = "E-Gateway Source List";
                doShowSourceList();
            }
        }

        private void doShowSourceList()
        {
            DataSet heardAboutVSODataSet = GetHeardAboutVSOList();

            DataTable heardAboutVSOTable = heardAboutVSODataSet.Tables["results"];
            // add some columns for the source info
            heardAboutVSOTable.Columns.Add("SourceGroup", typeof(string));
            heardAboutVSOTable.Columns.Add("SourceType", typeof(string));
            heardAboutVSOTable.Columns.Add("Source", typeof(string));
            heardAboutVSOTable.Columns.Add("SourceID", typeof(Guid));
            heardAboutVSOTable.Columns.Add("Notes", typeof(string));
            // set some captions (for the excel export)
            heardAboutVSOTable.Columns["lkuHeardAboutVSO_HeardAboutVSOID"].Caption = "HeardAboutVSOID";
            heardAboutVSOTable.Columns["lkuHeardAboutVSODetail_HeardAboutVSODetailID"].Caption = "HeardAboutVSODetailID";
            heardAboutVSOTable.Columns["lkuHeardAboutVSOFurtherDetail_HeardAboutVSOFurtherDetailID"].Caption = "HeardAboutVSOFurtherDetailID";
            heardAboutVSOTable.Columns["lkuHeardAboutVSO_Description"].Caption = "Heard About VSO Level 1";
            heardAboutVSOTable.Columns["lkuHeardAboutVSODetail_Description"].Caption = "Heard About VSO Level 2";
            heardAboutVSOTable.Columns["lkuHeardAboutVSOFurtherDetail_Description"].Caption = "Heard About VSO Level 3";
            heardAboutVSOTable.Columns["lkuHeardAboutVSO_IsOther"].Caption = "Level 1 IsOther";
            heardAboutVSOTable.Columns["lkuHeardAboutVSODetail_IsOther"].Caption = "Level 2 IsOther";
            heardAboutVSOTable.Columns["lkuHeardAboutVSOFurtherDetail_IsOther"].Caption = "Level 3 IsOther";

            // get source lookups
            BnbLookupDataTable sourceLookup = BnbEngine.LookupManager.GetLookup("vlkuBonoboSource");
            BnbLookupDataTable sourceTypeLookup = BnbEngine.LookupManager.GetLookup("vlkuBonoboSourceType");
            BnbLookupDataTable sourceGroupLookup = BnbEngine.LookupManager.GetLookup("vlkuBonoboSourceGroup");

            // loop through and work out mapping for each row
            foreach (DataRow rowLoop in heardAboutVSOTable.Rows)
            {
                int level1ID = (int)rowLoop["lkuHeardAboutVSO_HeardAboutVSOID"];
                int level2ID = (int)rowLoop["lkuHeardAboutVSODetail_HeardAboutVSODetailID"];
                int level3ID = (int)rowLoop["lkuHeardAboutVSOFurtherDetail_HeardAboutVSOFurtherDetailID"];
                Guid mappedSourceID = Guid.Empty;
                try
                {
                    mappedSourceID = BnbFormsProcessUtils.GetMappedSourceID(level1ID, level2ID, level3ID);
                }
                catch (Exception e)
                {
                    BonoboWebControls.BnbWebFormManager.LogErrorToDatabase(e);
                    rowLoop["Notes"] = e.Message;
                    continue;
                }
                rowLoop["SourceID"] = mappedSourceID;
                BnbLookupRow sourceRow = sourceLookup.FindByID(mappedSourceID);
                if (sourceRow == null)
                {
                    rowLoop["Notes"] = String.Format("No Source found in tblSource for SourceID {0}", mappedSourceID.ToString());
                    continue;
                }
                rowLoop["Source"] = sourceRow.Description;
                // get the source type
                Guid sourceTypeID = new Guid(sourceRow["SourceTypeGUID"].ToString());
                BnbLookupRow sourceTypeRow = sourceTypeLookup.FindByID(sourceTypeID);
                if (sourceTypeRow == null)
                    continue;
                rowLoop["SourceType"] = sourceTypeRow.Description;
                // get the source group
                Guid sourceGroupID = new Guid(sourceTypeRow["SourceGroupGUID"].ToString());
                BnbLookupRow sourceGroupRow = sourceGroupLookup.FindByID(sourceGroupID);
                if (sourceGroupRow == null)
                    continue;
                rowLoop["SourceGroup"] = sourceGroupRow.Description;
                
            }
            // display the data as a table
            uxSourceListTable.Text = this.SourceListAsTable(heardAboutVSOTable);
            // store the data table incase they want to export it
            Session["EGateway_SourceList"] = heardAboutVSOTable;
        }

        private string SourceListAsTable(DataTable sourceListTable)
        {
            StringWriter sw = new StringWriter();
            HtmlTextWriter writer = new HtmlTextWriter(sw);

            writer.AddAttribute("class", "datagrid");
            writer.RenderBeginTag("table");
            // render columns headers
            writer.AddAttribute("class","datagridheader");
            writer.RenderBeginTag("tr");
            writer.RenderBeginTag("td");
            writer.Write("Heard About VSO Level 1");
            writer.RenderEndTag();
            writer.RenderBeginTag("td");
            writer.Write("Heard About VSO Level 2");
            writer.RenderEndTag();
            writer.RenderBeginTag("td");
            writer.Write("Heard About VSO Level 3");
            writer.RenderEndTag();
            writer.RenderBeginTag("td");
            writer.Write("Source Group");
            writer.RenderEndTag();
            writer.RenderBeginTag("td");
            writer.Write("Source Type");
            writer.RenderEndTag();
            writer.RenderBeginTag("td");
            writer.Write("Source");
            writer.RenderEndTag();
            writer.RenderBeginTag("td");
            writer.Write("Notes");
            writer.RenderEndTag();
            writer.RenderEndTag();

            // render all the rows
            foreach(DataRow rowLoop in sourceListTable.Rows)
            {
                writer.RenderBeginTag("tr");
                if ((bool)rowLoop["lkuHeardAboutVSO_IsOther"])
                    writer.AddStyleAttribute(HtmlTextWriterStyle.FontWeight , "Bold");
                writer.RenderBeginTag("td");
                writer.Write(rowLoop["lkuHeardAboutVSO_Description"].ToString());
                writer.RenderEndTag();
                if ((bool)rowLoop["lkuHeardAboutVSODetail_IsOther"])
                    writer.AddStyleAttribute(HtmlTextWriterStyle.FontWeight, "Bold");
                writer.RenderBeginTag("td");
                writer.Write(rowLoop["lkuHeardAboutVSODetail_Description"].ToString());
                writer.RenderEndTag();
                if ((bool)rowLoop["lkuHeardAboutVSOFurtherDetail_IsOther"])
                    writer.AddStyleAttribute(HtmlTextWriterStyle.FontWeight, "Bold");
                writer.RenderBeginTag("td");
                writer.Write(rowLoop["lkuHeardAboutVSOFurtherDetail_Description"].ToString());
                writer.RenderEndTag();
                writer.RenderBeginTag("td");
                writer.Write(rowLoop["SourceGroup"].ToString());
                writer.RenderEndTag();
                writer.RenderBeginTag("td");
                writer.Write(rowLoop["SourceType"].ToString());
                writer.RenderEndTag();
                writer.RenderBeginTag("td");
                writer.Write(rowLoop["Source"].ToString());
                writer.RenderEndTag();
                writer.RenderBeginTag("td");
                writer.Write(rowLoop["Notes"].ToString());
                writer.RenderEndTag();
                writer.RenderEndTag();
            }

            return sw.ToString();
        }

        private static DataSet GetHeardAboutVSOList()
        {
            // call the web service
            string castanetUsername = ConfigurationSettings.AppSettings["CastanetAdminUsername"].ToString();
            string castanetPassword = ConfigurationSettings.AppSettings["CastanetAdminPassword"].ToString();

            WsOnlineForms.FrondIntegration fi = new WsOnlineForms.FrondIntegration();
            if (System.Configuration.ConfigurationManager.AppSettings["WebServiceDefaultCredentials"] != null)
            {
                try
                {
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["WebServiceDefaultCredentials"]) == true)
                    {
                        fi.PreAuthenticate = true;
                        fi.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    }
                }
                catch
                {
                    throw new ApplicationException("Please configure a valid 'WebServiceDefaultCredentials' in Appsettings");
                }
            }
            else
                throw new ApplicationException("Please configure 'WebServiceDefaultCredentials' in Appsettings");
            fi.Timeout = 30 * 1000;
            DataSet heardAboutVSODataSet = fi.GetHeardAboutVSOCombinedList(castanetUsername, castanetPassword);
            return heardAboutVSODataSet;
        }

        protected void uxExportSourceListButton_Click(object sender, EventArgs e)
        {
            DataTable heardAboutVSOTable = (DataTable)Session["EGateway_SourceList"];
            BnbExcelExporter.SetResponseHeadersForExcel(this.Response, "EGatewaySourceList.xls");
            string[] headerLines = new string[] {"E-Gateway Source Mapping List","This shows how 'Heard About VSO' picklists from the online applications forms map to Sources in Frond",
                        String.Format("Exported at {0}", DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss"))};
            BnbExcelExporter.GenerateExcel(this.Response.OutputStream, heardAboutVSOTable, headerLines);
            this.Page.Response.End();
        }


    }
}
