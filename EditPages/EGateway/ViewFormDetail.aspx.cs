using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboWebControls;

namespace Frond.EditPages.EGateway
{
    public partial class ViewFormDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                //BnbWorkareaManager.CheckPageAccess(this);
                this.RenderFormDetail();
                titleBar.TitleText = "View Form Detail";
            }
        }

        public void RenderFormDetail()
        {
            string source = this.Request.QueryString["DataSet"];
            string appFormIDString = this.Request.QueryString["BnbApplicationForm"];
            BnbQueryDataSet qds = null;
            switch (source)
            {
                case "RegForm":
                    qds = BnbFormsProcessUtils.StoredRegistrationFormData.QueryDataSet;
                    uxBackHyperlink.NavigateUrl = "ImportRegistrationForm.aspx";
                    uxBackHyperlink.Text = "Back to Import Registration Forms page";
                    break;
                case "AppForm":
                    qds = BnbFormsProcessUtils.StoredApplicationFormData.QueryDataSet;
                    uxBackHyperlink.NavigateUrl = "ImportApplicationForm.aspx";
                    uxBackHyperlink.Text = "Back to Import Application Forms page";
                    break;
                case "SearchForm":
                    qds = BnbFormsProcessUtils.StoredSearchFormData.QueryDataSet;
                    uxBackHyperlink.NavigateUrl = "SearchForms.aspx";
                    uxBackHyperlink.Text = "Back to Search Forms page";
                    break;
            }
            if (qds == null)
                throw new ApplicationException("ViewFormDetail.aspx could not find data because no known DataSet querystring parameter specified");
            
            // find row
            DataRow[] findRows = qds.Tables["results"].Select(String.Format("tblApplicationForm_ApplicationFormID = '{0}'", appFormIDString));
            if (findRows.Length == 1)
            {
                DataRow displayRow = findRows[0];
                uxDetailTable.Text = this.DataRowAsTable(displayRow, qds);
            }


        }

        private string DataRowAsTable(DataRow displayRow, BnbQueryDataSet qds)
        {
            StringWriter sw = new StringWriter();
            HtmlTextWriter writer = new HtmlTextWriter(sw);

            writer.AddAttribute("class", "fieldtable");
            writer.RenderBeginTag("table");
            foreach (DataColumn colLoop in displayRow.Table.Columns)
            {
                // dont display guids
                if (colLoop.DataType == typeof(Guid))
                    continue;
                // dont display Extra_ columns
                if (colLoop.ColumnName.StartsWith("Extra_"))
                    continue;
                // find corresponding colinfo in qds
                BnbQueryDataSet.ColumnInfoRow colInfo = (BnbQueryDataSet.ColumnInfoRow)qds.ColumnInfo.Rows.Find(colLoop.ColumnName);
                if (colInfo == null) 
                    continue;
                // dont display querykey or foreign keys
                if (colInfo.IsQueryKey || colInfo.IsForeignKey)
                    continue;
                
                // get as string
                string cellData = "";
                if (displayRow[colLoop] != DBNull.Value &&
                        displayRow[colLoop] != null)
                {
                    cellData = displayRow[colLoop].ToString();
                }

                // render as two cells
                writer.RenderBeginTag("tr");
                writer.AddAttribute("class", "label");
                writer.RenderBeginTag("td");
                writer.Write(HttpUtility.HtmlEncode(colInfo.FieldUserAlias));
                writer.RenderEndTag();
                writer.RenderBeginTag("td");
                writer.Write(HttpUtility.HtmlEncode(cellData));
                writer.RenderEndTag();
                writer.RenderEndTag();

            }


            return sw.ToString();
        }
    }
}
