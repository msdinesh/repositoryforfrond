<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WizardDirectApplicationInvite.aspx.cs" Inherits="Frond.EditPages.EGateway.WizardDirectApplicationInvite" %>

<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.CriteriaControls"
    TagPrefix="bnbcriteriacontrol" %>

<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.DataGrids"
    TagPrefix="bnbdatagrid" %>

<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../../UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Direct Application Invite Wizard</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server" ></uc1:MenuBar></div>
			<div id="baseContent">
				<DIV class="wizardheader" ms_positioning="FlowLayout">
					<H3><IMG class="icon24" src="../../Images/wizard24silver.gif">Direct Application Invite Wizard</H3>
				</DIV>
				<asp:panel id="panelZero" runat="server" CssClass="wizardpanel">
                    <p><i>This wizard can be used to invite someone (by email) to fill in the Application Form directly, without first going 
                    through the Registration Form stage.</i></p>
                <p>Please select the Individual you wish to Invite.</p>
                <p>(if they are not in the database, create a record for them using the <a href="../WizardNewIndividual.aspx">New Individual Wizard</a>)</p>
                    <p>
                        <bnbcriteriacontrol:BnbQueryBuilderComposite ID="uxQueryBuilderComposite" runat="server" AllowCustomisation="False">
                        </bnbcriteriacontrol:BnbQueryBuilderComposite></p>
                        <p><asp:Button ID="uxFindButton" runat="server" Text="Find" OnClick="uxFindButton_Click" />&nbsp;</p>
                    <p>
                        <bnbdatagrid:BnbSmartGridWithControls ID="uxResultsSmartGrid" runat="server" />
                        </p>
                    <p>
                        <asp:Label ID="uxPanelZeroFeedback" runat="server" CssClass="feedback"></asp:Label>&nbsp;</p>
								</asp:panel>
                <br />
                <asp:Panel ID="panelOne" runat="server" CssClass="wizardpanel" >
                    <p>Selected Individual:</p>
                    <table class="fieldtable">
                    <tr><td class="label">Ref No</td><td >
                        <asp:Label ID="uxChosenRefNoLabel" runat="server"></asp:Label></td></tr>
                    <tr><td class="label">Name</td><td >
                        <asp:Label ID="uxChosenNameLabel" runat="server"></asp:Label></td></tr>
                    <tr><td class="label">Email</td><td >
                        <asp:Label ID="uxChosenEmailLabel" runat="server"></asp:Label></td></tr>
                        <tr>
                            <td class="label">
                                Lead Skill</td>
                            <td>
                                <asp:Label ID="uxLeadSkill" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                    <br />
                    Please press the Invite button to send the Invitation email:<br />
                    <br />
                    <asp:Button ID="uxInviteButton" runat="server" OnClick="uxInviteButton_Click" Text="Invite" /><br />
                    <br />
                    <asp:Label ID="uxInviteProgress" runat="server"></asp:Label><br />
                    <br />
                    <asp:Label ID="uxFinishSummary" runat="server"></asp:Label><br />
                    <br />
                    <asp:Panel ID="uxSkillAdvicePanel" runat="server" Visible="false">
                        <p><b>The Individual does not have any Skills defined.</b> A Skill will need to be added to their record before their completed Application Form is imported.</p>
                        <p>Click 
                            <asp:HyperLink ID="uxIndividualLink" runat="server" Text="here"></asp:HyperLink> to go to their record and add a Skill.</p>
                    </asp:Panel>
                    <br />
                    <br />
                    <asp:Label ID="uxPanelOneFeedback" runat="server" CssClass="feedback"></asp:Label>
                    <asp:Panel ID="uxTestModeEmailPanel" runat="server" width="500px" Visible="False" BorderStyle="Dashed" BorderWidth="1px" >
                    <p><b>Test Mode Emailing</b></p>
                    <asp:Panel ID="uxTestModeEmailOn" runat="server" width="480px">
                    <p>Enabled. To avoid accidental emailing of real people during testing, all invite/decline emails will be sent to: 
                        <asp:Label ID="uxTestModeEmailToLabel" runat="server"></asp:Label></p>
                    <p>To disable, go to the Import Registration Form page.
                         </asp:Panel>
                        <asp:Panel ID="uxTestModeEmailOff" runat="server" width="480px" Visible="False">
                        <p>Currently Disabled - Emails will go to the normal addresses.</p>
                        <p>To re-enable, go to the Import Registration Form page.</p>
                        </asp:Panel>
                    </asp:Panel>
                    </asp:Panel>
                &nbsp;&nbsp;
				<P></P>
				<P><uc1:wizardbuttons id="wizardButtons" runat="server"></uc1:wizardbuttons></P>
				<P><asp:TextBox id="txtHiddenReferrer" runat="server" Visible="False"></asp:TextBox>
                    <asp:TextBox ID="uxHiddenIndividualID" runat="server" Visible="False"></asp:TextBox></P>
				<p></p>
			</div>
		</form>
	</body>
</HTML>

