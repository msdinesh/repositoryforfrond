<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EGatewayUtil.aspx.cs" Inherits="Frond.EditPages.EGateway.EGatewayUtil" %>

<%@ Register Src="../../UserControls/WaitMessagePanel.ascx" TagName="WaitMessagePanel"
    TagPrefix="uc2" %>

<%@ Register TagPrefix="uc1" TagName="Title" Src="../../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
  <HEAD>
		<title>E-Gateway Homepage</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server" ></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../../Images/batchprocess24silver.gif"></uc1:title>
				<div id="importOptionsDiv">
				<p>E-Gateway is a utility for managing Online Applications. From this homepage you can view and import Registration Forms and Application Forms and search previously imported Forms.</p>
				<table class="fieldtable">
				<tr><td class="label">Connection to Application Form</td><td><asp:Label ID="uxWsResponding" runat="server" Text=""></asp:Label></td></tr>
				<tr><td class="label">Application Form URL</td><td><asp:Label ID="uxWsApplicationFormUrl" runat="server" Text=""></asp:Label></td></tr>
				<tr><td></td><td>
                    <asp:Label ID="uxLiveModeWarning" runat="server" CssClass="feedback"></asp:Label></td></tr>
				<tr><td class="label">Completed Registration Forms</td><td><asp:Label ID="uxCompletedRegForms" runat="server" Text=""></asp:Label></td></tr>
				<tr><td class="label">Last Reg Forms Import</td><td><asp:Label ID="uxLastRegFormImport" runat="server" Text=""></asp:Label></td></tr>
				<tr id ="trRecruitmentBaseForReg" runat="server"><td class="label">Recruitment Base</td>
				<td> <asp:DropDownList CssClass="lookupcontrol" runat="server" ID="cmbRecruitmentBaseReg"></asp:DropDownList></td></tr>
				<tr><td style="height: 25px"></td><td style="height: 25px">
                    <p><asp:LinkButton ID="uxFetchRegFormsLink" runat="server" OnClick="uxFetchRegFormsLink_Click">Fetch Reg Forms</asp:LinkButton></p>
                    <p>
                        <asp:LinkButton ID="uxFetchRegFormsSmallerBatchLink" runat="server" OnClick="uxFetchRegFormsSmallerBatchLink_Click"
                            Visible="False">Fetch x Reg Forms</asp:LinkButton>&nbsp;</p></td></tr>
				<tr><td></td><td></td></tr>
				<tr><td class="label">Completed Application Forms</td><td><asp:Label ID="uxCompletedAppForms" runat="server" Text=""></asp:Label></td></tr>
				<tr><td class="label">Last App Forms Import</td><td><asp:Label ID="uxLastAppFormsImport" runat="server" Text=""></asp:Label></td></tr>
				<tr id ="trRecruitmentBaseForApp" runat="server"><td class="label">Recruitment Base</td><td><asp:DropDownList CssClass="lookupcontrol" runat="server" ID="cmbRecruitmentBaseApp"></asp:DropDownList></td></tr>
				<tr><td></td><td><p>
                    <asp:LinkButton ID="uxFetchAppFormsLink" runat="server" OnClick="uxFetchAppFormsLink_Click">Fetch App Forms</asp:LinkButton></p></td></tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Search Imported Forms
                        </td>
                        <td>
                        <a href="SearchForms.aspx">Search Page</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Invite someone directly<br />
                            to the Application Stage</td>
                        <td>
                        <a href="WizardDirectApplicationInvite.aspx">Direct Application Invite Wizard</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            View the Source Mapping List</td>
                        <td>
                        <a href="EGatewaySourceList.aspx">E-Gateway Source List</a>
                        </td>
                    </tr>
				</table>
                    <asp:Panel ID="uxTestUtilitiesPanel" runat="server" Width="100%" Visible="False">
                    <table class="fieldtable">
                    <tr><td></td><td></td></tr>
				<tr><td class="label">Testing Utilities</td><td><a href="testutilities.aspx">Test Utilities Page</a></td></tr>
				</table> 
                    </asp:Panel>
				</div>
                <uc2:WaitMessagePanel ID="uxWaitMessagePanel" runat="server" DivToHide="importOptionsDiv" />
                
            </div>
		</form>
	</body>
</HTML>
