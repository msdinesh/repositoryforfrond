<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewEGatewayLog.aspx.cs" Inherits="Frond.EditPages.EGateway.ViewEGatewayLog" %>


<%@ Register TagPrefix="uc1" TagName="Title" Src="../../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="NavigatePanel" Src="../../UserControls/NavigatePanel.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
  <HEAD>
		<title>View E-Gateway Log</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../../Images/batchprocess24silver.gif"></uc1:title>
                <p><a href="EGatewayUtil.aspx">E-Gateway Homepage</a></p>
                <p><asp:HyperLink ID="uxBackHyperlink" runat="server"></asp:HyperLink></p>
                <p>This page shows the import log for a Registration/Application Form</p>
                <p>
                    <asp:Button ID="uxToggleFinalSteps" runat="server" Text="Show All Steps" OnClick="uxToggleFinalSteps_Click" />&nbsp;</p>
                <table class="fieldtable">
                <tr><td class="label">Form Ref</td><td><asp:Label ID="uxFormRefLabel" runat="server" Text=""></asp:Label></td></tr>
                <tr><td class="label">Forename</td><td><asp:Label ID="uxForenameLabel" runat="server" Text=""></asp:Label></td></tr>
                <tr><td class="label">Surname</td><td><asp:Label ID="uxSurnameLabel" runat="server" Text=""></asp:Label></td></tr>
                </table>
                <br />
                <asp:Label ID="uxLogTable" runat="server" Text=""></asp:Label><br />
                <br />
                <asp:TextBox ID="uxHiddenApplicationFormID" runat="server" Visible="False"></asp:TextBox>
                <asp:TextBox ID="uxHiddenShowFinalOnly" runat="server" Visible="False"></asp:TextBox></div>
		</form>
	</body>
</HTML>