using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboDomainObjects;
using BonoboEngine;
using BonoboEngine.Query;

namespace Frond.EditPages.EGateway
{
    public partial class WizardImportRegistration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BnbEngine.SessionManager.ClearObjectCache();
            wizardButtons.AddPanel(panelZero);
            wizardButtons.CancelWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
            wizardButtons.FinishWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
            if (!this.IsPostBack)
            {
                uxHiddenApplicationFormID.Text = Request.QueryString["BnbApplicationForm"];
                this.SetupPanelZero();
            }
        }

        void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            uxPanelZeroFeedback.Text = "";
            if (uxHiddenIndividualID.Text == "" && uxNewRecordCheckbox.Checked == false)
            {
                uxPanelZeroFeedback.Text = "Please select an existing record or check the 'create a new record' checkbox";
                return;
            }
            Guid matchedIndividualID = Guid.Empty;
            if (uxHiddenIndividualID.Text != "")
                matchedIndividualID = new Guid(uxHiddenIndividualID.Text);

            this.ImportRecordAfterWizard(matchedIndividualID);

            // go back to page
            Response.Redirect("ImportRegistrationForm.aspx");
        }

        /// <summary>
        /// Imports the form after Finish is pressed on the Wizard
        /// matchedIndividualId should be an empty guid if the user decided to create a new record
        /// </summary>
        /// <param name="matchedIndividualID"></param>
        private void ImportRecordAfterWizard(Guid matchedIndividualID)
        {
            // get data from session data set and find the right row
            BnbQueryDataSet qds = BnbFormsProcessUtils.StoredRegistrationFormData.QueryDataSet;
            DataRow[] findRows = qds.Tables["results"].Select(String.Format("tblApplicationForm_ApplicationFormID = '{0}'", uxHiddenApplicationFormID.Text));
            if (findRows.Length == 0)
                throw new ApplicationException(String.Format("Could not find ApplicationFormID {0} in session-stored dataset", uxHiddenApplicationFormID.Text));
            DataRow importRow = findRows[0];

            // call the utility method to do the import
            BnbFormsProcessUtils.ImportRegistrationFormProcess(matchedIndividualID, importRow, qds);


        }

        void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            Response.Redirect("ImportRegistrationForm.aspx");
        }

        

        private void SetupPanelZero()
        {
            // get data from session data set and find the right row
            BnbQueryDataSet qds = BnbFormsProcessUtils.StoredRegistrationFormData.QueryDataSet;
            DataRow[] findRows = qds.Tables["results"].Select(String.Format("tblApplicationForm_ApplicationFormID = '{0}'", uxHiddenApplicationFormID.Text));
            if (findRows.Length == 0)
                throw new ApplicationException(String.Format("Could not find ApplicationFormID {0} in session-stored dataset", uxHiddenApplicationFormID.Text));
            DataRow importRow = findRows[0];

            // fill in labels
            uxRegFormReferenceLabel.Text = importRow["tblApplicationForm_FormReference"].ToString();
            uxRegCompletedDateLabel.Text = ((DateTime)importRow["tblApplicationForm_RegistrationCompletedDate"]).ToString("dd/MMM/yyyy");
            uxForenameLabel.Text = importRow["tblRegistrationPage1_FirstName"].ToString();
            uxSurnameLabel.Text = importRow["tblRegistrationPage1_Surname"].ToString();
            uxDateOfBirthLabel.Text = ((DateTime)importRow["tblRegistrationPage1_DateOfBirth"]).ToString("dd/MMM/yyyy");
            uxEmailLabel.Text = importRow["tblRegistrationPage1_Email"].ToString();
            uxAddressLine1Label.Text = importRow["tblRegistrationPage1_AddressLine1"].ToString();
            uxAddressCountryLabel.Text = importRow["Custom_AddressCountry"].ToString();

            // get duplicates data
            BnbQueryDataSet dupsDataSet = BnbFormsProcessUtils.CheckForDuplicates(uxEmailLabel.Text, uxSurnameLabel.Text, uxForenameLabel.Text);
            uxMatchingGrid.ClearSettings();
            uxMatchingGrid.QueryDataSet = dupsDataSet;
            uxMatchingGrid.AddControlColumnButton("tblIndividualKeyInfo_IndividualID", "Select", null, null);
            uxMatchingGrid.ColumnDisplayList = "tblIndividualKeyInfo_IndividualID, tblIndividualKeyInfo_old_indv_id, tblIndividualKeyInfo_Forename, tblIndividualKeyInfo_Surname, Custom_Email, tblIndividualKeyInfo_DateOfBirth, tblAddress_AddressLine1, lkuCountry_Description";
            uxMatchingGrid.Visible = true;
        }

        protected void uxMatchingGrid_ColumnButtonClicked(object sender, EventArgs e, string columnName, string rowID)
        {
            if (columnName == "tblIndividualKeyInfo_IndividualID")
            {
                Guid individualID = new Guid(rowID);
                this.MatchSelected(individualID);
            }
        }

        private void MatchSelected(Guid individualID)
        {
            uxHiddenIndividualID.Text = individualID.ToString();
            BnbIndividual matchedIndv = BnbIndividual.Retrieve(individualID);
            uxMappingSummary.Text = String.Format("Form will be mapped to {0} - press Finish to Import", matchedIndv.InstanceDescription);
        }

        


    }
}
