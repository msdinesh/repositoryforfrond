using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;
using BonoboEngine;
using System.Globalization;
namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for PaymentDetailPage.
	/// </summary>
	public partial class PaymentDetailPage : System.Web.UI.Page
	{
		protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox3;
		protected BonoboWebControls.DataControls.BnbLookupControl BnbLookupControl2;
		protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox4;
		protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox5;
		protected BonoboWebControls.DataControls.BnbLookupControl BnbLookupControl3;
		protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox9;
		protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox10;
		protected System.Web.UI.WebControls.Panel ContactNumberTab;
		protected BonoboWebControls.DataGrids.BnbDataGridForView BnbDataGridForView6;
		protected System.Web.UI.WebControls.Panel ContactBlockingTab;
		protected BonoboWebControls.DataGrids.BnbDataGridForView BnbDataGridForView8;
		protected System.Web.UI.WebControls.Panel SponsorshipTab;
		protected UserControls.Title titleBar;
		protected BonoboWebControls.DataControls.BnbTextBox Bnbtextbox2;
		protected BonoboWebControls.DataControls.BnbTextBox Bnbtextbox7;
		protected BonoboWebControls.DataControls.BnbTextBox Bnbtextbox8;
		protected BonoboWebControls.DataControls.BnbTextBox Bnbtextbox11;
		protected BonoboWebControls.DataControls.BnbDecimalBox BnbDecimalBox10;
		protected System.Web.UI.WebControls.Label Label3;
		protected BonoboWebControls.DataControls.BnbSearchIDComposite demoSearchIDComposite;
		protected Frond.UserControls.NavigationBar NavigationBar1;
		private BnbWebFormManager bnb;
		protected void Page_Load(object sender, System.EventArgs e)
		{


			// Put user code to initialize the page here
			bnb = new BnbWebFormManager(this,"BnbAccountItem");
			bnb.LogPageHistory = true;
			BnbWorkareaManager.SetPageStyleOfUser(this);
			bnb.PageNameOverride = "Payment Detail";

			BnbAccountHeader ach = bnb.GetPageDomainObject("BnbAccountHeader") as BnbAccountHeader;
			string appid = ach.ApplicationID.ToString();

			NavigationBar1.RootDomainObject = "BnbApplication";
			NavigationBar1.RootDomainObjectID = appid;
			
			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;

				
				bool generateRQ10 = false;
				if (Request.QueryString["GenerateRQ10"] != null && Request.QueryString["GenerateRQ10"] != ""
					&& bnb.PageState == PageState.New)
					generateRQ10 = true;
					
				// if special querystring param passed in in new mode, we
				// want to default some values
				if (generateRQ10)
				{
					BnbAccountItem bnbAccountItem= (BnbAccountItem)bnb.ObjectTree.DomainObjectDictionary["BnbAccountItem"];
					Guid accountHeaderID = new Guid(Request.QueryString["BnbAccountHeader"]);
					BnbAccountHeader ah = BnbAccountHeader.Retrieve(accountHeaderID);
					
					bnbAccountItem.AccountsCodeID = BnbConst.AccountCode_RQ10EOSGrantFinal;
					DataTable grantCalc = BnbAccountHeader.GetGrantPaymentsMade(ah.ApplicationID);
					if (grantCalc.Rows.Count == 1)
						bnbAccountItem.ItemAmount = (decimal)grantCalc.Rows[0]["EOSGrantCalcResult"];
				}
                doDefaultFundCodeForNewAccountItem();

			}

			
			
			


		}

        /// <summary>
        /// When a new payment item is added, this looks at the current placement
        /// and works out the current Fund Code
        /// 
        /// </summary>
        private void doDefaultFundCodeForNewAccountItem()
        {
            if (BnbWebFormManager.IsPageDomainObjectNew("BnbAccountItem"))
            {
                BnbAccountItem ai = bnb.GetPageDomainObject("BnbAccountItem",true) as BnbAccountItem;
                BnbAccountHeader ah = bnb.GetPageDomainObject("BnbAccountHeader") as BnbAccountHeader;

                ai.FundCodeID = ah.GetPlacementFundCodeForDate(DateTime.Today);
                
            }
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
