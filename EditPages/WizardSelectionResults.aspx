<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Page language="c#" Codebehind="WizardSelectionResults.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.WizardSelectionResults" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>WizardSelectionResults</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<DIV class="wizardheader" ms_positioning="FlowLayout">
					<H3><IMG class="icon24" src="../Images/wizard24silver.gif"> New Selection Results 
						Wizard</H3>
				</DIV>
				<INPUT id="lblHiddenAttendanceID" type="hidden" name="lblHiddenAttendanceID" runat="server">
				<asp:panel id="panelZero" runat="server" CssClass="wizardpanel">
					<P><EM>This wizard will help you to add a new&nbsp;Selection results record.</EM>
					<P>Please enter the&nbsp;Codes and press Finish<INPUT id="lblHiddenReferrer" type="hidden" name="lblHiddenReferrer" runat="server"></P>
					<P></P>
					<P>
						<asp:label id="lblPanelZeroFeedback" runat="server" CssClass="feedback"></asp:label></P>
					<asp:Panel id="panEntryForm" runat="server">
						<P>
							<TABLE class="wizardtable" id="Table1">
								<TR>
									<TD class="label" style="HEIGHT: 15px" width="25%">Volunteer Name</TD>
									<TD style="HEIGHT: 15px" width="25%">
										<asp:Label id="lblVolunteer" runat="server"></asp:Label></TD>
									<TD class="label" style="HEIGHT: 15px" width="25%">Ref No</TD>
									<TD style="HEIGHT: 15px" width="25%">
										<asp:Label id="lblRefNo" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD class="label" style="HEIGHT: 15px" width="25%">Vol Status</TD>
									<TD style="HEIGHT: 15px" width="25%">
										<asp:Label id="lblStatus" runat="server"></asp:Label></TD>
									<TD class="label" style="HEIGHT: 15px" width="25%">Event Title</TD>
									<TD style="HEIGHT: 15px" width="25%">
										<asp:Label id="lblEventTitle" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD class="label" style="HEIGHT: 15px" width="25%">Event Type</TD>
									<TD style="HEIGHT: 15px" width="25%">
										<asp:Label id="lblEventType" runat="server"></asp:Label></TD>
									<TD class="label" style="HEIGHT: 15px" width="25%">Event Start Date</TD>
									<TD style="HEIGHT: 15px" width="25%">
										<asp:Label id="lblEventStartDate" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD class="label" style="HEIGHT: 15px" width="25%">Selection Result</TD>
									<TD style="HEIGHT: 15px" width="25%">
										<bnbdatacontrol:BnbStandaloneLookupControl id="cmbSelectionResult" runat="server" CssClass="lookupcontrol" LookupTableName="lkuSelectionResult"
											LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="250px" ShowBlankRow="True" Height="22px"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
									<TD class="label" style="HEIGHT: 15px" width="25%">Pending Status Reason</TD>
									<TD style="HEIGHT: 15px" width="25%">
										<bnbdatacontrol:BnbStandaloneLookupControl id="cmbPendingReason" runat="server" CssClass="lookupcontrol" LookupTableName="vlkuPendStatusGroup"
											LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="250px" ShowBlankRow="True" Height="22px"></bnbdatacontrol:BnbStandaloneLookupControl>
										
									</TD>
								</TR>
								<tr><td colspan="3">&nbsp;</td>
								<td>(For Pending Statuses only - the reason entered here will be applied to the 
											Volunteer's new Candidate/Pend Application Status)</td>
								</tr>
							</TABLE>
						<P></P>
						<DIV class="sectionheading" id="DimensionsCodes" width="100%" height="20px">Dimension 
							Codes</DIV>
						<P>
							<TABLE class="wizardtable" id="Table2" width="100%" border="0">
								<TR>
									<TD style="HEIGHT: 15px" width="25%"></TD>
									<TD class="label" style="HEIGHT: 15px" align="center">1</TD>
									<TD class="label" style="HEIGHT: 15px" align="center">2</TD>
									<TD class="label" style="HEIGHT: 15px" align="center">3</TD>
									<TD class="label" style="HEIGHT: 15px" align="center">4</TD>
									<TD class="label" style="HEIGHT: 15px" align="center">5</TD>
									<TD class="label" style="HEIGHT: 15px" align="center">U</TD>
								</TR>
								<TR>
									<TD class="label" style="HEIGHT: 15px">Positive and Realistic Commitment</TD>
									<TD align="center">
										<asp:RadioButton id="radPRC1" runat="server" GroupName="radPRC1"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radPRC2" runat="server" GroupName="radPRC1"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radPRC3" runat="server" GroupName="radPRC1"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radPRC4" runat="server" GroupName="radPRC1"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radPRC5" runat="server" GroupName="radPRC1"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radPRCU" runat="server" GroupName="radPRC1"></asp:RadioButton></TD>
								</TR>
								<TR>
									<TD class="label" style="HEIGHT: 15px">Commitment to learning</TD>
									<TD align="center">
										<asp:RadioButton id="radCTL1" runat="server" GroupName="radCTL"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radCTL2" runat="server" GroupName="radCTL"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radCTL3" runat="server" GroupName="radCTL"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radCTL4" runat="server" GroupName="radCTL"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radCTL5" runat="server" GroupName="radCTL"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radCTLU" runat="server" GroupName="radCTL"></asp:RadioButton></TD>
								</TR>
								<TR>
									<TD class="label" style="HEIGHT: 15px">Practical Problem Solving</TD>
									<TD align="center">
										<asp:RadioButton id="radPPSA1" runat="server" GroupName="radPPSA"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radPPSA2" runat="server" GroupName="radPPSA"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radPPSA3" runat="server" GroupName="radPPSA"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radPPSA4" runat="server" GroupName="radPPSA"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radPPSA5" runat="server" GroupName="radPPSA"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radPPSAU" runat="server" GroupName="raradPPSAdPRC"></asp:RadioButton></TD>
								</TR>
								<TR>
									<TD class="label" style="HEIGHT: 15px">Flexibility</TD>
									<TD align="center">
										<asp:RadioButton id="radFlex1" runat="server" GroupName="radFlex"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radFlex2" runat="server" GroupName="radFlex"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radFlex3" runat="server" GroupName="radFlex"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radFlex4" runat="server" GroupName="radFlex"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radFlex5" runat="server" GroupName="radFlex"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radFlexU" runat="server" GroupName="radFlex"></asp:RadioButton></TD>
								</TR>
								<TR>
									<TD class="label" style="HEIGHT: 15px">Self Assurance</TD>
									<TD align="center">
										<asp:RadioButton id="radSA1" runat="server" GroupName="radSA"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radSA2" runat="server" GroupName="radSA"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radSA3" runat="server" GroupName="radSA"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radSA4" runat="server" GroupName="radSA"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radSA5" runat="server" GroupName="radSA"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radSAU" runat="server" GroupName="radSA"></asp:RadioButton></TD>
								</TR>
								<TR>
									<TD class="label" style="HEIGHT: 15px">Working with Others</TD>
									<TD align="center">
										<asp:RadioButton id="radWWO1" runat="server" GroupName="radWWO"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radWWO2" runat="server" GroupName="radWWO"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radWWO3" runat="server" GroupName="radWWO"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radWWO4" runat="server" GroupName="radWWO"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radWWO5" runat="server" GroupName="radWWO"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radWWOU" runat="server" GroupName="radWWO"></asp:RadioButton></TD>
								</TR>
								<TR>
									<TD class="label" style="HEIGHT: 15px">Sensitive to the Needs of Others</TD>
									<TD align="center">
										<asp:RadioButton id="radSON1" runat="server" GroupName="radSON"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radSON2" runat="server" GroupName="radSON"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radSON3" runat="server" GroupName="radSON"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radSON4" runat="server" GroupName="radSON"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radSON5" runat="server" GroupName="radSON"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radSONU" runat="server" GroupName="radSON"></asp:RadioButton></TD>
								</TR>
								<TR>
									<TD class="label" style="HEIGHT: 15px">Specific Technical Skills</TD>
									<TD align="center">
										<asp:RadioButton id="radSTS1" runat="server" GroupName="radSTS"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radSTS2" runat="server" GroupName="radSTS"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radSTS3" runat="server" GroupName="radSTS"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radSTS4" runat="server" GroupName="radSTS"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radSTS5" runat="server" GroupName="radSTS"></asp:RadioButton></TD>
									<TD align="center">
										<asp:RadioButton id="radSTSU" runat="server" GroupName="radSTS"></asp:RadioButton></TD>
								</TR>
							</TABLE>
						</P>
						<P></P>
						<DIV class="sectionheading" id="RejectionCodes" width="100%" height="20px">Rejection 
							Codes</DIV>
						<TABLE class="wizardtable" id="Table3" width="100%" border="0">
							<TR>
								<TD class="label" style="HEIGHT: 15px" width="50%">Codes</TD>
								<TD style="HEIGHT: 15px" width="50%">
									<asp:TextBox id="txtCodes" runat="server" Width="250px" Height="20px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD align="center" colSpan="2">Please enter two digit codes separated by spaces. 
									The first code will be the lead.</TD>
							</TR>
						</TABLE>
					</asp:Panel>
					<P>
						<uc1:wizardbuttons id="wizardButtons" runat="server" DESIGNTIMEDRAGDROP="66"></uc1:wizardbuttons></P>
					<P>
						<asp:label id="labelHiddenRequestIDUnused" runat="server" Visible="False"></asp:label></P>
				</asp:panel>
				<P></P>
				<P>&nbsp;</P>
				<P>
					<asp:Panel id="panelNoEntry" runat="server" Visible="False">
						<P><SPAN class="feedback"></SPAN></P>
						<P></P>
						<P>
							<asp:Label id="lblNoEntryFeedback" runat="server" CssClass="feedback"></asp:Label></P>
						<P>
							<asp:Button id="cmdNoPermissionCancel" runat="server" Text="Cancel" onclick="cmdNoPermissionCancel_Click"></asp:Button></P>
						<P>
					</asp:Panel></P>
				<p></p>
			</div>
		</form>
	</body>
</HTML>
