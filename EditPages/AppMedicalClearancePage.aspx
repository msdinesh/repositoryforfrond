<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AppMedicalClearancePage.aspx.cs" Inherits="Frond.EditPages.AppMedicalClearancePage" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">

<HTML>
	<HEAD>
		<title>PlacementLinkPage</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal><LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet"><asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
			<uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent">
			<uc1:title id="titleBar" runat="server" imagesource="../Images/bnbservice24silver.gif"></uc1:title><uc1:navigationbar id="NavigationBar1" runat="server"></uc1:navigationbar>
			  <bnbpagecontrol:BnbDataButtons ID="dataButtons" runat="server" UndoText="Undo"
                        SaveText="Save" EditAllText="Edit All" NewText="New" HeightAllButtons="25px"
                        UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" CssClass="databuttons" NewButtonEnabled="False">
                    </bnbpagecontrol:BnbDataButtons>
                    <bnbpagecontrol:BnbMessageBox ID="msgBox" runat="server" CssClass="messagebox">
                    </bnbpagecontrol:BnbMessageBox>
				<P>
					<TABLE class="fieldtable" id="Table1" width="100%" border="0">
						<TR>
							<TD class="label" style="HEIGHT: 10px" width="50%">Individual/Volunteer</TD>
							<TD style="HEIGHT: 10px" width="50%"> <bnbgenericcontrols:BnbHyperlink id="BnbLnkIndividual" runat="server"></bnbgenericcontrols:BnbHyperlink></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 10px" width="50%">Final Medical clearance date</TD>
							<TD style="HEIGHT: 10px" width="50%"><bnbdatacontrol:bnbdatebox id="dtFinalMedClearance" runat="server" CssClass="datebox" DataMember="BnbServiceMedicalClearance.ClearanceDate"
								Height="20px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB" Width="90px"></bnbdatacontrol:bnbdatebox></TD>
						</TR>
						<TR>
							<TD class="label" width="50%">Placement Link</TD>
							<TD width="50%"><asp:DropDownList ID ="lkuPlacementLink" runat="server"></asp:DropDownList>
							<asp:Label ID ="lblPlacementLink" runat="server"></asp:Label></TD>
						</TR>
						
					</TABLE>
				</P>
					<bnbpagecontrol:BnbDataButtons ID="BnbDataButtons1" runat="server" UndoText="Undo"
                        SaveText="Save" EditAllText="Edit All" NewText="New" HeightAllButtons="25px"
                        UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" CssClass="databuttons" NewButtonEnabled="False">
                    </bnbpagecontrol:BnbDataButtons>
			</div>
		</form>
	</body>
</HTML>