using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for ReportVisitPage.
    /// </summary>
    public partial class ReportVisitPage : System.Web.UI.Page
    {

        private BnbWebFormManager bnb = null;
        protected string DateSentVolEmpLabel = "";

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here

#if DEBUG
            if (Page.Request.QueryString.ToString() == "")
            {
                BnbWebFormManager.ReloadPage("BnbPosition=3b16c53f-aaec-11d4-908f-00508bace998&BnbRole=F3438E3D-AAEA-11D4-908F-00508BACE998");
                //"BnbPosition=246228D6-26B8-4D13-BD01-B277B339977D&BnbReportVisit=new&PageState=new");
            }
            else
            {
                bnb = new BnbWebFormManager(this, "BnbReportVisit");
            }
#else
			bnb = new BnbWebFormManager(this,"BnbReportVisit");
#endif

            // turn on history logging
            bnb.LogPageHistory = true;

            //<Integartion>
            bnb.PageNameOverride = "Reports and Visits";
            BnbWorkareaManager.SetPageStyleOfUser(this);

            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null)
                    titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
            }
            //</Integartion>
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            this.PreRender += new EventHandler(ReportVisitPage_PreRender);
            lckReportOrVisit.SelectedValueChanged += new EventHandler(lckReportOrVisit_SelectedValueChanged);
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        private void ReportVisitPage_PreRender(object sender, EventArgs e)
        {
            switch (HttpContext.Current.Request.Form["lckReportOrVisit"])
            {
                default:
                    DateSentVolEmpLabel = "Date";
                    break;
                case "1000":
                    //report
                    DateSentVolEmpLabel = "Date sent to Vol/Emp";
                    break;
                case "1001":
                    //visit
                    DateSentVolEmpLabel = "Visit Date";
                    break;
            }
        }

        private void lckReportOrVisit_SelectedValueChanged(object sender, EventArgs e)
        {
            // posts the selected value from the html form to the reporvisid attribute of the reportvisit object prior to rendering the controls on postbacks
            // this is a required step so the correct accessstate of the controls are set in the controls on postbacks
            ((BnbReportVisit)bnb.ObjectTree.DomainObjectDictionary["BnbReportVisit"]).RepOrVisID = Int32.Parse(HttpContext.Current.Request.Form["lckReportOrVisit"]);
        }
    }
}
