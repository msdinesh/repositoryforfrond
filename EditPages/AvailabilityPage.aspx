<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>

<%@ Page Language="c#" Codebehind="AvailabilityPage.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.EditPages.AvailabilityPage" %>

<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Register Src="../UserControls/NavigationBar.ascx" TagName="NavigationBar" TagPrefix="uc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>AvailabilityPage</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet" />
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/bnbindividual24silver.gif">
            </uc1:Title>
            <table style="margin: 0px" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td colspan="2" align="left" width="50%">
                            <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons1" runat="server" UndoText="Undo"
                            SaveText="Save" EditAllText="Edit All" NewText="New" HeightAllButtons="25px"
                            UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" CssClass="databuttons">
                            </bnbpagecontrol:BnbDataButtons>
                        </td>
                    </tr>
            </table>       
            <bnbpagecontrol:BnbMessageBox ID="BnbMessageBox1" runat="server" CssClass="messagebox">
            </bnbpagecontrol:BnbMessageBox>
                <p>
                    <table id="Table1" width="100%" border="0" class="fieldtable">
                        <tr>
                            <td class="label" width="50%">
                                Individual</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbDomainObjectHyperlink ID="BnbDomainObjectHyperlink1" runat="server"
                                    Target="_top" HyperlinkText="[BnbDomainObjectHyperlink]" DataMember="BnbIndividual.FullName"
                                    GuidProperty="BnbIndividual.ID" RedirectPage="IndividualPage.aspx"></bnbdatacontrol:BnbDomainObjectHyperlink>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="50%">
                                Availability Type</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl1" runat="server" CssClass="lookupcontrol"
                                    Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                    DataMember="BnbAvailability.AvailabilityTypeID" LookupTableName="lkuAvailabilityType">
                                </bnbdatacontrol:BnbLookupControl>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="50%">
                                Date From</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbDateBox ID="BnbDateBox1" runat="server" CssClass="datebox" Height="20px"
                                    Width="90px" DateCulture="en-GB" DateFormat="dd/mm/yyyy" DataMember="BnbAvailability.AvailableFromDate">
                                </bnbdatacontrol:BnbDateBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="50%">
                                Date To</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbDateBox ID="BnbDateBox2" runat="server" CssClass="datebox" Height="20px"
                                    Width="90px" DateCulture="en-GB" DateFormat="dd/mm/yyyy" DataMember="BnbAvailability.AvailableToDate">
                                </bnbdatacontrol:BnbDateBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="50%">
                                Comments</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox1" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" StringLength="0" Rows="3" MultiLine="True" DataMember="BnbAvailability.Comments">
                                </bnbdatacontrol:BnbTextBox>
                            </td>
                        </tr>
                    </table>
                </p>
 
        </div>
    </form>
</body>
</html>
