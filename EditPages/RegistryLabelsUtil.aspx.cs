using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine.Query;
using BonoboDomainObjects;
using BonoboEngine;
using System.Data.SqlClient;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for RegistryLabelsUtil.
	/// </summary>
	public partial class RegistryLabelsUtil : System.Web.UI.Page
	{

		protected UserControls.Title titleBar;



		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(!IsPostBack)
			{
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				Session["mailMergeDataTable"]=new DataTable();
				((DataTable)Session["mailMergeDataTable"]).Columns.Add("IndividualID" ,typeof(Guid));
				((DataTable)Session["mailMergeDataTable"]).Columns.Add("ApplicationID" ,typeof(Guid));
				((DataTable)Session["mailMergeDataTable"]).Columns.Add("RecruitmentBaseID" ,typeof(int));
				((DataTable)Session["mailMergeDataTable"]).Columns.Add("Forename" ,typeof(string));
				((DataTable)Session["mailMergeDataTable"]).Columns.Add("Surname" ,typeof(string));
				((DataTable)Session["mailMergeDataTable"]).Columns.Add("ReferenceNo" ,typeof(string));
				((DataTable)Session["mailMergeDataTable"]).Columns.Add("Salutation" ,typeof(string));
				((DataTable)Session["mailMergeDataTable"]).Columns.Add("Partner" ,typeof(string));
				((DataTable)Session["mailMergeDataTable"]).Columns.Add("RecruitmentBase" ,typeof(string));
				
				titleBar.TitleText = "Registry Labels";
				BnbTabControl1.SelectedTabID = FindVolunteersTab.ID;
				RecruitmentBaseLookupControl.SelectedValue=BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID;
				BonoboWebControls.BnbWebFormManager.LogAction(titleBar.TitleText, null, this);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.MailMergeDataGrid.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.MailMergeDataGrid_ItemCommand);
			this.FindResultsDataGrid.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.FindResultsDataGrid_ItemCommand_1);
			this.PreRender += new System.EventHandler(this.RegistryLabelsUtil_PreRender);

		}
		#endregion


		protected void Button1_Click(object sender, System.EventArgs e)
		{
			BnbCriteria criteria = new BnbCriteria();

			if(ForenameTextBox.Text!=string.Empty)
				criteria.QueryElements.Add(new BnbTextCondition
					("tblIndividualKeyInfo_Forename",ForenameTextBox.Text,BnbTextCompareOptions.StartOfField));

			if(SurnameTextBox.Text!=string.Empty)
				criteria.QueryElements.Add(new BnbTextCondition
					("tblIndividualKeyInfo_Surname",SurnameTextBox.Text,BnbTextCompareOptions.StartOfField));

			if(VolRefTextBox.Text!=string.Empty)
				criteria.QueryElements.Add(new BnbTextCondition
					("tblIndividualKeyInfo_old_indv_id",VolRefTextBox.Text,BnbTextCompareOptions.StartOfField));

			if(int.Parse(StatusLookupControl.SelectedValue.ToString()) != -1)
				criteria.QueryElements.Add(new BnbListCondition 
					("lnkApplicationStatus_StatusID",((int)StatusLookupControl.SelectedValue)));

			if(RecruitmentBaseLookupControl.SelectedValue !=null)
				criteria.QueryElements.Add(new BnbListCondition 
					("tblApplication_RecruitmentBaseID",((int)RecruitmentBaseLookupControl.SelectedValue)));
			try
			{
				BnbQuery query=new BnbQuery("vwBonoboFindApplication2",criteria);
				query.MaxRows=200;
				DataTable dataTable=query.Execute().Tables["Results"];
				if(query.MaxRowsExceeded) 
					ErrorLabel.Text=string.Format("Results have been limited to {0} rows",query.MaxRows); 
				else
					ErrorLabel.Text =string.Empty;
				FindResultsDataGrid.DataSource=dataTable;
				FindResultsDataGrid.DataBind();
				
			}
			catch(SqlException exception) 
			{
				if(exception.Message.StartsWith("Timeout Expired."))
					ErrorLabel.Text="Query timed out - please narrow your search criteria and try again";
				else
				{
					int errTicketID = BonoboWebControls.BnbWebFormManager.LogErrorToDatabase(exception);
					ErrorLabel.Text= String.Format("There was an error executing your query (ErrorTicketID {0} - {1}",
						errTicketID.ToString(), exception.Message);
				}
			}
		}
		private void FindResultsDataGrid_ItemCommand_1(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			((Button)e.CommandSource).Visible=false;
			foreach (DataRow currentRow in ((DataTable)Session["mailMergeDataTable"]).Rows)
				if(currentRow["ApplicationID"].ToString()==e.CommandArgument.ToString())
					return;//row has already been added
			BnbApplication bnbApplication =BnbApplication.Retrieve(new Guid(e.CommandArgument.ToString())); 
			DataRow row=((DataTable)Session["mailMergeDataTable"]).NewRow();
			row["IndividualID"]=bnbApplication.IndividualID;
			row["ApplicationID"]=bnbApplication.ID;
			row["RecruitmentBaseID"]=bnbApplication.RecruitmentBaseID;
			row["Forename"]=bnbApplication.Individual.Forename;
			row["Surname"]=bnbApplication.Individual.Surname;
			row["ReferenceNo"]=bnbApplication.Individual.RefNo;
			row["Salutation"]= bnbApplication.Individual.Additional.VMSMailSalutation;
			if(null!=bnbApplication.Individual.Partner)
				row["Partner"]=	bnbApplication.Individual.Partner.FullName;
			row["RecruitmentBase"]=	BnbEngine.LookupManager.GetLookupItemDescription("lkuRecruitmentBase", bnbApplication.RecruitmentBaseID);
			((DataTable)Session["mailMergeDataTable"]).Rows.Add(row);
			MailMergeDataGrid.DataSource=Session["mailMergeDataTable"];
			MailMergeDataGrid.DataBind();
			MessageLabel.Text=string.Format("Volunteer {0} was added to the Labels List.",bnbApplication.Individual.FullName);
		}


		private void MailMergeDataGrid_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			foreach (DataRow row in ((DataTable)Session["mailMergeDataTable"]).Rows)
				if(row["ApplicationID"].ToString()==e.CommandArgument.ToString())
				{
					((DataTable)Session["mailMergeDataTable"]).Rows.Remove(row);
					break;
				}
			MailMergeDataGrid.DataSource=Session["mailMergeDataTable"];
			MailMergeDataGrid.DataBind();
		}

		protected void BnbWordFormButton1_Clicked(object sender, System.EventArgs e)
		{
			DataTable labelsListOriginal = (DataTable)Session["mailMergeDataTable"];
			// clone the table and copy the rows
			// (we are cloning it because we might add blank labels later, see below)
			DataTable labelsList = labelsListOriginal.Clone();
			labelsList.BeginLoadData();
			foreach(DataRow rowLoop in labelsListOriginal.Rows)
			{
				labelsList.Rows.Add(rowLoop.ItemArray);
			}
			labelsList.EndLoadData();
			// process the default papermove record for each row
			foreach (DataRow row in labelsList.Rows)
			{
				BnbApplication bnbApplication =BnbApplication.Retrieve(new Guid(row["ApplicationID"].ToString()));
				bnbApplication.RecordPaperMove();
			}
			// pad out to a multiple of 14 rows so that spare labels are (mostly) blank
			// (otherwise the mail merge will leave the field name showing on unused labels)
			if ((labelsList.Rows.Count % 14) > 0)
			{
				int pages = (labelsList.Rows.Count / 14)+1;
				for(int i=labelsList.Rows.Count; i<14*pages; i++)
				{
					DataRow blankRow = labelsList.NewRow();
					labelsList.Rows.Add(blankRow);
				}
			}
			// do the mail merge
			BnbWordFormButton1.MergeData = labelsList;
			BnbWordFormButton1.WordTemplate=MapPath("~/Templates/RegLab.dot");
			BnbWordFormButton1.FilenameForUser = String.Format("RegistryLabels{0}.doc",
				DateTime.Now.ToString("ddMMMyyyy-hhmmss"));
			bool success = BnbWordFormButton1.GenerateWordForm();
			if (success)
				BonoboWebControls.BnbWebFormManager.LogAction(titleBar.TitleText + " (Word Form)",
					String.Format("Generated Word Form with {0} labels",
						labelsList.Rows.Count), this);
		}

		protected void RegistryLabelsUtil_PreRender(object sender, System.EventArgs e)
		{
			BnbWordFormButton1.Visible=	(Session["mailMergeDataTable"]!=null && ((DataTable)Session["mailMergeDataTable"]).Rows.Count>0);
		}
	}
}
