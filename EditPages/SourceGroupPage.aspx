<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Page language="c#" Codebehind="SourceGroupPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.SourceGroupPage" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal><LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet"><asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent"><uc1:title id="titleBar" runat="server" imagesource="../Images/bnbsource24silver.gif"></uc1:title><bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" NewButtonEnabled="True" NewWidth="55px" EditAllWidth="55px"
					SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px" NewText="New" EditAllText="Edit All" SaveText="Save" UndoText="Undo" CssClass="databuttons"></bnbpagecontrol:bnbdatabuttons><bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:bnbmessagebox><asp:panel id="Panel1" runat="server" Height="24px" Width="408px">
					<asp:HyperLink id="AllSourceGroupsHyperlink" runat="server" NavigateUrl="SourceGroupPage.aspx">All Source Groups</asp:HyperLink>
					<asp:HyperLink id="GrandParentItemHyperlink" runat="server">HyperLink</asp:HyperLink>
					<asp:HyperLink id="ParentItemHyperlink" runat="server">HyperLink</asp:HyperLink>
					<asp:Label id="CurrentItemLabel" runat="server">Label</asp:Label>
				</asp:panel><asp:panel id="PanelFields" runat="server" Height="64px" Width="312px">
					<TABLE class="fieldtable" id="FieldTableTable" style="WIDTH: 440px; HEIGHT: 64px" border="0">
						<asp:Panel id="panRecruitmentBase" runat="server" Visible="True">
							<TBODY>
								<TR>
									<TD class="label" style="WIDTH: 126px; HEIGHT: 14px" width="126">
										<P>Recruitment Base&nbsp;</P>
									</TD>
									<TD style="HEIGHT: 14px">
										<P>
											<bnbdatacontrol:BnbLookupControl id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" Height="22px" DataTextField="Description"
												ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuRecruitmentBase" DataMember="BnbSourceGroup.RecruitmentBaseID"></bnbdatacontrol:BnbLookupControl></P>
									</TD>
								</TR>
						</asp:Panel>
						<TR>
							<TD class="label" style="WIDTH: 126px; HEIGHT: 14px" width="126">
								<P>Exclude&nbsp;</P>
							</TD>
							<TD style="HEIGHT: 14px">
								<P>
									<bnbdatacontrol:bnbcheckbox id="BnbCheckBox1" runat="server" CssClass="checkbox" DataMember="BnbSourceGroup.Exclude"></bnbdatacontrol:bnbcheckbox></P>
							</TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 126px; HEIGHT: 5px" width="126">
								<P>Description&nbsp;</P>
							</TD>
							<TD>
								<P>
									<bnbdatacontrol:bnbtextbox id="BnbTextBox1" runat="server" CssClass="textbox" Height="20px" Width="250px" DataMember="BnbSourceGroup.Description"
										StringLength="0" Rows="1" MultiLine="false" ControlType="Text"></bnbdatacontrol:bnbtextbox></P>
							</TD>
						</TR>
						</TBODY></TABLE>
				</asp:panel>
				<P><bnbgenericcontrols:bnbhyperlink id="NewItemHyperLink" runat="server"></bnbgenericcontrols:bnbhyperlink></P>
				<asp:panel id="panRepeater" runat="server" Visible="False">
					<P>
						<asp:repeater id="Repeater1" runat="server">
							<ItemTemplate>
								<a href='<%#GetUrl(DataBinder.Eval(Container.DataItem,"ID").ToString())%>'>
									<%# DataBinder.Eval(Container.DataItem,"Description")%>
								</a>
								<br>
								<br>
							</ItemTemplate>
						</asp:repeater></P>
				</asp:panel><asp:panel id="panDataGridForView" runat="server">
					<P>
						<bnbdatagrid:BnbDataGridForView id="BnbDtGSourceGroup" runat="server" CssClass="datagrid" Width="100%" GuidKey="lkuSourceGroup_SourceGroupGUID"
							ViewName="vwBonoboSourceGroups" DomainObject="BnbSourceGroup" DisplayNoResultMessageOnly="false" DisplayResultCount="false"
							ShowResultsOnNewButtonClick="false" ViewLinksVisible="true" NewLinkVisible="False" NewLinkText="New" ViewLinksText="View"
							RedirectPage="SourceGroupPage.aspx" QueryStringKey="BnbSourceGroup"></bnbdatagrid:BnbDataGridForView></P>
				</asp:panel></div>
		</form>
	</body>
</HTML>
