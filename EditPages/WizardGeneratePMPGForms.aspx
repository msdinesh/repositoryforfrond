<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WizardGeneratePMPGForms.aspx.cs" Inherits="Frond.EditPages.WizardGeneratePMPGForms" %>

<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.ServicesControls"
    TagPrefix="cc1" %>
<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.DataControls"
    TagPrefix="bnbdatacontrol" %>

<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdmssearchcontrol" Namespace="BonoboWebControls.DMSControls" Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Generate PMPG Forms Wizard</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<DIV class="wizardheader" >
					<H3><img class="icon24" src="../Images/wizard24silver.gif">Generate PMPG Forms Wizard</H3>
				</DIV>
				<asp:panel id="panelZero" runat="server" CssClass="wizardpanel">
					<P><EM>This wizard will help you to generate a new PMPG Document.</EM>
					</P>
					
						<TABLE class="wizardtable" id="Table2">
							<TR>
								<TD class="label" style="HEIGHT: 15px">
                                    Project Name</TD>
								<TD style="HEIGHT: 15px;">
                                    <asp:Label ID="uxProjectName" runat="server"></asp:Label></TD>
							</TR>
							<TR>
								<TD class="label">
                                    Project Ref</TD>
								<TD >
                                    <asp:Label ID="uxProjectRef" runat="server"></asp:Label></TD>
							</TR>
                            <tr>
                                <td class="label">
                                    PMF Status</td>
                                <td>
                                    <asp:Label ID="uxPMFStatus" runat="server"></asp:Label></td>
                            </tr>
							<TR>
								<TD class="label">
                                    <asp:Label ID="uxGrantLabel" runat="server"></asp:Label></TD>
								<TD >
                                    <asp:Label ID="uxGrantDetails" runat="server"></asp:Label></TD>
							</TR>
						</TABLE>
						<p>Check the following list of PMPG documents <i>already generated</i> for this Project, to make sure the one you want to do has not already been done:</p>
						<P>
					<bnbdmsSearchControl:BnbDMSSearchControl id="dmsSearchControlPMPG" runat="server" CssClass="datagrid" Width="100%" ViewLinksText="View"
					NewLinkText="New" NewLinkVisible="true" ViewLinksVisible="true" ShowResultsOnNewButtonClick="false" DisplayResultCount="false" 
					DisplayNoResultMessageOnly="false" CacheByQueryKey="BnbProject"  RedirectPageURL="../Redirect.aspx" NoResultMessage="(No PMPG Documents Found)" /></P>
                        <p>
                            Please choose the type of PMPG Document you would like to generate:</p>
                    <p>
                        <bnbdatacontrol:bnbstandalonelookupcontrol id="uxChoosePMPGDocDropdown" runat="server"
                            cssclass="lookupcontrol" datatextfield="Description" height="22px" listboxrows="4"
                            lookupcontroltype="ComboBox" lookuptablename="lkuPMPGDocumentType" width="250px" ShowBlankRow="True" SortByDisplayOrder="True"></bnbdatacontrol:bnbstandalonelookupcontrol>
                        </p>
                    <p>
                        Then press Next.</p>
					
						<p><asp:label id="uxPanelZeroFeedback" runat="server" CssClass="feedback"></asp:label></P>
				</asp:panel>
                <asp:Panel ID="panelOne" runat="server" CssClass="wizardpanel" Visible="False" >
					<TABLE class="wizardtable" id="Table4">
						<TR>
							<TD class="label" style="HEIGHT: 15px">
                                Project Name</TD>
							<TD style="HEIGHT: 15px">
								<asp:Label id="uxProjectName2" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 25px">
                                Project Ref</TD>
							<TD style="HEIGHT: 25px">
								<asp:Label id="uxProjectRef2" runat="server"></asp:Label></TD>
						</TR>
                        <tr>
                            <td class="label" style="height: 25px">
                                PMF Status</td>
                            <td style="height: 25px">
                                <asp:Label ID="uxPMFStatus2" runat="server"></asp:Label></td>
                        </tr>
						<TR>
							<TD class="label">
                                PMPG Doc</TD>
							<TD>
                                <asp:Label id="uxPMPGDocChosen" runat="server"></asp:Label></TD>
						</TR>
					</TABLE>
                    <p>
                        Please choose the
                        <asp:Label ID="uxGrantTypeLabel" runat="server"></asp:Label>
                        to use to generate the document:</p>
                        <p>
                        <asp:DropDownList ID="uxChooseGrantDropdown" runat="server">
                        </asp:DropDownList></p>
                       <p>Then press Next.</p>
                    <p>
                        <asp:Label ID="uxPanelOneFeedback" runat="server" CssClass="feedback"></asp:Label>&nbsp;</p>
                </asp:Panel>
				<asp:Panel id="panelTwo" runat="server" CssClass="wizardpanel">
					<TABLE class="wizardtable" id="Table1">
                            <tr>
                                <TD class="label" style="HEIGHT: 15px">
                                    Project Name</td>
                                <TD style="HEIGHT: 15px">
                                    <asp:Label ID="uxProjectName3" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <TD class="label" style="HEIGHT: 25px">
                                    Project Ref</td>
                                <TD style="HEIGHT: 25px">
                                    <asp:Label ID="uxProjectRef3" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="label" style="height: 25px">
                                    PMF Status</td>
                                <td style="height: 25px">
                                    <asp:Label ID="uxPMFStatus3" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="label" style="height: 25px">
                                    <asp:Label ID="uxGrantLabel3" runat="server"></asp:Label></td>
                                <td style="height: 25px">
                                    <asp:Label ID="uxGrantDetails3" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <TD class="label">
                                    PMPG Doc</td>
                                <TD>
                                    <asp:Label ID="uxPMPGDocChosen3" runat="server"></asp:Label></td>
                            </tr>
                        </table>
                    <p>
                    Press the button below to generate the document:</p>
                    <p>
                    <cc1:BnbWordFormButton ID="uxGeneratePMPGDoc" runat="server" CssClass="button" Text="Generate Document" OnClicked="uxGeneratePMPGDoc_Clicked" /></p>
                    <p>
                    Then save the document to your PC and press Next.</p>
                    <br />
					<P>
						<asp:Label id="uxPanelTwoFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				</asp:Panel>
                <asp:Panel ID="panelThree" runat="server" CssClass="wizardpanel" >
                    <TABLE class="wizardtable" id="Table3">
                        <tr>
                            <TD class="label" style="HEIGHT: 15px">
                                Project Name</td>
                            <TD style="HEIGHT: 15px">
                                <asp:Label ID="uxProjectName4" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <TD class="label" style="HEIGHT: 25px">
                                Project Ref</td>
                            <TD style="HEIGHT: 25px">
                                <asp:Label ID="uxProjectRef4" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="label" style="height: 25px">
                                PMF Status</td>
                            <td style="height: 25px">
                                <asp:Label ID="uxPMFStatus4" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="label" style="height: 25px">
                                <asp:Label ID="uxGrantLabel4" runat="server"></asp:Label></td>
                            <td style="height: 25px">
                                <asp:Label ID="uxGrantDetails4" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <TD class="label">
                                PMPG Doc</td>
                            <TD>
                                <asp:Label ID="uxPMPGDocChosen4" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                    <p>
                    After filling in the document, please upload it to DMS by clicking
                    here:</p>
                    <p>
                    <asp:HyperLink ID="uxUploadDocHyperlink" runat="server">Upload Document to DMS</asp:HyperLink></p>
                    <p>
                    Suggested Document Name: <i><asp:Label ID="uxSuggestedDocumentName" runat="server"></asp:Label></i></p>
                    
                    <p>
                        Documents can also be uploaded and viewed from the
                    <asp:HyperLink ID="uxProjectPageLink" runat="server">Project page</asp:HyperLink>
                    </p>
                    <p>When you have uploaded the document to DMS, press Finish.</p>
                </asp:Panel>
				<P>
					<uc1:WizardButtons id="wizardButtons" runat="server"></uc1:WizardButtons></P>
				<P>
					<asp:Label id="uxHiddenProjectID" runat="server" Visible="False"></asp:Label>
					<asp:Label id="uxHiddenGrantID" runat="server" Visible="False"></asp:Label>
                    <asp:TextBox ID="uxHiddenReferrer" runat="server" Visible="False"></asp:TextBox>
                    &nbsp;
                    <asp:TextBox ID="uxHiddenDocType" runat="server" Visible="False"></asp:TextBox>
                    <asp:TextBox ID="uxHiddenGrantRequired" runat="server" Visible="False"></asp:TextBox></P>
			</div>
		</form>
	</body>
</HTML>

