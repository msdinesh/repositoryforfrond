<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WizardProposalToContract.aspx.cs" Inherits="Frond.EditPages.WizardProposalToContract" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>WizardProposalToContract</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<link rel="stylesheet" href="../Css/calendar.css" type="text/css" />
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar>
			</div>
			<div id="baseContent">
                <div class="wizardheader" ms_positioning="FlowLayout">
                    <h3>
                        <img class="icon24" src="../Images/wizard24silver.gif">
                        Proposal-To-Contract Wizard</h3>
                </div>
                <asp:panel id="panelZero" runat="server" CssClass="wizardpanel">
                    <P><EM>This wizard will help you turn a Proposal into a Contract</EM></P>
    			    <P>Please enter the Amount Approved in Donor Currency and Contract Conditions
	    		    </P>
		    	    <P/>
                
						<P>
                        <TABLE class="wizardtable" id="Table2">
					        <TR>
						        <TD class="label" style="HEIGHT: 15px">Project Title</TD>
						        <TD style="HEIGHT: 15px"><bnbdatacontrol:bnbtextbox id="BnbTextBox2" runat="server" Width="250px" Height="20px" CssClass="textbox" DataType="String"
								        ControlType="Label" StringLength="0" Rows="1" MultiLine="false" DataMember="BnbProject.ProjectTitle"></bnbdatacontrol:bnbtextbox></TD>
					        </TR>
					        <TR>
						        <TD class="label">
							        Start Date / End Date</TD>
						        <TD>
							        <bnbdatacontrol:BnbDateBox id="BnbDateBox1"  ShowCalender="true" runat="server" Width="90px" Height="20px" CssClass="datebox" DataMember="BnbGrantProgress.StartDate"
								        DateCulture="en-GB" DateFormat="dd/MMM/yyyy" OnPreRender="BnbDateBox1_PreRender"></bnbdatacontrol:BnbDateBox>&nbsp;/
							        <bnbdatacontrol:BnbDateBox id="BnbDateBox2"  ShowCalender="true" runat="server" DataMember="BnbGrantProgress.EndDate" CssClass="datebox"
								        Height="20px" Width="90px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:BnbDateBox></TD>
					        </TR>
					        <TR>
						        <TD class="label" style="HEIGHT: 26px">Contract Status</TD>
						        <TD style="HEIGHT: 26px">
							        <bnbdatacontrol:BnbLookupControl id="BnbLookupControl1" runat="server" Width="250px" Height="22px" CssClass="lookupcontrol"
								        DataMember="BnbGrantProgress.GrantStatusID" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
								        LookupTableName="vlkuBonoboContractStatus" OnPreRender="BnbLookupControl1_PreRender"></bnbdatacontrol:BnbLookupControl></TD>
					        </TR>
					        <TR>
						        <TD class="label">
							        <P>Contract Amount<BR>
								        (
								        <bnbdatacontrol:BnbLookupControl id="BnbLookupControl2" runat="server" DataMember="BnbGrant.CurrencyID" CssClass="label"
									        Height="22px" LookupTableName="lkuCurrency" LookupControlType="ReadOnlyLabel" ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:BnbLookupControl>)</P>
						        </TD>
						        <TD>
							        <bnbdatacontrol:bnbdecimalbox id="BnbDecimalBox1" runat="server" Width="90px" DecimalPlaces="2" Height="20px"
								        CssClass="textbox" DataMember="BnbGrantProgress.ContractAmountCurrency" OnPreRender="BnbDecimalBox1_PreRender"></bnbdatacontrol:bnbdecimalbox></TD>
					        </TR>
					        <TR>
						        <TD class="label">Exchange Rate</TD>
						        <TD>
							        <bnbdatacontrol:BnbDecimalBox id="BnbDecimalBox2" runat="server" DataMember="BnbGrantProgress.ExchangeRate" CssClass="textbox"
								        Height="20px" Width="90px" TextAlign="Default" FormatExpression="#0.000" DefaultValue="0" onprerender="BnbDecimalBox2_PreRender"></bnbdatacontrol:BnbDecimalBox></TD>
					        </TR>
					        <tr>
					         <TD class="label">Contract Signed?</TD>
					         <td> 
					         <bnbdatacontrol:BnbLookupControl id="lkuContract" runat="server" Width="250px" Height="22px" CssClass="lookupcontrol"
								        DataMember="BnbGrant.ContractSignedID" DataTextField="Description" ListBoxRows="3" LookupControlType="ComboBox"
								        LookupTableName="lkuYesNoForContract" OnPreRender="lkuContract_PreRender"></bnbdatacontrol:BnbLookupControl>
					         </td>
					         </tr>
					        <TR>
						        <TD class="label">Contract conditions</TD>
						        <TD><bnbdatacontrol:bnbtextbox id="BnbTextBox1" runat="server" Width="500px" Height="20px" CssClass="textbox" DataType="String"
								        ControlType="Text" StringLength="0" Rows="5" MultiLine="True" DataMember="BnbGrant.ContractConditions"></bnbdatacontrol:bnbtextbox></TD>
					        </TR>
					          <tr>
					        <td colspan="2">					        
					        <p style="color: black; font-family: Verdana; text-align: justify;">
					        "If the contract has been signed and you want to request a grant code from Finance, please click on the "Request grant code" button below and then upload the signed contract by clicking on the �UPLOAD NEW DOCUMENT� button on the next screen. Except in exceptional circumstances, Finance cannot assign a grant code unless the signed contract has been uploaded.
You should receive a copy of the email that is sent to Finance. If you do not receive a copy within 24 hours please contact the IT Helpdesk"
</p>
					        </td>
					        </tr>
					        <tr>
					        <td colspan="2">
					        <asp:Button ID="btnRequestGrantCode" runat="server" Text="Request Grant Code" Width="228px" OnClick="btnRequestGrantCode_Click"/>
					        </td>
					        </tr>
				        </TABLE>
					<P>

						<asp:Label id="lblPanelTwoFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				  </asp:panel>
				<P></P>
									<bnbpagecontrol:BnbMessageBox id="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:BnbMessageBox>
				
			<P>
				<bnbgenericcontrols:BnbButton id="bnbFinish" runat="server" Text="Finish" onclick="bnbFinish_Click"></bnbgenericcontrols:BnbButton>
				<bnbgenericcontrols:BnbButton id="BnbButton1" runat="server" Text="Cancel" onclick="BnbButton1_Click"></bnbgenericcontrols:BnbButton></P>
				<P><asp:label id="labelHiddenRequestIDUnused" runat="server" Visible="False"></asp:label>
				    <asp:TextBox ID="lblHiddenGrantID" runat="server" Visible="false"></asp:TextBox>
					<asp:TextBox id="txtHiddenReferrer" runat="server" Visible="False"></asp:TextBox></P>
				<p></p>
			</div>
        
    </form>
</body>
</html>
