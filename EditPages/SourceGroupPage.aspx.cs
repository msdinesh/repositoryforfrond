using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;
using BonoboEngine;
using BonoboEngine.Utilities;
using System.Text;
using System.Collections.Specialized;
namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for SourceGroupPage.
	/// </summary>
	public partial class SourceGroupPage : System.Web.UI.Page
	{
		#region Control Declarations
		protected BonoboWebControls.DataControls.BnbLookupControl Bnblookupcontrol8;
		
		protected BonoboWebControls.DataControls.BnbDomainObjectHyperlink BnbDomainObjectHyperlink1;
		protected BonoboWebControls.DataControls.BnbDomainObjectHyperlink BnbDomainObjectHyperlinkCh;
		protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox2;
		protected System.Web.UI.WebControls.Panel Panel2;
		
		# endregion

		private BnbWebFormManager bnb;
		private string context;
		protected UserControls.Title titleBar;
        private string m_thisPage = "SourceGroupPage.aspx";

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(Request.QueryString.ToString()==string.Empty)
				context="AllSourceGroups";
			else if(Request.QueryString["BnbSource"]!=null)
				context="BnbSource";
			else if(Request.QueryString["BnbSourceType"]!=null)
				context="BnbSourceType";
			else
				context="BnbSourceGroup";
			if(context!="AllSourceGroups")
			{
				BnbTextBox1 .DataMember=context + ".Description";
				BnbCheckBox1.DataMember=context + ".Exclude";
			}
			bnb = new BnbWebFormManager(this,context);
//			bnb.LogPageHistory = true;

			BnbWorkareaManager.SetPageStyleOfUser(this);
			BnbSourceGroup bnbSourceGroup=null;
			BnbSourceType bnbSourceType=null;
			BnbSource bnbSource=null;
			if(context!="AllSourceGroups")
			{
				bnb.LogPageHistory = true;
				bnbSourceGroup=bnb.ObjectTree.DomainObjectDictionary["BnbSourceGroup"]as BnbSourceGroup;
				bnbSourceType=bnb.ObjectTree.DomainObjectDictionary["BnbSourceType"]as BnbSourceType;
				bnbSource=bnb.ObjectTree.DomainObjectDictionary["BnbSource"]as BnbSource;
			}
			else
			{
				bnb.PageTitleDescriptionOverride = "All Sources Groups";
				bnb.LogPageHistory = true;
			}
			QueryStringBuilder qsb=new QueryStringBuilder(Request.QueryString);
			qsb[context]=string.Empty;
			QueryStringBuilder qsbNew=new QueryStringBuilder(Request.QueryString);
			switch(context)
			{
				case "BnbSourceGroup":
					Repeater1.DataSource=	bnbSourceGroup.SourceTypes ;
					panRecruitmentBase.Visible = true;
					panDataGridForView.Visible = false;
					panRepeater.Visible = true;
					CurrentItemLabel.Text=bnbSourceGroup.Description;
					ParentItemHyperlink.Visible=false;
					GrandParentItemHyperlink.Visible=false;
					NewItemHyperLink.Text="New Source Type";
					qsbNew["BnbSourceType"]="new";
					qsbNew["PageState"]="new";
                    NewItemHyperLink.NavigateUrl = qsbNew.ToString(m_thisPage);
					titleBar.TitleText = String.Format("Source Group - {0}",
						bnbSourceGroup.Description);
					break;
				case "BnbSourceType":
					panRepeater.Visible = true;
					panRecruitmentBase.Visible = false;
					Repeater1.DataSource=	bnbSourceType.Sources ;
					panDataGridForView.Visible = false;
					CurrentItemLabel.Text=bnbSourceType.Description;
					ParentItemHyperlink.Text  =bnbSourceGroup.Description;
                    ParentItemHyperlink.NavigateUrl = qsb.ToString(m_thisPage);
					GrandParentItemHyperlink.Visible=false;
					NewItemHyperLink.Text="New Source";
					qsbNew["BnbSource"]="new";
					qsbNew["PageState"]="new";
                    NewItemHyperLink.NavigateUrl = qsbNew.ToString(m_thisPage);
					titleBar.TitleText = String.Format("Source Type - {0}/{1}",
						bnbSourceGroup.Description, bnbSourceType.Description);
					break;
				case "BnbSource":
					panRepeater.Visible = true;
					panRecruitmentBase.Visible = false;
					panDataGridForView.Visible = false;
					CurrentItemLabel.Text=bnbSource.Description;
					ParentItemHyperlink.Text=bnbSourceType.Description;
                    ParentItemHyperlink.NavigateUrl = qsb.ToString(m_thisPage);
					GrandParentItemHyperlink.Text=bnbSourceGroup.Description;
					qsb["BnbSourceType"]=string.Empty;
                    GrandParentItemHyperlink.NavigateUrl = qsb.ToString(m_thisPage);
					titleBar.TitleText = String.Format("Source - {0}/{1}/{2}",
						bnbSourceGroup.Description, bnbSourceType.Description, bnbSource.Description);
					break;
				case "AllSourceGroups":
//					panRepeater.Visible = false;
					panDataGridForView.Visible = true;
					panRecruitmentBase.Visible = false;
					PanelFields.Visible=false;					
					CurrentItemLabel.Text="All Source Groups";
					ParentItemHyperlink.Visible=false;
					GrandParentItemHyperlink.Visible=false;
					AllSourceGroupsHyperlink.Visible=false;
//					Repeater1.DataSource = BnbSourceGroup.AllSourceGroups;
//					BnbDataButtons1.Visible=false;
					NewItemHyperLink.Text="New Source Group";
					qsbNew["BnbSourceGroup"]="new";
					qsbNew["PageState"]="new";
                    NewItemHyperLink.NavigateUrl = qsbNew.ToString(m_thisPage);
					titleBar.TitleText = "All Source Groups";
					break;

				default:
					break;
			}
			Repeater1.DataBind();
		}

		protected string GetUrl(string id)
		{
			//copy the original
			QueryStringBuilder sb=new QueryStringBuilder(Request.QueryString);
			switch(context)
			{
				case "BnbSourceGroup":
					//add child item
					sb["BnbSourceType"]=id;
					break;
				case "BnbSourceType":
					sb["BnbSource"]=id;
					break;
				case "AllSourceGroups":
					sb["BnbSourceGroup"]=id;
					break;

				default:
					Response.Write("problems");
					break;
			}
            return sb.ToString(m_thisPage);
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


	}
}
