using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboDomainObjects;
using BonoboEngine;
using BonoboWebControls;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for WizardNewCompanyAddress.
	/// </summary>
	public class WizardNewOrganisationAddress : System.Web.UI.Page
	{
		#region controls declaration
		protected System.Web.UI.WebControls.Literal literalBeforeCss;
		protected System.Web.UI.WebControls.Literal literalAfterCss;
		protected System.Web.UI.WebControls.Label lblOrganisationName;
		protected System.Web.UI.WebControls.DropDownList cmbCountry;
		protected System.Web.UI.WebControls.TextBox txtPostCode;
		protected System.Web.UI.WebControls.Button cmdFind1;
		protected BonoboWebControls.ServicesControls.BnbAddressLookupComposite uxAddressLookup;
		protected System.Web.UI.WebControls.Label labelPanelZeroFeedback;
		protected System.Web.UI.WebControls.Panel panelZero;
		protected System.Web.UI.WebControls.Label labelHiddenRequestIDUnused;
		protected System.Web.UI.HtmlControls.HtmlInputHidden lblHiddenCompanyID;
		protected UserControls.WizardButtons wizardButtons;

		#endregion controls declaration

		#region page load
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			//BnbIndividual=D1DAAB24-AADF-11D4-908F-00508BACE998
			// Put user code to initialize the page here
			BnbEngine.SessionManager.ClearObjectCache();
			// Put user code to initialize the page here
#if DEBUG	
			
			if (Request.QueryString.ToString() == "")
				Response.Redirect("WizardNewOrganisationAddress.aspx?BnbCompany=CA9E5A2D-603B-11D7-8FFA-00805F031656");
#endif
					
			BnbWorkareaManager.SetPageStyleOfUser(this);
			if (!this.IsPostBack)
			{
				if (this.Request.QueryString["BnbCompany"] != null)
					this.CompanyID = new Guid(this.Request.QueryString["BnbCompany"]);
				else
					throw new ApplicationException("WizardNewOrganisationAddress.aspx expects a BnbCompany parameter in the querystring");

				this.initWizard();
				BnbWebFormManager.LogAction("New Organisation Address Wizard", null, this);
			}

			// Put user code to initialize the page here
			wizardButtons.AddPanel(panelZero);
		}
		#endregion page load
		/// <summary>
		/// stores the individual that the action is being added to
		/// </summary>

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			wizardButtons.CancelWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
			wizardButtons.FinishWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);

			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.cmdFind1.Click += new System.EventHandler(this.cmdFind1_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region properties
		private Guid CompanyID
		{
			get{return new Guid(lblHiddenCompanyID.Value);}
			set{lblHiddenCompanyID.Value = value.ToString();}
		}
		#endregion properties

		#region private methods
		private void initWizard()
		{
			Utilities objUtilities = new Utilities();
			uxAddressLookup.Visible = false;

			BnbCompany objCompany = BnbCompany.Retrieve(this.CompanyID);
			lblOrganisationName.Text = objCompany.CompanyName;


			//country combos default.
			objUtilities.FillCombos(cmbCountry,"lkuCountry","GuidID","Exclude = 0","");
			cmbCountry.SelectedValue = BnbEngine.SessionManager.GetSessionInfo().DefaultCountryID.ToString();
			
			uxAddressLookup.CountryID = BnbEngine.SessionManager.GetSessionInfo().DefaultCountryID;

		}

		private void PassAddressToAddressPage()
		{
			if (uxAddressLookup.Visible && uxAddressLookup.SelectedValue != null)
			{
				// get the selected address
				uxAddressLookup.CopySelectedAddressToProperties();
				// copy the data to a hashtable
				Hashtable addressTransfer = new Hashtable();
				addressTransfer.Add("Line1", uxAddressLookup.Line1);
				addressTransfer.Add("Line2", uxAddressLookup.Line2);
				addressTransfer.Add("Line3", uxAddressLookup.Line3);
				addressTransfer.Add("PostCode", uxAddressLookup.PostCode);
				addressTransfer.Add("County", uxAddressLookup.County);
				addressTransfer.Add("CountryID", uxAddressLookup.CountryID);
				// put it in the session
				this.Session["AddressTransfer"] = addressTransfer;
				// redirect to address page
				Response.Redirect(String.Format("OrganisationAddressPage.aspx?BnbCompany={0}&BnbCompanyAddress=new&PageState=new&AddressTransfer=1",
					this.CompanyID));
				this.Session["ReturnUrl"] = String.Format("OrganisationPage.aspx?BnbCompany={0}",
					this.CompanyID);
			}
			else
			{
				
				// pass dontredirect param to stop it re-directing back to here!
				Response.Redirect(String.Format("OrganisationAddressPage.aspx?BnbCompany={0}&BnbCompanyAddress=new&PageState=new&DontRedirect=1",
					this.CompanyID));
				this.Session["ReturnUrl"] = String.Format("OrganisationPage.aspx?BnbCompany={0}",
					this.CompanyID);
			}
		}

		#endregion private methods

		#region events

	

		private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			Response.Redirect("../FindGeneral.aspx");
		}

		private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			this.PassAddressToAddressPage();
		}


		private void cmdFind1_Click(object sender, System.EventArgs e)
		{
			//display the address lookup control and fill in the necessary properties
			if (txtPostCode.Text.Length > 0 && cmbCountry.SelectedValue.ToString().Length > 0)
			{
				uxAddressLookup.Visible = true;
				uxAddressLookup.CountryID = new Guid(cmbCountry.SelectedValue);
				uxAddressLookup.PostCode = txtPostCode.Text;
				uxAddressLookup.PerformSearch();
				uxAddressLookup.Visible = true;
			}
		}
		
		




		#endregion

	}
}
