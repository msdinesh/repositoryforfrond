using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for WizardNewPersonnel.
	/// </summary>
	public partial class WizardNewPersonnel : System.Web.UI.Page
	{
		protected UserControls.WizardButtons wizardButtons;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{

#if DEBUG
			if (!this.IsPostBack && this.Request.QueryString.ToString() == "")
				Response.Redirect("WizardNewPersonnel.aspx?BnbCompany=A26D539E-C185-43A2-A306-C1AF3E7ECAFA");
#endif


			BnbEngine.SessionManager.ClearObjectCache();
			
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);

			if (!this.IsPostBack)
			{
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				if (this.Request.QueryString["BnbCompany"] != null)
					this.CompanyID = new Guid(this.Request.QueryString["BnbCompany"]);
				else
					throw new ApplicationException("WizardNewPersonnel.aspx expects a BnbCompany parameter in the querystring");

				this.InitWizard();
				BonoboWebControls.BnbWebFormManager.LogAction("New Personnel Wizard", null, this);
			}

			wizardButtons.AddPanel(panelZero);
			wizardButtons.AddPanel(panelOne);
			wizardButtons.AddPanel(panelTwo);

		}

		/// <summary>
		/// stores the companyID that ther personnel are being added to
		/// </summary>
		public Guid CompanyID
		{
			get{return new Guid(lblHiddenCompanyID.Text);}
			set{lblHiddenCompanyID.Text = value.ToString();}
		}
		
		/// <summary>
		/// The existing individual, selected by the user (if any)
		/// </summary>
		public Guid IndividualID
		{
			get{return new Guid(lblHiddenIndividualID.Text);}
			set{lblHiddenIndividualID.Text = value.ToString();}
		}

		private void InitWizard()
		{
			// show company name 
			BnbCompany useCompany = BnbCompany.Retrieve(this.CompanyID);
			lblCompanyName.Text = useCompany.CompanyName;
			lblCompanyName2.Text = useCompany.CompanyName;

			// show live company addresses in combo on paneltwo
			foreach (BnbCompanyAddress caLoop in useCompany.CompanyAddresses)
			{
				if (caLoop.AddressLive && caLoop.Address != null)
				{
					ListItem newItem = new ListItem(caLoop.Address.AddressLine1, caLoop.Address.ID.ToString());
					cboCompanyAddress.Items.Add(newItem);
				}
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			wizardButtons.CancelWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
			wizardButtons.FinishWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
			wizardButtons.ShowPanel +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
			wizardButtons.ValidatePanel +=new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			this.ReturnToCompanyPage();
		}

		private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			this.SavePersonnel();
		}

		private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			if (e.CurrentPanel == 0)
				lblPanelZeroFeedback.Text = "";

			if (e.CurrentPanel == 1)
				this.SetupPanelOne();

			if (e.CurrentPanel == 2)
				this.SetupPanelTwo();

		}

		private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
		{
			if (e.CurrentPanel == 0)
			{
				if (txtRefNo.Text == "" && (txtSurname.Text == "" || txtForename.Text == ""))
				{
					e.Proceed = false;
					lblPanelZeroFeedback.Text = "Please fill in Ref No or Forename and Surname";
				}
			}
			if (e.CurrentPanel == 1)
			{
				if (bdgIndividuals.SelectedDataRowID == Guid.Empty && chkMakeNewIndividual.Checked == false)
					e.Proceed = false;
				lblPanelOneFeedback.Text = "You must select an existing Individual, or check the checkbox to make a new Individual.";
			}

		}

		private void SetupPanelOne()
		{
			lblPanelOneFeedback.Text = "";
			
			BnbCriteria dupCriteria = new BnbCriteria();
			if (txtForename.Text != "")
			{
				string forenameBit = txtForename.Text;
				if (forenameBit.Length > 5)
					forenameBit = forenameBit.Substring(0,5);			
				BnbTextCondition forenameMatch = new BnbTextCondition("tblIndividualKeyInfo_Forename", forenameBit, BnbTextCompareOptions.StartOfField);
				dupCriteria.QueryElements.Add(forenameMatch);
			}
			if (txtSurname.Text != "")
			{
				BnbTextCondition surnameMatch = new BnbTextCondition("tblIndividualKeyInfo_Surname", txtSurname.Text, BnbTextCompareOptions.StartOfField);
				dupCriteria.QueryElements.Add(surnameMatch);
			}
			if (txtRefNo.Text != "")
			{
				BnbTextCondition refnoMatch = new BnbTextCondition("tblIndividualKeyInfo_old_indv_id", txtRefNo.Text, BnbTextCompareOptions.ExactMatch);
				dupCriteria.QueryElements.Add(refnoMatch);
			}
			
			bdgIndividuals.Criteria = dupCriteria;
			bdgIndividuals.MaxRows = 30;
			bdgIndividuals.DataBind();
			
			lblMaxRowsExceeded.Text = "";
			if (bdgIndividuals.ResultCount > 0)
			{
				panelChoose.Visible = true;
				lblNoDuplicates.Visible = false;
				chkMakeNewIndividual.Checked = false;
				if (bdgIndividuals.MaxRowsExceeded )
					lblMaxRowsExceeded.Text = "More than 30 possible matches were found. Only the first 30 are shown here.";
			}
			else
			{
				panelChoose.Visible = false;
				lblNoDuplicates.Visible = true;
				chkMakeNewIndividual.Checked = true;
			
			}

			
			
		}

		private void SetupPanelTwo()
		{
			if (chkMakeNewIndividual.Checked)
				bdgIndividuals.SelectedDataRowID = Guid.Empty;

			if (chkMakeNewIndividual.Checked == false)
			{
				this.IndividualID = bdgIndividuals.SelectedDataRowID;
				BnbIndividual useIndiv = BnbIndividual.Retrieve(this.IndividualID);
				lblForename.Text = useIndiv.Forename;
				lblSurname.Text = useIndiv.Surname;
				txtForenameFinal.Visible = false;
				txtSurnameFinal.Visible = false;
				lblRefNo.Text = useIndiv.RefNo;
				txtEmail.Visible = false;
				BnbContactNumber useEmail = useIndiv.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Email, useIndiv.ID);
				if (useEmail != null)
					lblEmail.Text = useEmail.ContactNumber;
			}
			else
			{
				lblForename.Text = "";
				txtForenameFinal.Text = txtForename.Text;
				txtForenameFinal.Visible = true;
				lblSurname.Text = "";
				txtSurnameFinal.Text = txtSurname.Text;
				txtSurnameFinal.Visible = true;

				lblRefNo.Text = "(new)";
				txtEmail.Text = "";
				txtEmail.Enabled = true;
				txtEmail.Visible = true;
				lblEmail.Text = "";
			}

		}

		private void SavePersonnel()
		{
			// personnel are added by linking the individual to the address that belongs
			// to the company
			// ie. BnbIndividual -> BnbIndividualAddress -> BnbAddress -> BnbCompanyAddress -> BnbCompany
			BnbEditManager em = new BnbEditManager();
			Guid companyAddressID = new Guid(cboCompanyAddress.SelectedValue);
			BnbAddress useCompanyAddress = BnbAddress.Retrieve(companyAddressID);
			useCompanyAddress.RegisterForEdit(em);
			BnbIndividualAddress indvLink = new BnbIndividualAddress(true);
			indvLink.RegisterForEdit(em);

			BnbIndividual useIndv = null;
			if (chkMakeNewIndividual.Checked == false)
			{
				// fetch existing indv
				useIndv = BnbIndividual.Retrieve(this.IndividualID);
				useIndv.RegisterForEdit(em);
			}
			else
			{
				// make new individual
				useIndv = new BnbIndividual(true);
				useIndv.RegisterForEdit(em);
				useIndv.Forename = txtForenameFinal.Text;
				useIndv.Surname = txtSurnameFinal.Text;
				// add email if specified
				if (txtEmail.Text != "")
				{
					BnbContactNumber newEmail = new BnbContactNumber(true);
					newEmail.RegisterForEdit(em);
					newEmail.ContactNumber = txtEmail.Text;
					newEmail.ContactNumberTypeID = BnbConst.ContactNumber_Email;
					useIndv.ContactNumbers.Add(newEmail);
				}
			}

			// link the individual through to the address
			useIndv.IndividualAddresses.Add(indvLink);
			useCompanyAddress.IndividualAddresses.Add(indvLink);
			// set role and dept
			indvLink.CompanyRole = txtRole.Text;
			indvLink.CompanyDepartment = txtDept.Text;
			// if telephone specified then add it via the address
			if (txtTelephone.Text != "")
			{
				useCompanyAddress.SetTelephone(useIndv, txtTelephone.Text);
			}
			// address type will default to other/work
			// however if this is a new individual, we must set type to
			// permanent/work
			if (chkMakeNewIndividual.Checked == true)
				indvLink.AddressTypeID = BnbConst.AddressType_PermanentWork;

			try
			{
				em.SaveChanges();
				this.ReturnToCompanyPage();
			}
			catch(BnbProblemException pe)
			{
				lblPanelTwoFeedback.Text = "Unfortunately, the Personnel record could not be created due to the following problems: <br/>";
				foreach(BnbProblem p in pe.Problems)
					lblPanelTwoFeedback.Text += p.Message + "<br/>";

			}

		}

		private void ReturnToCompanyPage()
		{
			Response.Redirect("OrganisationPage.aspx?BnbCompany=" + this.CompanyID.ToString());
		}
	}
}
