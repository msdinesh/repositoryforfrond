using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboDomainObjects;
using BonoboWebControls;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for SupplierPage.
	/// </summary>
	public partial class SupplierPage : System.Web.UI.Page
	{
		protected BonoboWebControls.DataControls.BnbLookupControl BnbLookupControl1;
		protected UserControls.Title titleBar;

	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			BnbWebFormManager bnb = null;
#if DEBUG	
			//			bnb = null;
			if (Request.QueryString.ToString() == "")
				BnbWebFormManager.ReloadPage("BnbCompany=BEAC6FC5-62A3-4CDE-B190-56D2FB224786&BnbCompanySupplier=898DFDF6-2F5F-44E6-BF9C-4BE290CAD4B6");
			else
				bnb = new BnbWebFormManager(this,"BnbCompanySupplier");
#else	
			bnb = new BnbWebFormManager(this,"BnbCompanySupplier");
#endif
			bnb.LogPageHistory = true;
			BnbWorkareaManager.SetPageStyleOfUser(this);
			// tweak page title settings
			bnb.PageNameOverride = "Supplier";
			if (!this.IsPostBack)
			{
				titleBar.TitleText = "Supplier";
			}		
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
