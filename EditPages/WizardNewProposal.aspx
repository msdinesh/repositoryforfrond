<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WizardNewProposal.aspx.cs" Inherits="Frond.EditPages.WizardNewProposal" %>

<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="~/UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="~/UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>New Proposal Wizard</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C#" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<link href="../Css/PGMSInformation.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet"/>
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<link rel="stylesheet" href="../Css/calendar.css" type="text/css" />
		<script type="text/javascript" language="javascript">
		function valExchangeRateOnKeyPress(evt)
		{
		    document.getElementById('txtExchangeRate').onkeyup=function(){
		        var exchangeRate=document.getElementById('txtExchangeRate').value;		
		        if(exchangeRate < 1 && exchangeRate!="") 
		            document.getElementById('spnViewWarning').style.display='block';
		        else
		            document.getElementById('spnViewWarning').style.display='none';
		    }
		}
		</script>
</head>
<body onclick="hideCalendar(event);">
    <form id="form1" runat="server">
    <div>
    <div id="baseMenu">
	    <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar></div>
	<div id="baseContent">	
	    <asp:Panel ID="panelZero" runat ="server" >
	     <div class="wizardheader" ms_positioning="FlowLayout">
			    <h3><img class="icon24" src="../Images/wizard24silver.gif" alt ="" /> New Proposal Wizard</h3>
	        </div>
	        <asp:Panel ID="pnlProposalChecklist" runat="server" CssClass="wizardpanel">
                    <br />
                    <p style="text-align: center; font-size: 15px">
                        <b>Project Proposal Checklist</b></p>
                    <br />
                    <p class="font">
                        It will take approximately 15 minutes to initiate a project into PGMS. To assist
                        please make sure you have the following information in hand.</p>
                    <ul class="tick">
                        <li class="style">Has the donor organisation information already been entered into PGMS? If not, contact the Donor Manager for support.</li>
                        <li class="style">Do you have the completed project narrative/proposal information?</li>
                        <li class="style">Do you have the completed Project Total Cost budgeting tool completed in detail?</li>
                        <li class="style">Do you have a list of the volunteer placement types?</li>
                        <li class="style">Do you have the Donor Budget filled out?</li>
                        <li class="style">If you are not the proposal lead, have you been delegated or checked that the proposal lead hasn't entered the information</li>
                    </ul>
            </asp:Panel>
            </asp:Panel>
	        <asp:Panel ID="panelOne" runat ="server" >
	        <div class="wizardheader" ms_positioning="FlowLayout">
			    <h3><img class="icon24" src="../Images/wizard24silver.gif" alt ="" /> New Project Wizard - Project Proposal</h3>
	        </div>
	        <br />
	        <asp:Label ID="lblErrMessage" runat="server" CssClass="feedback" Visible="false" ></asp:Label>
	             <p class="SectionHeading">PROPOSAL DETAILS</p>
                    <table  class ="wizardtable" id="tblProposalInfo">    
                     <tr>
                        <td class ="label" width="25%">Project Title</td>
                        <td colspan="2">
                            <asp:Label ID="lblProjectTitle" runat="server" Text="Label"></asp:Label><br />
                        </td>
	                </tr>       
                     <tr>
                        <td class ="label" width="25%">Proposal Lead</td>
                        <td width="25%">
                            <asp:DropDownList ID="drpProposalLead" runat="server" Width="250px" Height="20px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ControlToValidate="drpProposalLead" ErrorMessage="Required field cannot be left blank" />
                        </td>                        
                    </tr>
                    <tr>
                       
                        <td class ="label" width="25%" style="height: 25px">Donor Organization</td>
                        <td colspan ="3" width="25%" style="height: 25px">
                            <asp:Label ID="lblOrg" runat ="server" text="None"></asp:Label>&nbsp;&nbsp;&nbsp;<asp:Button runat="server" Text="Add" ID="addOrg" CausesValidation ="false"  OnClick="AddOrg_Click"/>
                            <asp:Label ID="lblOrgErr" runat="server" ForeColor="red"></asp:Label><br /><br />
                            
                            <asp:Label ID="lblFind" Text="Please enter any part of the organisation name and click find." runat="server" Visible="false" ></asp:Label><br />
                            <asp:TextBox ID="txtDonorOrg" Width ="100px" runat ="server" Height="15px" Visible="false" MaxLength="10"/> <asp:Button runat ="server" ID="btnFind" OnClick ="findButton_Click" Height="20px" Width="50px" Text="Find" CausesValidation="false" Visible="false"/> 
                            &nbsp;&nbsp;<asp:Button ID="btnCancel" Text="Cancel" runat="server" CausesValidation="false" Visible ="false" OnClick="btnCancel_Onclick" />               
                            <br />
                            
                            <asp:Label ID="lbldrpOrg" Text="Please select your item from the list below." runat="server" Visible="false"></asp:Label><br />
                            <asp:DropDownList runat ="server" ID ="drpOrganisation" Width ="250px" Height="20px" Visible ="false"
                             OnSelectedIndexChanged="Organisation_SelectedIndexChanged" AutoPostBack="true" /> <br />
                             
                        </td>
	                </tr>
	                <tr>                       
                        <td class ="label" width="25%">Proposal Progress</td>
                        <td width="25%"><asp:DropDownList ID="drpProposalProgress" runat ="server" Width ="250px" Height="20px" ></asp:DropDownList><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required field cannot be left blank" ControlToValidate="drpProposalProgress" /></td>
	                
                        <td class ="label" width="25%">Likelihood of success of application</td>
                        <td width="25%"><asp:DropDownList ID="drpLikelihood" runat ="server" Width ="250px" Height="20px" ></asp:DropDownList><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required field cannot be left blank" ControlToValidate="drpLikelihood" /></td>
	                </tr>
	                 <tr>
                        <td class ="label" width="25%">Submission Date</td>
                        <td><bnbdatacontrol:BnbStandaloneDateBox id="txtSubmissionDate" runat="server" CssClass="datebox" Width="90px" Height="20px" DateCulture="en-GB"
									DateFormat="dd/MMM/yyyy" ShowCalender="true" ></bnbdatacontrol:BnbStandaloneDateBox><br />									
									<asp:Label ID="lblSubmissionErr" runat="server" ForeColor="red" ></asp:Label>
						</td>
	                </tr>	                	               
	            </table>
	            <br />
	            <asp:ValidationSummary runat="server" ID="ValidationSummary2" CssClass="valsummary" 
                    HeaderText="It seems you have not entered the appropriate information in the mandatory fields.Please fill in these fields correctly in order to proceed." />	        
	    </asp:Panel>	
	    <asp:Panel ID="panelTwo" runat="server">
	        <div class="wizardheader" ms_positioning="FlowLayout">
			    <h3><img class="icon24" src="../Images/wizard24silver.gif" alt ="" /> New Proposal Wizard - Finance/Risk Information</h3>
	        </div>
	       
	         <asp:Panel ID="pnlFinanceAndRiskInfo" runat="server" CssClass="wizardpanel">
	            <br />	        
	            <table id="tblFinanceInfo" class="wizardtable">
	                <tr>
	                    <td class="label">Project/Donor</td>
	                    <td><asp:TextBox ID="txtProjectDonor" Width ="250px" runat ="server" TextMode="MultiLine" Rows ="3" ReadOnly="true" ></asp:TextBox></td>	                    	                    
	                </tr>
	                <tr>
	                    <td class="label">Project Total Cost (Donor Currency)</td>
	                    <td><asp:TextBox ID="txtProjectTotalCost" Width ="150px" runat ="server" Height="20px" MaxLength="20" ></asp:TextBox><br />
	                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Required field cannot be left blank" ControlToValidate="txtProjectTotalCost" /><br />
	                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtProjectTotalCost" ErrorMessage="Please enter a valid amount.Accepts only two decimal places(e.g.,12345.00)"
	                     MinimumValue="0" MaximumValue="99999999999999" Type="Currency"  /></td>
	                     <td class="label tooltip">
	                    <a id="wizardaid" class="tooltip" href="javascript:void(0)">Exchange Rate <br />(<small>e.g. how many dollars to the pound</small>)<span
                                        id="wizardsid" class="wizardcustom info">
                                        <img src="../Images/Info_icon.gif" alt="Information" />
                                        The exchange rate is found in the Project Total Cost spreadsheet, cell N3<br />
                                        </span></a>
	                    </td>
	                    <td><asp:TextBox ID="txtExchangeRate" onkeyup="valExchangeRateOnKeyPress(event);" Width ="150px" runat ="server" Height="20px" MaxLength="20"></asp:TextBox><br />
	                    <span id="spnViewWarning" runat="server" class="viewwarn" style="display:none;">Exchange rate <1 � are you sure?</span>
	                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Please enter the Exchange Rate" ControlToValidate="txtExchangeRate" /><br />	  	                    
	                    <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtExchangeRate" ErrorMessage="Please enter a valid amount.Accepts only two decimal places(e.g.,12345.00)" MinimumValue="-99999999999999" 
	                    MaximumValue="99999999999999" Type ="Currency"  />	                    	  	                    
	                </td>
	                </tr>
	                <tr>
	                    <td class="label">Donor Budget (Donor Currency)</td>
	                    <td><asp:TextBox ID="txtDonorBudget" Width ="150px" runat ="server" Height="20px" MaxLength="12"></asp:TextBox><br />
	                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Required field cannot be left blank" ControlToValidate="txtDonorBudget" /><br />
	                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtDonorBudget" ErrorMessage="Please enter a valid amount.Accepts only two decimal places(e.g.,12345.00)" 
	                    MinimumValue="0" MaximumValue="9999999999" Type="Currency"  /></td>	                    	                    
	                </tr>
	                <tr>
	                     <td class="label">Donor Request (Donor Currency)</td>
	                    <td><asp:TextBox ID="txtDonorRequest" Width ="150px" runat ="server" Height="20px" MaxLength="12"></asp:TextBox><br />
	                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Required field cannot be left blank" ControlToValidate="txtDonorRequest" /><br />
	                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtDonorRequest" ErrorMessage="Please enter a valid amount.Accepts only two decimal places(e.g.,12345.00)"
	                     MinimumValue="0" MaximumValue="9999999999" Type="Currency"  /></td>
	                     <td class="label">Donor Currency</td>
	                    <td><asp:DropDownList ID="drpCurrency" runat ="server" Width ="150px" Height="20px" ></asp:DropDownList><br />
	                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Required field cannot be left blank" ControlToValidate="drpCurrency" /></td>	                     	                    
	                </tr>
	                 
	                </table><br /><br />
	                <table id="tblFinancialyear" class="wizardtable" width="50%">
	                   <tr>                 
	                        <td class="label">First financial year of grant income</td>
	                        <td>
	                            <asp:DropDownList ID="ddlFinancialYear" runat ="server" Width ="150px" Height="20px" ></asp:DropDownList><br />
	                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required field cannot be left blank" ControlToValidate="ddlFinancialYear" /></td>
	                    </tr>
	                    <tr>
	                        <td class="label">Total income expected in first year (donor currency)</td>
	                        <td>
	                            <asp:TextBox ID="txtAnnualIncomeCurrency" Width ="150px" runat ="server" Height="20px" MaxLength="12"></asp:TextBox><br />
	                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required field cannot be left blank" ControlToValidate="txtAnnualIncomeCurrency" /><br />
	                            <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="txtAnnualIncomeCurrency" ErrorMessage="Please enter a valid amount.Accepts only two decimal places(e.g.,12345.00)" 
	                                MinimumValue="0" MaximumValue="9999999999" Type="Currency"  /></td>    
	                    </tr>
	            </table><br />
	            <asp:ValidationSummary runat="server" ID="ValidationSummary1" CssClass="valsummary" 
                    HeaderText="It seems you have not entered the appropriate information in the mandatory fields.Please fill in these fields correctly in order to proceed." />
	        </asp:Panel>
	        </asp:Panel>	         

	        <asp:Panel ID="panelThree" runat="server">
	        <div class="wizardheader" ms_positioning="FlowLayout">
			    <h3><img class="icon24" src="../Images/wizard24silver.gif" alt ="" /> New Proposal Wizard - Proposal Phasing For Income Pipeline</h3>
	        </div>	        
            <asp:Panel ID="pnlIncomePipeline" runat="server" CssClass="wizardpanel">
                <table>
                    <tr>
                        <td rowspan="2"><img src="../Images/exclamatory.jpg" alt="exclamatory"/>
                        </td>                
                    </tr>
                    <tr>
                        <td>
                            <p class="warningmsg">IMPORTANT NOTE :</p>            
                            <ul><li class="style">Is the income for this proposal phased across more than one year?</li></ul>
                            <p class="wizardstyle">If so, please click the finish button and then scroll down to the final section of the proposal screen and add additional rows to the "proposal phasing for income pipeline" table.<br /><br />
                            EXAMPLE: If the income is phased across three financial years then you should have three rows in the "proposal phasing for income pipeline" table otherwise the income pipeline report will be incorrect</p>
                        </td>
                    </tr>
                </table> 
            </asp:Panel>            
	         <asp:label ID ="lblPanelOneFeedback" runat="server" CssClass="feedback"></asp:label>
	         <bnbpagecontrol:BnbMessageBox id="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:BnbMessageBox>
	    </asp:Panel> 
	
    <p><uc1:Wizardbuttons id="wizardButtons" runat="server"></uc1:Wizardbuttons></p><br /><br />
	<p><asp:label id="labelHiddenRequestIDUnused" runat="server" Visible="False"></asp:label>
	    <asp:TextBox id="txtHiddenReferrer" runat="server" Visible="False"></asp:TextBox></p>
	</div> 
    </div>
    </form>
</body>
</html>
