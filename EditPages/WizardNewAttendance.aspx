<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="WizardNewAttendance.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.WizardNewAttendance" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>WizardNewAttendance</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent">
				<DIV class="wizardheader" ms_positioning="FlowLayout">
					<H3><IMG class="icon24" src="../Images/wizard24silver.gif"> New&nbsp;
						<asp:label id="uxWizardModeTitle" runat="server"></asp:label>&nbsp;Wizard</H3>
				</DIV>
				<asp:panel id="panelChooseEvent" runat="server" CssClass="wizardpanel" Visible="False">
					<P><EM>This wizard will help you to add a new&nbsp;
							<asp:Label id="uxWizardMode1" runat="server"></asp:Label>&nbsp;record.</EM>
					</P>
					<P>Please select an Event to link to the Individual:</P>
					<P>
						<TABLE class="fieldtable">
							<TR>
								<TD class="label" style="WIDTH: 25%">Individual</TD>
								<TD>
									<asp:Label id="uxIndividualDesc" runat="server"></asp:Label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%; TEXT-ALIGN: right">Event Ref</TD>
								<TD>
									<asp:TextBox id="uxFindEventRef" runat="server" Width="232px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%; TEXT-ALIGN: right">Event Type</TD>
								<TD>
									<bnbdatacontrol:BnbStandaloneLookupControl id="uxFindEventTypeID" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
										ShowBlankRow="True" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboEventTypeWithGroup"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%; TEXT-ALIGN: right">Event Start</TD>
								<TD>
									<bnbdatacontrol:BnbStandaloneDateBox id="uxFindEventBeginDateRange" runat="server" CssClass="datebox" Height="20px" Width="90px"
										DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbStandaloneDateBox>to
									<bnbdatacontrol:BnbStandaloneDateBox id="uxFindEventEndDateRange" runat="server" CssClass="datebox" Height="20px" Width="90px"
										DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbStandaloneDateBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%; TEXT-ALIGN: right"></TD>
								<TD>
									<asp:Button id="uxEventFind" runat="server" Text="Find" onclick="uxEventFind_Click"></asp:Button></TD>
							</TR>
						</TABLE>
					</P>
					<P>
						<bnbdatagrid:BnbDataGridForView id="uxEventGrid" runat="server" CssClass="datagrid" Width="100%" Visible="False"
							GuidKey="tblEvent_EventID" ViewName="vwBonoboFindEvent" DataGridType="RadioButtonList" DisplayNoResultMessageOnly="True"
							DisplayResultCount="false" ShowResultsOnNewButtonClick="false" ViewLinksVisible="False" NewLinkVisible="False"
							NewLinkText="New" ViewLinksText="View" MaxRows="20"></bnbdatagrid:BnbDataGridForView></P>
					<P>
						<asp:label id="uxPanelChooseEventFeedback" runat="server" CssClass="feedback"></asp:label></P>
				</asp:panel><asp:panel id="panelChooseIndividual" runat="server" CssClass="wizardpanel" Visible="False">
					<P><EM>This wizard will help you to add a new&nbsp;
							<asp:Label id="uxWizardMode2" runat="server"></asp:Label>&nbsp;record.</EM>
					</P>
					<P>Please select an Individual&nbsp;to link to the Event:</P>
					<P>
						<TABLE class="fieldtable">
							<TR>
								<TD class="label" style="WIDTH: 25%">Event</TD>
								<TD>
									<asp:Label id="uxEventDesc" runat="server"></asp:Label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%; TEXT-ALIGN: right">Ref No</TD>
								<TD>
									<asp:TextBox id="uxFindIndvRefno" runat="server" Width="64px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%; TEXT-ALIGN: right">Forename</TD>
								<TD>
									<asp:TextBox id="uxFindIndvForename" runat="server" Width="224px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%; TEXT-ALIGN: right">Surname</TD>
								<TD>
									<asp:TextBox id="uxFindIndvSurname" runat="server" Width="224px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%; TEXT-ALIGN: right">Status</TD>
								<TD>
									<bnbdatacontrol:BnbStandaloneLookupControl id="uxFindIndvStatusID" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
										ShowBlankRow="True" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboStatusFull"
										Visible="True"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%; TEXT-ALIGN: right"></TD>
								<TD>
									<asp:Button id="uxFindIndv" runat="server" Text="Find" onclick="uxFindIndv_Click"></asp:Button></TD>
							</TR>
						</TABLE>
					</P>
					<P>
						<bnbdatagrid:BnbDataGridForView id="uxIndvGrid" runat="server" CssClass="datagrid" Width="100%" Visible="False"
							GuidKey="tblIndividualKeyInfo_IndividualID" ViewName="vwBonoboFindIndividualGeneral" DataGridType="RadioButtonList"
							DisplayNoResultMessageOnly="True" DisplayResultCount="false" ShowResultsOnNewButtonClick="false" ViewLinksVisible="False"
							NewLinkVisible="False" NewLinkText="New" ViewLinksText="View" MaxRows="20"></bnbdatagrid:BnbDataGridForView></P>
					<P>
						<asp:Label id="uxEventPersonnelFinishMessage" runat="server" Visible="False">When you have selected a row, press Finish to create the Event Personnel record.</asp:Label></P>
					<P>
						<asp:Label id="uxPanelChooseIndividualFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				</asp:panel><asp:panel id="panelAttendanceOptions" runat="server" CssClass="wizardpanel" Visible="False">
					<P>Please select the initial options for the Attendance record and then press 
						Finish:
					</P>
					<TABLE class="fieldtable">
						<TR>
							<TD class="label" style="WIDTH: 25%">Event</TD>
							<TD>
								<asp:Label id="uxEventDesc2" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 25%">Individual</TD>
							<TD>
								<asp:Label id="uxIndvDesc2" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 25%; HEIGHT: 29px">
								<asp:Label id="uxApplicationLabel" runat="server">Application</asp:Label></TD>
							<TD style="HEIGHT: 29px">
								<asp:DropDownList id="uxApplicationChoose" runat="server" Width="224px"></asp:DropDownList>
								<asp:Label id="uxApplicationChooseOnlyOne" runat="server"></asp:Label><br />
								<asp:Label id="uxAppChooseExplain" runat="server">(Use this to select which of the Volunteer's Applications will be related to the Event)</asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 25%"></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 25%">Situation</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="uxSituationID" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
									DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuAttendanceStatus"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 25%">Situation Date</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneDateBox id="uxSituationDate" runat="server" CssClass="datebox" Height="20px" Width="90px"
									DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbStandaloneDateBox></TD>
						</TR>
					</TABLE>
					<P>
						<asp:Label id="uxPanelAttendanceOptionsFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				</asp:panel>
				<P><uc1:wizardbuttons id="wizardButtons" runat="server"></uc1:wizardbuttons></P>
				<p><asp:textbox id="uxHiddenIndividualID" runat="server" Visible="False" DESIGNTIMEDRAGDROP="92"></asp:textbox><asp:textbox id="uxHiddenEventID" runat="server" Visible="False"></asp:textbox><asp:textbox id="uxHiddenReferrer" runat="server" Visible="False"></asp:textbox><asp:textbox id="uxHiddenMode" runat="server" Visible="False"></asp:textbox></p>
			</div>
		</form>
	</body>
</HTML>
