using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for WizardNewAttendance.
	/// </summary>
	public partial class WizardNewAttendance : System.Web.UI.Page
	{
		protected UserControls.WizardButtons wizardButtons;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{

#if DEBUG
			if(Request.QueryString.ToString() == "")				
				Response.Redirect("WizardNewAttendance.aspx?BnbIndividual=20FD80A3-6D89-4945-806E-0AB5DF60B715");
#endif

			BnbEngine.SessionManager.ClearObjectCache();
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
			if (!this.IsPostBack)
			{
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				// gather data from querystring
				if (Request.QueryString["BnbIndividual"] != null)
					this.IndividualID = new Guid(Request.QueryString["BnbIndividual"]);
				if (Request.QueryString["BnbEvent"] != null)
					this.EventID = new Guid(Request.QueryString["BnbEvent"]);
				if (Request.QueryString["EVP"] != null)
					this.Mode = "evp";
				else
					this.Mode = "att";
				
				if (this.Request.UrlReferrer != null)
					uxHiddenReferrer.Text = this.Request.UrlReferrer.ToString();
				else
					uxHiddenReferrer.Text = "~/Menu.aspx";

				this.InitWizard();
				BonoboWebControls.BnbWebFormManager.LogAction(String.Format("New {0} Wizard",
					uxWizardModeTitle.Text), null, this);
			}

			// the panels that make up the wizard depend on the values from the querystring
			if (this.EventID == Guid.Empty)
				wizardButtons.AddPanel(panelChooseEvent);
			if (this.IndividualID == Guid.Empty)
				wizardButtons.AddPanel(panelChooseIndividual);
			if (this.Mode == "att")
				wizardButtons.AddPanel(panelAttendanceOptions);
		}

		public Guid IndividualID
		{
			get{
				if (uxHiddenIndividualID.Text == "")
					return Guid.Empty;
				else
				 return new Guid(uxHiddenIndividualID.Text);
			}
			set{uxHiddenIndividualID.Text = value.ToString();}
		}

		public Guid EventID
		{
			get{
				if (uxHiddenEventID.Text == "")
					return Guid.Empty;
				else
					return new Guid(uxHiddenEventID.Text);
			}
			set{uxHiddenEventID.Text = value.ToString();}
		}

		/// <summary>
		/// returns what mode the wizard is in:
		/// att = Attendance
		/// evp = Event Personnel
		/// </summary>
		public string Mode
		{
			get{return uxHiddenMode.Text;}
			set{uxHiddenMode.Text = value;}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			wizardButtons.CancelWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
			wizardButtons.ShowPanel +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
			wizardButtons.FinishWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
			wizardButtons.ValidatePanel +=new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			if (e.CurrentPanelHtmlID == panelChooseEvent.ID)
			{
				BnbIndividual chosenIndv = BnbIndividual.Retrieve(this.IndividualID);
				uxIndividualDesc.Text = chosenIndv.InstanceDescription;
			}
			if (e.CurrentPanelHtmlID == panelChooseIndividual.ID)
			{
				BnbEvent chosenEvent = BnbEvent.Retrieve(this.EventID);
				uxEventDesc.Text = chosenEvent.InstanceDescription;
			}
			if (e.CurrentPanelHtmlID == panelAttendanceOptions.ID)
				this.SetupAttendanceOptions();

		}

		private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
		{
			if (e.CurrentPanelHtmlID == panelChooseEvent.ID)
			{
				e.Proceed = this.GetSelectedEvent();
			}
			if (e.CurrentPanelHtmlID == panelChooseIndividual.ID)
			{
				e.Proceed = this.GetSelectedIndividual();
			}

		}

		private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			Response.Redirect(uxHiddenReferrer.Text);
		}

		private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			this.CreateRecord();
		}


		protected void uxEventFind_Click(object sender, System.EventArgs e)
		{
			BnbCriteria eventCrit = new BnbCriteria();
			if (uxFindEventRef.Text != "")
				eventCrit.QueryElements.Add(new BnbTextCondition("tblEvent_EventDescription", uxFindEventRef.Text, BnbTextCompareOptions.StartOfField));
			if (uxFindEventTypeID.SelectedValue.ToString() != "-1")
				eventCrit.QueryElements.Add(new BnbListCondition("tblEvent_EventTypeID", int.Parse(uxFindEventTypeID.SelectedValue.ToString())));
			if (!uxFindEventBeginDateRange.IsEmpty || !uxFindEventEndDateRange.IsEmpty)
				eventCrit.QueryElements.Add(new BnbDateCondition("tblEvent_EventStart", uxFindEventBeginDateRange.Date, uxFindEventEndDateRange.Date));

			// so far, if we dont have any criteria, then reject
			if (eventCrit.QueryElements.Count == 0)
			{
				uxPanelChooseEventFeedback.Text = "Please enter some filter criteria before pressing Find";
				return;
			}
	
			// supply criteria to grid
			uxEventGrid.Criteria = eventCrit;
			uxEventGrid.Visible = true;
			uxEventGrid.DataBind();
		}

		protected void uxFindIndv_Click(object sender, System.EventArgs e)
		{
			BnbCriteria indvCrit = new BnbCriteria();
			if (uxFindIndvRefno.Text != "")
				indvCrit.QueryElements.Add(new BnbTextCondition("tblIndividualKeyInfo_old_indv_id", uxFindIndvRefno.Text, BnbTextCompareOptions.ExactMatch));
			if (uxFindIndvForename.Text != "")
				indvCrit.QueryElements.Add(new BnbTextCondition("tblIndividualKeyInfo_Forename", uxFindIndvForename.Text, BnbTextCompareOptions.StartOfField));
			if (uxFindIndvSurname.Text != "")
				indvCrit.QueryElements.Add(new BnbTextCondition("tblIndividualKeyInfo_Surname", uxFindIndvSurname.Text, BnbTextCompareOptions.StartOfField));
			if (uxFindIndvStatusID.SelectedValue.ToString() != "-1")
				indvCrit.QueryElements.Add(new BnbListCondition("lnkApplicationStatus_StatusID", int.Parse(uxFindIndvStatusID.SelectedValue.ToString())));

			// so far, if we dont have any criteria, then reject
			if (indvCrit.QueryElements.Count == 0)
			{
				uxPanelChooseIndividualFeedback.Text = "Please enter some filter criteria before pressing Find";
				return;
			}
	
			// supply criteria to grid
			uxIndvGrid.Criteria = indvCrit;
			uxIndvGrid.Visible = true;
			uxIndvGrid.DataBind();
			
		}

		private void InitWizard()
		{
			string wizName = null;
			if (this.Mode == "att")
			{
				wizName = "Attendance";
				uxEventPersonnelFinishMessage.Visible = false;
			}
			else
			{
				wizName = "Event Personnel";
				uxEventPersonnelFinishMessage.Visible = true;
			}
			uxWizardModeTitle.Text = wizName;
			uxWizardMode1.Text = wizName;
			uxWizardMode2.Text = wizName;
		}

		private void SetupAttendanceOptions()
		{
			BnbIndividual chosenIndv = BnbIndividual.Retrieve(this.IndividualID);
			uxIndvDesc2.Text = chosenIndv.InstanceDescription;

			BnbEvent chosenEvent = BnbEvent.Retrieve(this.EventID);
			uxEventDesc2.Text = chosenEvent.InstanceDescription;

            // inspect the individuals applications:
			if (chosenIndv.Applications.Count == 0)
			{
				uxApplicationLabel.Visible = false;
				uxApplicationChoose.Visible = false;
				uxAppChooseExplain.Visible = false;
			}
			else
			{
				uxApplicationLabel.Visible = true;
				uxApplicationChoose.Items.Clear();
				BnbApplication currentApp = chosenIndv.CurrentApplication;
				foreach(BnbApplication appLoop in chosenIndv.Applications)
				{
					string appDesc = String.Format("{0} - {1}",
						appLoop.ApplicationOrderFull,
						BnbEngine.LookupManager.GetLookupItemDescription("vlkuBonoboStatusFull", appLoop.CurrentStatus.StatusID));
					ListItem newItem = new ListItem(appDesc, appLoop.ID.ToString());
					if (appLoop == currentApp)
						newItem.Selected = true;

					uxApplicationChoose.Items.Add(newItem);
				}
				if (chosenIndv.Applications.Count == 1)
				{
					// get rid of combo and replace with label
					uxApplicationChooseOnlyOne.Text = uxApplicationChoose.SelectedItem.Text;
					uxApplicationChoose.Visible = false;
					uxApplicationChooseOnlyOne.Visible = true;
					uxAppChooseExplain.Visible = false;
				} 
				else
				{
					// show the user a hint
					uxAppChooseExplain.Visible = true;
				}
			}
			
			uxSituationDate.Date = DateTime.Today;

			uxSituationID.SelectedValue = BnbConst.AttendanceStatus_Booked;
		
		}


		// returns true if a row has been selected. also sets messages and properties 
		private bool GetSelectedIndividual()
		{
			if (uxIndvGrid.SelectedDataRowID == Guid.Empty)
			{
				uxPanelChooseIndividualFeedback.Text = "Please choose an Individual before pressing Next";
				return false;
			}
			else
			{
				this.IndividualID = uxIndvGrid.SelectedDataRowID;
				return true;
			}
		}

		private bool GetSelectedEvent()
		{
			if (uxEventGrid.SelectedDataRowID == Guid.Empty)
			{
				uxPanelChooseEventFeedback.Text = "Please choose an Event before pressing Next";
				return false;
			}
			else
			{
				this.EventID = uxEventGrid.SelectedDataRowID;
				return true;
			}
		}

		private void CreateRecord()
		{
			if (this.Mode == "evp")
			{
				// because there's no extra page and hence no valdiation occuring before
				// they press finish, we have to check here
				if (this.IndividualID == Guid.Empty && !this.GetSelectedIndividual())
					return;
				if (this.EventID == Guid.Empty && !this.GetSelectedEvent())
					return;
	
			}

			BnbIndividual chosenIndv = BnbIndividual.Retrieve(this.IndividualID);
			BnbEvent chosenEvent = BnbEvent.Retrieve(this.EventID);

			if (this.Mode == "att")
			{
				// create a new BnbAttendance
				BnbEditManager em = new BnbEditManager();
				BnbAttendance newAtt = new BnbAttendance(true);
				newAtt.RegisterForEdit(em);
				newAtt.Individual = chosenIndv;
				newAtt.Event = chosenEvent;

				// link to app if specified
				if (uxApplicationLabel.Visible == true)
				{
					Guid appID = new Guid(uxApplicationChoose.SelectedValue.ToString());
					BnbApplication chosenApp = BnbApplication.Retrieve(appID);
					newAtt.Application = chosenApp;
				}

				int situationID = (int)uxSituationID.SelectedValue;
				if (!uxSituationDate.IsValid)
				{
					em.EditCompleted();
					uxPanelAttendanceOptionsFeedback.Text = "Situation Date was not in 'dd/mmm/yyyy' format";
					return;
				}
				DateTime situationDate = uxSituationDate.Date;
				if (situationDate == DateTime.MinValue)
					situationDate = DateTime.Now;

				// att progress will probably get created by default
				BnbAttendanceProgress newAP = null;
				if (newAtt.AttendanceProgresses.Count == 1)
					// get ref to default created one
					newAP = newAtt.AttendanceProgresses[0] as BnbAttendanceProgress;
				else
				{
					// or make a new one
					newAP = new BnbAttendanceProgress(true);
					newAP.RegisterForEdit(em);
					newAP.Attendance = newAtt;
				}
				newAP.AttendanceStatusID = situationID;
				newAP.StatusDate = situationDate;


				try
				{
					em.SaveChanges();
					Response.Redirect(uxHiddenReferrer.Text);
				}
				catch(BnbProblemException pe)
				{
					uxPanelAttendanceOptionsFeedback.Text = "Unfortunately, the Attendance record could not be created due to the following problems: <br/>";
					foreach(BnbProblem p in pe.Problems)
						uxPanelAttendanceOptionsFeedback.Text += p.Message + "<br/>";
				}

			} 
			else
			{
				// create a new BnbEventPersonnel
				BnbEditManager em = new BnbEditManager();
				BnbEventPersonnel newEP = new BnbEventPersonnel(true);
				newEP.RegisterForEdit(em);
				newEP.Individual = chosenIndv;
				newEP.Event = chosenEvent;
			
				try
				{
					em.SaveChanges();
					Response.Redirect(uxHiddenReferrer.Text);
				}
				catch(BnbProblemException pe)
				{
					uxPanelAttendanceOptionsFeedback.Text = "Unfortunately, the Event Personnel record could not be created due to the following problems: <br/>";
					foreach(BnbProblem p in pe.Problems)
						uxPanelAttendanceOptionsFeedback.Text += p.Message + "<br/>";
				}



			}


		}
				


			
	}
}
