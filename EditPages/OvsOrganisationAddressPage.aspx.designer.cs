//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3082
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Frond.EditPages {
    
    
    /// <summary>
    /// OvsOrganisationAddressPage class.
    /// </summary>
    /// <remarks>
    /// Auto-generated class.
    /// </remarks>
    public partial class OvsOrganisationAddressPage {
        
        /// <summary>
        /// literalBeforeCss control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal literalBeforeCss;
        
        /// <summary>
        /// literalAfterCss control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal literalAfterCss;
        
        /// <summary>
        /// Form1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm Form1;
        
        /// <summary>
        /// menuBar control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Frond.UserControls.MenuBar menuBar;
        
        /// <summary>
        /// titleBar control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Frond.UserControls.Title titleBar;
        
        /// <summary>
        /// BnbDataButtons1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::BonoboWebControls.PageControls.BnbDataButtons BnbDataButtons1;
        
        /// <summary>
        /// BnbMessageBox1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::BonoboWebControls.PageControls.BnbMessageBox BnbMessageBox1;
        
        /// <summary>
        /// BnbDomainObjectHyperlink1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::BonoboWebControls.DataControls.BnbDomainObjectHyperlink BnbDomainObjectHyperlink1;
        
        /// <summary>
        /// BnbTextBox1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::BonoboWebControls.DataControls.BnbTextBox BnbTextBox1;
        
        /// <summary>
        /// BnbTextBox2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::BonoboWebControls.DataControls.BnbTextBox BnbTextBox2;
        
        /// <summary>
        /// BnbTextBox3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::BonoboWebControls.DataControls.BnbTextBox BnbTextBox3;
        
        /// <summary>
        /// BnbTextBox4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::BonoboWebControls.DataControls.BnbTextBox BnbTextBox4;
        
        /// <summary>
        /// BnbTextBox5 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::BonoboWebControls.DataControls.BnbTextBox BnbTextBox5;
        
        /// <summary>
        /// BnbTextBox6 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::BonoboWebControls.DataControls.BnbTextBox BnbTextBox6;
        
        /// <summary>
        /// BnbLookupControl1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::BonoboWebControls.DataControls.BnbLookupControl BnbLookupControl1;
        
        /// <summary>
        /// btnShowLocation control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputButton btnShowLocation;
        
        /// <summary>
        /// BnbDataGridForDomainObjectList1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::BonoboWebControls.DataGrids.BnbDataGridForDomainObjectList BnbDataGridForDomainObjectList1;
        
        /// <summary>
        /// headingGeoDetails control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::BonoboWebControls.PageControls.BnbSectionHeading headingGeoDetails;
        
        /// <summary>
        /// showLocation control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Frond.UserControls.GShowLocation showLocation;
        
        /// <summary>
        /// BnbDataButtons2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::BonoboWebControls.PageControls.BnbDataButtons BnbDataButtons2;
    }
}
