using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for MonitorEvaluationPage.
    /// </summary>
    public partial class MonitorEvaluationPage : System.Web.UI.Page
    {

        private BnbWebFormManager bnb = null;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            bnb = new BnbWebFormManager(this, "BnbMonitor");

            // turn on logging for this page
            bnb.LogPageHistory = true;
            //<Integartion>
            bnb.PageNameOverride = "Monitor and Evaluation";
            BnbWorkareaManager.SetPageStyleOfUser(this);

            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null)
                    titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
            }
            //</Integartion>
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            lucEmployerType.PreRender += new EventHandler(lucEmployerType_PreRender);
            //bnblckVSOGoal.PreRender+=new EventHandler(bnblckVSOGoal_PreRender);
            this.PreRender += new EventHandler(MonitorEvaluationPage_PreRender);
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        private void lucEmployerType_PreRender(object sender, EventArgs e)
        {
            lucEmployerType.AlwaysReadOnly = true;
        }

        private void MonitorEvaluationPage_PreRender(object sender, EventArgs e)
        {
            //if placement has no vso goal
            if (((BnbPosition)bnb.ObjectTree.DomainObjectDictionary["BnbPosition"]).PositionObjectives.Count == 0)
            {
                //show the the lookup control
                bnblckVSOGoal.Visible = true;
                bnbdtgVSOGoals.Visible = false;
            }
            else
            {
                //show the datagrid
                bnblckVSOGoal.Visible = false;
                if (bnb.PageState == PageState.New)
                    bnbdtgVSOGoals.DataGridType = BonoboWebControls.DataGrids.DataGridForDomainObjectListType.ViewOnly;
                else
                    bnbdtgVSOGoals.DataGridType = BonoboWebControls.DataGrids.DataGridForDomainObjectListType.Editable;
                bnbdtgVSOGoals.Visible = true;
            }
        }
    }
}
