using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using BonoboWebControls;
using BonoboWebControls.DataGrids;

namespace Frond.EditPages
{
    public partial class WizardWendy : System.Web.UI.Page
    {
        protected UserControls.WizardButtons wizardButtons;
        bool recieveExists = false;
        bool smsExists = false;
        bool emailExists = false;

        #region " Page_Init "

        protected void Page_Init(object sender, EventArgs e)
        {
            wizardButtons.ShowPanel += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
            wizardButtons.ValidatePanel += new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
            wizardButtons.CancelWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
            wizardButtons.FinishWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
        }

        #endregion

        #region " Page_Load "

        protected void Page_Load(object sender, EventArgs e)
        {
            BnbEngine.SessionManager.ClearObjectCache();
            BnbWorkareaManager.SetPageStyleOfUser(this);

            if (!this.IsPostBack)
            {
                BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
                BnbWebFormManager.LogAction("Wendy Wizard", null, this);
                this.bnbAddressLookup1.Visible = false;
                doMapVisibility();
                this.PopulateListBoxes();
                this.ListBoxDefaultValue();
                //code to display the default country
                Guid defaultCountryID = BnbEngine.SessionManager.GetSessionInfo().DefaultCountryID;
                cmbCountry.SelectedValue = defaultCountryID.ToString();
            }
            wizardButtons.Visible = true;
            doLoadGeo();
            wizardButtons.AddPanel(panelGetName);
            wizardButtons.AddPanel(panelDuplicateCheck);
            wizardButtons.AddPanel(panelWendy);
            //Added by TPT
            doAppHyperlinks();
        }

        #endregion

        #region " Events "

        private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            lblPanelOneFeedback.Text = "";
            labelPanelZeroFeedback.Text = "";
            lblPanelEnquierDetailsFeedback.Text = "";

            if (e.CurrentPanelHtmlID == panelGetName.ID)
            {
                this.SetFocus(txtFirstname);
                this.SetupPanelGetName();
            }
            if (e.CurrentPanelHtmlID == panelDuplicateCheck.ID)
            {
                this.SetupPanelDuplicateCheck();

            }
            if (e.CurrentPanelHtmlID == panelWendy.ID)
            {
                this.SetupPanelWendy();
            }
        }



        private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            Response.Redirect("~/Menu.aspx");
        }

        public void saveIndividualAddressDetails(BnbIndividual objIndividual, BnbEditManager em)
        {
            objIndividual.Additional.GCContactUserID = BnbEngine.SessionManager.GetSessionInfo().UserID;
            BnbAddress newAdd = new BnbAddress(true);
            newAdd.RegisterForEdit(em);
            newAdd.AddressLine1 = txtAddressLine1.Text.Trim();
            newAdd.AddressLine2 = txtAddressLine2.Text.Trim();
            newAdd.AddressLine3 = txtAddressLine3.Text.Trim();
            newAdd.AddressLine4 = txtAddressLine4.Text.Trim();
            newAdd.PostCode = txtPostcode.Text.Trim();
            newAdd.County = txtCounty.Text.Trim();
            newAdd.CountryID = new Guid(cmbCountry.SelectedValue.ToString());

            //Capture latitude, longitude values from Google map
            if (HasPermissionToLocateOnMap())
            {
                if (showLocation.Latitude.Trim() != "")
                    newAdd.Latitude = Convert.ToSingle(showLocation.Latitude);
                else
                    //Setting null value for validation
                    newAdd.Latitude = Convert.ToSingle(0.0);

                if (showLocation.Longitude.Trim() != "")
                    newAdd.Longitude = Convert.ToSingle(showLocation.Longitude);
                else
                    //Setting null value for validation
                    newAdd.Longitude = Convert.ToSingle(0.0);
            }

            BnbIndividualAddress iaLink = new BnbIndividualAddress(true);
            iaLink.RegisterForEdit(em);
            // link address to indiv
            objIndividual.IndividualAddresses.Add(iaLink);
            newAdd.IndividualAddresses.Add(iaLink);

            if (txtNumber.Text != "")
                newAdd.SetTelephone(objIndividual, txtNumber.Text);

            // add the email, if specified
            if (txtEmail.Text.Trim() != "")
            {
                BnbContactNumber newEmail = new BnbContactNumber(true);
                newEmail.RegisterForEdit(em);
                objIndividual.ContactNumbers.Add(newEmail);
                newEmail.ContactNumberTypeID = BnbConst.ContactNumber_Email;
                newEmail.ContactNumber = txtEmail.Text.Trim();
            }
        }

        private BnbIndividual RegisterIndividualObjects(BnbEditManager em)
        {
            BnbIndividual objIndividual;
            objIndividual = new BnbIndividual(true);
            objIndividual.RegisterForEdit(em);
            objIndividual.Forename = txtForename.Text.Trim();
            objIndividual.Surname = txtSurnamePanel3.Text.Trim();
            objIndividual.Title = cmbTitle.SelectedValue.ToString();
            objIndividual.DateOfBirth = Convert.ToDateTime(txtDOB.Text.Trim());
            objIndividual.Gender = cmbSex.SelectedValue.ToString();
            saveIndividualAddressDetails(objIndividual, em);
            return objIndividual;
        }

        private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            bool flag = false;

            try
            {
                //Validations
                string validationMessage = this.WendyValidations();

                if (validationMessage.ToUpper() == "SUCCESS")
                {
                    if (this.IndividualID == Guid.Empty)
                    {
                        //Creating a new enquirer in Frond
                        BnbIndividual objIndv = this.CreateNewEnquirer();
                        ////Creating a new enquirer in Egateway
                        flag = this.CreateEnquirerEgateway(objIndv.Forename, objIndv.Surname, objIndv.Old_indv_id);
                    }
                    else
                    {
                        flag = ExsistingIndividual();                      
                    }
                }
                else
                {
                    lblPanelEnquierDetailsFeedback.Text = validationMessage;
                    return;
                }

                if (flag)
                {
                    if (chkBookEvents.Checked)
                        Response.Redirect("WizardNewAttendance.aspx?BnbIndividual=" + this.IndividualID.ToString().ToUpper());
                    else
                        Response.Redirect("IndividualPage.aspx?BnbIndividual=" + this.IndividualID.ToString());
                }
                else
                {
                    lblPanelEnquierDetailsFeedback.Text = "Synchronisation error occured(E-Gateway update failed). Please contact system administrator.";
                    wizardButtons.Visible = false;
                }
            }
            catch (BnbProblemException pe)
            {
                lblPanelEnquierDetailsFeedback.Text = "Unfortunately, the  enquirer data could not saved due to the following problems: <br/>";
                foreach (BnbProblem p in pe.Problems)
                    lblPanelEnquierDetailsFeedback.Text += p.Message + "<br/>";
            }
            catch (Exception err)
            {
                int ticketID = BnbWebFormManager.LogErrorToDatabase(err);
                lblPanelEnquierDetailsFeedback.Text = String.Format("Status: Error: {0} (TicketID {1})",
                    err.Message, ticketID.ToString());
            }
        }

        /// <summary>
        /// If the current panel represents the list of individuals found with previous panel criteria then
        /// If the individual is not found then create it with wizardnewindividual 
        /// otherwise continue with this wizard.
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
        {
            if (e.CurrentPanelHtmlID == panelGetName.ID)
            {
                if (txtFirstname.Text.Trim() == "" || txtSurname.Text.Trim() == "")
                {
                    e.Proceed = false;
                    labelPanelZeroFeedback.Text = "Please enter a Forename and Surname before pressing Next";
                }
            }

            if (e.CurrentPanelHtmlID == panelDuplicateCheck.ID)
            {
                e.Proceed = true;
                this.IndividualID = bdgIndividuals.SelectedDataRowID;
            }

            if (e.CurrentPanelHtmlID == panelWendy.ID)
            {
                e.Proceed = true;
            }
        }

        #endregion events

        #region " SetupPanelGetName "

        public void SetupPanelGetName()
        {
            // clear individualid to allow repeated searches
            bdgIndividuals.SelectedDataRowID = Guid.Empty;
        }

        #endregion

        #region " SetupPanelDuplicateCheck "

        public void SetupPanelDuplicateCheck()
        {
            string forenameBit = txtFirstname.Text.Trim();
            if (forenameBit.Length > 5)
                forenameBit = forenameBit.Substring(0, 5);

            BnbTextCondition surnameMatch = new BnbTextCondition("tblIndividualKeyInfo_Surname", txtSurname.Text.Trim(), BnbTextCompareOptions.StartOfField);
            BnbTextCondition forenameMatch = new BnbTextCondition("tblIndividualKeyInfo_Forename", forenameBit, BnbTextCompareOptions.StartOfField);

            BnbCriteria dupCriteria = new BnbCriteria(surnameMatch);
            dupCriteria.QueryElements.Add(forenameMatch);
            bdgIndividuals.Criteria = dupCriteria;
            bdgIndividuals.MaxRows = 30;

            if (bdgIndividuals.ResultCount > 0)
            {
                subpanelChoose.Visible = true;
                lblNoDuplicates.Visible = false;
            }
            else
            {
                subpanelChoose.Visible = false;
                lblNoDuplicates.Visible = true;
            }
        }

        private void doAppHyperlinks()
        {
            // add extra hyperlinks into the two event grids
            BnbDataGridHyperlink appHyperCol = new BnbDataGridHyperlink();
            appHyperCol.ColumnNameToRenderControl = "Custom_AppStatusForWendy";
            appHyperCol.HrefTemplate = "ApplicationPage.aspx?BnbApplication={0}";
            appHyperCol.KeyColumnNames = new string[] { "tblApplication_applicationID" };
            bdgIndividuals.ColumnControls.Add(appHyperCol);
        }

        #endregion

        #region " SetupPanelWendy "

        public void SetupPanelWendy()
        {
            cmbVSOType.SelectedValue = BnbConst.VSOType_VSOStandard;
            cmbStatus.SelectedValue = BnbConst.Status_EnquirerEnquirer;
            cmbEnquiryMethod.SelectedValue = BnbConst.ApplicationSource_Phone;
            cmbStatusGroupEnquirer.SelectedValue = BnbConst.StatusGroup_Enquirer;
            lblEnquiryDate.Text = DateTime.Now.Date.ToString("dd/MMM/yyyy");

            EnableAllFields();

            if (this.IndividualID != Guid.Empty)
            {
                BnbIndividual tempIndv = BnbIndividual.Retrieve(this.IndividualID);
                txtSurnamePanel3.Text = tempIndv.Surname;
                txtForename.Text = tempIndv.Forename;
                cmbTitle.SelectedValue = tempIndv.Title;
                cmbCountry.SelectedValue = tempIndv.CurrentIndividualAddress.Address.CountryID;
                txtPostcode.Text = tempIndv.CurrentIndividualAddress.Address.PostCode;
                txtAddressLine1.Text = tempIndv.CurrentIndividualAddress.Address.AddressLine1;
                txtAddressLine2.Text = tempIndv.CurrentIndividualAddress.Address.AddressLine2;
                txtAddressLine3.Text = tempIndv.CurrentIndividualAddress.Address.AddressLine3;
                txtAddressLine4.Text = tempIndv.CurrentIndividualAddress.Address.AddressLine4;
                txtCounty.Text = tempIndv.CurrentIndividualAddress.Address.County;
                txtNumber.Text = tempIndv.CurrentIndividualAddress.Address.GetTelephone(tempIndv);
                if (!(tempIndv.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Email, tempIndv.ID) == null))
                    txtEmail.Text = tempIndv.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Email, tempIndv.ID).ContactNumber;
                else
                    txtEmail.Text = string.Empty;
                txtDOB.Date = tempIndv.DateOfBirth;
                cmbSex.SelectedValue = tempIndv.Gender;
                showLocation.Latitude = tempIndv.CurrentIndividualAddress.Address.Latitude.ToString();
                showLocation.Longitude = tempIndv.CurrentIndividualAddress.Address.Longitude.ToString();
                SetGeoModeForView("new/edit");
                //Code added for Jira Call VAF - 909
                if(!(txtDOB.Date == BnbDomainObject.NullDateTime))
                    txtDOB.Enabled = false;
                this.SetFocus(txtForename);
              
            }
            else
            {
                SetGeoMode("new/edit");
                txtSurnamePanel3.Text = txtSurname.Text.Trim();
                txtForename.Text = txtFirstname.Text.Trim();
                this.SetFocus(txtForename);
            }
        }

        #endregion

        #region " cmdFind_Click "

        protected void cmdFind_Click(object sender, System.EventArgs e)
        {
            //display the address lookup control and fill in the necessary properties
            bnbAddressLookup1.Visible = true;
            bnbAddressLookup1.CountryID = new Guid(cmbCountry.SelectedValue.ToString());
            bnbAddressLookup1.PostCode = txtPostcode.Text.Trim();
            bnbAddressLookup1.Line1 = txtAddressLine1.Text.Trim();
            bnbAddressLookup1.PerformSearch();
            bnbAddressLookup1.Visible = true;

        }

        #endregion

        #region " bnbAddressLookup1_SelectedIndexChanged "

        protected void bnbAddressLookup1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            txtAddressLine1.Text = bnbAddressLookup1.Line1;
            txtAddressLine2.Text = bnbAddressLookup1.Line2;
            txtAddressLine3.Text = bnbAddressLookup1.Line3;
            txtAddressLine4.Text = bnbAddressLookup1.Town;
            txtCounty.Text = bnbAddressLookup1.County;
            txtPostcode.Text = bnbAddressLookup1.PostCode;
        }

        #endregion

        #region " HasPermissionToLocateOnMap "
        /// <summary>
        /// Check permisssion to access the Google map functionality
        /// </summary>
        private bool HasPermissionToLocateOnMap()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_LocateAddressOnMap))
                return true;
            else
                return false;
        }

        #endregion

        #region "doMapVisibility"
        /// <summary>
        /// Added newly for role based access
        /// </summary>
        private void doMapVisibility()
        {
            if (HasPermissionToLocateOnMap())
            {
                showLocation.Visible = true;
                btnShowLocation.Visible = true;
            }
            else
            {
                showLocation.Visible = false;
                btnShowLocation.Visible = false;
            }
        }

        #endregion

        #region " doLoadGeo "
        /// <summary>
        /// //For capturing Geographical locations
        /// </summary>
        private void doLoadGeo()
        {
            if (HasPermissionToLocateOnMap())
            {
                StringBuilder script = new StringBuilder();
                script.Append("pageName ='WizardWendy';");
                // Page mode

                if (!(showLocation.Latitude.Trim() == string.Empty))
                {
                    ViewState["Latittude"] = showLocation.Latitude;
                    ViewState["Longitude"] = showLocation.Longitude;
                }

                if (!(ViewState["Latittude"] == null))
                {
                    string latitude = ViewState["Latittude"].ToString();
                    string longitude = ViewState["Longitude"].ToString();
                    script.Append("latVal = " + latitude + ";lngVal = " + longitude + ";");
                }
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "WNELoadGeo", script.ToString(), true);
            }
        }

        #endregion

        #region " IndividualID "

        private Guid IndividualID
        {
            get
            {
                if (lblHiddenIndividualID.Value.Trim().Length > 0)
                    return new Guid(lblHiddenIndividualID.Value);
                else
                    return Guid.Empty;
            }
            set { lblHiddenIndividualID.Value = value.ToString(); }
        }

        #endregion
 
        #region " EnableAllFields "

        private void EnableAllFields()
        {
            cmbSkillCode.Enabled = true;
            txtFurtherSkillInfo.Enabled = true;
            cmbVSOType.Enabled = true;
            cmbStatus.Enabled = true;
            cmbAction.Enabled = true;
            cmbMainMotivation.Enabled = true;
            cmbAffliation.Enabled = true;
            cmbBestExplains.Enabled = true;
            cmbPassionateAbout.Enabled = true;
            cmbSupportOtherCharities.Enabled = true;
            cmbSupportTheseCharities.Enabled = true;
            cmbActiveCommunity.Enabled = true;
            txtSourceOther.Enabled = true;
            cmbSources.Enabled = true;
            cmbRecieveSMS.Enabled = true;
            cmbRecieveRangeOfWays.Enabled = true;
            cmbRecieveEmails.Enabled = true;
            cmbVSOPartnerships.Enabled = true;
            cmbSex.Enabled = true;
            txtDOB.Enabled = true;
            cmbTitle.Enabled = true;
            txtEmail.Enabled = true;
            txtNumber.Enabled = true;
            txtCounty.Enabled = true;
            txtAddressLine4.Enabled = true;
            txtAddressLine3.Enabled = true;
            txtAddressLine2.Enabled = true;
            txtAddressLine1.Enabled = true;
            bnbAddressLookup1.Enabled = true;
            cmdFindAddressLine1.Enabled = true;
            cmdFind.Enabled = true;
            txtPostcode.Enabled = true;
            cmbCountry.Enabled = true;
            cmbEnquiryMethod.Enabled = true;
        }

        #endregion

        #region " SetGeoMode "

        //For capturing Geographical locations
        //Set page mode to control GMap 
        private void SetGeoMode(string mode)
        {
            if (HasPermissionToLocateOnMap())
            {
                string strScript = "mode='" + mode + "';";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "SetMode", strScript, true);
            }

        }

        #endregion

        #region " SetGeoModeForView "

        //For capturing Geographical locations
        //Set page mode to control GMap 
        private void SetGeoModeForView(string mode)
        {

            if (HasPermissionToLocateOnMap())
            {
                if (!(showLocation.Latitude.Trim() == string.Empty))
                {
                    ViewState["Latittude"] = showLocation.Latitude;
                    ViewState["Longitude"] = showLocation.Longitude;
                }

                string strScript = "mode='" + mode + "';";
                strScript += "latVal = " + showLocation.Latitude.Trim() + ";lngVal = " + showLocation.Longitude.Trim() + ";";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "SetMode", strScript, true);
            }

        }

        #endregion

        #region " Wendy Validations "
        private string WendyValidations()
        {

            if (txtForename.Text.Trim().Equals(string.Empty))
            {
                Page.SetFocus(txtForename);
                return "Please enter a forename.";
            }

            if (txtSurnamePanel3.Text.Trim().Equals(string.Empty))
            {
                Page.SetFocus(txtSurnamePanel3);
                return "Please enter a surname.";
            }

            if (cmbTitle.SelectedValue.ToString().Trim().Equals(string.Empty))
            {
                Page.SetFocus(cmbTitle);
                return "Please select a Title.";
            }

            if (cmbCountry.SelectedValue.ToString() == Guid.Empty.ToString())
            {
                Page.SetFocus(cmbCountry);
                return "Please select a Country.";
            }

            if (txtPostcode.Text.Trim().Equals(string.Empty))
            {
                Page.SetFocus(txtPostcode);
                return "Please enter a Postcode.";
            }

            if (txtAddressLine1.Text.Trim().Equals(string.Empty))
            {
                Page.SetFocus(txtAddressLine1);
                return "Please enter AddressLine1.";
            }

            if (txtAddressLine4.Text.Trim().Equals(string.Empty))
            {
                Page.SetFocus(txtAddressLine4);
                return "Please enter Postal / Town.";
            }

            if (txtNumber.Text.Trim().Equals(string.Empty))
            {
                Page.SetFocus(txtNumber);
                return "Please enter Telephone Number.";
            }

            if (txtEmail.Text.Trim().Equals(string.Empty))
            {
                Page.SetFocus(txtEmail);
                return "Please enter Email.";
            }
            else
            {
                Regex emailCheck = new Regex("[^0-9a-z\\@\\.\\-_]");
                if (emailCheck.Match(txtEmail.Text.ToLower()).Success)
                    return "Email contains illegal characters";

                if (txtEmail.Text.IndexOf("@") == -1)
                    return "Email should contain an @ sign";

                if (txtEmail.Text.IndexOf(".") == -1)
                    return "Email should contain a '.'";
            }
            if (txtDOB.Enabled)
                if (txtDOB.Text.Trim().Equals(DateTime.MinValue.ToString("dd/MMM/yyyy")))
                {
                    Page.SetFocus(txtDOB);
                    return "Please enter Date of birth in 'dd/MMM/yyyy' format.";
                }

            if (txtDOB.Enabled)
                if (!(txtDOB.IsValid))
                {
                    Page.SetFocus(txtDOB);
                    return "Date of Birth was not in 'dd/MMM/yyyy' format.";
                }

            if (cmbSex.SelectedValue.ToString().Trim().Equals(string.Empty))
            {
                Page.SetFocus(cmbSex);
                return "Please select a Sex.";
            }

            if (cmbRecieveRangeOfWays.SelectedValue.ToString().Trim().Equals(string.Empty))
            {
                Page.SetFocus(cmbRecieveRangeOfWays);
                return "Please select 'Happy to Receive range of ways'.";
            }

            if (cmbRecieveEmails.SelectedValue.ToString().Trim().Equals(string.Empty))
            {
                Page.SetFocus(cmbRecieveEmails);
                return "Please select 'Happy to Receive Emails'.";
            }

            if (cmbRecieveSMS.SelectedValue.ToString().Trim().Equals(string.Empty))
            {
                Page.SetFocus(cmbRecieveSMS);
                return "Please select 'Happy to Receive SMS'.";
            }

            if (cmbSources.SelectedValue.ToString().Trim() == Guid.Empty.ToString())
            {
                Page.SetFocus(cmbSources);
                return "Please select a Source.";
            }

            if (cmbSkillCode.SelectedValue.ToString().Trim().Equals("-1"))
            {
                Page.SetFocus(cmbSkillCode);
                return "Please select a Skill Code.";
            }

            if (cmbVSOType.SelectedValue.ToString().Trim().Equals("-1"))
            {
                Page.SetFocus(cmbVSOType);
                return "Please select a VSO Type.";
            }

            if (cmbStatus.SelectedValue.ToString().Trim().Equals(string.Empty))
            {
                Page.SetFocus(cmbStatus);
                return "Please select a Status.";
            }

            if (cmbMainMotivation.SelectedValue.ToString().Trim() == Guid.Empty.ToString())
            {
                Page.SetFocus(cmbMainMotivation);
                return "Please select a Motivation.";
            }

            if (cmbAffliation.SelectedValue.ToString().Trim() == Guid.Empty.ToString())
            {
                Page.SetFocus(cmbAffliation);
                return "Please select a Region Affliation.";
            }

            if (cmbBestExplains.SelectedValue.ToString().Trim() == Guid.Empty.ToString())
            {
                Page.SetFocus(cmbBestExplains);
                return "Please select 'What best explains this affliation?'";
            }

            if (cmbPassionateAbout.SelectedValue.ToString().Trim().Equals("-1"))
            {
                Page.SetFocus(cmbPassionateAbout);
                return "Please select 'Which development issues do you feel passionate about?'";
            }

            if (cmbSupportOtherCharities.SelectedValue.ToString().Trim().Equals("-1"))
            {
                Page.SetFocus(cmbSupportOtherCharities);
                return "Please select 'If you support other charities, what are they involved in?'";
            }

            if (cmbSupportTheseCharities.SelectedValue.ToString().Trim().Equals("-1"))
            {
                Page.SetFocus(cmbSupportTheseCharities);
                return "Please select 'How do you support these charities?'";
            }

            if (cmbActiveCommunity.SelectedValue.ToString().Trim().Equals("-1"))
            {
                Page.SetFocus(cmbActiveCommunity);
                return "Please select 'In what ways are you active in your community?'";
            }

            return "Success";
        }
        #endregion

        #region " New Enquirer - Frond "
        private BnbIndividual CreateNewEnquirer()
        {

            BnbEditManager em = new BnbEditManager();
            BnbIndividual objIndividual = this.RegisterIndividualObjects(em);
            try
            {
                this.SaveIndividualDetails(objIndividual, em);
                return objIndividual;
            }
            catch (BnbProblemException pe)
            {
                throw pe;
            }
            catch (Exception err)
            {
                throw err;
            }

        }
        #endregion

        #region " Enquirer - Egateway "
        /// <summary>
        /// Method used to create a enquirer record in Egateway by consuming the FrondIntegration webservice
        /// </summary>
        private bool CreateEnquirerEgateway(string foreName, string surName, string refNo)
        {
            DateTime dob = Convert.ToDateTime(txtDOB.Text.Trim());
            Guid titleID = GetTitleID(cmbTitle.SelectedValue.ToString());
            Guid countryID = new Guid(cmbCountry.SelectedValue.ToString());
            int sex = GetSexID(cmbSex.SelectedValue.ToString());
            Guid partnershipID = new Guid(cmbVSOPartnerships.SelectedValue.ToString());
            Guid motivationID = new Guid(cmbMainMotivation.SelectedValue.ToString());
            Guid countryRegionID = new Guid(cmbAffliation.SelectedValue.ToString());
            Guid reasonID = new Guid(cmbBestExplains.SelectedValue.ToString());

            string interestID = GetSelectedValues(cmbPassionateAbout);
            string charityID = GetSelectedValues(cmbSupportOtherCharities);
            string charitySupportID = GetSelectedValues(cmbSupportTheseCharities);
            string civilManagementID = GetSelectedValues(cmbActiveCommunity);

            string castanetUsername = ConfigurationManager.AppSettings["CastanetAdminUsername"];
            string castanetPassword = ConfigurationManager.AppSettings["CastanetAdminPassword"];

            //Logging for no data loss
            string egateWayDetails = "WendyNewEnquirer: Fn: " +
                                     foreName + ",Sn: " + surName + ",Titl: " + titleID.ToString() +
                                      ",Countr:" + countryID.ToString() + ",Postc:" + txtPostcode.Text.Trim() + ",Ad1:" +
                                      txtAddressLine1.Text.Trim() + ",Ad2:" + txtAddressLine2.Text.Trim() +
                                      ",Ad3:" + txtAddressLine3.Text.Trim() + ",Ad4:" + txtAddressLine4.Text.Trim() + ",town:" + txtCounty.Text.Trim() + ",email:" +
                                      txtEmail.Text.Trim() + "," + dob.ToString() + "," + sex.ToString() +
                                       ",partner:" + partnershipID.ToString() +
                                      ",Occupa:" + txtFurtherSkillInfo.Text.Trim() + ",Motiv:" + motivationID.ToString() +
                                      ",ContReg:" + countryRegionID.ToString() + ",ContReas:" + reasonID.ToString() + ",Intrest:" +
                                       interestID.ToString() + ",Charit:" + charityID.ToString() + ",CharOther:" + charitySupportID.ToString() +
                                       ",CivilEng:" + civilManagementID.ToString() + ",IndId:" + this.IndividualID.ToString() +
                                       ",Phone:" + txtNumber.Text.Trim() + ",RefNo:" + refNo;

            BnbWebFormManager.LogAction(egateWayDetails, null, this);

            WsOnlineForms.FrondIntegration wsEgateway = new WsOnlineForms.FrondIntegration();            

            WsOnlineForms.BnbWebServiceProcessInfo wsProcess = new WsOnlineForms.BnbWebServiceProcessInfo();

            // object to keep track of progress
            Frond.EditPages.EGateway.BnbFormsProcessUtils.BnbProcessInfo processInfo = new Frond.EditPages.EGateway.BnbFormsProcessUtils.BnbProcessInfo();

            processInfo.AddProgressMessage("Connecting webservice to insert wendy data to ApplicationForm database");

            if (System.Configuration.ConfigurationManager.AppSettings["WebServiceDefaultCredentials"] != null)
            {
                try
                {
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["WebServiceDefaultCredentials"]) == true)
                    {
                        wsEgateway.PreAuthenticate = true;
                        wsEgateway.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    }
                }
                catch
                {
                    throw new ApplicationException("Please configure a valid 'WebServiceDefaultCredentials' in Appsettings");
                }
            }
            else
                throw new ApplicationException("Please configure 'WebServiceDefaultCredentials' in Appsettings");

            //Web service call to pass screen values to Application form database
            wsProcess = wsEgateway.WendyEnquirer(castanetUsername, castanetPassword, foreName, surName, titleID, countryID,
                                              txtPostcode.Text.Trim(), txtAddressLine1.Text.Trim(), txtAddressLine2.Text.Trim(), txtAddressLine3.Text.Trim(),
                                              txtAddressLine4.Text.Trim(), txtCounty.Text.Trim(), txtEmail.Text.Trim(), dob, sex, partnershipID, txtFurtherSkillInfo.Text.Trim(),
                                              motivationID, countryRegionID, reasonID, interestID, charityID, charitySupportID, civilManagementID,
                                              this.IndividualID, txtNumber.Text.Trim(), refNo);
            if (!wsProcess.Success)
            {
                // add progress messages from web service into our own list:
                processInfo.AddProgressMessage("Web Service call to insert wendy data failed. Details follow:");

                if (wsProcess.CastanetErrorTicketID > 0)
                    processInfo.AddProgressMessage(String.Format("ErrorTicketID in ApplicationForm database: {0}", wsProcess.CastanetErrorTicketID));
            }
            else
            {
                processInfo.AddProgressMessage("Wendy data successfully inserted to ApplicationForm database");
            }

            return wsProcess.Success;
           

        }
        #endregion        

        #region " Exsisting Individual Address Change Frond "
        private BnbIndividual ExsistingIndividualUpdate(BnbIndividual objIndividual, BnbEditManager em)
        {
            objIndividual.Forename = txtForename.Text.Trim();
            objIndividual.Surname = txtSurnamePanel3.Text.Trim();
            objIndividual.Title = cmbTitle.SelectedValue.ToString();
            objIndividual.DateOfBirth = Convert.ToDateTime(txtDOB.Text.Trim());
            objIndividual.Gender = cmbSex.SelectedValue.ToString();

            BnbAddress currentAddress = objIndividual.CurrentIndividualAddress.Address;
            currentAddress.RegisterForEdit(em);
            currentAddress.AddressLine1 = txtAddressLine1.Text.Trim();
            currentAddress.AddressLine2 = txtAddressLine2.Text.Trim();
            currentAddress.AddressLine3 = txtAddressLine3.Text.Trim();
            currentAddress.AddressLine4 = txtAddressLine4.Text.Trim();
            currentAddress.PostCode = txtPostcode.Text.Trim();
            currentAddress.County = txtCounty.Text.Trim();
            currentAddress.CountryID = new Guid(cmbCountry.SelectedValue.ToString());
            currentAddress.SetTelephone(objIndividual, txtNumber.Text);

            //Condition for individuals with no email contact type
            BnbContactNumber indvEmail = objIndividual.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Email, objIndividual.ID);
            if (indvEmail == null)
            {
                BnbContactNumber cn = new BnbContactNumber(true);
                cn.RegisterForEdit(em);
                cn.ContactNumberTypeID = BnbConst.ContactNumber_Email;
                cn.ContactNumber = txtEmail.Text.Trim();
                objIndividual.ContactNumbers.Add(cn);
            }
            else
            {
                indvEmail.RegisterForEdit(em);
                indvEmail.ContactNumberTypeID = BnbConst.ContactNumber_Email;
                indvEmail.ContactNumber = txtEmail.Text.Trim();
            }
            return objIndividual;
        }
        #endregion        

        #region " Exsisting Individual "
        private bool ExsistingIndividual()
        {
            bool successFlag = false;
            try
            {
                BnbIndividual indv = ExsistingIndividualFrond();

                successFlag = CreateEnquirerEgateway(indv.Forename, indv.Surname, indv.Old_indv_id);

                return successFlag;
            }
            catch (BnbProblemException pe)
            {
                throw pe;

            }
            catch (Exception err)
            {
                throw err;
            }
        }
        #endregion        

        #region " Exsisting Individual Frond "
        private BnbIndividual ExsistingIndividualFrond()
        {
            BnbEditManager em = new BnbEditManager();
            BnbIndividual indv = BnbIndividual.Retrieve(this.IndividualID);
            indv.RegisterForEdit(em);
            indv = ExsistingIndividualUpdate(indv, em);
            

            this.SaveIndividualDetails(indv, em);

            return indv;

        }
        #endregion

        #region " Get Title ID "

        private Guid GetTitleID(string title)
        {
            BnbLookupDataTable dtbResult = BnbEngine.LookupManager.GetLookup("lkuTitle");
            DataRow[] tilteRows = dtbResult.Select("Description = '" + cmbTitle.SelectedValue.ToString() + "'");
            int titleIDx = -1;
            Guid titleID = new Guid();
            if (tilteRows.Length > 0)
            {
                titleIDx = dtbResult.Columns["GuidID"].Ordinal;
                titleID = new Guid(tilteRows[0].ItemArray[titleIDx].ToString());
            }
            return titleID;
        }
        #endregion

        #region " Get Sex ID "

        private int GetSexID(string sex)
        {
            BnbLookupDataTable dtbResult = BnbEngine.LookupManager.GetLookup("lkuGender");
            DataRow[] sexRows = dtbResult.Select("Abbreviation = '" + cmbSex.SelectedValue.ToString() + "'");
            int sexIDx = -1;
            int sexID = -1;
            if (sexRows.Length > 0)
            {
                sexIDx = dtbResult.Columns["IntID"].Ordinal;
                sexID = Convert.ToInt32(sexRows[0].ItemArray[sexIDx].ToString());
            }
            return sexID;
        }
        #endregion

        #region " Individual details "
        private void SaveIndividualDetails(BnbIndividual indvdetails, BnbEditManager em)
        {
            indvdetails = this.IndividualContactBlockUpdate(indvdetails, em);

            BnbApplication objApplication = new BnbApplication(true);
            objApplication.RegisterForEdit(em);

            //add the application to the individual
            indvdetails.Applications.Add(objApplication);
            BnbIndividualContactBlock objIndividualContactBlock = null;
            if (indvdetails.IndividualContactBlocks.Count <= 0)
            {
                objIndividualContactBlock = new BnbIndividualContactBlock(true);
                objIndividualContactBlock.RegisterForEdit(em);
                objIndividualContactBlock.ContactBlockTypeID = BnbConst.ContactBlockType_AllowSelectedMail;
                indvdetails.IndividualContactBlocks.Add(objIndividualContactBlock);
            }
            else
            {
                objIndividualContactBlock = (BnbIndividualContactBlock)indvdetails.IndividualContactBlocks[0];
            }

            if (!(recieveExists))
            {

                if (cmbRecieveRangeOfWays.SelectedValue.ToString().ToUpper() == "YES")
                {
                    BnbSelectedContact objSelectedContact = new BnbSelectedContact(true);
                    objSelectedContact.RegisterForEdit(em);
                    objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_HappyToReceiveInfoRangeOfWaysTheyCanHelpVso;// 1019;
                    objIndividualContactBlock.SelectedContacts.Add(objSelectedContact);
                }
                else
                {
                    BnbSelectedContact objSelectedContact = new BnbSelectedContact(true);
                    objSelectedContact.RegisterForEdit(em);
                    objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_VolunteeringCommunicationOnly;// 1012;
                    objIndividualContactBlock.SelectedContacts.Add(objSelectedContact);

                }
            }
            if (!(emailExists))
            {

                if (cmbRecieveEmails.SelectedValue.ToString().ToUpper() == "YES")
                {
                    BnbSelectedContact objSelectedContact = new BnbSelectedContact(true);
                    objSelectedContact.RegisterForEdit(em);
                    objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_OnlineEmailOptIn;// 1023;
                    objIndividualContactBlock.SelectedContacts.Add(objSelectedContact);
                }
                else
                {
                    BnbSelectedContact objSelectedContact = new BnbSelectedContact(true);
                    objSelectedContact.RegisterForEdit(em);
                    objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_OnlineEmailOptOut;//1024;
                    objIndividualContactBlock.SelectedContacts.Add(objSelectedContact);
                }
            }
            if (!(smsExists))
            {
                if (cmbRecieveSMS.SelectedValue.ToString().ToUpper() == "YES")
                {
                    BnbSelectedContact objSelectedContact = new BnbSelectedContact(true);
                    objSelectedContact.RegisterForEdit(em);
                    objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_OnlineSMSOptIn;//1021;
                    objIndividualContactBlock.SelectedContacts.Add(objSelectedContact);
                }
                else
                {
                    BnbSelectedContact objSelectedContact = new BnbSelectedContact(true);
                    objSelectedContact.RegisterForEdit(em);
                    objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_OnlineSMSOptOut;//1022;
                    objIndividualContactBlock.SelectedContacts.Add(objSelectedContact);
                }
            }

            objApplication.SourceOther = txtSourceOther.Text.Trim();
            objApplication.RecruitmentBaseID = BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID;
            if (!cmbEnquiryMethod.SelectedValue.ToString().Equals("-1"))
                objApplication.EnquiryMethodID = Convert.ToInt32(cmbEnquiryMethod.SelectedValue);
            BnbApplicationSource objApplicationSource = new BnbApplicationSource(true);
            objApplicationSource.RegisterForEdit(em);
            objApplicationSource.SourceID = new Guid(cmbSources.SelectedValue.ToString());
            objApplication.ApplicationSources.Add(objApplicationSource);

            //Code added for Jira Call VAF - 908
            BnbIndividualSkill objIndividualSkill;
            int skillCount = indvdetails.IndividualSkills.Count;
            for (int idx = 0; idx < skillCount; idx++)
            {
                objIndividualSkill = (BnbIndividualSkill) indvdetails.IndividualSkills[idx];
                if (cmbSkillCode.SelectedValue.ToString() == objIndividualSkill.SkillID.ToString())
                {
                    objIndividualSkill.RegisterForEdit(em);
                    indvdetails.IndividualSkills.RemoveAndDelete(objIndividualSkill);
                    break;
                }
            }          

            objIndividualSkill = new BnbIndividualSkill(true);
            objIndividualSkill.RegisterForEdit(em);
            objIndividualSkill.SkillID = Convert.ToInt32(cmbSkillCode.SelectedValue);
            objIndividualSkill.Order = 1;
            indvdetails.IndividualSkills.Add(objIndividualSkill);
            indvdetails.IndividualSkills.SetLeadID(objIndividualSkill.ID, em);

            if (cmbVSOType.SelectedValue != null)
                objApplication.VSOTypeID = Convert.ToInt32(cmbVSOType.SelectedValue);
            objApplication.AddApplicationStatus(Convert.ToInt32(cmbStatus.SelectedValue));

            if (!(cmbAction.SelectedValue.ToString().Equals("-1")))
            {
                BnbContactDetail objContactDetail = new BnbContactDetail(true);
                objContactDetail.RegisterForEdit(em);
                objContactDetail.ContactDescriptionID = Convert.ToInt32(cmbAction.SelectedValue);
                objContactDetail.SentViaID = BnbConst.SentVia_Byhand;// 1003;
                indvdetails.ContactDetails.Add(objContactDetail);
            }

            try
            {
                em.SaveChanges();
                this.IndividualID = indvdetails.ID;
            }
            catch (BnbProblemException pe)
            {
                throw pe;
            }
            catch (Exception err)
            {
                throw err;
            }


        }
        #endregion

        #region " Individual Contact Block Update "
        private BnbIndividual IndividualContactBlockUpdate(BnbIndividual objIndv, BnbEditManager em)
        {
            int contactBlockCount = objIndv.IndividualContactBlocks.Count;

            for (int i = 0; i < contactBlockCount; i++)
            {
                BnbIndividualContactBlock objContactBlock = (BnbIndividualContactBlock)objIndv.IndividualContactBlocks[i];
                objContactBlock.RegisterForEdit(em);
                if (objContactBlock.ContactBlockTypeID == BnbConst.ContactBlockType_AllowSelectedMail)
                {
                    for (int j = 0; j < objContactBlock.SelectedContacts.Count; j++)
                    {
                        BnbSelectedContact objSelectedContact = (BnbSelectedContact)objContactBlock.SelectedContacts[j];
                        objSelectedContact.RegisterForEdit(em);
                        // Contact Block type - Happy to recieve range of ways 
                        if (cmbRecieveRangeOfWays.SelectedValue.ToString().ToUpper() == "YES")
                        {
                            if (objSelectedContact.SelectedContactTypeID == BnbConst.SelectedContactType_VolunteeringCommunicationOnly)
                            {
                                objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_HappyToReceiveInfoRangeOfWaysTheyCanHelpVso;
                                recieveExists = true;
                            }
                            if (objSelectedContact.SelectedContactTypeID == BnbConst.SelectedContactType_HappyToReceiveInfoRangeOfWaysTheyCanHelpVso)
                            {
                                objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_HappyToReceiveInfoRangeOfWaysTheyCanHelpVso;
                                recieveExists = true;
                            }
                        }
                        else
                        {
                            if (objSelectedContact.SelectedContactTypeID == BnbConst.SelectedContactType_HappyToReceiveInfoRangeOfWaysTheyCanHelpVso)
                            {
                                objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_VolunteeringCommunicationOnly;
                                recieveExists = true;
                            }
                            if (objSelectedContact.SelectedContactTypeID == BnbConst.SelectedContactType_VolunteeringCommunicationOnly)
                            {
                                objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_VolunteeringCommunicationOnly;
                                recieveExists = true;
                            }
                        }

                        // Contact Block type - Email 
                        if (cmbRecieveEmails.SelectedValue.ToString().ToUpper() == "YES")
                        {
                            if (objSelectedContact.SelectedContactTypeID == BnbConst.SelectedContactType_OnlineEmailOptOut)
                            {
                                objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_OnlineEmailOptIn;
                                emailExists = true;
                            }
                            if (objSelectedContact.SelectedContactTypeID == BnbConst.SelectedContactType_OnlineEmailOptIn)
                            {
                                objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_OnlineEmailOptIn;
                                emailExists = true;
                            }
                        }
                        else
                        {
                            if (objSelectedContact.SelectedContactTypeID == BnbConst.SelectedContactType_OnlineEmailOptIn)
                            {
                                objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_OnlineEmailOptOut;
                                emailExists = true;
                            }
                            if (objSelectedContact.SelectedContactTypeID == BnbConst.SelectedContactType_OnlineEmailOptOut)
                            {
                                objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_OnlineEmailOptOut;
                                emailExists = true;
                            }
                        }

                        // Contact Block type - SMS 
                        if (cmbRecieveSMS.SelectedValue.ToString().ToUpper() == "YES")
                        {
                            if (objSelectedContact.SelectedContactTypeID == BnbConst.SelectedContactType_OnlineSMSOptOut)
                            {
                                objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_OnlineSMSOptIn;
                                smsExists = true;
                            }
                            if (objSelectedContact.SelectedContactTypeID == BnbConst.SelectedContactType_OnlineSMSOptIn)
                            {
                                objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_OnlineSMSOptIn;
                                smsExists = true;
                            }
                        }
                        else
                        {
                            if (objSelectedContact.SelectedContactTypeID == BnbConst.SelectedContactType_OnlineSMSOptIn)
                            {
                                objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_OnlineSMSOptOut;
                                smsExists = true;
                            }
                            if (objSelectedContact.SelectedContactTypeID == BnbConst.SelectedContactType_OnlineSMSOptOut)
                            {
                                objSelectedContact.SelectedContactTypeID = BnbConst.SelectedContactType_OnlineSMSOptOut;
                                smsExists = true;
                            }
                        }

                    }
                }
            }
            return objIndv;
        }
        #endregion

        #region " ListBox Default Value "
        private void ListBoxDefaultValue()
        {
            cmbMainMotivation.SelectedValue = BnbConst.Motivation_Unknown;
            cmbAffliation.SelectedValue = BnbConst.CountryRegion_Unknown;
            cmbBestExplains.SelectedValue = BnbConst.CountryReason_Unknown;
            cmbPassionateAbout.SelectedValue = BnbConst.Interest_Unknown;
            cmbSupportOtherCharities.SelectedValue = BnbConst.Charity_Unknown;
            cmbSupportTheseCharities.SelectedValue = BnbConst.CharitySupport_Unknown;
            cmbActiveCommunity.SelectedValue = BnbConst.CivilEngagement_Unknown;
        }
        #endregion

        #region " PopulateListBoxes "
        private void PopulateListBoxes()
        {
            BnbLookupDataTable interestLookup = BnbEngine.LookupManager.GetLookup("lkuInterest");
            DataView interestView = new DataView(interestLookup);
            interestView.RowFilter = "Exclude = 0";
            interestView.Sort = "DisplayOrder";
            cmbPassionateAbout.DataSource = interestView;
            cmbPassionateAbout.DataValueField = "IntID";
            cmbPassionateAbout.DataTextField = "Description";
            cmbPassionateAbout.DataBind();

            BnbLookupDataTable charityLookup = BnbEngine.LookupManager.GetLookup("lkuCharity");
            DataView charityView = new DataView(charityLookup);
            charityView.RowFilter = "Exclude = 0";
            charityView.Sort = "DisplayOrder";
            cmbSupportOtherCharities.DataSource = charityView;
            cmbSupportOtherCharities.DataValueField = "IntID";
            cmbSupportOtherCharities.DataTextField = "Description";
            cmbSupportOtherCharities.DataBind();


            BnbLookupDataTable charitySupportLookup = BnbEngine.LookupManager.GetLookup("lkuCharitySupport");
            DataView charitySupportView = new DataView(charitySupportLookup);
            charitySupportView.RowFilter = "Exclude = 0";
            charitySupportView.Sort = "DisplayOrder";
            cmbSupportTheseCharities.DataSource = charitySupportView;
            cmbSupportTheseCharities.DataValueField = "IntID";
            cmbSupportTheseCharities.DataTextField = "Description";
            cmbSupportTheseCharities.DataBind();

            BnbLookupDataTable civilEngagementLookup = BnbEngine.LookupManager.GetLookup("lkuCivilEngagement");
            DataView civilEngagementView = new DataView(civilEngagementLookup);
            civilEngagementView.RowFilter = "Exclude = 0";
            civilEngagementView.Sort = "DisplayOrder";
            cmbActiveCommunity.DataSource = civilEngagementView;
            cmbActiveCommunity.DataValueField = "IntID";
            cmbActiveCommunity.DataTextField = "Description";
            cmbActiveCommunity.DataBind();

        }
        #endregion

        #region " GetSelectedValues "
        /// <summary>
        /// To get the concatenated value of selected items in the listboxes
        /// </summary>
        /// <param name="listBox"></param>
        /// <returns></returns>
        private string GetSelectedValues(ListBox listBox)
        {
            StringBuilder values = new StringBuilder();
            for (int idx = 0; idx < listBox.Items.Count; idx++)
                if (listBox.Items[idx].Selected)
                    values.Append(listBox.Items[idx].Value.Trim()+ ",");

            string selectedValues = values.ToString();

            if(values.Length > 0)
                selectedValues = selectedValues.Remove(selectedValues.Length - 1);

            return selectedValues;
        }

        #endregion
    }
}
