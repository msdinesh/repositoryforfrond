<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Page language="c#" Codebehind="PaperMovePage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.PaperMovePage" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
  <HEAD>
		<title>PaperMove</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/bnbapplication24silver.gif"></uc1:title>&nbsp;
				<uc1:NavigationBar id="NavigationBar1" runat="server"></uc1:NavigationBar>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" NewWidth="55px" EditAllWidth="55px" SaveWidth="55px"
					UndoWidth="55px" HeightAllButtons="25px" NewText="New" EditAllText="Edit All" SaveText="Save" UndoText="Undo"
					CssClass="databuttons" NewButtonEnabled="False"></bnbpagecontrol:bnbdatabuttons>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox" Width="224px" SuppressRecordSavedMessage="false"
					PopupMessage="false"></bnbpagecontrol:bnbmessagebox>
				<TABLE class="fieldtable" id="Table1" style="HEIGHT: 329px" width="624" border="0">
					<TR>
						<TD class="label" style="WIDTH: 49.41%; HEIGHT: 23px">Type:
						</TD>
						<TD style="HEIGHT: 23px"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" DataMember="BnbPaperMove.PaperTypeID"
								LookupTableName="lkuRegistryType" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="250px" Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 49.41%; HEIGHT: 36px">
							<P>Location:</P>
						</TD>
						<TD><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl2" runat="server" CssClass="lookupcontrol" DataMember="BnbPaperMove.PaperLocationID"
								LookupTableName="lkuRegistryLocation" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"
								Width="250px" Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 49.41%">
							<P>Box No, Ref:</P>
						</TD>
						<TD>
							<P><bnbdatacontrol:bnbtextbox id="BnbTextBox1" runat="server" CssClass="textbox" DataMember="BnbPaperMove.Old_papmove_id"
									Width="250px" Height="20px" ControlType="Text" MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:bnbtextbox></P>
						</TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 49.41%">Borrowed By:&nbsp;
						</TD>
						<TD><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl3" runat="server" CssClass="lookupcontrol" DataMember="BnbPaperMove.BorrowUserID"
								LookupTableName="vlkuBonoboUser" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Width="250px"
								Height="22px"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 49.41%">On:</TD>
						<TD><bnbdatacontrol:bnbdatebox id="BnbDateBox1" runat="server" CssClass="datebox" DataMember="BnbPaperMove.LoanedDate"
								Width="90px" Height="20px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 49.41%">Due:</TD>
						<TD><bnbdatacontrol:bnbdatebox id="BnbDateBox2" runat="server" CssClass="datebox" DataMember="BnbPaperMove.DueBackDate"
								Width="90px" Height="20px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 49.41%">Comments:</TD>
						<TD><bnbdatacontrol:bnbtextbox id="BnbTextBox2" runat="server" CssClass="textbox" DataMember="BnbPaperMove.Info"
								Width="300px" Height="80px" ControlType="Text" MultiLine="True" Rows="4" StringLength="0"></bnbdatacontrol:bnbtextbox>&nbsp;
						</TD>
					</TR>
					<TR>
						<TD style="WIDTH: 49.41%"></TD>
						<TD>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</TD>
					</TR>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
