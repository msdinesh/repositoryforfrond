<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Page language="c#" Codebehind="EventPersonnelPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.EventPersonnelPage" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>EventPersonnelPage</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/bnbeventpersonnel24silver.gif"></uc1:title>&nbsp;
				<uc1:NavigationBar id="NavigationBar1" runat="server"></uc1:NavigationBar>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" UndoText="Undo" SaveText="Save"
					EditAllText="Edit All" NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px"
					NewWidth="55px" NewButtonEnabled="False"></bnbpagecontrol:bnbdatabuttons>
				<bnbpagecontrol:BnbMessageBox id="BnbMessageBox2" runat="server" CssClass="messagebox" SuppressRecordSavedMessage="false"
					PopupMessage="false"></bnbpagecontrol:BnbMessageBox>
				<TABLE class="fieldtable" id="Table1" width="100%" border="0">
					<TR>
						<TD class="label" width="40%">Event</TD>
						<TD><bnbdatacontrol:bnbdomainobjecthyperlink id="BnbDomainObjectHyperlink1" runat="server" RedirectPage="EventPage.aspx" GuidProperty="BnbEventPersonnel.Event.EventID"
								DataMember="BnbEventPersonnel.Event.EventDescription" Target="_top" HyperlinkText="[BnbDomainObjectHyperlink]"></bnbdatacontrol:bnbdomainobjecthyperlink>
						</TD>
					</TR>
					<TR>
						<TD class="label" width="40%">Individual</TD>
						<TD><bnbdatacontrol:bnbdomainobjecthyperlink id="BnbDomainObjectHyperlink2" runat="server" RedirectPage="IndividualPage.aspx"
								GuidProperty="tblEventPersonnel.Individual.ID" DataMember="BnbEventPersonnel.Individual.FullName" Target="_top" HyperlinkText="[BnbDomainObjectHyperlink]"></bnbdatacontrol:bnbdomainobjecthyperlink></TD>
					</TR>
					<TR>
						<TD class="label" width="40%" style="HEIGHT: 112px">Roles</TD>
						<TD style="HEIGHT: 112px">
							<P><bnbdatagrid:bnbdatagridfordomainobjectlist id="BnbDataGridForDomainObjectList1" runat="server" CssClass="datagrid" Width="200px"
									DataMember="BnbEventPersonnel.EventPersonnelRoles" DataProperties="EventRoleID[BnbLookupControl:lkuEventRole]" DataGridForDomainObjectListType="Editable"
									DeleteButtonVisible="True" EditButtonVisible="true" AddButtonText="Add" EditButtonText="Edit" AddButtonVisible="true" VisibleOnNewParentRecord="False"
									SortBy="ID" DataGridType="Editable"></bnbdatagrid:bnbdatagridfordomainobjectlist></P>
						</TD>
					</TR>
					<TR>
						<TD class="label" width="40%">Comments</TD>
						<TD><bnbdatacontrol:bnbtextbox id="BnbTextBox1" runat="server" CssClass="textbox" Height="20px" Width="250px" DataMember="BnbEventPersonnel.Comments"
								ControlType="Text" DataType="String" StringLength="0" Rows="3" MultiLine="True"></bnbdatacontrol:bnbtextbox></TD>
					</TR>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
