using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using BonoboWebControls;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for WizardSelectionResults.
	/// </summary>
	public partial class WizardSelectionResults : System.Web.UI.Page
	{
		protected UserControls.WizardButtons wizardButtons;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
#if DEBUG
			if(Request.QueryString.ToString() == "")				
				Response.Redirect("WizardSelectionResults.aspx?BnbAttendance=DE520D5B-429A-49BE-BF56-EF7524709108");
#endif
			AttendanceID = new Guid(Request.QueryString["BnbAttendance"].ToString());

						
			BnbEngine.SessionManager.ClearObjectCache();

			BnbWorkareaManager.SetPageStyleOfUser(this);

			
			string strError = "";


	
			if (!this.IsPostBack)
			{
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				if (this.Request.UrlReferrer != null)
					lblHiddenReferrer.Value = this.Request.UrlReferrer.ToString();
				else
					lblHiddenReferrer.Value= "~/Menu.aspx";

				initWizard();
				BnbWebFormManager.LogAction("Selection Results Wizard", null, this);
			}
			if (panelNoEntry.Visible == false)
				wizardButtons.AddPanel(panelZero);
		}

		#region property
		/// <summary>
		/// stores the individual that the action is being added to
		/// </summary>
		private Guid AttendanceID
		{
			get
			{
				if (lblHiddenAttendanceID.Value.Trim().Length >0)
					return new Guid(lblHiddenAttendanceID.Value);
				else
					return Guid.Empty;
			}
			set{lblHiddenAttendanceID.Value = value.ToString();}
		}
		#endregion property

		#region private methods
		private void initWizard()
		{
			cmbPendingReason.SelectedValue = 0;
			this.CheckPriorToEnterResults();
			//only display the form if there are not errors.
			Utilities objUtility = new Utilities();
			if (panelNoEntry.Visible == false)
			{
				//Get the labels and combos data
				BnbAttendance objAttendance = BnbAttendance.Retrieve(AttendanceID);
				lblVolunteer.Text = objAttendance.Individual.FullName;
				lblRefNo.Text = objAttendance.Individual.RefNo;

				lblStatus.Text = BnbEngine.LookupManager.GetLookupItemDescription("vlkuBonoboStatusFull", objAttendance.Application.CurrentStatus.StatusID);

				lblEventTitle.Text = objAttendance.Event.EventDescription;
				lblEventType.Text = BnbEngine.LookupManager.GetLookupItemDescription("vlkuBonoboEventTypeWithGroup",objAttendance.Event.EventTypeID);
				lblEventStartDate.Text = objAttendance.Event.EventStart.ToString("dd/MMM/yyyy");

				
			}
		}
		/// <summary>
		/// Given a lookup name and an id returns the corresponding description value.
		/// </summary>
		/// <param name="lookupName"></param>
		/// <param name="ID"></param>
		/// <returns>string</returns>
		private string getDescription(string lookupName, string Filter)
		{
			
			
			BnbLookupDataTable dtLookup = BnbEngine.LookupManager.GetLookup(lookupName);
			DataView dtv = new DataView(dtLookup);
			dtv.RowFilter = Filter;
			dtv.Sort = "Description";
			DataTable dtbResult = dtv.Table;
			if (dtbResult.Rows.Count > 0)
				return dtbResult.Rows[0]["Description"].ToString();
			return "";

		}


		/// <summary>
		/// Ensure that this page can be displayed to enter results code.
		/// The wizard will carry out a few checks before displaying the page: 
		///* If the Event is not of EventGroup = Assessment Event then show a message 
		///		'No results can be entered for this type of event'. The user must then press Cancel 
		///		to go back to the page they came from. 
		///* If BnbAttendance.Application is null then show a message 
		///		'Cannot enter Selection Results for a non-Volunteer Attendee'. The user must then 
		///		press Cancel to go back to the page they came from. 
		///* If a BnbAttendanceResult object already exists for the BnbAttendance and SelectionResultID is not 
		///		null, then redirect to AttendanceResultPage.aspx with BnbAttendanceResult = the AttendanceResultID 
		/// </summary>
		/// <returns></returns>
		private void CheckPriorToEnterResults()
		{
			string strMessage = "";
			BnbAttendance objAttendance = BnbAttendance.Retrieve(AttendanceID);
						
			// if user doesnt have the right permission show a message
			if (!BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_AssessmentResults))	
				strMessage = "You dont have permission to edit Assessment Results. <BR>Please press 'Cancel'.<br>";
			if (!objAttendance.Event.IsAttendanceResultsEvent)
				strMessage = "No results can be entered for this type of event. <BR>Please press 'Cancel'.<br>";
			if (objAttendance.Application == null)
				strMessage+= "Cannot enter Selection Results for a non-Volunteer Attendee.<br>Please press 'Cancel'.<br>";
			if (objAttendance.AttendanceResults.Count > 0)
			{
				BnbAttendanceResult res = objAttendance.AttendanceResults[0] as BnbAttendanceResult;
				if (res.SelectionResultID != 0)
					Response.Redirect("AttendanceResultPage.aspx?BnbAttendanceResult=" + res.SelectionResultID.ToString());
			}

			if (strMessage != "")
			{
				panelZero.Visible = false;
				panelNoEntry.Visible = true;
				lblNoEntryFeedback.Text = strMessage;
			}
		}

		/// <summary>
		/// Given a radio button name finds out which one was checked.
		/// </summary>
		/// <param name="RadioName"></param>
		/// <returns></returns>
		private int GetSelectedResult(string RadioName)
		{
			
			foreach (Control objControl in panEntryForm.Controls)
			{
				if (objControl.GetType() == typeof(RadioButton))
				{
					RadioButton objRadio = (RadioButton)objControl;
					for (int lngCounter = 1; lngCounter <=5; lngCounter++)
					{
						if (objRadio.ID == RadioName + Convert.ToString(lngCounter) && objRadio.Checked)
							return lngCounter;
					}
				}
			}
			return 0;
		}
		/// <summary>
		/// Given a BnbAttendance object check if it has some BnbAttendanceResult with
		/// a SelectionResultID not set. If so, returns the BnbAttendanceResult ID.
		/// </summary>
		/// <param name="Attendance">BnbAttendance</param>
		/// <returns>Guid</returns>
		private Guid GetAttendanceResultID(BnbAttendance Attendance)
		{
			if (Attendance.AttendanceResults.Count > 0)
			{
				foreach(BnbAttendanceResult objItem in Attendance.AttendanceResults)
				{	
					if (objItem.SelectionResultID ==0)
					{
						return objItem.ID;
					}
				}
			}
			return Guid.Empty;
		}
		/// <summary>
		/// Retrieve the existing BnbAttendanceResult for the BnbAttendance (if there is one) 
		///or else create a new one and link it to the BnbAttendance. 
		///	set SelectionResultID on BnbAttendanceResult 
		/// set the BnbAttendanceResult.Score... values if specified 
		/// A bit of processing will be needed to turn text in the RejectionCodes textbox into a collection of BnbAttendanceResultCode objects: 
		///	-Take the text for the RejectionCode textbox and Split in into an array using the Split() method 
		/// -For each string in the array: 
		///		- if it can be matched against a 'Description' from lkuRejectionCode then 
		///		-create a new BnbAttendanceResultCode 
		///		-set RejectionCodeID to the correct value 
		///		-if its the first one, set Order to 1, otherwise set it to 2 
		///(if any of the codes cannot be found in lkuRejectionCode then stop processing and show the user a message in a red label) 
		///save the new data using em.SaveChanges() 
		/// </summary>
		private void CreateNewSelectionResult()
		{
			bool blnNewAttendanceResult = false;
			int lngRejectionID = 0;
			Guid guiAttendanceResultID;
			BnbAttendanceResult objAttendanceResult;

			BnbEditManager em = new BnbEditManager();
			BnbAttendance objAttendance = BnbAttendance.Retrieve(this.AttendanceID);
			guiAttendanceResultID = GetAttendanceResultID(objAttendance);
			if (guiAttendanceResultID!= Guid.Empty)
				objAttendanceResult = BnbAttendanceResult.Retrieve(guiAttendanceResultID);
			else
			{
				objAttendanceResult = new BnbAttendanceResult(true);
				blnNewAttendanceResult = true;
			}

			objAttendance.RegisterForEdit(em);	
			objAttendanceResult.RegisterForEdit(em);

			if (cmbSelectionResult.SelectedValue.ToString().Length > 0)

					objAttendanceResult.SelectionResultID = Convert.ToInt32(cmbSelectionResult.SelectedValue);
			
			objAttendanceResult.ScorePRC = GetSelectedResult("radPRC");
			objAttendanceResult.ScoreCTL = GetSelectedResult("radCTL");
			objAttendanceResult.ScoreFlex = GetSelectedResult("radFlex");
			objAttendanceResult.ScorePPSA = GetSelectedResult("radPPSA");
			objAttendanceResult.ScoreSA = GetSelectedResult("radSA");
			objAttendanceResult.ScoreSON = GetSelectedResult("radSON");
			objAttendanceResult.ScoreSTS = GetSelectedResult("radSTS");
			objAttendanceResult.ScoreWWO = GetSelectedResult("radWWO");


			if (txtCodes.Text.Length > 0)
			{
				string [] vntRejectionCodes  = null;
				string delimStr = " ";
				char [] delimiter = delimStr.ToCharArray();

				vntRejectionCodes = txtCodes.Text.Split(delimiter,100);
				for (int lngCounter=0; lngCounter < vntRejectionCodes.Length; lngCounter ++)
				{
					lngRejectionID = GetRejectionID(vntRejectionCodes.GetValue(lngCounter).ToString());
					if (lngRejectionID ==0)
					{
						//display error message and don't save.
						lblPanelZeroFeedback.Text = "Unfortunately, the Selection results could not be created due to Rejection Code " + vntRejectionCodes[lngCounter] + " not existing."; 
						return;
					}
					else
						//create a BnbAttendanceResultCode.
						AddAttendanceResultCode(lngRejectionID,objAttendanceResult,em, lngCounter);
				}
			}
			if (blnNewAttendanceResult)
				objAttendance.AttendanceResults.Add(objAttendanceResult);
	
			try
				{
					em.SaveChanges();


					//If the user chose SelectionResultID = 'Pending', we want to re-load the data and set the status reason: 
					//- check the BnbApplication.CurrentStatus 
					//- if StatusID = 1046 (Candidate/Pend) then 
					//-edit the BnbApplciationStatus object and set StatusReasonID = the reason from the Pending Reason combo
					
					
					if (Convert.ToInt32(cmbSelectionResult.SelectedValue) == BnbConst.SelectionResult_Pending)
					{	
						BnbEngine.SessionManager.ClearObjectCache();
						BnbEditManager em2 = new BnbEditManager();
						BnbAttendance objAttendance2 = BnbAttendance.Retrieve(AttendanceID);
						BnbApplicationStatus appStatus = objAttendance2.Application.CurrentStatus;
						appStatus.RegisterForEdit(em2);
	
						if (Convert.ToInt32(appStatus.StatusID) == BnbConst.Status_CandidatePend)
						{
							if (cmbPendingReason.SelectedValue.ToString().Length > 0)
								appStatus.StatusReasonID = Convert.ToInt32(cmbPendingReason.SelectedValue);
						}
						em2.SaveChanges();
				}
			
					Response.Redirect(lblHiddenReferrer.Value);
				}
				catch(BnbProblemException pe)
				{
					lblPanelZeroFeedback.Text = "Unfortunately, the Selection results could not be created due to the following problems: <br/>";
					foreach(BnbProblem p in pe.Problems)
						lblPanelZeroFeedback.Text += p.Message + "<br/>";
				}
			}
		/// <summary>
		/// Given a rejection code it returns it corresponding id.
		/// </summary>
		/// <param name="RejectionCode">string RejectionCode</param>
		/// <returns></returns>
		private int GetRejectionID(string RejectionCode)
		{
			BnbLookupDataTable dtLookup = BnbEngine.LookupManager.GetLookup("lkuRejectionCode");
			DataRow[] foundRows = dtLookup.Select("Description like '" + RejectionCode + "'");
			if (foundRows.Length > 0)
				return Convert.ToInt32(foundRows[0]["IntID"].ToString());

			return 0;
		}
		/// <summary>
		/// Creates a new AttendanceResultCode and add it the AttendanceResult object created in 
		/// in CreateNewSelectionResult method.
		/// </summary>
		/// <param name="ID">AttendanceResultID</param>
		/// <param name="AttendanceResult">the BnbAttendanceResult object to add the AttendanceResultCodeTo</param>
		/// <param name="em">BnbEditManager</param>
		private void AddAttendanceResultCode(int ID, BnbAttendanceResult AttendanceResult, BnbEditManager em, int Order)
		{
			
			BnbAttendanceResultCode objResultCode = new BnbAttendanceResultCode(true);
			objResultCode.RegisterForEdit(em);
			objResultCode.RejectionCodeID = ID;
			if (Order == 0)
				objResultCode.Order = 1;
			else
				objResultCode.Order = 2;
			AttendanceResult.AttendanceResultCodes.Add(objResultCode);
		}

		public void SetupPanelZero()
		{
			// nothing
		}

		#endregion private methods

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			wizardButtons.ShowPanel +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
			wizardButtons.CancelWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
			wizardButtons.FinishWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


		#region Events
		private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			if (e.CurrentPanel == 0)
			{
				this.SetupPanelZero();
			}
		}

		private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			Response.Redirect(lblHiddenReferrer.Value);
		}

		private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			this.CreateNewSelectionResult();
		}
		#endregion Events

		protected void cmdNoPermissionCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect(lblHiddenReferrer.Value);
		}

	}
}
