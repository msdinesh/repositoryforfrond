<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="IndividualAddressPanel" Src="../UserControls/IndividualAddressPanel.ascx" %>
<%@ Page language="c#" Codebehind="WizardNewEnquiry.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.WizardNewEnquiry" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML xmlns="http://www.w3.org/1999/xhtml">
	<HEAD>
		<title>WizardNewEnquiry</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal><LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet"><asp:literal id="literalAfterCss" runat="server"></asp:literal>
				<script language="javascript" src="../Javascripts/GoogleMap.js"></script>

	</HEAD>
	<body onload="initialize()" onunload="GUnload()">
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent">
				<DIV class="wizardheader" ms_positioning="FlowLayout">
					<H3><IMG class="icon24" src="../Images/wizard24silver.gif"> New Enquiry / 
						Application Wizard</H3>
				</DIV>
				<asp:panel id="panelGetName" runat="server" CssClass="wizardpanel">
					<P><EM>This wizard will help you to add a new Enquiry or Application.</EM></P>
					<P>Please select Enquirer or Applicant, enter Forenames and Surname and then press 
						Next</P>
					<P></P>
					<P>
						<TABLE class="wizardtable" id="Table2">
							<TR>
								<TD class="label" style="HEIGHT: 6px"></TD>
								<TD>
									<asp:RadioButton id="radEnquiry" runat="server" Text="Enquirer" GroupName="radGroup"></asp:RadioButton>
									<asp:RadioButton id="radApplication" runat="server" Text="Applicant" GroupName="radGroup"></asp:RadioButton></TD>
							</TR>
							<TR>
								<TD class="label" style="HEIGHT: 6px">Forenames</TD>
								<TD>
									<asp:TextBox id="txtFirstname" runat="server" Width="240px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD class="label">Surname</TD>
								<TD>
									<asp:TextBox id="txtSurname" runat="server" Width="240px"></asp:TextBox></TD>
							</TR>
						</TABLE>
						<INPUT id="lblHiddenIndividualID" type="hidden" name="lblHiddenIndividualID" runat="server">
						<asp:label id="labelPanelZeroFeedback" runat="server" CssClass="feedback"></asp:label></P>
				</asp:panel><asp:panel id="panelDuplicateCheck" runat="server" CssClass="wizardpanel">
					<asp:Panel id="subpanelChoose" runat="server" Visible="True">
						<P>The following similar Individuals already exist. If any of them match the 
							enquirer or applicant you were going to enter, then please select them and 
							press Next.</P>
						<P>
							<bnbdatagrid:BnbDataGridForView id="bdgIndividuals" runat="server" CssClass="datagrid" Width="100%" ViewLinksText="View"
								NewLinkText="New" NewLinkVisible="False" ViewLinksVisible="true" ShowResultsOnNewButtonClick="false" DisplayResultCount="false"
								DisplayNoResultMessageOnly="false" DataGridType="RadioButtonList" ColumnNames="tblIndividualKeyInfo_Forename, tblIndividualKeyInfo_Surname, tblIndividualKeyInfo_old_indv_id, tblIndividualAdditional_PreviousSurname, tblIndividualKeyInfo_DateOfBirth, tblAddress_AddressLine1"
								ViewName="vwBonoboIndividualDuplicateCheckFast" GuidKey="tblIndividualKeyInfo_IndividualID"></bnbdatagrid:BnbDataGridForView><BR>
							<asp:Label id="lblMaxRowsExceeded" runat="server"></asp:Label></P>
						<P>If none of these existing Individuals match, then press Next to create&nbsp;a 
							new Individual.</P>
						<P></P>
					</asp:Panel>
					<asp:Label id="lblNoDuplicates" runat="server">No similar Individuals already exist. Press Next to continue.</asp:Label>
					<P>
						<asp:Label id="lblPanelOneFeedback" runat="server" CssClass="feedback"></asp:Label>
				</asp:panel><asp:panel id="panelAddress" runat="server" CssClass="wizardpanel">
					<P>Please enter further details about the Individual's address</P>
					<P>
						<uc1:IndividualAddressPanel id="IndividualAddressPanel1" runat="server"></uc1:IndividualAddressPanel></P>
					<P>
						<asp:Label id="lblPanelAddressFeedback" runat="server" CssClass="feedback"></asp:Label>
						<asp:CheckBox id="chkIgnoreAddressWarnings" runat="server" CssClass="feedback" Text="Ignore Warnings"
							Visible="False"></asp:CheckBox></P>
				</asp:panel><asp:panel id="panelChooseAppType" runat="server" CssClass="wizardpanel">
					<P>Please select Enquirer or Applicant</P>
					<TABLE class="wizardtable" id="Table2">
						<TR>
							<TD class="label" style="WIDTH: 100px; HEIGHT: 6px"></TD>
							<TD>
								<asp:RadioButton id="radEnquiry2" runat="server" Text="Enquirer" GroupName="radGroup2"></asp:RadioButton>
								<asp:RadioButton id="radApplication2" runat="server" Text="Applicant" GroupName="radGroup2"></asp:RadioButton></TD>
						</TR>
					</TABLE>
				</asp:panel><asp:panel id="panelEnquirerDetails" runat="server" CssClass="wizardpanel">
					<P>Please enter further details about the new Enquirer, then press Finish to create 
						the new record:
					</P>
					<TABLE class="wizardtable" id="Table4">
						<TR>
							<TD class="label">Forename</TD>
							<TD>
								<asp:Label id="uxSummaryForename" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label">Surname</TD>
							<TD>
								<asp:Label id="uxSummarySurname" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label">Ref No</TD>
							<TD>
								<asp:Label id="uxSummaryRefNo" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label">Date Of Birth</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneDateBox id="txtDOB" runat="server" CssClass="datebox" Width="90px" Height="20px" DateCulture="en-GB"
									DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbStandaloneDateBox></TD>
						</TR>
						<TR>
							<TD class="label">Enquiry method</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="cmbMethod" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuEnquiryMethod"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 21px">Status</TD>
							<TD style="HEIGHT: 21px">
								<P>
									<bnbdatacontrol:BnbStandaloneLookupControl id="cmbStatusGroupEnquirer" runat="server" CssClass="lookupcontrol" Width="250px"
										Height="22px" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel" LookupTableName="lkuStatusGroup"></bnbdatacontrol:BnbStandaloneLookupControl>/
									<bnbdatacontrol:BnbStandaloneLookupControl id="cmbStatus" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
										DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuStatus" ColumnRelatedToParent="StatusGroupID"
										ParentControlID="cmbStatusGroupEnquirer"></bnbdatacontrol:BnbStandaloneLookupControl></P>
							</TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 21px">VSO type</TD>
							<TD style="HEIGHT: 21px">
								<bnbdatacontrol:BnbStandaloneLookupControl id="cmbVSOType" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboVSOTypeWithGroup"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label">Recruitment base</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="cmbRecruitment" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuRecruitmentBase"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
					
						<TR>
							<TD class="label">Recruited By</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="cmbRecruitedBy" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuRecruitedBy"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label">Comments</TD>
							<TD>
								<asp:TextBox id="txtComments" runat="server" Width="250px" Height="80px" Rows="3" TextMode="MultiLine"></asp:TextBox></TD>
						</TR>
					</TABLE>
					<P>
						<asp:Label id="lblPanelEnquierDetailsFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				</asp:panel><asp:panel id="panelApplicantDetails" runat="server" CssClass="wizardpanel">
					<P>Please enter further details about the new Applicant, then press Finish to 
						create the new record:
					</P>
					<TABLE class="wizardtable" id="Table7">
						<TR>
							<TD class="label">Forename</TD>
							<TD>
								<asp:Label id="uxSummaryForename2" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label">Surname</TD>
							<TD>
								<asp:Label id="uxSummarySurname2" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label">Ref No</TD>
							<TD>
								<asp:Label id="uxSummaryRefNo2" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label">Date Of Birth</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneDateBox id="txtDOB2" runat="server" CssClass="datebox" Width="90px" Height="20px" DateCulture="en-GB"
									DateFormat="dd/MMM/yyyy" DESIGNTIMEDRAGDROP="189"></bnbdatacontrol:BnbStandaloneDateBox></TD>
						</TR>
						<TR>
							<TD class="label">Lead Skill</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="cmbLeadSkill2" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboSkillWithGroupAbbrev"
									ShowBlankRow="True"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label">Marital Status</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="cmbMaritalStatus2" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuMaritalStatus" ShowBlankRow="True"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 21px">Status</TD>
							<TD style="HEIGHT: 21px">
								<P>
									<bnbdatacontrol:BnbStandaloneLookupControl id="cmbStatusGroupApplicant" runat="server" CssClass="lookupcontrol" Width="250px"
										Height="22px" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel" LookupTableName="lkuStatusGroup"></bnbdatacontrol:BnbStandaloneLookupControl>/
									<bnbdatacontrol:BnbStandaloneLookupControl id="cmbStatus2" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
										DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuStatus" ColumnRelatedToParent="StatusGroupID"
										ParentControlID="cmbStatusGroupApplicant"></bnbdatacontrol:BnbStandaloneLookupControl></P>
							</TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 21px">VSO type</TD>
							<TD style="HEIGHT: 21px">
								<bnbdatacontrol:BnbStandaloneLookupControl id="cmbVSOType2" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboVSOTypeWithGroup"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 6px">Application&nbsp;source</TD>
							<TD style="HEIGHT: 6px">
								<bnbdatacontrol:BnbStandaloneLookupControl id="cmbApplicationSource2" runat="server" CssClass="lookupcontrol" Width="250px"
									Height="22px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuApplicationSource"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label">Available from</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneDateBox id="txtAvailableFrom2" runat="server" CssClass="datebox" Width="90px" Height="20px"
									DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbStandaloneDateBox></TD>
						</TR>
						<TR>
							<TD class="label">Recruitment Base</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="cmbRecruitmentBase2" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuRecruitmentBase"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						
						<TR>
							<TD class="label">Recruited By</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="cmbRecruitedBy2" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuRecruitedBy"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 29px">Comments</TD>
							<TD style="HEIGHT: 29px">
								<asp:TextBox id="txtComments2" runat="server" Width="250px" Height="80px" Rows="3" TextMode="MultiLine"></asp:TextBox></TD>
						</TR>
					</TABLE>
					<P>
						<asp:Label id="lblPanelApplicantDetailsFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				</asp:panel>
				<P></P>
				<P><uc1:wizardbuttons id="wizardButtons" runat="server"></uc1:wizardbuttons></P>
				<P><asp:label id="labelHiddenRequestIDUnused" runat="server" Visible="False"></asp:label><asp:textbox id="uxHiddenMode" runat="server" Visible="False"></asp:textbox><asp:textbox id="txtHiddenReferrer" runat="server" Visible="False"></asp:textbox></P>
				<p></p>
			</div>
		</form>
	</body>
</HTML>
