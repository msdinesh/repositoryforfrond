<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuditingHistoryPage.aspx.cs" Inherits="Frond.EditPages.AuditingHistoryPage" %>
<%@ Register Src="../UserControls/PMFStatusPanel.ascx" TagName="PMFStatusPanel" TagPrefix="uc2" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>    
</head>
<body>
    <form id="form1" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/history24silver.gif">
            </uc1:Title> 
             <uc2:PMFStatusPanel ID="PMFStatusPanel2" runat="server" />   
            <bnbpagecontrol:BnbMessageBox ID="BnbMessageBox1" runat="server" CssClass="messagebox"
                SuppressRecordSavedMessage="false" PopupMessage="false"></bnbpagecontrol:BnbMessageBox>   
        <p class="sectionHeading" style="padding:3px">
         <asp:Label ID="lblAuditHeading" runat="server"></asp:Label>
        </p>
        <table id="table1" border="0" class="fieldtable">
            <tr>
                <td>
                    <asp:LinkButton ID="lnkGoToSignOffTab" runat="server" Text="Back To SignOff Tab" OnClick="lnkGoToSignOffTab_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnl1" runat="server">
                    <p>
                        <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView1" runat="server" CssClass="datagrid"
                            Width="100%" NewLinkReturnToParentPageDisabled="True" FieldName="tblProjectAudit_ProjectID"  GuidKey="tblProjectAudit_ProjectID"                            
                            ViewLinksVisible="false" NewLinkVisible="false" ViewLinksText="View" QueryStringKey="BnbProject">
                        </bnbdatagrid:BnbDataGridForView>
                    </p>
                    </asp:Panel>
                </td>
            </tr>     
            <tr></tr>  
        </table>
    </div>
    </form>
</body>
</html>
