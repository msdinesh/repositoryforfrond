<%@ Page language="c#" Codebehind="WizardNewWorkAddress.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.WizardNewWorkAddress" %>
<%@ Register TagPrefix="uc1" TagName="OrganisationAddressPanel" Src="../UserControls/OrganisationAddressPanel.ascx" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="cc1" Namespace="BonoboWebControls.ServicesControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML xmlns="http://www.w3.org/1999/xhtml">
	<HEAD>
		<title>WizardNewWorkAddress</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal><LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet"><asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<script language="javascript" src="../Javascripts/GoogleMap.js"></script>

	</HEAD>
	<body onload="initialize()" onunload="GUnload()">
		<FORM id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent">
				<DIV class="wizardheader" ms_positioning="FlowLayout">
					<H3><IMG class="icon24" src="../Images/wizard24silver.gif"> New Work 
						Address&nbsp;Wizard</H3>
				</DIV>
				<asp:panel id="panelGetName" runat="server" CssClass="wizardpanel">
					<P><EM>This wizard will help you to add a new&nbsp;Work or Bank Address for an 
							individual.</EM>
					<P><EM>In Frond, Work/Bank addresses&nbsp;are also links to Organisations. Hence the 
							wizard will attempt to match the Organisation Name you enter with existing 
							Organisations in the database. If a match is found, you will have the option of 
							using one of the Organisation's existing addresses, or adding a new one. If a 
							match is not found, a new Organisation will be created as part of the new 
							Address.</EM>
					<P>Please enter the Country and&nbsp;Organisation Name&nbsp;for the new 
						Address&nbsp;and press Next</P>
					<P></P>
					<P>
						<TABLE class="wizardtable" id="Table2">
							<TR>
								<TD class="label" style="HEIGHT: 15px">Individual</TD>
								<TD style="HEIGHT: 15px">
									<asp:Label id="uxIndividualSummaryLabel1" runat="server"></asp:Label></TD>
							</TR>
							<TR>
								<TD class="label" style="HEIGHT: 15px">Address Type</TD>
								<TD style="HEIGHT: 15px">
									<asp:Label id="uxAddressTypeLabel1" runat="server"></asp:Label></TD>
							</TR>
							<TR>
								<TD class="label" style="HEIGHT: 15px">Organisation Country</TD>
								<TD style="HEIGHT: 15px">
									<bnbdatacontrol:BnbStandaloneLookupControl id="uxOrgCountryDropDown" runat="server" CssClass="lookupcontrol" LookupTableName="lkuCountry"
										LookupControlType="ComboBox" DataTextField="Description" ListBoxRows="4" Height="22px" Width="250px"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
							</TR>
							<TR>
								<TD class="label">Organisation Name</TD>
								<TD>
									<asp:TextBox id="uxOrgNameTextbox" runat="server" Width="240px"></asp:TextBox></TD>
							</TR>
						</TABLE>
					<P>
						<asp:label id="uxPanelGetNameFeedback" runat="server" CssClass="feedback"></asp:label></P>
				</asp:panel><asp:panel id="panelSearchResults" runat="server" CssClass="wizardpanel">
<asp:panel id="uxOrgResultsPanel" runat="server">
<P>The following similar&nbsp;Organisations already exist. If any of them match 
the data you were going to enter, then please select them and press Next. 
<P></P>
<bnbdatagrid:BnbDataGridForView id="uxOrgResultsGrid" runat="server" CssClass="datagrid" Width="100%" NoResultMessage="No matching Organisations were found"
							GuidKey="lnkCompanyAddress_AddressID" ViewName="vwBonoboCompanyDuplicateCheck" Visible="False"
							ViewLinksText="View" NewLinkText="New" NewLinkVisible="true" ViewLinksVisible="True"
							ShowResultsOnNewButtonClick="True" DisplayResultCount="false" DisplayNoResultMessageOnly="True"
							DataGridType="RadioButtonList"></bnbdatagrid:BnbDataGridForView>
<P></P>
<P>If no existing Organisations match, then press Next to create the new Address 
and Organisation. </asp:panel></P>
<P>
						<asp:label id="uxNoSimilarLabel" runat="server" Visible="False">No similar Organisations already exist. Press Next to continue.</asp:label></P>
<P>
						<asp:label id="uxPanelSearchResultsFeedback" runat="server" CssClass="feedback"></asp:label></P></asp:panel><asp:panel id="panelChooseOrgAddress" runat="server" CssClass="wizardpanel">
					<TABLE class="wizardtable" id="Table2">
						<TR>
							<TD class="label" style="HEIGHT: 15px">Individual</TD>
							<TD style="HEIGHT: 15px">
								<asp:Label id="uxIndividualSummaryLabel2" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 15px">Address Type</TD>
							<TD style="HEIGHT: 15px">
								<asp:Label id="uxAddressTypeLabel2" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 15px">Org Name</TD>
							<TD style="HEIGHT: 15px">
								<asp:Label id="uxOrgNameLabel2" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 15px">Org Ref</TD>
							<TD style="HEIGHT: 15px">
								<asp:Label id="uxOrgRefLabel2" runat="server"></asp:Label></TD>
						</TR>
					</TABLE>
					<P>The Organisation has the following Addresses. Please choose one, or check the 
						box to say that you want to add a new address.</P>
					<P>
						<bnbdatagrid:BnbDataGridForView id="uxOrgAddressesGrid" runat="server" CssClass="datagrid" Width="100%" GuidKey="lnkCompanyAddress_AddressID"
							ViewName="vwBonoboCompanyAddresses3" ViewLinksText="View" NewLinkText="New" NewLinkVisible="False" ViewLinksVisible="true"
							ShowResultsOnNewButtonClick="True" DisplayResultCount="false" DisplayNoResultMessageOnly="false" DataGridType="RadioButtonList"></bnbdatagrid:BnbDataGridForView></P>
					<P>
						<asp:CheckBox id="uxAddOrgAddressCheckbox" runat="server" Text="I want to add a new Address for the Organisation"></asp:CheckBox></P>
					<P>
						<asp:Label id="uxPanelChooseOrgAddressFeedback" runat="server" CssClass="feedback"></asp:Label></P>
					<P>&nbsp;</P>
				</asp:panel><asp:panel id="panelEnterAddress" runat="server" CssClass="wizardpanel">
					<P>Please enter further details about the new Organisation, then press Next:</P>
					<TABLE class="wizardtable" id="Table4">
						<TR>
							<TD class="label">Organisation Name</TD>
							<TD>
								<asp:TextBox id="uxOrgNameTextbox3" runat="server" Width="224px"></asp:TextBox>
								<asp:Label id="uxOrgNameLabel3" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label">Org Ref</TD>
							<TD>
								<asp:Label id="uxOrgRefLabel3" runat="server"></asp:Label></TD>
						</TR>
						<uc1:OrganisationAddressPanel id="uxOrganisationAddressUserControl" runat="server"></uc1:OrganisationAddressPanel></TABLE>
					<P>
						<asp:Label id="uxPanelEnterAddressFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				</asp:panel><asp:panel id="panelLinkDetails" runat="server" CssClass="wizardpanel">
					<P>Please enter further details about the new Work/Bank Address, then press Finish 
						to create the new record:</P>
					<TABLE class="wizardtable" id="Table8" width="80%">
						<TR>
							<TD class="label" width="50%">Individual</TD>
							<TD width="50%">
								<asp:Label id="uxIndividualSummaryLabel4" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label">Address Type</TD>
							<TD>
								<asp:Label id="uxAddressTypeLabel4" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label">Organisation Name</TD>
							<TD>
								<asp:Label id="uxOrgNameLabel4" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label">Org Ref</TD>
							<TD>
								<asp:Label id="uxOrgRefLabel4" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label">Org Address</TD>
							<TD>
								<asp:Label id="uxOrgFullAddressLabel" runat="server"></asp:Label></TD>
						</TR>
					</TABLE>
					<asp:Panel id="uxWorkAddressPanel" runat="server">
						<TABLE class="wizardtable" id="Table9" width="80%">
							<TR>
								<TD class="label" width="50%">Role</TD>
								<TD width="50%">
									<asp:TextBox id="uxWorkRoleTextbox" runat="server"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD class="label">Department</TD>
								<TD>
									<asp:TextBox id="uxWorkDepartmentTextbox" runat="server"></asp:TextBox></TD>
							</TR>
						</TABLE>
					</asp:Panel>
					<asp:Panel id="uxBankAddressPanel" runat="server">
						<TABLE class="wizardtable" id="Table10" width="80%">
							<TR>
								<TD class="label" width="50%">Account Name</TD>
								<TD width="50%">
									<asp:TextBox id="uxAccountNameTextbox" runat="server"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD class="label">Account No</TD>
								<TD>
									<asp:TextBox id="uxAccountNumberTextbox" runat="server"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD class="label">Sort Code/Routing No</TD>
								<TD>
									<asp:TextBox id="uxSortCodeTextbox" runat="server"></asp:TextBox></TD>
							</TR>
						</TABLE>
					</asp:Panel>
					<P>
						<asp:Label id="uxPanelLinkDetailsFeedback" runat="server" CssClass="feedback"></asp:Label>
						<asp:CheckBox id="uxIgnoreWarningsCheckbox" runat="server" CssClass="feedback" Visible="False"
							Text="Ignore Warnings"></asp:CheckBox></P>
				</asp:panel>
				<p><uc1:wizardbuttons id="wizardButtons" runat="server"></uc1:wizardbuttons><asp:textbox id="uxHiddenReferrer" runat="server" Visible="False"></asp:textbox><asp:textbox id="uxHiddenIndividualID" runat="server" Visible="False"></asp:textbox><asp:textbox id="uxHiddenAddressTypeID" runat="server" Visible="False"></asp:textbox><asp:textbox id="uxHiddenCompanyID" runat="server" Visible="False"></asp:textbox><asp:textbox id="uxHiddenCompanyAddressID" runat="server" Visible="False"></asp:textbox></p>
				<P>&nbsp;</P>
			</div>
		</FORM>
	</body>
</HTML>
