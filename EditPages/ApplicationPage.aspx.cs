using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Text;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;
using BonoboEngine.Query;
using BonoboWebControls.DataGrids;
using BonoboEngine.Utilities;
using System.Data.SqlClient;
using BonoboEngine;



namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for VolunteerService.
    /// </summary>
    public partial class ApplicationPage : System.Web.UI.Page
    {

        #region controls
        protected BonoboWebControls.PageControls.BnbDataButtons BnbDataButtons1;
        protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading2;
        protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading6;
        protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading7;
        protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading8;
        protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading9;
        protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading10;
        protected BonoboWebControls.DataControls.BnbLookupControl BnbLookupControl4;
        protected BonoboWebControls.DataControls.BnbLookupControl BnbLookupControl5;
        protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox10;
        protected BonoboWebControls.DataControls.BnbDateBox BnbDateBox3;
        protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox11;
        protected BonoboWebControls.DataControls.BnbDateBox BnbDateBox4;
        protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox12;
        protected BonoboWebControls.DataControls.BnbDateBox BnbDateBox5;
        // removed for upgrade to .net 2
        //protected BonoboWebControls.DataGrids.BnbDataGridForView BnbDataGridForView2;
        protected BonoboWebControls.DataGrids.BnbDataGridForView BnbDataGridForView4;
        protected BonoboWebControls.DataGrids.BnbDataGridForView bnbPlacementsForReview;
        protected UserControls.Title titleBar;

        private string m_IndividualID = "";
        private string m_ApplicationID = "";
        protected System.Web.UI.WebControls.PlaceHolder titlePlaceHolder;
        protected BonoboWebControls.DataGrids.BnbDataGridForDomainObjectList BnbDataGridForDomainObjectList4;
        protected BonoboWebControls.DataGrids.BnbDataGridForDomainObjectList BnbDataGridForDomainObjectList5;
        protected Microsoft.Web.UI.WebControls.TabStrip TabStrip1;
        protected BonoboWebControls.DataGrids.BnbDataGridForDomainObjectList BnbDataGridForDomainObjectList1;
        protected BonoboWebControls.DataGrids.BnbDataGridForDomainObjectList BnbDataGridForDomainObjectList2;
        protected BonoboWebControls.DataGrids.BnbDataGridForDomainObjectList BnbDataGridForDomainObjectList3;
        protected System.Web.UI.WebControls.Panel Main;
        protected System.Web.UI.WebControls.Panel Panel2;
        protected System.Web.UI.WebControls.Panel Panel3;
        protected System.Web.UI.WebControls.Panel Panel4;
        protected System.Web.UI.WebControls.Panel Panel5;
        protected System.Web.UI.WebControls.Panel Panel8;
        protected System.Web.UI.WebControls.Panel Panel9;
        protected System.Web.UI.WebControls.Panel Panel10;
        protected System.Web.UI.WebControls.Panel Panel11;
        protected System.Web.UI.WebControls.Panel Panel12;
        protected System.Web.UI.WebControls.Panel Panel13;
        protected System.Web.UI.WebControls.Panel Panel14;
        protected System.Web.UI.WebControls.Panel Panel15;
        protected System.Web.UI.WebControls.Panel Panel7;
        protected BonoboWebControls.PageControls.BnbSectionHeading BnbSectionHeading4;
        protected BonoboWebControls.ServicesControls.BnbWordFormButton BnbWordFormButton1;
        protected UserControls.NavigationBar navigationBar1;
        protected UserControls.MenuBar menuBar;


        private BnbWebFormManager bnb = null;

        #endregion

        private bool disableMedicalReq = true;
        protected void Page_Load(object sender, System.EventArgs e)
        {

#if DEBUG
            if (Request.ServerVariables["QUERY_STRING"] == "")
            {
                BnbWebFormManager.ReloadPage("BnbApplication=8eaf718f-8404-4429-9b20-0ffc7da13f92");
                //			BnbWebFormManager.ReloadPage("BnbApplication=CD294AB1-3BA0-4D97-8B63-44675C6FFB5C");


            }
            else
            {
                loadManager();

            }
#else
			loadManager();
#endif

            // call shared code to show any view warnings
            Utilities.ShowViewWarnings(bnb, lblViewWarnings);

            if (this.Request.QueryString["BnbApplication"] != null &&
                this.Request.QueryString["BnbApplication"].ToLower() == "new")
            {
                doRedirectToNewEnquiryWizard();
            }
            // tweak logging settings
            bnb.PageNameOverride = "Application";
            bnb.LogPageHistory = true;

            bnb.SaveEvent += new EventHandler(bnb_SaveEvent);
            bnb.EditModeEvent += new EventHandler(bnb_EditEvent);
            bnb.ViewModeEvent += new EventHandler(bnb_ViewEvent);
      
            BnbWorkareaManager.SetPageStyleOfUser(this);

            doAddressLabels();

            string appid = this.Request.QueryString["BnbApplication"];
            BnbApplication app = BnbApplication.Retrieve(new System.Guid(appid));

            if (app.HasBenefitPayments != true)
                cmdWFrmBenefitSummary.Enabled = false;


            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null)
                    titleBar.TitleText = bnb.PageTitleDescriptionCurrent;

                menuBar.SetOptionalFindParameter(3);

                // work out which tab to show based on where we came from
                if (this.Request.UrlReferrer != null)
                {
                    string referrer = this.Request.UrlReferrer.AbsolutePath;
                    if (referrer.EndsWith("WizardNewFunding.aspx")
                        || referrer.EndsWith("FundingPage.aspx")
                        || referrer.EndsWith("ProgrammeFundingPage.aspx"))
                        uxTabControl.SelectedTabID = FundingTab.ID;

                    if (referrer.EndsWith("PaymentPage.aspx"))
                        uxTabControl.SelectedTabID = PaymentsTab.ID;

                    if (referrer.EndsWith("PaperMovePage.aspx"))
                        uxTabControl.SelectedTabID = PaperMoveTab.ID;

                    if (referrer.EndsWith("WizardNewPlacementLink.aspx") ||
                        referrer.EndsWith("PlacementLinkPage.aspx") || referrer.EndsWith("WizardICT.aspx"))
                        uxTabControl.SelectedTabID = PlacementLinksTab.ID;

                    if (referrer.EndsWith("AppStatusPage.aspx"))
                        uxTabControl.SelectedTabID = AppStatusTab.ID;

                    if (referrer.EndsWith("AppMedicalClearancePage.aspx"))
                        uxTabControl.SelectedTabID = MedicalTab.ID;

                }

                //setting the correct tab if it's been requested from Menu page - our volunteers tab
                if (this.Request.QueryString["TAB"] != null)
                {
                    string selectab = this.Request.QueryString["TAB"].ToString();
                    if (selectab.ToUpper() == "EOSPAST")
                        uxTabControl.SelectedTabID = EOSTab.ID;
                }
            
            }
            SetEOSDate(app); 
            CheckSourceGroup();
            doPartnerHyperlink();
            doApplicationChangeHyperlinks();
            doHideJobField();
            doProgrammeFundingHyperlink();
            doNewPlacementLinkHyperlinks();
            doPlacementLinkHyperlinks();
            doFilterDataGridForViewControl();
            doAddNewFinalMedicalClearanceLinkVisibility();

            cmbTandC.Attributes.Add("onchange", "javascript:DisableIfNoTAndCOnChange();");
            if (!Page.ClientScript.IsClientScriptBlockRegistered("script_DisableIfNoTAndC"))
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "script_DisableIfNoTAndC", TAndCDisableScript(), true);

        }

  
 
        /// <summary> 
        /// EOS Date Fix 
        /// </summary> 
        /// <param name="app"></param> 
        private void SetEOSDate(BnbApplication app) 
        { 
            //VAF-450 
            if (!(app.EOSDate == BnbDomainObject.NullDateTime))            
                lblEOSDate.Text = app.EOSDate.ToString("dd/MMM/yyyy");  
        } 
  

        private void doRedirectToNewEnquiryWizard()
        {
            string appID = this.Request.QueryString["BnbApplication"];
            string indvID = getIndividualIDFromApplication(appID);
            // need to pass appID to enquiry wizard
            if (appID != null && appID.ToLower() != "new")
                Response.Redirect(String.Format("WizardNewEnquiry.aspx?BnbIndividual={0}",
                    indvID));
        }

        private void CheckSourceGroup()
        {
            BnbApplication objApplication = bnb.GetPageDomainObject("BnbApplication") as BnbApplication;





            if (objApplication != null)
            {
                string strSourceFilterView = "";

                BnbLookupDataTable dtbResult = BnbEngine.LookupManager.GetLookup("lkuRecruitmentBase");
                BnbLookupRow objFoundRow = dtbResult.FindByID(objApplication.RecruitmentBaseID);
                if (objFoundRow == null)
                {
                    BnbDataGridForDomainObjectList6.DataProperties = "SourceID [BnbLookupControl:vlkuBonoboSourceFull]";
                }
                else
                {
                    strSourceFilterView = objFoundRow["SourceFilterView"].ToString();
                    if (strSourceFilterView.Length > 0)
                    {
                        BnbDataGridForDomainObjectList6.DataProperties = "SourceID [BnbLookupControl:" + strSourceFilterView + "]";
                    }
                    else
                        BnbDataGridForDomainObjectList6.DataProperties = "SourceID [BnbLookupControl:vlkuBonoboSourceFull]";

                }

            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
            navigationBar1.RootDomainObject = "BnbApplication";
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        protected void BnbDataButtons1_NewClick(object sender, System.EventArgs e)
        {
            doRedirectToNewEnquiryWizard();

        }



        private void loadManager()
        {
            // Put user code to initialize the page here
            string qs = Page.Request.QueryString.ToString();
            m_IndividualID = Page.Request.QueryString["BnbIndividual"];

            if (m_IndividualID != null && m_ApplicationID != null)
            // individual and application are given
            {
                m_ApplicationID = Request.QueryString["BnbApplication"];
                m_IndividualID = Request.QueryString["BnbIndividual"];
                bnb = new BnbWebFormManager(this, "BnbApplication");
            }
            else if (m_IndividualID != null)
            {
                // individual is given
                m_ApplicationID = getApplicationIDFromIndividual(m_IndividualID);
                qs = qs.Replace("BnbIndividual=" + m_IndividualID, "BnbApplication=" + m_IndividualID);
                bnb = new BnbWebFormManager(qs);
            }
            else
            {
                // application is given
                m_ApplicationID = Request.QueryString["BnbApplication"];
                m_IndividualID = getIndividualIDFromApplication(m_ApplicationID);
                bnb = new BnbWebFormManager(this, "BnbApplication");
            }
        }

        private string getApplicationIDFromIndividual(string id)
        {
            BnbIndividual ind = BnbIndividual.Retrieve(new System.Guid(id));
            return ind.CurrentApplication.ID.ToString();
        }

        private string getIndividualIDFromApplication(string id)
        {
            BnbApplication app = BnbApplication.Retrieve(new System.Guid(id));
            //BnbApplication app = (BnbApplication)bnb.ObjectTree.DomainObjectDictionary["BnbApplication"];
            return app.IndividualID.ToString();
        }




        public void doPartnerHyperlink()
        {
            BnbApplication app = bnb.GetPageDomainObject("BnbApplication") as BnbApplication;
            if (app != null && app.Individual != null)
            {
                BnbIndividual indiv = app.Individual;
                if (indiv.Partner != null)
                {
                    BnbIndividual partner = indiv.Partner;
                    uxPartnerHyper.Text = String.Format("{0} - {1}", partner.InstanceDescription,
                        partner.PartnerTypeWithStatus);
                    uxPartnerHyper1.Text = String.Format("{0} - {1}", partner.InstanceDescription,
                        partner.PartnerTypeWithStatus);
                    if (partner.IndividualType == BnbIndividual.BnbIndividualType.HasAdditional)
                    {
                        // redirect will go to app or indiv page, whichever releavant						
                        uxPartnerHyper.NavigateUrl = String.Format("../Redirect.aspx?BnbIndividual={0}",
                            partner.ID);
                        uxPartnerHyper1.NavigateUrl = String.Format("../Redirect.aspx?BnbIndividual={0}",
                            partner.ID);
                    }
                    else
                        uxPartnerHyper.NavigateUrl = null;
                    uxPartnerHyper1.NavigateUrl = null;
                }
                else
                {
                    uxPartnerHyper.Text = "(None)";
                    uxPartnerHyper.NavigateUrl = null;
                    uxPartnerHyper1.Text = "(None)";
                    uxPartnerHyper1.NavigateUrl = null;
                }
            }

        }

        public void doApplicationChangeHyperlinks()
        {
            string appguid = Request.QueryString["BnbApplication"];
            if (appguid == null || appguid.ToLower() == "new")
            {
                hyperlinkNextApplication.Visible = false;
                hyperlinkPrevApplication.Visible = false;
                hyperlinkNextApplication1.Visible = false;
                hyperlinkPrevApplication1.Visible = false;
            }
            else
            {
                // hide links initially
                hyperlinkNextApplication.Visible = false;
                hyperlinkPrevApplication.Visible = false;
                hyperlinkNextApplication1.Visible = false;
                hyperlinkPrevApplication1.Visible = false;

                BnbApplication app = BnbApplication.Retrieve(new System.Guid(appguid));
                int currentApp = app.ApplicationOrder;
                int maxApps = 0;
                if (app.Individual != null)
                    maxApps = app.Individual.Applications.Count;
                if (maxApps > 1 && currentApp > -1)
                {
                    app.Individual.Applications.Sort("ApplicationOrder");
                    if (currentApp > 1)
                    {
                        BnbApplication prevApp = app.Individual.Applications[currentApp - 2] as BnbApplication;
                        if (prevApp != null)
                        {
                            hyperlinkPrevApplication.NavigateUrl =
                                String.Format("ApplicationPage.aspx?BnbApplication={0}",
                                prevApp.ID);
                            hyperlinkPrevApplication.Visible = true;

                            hyperlinkPrevApplication1.NavigateUrl =
                                String.Format("ApplicationPage.aspx?BnbApplication={0}",
                                prevApp.ID);
                            hyperlinkPrevApplication1.Visible = true;
                        }
                    }
                    if (currentApp < maxApps)
                    {
                        BnbApplication nextApp = app.Individual.Applications[currentApp] as BnbApplication;
                        if (nextApp != null)
                        {
                            hyperlinkNextApplication.NavigateUrl =
                                String.Format("ApplicationPage.aspx?BnbApplication={0}",
                                nextApp.ID);
                            hyperlinkNextApplication.Visible = true;

                            hyperlinkNextApplication1.NavigateUrl =
                                String.Format("ApplicationPage.aspx?BnbApplication={0}",
                                nextApp.ID);
                            hyperlinkNextApplication1.Visible = true;
                        }
                    }
                }
            }
        }

        private void doHideJobField()
        {
            // if no job, we want to hide the 'job comments' field,
            // otherwise a new BnbRole object gets created on the fly and 
            // messes up the save
            BnbApplication pageApp = bnb.GetPageDomainObject("BnbApplication") as BnbApplication;
            if (pageApp != null && pageApp.PlacedService == null)
            {
                uxRoleComments.Visible = false;
            }




        }

        protected void cmdWFrmBenefitSummary_Clicked(object sender, System.EventArgs e)
        {

            string dir = AppDomain.CurrentDomain.BaseDirectory;
            Guid guiApplicationID = ((BnbApplication)bnb.ObjectTree.DomainObjectDictionary["BnbApplication"]).ID;

            //Set the Word Template file path to the control WordTemplate //property.
            cmdWFrmBenefitSummary.WordTemplate = dir + @"Templates/BenSum.dot";

            DataTable dtResults = BnbAccountHeader.GetBenefitSummaryAdvice(guiApplicationID);

            //only if there are some results then merge the data.
            if (dtResults.Rows.Count > 0)
            {
                // set the control MergeData property with the created DataTable row.
                cmdWFrmBenefitSummary.MergeData = dtResults;

                //set the temporary files directory property.
                cmdWFrmBenefitSummary.TemporaryFilesDirectoryPath = dir + @"temp\";

                cmdWFrmBenefitSummary.FilenameForUser = String.Format("BenefitSummary{0}.doc",
                    DateTime.Now.ToString("ddMMMyyyy-hhmmss"));

                //Call the control's method GenerateWordForm, to generate the merged word document. 
                bool success = cmdWFrmBenefitSummary.GenerateWordForm();

                if (success)
                {
                    BnbWebFormManager.LogAction("Application (Word Form - Benefit Summary)",
                        "Generated Benefit Summary", this);
                    cmdWFrmBenefitSummary.SendResponse(bnb);
                }

            }
            else
                cmdWFrmBenefitSummary.ErrorMessage = "No Benefit Summary data found";
        }
        protected void cmdTravelReqWordForm_Clicked(object sender, System.EventArgs e)
        {

            string dir = AppDomain.CurrentDomain.BaseDirectory;
            Guid guiApplicationID = ((BnbApplication)bnb.ObjectTree.DomainObjectDictionary["BnbApplication"]).ID;

            //Set the Word Template file path to the control WordTemplate //property.
            cmdTravelReqWordForm.WordTemplate = dir + @"Templates/TravReq.dot";

            //create a datatable with data for the word template and 

            BnbListCondition objApplicationMatch = new BnbListCondition("ApplicationID", guiApplicationID);
            BnbCriteria objCriteria = new BnbCriteria(objApplicationMatch);

            BnbQuery objQuery = new BnbQuery("vwBonoboTravelRequisition", objCriteria);
            BnbQueryDataSet objDataSet = objQuery.Execute();
            DataTable dtResults = BonoboWebControls.BnbWebControlServices.GetHtmlEncodedDataTable(objDataSet.Tables["Results"]);
            if (dtResults.Rows.Count > 0)
            {
                // set the control MergeData property with the created DataTable row.
                cmdTravelReqWordForm.MergeData = dtResults;

                //set the temporary files directory property.
                cmdTravelReqWordForm.TemporaryFilesDirectoryPath = dir + @"temp\";
                cmdTravelReqWordForm.FilenameForUser = String.Format("TravelReq{0}.doc",
                    DateTime.Now.ToString("ddMMMyyyy-hhmmss"));

                //Call the control's method GenerateWordForm, to generate the //merged word document. 
                bool success = cmdTravelReqWordForm.GenerateWordForm();

                if (success)
                {
                    BnbEditManager em = new BnbEditManager();
                    BnbApplication objApplication = BnbApplication.Retrieve(guiApplicationID);
                    objApplication.RegisterForEdit(em);
                    objApplication.TravelRequisitionDate = DateTime.Now;
                    em.SaveChanges();

                    BnbWebFormManager.LogAction("Application (Word Form - Travel Req)",
                        "Generated Travel Req", this);

                    cmdTravelReqWordForm.SendResponse(bnb);

                }
            }
            else
                cmdTravelReqWordForm.ErrorMessage = "There is no information available to create the Travel Req";

        }

        private void doAddressLabels()
        {
            //building up a label of all the address details
            Guid indvID = new Guid(m_IndividualID);
            BnbIndividual indiv = BnbIndividual.Retrieve(indvID);

            BnbAddress currentAddress = indiv.CurrentIndividualAddress.Address;
            string[] addressLines = currentAddress.GetAddressLines();

            StringBuilder stbAddress = new StringBuilder();
            foreach (string line in addressLines)
                stbAddress.Append(line + "<br/>");


            if ((currentAddress.GetTelephone(indiv) != null) &&
                (currentAddress.GetTelephone(indiv) != ""))
                stbAddress.Append(currentAddress.GetTelephone(indiv) + "<br/>");

            if ((currentAddress.GetFax(indiv) != null) &&
                (currentAddress.GetFax(indiv) != ""))
                stbAddress.Append("Fax: " + currentAddress.GetFax(indiv) + "<br/>");

            lblAddress.Text = stbAddress.ToString();
            lblAddress1.Text = stbAddress.ToString();

            if (indiv.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Email) != null)
            {
                lblEmail.Text = indiv.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Email).ContactNumber;
                lblEmail1.Text = indiv.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Email).ContactNumber;

            }
            if (indiv.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Mobile) != null)
            {
                lblMobile.Text = indiv.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Mobile).ContactNumber;
                lblMobile1.Text = indiv.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Mobile).ContactNumber;

            }

            //if (indiv.CurrentApplication.EOSDate != DateTime.MinValue)
            //    lblEOSDate.Text = indiv.CurrentApplication.EOSDate.ToString("dd/MMM/yyyy");

            if (indiv.CurrentApplication != null)
            {
                if (indiv.CurrentApplication.EOSConfirmed == true)
                    lblEOSConfirmed.Text = "Yes";
                else
                    lblEOSConfirmed.Text = "No";
            }

        }

        private void doProgrammeFundingHyperlink()
        {

            BnbApplication tempApp = bnb.GetPageDomainObject("BnbApplication") as BnbApplication;
            if (tempApp != null)
            {
                // activate prog funding url if there is a placement
                if (tempApp.PlacedService != null && tempApp.PlacedService.Position != null)
                {
                    hypNewProgrammeFunding.NavigateUrl = String.Format("ProgrammeFundingPage.aspx?BnbPosition={0}&BnbProgrammeFunding=new&PageState=New",
                        tempApp.PlacedService.Position.ID);
                }
                else
                {
                    // disable hyperlink but leave it visible with a message
                    hypNewProgrammeFunding.NavigateUrl = "";
                    hypNewProgrammeFunding.Text = "(No Placement so cannot add Programme Funding)";
                }
            }

        }

        private void doPlacementLinkHyperlinks()
        {
            // add extra hyperlinks into the two event grids
            BnbDataGridHyperlink placementHyperCol = new BnbDataGridHyperlink();
            placementHyperCol.ColumnNameToRenderControl = "Custom_FullPositionReference";
            placementHyperCol.HrefTemplate = "PlacementPage.aspx?BnbPosition={0}";
            placementHyperCol.KeyColumnNames = new string[] { "tblService_PositionID" };
            uxPlacementLinkDataGrid.ColumnControls.Add(placementHyperCol);

            //Added for Medical Clearance grids

            BnbdatagridforviewForMedicalClearance.ColumnControls.Add(placementHyperCol);
            bnbPlacementsForReview.ColumnControls.Add(placementHyperCol);
        }


        private void doNewPlacementLinkHyperlinks()
        {
            string appguid = Request.QueryString["BnbApplication"];
            if (appguid == null || appguid.ToLower() == "new")
            {
                hyperlinkICT.Visible = false;
                hyperlinkNewPlacementLink.Visible = false;
            }
            else
            {
                BnbApplication app = BnbApplication.Retrieve(new System.Guid(appguid));
                if (app.CurrentStatus != null)
                {
                    int statusGroupID = app.CurrentStatus.StatusGroupID;
                    int statusTypeID = app.CurrentStatus.StatusID;
                    //rv
                    if (statusGroupID == BnbConst.StatusGroup_RV)
                    {
                        hyperlinkICT.Visible = false;
                        hyperlinkNewPlacementLink.Visible = false;
                    }
                    //overseas
                    else if (statusGroupID == BnbConst.StatusGroup_Overseas)
                    {
                        if (statusTypeID != BnbConst.Status_OverseasCrossCountryTransfer
                            && statusTypeID != BnbConst.Status_OverseasDeceased)
                        {
                            hyperlinkICT.NavigateUrl = String.Format("WizardICT.aspx?BnbApplication={0}",
                                appguid);
                            hyperlinkICT.Visible = true;
                            hyperlinkNewPlacementLink.Visible = false;
                        }
                        else
                        {
                            hyperlinkICT.Visible = false;
                            hyperlinkNewPlacementLink.Visible = false;
                        }
                    }
                    //other
                    else
                    {

                        hyperlinkNewPlacementLink.NavigateUrl = String.Format("WizardNewPlacementLink.aspx?BnbApplication={0}",
                            appguid);
                        hyperlinkICT.Visible = false;
                        hyperlinkNewPlacementLink.Visible = true;
                    }
                }
            }
        }

        private void doAddNewFinalMedicalClearanceLinkVisibility()
        {
            if (bnbPlacementsForReview.ResultCount > 0 && hasPermissionToEditMedicalClearance())
                BnbdatagridforviewForMedicalClearance.NewLinkVisible = true;
            else
                BnbdatagridforviewForMedicalClearance.NewLinkVisible = false;
        }

        #region " doFilterDataGridForViewControl "
       
        private void doFilterDataGridForViewControl()
        {
            BnbApplication tempApp = bnb.GetPageDomainObject("BnbApplication") as BnbApplication;

            if (tempApp != null)
            {
                BnbCriteria criteria = new BnbCriteria();
                BnbTextCondition app = new BnbTextCondition("tblService_ApplicationID", tempApp.ID.ToString(),BnbTextCompareOptions.ExactMatch);
                criteria.QueryElements.Add(app);
                bnbPlacementsForReview.Criteria = criteria;
            }
        }
        //-->

        #endregion

        private void bnb_SaveEvent(object sender, EventArgs e)
        {
            BnbApplication app = bnb.GetPageDomainObject("BnbApplication", true) as BnbApplication;
            if (app != null)
            {
                if (rdoFull.Checked || rdoPartial.Checked || rdoMCNone.Checked)
                {
                    if (rdoFull.Checked) app.MedicalClearanceTypeID = BnbConst.ClearanceType_Full;
                    else if (rdoPartial.Checked) app.MedicalClearanceTypeID = BnbConst.ClearanceType_Partial;
                    else if (rdoMCNone.Checked) app.MedicalClearanceTypeID = BnbConst.ClearanceType_None;
                }                

                if (cmbTandC.SelectedValue == BnbConst.YesNo_Yes)
                    app.HCFATermsAndConditions = true;
                else if (cmbTandC.SelectedValue == BnbConst.YesNo_No)
                {
                    app.HCFATermsAndConditions = false;
                    app.StartOfPayment = BnbDomainObject.NullDateTime;
                    app.PrepPaymentStatusID = BnbDomainObject.NullInt;
                }                
            }
        }

        private void bnb_EditEvent(object sender, EventArgs e)
        {
            lblClearanceType.Visible = false;
            rdoPartial.Visible = true;
            rdoFull.Visible = true;
            rdoMCNone.Visible = true;
            cmbTandC.Visible = true;
            lblTAndC.Visible = false;

        }

        private void bnb_ViewEvent(object sender, EventArgs e)
        {
            doLoadClearanceType();
            doLoadHCFATermsAndConditions();
            doVisibilityClearanceFields();
            lblClearanceType.Visible = true;
            rdoPartial.Visible = false;
            rdoFull.Visible = false;
            rdoMCNone.Visible = false;
            cmbTandC.Visible = false;
            lblTAndC.Visible = true;
        }

        private void doLoadHCFATermsAndConditions()
        {
            BnbLookupDataTable tAndCLookup = BnbEngine.LookupManager.GetLookup("lkuYesNo");
            DataView tAndCView = new DataView(tAndCLookup);
            tAndCView.RowFilter = "Exclude = 0";
            cmbTandC.DataSource = tAndCView;
            cmbTandC.DataValueField = "Description";
            cmbTandC.DataTextField = "Description";
            cmbTandC.DataBind();
            BnbApplication app = bnb.GetPageDomainObject("BnbApplication", true) as BnbApplication;
            if (app != null)
            {
                if (app.HCFATermsAndConditions)
                {
                    cmbTandC.SelectedValue = BnbConst.YesNo_Yes;
                    lblTAndC.Text = BnbConst.YesNo_Yes;
                }
                else
                {
                    cmbTandC.SelectedValue = BnbConst.YesNo_No;
                    lblTAndC.Text = BnbConst.YesNo_No;
                }
            }
        }

        private void doLoadClearanceType()
        {
            BnbApplication app = bnb.GetPageDomainObject("BnbApplication", true) as BnbApplication;

            if (app != null)
            {
                if (app.MedicalClearanceTypeID == BnbConst.ClearanceType_Full)
                {
                    disableMedicalReq = true;
                    rdoFull.Checked = true;
                    rdoPartial.Checked = false;
                    rdoMCNone.Checked = false;
                    lblClearanceType.Text = BnbConst.ClearanceText_Full;
                    sectionPartialClearance.Visible = false;
                }
                else if (app.MedicalClearanceTypeID == BnbConst.ClearanceType_Partial)
                {
                    disableMedicalReq = false;
                    rdoPartial.Checked = true;
                    rdoFull.Checked = false;
                    rdoMCNone.Checked = false;
                    lblClearanceType.Text = BnbConst.ClearanceText_Partial;
                    sectionPartialClearance.Visible = true;
                }
                else if (app.MedicalClearanceTypeID == BnbConst.ClearanceType_None)
                {
                    if(app.ApplicationMedicalRequirements.Count > 0)
                                disableMedicalReq = false;
                    else
                                disableMedicalReq = true;
                    rdoPartial.Checked = false;
                    rdoFull.Checked = false;
                    rdoMCNone.Checked = true;
                    lblClearanceType.Text = BnbConst.ClearanceText_None;
                    sectionPartialClearance.Visible = false;
                }
            }
        }

        private void doVisibilityClearanceFields()
        {
            if (!(hasPermissionToEditMedicalClearance()))
            {
                BnbGridMedicalRequirements.DisableAllButtons = true;
                rdoPartial.Enabled = false;
                rdoFull.Enabled = false;
                rdoMCNone.Enabled = false;
            }
            else
            {
                if(disableMedicalReq)
                    BnbGridMedicalRequirements.DisableAllButtons = true;
                else
                    BnbGridMedicalRequirements.DisableAllButtons = false;
                rdoPartial.Enabled = true;
                rdoFull.Enabled = true;
                rdoMCNone.Enabled = true;
            }
        }

        private bool hasPermissionToEditMedicalClearance()
        {
            return BnbEngine.SessionManager.GetSessionInfo().HasPermission(
                BnbConst.Permission_EditFinalMedical);
        }


        /// <summary>
        /// Terms and condition -  disable fields - javascript
        /// </summary>
        /// <returns></returns>
        private string TAndCDisableScript()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("function DisableIfNoTAndCOnChange(){");
            sb.Append("if(document.getElementById('" + this.txtStartOfPayment.ClientID + "')){");
            sb.Append("var startOfPay = document.getElementById('" + this.txtStartOfPayment.ClientID + "');");
            sb.Append("var paymentStatus = document.getElementById('" + this.cmbPaymentStatus.ClientID + "');");
            sb.Append("if(document.getElementById('" + this.cmbTandC.ClientID + "'))");
            sb.Append("{");
            sb.Append("var selIdx = document.getElementById('" + this.cmbTandC.ClientID + "').selectedIndex;");
            sb.Append("var tandc  = document.getElementById('" + this.cmbTandC.ClientID + "').options[selIdx].text;");
            sb.Append("if(tandc == '" + BnbConst.YesNo_Yes + "')");
            sb.Append("{startOfPay.readOnly = false;");
            sb.Append("startOfPay.style.backgroundColor = '';");
            sb.Append("paymentStatus.disabled=false;}");
            sb.Append("else{startOfPay.value='dd/mmm/yyyy';");
            sb.Append("startOfPay.readOnly = true; startOfPay.style.backgroundColor = 'rgb(225,225,225)'; ");
            sb.Append("paymentStatus.disabled=true;");
            sb.Append("setSelectedIndex(paymentStatus,''); }}}}");

            sb.Append("function setSelectedIndex(s, v) {");
            sb.Append("for ( var i = 0; i < s.options.length; i++ ) {");
            sb.Append("if ( s.options[i].text == v ) {");
            sb.Append("s.options[i].selected = true;");
            sb.Append("return;}}}");
            sb.Append("if(typeof(window.DisableIfNoTAndCOnChange == 'function'))DisableIfNoTAndCOnChange();");

            return sb.ToString();

        }

    }
}
