using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboDomainObjects;
using BonoboWebControls;


namespace Frond.EditPages
{
    public partial class WizardProposalToContract : System.Web.UI.Page
    {
        private BnbWebFormManager bnb = null;
        private BnbGrant grant = null;
        //private Guid grantID;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
#if DEBUG
            			if (!this.IsPostBack && this.Request.QueryString.ToString() == "")
				Response.Redirect("WizzardProposalToContract.aspx?BnbGrant=5DDE4B6B-F0C9-43A4-ACF3-003E5EC9AE43&BnbGrantProgress=new&PageState=new");
#endif
            bnb = new BnbWebFormManager(this, "BnbGrantProgress");
            grant = (BnbGrant)bnb.ObjectTree.DomainObjectDictionary["BnbGrant"];

			BnbEngine.SessionManager.ClearObjectCache();
            BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
          
            BonoboDomainObjects.BnbProject objProject = BnbProject.Retrieve(grant.ProjectID);
            BnbTextBox2.Text = objProject.ProjectTitle;
           
            if (!this.IsPostBack)
            {
                BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
                BonoboWebControls.BnbWebFormManager.LogAction("New Proposal to Contract Wizard", null, this);
            }

            //this.initWizard();

        }

        public void initWizard()
        {

            if (bnb.PageState == PageState.Valid && Request.QueryString["Wizard"] != "True")
            {
                string querystring = String.Format("ContractPage.aspx?BnbGrant={0}&Ed=1", Request.QueryString["BnbGrant"]);
                Response.Redirect(querystring);
                Response.End();
            }
            

        }

  
        protected void BnbDateBox1_PreRender(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                //--PGM-331
                BonoboDomainObjects.BnbProject objProject = BnbProject.Retrieve(grant.ProjectID);
                BnbDateBox1.Date = objProject.StartDate;
                BnbTextBox2.Text = objProject.ProjectTitle;

            }
        }
        protected void BnbLookupControl1_PreRender(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
                BnbLookupControl1.SelectedValue = BnbConst.GrantStatus_ContractApproved;
        }

        protected void lkuContract_PreRender(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack) 
                lkuContract.SelectedValue = BnbConst.ContractSignedYes_ID;
        }

        protected void BnbDecimalBox1_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) BnbDecimalBox1.DecimalValue = grant.AmountApprovedCurrency;
        }

        protected void BnbDecimalBox2_PreRender(object sender, System.EventArgs e)
        {
            if (!Page.IsPostBack) BnbDecimalBox2.DecimalValue = grant.ExchangeRate;
        }

        protected void BnbButton1_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("ProposalPage.aspx?BnbGrant=" + Request.QueryString["BnbGrant"] + "&Ed=1");
            Response.End();
        }

        protected void bnbFinish_Click(object sender, System.EventArgs e)
        {
            //bnb.Click_Save(sender, e);
            this.initWizard();
        }


        public Guid GrantID
        {
            get { return new Guid(lblHiddenGrantID.Text); }
            set { lblHiddenGrantID.Text = value.ToString(); }
        }

        protected void btnRequestGrantCode_Click(object sender, EventArgs e)
        {
            bnb.Click_Save(sender, e);
        }
    

    }
}
