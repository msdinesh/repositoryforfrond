using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboDomainObjects;
using BonoboEngine;
using BonoboEngine.Query;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for WizardRequestLink.
    /// </summary>
    public partial class WizardRequestLink : System.Web.UI.Page
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here

            if (!this.IsPostBack)
            {
                this.InitWizard();

            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridRequests.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dataGridRequests_ItemDataBound);

        }
        #endregion

        private void InitWizard()
        {
            if (Request.QueryString["BnbPosition"] != null && Request.QueryString["BnbPosition"] != "New")
            {
                Guid positionID = new Guid(Request.QueryString["BnbPosition"]);
                BnbPosition currentPos = BnbPosition.Retrieve(positionID);
                labelPlacementRef.Text = HttpUtility.HtmlEncode(currentPos.FullPlacementReference);
                BnbLookupDataTable progAreaLookup = BnbEngine.LookupManager.GetLookup("lkuProgrammeArea");
                BnbLookupRow foundRow = progAreaLookup.FindByID(currentPos.ProgrammeAreaID);
                if (foundRow != null)
                    labelProgrammeArea.Text = HttpUtility.HtmlEncode(foundRow.Description);
                else
                    labelProgrammeArea.Text = "<not set>";

                if (BnbRuleUtils.HasValue(currentPos.ProgrammeAreaID))
                {

                    BnbListCondition progAreaFilter = new BnbListCondition("tblProgramme_ProgrammeAreaID", currentPos.ProgrammeAreaID);
                    BnbListCondition unLinkedFilter = new BnbListCondition();
                    unLinkedFilter.Fieldname = "tblRequest_PositionID";
                    unLinkedFilter.ListCompareOption = BnbListCompareOptions.IsEmpty;
                    BnbCriteria requestCriteria = new BnbCriteria(progAreaFilter);

                    requestCriteria.QueryElements.Add(unLinkedFilter);
                    BnbQuery requestQuery = new BnbQuery("vwBonoboFindRequest", requestCriteria);
                    BnbQueryDataSet matchRequests = requestQuery.Execute();

                    if (matchRequests.Tables["Results"].Rows.Count > 0)
                    {
                        panelRequestChoose.Visible = true;
                        dataGridRequests.DataSource = BonoboWebControls.BnbWebControlServices.GetHtmlEncodedDataTable(matchRequests.Tables["Results"]);
                        dataGridRequests.DataBind();
                    }
                    else
                    {
                        buttonFinish.Enabled = false;
                        panelRequestChoose.Visible = false;
                        labelPanelOneFeedback.Text = "No unlinked Requests could be found for the Programme Area of the Placement. "
                            + " Please use the Programme page to add suitable Requests.";

                    }
                }
                else
                {
                    buttonFinish.Enabled = false;
                    panelRequestChoose.Visible = false;
                    labelPanelOneFeedback.Text = "Programme Area must be set for the Placement before it can be linked to a Request. "
                        + "Please return to the Placement Page by pressing Cancel below, and set the Programme Area.";
                }
            }
            else
                throw new ApplicationException("Wizard must be passed a BnbPosition querystring to work properly");



        }



        private void dataGridRequests_ItemDataBound(object sender, DataGridItemEventArgs e)
        {

            Label itemLabel = e.Item.FindControl("labelRadio") as Label;
            if (itemLabel != null)
            {
                itemLabel.Text = this.GetRadioHTML(e.Item);
            }
        }

        private string GetRadioHTML(DataGridItem item)
        {
            string loopRequestID = item.Cells[1].Text;
            string htmlRadio = "<input type=radio name='requestGroup' value='" + loopRequestID + "'";
            if (loopRequestID == this.SelectedRequestID)
                htmlRadio += " checked ";
            htmlRadio += ">";
            return htmlRadio;

        }



        private string SelectedRequestID
        {
            get { return labelHiddenRequestID.Text; }
            set { labelHiddenRequestID.Text = value; }
        }

        private void ShowCheckedItem()
        {

            foreach (DataGridItem iLoop in dataGridRequests.Items)
            {
                Label radioLabel = (Label)iLoop.FindControl("labelRadio");
                if (radioLabel != null)
                {
                    radioLabel.Text = this.GetRadioHTML(iLoop);
                }
            }
        }

        protected void buttonFinish_Click(object sender, System.EventArgs e)
        {
            string selectedButton = Request.Form["requestGroup"];
            if (selectedButton == null)
                labelPanelOneFeedback.Text = "Please select a Request";
            else
            {
                this.SelectedRequestID = selectedButton;

                // so now we actually do it!
                Guid positionID = new Guid(Request.QueryString["BnbPosition"]);
                BnbPosition linkPos = BnbPosition.Retrieve(positionID);
                BnbRequest req = BnbRequest.Retrieve(new Guid(this.SelectedRequestID));
                BnbEditManager em = new BnbEditManager();
                req.RegisterForEdit(em);
                req.Position = linkPos;
                try
                {
                    em.SaveChanges();
                    Response.Redirect(String.Format("RequestPage.aspx?BnbProgramme={0}&BnbRequest={1}",
                        req.ProgrammeID.ToString(),
                        req.ID.ToString()));
                }
                catch (BnbProblemException pe)
                {
                    labelPanelOneFeedback.Text = "";
                    foreach (BnbProblem p in pe.Problems)
                        labelPanelOneFeedback.Text += p.Message + "<br/>";

                }
            }
        }

        protected void buttonCancel_Click(object sender, System.EventArgs e)
        {
            Guid positionID = new Guid(Request.QueryString["BnbPosition"]);
            Response.Redirect("PlacementPage.aspx?BnbPosition=" + positionID.ToString());

        }
    }
}
