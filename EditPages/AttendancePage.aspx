<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Page language="c#" Codebehind="AttendancePage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.AttendancePage" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/bnbattendance24silver.gif"></uc1:title>
				<uc1:NavigationBar id="NavigationBar1" runat="server"></uc1:NavigationBar>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" NewButtonEnabled="False"
					NewWidth="55px" EditAllWidth="55px" SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px" NewText="New"
					EditAllText="Edit All" SaveText="Save" UndoText="Undo"></bnbpagecontrol:bnbdatabuttons>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox2" runat="server" PopupMessage="false" SuppressRecordSavedMessage="false"
					CssClass="messagebox"></bnbpagecontrol:bnbmessagebox>
				<bnbpagecontrol:bnbsectionheading id="BnbSectionHeading1" runat="server" CssClass="sectionheading" SectionHeading="Attendance Details"
					Width="100%" Height="20px" Collapsed="false"></bnbpagecontrol:bnbsectionheading>
				<P>
					<TABLE class="fieldtable" id="Table1" width="100%" border="0">
						<TR>
							<TD class="label" width="50%" style="HEIGHT: 22px">Event Reference</TD>
							<TD width="50%" style="HEIGHT: 22px"><bnbdatacontrol:bnbdomainobjecthyperlink id="BnbDomainObjectHyperlink1" runat="server" RedirectPage="EventPage.aspx" GuidProperty="BnbAttendance.Event.EventID"
									DataMember="BnbAttendance.Event.EventDescription" HyperlinkText="[BnbDomainObjectHyperlink]" Target="_top"></bnbdatacontrol:bnbdomainobjecthyperlink>
							</TD>
						</TR>
						<TR>
							<TD class="label" width="50%">Volunteer Ref</TD>
							<TD width="50%"><bnbdatacontrol:bnbtextbox id="BnbTextBox1" runat="server" CssClass="textbox" Width="250px" Height="20px" DataMember="BnbAttendance.Individual.RefNo"
									MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 25px" width="50%">Name</TD>
							<TD style="HEIGHT: 25px" width="50%"><bnbdatacontrol:bnbtextbox id="BnbTextBox2" runat="server" CssClass="textbox" Width="250px" Height="20px" DataMember="BnbAttendance.Individual.FullName"
									MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" width="50%">Accompanying</TD>
							<TD width="50%">
								<bnbdatacontrol:BnbDecimalBox id="BnbDecimalBox1" runat="server" CssClass="textbox" Height="20px" Width="246px"
									DataMember="BnbAttendance.Accompanying" TextAlign="left" FormatExpression="#,0" DisplayEmptyValue="False"></bnbdatacontrol:BnbDecimalBox></TD>
						</TR>
						<TR>
							<TD class="label" width="50%">Outcome</TD>
							<TD width="50%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									DataMember="BnbAttendance.OutcomeID" LookupTableName="lkuOutcome" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>
						<TR>
							<TD class="label" width="50%">Outcome Reason</TD>
							<TD width="50%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl2" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									DataMember="BnbAttendance.OutcomeReasonID" LookupTableName="lkuOutcomeReason" LookupControlType="ComboBox" ListBoxRows="4"
									DataTextField="Description"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>
						<TR>
							<TD class="label" width="50%">Comments</TD>
							<TD width="50%"><bnbdatacontrol:bnbtextbox id="BnbTextBox4" runat="server" CssClass="textbox" Width="250px" Height="20px" DataMember="BnbAttendance.Comments"
									MultiLine="True" Rows="3" StringLength="0"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" width="50%">Paid amount</TD>
							<TD width="50%"><bnbdatacontrol:bnbtextbox id="BnbTextBox5" runat="server" CssClass="textbox" Width="250px" Height="20px" DataMember="BnbAttendance.PaidAmount"
									MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" width="50%">Paid Date</TD>
							<TD width="50%"><bnbdatacontrol:bnbdatebox id="BnbDateBox1" runat="server" CssClass="datebox" Width="90px" Height="20px" DataMember="BnbAttendance.PaidDate"
									DateFormat="dd/mm/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
						</TR>
						<TR>
							<TD class="label" width="50%">Arrive</TD>
							<TD width="50%">
								<bnbdatacontrol:BnbDateBox id="BnbDateBox2" runat="server" CssClass="datebox" Height="20px" Width="90px" DataMember="BnbAttendance.ArrivalDate"
									DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbDateBox></TD>
						</TR>
						<TR>
							<TD class="label" width="50%">Depart</TD>
							<TD width="50%">
								<bnbdatacontrol:BnbDateBox id="BnbDateBox3" runat="server" CssClass="datebox" Height="20px" Width="90px" DataMember="BnbAttendance.DepartureDate"
									DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbDateBox></TD>
						</TR>
					</TABLE>
				</P>
				<P><bnbpagecontrol:bnbsectionheading id="Bnbsectionheading3" runat="server" CssClass="sectionheading" SectionHeading="Situation"
						Width="100%" Height="20px" Collapsed="false"></bnbpagecontrol:bnbsectionheading></P>
				<P>
					<bnbdatagrid:BnbDataGridForDomainObjectList id="BnbDataGridForDomainObjectList1" runat="server" CssClass="datagrid" DataMember="BnbAttendance.AttendanceProgresses"
						AddButtonVisible="true" EditButtonText="Edit" AddButtonText="Add" EditButtonVisible="False" DeleteButtonVisible="false"
						DataGridType="Editable" DataProperties="AttendanceStatusID [BnbLookupControl:lkuAttendanceStatus], StatusDate [BnbDateBox], Reason [BnbTextBox]"></bnbdatagrid:BnbDataGridForDomainObjectList></P>
				<P>&nbsp;</P>
			</div>
		</form>
	</body>
</HTML>
