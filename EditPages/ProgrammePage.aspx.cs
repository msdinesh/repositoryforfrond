using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboWebControls.DataGrids;
using BonoboEngine.Query;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for ProgrammePage.
    /// </summary>
    public partial class ProgrammePage : System.Web.UI.Page
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
#if DEBUG
            BnbWebFormManager bnb = null;
            //if (Request.ServerVariables["QUERY_STRING"] == "")
            //{
            //    bnb = new BnbWebFormManager("BnbProgramme=2966D6B1-9E13-473F-A9E6-AF92BF9756A3");
            //}
            //else
            //{
                bnb = new BnbWebFormManager(this, "BnbProgramme");
           // }
#else
			BnbWebFormManager bnb = new BnbWebFormManager(this,"BnbProgramme");
#endif
            // turn on history logging
            bnb.LogPageHistory = true;

            //<Integartion>
            bnb.PageNameOverride = "Programme";
            BnbWorkareaManager.SetPageStyleOfUser(this);

            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null)
                    titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
                this.FilterProgrammeViews();
                if (!this.IsPostBack)
                {
                    // work out which tab to show based on where we came from
                    if (this.Request.UrlReferrer != null)
                    {
                        string referrer = this.Request.UrlReferrer.AbsolutePath;
                        if (referrer.EndsWith("ProgrammeComments.aspx"))              
                            BnbTabControl1.SelectedTabID = ProgrammeNotesTab.ID;
                        if(referrer.EndsWith("RequestPage.aspx"))
                            BnbTabControl1.SelectedTabID = RequestsTab.ID;
                    }
                }





            }
            //</Integartion>
          
            doAddHyperlinksToDataGrids();

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        protected void cmdFilter_Click(object sender, System.EventArgs e)
        {
            if (txtFilterStart.Text != "" && !BnbWebControlServices.IsDate(txtFilterStart.Text))
            {
                lblFilterMessage.Text = "Start date entered is invalid.";
                return;
            }
            if (txtFilterEnd.Text != "" && !BnbWebControlServices.IsDate(txtFilterEnd.Text))
            {
                lblFilterMessage.Text = "End date entered is invalid.";
                return;
            }
            this.FilterProgrammeViews();
        }

        private void FilterProgrammeViews()
        {
            lblFilterMessage.Text = "";

            Guid filterProgrammeID = Guid.Empty;
            if (this.Request.QueryString["BnbProgramme"] != null)
            {
                string queryProgrammeID = this.Request.QueryString["BnbProgramme"];
                if (queryProgrammeID != "new")
                    filterProgrammeID = new Guid(queryProgrammeID);
            }

            DateTime compareDate1 = DateTime.MinValue;
            DateTime compareDate2 = DateTime.MinValue;
            if (txtFilterStart.Text != "")
                compareDate1 = Convert.ToDateTime(txtFilterStart.Text, new System.Globalization.CultureInfo("en-GB"));
            else
                compareDate1 = new DateTime(1901, 1, 1);

            if (txtFilterEnd.Text != "")
                compareDate2 = Convert.ToDateTime(txtFilterEnd.Text, new System.Globalization.CultureInfo("en-GB"));
            else
                compareDate2 = new DateTime(2500, 1, 1);


            dataGridProgrammeComments.Criteria = this.MakeViewCriteria(filterProgrammeID,
                    compareDate1, compareDate2, "tblProgrammeComments_ProgrammeID", "tblProgrammeComments_CommentDate");

            dataGridRequests.Criteria = this.MakeViewCriteria(filterProgrammeID,
                compareDate1, compareDate2, "tblRequest_ProgrammeID", "tblRequest_VolStartDate");

            dataGridPositions.Criteria = this.MakeViewCriteria(filterProgrammeID,
                compareDate1, compareDate2, "tblProgramme_ProgrammeID", "tblPosition_EarliestStartDate");


        }

        private BnbCriteria MakeViewCriteria(Guid filterProgrammeID,
            DateTime dateFilterStart,
            DateTime dateFilterEnd,
            string programmeIDFieldName,
            string dateFieldName)
        {
            BnbDateCondition dateFilter = new BnbDateCondition();
            dateFilter.CompareDate1 = dateFilterStart;
            dateFilter.CompareDate2 = dateFilterEnd;
            dateFilter.DateCompareOptions = BnbDateCompareOptions.FieldBetweenDates;
            dateFilter.Fieldname1 = dateFieldName;
            BnbListCondition programmeFilter = new BnbListCondition(programmeIDFieldName, filterProgrammeID);
            BnbCriteria returnCritera = new BnbCriteria();
            returnCritera.QueryElements.Add(programmeFilter);
            returnCritera.QueryElements.Add(dateFilter);
            return returnCritera;
        }

        private void doAddHyperlinksToDataGrids()
        {
            BnbDataGridHyperlink hl1 = new BnbDataGridHyperlink();
            hl1.HrefTemplate = "PlacementPage.aspx?BnbPosition={0}";
            hl1.KeyColumnNames = new string[] { "tblRequest_PositionID" };
            hl1.ColumnNameToRenderControl = "Custom_FullPositionReference";
            hl1.PositionForNewColumn = BnbDataGridNewColumnPosition.RightHandSideOfGrid;
            hl1.HeadingTitleForNewColumn = "New column for Click me";

            BnbDataGridHyperlink hl2 = new BnbDataGridHyperlink();
            hl2.HrefTemplate = "RequestPage.aspx?BnbProgramme={0}&BnbRequest={1}";
            hl2.KeyColumnNames = new string[] { "tblProgramme_ProgrammeID", "tblRequest_RequestID" };
            hl2.ColumnNameToRenderControl = "Custom_RequestLinkInfo";

            BnbDataGridHyperlink hlVol = new BnbDataGridHyperlink();
            //	hlVol.HrefTemplate		= "IndividualPage.aspx?BnbIndividual={0}";
            //	hlVol.KeyColumnNames		= new string[] {"tblIndividualKeyInfo_IndividualID"};
            hlVol.HrefTemplate = "ApplicationPage.aspx?BnbApplication={0}";
            hlVol.KeyColumnNames = new string[] { "tblApplication_applicationID" };

            hlVol.ColumnNameToRenderControl = "Custom_DisplayName";

            dataGridRequests.ColumnControls.Add(hl1);
            dataGridPositions.ColumnControls.Add(hl2);
            dataGridPositions.ColumnControls.Add(hlVol);
        }
    }
}
