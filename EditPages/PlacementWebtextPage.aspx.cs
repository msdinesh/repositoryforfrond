using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboWebControls;

namespace Frond.EditPages
{
    public partial class PlacementWebTextPage : System.Web.UI.Page
    {

        private BnbWebFormManager bnb = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            bnb = new BnbWebFormManager(this, "BnbPositionWebText");
            bnb.LogPageHistory = true;
            BnbWorkareaManager.SetPageStyleOfUser(this);
            string posID = Page.Request.QueryString["BnbPosition"]; 
           
            navigationBar.RootDomainObject = "BnbPosition";
            navigationBar.RootDomainObjectID = posID;

            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null)
                    titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
            }
        }
    }
}
