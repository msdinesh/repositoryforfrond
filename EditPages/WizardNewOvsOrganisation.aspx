<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>

<%@ Page Language="c#" Codebehind="WizardNewOvsOrganisation.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.EditPages.WizardNewOvsOrganisation" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>WizardNewOvsOrganisation</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form2" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <div class="wizardheader" ms_positioning="FlowLayout">
                <h3>
                    <img class="icon24" src="../Images/wizard24silver.gif">
                    New Overseas Organisation Wizard</h3>
            </div>
            <asp:Panel ID="panelOne" runat="server" CssClass="wizardpanel">
                <p>
                    <em>This wizard will help you to add a new Ovs Organisation record.</em></p>
                <p>
                    Please enter the Country and Name of the new Ovs Organisation and press 
                    Next</p>
                <p>
                </p>
                <table class="wizardtable" id="Table2">
                    <tr>
                        <td class="label" style="height: 15px">
                            Organisation Country</td>
                        <td style="height: 15px">
                            <asp:DropDownList ID="dropDownCountry" runat="server" Width="229px">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td class="label">
                            Organisation Name</td>
                        <td>
                            <asp:TextBox ID="textBoxOrganisationName" runat="server" Width="240px"></asp:TextBox></td>
                    </tr>
                </table>
                <p>
                    <asp:Label ID="labelPanelOneFeedback" runat="server" CssClass="feedback"></asp:Label></p>
            </asp:Panel>
            <asp:Panel ID="panelTwo" runat="server" CssClass="wizardpanel">
                <asp:Panel ID="panelOrganisationChoose" runat="server">
                    <p>
                        The following similar Ovs Organisations already exist. If any of them match the
                        data you were going to enter, then please use them instead by clicking on the view
                        links.</p>
                    <p>
                    </p>
                    <p>
                        <asp:DataGrid ID="dataGridOrganisations" runat="server" CssClass="datagrid" AutoGenerateColumns="False">
                            <Columns>
                                <asp:HyperLinkColumn Text="View" DataNavigateUrlField="tblOrganisation_OrganisationID"
                                    DataNavigateUrlFormatString="OvsOrganisationPage.aspx?BnbOrganisation={0}"></asp:HyperLinkColumn>
                                <asp:BoundColumn DataField="tblOrganisation_OrganisationName" HeaderText="Organisation Name">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="lkuCountry_Description" HeaderText="Country"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tblAddress_AddressLine1" HeaderText="Address Line 1"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tblAddress_AddressLine2" HeaderText="Address Line 2"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid></p>
                    <p>
                        If none of these existing Organisations match, then press Finish to create the
                    new Organisation.</asp:Panel>
                <asp:Label ID="labelNoDuplicates" runat="server">No similar Ovs Organisations already exist. Press Finish to create your new Organisation.</asp:Label>
                <p>
                    <asp:Label ID="labelPanelTwoFeedback" runat="server"></asp:Label>
            </asp:Panel>
            <div class="wizardheader">
                <table id="Table1" style="margin: 0px" cellspacing="0" cellpadding="0" width="100%"
                    border="0">
                    <tr>
                        <td align="right" width="30%">
                            <asp:Button ID="buttonPrev" runat="server" Text="<< Prev" OnClick="buttonPrev_Click">
                            </asp:Button></td>
                        <td align="center" width="40%">
                            <asp:Button ID="buttonCancel" runat="server" Text="Cancel" OnClick="buttonCancel_Click">
                            </asp:Button></td>
                        <td align="left" width="30%">
                            <asp:Button ID="buttonFinish" runat="server" Text="Finish" OnClick="buttonFinish_Click">
                            </asp:Button>
                            <asp:Button ID="buttonNext" runat="server" Text="Next >>" OnClick="buttonNext_Click">
                            </asp:Button></td>
                    </tr>
                </table>
            </div>
            <p>
                &nbsp;</p>
            <p>
                <asp:Label ID="labelHiddenRequestID" runat="server" Visible="False"></asp:Label></p>
        </div>
    </form>
</body>
</html>
