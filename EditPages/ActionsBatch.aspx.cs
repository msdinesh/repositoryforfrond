using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using BonoboDomainObjects;
using BonoboEngine.Utilities;
using BonoboEngine;
using BonoboWebControls;
using System.Text.RegularExpressions;
namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for ActionsBatch.
	/// </summary>
	public partial class ActionsBatch : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
		protected void Page_Load(object sender, System.EventArgs e)
		{
			BnbWorkareaManager.SetPageStyleOfUser(this);
			if(! IsPostBack )
			{
				BnbWorkareaManager.CheckPageAccess(this);
				titleBar.TitleText = "Actions Batch";
				dropdownActionType.SelectedValue = BnbConst.ContactType_Other;
				dropdownDescription.SelectedValue = BnbConst.ContactDescription_AMailing ;
				dropdownStatus.SelectedValue=BnbConst.ContactStatus_Complete ;
				dropdownSentVia.SelectedValue=BnbConst.SentVia_Mailing ;
				//errorLabel.Text=string.Empty;
				//successLabel.Text=string.Empty;
				BnbWebFormManager.LogAction(titleBar.TitleText, null, this);
			}   
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		protected void buttonCreateActions_Click(object sender, System.EventArgs e)
		{	
			errorLabel.Text=string.Empty;
			successLabel.Text=string.Empty;
			//the uploaded csv file must have a column with the following header:
			const string RefColumnHeader="Ref No";
			//and may have a column for app no.
			const string AppColumnHeader="App";
			//regular expression for spliting on comma, but not comma appearing between quotes
			//if parsing errors occur with a healthy csv file, the problem may be with the regular expression
			string regexPattern = ",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))";
			ArrayList csv=new ArrayList();
			try 
			{
				using (Stream stream=fileUploader.PostedFile.InputStream)
				using (StreamReader sr = new StreamReader(stream))
				{
					Regex regex = new Regex(regexPattern);
					string line;
				
					while ((line = sr.ReadLine()) != null) 
					{

						//slit the line on comma (ignoring certain commas - defined in regexPattern)
						string[]splitLine=regex.Split (line);
						//add this line as an array to csv, which will eventually 
						//contain a parsed version of the complete file.
						//trim double quotes that may be surrounding individual fields
						csv.Add(BnbGeneralUtilities.TrimStringArrayItems(splitLine,"\"","\""));
					}
				}
				if(csv.Count==0)//csv has no rows
					throw new CSVFormatException("The uploaded CSV file is empty.");
				//location of ref column
				int refColumn=  BnbGeneralUtilities.GetIndexOfArrayItem((string[])csv[0],RefColumnHeader);
				
				//location of app column, if any
				int appColumn= BnbGeneralUtilities.GetIndexOfArrayItem((string[])csv[0],AppColumnHeader);
				if(refColumn<0)
					throw new CSVFormatException("A CSV file with a column titled 'Ref No' must be uploaded.");
				
				//get the csv values without duplicate
				ArrayList csvNoDuplicate = new ArrayList();
				csvNoDuplicate = RemoveDuplicates(csv,refColumn);

				ArrayList items=new ArrayList();
//				foreach(string[] row in csv.GetRange(1,csv.Count-1))
				foreach(string[] row in csvNoDuplicate.GetRange(0,csvNoDuplicate.Count))
					//for each row in the file - except the first one which contains headers...
				{
					FeedbackItem feedbackItem=new FeedbackItem();
					try
					{
						
						BnbEditManager em = new BnbEditManager();
						BnbContactDetail bnbContactDetail=new BnbContactDetail(true);
						bnbContactDetail.RegisterForEdit(em);
						feedbackItem.RefNo=row[refColumn];
						BnbIndividual individual=BnbIndividual.RetrieveByRefNo(row[refColumn]);
						if(individual==null)
						{
							//feedbackItem.Problems.Add("There is no individual on the system with this Ref no.");
							continue;//skip to next item (although there is a finally block that will run)
						}
						if(radioIndividual.Checked) 
							bnbContactDetail.Individual=individual;//link directly to individual
						else
							try
							{//try to get a valid number from the app column
								string appString= 	row[appColumn];
								//handle apps of the form 1/3
								int slashPosition = appString.IndexOf('/');
								if(slashPosition >0)
									appString=appString.Substring(0,slashPosition );
								int appNo=int.Parse(appString);
								individual.Applications.Sort("ApplicationOrder");
								//and try to add the corresponding application to the contact detail
								bnbContactDetail.Application = (BnbApplication)individual.Applications[appNo-1] ;
							}
							catch
							{//failing that, use the current application
								bnbContactDetail.Application  =individual.CurrentApplication;
							}
						bnbContactDetail.ContactDescriptionID =(int)dropdownDescription.SelectedValue;  
						bnbContactDetail.Information=textBoxInformation.Text;
						bnbContactDetail.ContactStatusID=(int)dropdownStatus.SelectedValue;
						bnbContactDetail.SentViaID=(int)dropdownSentVia.SelectedValue;
						em.SaveChanges();
						feedbackItem.Success=true;
					} 
					catch (ApplicationException catchAll)
					{
						BnbProblemException bnbProblemException = catchAll as BnbProblemException; 
						if(bnbProblemException !=null)
							foreach(BnbProblem problem in bnbProblemException.Problems)
								feedbackItem.Problems.Add(problem.Message);	
						else
							feedbackItem.Problems.Add(catchAll.Message );
					}
					finally
					{
						items.Add(feedbackItem);
					}
				}
				errorLabel.Text=string.Empty;
				successLabel.Text=string.Format("Read {0} rows from file",csv.Count-1)+ "<br/>";
				int successCount=0;
				foreach(FeedbackItem feedbackItem in items)
				{
					if(feedbackItem.Success)
						successCount++;
					else
					{
						if(errorLabel.Text==string.Empty)
							errorLabel.Text="The following actions failed:<br/>";
						errorLabel.Text+="&nbsp;&nbsp;There is no individual in VSO Database with Reference Number: " + feedbackItem.RefNo + "<br/>";
						foreach(string problem in feedbackItem.Problems)
							errorLabel.Text+="&nbsp;&nbsp;&nbsp;&nbsp;" + problem + "<br/>";
					}
				}
				successLabel.Text+=string.Format ("{0} actions were successfully created",successCount)+ "<br/>";
				// log a summary to the action log
				BnbWebFormManager.LogAction(titleBar.TitleText + " (Batch Run)", String.Format ("Read {0} rows from file, {1} actions were successfully created",csv.Count-1, successCount), this);
			}
			catch (CSVFormatException CsvE)
			{
				errorLabel.Text=CsvE.Message;
			}
			catch (ApplicationException catchAll)
			{
				errorLabel.Text="other error: " + catchAll.Message;
			}
		}
		

		
		//	This method removes duplicates from the given arraylist
		private ArrayList RemoveDuplicates(ArrayList items, int refColumn) 
		{ 
			//array containing only the individual reference number
			ArrayList vntRefColumn = new ArrayList();

			ArrayList vntNoDups = new ArrayList(); 
			foreach(string[] row in items.GetRange(1,items.Count-1)) 
			{ 
				if (!vntRefColumn.Contains(row[refColumn]))
				{
					vntRefColumn.Add(row[refColumn]);
					vntNoDups.Add(row); 
				} 

			} 
			//noDups.Sort(); 
			return vntNoDups; 

		}


        protected void buttonCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("../Menu.aspx");
		}
	}
	public class CSVFormatException:ApplicationException
	{
		public CSVFormatException(string message)
			: base(message)
		{
		}
	}
	class FeedbackItem
	{
		public string RefNo;
		public ArrayList Problems=new ArrayList();
		public bool Success;
	}
}
