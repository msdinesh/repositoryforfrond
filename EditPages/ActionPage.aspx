<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="cc1" Namespace="BonoboWebControls.ServicesControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="ActionPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.ActionPage" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/bnbcontactdetail24silver.gif"></uc1:title>
				<uc1:NavigationBar id="NavigationBar1" runat="server"></uc1:NavigationBar>
                &nbsp;
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" NewButtonEnabled="False"
					NewWidth="55px" EditAllWidth="55px" SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px" NewText="New"
					EditAllText="Edit All" SaveText="Save" UndoText="Undo" Height="41px"></bnbpagecontrol:bnbdatabuttons>
				<bnbgenericcontrols:BnbButton id="BnbButton1" runat="server" Width="55px" Text="Delete" Height="25px" onclick="BnbButton1_Click" CssClass="databuttons" border="4"></bnbgenericcontrols:BnbButton>		
				
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:bnbmessagebox>
				<asp:Label id="lblSaveError" runat="server" Width="33px" ForeColor="Red"></asp:Label>
				<TABLE class="fieldtable" id="Table1" width="100%" border="0">
					<TR>
						<TD class="label" style="HEIGHT: 20px" width="25%">Individual Name</TD>
						<TD style="HEIGHT: 20px" width="25%">
							<bnbdatacontrol:BnbDomainObjectHyperlink id="BnbDomainObjectHyperlink2" runat="server" DataMember="BnbIndividual.FullName"
								Target="_top" HyperlinkText="[BnbDomainObjectHyperlink]" GuidProperty="BnbIndividual.ID" RedirectPage="IndividualPage.aspx"></bnbdatacontrol:BnbDomainObjectHyperlink></TD>
					</TR>
					<TR>
						<TD class="label" style="HEIGHT: 20px" width="25%">Application Link</TD>
						<TD style="HEIGHT: 20px" width="25%">
							<asp:DropDownList id="uxApplicationPicker" runat="server" Width="248px"></asp:DropDownList>
							<asp:Label id="uxApplicationLabel" runat="server" Visible="False"></asp:Label></TD>
					</TR>
					<TR>
						<TD class="label" style="HEIGHT: 20px" width="25%">VSO Description</TD>
						<TD style="HEIGHT: 20px" width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLkuContactDescription" runat="server" CssClass="lookupcontrol" Width="304px"
								Height="22px" LookupTableName="vlkuBonoboContactDescriptionWithType" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"
								DataMember="BnbContactDetail.ContactDescriptionID"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="HEIGHT: 20px" width="25%">Action Status</TD>
						<TD style="HEIGHT: 20px" width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl2" runat="server" CssClass="lookupcontrol" Width="136px" Height="22px"
								LookupTableName="lkuContactStatus" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" DataMember="BnbContactDetail.ContactStatusID"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="HEIGHT: 20px" width="25%">Sent Via</TD>
						<TD style="HEIGHT: 20px" width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLkuSentVia" runat="server" CssClass="lookupcontrol" Width="136px" Height="22px"
								LookupTableName="lkuSentVia" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" DataMember="BnbContactDetail.SentViaID"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="HEIGHT: 20px" width="25%">Actioned On</TD>
						<TD style="HEIGHT: 20px" width="25%"><bnbdatacontrol:bnbdatebox id="BnbDteActionedOn" runat="server" CssClass="datebox" Width="90px" Height="20px"
								DataMember="BnbContactDetail.DateSent" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
					<TR>
						<TD class="label" style="HEIGHT: 20px" width="25%">Actioned By</TD>
						<TD style="HEIGHT: 20px" width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLkuActionedBy" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
								LookupTableName="vlkuBonoboUser" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" DataMember="BnbContactDetail.SentByID"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="HEIGHT: 10px" width="25%">To Individual</TD>
						<TD style="HEIGHT: 10px" width="25%"><bnbdatacontrol:bnbcheckbox id="BnbChkToFrom" runat="server" CssClass="checkbox" DataMember="BnbContactDetail.ToFrom"></bnbdatacontrol:bnbcheckbox></TD>
					</TR>
					<TR>
						<TD class="label" style="HEIGHT: 10px" width="25%">Information</TD>
						<TD style="HEIGHT: 10px" width="25%"><bnbdatacontrol:bnbtextbox id="BnbTxtInformation" runat="server" CssClass="textbox" Width="250px" Height="20px"
								DataMember="BnbContactDetail.Information" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
					</TR>
					<TR>
						<TD class="label" style="HEIGHT: 10px" width="25%">Mailing Reference</TD>
						<TD style="HEIGHT: 10px" width="25%"><bnbdatacontrol:bnbtextbox id="BnbTxtMailingReference" runat="server" CssClass="textbox" Width="128px" Height="20px"
								DataMember="BnbContactDetail.MailingReference" MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
					</TR>
					<TR>
						<TD class="label" style="HEIGHT: 10px" width="25%">Chase Date</TD>
						<TD style="HEIGHT: 10px" width="25%"><bnbdatacontrol:bnbdatebox id="BnbDteDateChase" runat="server" CssClass="datebox" Width="90px" Height="20px"
								DataMember="BnbContactDetail.DateChase" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
				</TABLE>
				<cc1:bnbwordformbutton id="BnbWordFormButton1" runat="server" CssClass="button" Width="128px" Text="Standard Letters" onclicked="BnbWordFormButton1_Clicked"></cc1:bnbwordformbutton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</div>
		</form>
	</body>
</HTML>
