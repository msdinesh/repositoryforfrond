using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for OvsOrganisationPage.
    /// </summary>
    public partial class OvsOrganisationPage : System.Web.UI.Page
    {

        //protected UserControls.SFNavigationBar SFNavigationBar1;
        //<Integration>
        protected UserControls.NavigationBar navigationBar;
        protected UserControls.Title titleBar;
        //</Integration>

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            // 

            // re-direct to wizard if new mode
            if (this.Request.QueryString["BnbOrganisation"] != null &&
                this.Request.QueryString["BnbOrganisation"].ToLower() == "new")
                Response.Redirect("WizardNewOvsOrganisation.aspx");
#if DEBUG
            BnbWebFormManager bnb = null;
            if (Request.QueryString.ToString() == "")
                BnbWebFormManager.ReloadPage("BnbOrganisation=30D76E24-8578-49C6-9D83-060A2531F00F");
            else
                bnb = new BnbWebFormManager(this, "BnbOrganisation");
#else
			BnbWebFormManager bnb = new BnbWebFormManager(this,"BnbOrganisation");
#endif
            // turn on logging
            bnb.LogPageHistory = true;
            //<Integartion>
            bnb.PageNameOverride = "Overseas Organisation";
            BnbWorkareaManager.SetPageStyleOfUser(this);

            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null)
                    titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
            }
            //</Integartion>
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.OvsOrganisationPage1_Init);

        }
        #endregion

        protected void OvsOrganisationPage1_Init(object sender, EventArgs e)
        {
            navigationBar.RootDomainObject = "BnbOrganisation";            
        }
    }
}
