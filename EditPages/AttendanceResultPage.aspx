<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="AttendanceResultPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.AttendanceResultPage" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>AttendancePage</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/bnbattendance24silver.gif"></uc1:title>&nbsp;
				<uc1:NavigationBar id="NavigationBar1" runat="server"></uc1:NavigationBar>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" NewButtonEnabled="False"
					NewWidth="55px" EditAllWidth="55px" SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px" NewText="New"
					EditAllText="Edit All" SaveText="Save" UndoText="Undo"></bnbpagecontrol:bnbdatabuttons>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox2" runat="server" PopupMessage="false" SuppressRecordSavedMessage="false"
					CssClass="messagebox"></bnbpagecontrol:bnbmessagebox>
				<TABLE class="fieldtable" id="Table1" width="100%" border="0">
					<TR>
						<TD class="label" width="50%">Event Reference</TD>
						<TD width="50%"><bnbdatacontrol:bnbdomainobjecthyperlink id="BnbDomainObjectHyperlink1" runat="server" RedirectPage="EventPage.aspx" GuidProperty="BnbAttendance.EventID"
								DataMember="BnbAttendance.Event.EventDescription" HyperlinkText="[BnbDomainObjectHyperlink]" Target="_top"></bnbdatacontrol:bnbdomainobjecthyperlink></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Volunteer Ref</TD>
						<TD width="50%"><bnbdatacontrol:bnbtextbox id="BnbTextBox1" runat="server" CssClass="textbox" Width="250px" Height="20px" DataMember="BnbAttendance.Individual.RefNo"
								MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:bnbtextbox></TD>
					</TR>
					<TR>
						<TD class="label" style="HEIGHT: 25px" width="50%">Name</TD>
						<TD style="HEIGHT: 25px" width="50%"><bnbdatacontrol:bnbtextbox id="BnbTextBox2" runat="server" CssClass="textbox" Width="250px" Height="20px" DataMember="BnbAttendance.Individual.FullName"
								MultiLine="false" Rows="1" StringLength="0"></bnbdatacontrol:bnbtextbox></TD>
					</TR>
					<TR>
						<TD class="label" style="HEIGHT: 25px" width="50%">Status</TD>
						<TD style="HEIGHT: 25px" width="50%">
							<bnbdatacontrol:BnbLookupControl id="BnbLookupControl10" runat="server" CssClass="lookupcontrol" DataMember="BnbAttendance.Application.CurrentStatus.StatusID"
								Height="22px" LookupTableName="vlkuBonoboStatusFull" LookupControlType="ReadOnlyLabel" ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:BnbLookupControl></TD>
					</TR>
					<TR>
						<TD class="label" style="HEIGHT: 25px" width="50%">Status Reason</TD>
						<TD style="HEIGHT: 25px" width="50%">
							<bnbdatacontrol:BnbLookupControl id="BnbLookupControl11" runat="server" CssClass="lookupcontrol" DataMember="BnbAttendance.Application.CurrentStatus.StatusReasonID"
								Height="22px" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel" LookupTableName="lkuStatusReason"></bnbdatacontrol:BnbLookupControl></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Selection Result</TD>
						<TD width="50%">
							<bnbdatacontrol:BnbLookupControl id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" Height="22px" DataMember="BnbAttendance.CurrentAttendanceResult.SelectionResultID"
								DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuSelectionResult"></bnbdatacontrol:BnbLookupControl></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Positive and Realistic Commitment</TD>
						<TD width="50%">
							<bnbdatacontrol:BnbLookupControl id="BnbLookupControl2" runat="server" CssClass="lookupcontrol" Height="22px" DataMember="BnbAttendance.CurrentAttendanceResult.ScorePRC"
								DataTextField="Description" ListBoxRows="4" LookupControlType="RadioButtonList" LookupTableName="lkuDimensionScore"></bnbdatacontrol:BnbLookupControl></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Commitment to learning</TD>
						<TD width="50%">
							<bnbdatacontrol:BnbLookupControl id="BnbLookupControl3" runat="server" CssClass="lookupcontrol" Height="22px" DataMember="BnbAttendance.CurrentAttendanceResult.ScoreCTL"
								DataTextField="Description" ListBoxRows="4" LookupControlType="RadioButtonList" LookupTableName="lkuDimensionScore"></bnbdatacontrol:BnbLookupControl></TD>
					</TR>
					<TR>
						<TD class="label" width="50%" style="HEIGHT: 16px">Practical Problem Solving</TD>
						<TD width="50%" style="HEIGHT: 16px">
							<bnbdatacontrol:BnbLookupControl id="BnbLookupControl4" runat="server" CssClass="lookupcontrol" Height="22px" DataMember="BnbAttendance.CurrentAttendanceResult.ScorePPSA"
								DataTextField="Description" ListBoxRows="4" LookupControlType="RadioButtonList" LookupTableName="lkuDimensionScore"></bnbdatacontrol:BnbLookupControl></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Flexibility</TD>
						<TD width="50%">
							<bnbdatacontrol:BnbLookupControl id="BnbLookupControl5" runat="server" CssClass="lookupcontrol" Height="22px" DataMember="BnbAttendance.CurrentAttendanceResult.ScoreFlex"
								DataTextField="Description" ListBoxRows="4" LookupControlType="RadioButtonList" LookupTableName="lkuDimensionScore"></bnbdatacontrol:BnbLookupControl></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Self Assurance</TD>
						<TD width="50%">
							<bnbdatacontrol:BnbLookupControl id="BnbLookupControl6" runat="server" CssClass="lookupcontrol" Height="22px" DataMember="BnbAttendance.CurrentAttendanceResult.ScoreSA"
								DataTextField="Description" ListBoxRows="4" LookupControlType="RadioButtonList" LookupTableName="lkuDimensionScore"></bnbdatacontrol:BnbLookupControl></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Working with Others</TD>
						<TD width="50%">
							<bnbdatacontrol:BnbLookupControl id="BnbLookupControl7" runat="server" CssClass="lookupcontrol" Height="22px" DataMember="BnbAttendance.CurrentAttendanceResult.ScoreWWO"
								DataTextField="Description" ListBoxRows="4" LookupControlType="RadioButtonList" LookupTableName="lkuDimensionScore"></bnbdatacontrol:BnbLookupControl></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Sensitive to the Needs of Others</TD>
						<TD width="50%">
							<bnbdatacontrol:BnbLookupControl id="BnbLookupControl8" runat="server" CssClass="lookupcontrol" Height="22px" DataMember="BnbAttendance.CurrentAttendanceResult.ScoreSON"
								DataTextField="Description" ListBoxRows="4" LookupControlType="RadioButtonList" LookupTableName="lkuDimensionScore"></bnbdatacontrol:BnbLookupControl></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Specific Technical Skills</TD>
						<TD width="50%">
							<bnbdatacontrol:BnbLookupControl id="BnbLookupControl9" runat="server" CssClass="lookupcontrol" Height="22px" DataMember="BnbAttendance.CurrentAttendanceResult.ScoreSTS"
								DataTextField="Description" ListBoxRows="4" LookupControlType="RadioButtonList" LookupTableName="lkuDimensionScore"></bnbdatacontrol:BnbLookupControl></TD>
					</TR>
					<TR>
						<TD class="label" width="50%">Rejection Codes</TD>
						<TD width="50%">
							<bnbdatagrid:BnbDataGridForDomainObjectList id="uxRejectionCodeGrid" runat="server" CssClass="datagrid" DataMember="BnbAttendance.CurrentAttendanceResult.AttendanceResultCodes"
								SortBy="ID" VisibleOnNewParentRecord="false" AddButtonVisible="true" EditButtonText="Edit" AddButtonText="Add" EditButtonVisible="true"
								DeleteButtonVisible="false" DataGridForDomainObjectListType="Editable" DataProperties="RejectionCodeID[BnbLookupControl:lkuRejectionCode]"
								DataGridType="Editable"></bnbdatagrid:BnbDataGridForDomainObjectList></TD>
					</TR>
				</TABLE>
				<P></P>
				<P>&nbsp;</P>
				<P>&nbsp;</P>
			</div>
		</form>
	</body>
</HTML>
