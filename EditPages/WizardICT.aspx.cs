using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using BonoboWebControls;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for WizardICT.
    /// </summary>
    public partial class WizardICT : System.Web.UI.Page
    {
        protected UserControls.WizardButtons wizardButtons;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            wizardButtons.AddPanel(panelZero);
            wizardButtons.AddPanel(panelOne);
            wizardButtons.AddPanel(panelTwo);
        }

        public Guid ApplicationID
        {
            get
            {
                if (this.Request.QueryString["BnbApplication"] != null)
                    return new Guid(this.Request.QueryString["BnbApplication"]);
                else
                    return Guid.Empty;
            }
        }

        public string SelectedPositionID
        {
            get { return labelHiddenSelectedPositionID.Text; }
            set { labelHiddenSelectedPositionID.Text = value; }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
            wizardButtons.CancelWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
            wizardButtons.FinishWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
            wizardButtons.ShowPanel += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
            wizardButtons.ValidatePanel += new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
            dataGridPlacements.ItemDataBound += new DataGridItemEventHandler(dataGridPlacements_ItemDataBound);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            Response.Redirect("ApplicationPage.aspx?BnbApplication=" + this.ApplicationID);
        }

        private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            DateTime transferDate = DateTime.MinValue;
            try
            {
                transferDate = DateTime.Parse(textTransferDate.Text);
            }
            catch (FormatException)
            {
                labelPanelTwoFeedback.Text = "Transfer date is not in the right format";
                CheckBoxPanelTwo.Visible = false;
                CheckBoxPanelTwo.Checked = false;
                labelPanelTwoCheckBox.Text = "";
            }

            if (transferDate > DateTime.Today)
            {
                labelPanelTwoFeedback.Text = "Transfer date is in the future. When setting a date of transfer in the future, the status of the volunteer immediately changes to Ovs/ICT";
                CheckBoxPanelTwo.Visible = true;
                labelPanelTwoCheckBox.Text = "Ignore warning on next save";
            }

            if (
                ((transferDate > DateTime.Today) && (this.CheckBoxPanelTwo.Checked == true))
                ||
                ((transferDate != DateTime.MinValue) && (transferDate <= DateTime.Today))
                )
            {
                // do the ICT!
                BnbEditManager em = new BnbEditManager();
                BnbApplication volApp = BnbApplication.Retrieve(this.ApplicationID);
                BnbService ictService = new BnbService(true);
                ictService.RegisterForEdit(em);
                BnbPosition newPos = BnbPosition.Retrieve(new Guid(this.SelectedPositionID));
                newPos.RegisterForEdit(em);
                newPos.Services.Add(ictService);
                volApp.Services.Add(ictService);
                // make placed if not already
                if (ictService.CurrentServiceProgress == null ||
                    ictService.CurrentServiceProgress.ServiceStatusID != BnbConst.ServiceStatus_Placed)
                    ictService.AddServiceProgress(BnbConst.ServiceStatus_Placed);

                // set transfer date
                ictService.PlacementStartDate = transferDate;
                // try and save
                try
                {
                    em.SaveChanges();
                    Response.Redirect("ApplicationPage.aspx?BnbApplication=" + this.ApplicationID);
                }
                catch (BnbProblemException pe)
                {
                    labelPanelTwoFeedback.Text = "ICT Wizard was not able to save the ICT due to the following problems:<br/>";
                    foreach (BnbProblem p in pe.Problems)
                        labelPanelTwoFeedback.Text += "-" + p.Message + "<br/>";
                    labelPanelTwoFeedback.Text += "Please press Cancel and go to the other screens to correct these problems.";
                    em.EditCompleted();
                }
            }
        }

        private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
        {
            if (e.CurrentPanel == 0)
            {
                if (labelVolunteerDetails.Text == "")
                    this.SetupPanelZero();
            }
            if (e.CurrentPanel == 1)
            {
                labelPanelOneFeedback.Text = "";
                if (textESDStart.Text == "")
                    this.SetupPanelOne();
            }
            if (e.CurrentPanel == 2)
            {
                this.SetupPanelTwo();
            }

        }

        private void SetupPanelZero()
        {
            BnbApplication volApp = BnbApplication.Retrieve(this.ApplicationID);
            string indvDesc = volApp.Individual.InstanceDescription;
            string statusDesc = BnbEngine.LookupManager.GetLookupItemDescription("vlkuBonoboStatusFull",
                volApp.CurrentStatus.StatusID);
            string appDesc = String.Format("{0}/{1}",
                volApp.ApplicationOrder, volApp.Individual.Applications.Count);
            string volDetails = HttpUtility.HtmlEncode(indvDesc + " " + statusDesc + " " + appDesc);
            labelVolunteerDetails.Text = volDetails;
            labelVolunteerDetails2.Text = volDetails;
            labelVolunteerDetails3.Text = volDetails;

            bool placementFound = false;
            string placeDesc = null;
            if (volApp.PlacedService != null)
            {
                BnbPosition placedPos = volApp.PlacedService.Position;
                placeDesc = HttpUtility.HtmlEncode(placedPos.FullPlacementReference + " " + placedPos.Role.RoleName + " (" + placedPos.Role.Employer.EmployerName + ")");
                labelHiddenCountryID.Text = HttpUtility.HtmlEncode(placedPos.Role.Employer.CountryID.ToString());
                placementFound = true;
            }
            else
            {
                placeDesc = "(No Placement)";
            }
            labelCurrentPlacement.Text = placeDesc;
            labelCurrentPlacement2.Text = placeDesc;
            labelCurrentPlacement3.Text = placeDesc;

            // check status
            if (volApp.CurrentStatus.StatusGroupID == BnbConst.StatusGroup_Overseas)
            {
                bool hasICTPermission = BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_PerformICTPlacements);
                if (hasICTPermission)
                {
                    if (placementFound)
                    {
                        labelPanelZeroContinue.Visible = true;
                    }
                    else
                    {
                        labelPanelZeroFeedback.Text = "Bad data - Volunteer not properly placed to a Placement.";
                    }
                }
                else
                {
                    labelPanelZeroFeedback.Text = "You do not have permission to perform ICTs. Please contact the IT Helpdesk to request the permission.";
                }
            }
            else
            {
                labelPanelZeroFeedback.Text = "The Volunteer must have Overseas status for an ICT to be possible. Please press Cancel.";
            }


        }

        private void SetupPanelOne()
        {
            textESDStart.Text = DateTime.Now.ToString("dd/MMM/yyyy");
            textESDEnd.Text = DateTime.Now.AddMonths(2).ToString("dd/MMM/yyyy");
            Guid countryID = new Guid(labelHiddenCountryID.Text);
            labelCountry.Text = HttpUtility.HtmlEncode(BnbEngine.LookupManager.GetLookupItemDescription("lkuCountry", countryID));
            this.ShowEmptyPlacements();

        }

        private void SetupPanelTwo()
        {
            Guid newPositionID = new Guid(this.SelectedPositionID);
            BnbPosition nextPos = BnbPosition.Retrieve(newPositionID);
            labelNewPlacementDetails.Text = HttpUtility.HtmlEncode(nextPos.FullPlacementReference + " " + nextPos.Role.RoleName + " (" + nextPos.Role.Employer.EmployerName + ")");

            textTransferDate.Text = DateTime.Now.ToString("dd/MMM/yyyy");
            labelPanelTwoFeedback.Text = "";

        }

        private void ShowEmptyPlacements()
        {
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;
            try
            {
                startDate = DateTime.Parse(textESDStart.Text, new System.Globalization.CultureInfo("en-GB"));
            }
            catch (FormatException)
            {
                labelFindFeedback.Text = "Start date not in correct format";
            }
            try
            {
                endDate = DateTime.Parse(textESDEnd.Text, new System.Globalization.CultureInfo("en-GB"));
            }
            catch (FormatException)
            {
                labelFindFeedback.Text = "End date not in correct format";
            }

            if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
            {
                labelFindFeedback.Text = "";
                this.SelectedPositionID = "";
                Guid countryID = new Guid(labelHiddenCountryID.Text);
                BnbDateCondition dateFilter = new BnbDateCondition();
                dateFilter.Fieldname1 = "tblPosition_EarliestStartDate";
                dateFilter.CompareDate1 = startDate;
                dateFilter.CompareDate2 = endDate;
                dateFilter.DateCompareOptions = BnbDateCompareOptions.FieldBetweenDates;
                BnbListCondition countryFilter = new BnbListCondition("tblEmployer_CountryID", countryID);
                BnbListCondition unfilledFilter = new BnbListCondition("tblPosition_FillStatus", BnbConst.PositionFillStatus_Unfilled);
                BnbCriteria emptyPlacementCriteria = new BnbCriteria();
                emptyPlacementCriteria.QueryElements.Add(countryFilter);
                emptyPlacementCriteria.QueryElements.Add(dateFilter);
                emptyPlacementCriteria.QueryElements.Add(unfilledFilter);
                BnbQuery emptyPlacementQuery = new BnbQuery("vwBonoboFindPosition", emptyPlacementCriteria);
                emptyPlacementQuery.MaxRows = 50;
                BnbQueryDataSet emptyPlacementDataSet = emptyPlacementQuery.Execute();

                DataTable placementResults = BnbWebControlServices.GetHtmlEncodedDataTable(emptyPlacementDataSet.Tables["Results"]);

                dataGridPlacements.DataSource = placementResults;
                dataGridPlacements.DataBind();
                if (placementResults.Rows.Count == 0)
                    labelFindNotes.Text = "No unfilled placements found for the specified dates. Please enter a wider date range and press Refresh";
                if (emptyPlacementQuery.MaxRowsExceeded)
                    labelFindNotes.Text = "More than 50 unfilled placements were found, only the first 50 displayed here.";


            }
        }

        private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
        {
            if (e.CurrentPanel == 0)
            {
                e.Proceed = labelPanelZeroContinue.Visible;
            }
            if (e.CurrentPanel == 1)
            {
                string selectedButton = Request.Form["placementGroup"];
                string enteredPlacementRef = txtPlacementRef.Text;
                if (selectedButton == null && enteredPlacementRef == "")
                {
                    labelPanelOneFeedback.Text = "Please select a Placement or enter a Placement Reference";
                    e.Proceed = false;
                }
                else if (selectedButton == null && enteredPlacementRef != "")
                {
                    //validate placement ref
                    string validationmessage = getPlaceRefValidationMessage();
                    if (validationmessage != "")
                    {
                        labelPanelOneFeedback.Text = validationmessage;
                        e.Proceed = false;
                    }
                }
                else
                {
                    labelPanelOneFeedback.Text = "";
                    this.SelectedPositionID = selectedButton;
                }
            }
        }

        protected void buttonRefreshPlacements_Click(object sender, System.EventArgs e)
        {
            this.ShowEmptyPlacements();
        }

        private string GetRadioHTML(DataGridItem item)
        {
            string loopPositionID = item.Cells[1].Text;
            string htmlRadio = "<input type=radio name='placementGroup' value=" + loopPositionID;
            if (loopPositionID == this.SelectedPositionID)
                htmlRadio += " checked ";
            htmlRadio += ">";
            return htmlRadio;

        }

        private void dataGridPlacements_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            Label itemLabel = e.Item.FindControl("labelRadio") as Label;
            if (itemLabel != null)
            {
                itemLabel.Text = this.GetRadioHTML(e.Item);
            }
        }

        private string getPlaceRefValidationMessage()
        {
            BnbQuery qry = new BnbQuery("vwBonoboFindPosition", "Custom_FullPositionReference", txtPlacementRef.Text);
            DataTable qresult = qry.Execute().Tables["Results"];
            if (qresult.Rows.Count == 0) return "The Placement reference entered could not be found";

            BnbPosition pos = BnbPosition.Retrieve(new System.Guid(qresult.Rows[0]["tblPosition_PositionID"].ToString()));
            if (pos == null) return "The Placement referenace entered could not be found";
            bool isnotfilled = (pos.FillStatus == BnbConst.PositionFillStatus_Unfilled);
            if (!isnotfilled)
            {
                return "The Placement reference found [" + pos.ToString() + "] is already filled";
            }

            this.SelectedPositionID = pos.ID.ToString();
            return "";
        }
    }
}

