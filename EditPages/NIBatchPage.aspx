<%@ Page language="c#" Codebehind="NIBatchPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.NIBatchPage" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/batchprocess24silver.gif"></uc1:title>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" UndoText="Undo" SaveText="Save"
					EditAllText="Edit All" NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px"
					NewWidth="55px" NewButtonEnabled="True"></bnbpagecontrol:bnbdatabuttons>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:bnbmessagebox>
				<bnbpagecontrol:bnbtabcontrol id="BnbTabControl1" runat="server"></bnbpagecontrol:bnbtabcontrol>
				<asp:panel id="MainTab" runat="server" Height="282px" CssClass="tcTab" BnbTabName="Main" Width="800px"> <!-- main tab ************************************************************************ -->
					<TABLE class="fieldtable" id="Table1" style="WIDTH: 799px; HEIGHT: 190px" border="0">
						<TR>
							<TD class="label" style="WIDTH: 126px; HEIGHT: 5px" width="126">Created On&nbsp;</TD>
							<TD style="WIDTH: 185px; HEIGHT: 5px" width="185">
								<bnbdatacontrol:BnbDateBox id="BnbDateBox4" runat="server" CssClass="datebox" Width="90px" Height="20px" DataMember="BnbNIBatchHeader.CreatedOn"
									DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbDateBox></TD>
							<TD class="label" style="WIDTH: 138px; HEIGHT: 5px" width="138">Created By
							</TD>
							<TD style="WIDTH: 151px; HEIGHT: 5px" width="151">
								<bnbdatacontrol:bnblookupcontrol id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" Width="168px" Height="19px"
									DataMember="BnbNIBatchHeader.CreatedBy" LookupTableName="vlkuBonoboUser" DataTextField="Description" ListBoxRows="4"
									LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" style="WIDTH: 107px; HEIGHT: 5px" width="107">Benefit Type</TD>
							<TD style="WIDTH: 237px; HEIGHT: 5px" width="237">
								<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol12" runat="server" CssClass="lookupcontrol" Width="144px" Height="19"
									DataMember="BnbNIBatchHeader.BenefitTypeID" LookupTableName="lkuBenefitType" DataTextField="Description" ListBoxRows="4"
									LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 126px; HEIGHT: 19px" width="126">Currency</TD>
							<TD style="WIDTH: 185px; HEIGHT: 19px" width="185">
								<bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol13" runat="server" CssClass="lookupcontrol" Width="129px" Height="19px"
									DataMember="BnbNIBatchHeader.CurrencyID" LookupTableName="lkuCurrency" DataTextField="Description" ListBoxRows="4"
									LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" style="WIDTH: 138px; HEIGHT: 19px" width="138">Financial Year</TD>
							<TD style="WIDTH: 151px; HEIGHT: 19px" width="151">
								<bnbdatacontrol:bnblookupcontrol id="FinancialYearBnblookupcontrol" runat="server" CssClass="lookupcontrol" Width="97px"
									Height="19px" DataMember="BnbNIBatchHeader.FinancialYearID" LookupTableName="lkuFinancialYear" DataTextField="Description"
									ListBoxRows="4" LookupControlType="ComboBox"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" style="WIDTH: 107px; HEIGHT: 19px" width="107">Date Start</TD>
							<TD style="WIDTH: 237px; HEIGHT: 19px" width="237">
								<bnbdatacontrol:bnbdatebox id="Bnbdatebox3" runat="server" CssClass="datebox" Width="91" Height="21" DataMember="BnbNIBatchHeader.DateStart"
									DateCulture="en-GB" DateFormat="dd/mm/yyyy"></bnbdatacontrol:bnbdatebox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 126px; HEIGHT: 16px" width="126">Date End</TD>
							<TD style="WIDTH: 185px; HEIGHT: 16px" width="185">
								<bnbdatacontrol:bnbdatebox id="BnbDateBox2" runat="server" CssClass="datebox" Width="91" Height="21" DataMember="BnbNIBatchHeader.DateEnd"
									DateCulture="en-GB" DateFormat="dd/mm/yyyy"></bnbdatacontrol:bnbdatebox></TD>
							<TD class="label" style="WIDTH: 138px; HEIGHT: 16px" width="138">Value</TD>
							<TD style="WIDTH: 151px; HEIGHT: 16px" width="151">
								<bnbdatacontrol:bnbdecimalbox id="BnbDecimalBox1" runat="server" CssClass="textbox" Width="95px" Height="21px"
									DataMember="BnbNIBatchHeader.Value" FormatExpression="0.0000" TextAlign="Default" DisplayEmptyValue="False"
									DefaultValue="0"></bnbdatacontrol:bnbdecimalbox></TD>
							<TD class="label" style="WIDTH: 107px; HEIGHT: 16px" width="107">Weeks In 
								Year&nbsp;</TD>
							<TD style="HEIGHT: 16px" width="25%">
								<bnbdatacontrol:BnbTextBox id="BnbTextBox2" runat="server" CssClass="textbox" Width="54px" Height="20px" DataMember="BnbNIBatchHeader.WeeksInYear"
									StringLength="0" Rows="1" MultiLine="false" ControlType="Text"></bnbdatacontrol:BnbTextBox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 126px; HEIGHT: 68px" width="126">Cheque Number</TD>
							<TD style="WIDTH: 185px; HEIGHT: 68px" width="185">
								<bnbdatacontrol:BnbTextBox id="ChequeNumberTextBox" runat="server" CssClass="textbox" Width="128px" Height="20px"
									DataMember="BnbNIBatchHeader.ChequeNumber" StringLength="0" Rows="1" MultiLine="false" ControlType="Text"></bnbdatacontrol:BnbTextBox>
								<asp:Label id="ChequeNumberWarningLabel" runat="server" Width="120px" Height="16px" Visible="False"
									ForeColor="Red">Cheque Number must be entered</asp:Label></TD>
							<TD class="label" style="WIDTH: 138px; HEIGHT: 68px" width="138">Comments</TD>
							<TD style="WIDTH: 151px; HEIGHT: 68px" width="151">
								<bnbdatacontrol:bnbtextbox id="Bnbtextbox01" runat="server" CssClass="textbox" Width="160px" Height="32px"
									DataMember="BnbNIBatchHeader.Comments" StringLength="0" Rows="3" MultiLine="True"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" style="WIDTH: 107px; HEIGHT: 68px" width="107">&nbsp;Total Vols</TD>
							<TD style="HEIGHT: 68px" width="25%">
								<bnbdatacontrol:bnbtextbox id="Bnbtextbox4" runat="server" CssClass="textbox" Width="95px" Height="21px" DataMember="BnbNIBatchHeader.TotalVols"
									StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 126px" width="126">Included</TD>
							<TD style="WIDTH: 185px" width="185">
								<bnbdatacontrol:bnbtextbox id="Bnbtextbox5" runat="server" CssClass="textbox" Width="95px" Height="21px" DataMember="BnbNIBatchHeader.IncludedVols"
									StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" style="WIDTH: 138px" width="138">&nbsp;&nbsp;Excluded</TD>
							<TD style="WIDTH: 151px" width="151">
								<bnbdatacontrol:bnbtextbox id="Bnbtextbox6" runat="server" CssClass="textbox" Width="95px" Height="21px" DataMember="BnbNIBatchHeader.ExcludedVols"
									StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:bnbtextbox></TD>
							<TD style="WIDTH: 107px" width="107"></TD>
							<TD width="25%"></TD>
						</TR>
					</TABLE>
					<asp:Panel id="Panel2" runat="server" Width="584px" Height="112px" HorizontalAlign="Center">
						<asp:Panel id="ConditionallyVisibleOnUserOverridePanel" runat="server" Width="463px" Height="112px"
							Visible="False" ForeColor="Blue" BorderWidth="1px" BorderStyle="Solid" BorderColor="Red">
							<P>&nbsp;</P>
							<P>
								<asp:Label id="ConditionalPanelLabel" runat="server">Some ad-hoc changes have been applied to the list. Re-running the batch job will cause those changes to be lost. Are you sure you want to run the batch job?</asp:Label></P>
							<P>&nbsp;
								<asp:Button id="YesContinueButton" runat="server" Width="40px" Text="Yes" onclick="YesContinueButton_Click"></asp:Button>&nbsp;&nbsp;&nbsp;
								<asp:Button id="NoContinueButton" runat="server" Width="40px" Text="No" onclick="NoContinueButton_Click"></asp:Button></P>
						</asp:Panel>
					</asp:Panel>
					<P></P>
					<asp:panel id="ActionButtonsPanel" runat="server" Width="364px" Height="84px">
						<TABLE class="fieldtable" id="Table2" style="WIDTH: 360px; HEIGHT: 82px" border="0">
							<TR>
								<TD style="WIDTH: 137px; HEIGHT: 5px" width="137">
									<P>
										<asp:Button id="RunBatchButton" runat="server" Text="Run Batch" onclick="RunBatchButton_Click"></asp:Button></P>
								</TD>
								<TD style="WIDTH: 82px; HEIGHT: 5px" width="82">
									<asp:Label id="Label1" runat="server">Last Run Date</asp:Label></TD>
								<TD style="WIDTH: 138px; HEIGHT: 5px" width="138">
									<bnbdatacontrol:BnbTextBox id="BnbTextBox3" runat="server" CssClass="textbox" Width="80px" Height="20px" DataMember="BnbNIBatchHeader.BatchRunDate"
										StringLength="0" Rows="1" MultiLine="false" ControlType="Label"></bnbdatacontrol:BnbTextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 137px; HEIGHT: 19px" width="137">
									<asp:Button id="FreezeDataButton" runat="server" Text="Freeze Data" onclick="FreezeDataButton_Click"></asp:Button></TD>
								<TD style="WIDTH: 82px; HEIGHT: 19px" width="82">
									<asp:Label id="Label2" runat="server">Frozen</asp:Label></TD>
								<TD style="WIDTH: 138px; HEIGHT: 19px" width="138">
									<bnbdatacontrol:BnbTextBox id="BnbTextBox7" runat="server" CssClass="textbox" Width="80px" Height="20px" DataMember="BnbNIBatchHeader.FreezeData"
										StringLength="0" Rows="1" MultiLine="false" ControlType="Label"></bnbdatacontrol:BnbTextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 137px; HEIGHT: 16px" width="137">
									<asp:Button id="ApproveDataButton" runat="server" Text="Approve Data" onclick="ApproveDataButton_Click"></asp:Button></TD>
								<TD style="WIDTH: 82px; HEIGHT: 16px" width="82">
									<asp:Label id="Label3" runat="server">Approved</asp:Label></TD>
								<TD style="HEIGHT: 16px" width="25%">
									<bnbdatacontrol:BnbTextBox id="BnbTextBox8" runat="server" CssClass="textbox" Width="80px" Height="20px" DataMember="BnbNIBatchHeader.Approved"
										StringLength="0" Rows="1" MultiLine="false" ControlType="Label"></bnbdatacontrol:BnbTextBox></TD>
							</TR>
						</TABLE>
					</asp:panel>
					<P></P>
					<P>&nbsp;</P>
					<P>&nbsp;</P>
				</asp:panel><asp:panel id="DetailsTab" runat="server" Height="52px" CssClass="tcTab" BnbTabName="Details"
					Width="912px">
					<P>
						<asp:DropDownList id="CountriesDropDownList" runat="server" Width="164px"></asp:DropDownList>&nbsp;&nbsp;
						<asp:LinkButton id="ApplyCountryFilterButton" runat="server" onclick="ApplyCountryFilterButton_Click">Apply Filter</asp:LinkButton></P>
					<P>
						<bnbdatagrid:bnbdatagridforview id="accountItemsBnbDataGridForView" runat="server" CssClass="datagrid" Width="912px"
							ColumnNames="Include, VolRef, FullName, Gender" FieldName="NIBatchHeaderID" QueryStringKey="BnbNIBatchHeader"
							GuidKey="NIBatchDetailID" DomainObject="BnbNIBatchDetail" ViewName="vwNIBatchDetailDescriptive" ViewLinksVisible="true"
							NewLinkVisible="False" NewLinkText="Ne" RedirectPage="NIBatchDetailPage.aspx"></bnbdatagrid:bnbdatagridforview></P>
				</asp:panel>
			</div>
		</form>
	</body>
</HTML>
