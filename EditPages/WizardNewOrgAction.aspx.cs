using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for WizardNewAction.
	/// </summary>
	public partial class WizardNewAction : System.Web.UI.Page
	{
		protected UserControls.WizardButtons wizardButtons;

	
		protected void Page_Load(object sender, System.EventArgs e)
		{

#if DEBUG
			if (!this.IsPostBack && this.Request.QueryString.ToString() == "")
				Response.Redirect("WizardNewOrgAction.aspx?BnbCompany=E4D7C5D1-85A0-46A6-BDCC-E56B00782C73");
#endif

			BnbEngine.SessionManager.ClearObjectCache();

			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
			
			if (!this.IsPostBack)
			{
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				if (this.Request.QueryString["BnbCompany"] != null)
					this.CompanyID = new Guid(this.Request.QueryString["BnbCompany"]);
				else
					throw new ApplicationException("WizardNewOrgAction.aspx expects a BnbCompany parameter in the querystring");

				this.InitWizard();
				BonoboWebControls.BnbWebFormManager.LogAction("New Organisation Action Wizard", null, this);
			}

			wizardButtons.AddPanel(panelZero);
			wizardButtons.AddPanel(panelOne);
		}

		/// <summary>
		/// stores the companyID that the action is being added to
		/// </summary>
		public Guid CompanyID
		{
			get{return new Guid(lblHiddenCompanyID.Text);}
			set{lblHiddenCompanyID.Text = value.ToString();}
		}

		/// <summary>
		/// The existing individual, selected by the user 
		/// </summary>
		public Guid IndividualID
		{
			get{return new Guid(lblHiddenIndividualID.Text);}
			set{lblHiddenIndividualID.Text = value.ToString();}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			wizardButtons.CancelWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
			wizardButtons.FinishWizard += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
			wizardButtons.ValidatePanel += new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
			wizardButtons.ShowPanel += new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			this.ReturnToCompanyPage();
		}

		private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			this.SaveAction();
		}

		private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
		{
			if (e.CurrentPanel == 0)
			{
				if ((cboPersonnel.SelectedValue)!= "")
					e.Proceed = true;
				else
				{
					e.Proceed = false;
					lblPanelZeroFeedback.Text = "You must create personnel before adding an action";
				}
			}	
		}

		private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			if (e.CurrentPanel == 0)
				lblPanelZeroFeedback.Text = "";

			if (e.CurrentPanel == 1)
				SetupPanelOne();

		}

		private void ReturnToCompanyPage()
		{
			Response.Redirect("OrganisationPage.aspx?BnbCompany=" + this.CompanyID.ToString());
		}

		private void InitWizard()
		{
			// show company name 
			BnbCompany useCompany = BnbCompany.Retrieve(this.CompanyID);
			lblCompanyName.Text = useCompany.CompanyName;
			lblCompanyName2.Text = useCompany.CompanyName;

			// show live personnel on panel one
			foreach (BnbCompanyAddress caLoop in useCompany.CompanyAddresses)
			{
				if (caLoop.Address != null)
				{
					foreach (BnbIndividualAddress indLink in caLoop.Address.IndividualAddresses)
					{
						if (indLink.IsLive && indLink.Individual != null)
						{
							BnbIndividual indivLoop = indLink.Individual;
							string desc = indivLoop.RefNo + " " + indivLoop.FullName;
							if (indLink.CompanyRole != null && indLink.CompanyRole != "")
								desc = desc + " (" + indLink.CompanyRole + ")";
							ListItem newItem = new ListItem(desc, indivLoop.ID.ToString());
							cboPersonnel.Items.Add(newItem);

						}
					}
				}
			}



			
		}

		private void PopulateCombo(string lookupName, string filter, DropDownList combo)
		{
			combo.Items.Clear();
			BnbLookupDataTable lookup = BnbEngine.LookupManager.GetLookup(lookupName);
			DataView lookupView = new DataView(lookup);
			lookupView.RowFilter = filter;
			lookupView.Sort = "Description";
			combo.DataSource = lookupView;
			if (lookup.KeyType == LookupTableKeyType.GuidKey)
				combo.DataValueField = "GuidID";
			else
				combo.DataValueField = "IntID";

			combo.DataTextField = "Description";
			combo.DataBind();
		}

		private void SetupPanelOne()
		{
			lblPanelOneFeedback.Text = "";
				this.IndividualID = new Guid(cboPersonnel.SelectedValue);
				lblChosenPersonnel.Text = cboPersonnel.SelectedItem.Text;
				lblChosenPersonnel.Text = cboPersonnel.SelectedItem.Text;
				lblActionType.Text = BnbEngine.LookupManager.GetLookupItemDescription("lkuContactType",int.Parse(cboActionType.SelectedValue.ToString()));
				this.PopulateCombo("lkuContactDescription","Exclude = 0 and ContactTypeID = " + cboActionType.SelectedValue, cboVSODescription);
				cboVSODescription.Items.Add(new ListItem("","0"));
				cboVSODescription.Items[cboVSODescription.Items.Count-1].Selected = true;
			
		}

		private void SaveAction()
		{
			BnbEditManager em = new BnbEditManager();
			BnbContactDetail newAction = new BnbContactDetail(true);
			newAction.RegisterForEdit(em);
			BnbIndividual useIndiv = BnbIndividual.Retrieve(this.IndividualID);
			useIndiv.ContactDetails.Add(newAction);
			newAction.ContactDescriptionID = int.Parse(cboVSODescription.SelectedValue);
			newAction.ContactStatusID = int.Parse(cboStatus.SelectedValue.ToString());
			newAction.Information = txtInformation.Text;
			newAction.SentViaID = int.Parse(cboSentVia.SelectedValue.ToString());
			newAction.DateSent = DateTime.Now;
			newAction.SentByID = BnbEngine.SessionManager.GetSessionInfo().UserID;
			newAction.MailingReference = txtReference.Text;

			try
			{
				em.SaveChanges();
				this.ReturnToCompanyPage();
			}
			catch(BnbProblemException pe)
			{
				lblPanelOneFeedback.Text = "Unfortunately, the Action record could not be created due to the following problems: <br/>";
				foreach(BnbProblem p in pe.Problems)
					lblPanelOneFeedback.Text += p.Message + "<br/>";

			}


		}
	
	}
}
