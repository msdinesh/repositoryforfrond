using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using BonoboWebControls.DataGrids;
using BonoboWebControls;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for FundChargeRateList.
	/// </summary>
	public partial class FundChargeRateList : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			BnbWorkareaManager.SetPageStyleOfUser(this);
			// Put user code to initialize the page here
			if (!this.IsPostBack)
			{
				BnbWorkareaManager.CheckPageAccess(this);
				titleBar.TitleText = "Fund Charge Rate Admin";
				ShowFundChargeRates();
				if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(
					BnbConst.Permission_ProgrammeFundingAdmin))
				{
					dgrFundChargeRate.ViewLinksVisible = true;
					hlkNewFundChargeRate.Visible = true;
				}
				else
				{
					dgrFundChargeRate.ViewLinksVisible = false;
					hlkNewFundChargeRate.Visible = false;
					lblNoPermission.Text = "(You do not have permission to edit Fund Charge Rates)";
				}

				BnbWebFormManager.LogAction(titleBar.TitleText, null, this);
				

			}
			//BnbDataGridColumn col = dgrFundChargeRate.Columns[5];
			//col.FieldUserAlias = "VSO Type";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void ShowFundChargeRates()
		{
			// blank criteria
			BnbCriteria showAll = new BnbCriteria();
			// feed it to datagrid
			dgrFundChargeRate.Criteria = showAll;
			dgrFundChargeRate.Visible = true;
			dgrFundChargeRate.DataBind();


		}
	}
}
