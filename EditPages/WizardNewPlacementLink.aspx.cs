using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using BonoboEngine.Query;
using BonoboEngine; 
using BonoboDomainObjects;
using System.Text;


namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for WizardNewPlacementLink.
	/// </summary>
	public partial class WizardNewPlacementLink : System.Web.UI.Page
	{
		protected BonoboWebControls.DataControls.BnbStandaloneDecimalBox BnbStandaloneDecimalBox1;
		protected BonoboWebControls.DataControls.BnbStandaloneDecimalBox BnbStandaloneDecimalBox2;
		protected BonoboWebControls.DataControls.BnbStandaloneDecimalBox BnbStandaloneDecimalBox3;
		protected UserControls.WizardButtons wizardButtons;
		
		


		/// <summary>
		/// Returns the ApplicationId the passed in the query string
		/// </summary>
		public Guid ApplicationID
		{
			get
			{
				if (lblHiddenApplicationID.Text == "")
					return Guid.Empty;
				else
					return new Guid(lblHiddenApplicationID.Text );
			}
			set{lblHiddenApplicationID.Text = value.ToString();}
		}


		/// <summary>
		/// Returns the PositionId the placement is attached to
		/// may also return Guid.Empty if no placement is selected.
		/// </summary>
		public Guid PositionID
		{
			get
			{
				if (lblHiddenPositionID.Text == "")
					return Guid.Empty;
				else
					return new Guid(lblHiddenPositionID.Text );
			}
			set{lblHiddenPositionID.Text = value.ToString();}
		}
		
		protected void Page_Load(object sender, System.EventArgs e)
		{
			
			
			
			
#if DEBUG
			if(Request.QueryString.ToString() == "")
				this.ApplicationID =new Guid("1E17E6CA-EE11-4B2A-9E00-AEE2BF400774");
#endif
									
			if(this.Request.QueryString["BnbApplication"]!=null) 
			{
				Guid id = new Guid(this.Request.QueryString["BnbApplication"]);
				this.ApplicationID =id;
			}
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
		
			BnbEngine.SessionManager.ClearObjectCache();
			wizardButtons.AddPanel(panelZero);
			wizardButtons.AddPanel(panelOne);
			
			if (!this.IsPostBack)
			{
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				if (this.Request.UrlReferrer != null)
					uxHiddenReferrer.Text = this.Request.UrlReferrer.ToString();
				else
					uxHiddenReferrer.Text = "~/Menu.aspx";

				initWizard();
				BonoboWebControls.BnbWebFormManager.LogAction("New Placement Link Wizard", null, this);
			}
		}

		public void initWizard()
		{
			SetupPanelZero();
			uxIgnoreWarningsCheckbox.Visible = false;
		}
		
		public void SetupPanelZero()
		{
			BnbApplication bnbApp = BnbApplication.Retrieve(this.ApplicationID);
			uxVolSummary.Text = bnbApp.InstanceDescription;
			uxPSDStart.Date = DateTime.Today;
				
		}

		public void SetupPanelOne()
		{
			if(bdgPlacement.SelectedDataRow!=null)
			{
				System.Guid positionID = bdgPlacement.SelectedDataRowID;
				this.PositionID  = positionID; 
				BnbApplication bnbApp = BnbApplication.Retrieve(this.ApplicationID);
				lblVolName1.Text = bnbApp.Individual.FullName;  
				lblVolRef1.Text =  bnbApp.Individual.Additional.ReferenceNo; 
				BnbLookupDataTable status = BnbEngine.LookupManager.GetLookup("lkuStatus"); 
				lblCurrentStatus1.Text = status.FindByID(bnbApp.CurrentStatus.StatusID).Description;
				// default in status combo depends on whether vol is placed yet
				if (bnbApp.PlacedService == null)
					uxServiceStatusLookup.SelectedValue = BnbConst.ServiceStatus_Considered;
				else
					uxServiceStatusLookup.SelectedValue = BnbConst.ServiceStatus_Placed;

				BnbPosition bnbPos = BnbPosition.Retrieve(this.PositionID);
				lblPlacementRef.Text =bnbPos.FullPlacementReference; 
				lblJobtitle.Text =bnbPos.Role.RoleAbbrev;  
				lblEmployer.Text =bnbPos.Role.Employer.EmployerName;
			}
		}

		private void CreateService()
		{
			lblPanelTwoFeedback.Text = "";
			lblViewWarnings.Text = "";
			lblIgnoreWarnings.Text = "";
			uxIgnoreWarningsCheckbox.Visible = false;
			try
			{
				BnbApplication obApp = BnbApplication.Retrieve(this.ApplicationID);
				BnbEditManager em = new BnbEditManager();
				BnbService newServ = new BnbService(true);
				newServ.RegisterForEdit(em);
				obApp.RegisterForEdit(em);
				obApp.Services.Add(newServ);
				//remove the automatically created Service Progress VAM-156
				if (newServ.ServiceProgress.Count > 0)
				{
					newServ.ServiceProgress.RemoveAndDelete(newServ.ServiceProgress[0]);
				}

				newServ.AddServiceProgress((int)uxServiceStatusLookup.SelectedValue);
				BnbPosition obPos = BnbPosition.Retrieve(this.PositionID);
				obPos.Services.Add(newServ);

				BnbWarningCollection warns = em.GetEditWarnings();

				if(warns.Count == 0 || uxIgnoreWarningsCheckbox.Checked == true)
				{
					em.SaveChanges();
					Response.Redirect("./ApplicationPage.aspx?BnbApplication=" + this.ApplicationID.ToString());
				}
				else
				{
					StringBuilder sb = new StringBuilder();
					sb.Append("Please check the following warnings:<br/>");
					foreach(BnbWarning warnLoop in warns)
					{
						sb.Append(warnLoop.Message);
						sb.Append("<br/>");
					}
					uxIgnoreWarningsCheckbox.Visible = true;
					lblViewWarnings.Text = sb.ToString();
					lblIgnoreWarnings.Text = "Ignore Warnings on next save <br>";
				}
			}
			catch(BnbProblemException pe)
			{
				lblPanelTwoFeedback.Text = lblPanelTwoFeedback.Text + "Unfortunately, the record could not be created due to the following problems: <br/>";
				foreach(BnbProblem p in pe.Problems)
					lblPanelTwoFeedback.Text += p.Message + "<br/>";

			}
			catch(BnbLockException le)
			{
				lblPanelTwoFeedback.Text = le.Message;
			}
		}

		private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
		{
			if (e.CurrentPanel == 0)
			{
				if (bdgPlacement.SelectedDataRowID == Guid.Empty)
				{
					lblPanelZeroFeedback.Text = "Please use the Find and select a Placement Row before pressing Next";
					e.Proceed = false;
				}
			}
		
			if (e.CurrentPanel == 1)
			{

			}
		}

		private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			if (e.CurrentPanel == 0)
			{
				this.SetupPanelZero();
			}
			
			if (e.CurrentPanel == 1)
			{
				this.SetupPanelOne();
			}
		}

		private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			Response.Redirect(uxHiddenReferrer.Text);
		}

		private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			CreateService();
		}

		protected void btnFindPlacement_Click(object sender, System.EventArgs e)
		{
			FindPlacement();
		}

		private void FindPlacement()
		{
			lblPanelZeroFeedback.Text = "";
			// build criteria
			BnbCriteria findPlacement = new BnbCriteria();
			
			if (txtFindPlacementRefNo.Text != "")
				findPlacement.QueryElements.Add(new BnbTextCondition("Custom_FullPositionReference", 
					txtFindPlacementRefNo.Text, BnbTextCompareOptions.StartOfField));

			if (txtFindJobTitle.Text != "")
				findPlacement.QueryElements.Add(new BnbTextCondition("tblRole_RoleName", 
					txtFindJobTitle.Text, BnbTextCompareOptions.StartOfField));

			if (txtFindEmployer.Text != "")
				findPlacement.QueryElements.Add(new BnbTextCondition("tblEmployer_EmployerName", 
					txtFindEmployer.Text, BnbTextCompareOptions.StartOfField));

			if (!uxPSDStart.IsEmpty)
				findPlacement.QueryElements.Add(new BnbDateCondition("tblPosition_EarliestStartDate", 
					uxPSDStart.Date, BnbDateCompareOptions.FieldFromDate));
			
			if (!uxPSDEnd.IsEmpty)
				findPlacement.QueryElements.Add(new BnbDateCondition("tblPosition_EarliestStartDate", 
					uxPSDEnd.Date, BnbDateCompareOptions.FieldToDate));
			
			// country might not be specified
			if (BnbLookupControl1.SelectedValue.ToString()!= "00000000-0000-0000-0000-000000000000")
				findPlacement.QueryElements.Add(new BnbListCondition("tblEmployer_CountryID", 
					new Guid(BnbLookupControl1.SelectedValue.ToString()  )));	
			
			// so far, if we dont have any criteria, then reject
			if (findPlacement.QueryElements.Count == 0)
			{
				lblPanelZeroFeedback.Text = "Please enter some criteria to search on before pressing Find";
				return;
			}

			if (chkUnFilled.Checked ==true  )
			{
				findPlacement.QueryElements.Add(new BnbBooleanCondition("Custom_Unfilled",true));
				BnbListCondition notCancelled = new BnbListCondition("tblPosition_FillStatus", BnbConst.PositionFillStatus_Cancelled);
				notCancelled.LogicalOperator = BnbLogicalOperatorType.AndNot;
				findPlacement.QueryElements.Add(notCancelled);
			}
				
			int maxRows = int.Parse(uxMaxResultsDropdown.SelectedValue);
			
			// supply criteria to grid
			bdgPlacement.Criteria = findPlacement;
			bdgPlacement.MaxRows = maxRows;
			bdgPlacement.Visible = true;
			bdgPlacement.DataBind();
					
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			wizardButtons.ShowPanel +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
			wizardButtons.ValidatePanel +=new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
			wizardButtons.CancelWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
			wizardButtons.FinishWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		
	}
}
