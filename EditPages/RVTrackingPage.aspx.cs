using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboDomainObjects;
using BonoboWebControls;
using BonoboEngine;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for RVTrackingPage.
	/// </summary>
	public partial class RVTrackingPage : System.Web.UI.Page
	{
		protected BonoboWebControls.DataControls.BnbLookupControl BnbLookupControl3;
		protected UserControls.Title titleBar;

		private BnbWebFormManager bnb = null;
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			titleBar.TitleText = "RV Tracking";
			BnbButton1.Visible = false;
#if DEBUG
			if(Request.QueryString.ToString() == "")
				bnb = new BnbWebFormManager("BnbRVTracking=1B544CD6-A87D-413C-98A3-0CD3649E0BAD");
			else
				bnb = new BnbWebFormManager(this,"BnbRVTracking");
#endif
			// re-direct to wizard if new mode
			if (this.Request.QueryString["BnbRVTracking"] != null &&
				this.Request.QueryString["BnbRVTracking"].ToLower() == "new")
			{
				doRedirectToRVTrackingWizard();
			}
			bnb = new BnbWebFormManager(this,"BnbRVTracking");
			bnb.LogPageHistory = true;
			BnbWorkareaManager.SetPageStyleOfUser(this);
						
			if(this.Request.QueryString["BnbRVTracking"]!=null) 
			{
				Guid id = new Guid(this.Request.QueryString["BnbRVTracking"]);
				BnbRVTracking rv = BnbRVTracking.Retrieve(id);
				if(bnb.InEditMode ==false)

				{
					if(rv.RVRecordStatusID ==BnbConst.RVRecordStatus_Ready)
						BnbButton1.Visible = true;
					
					if(rv.IndividualID !=Guid.Empty)
					{
						BnbHyperlink3.NavigateUrl = "IndividualPage.aspx?BnbIndividual=" + rv.IndividualID.ToString();
						BnbHyperlink3.Text ="View the Original Individual Record";
						BnbHyperlink3.Visible =true;
						if(rv.ApplicationID !=Guid.Empty)
						{
							BnbHyperlink2.NavigateUrl = "ApplicationPage.aspx?BnbIndividual="+rv.IndividualID.ToString()+"&BnbApplication=" + rv.ApplicationID.ToString();
							BnbHyperlink2.Text ="View the Original Application Record";
							BnbHyperlink2.Visible =true;
						}
					}

				}
			}

			

		}

		private void doRedirectToRVTrackingWizard()
		{
			if (this.Request.QueryString["BnbRVTracking"] != null &&
				this.Request.QueryString["BnbRVTracking"].ToLower() == "new")
				Response.Redirect("WizardNewRVTracking.aspx?BnbRVTracking=new");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void BnbButton1_Click(object sender, System.EventArgs e)
		{
			if(this.Request.QueryString["BnbRVTracking"]!=null) 
			{
				Guid[] idArray = new Guid[1];
				idArray[0] = new Guid(this.Request.QueryString["BnbRVTracking"]);
				BnbRVConvertLog newRVCL = new BnbRVConvertLog(true);
				BnbEditManager em = new BnbEditManager();
                newRVCL.RegisterForEdit(em);
				newRVCL.RunConversion(idArray,false);
				em.SaveChanges();
				BnbWebFormManager.LogAction("RV Tracking (RV Conversion Run)",
					String.Format("{0} records transferred, {1} records failed.",
					newRVCL.RecordsTransferred.ToString(),
					newRVCL.RecordsFailed.ToString()), this);
			}	
		}
	}
}
