<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Page language="c#" Codebehind="FundCodeList.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.FundCodeList" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>FundingPage</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server"></uc1:title>
				<P>
					<asp:HyperLink id="hlkNewFundCode" runat="server" NavigateUrl="FundCodePage.aspx?BnbFundCode=new&amp;PageState=New">New Fund Code</asp:HyperLink>
					<asp:Label id="lblNoPermission" runat="server"></asp:Label></P>
				<P>
					<bnbdatagrid:BnbDataGridForView id="dgrFundCode" runat="server" CssClass="datagrid" Width="100%" ViewLinksText="View"
						NewLinkText="New" NewLinkVisible="False" ViewLinksVisible="true" ShowResultsOnNewButtonClick="false" DisplayResultCount="false"
						DisplayNoResultMessageOnly="false" ViewName="vwBonoboFundCodeList" ColumnNames="tblFundCode_FundCode,tblFundCode_FundCodeDescription"
						DomainObject="BnbFundCode" GuidKey="tblFundCode_FundCodeID" RedirectPage="FundCodePage.aspx" Visible="False"></bnbdatagrid:BnbDataGridForView></P>
			</div>
		</form>
	</body>
</HTML>
