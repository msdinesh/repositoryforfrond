using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for AvailabilityPage.
    /// </summary>
    public partial class AvailabilityPage : System.Web.UI.Page
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
#if DEBUG
            BnbWebFormManager bnb = null;
            if (Request.ServerVariables["QUERY_STRING"] == "")
            {
                BnbWebFormManager.ReloadPage("BnbIndividual=50E4B11B-5376-11D6-8B80-00805F5D8E16&BnbAvailability=F868CAF2-7EDB-11D6-8B9F-00805F5D8E16");
            }
            else
            {
                bnb = new BnbWebFormManager(this, "BnbIndividual");
            }
#else
			BnbWebFormManager bnb = new BnbWebFormManager(this,"BnbIndividual");
#endif

            // turn on logging for this page
            bnb.LogPageHistory = true;
            //<Integartion>
            bnb.PageNameOverride = "Availability";
            BnbWorkareaManager.SetPageStyleOfUser(this);

            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null)
                    titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
            }
            //</Integartion>
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
