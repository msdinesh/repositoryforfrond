<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="SFEventAttendeesGrid" Src="../UserControls/SFEventAttendeesGrid.ascx" %>
<%@ Page language="c#" Codebehind="EventPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.EventPage" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="SFEventPersonnelGrid" Src="../UserControls/SFEventPersonnelGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>EventPage</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/bnbevent24silver.gif"></uc1:title>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" UndoText="Undo" SaveText="Save"
					EditAllText="Edit All" NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px"
					NewWidth="55px" NewButtonEnabled="True"></bnbpagecontrol:bnbdatabuttons>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox2" runat="server" CssClass="messagebox" SuppressRecordSavedMessage="false"
					PopupMessage="false"></bnbpagecontrol:bnbmessagebox>
				<bnbpagecontrol:bnbtabcontrol id="BnbTabControl1" runat="server"></bnbpagecontrol:bnbtabcontrol>
				<asp:panel id="MainTab" runat="server" CssClass="tcTab" BnbTabName="Main">
					<TABLE class="fieldtable" id="Table1" width="100%" border="0">
						<TR>
							<TD class="label" style="HEIGHT: 33px" width="25%">Event Type</TD>
							<TD style="HEIGHT: 33px" width="25%">
								<P>
									<bnbdatacontrol:bnblookupcontrol id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" AutoPostBack="True"
										LookupTableName="lkuEventGroup" DataMember="BnbEvent.EventGroupID" DataTextField="Description" ListBoxRows="4"
										LookupControlType="ReadOnlyLabel" Height="22px" Width="250px"></bnbdatacontrol:bnblookupcontrol>
									<bnbdatacontrol:bnblookupcontrol id="BnbLookupControl6" runat="server" CssClass="lookupcontrol" LookupTableName="lkuEventType"
										DataMember="BnbEvent.EventTypeID" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
										Height="22px" Width="250px" ColumnRelatedToParent="EventGroupID" ParentControlID="BnbLookupControl1"></bnbdatacontrol:bnblookupcontrol></P>
							</TD>
							<TD class="label" style="HEIGHT: 33px" width="25%">Reference</TD>
							<TD style="HEIGHT: 33px" width="25%">
								<bnbdatacontrol:bnbtextbox id="BnbTextBox2" runat="server" CssClass="textbox" DataMember="BnbEvent.EventDescription"
									Height="20px" Width="250px" StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Event Country</TD>
							<TD width="25%">
								<bnbdatacontrol:bnblookupcontrol id="BnbLookupControl2" runat="server" CssClass="lookupcontrol" LookupTableName="lkuCountry"
									DataMember="BnbEvent.CountryID" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
									Height="22px" Width="250px"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" width="25%">Site</TD>
							<TD width="25%">
								<bnbdatacontrol:bnblookupcontrol id="BnbLookupControl3" runat="server" CssClass="lookupcontrol" LookupTableName="vlkuBonoboEventSite"
									DataMember="BnbEvent.EventSiteID" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
									Height="22px" Width="250px"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Location</TD>
							<TD width="25%">
								<bnbdatacontrol:bnblookupcontrol id="BnbLookupControl4" runat="server" CssClass="lookupcontrol" LookupTableName="lkuEventLocation"
									DataMember="BnbEvent.EventLocationID" DataTextField="Description" ListBoxRows="4" LookupControlType="ReadOnlyLabel"
									Height="22px" Width="250px"></bnbdatacontrol:bnblookupcontrol></TD>
							<TD class="label" width="25%">Other Location</TD>
							<TD width="25%">
								<bnbdatacontrol:bnbtextbox id="BnbTextBox3" runat="server" CssClass="textbox" DataMember="BnbEvent.OtherLocation"
									Height="20px" Width="250px" StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Date From</TD>
							<TD width="25%">
								<bnbdatacontrol:bnbdatebox id="BnbDateBox1" runat="server" CssClass="datebox" DataMember="BnbEvent.EventStart"
									Height="20px" Width="90px" DateCulture="en-GB" DateFormat="dd/mm/yyyy"></bnbdatacontrol:bnbdatebox></TD>
							<TD class="label" width="25%">Date To</TD>
							<TD width="25%">
								<bnbdatacontrol:bnbdatebox id="BnbDateBox2" runat="server" CssClass="datebox" DataMember="BnbEvent.EventEnd"
									Height="20px" Width="90px" DateCulture="en-GB" DateFormat="dd/mm/yyyy"></bnbdatacontrol:bnbdatebox></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Time From
							</TD>
							<TD width="25%">
								<bnbdatacontrol:bnbtextbox id="BnbTextBox1" runat="server" CssClass="textbox" DataMember="BnbEvent.StartTime"
									Height="20px" Width="88px" StringLength="0" Rows="1" MultiLine="false" ControlType="Text" DataType="String"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" width="25%">Time To</TD>
							<TD width="25%">
								<bnbdatacontrol:bnbtextbox id="BnbTextBox8" runat="server" CssClass="textbox" DataMember="BnbEvent.EndTime"
									Height="20px" Width="88px" StringLength="0" Rows="1" MultiLine="false" ControlType="Text" DataType="String"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Max Attendees</TD>
							<TD width="25%">
								<bnbdatacontrol:BnbDecimalBox id="BnbDecimalBox1" runat="server" CssClass="textbox" DataMember="BnbEvent.MaxAttendance"
									Height="20px" Width="48px" TextAlign="left" FormatExpression="#,0"></bnbdatacontrol:BnbDecimalBox></TD>
							<TD class="label" width="25%">Event Status</TD>
							<TD width="25%">
								<bnbdatacontrol:bnblookupcontrol id="BnbLookupControl5" runat="server" CssClass="lookupcontrol" LookupTableName="lkuEventStatus"
									DataMember="BnbEvent.EventStatusID" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
									Height="22px" Width="250px"></bnbdatacontrol:bnblookupcontrol></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Current Attendees</TD>
							<TD width="25%">
								<P>
									<bnbdatacontrol:bnbtextbox id="BnbTextBox5" runat="server" CssClass="textbox" DataMember="BnbEvent.CurrentAttendees"
										Height="20px" Width="48px" StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:bnbtextbox></P>
							</TD>
							<TD class="label" width="25%">Cancelled Date</TD>
							<TD width="25%">
								<bnbdatacontrol:bnbdatebox id="BnbDateBox5" runat="server" CssClass="datebox" DataMember="BnbEvent.EventCancelDate"
									Height="20px" Width="90px" DateCulture="en-GB" DateFormat="dd/mm/yyyy"></bnbdatacontrol:bnbdatebox></TD>
						</TR>
						<TR>
							<TD class="label" width="25%">Comments</TD>
							<TD width="25%">
								<bnbdatacontrol:bnbtextbox id="BnbTextBox7" runat="server" CssClass="textbox" DataMember="BnbEvent.Comments"
									Height="20px" Width="250px" StringLength="0" Rows="3" MultiLine="True"></bnbdatacontrol:bnbtextbox></TD>
							<TD class="label" width="25%">Cancelled Reason</TD>
							<TD width="25%">
								<bnbdatacontrol:bnbtextbox id="BnbTextBox6" runat="server" CssClass="textbox" DataMember="BnbEvent.EventCancelReason"
									Height="20px" Width="250px" StringLength="0" Rows="3" MultiLine="True"></bnbdatacontrol:bnbtextbox></TD>
						</TR>
					</TABLE>
				</asp:panel><asp:panel id="AttendeesTab" runat="server" CssClass="tcTab" BnbTabName="Attendees">
					<P>
						<bnbgenericcontrols:BnbHyperlink id="uxNewAttendanceHyper" runat="server">New Attendee Wizard</bnbgenericcontrols:BnbHyperlink></P>
					<uc1:sfeventattendeesgrid id="SFEventAttendeesGrid1" runat="server"></uc1:sfeventattendeesgrid>
				</asp:panel><asp:panel id="EventPersonnelTab" runat="server" CssClass="tcTab" BnbTabName="Event Personnel">
					<P>
						<bnbgenericcontrols:BnbHyperlink id="uxNewEventPersonnelHyper" runat="server">New Event Personnel Wizard</bnbgenericcontrols:BnbHyperlink></P>
					<P>
						<uc1:sfeventpersonnelgrid id="SFEventPersonnelGrid1" runat="server"></uc1:sfeventpersonnelgrid></P>
				</asp:panel>
				<asp:panel id="AttendeesResultsTab" runat="server" CssClass="tcTab" BnbTabName="Attendee Results">
					<asp:panel id="uxResultsGridPanel" runat="server">
						<P>
							<bnbdatagrid:BnbDataGridForView id="BnbDataGridForView1" runat="server" CssClass="datagrid" Width="70%" MaxRows="150"
								QueryStringKey="BnbEvent" FieldName="tblAttendance_EventID" ViewName="vwBonoboEventAttendeesResults" DisplayNoResultMessageOnly="false"
								DisplayResultCount="False" ShowResultsOnNewButtonClick="false" ViewLinksVisible="True" NewLinkVisible="False"
								NewLinkText="New" ViewLinksText="View" DataGridType="ViewOnly" RedirectPage="AttendanceResultPage.aspx" DomainObject="BnbAttendance"
								GuidKey="tblAttendance_AttendanceID"></bnbdatagrid:BnbDataGridForView></P>
					</asp:panel>
					<asp:Label id="uxNoAttendeeResultsLabel" runat="server">
						<p>(Attendee Results are for certain Volunteer Selection events only)</p>
					</asp:Label>
				</asp:panel>
			</div>
		</form>
		<P></P>
		</TD></TR></TBODY></TABLE>
	</body>
</HTML>
