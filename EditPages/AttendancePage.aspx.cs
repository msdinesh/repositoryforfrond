using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using BonoboWebControls;
using BonoboDomainObjects;


namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for AttendancePage.
	/// </summary>
	public partial class AttendancePage : System.Web.UI.Page
	{
		protected BonoboWebControls.PageControls.BnbDataButtons Bnbdatabuttons2;
		protected BonoboWebControls.PageControls.BnbSectionLinks BnbSectionLinks1;
		protected BonoboWebControls.PageControls.BnbMessageBox BnbMessageBox1;
		protected BonoboWebControls.PageControls.BnbSectionHeading Bnbsectionheading2;
		protected Frond.UserControls.NavigationBar NavigationBar1;

		protected UserControls.Title titleBar;


		protected void Page_Load(object sender, System.EventArgs e)
		{

			// if new, re-direct to wizard
			if (BnbWebFormManager.IsPageDomainObjectNew("BnbAttendance"))
			{
				// this must have come from individual page - get individual ID
				string indvIDasString = Request.QueryString["BnbIndividual"];
				Response.Redirect(String.Format("WizardNewAttendance.aspx?BnbIndividual={0}",
					indvIDasString));
			}

#if DEBUG	
			BnbWebFormManager bnb = null;
			if (Request.QueryString.ToString() == "")
				BnbWebFormManager.ReloadPage("BnbEvent=046d6877-9dff-4ac5-8212-b082b185e01c&BnbAttendance=2F13E3BE-6F7D-443C-9F39-F61EFB3E27DB");
			else
				bnb = new BnbWebFormManager(this,"BnbAttendance");
#else	
			BnbWebFormManager bnb = new BnbWebFormManager(this,"BnbAttendance");
#endif
			bnb.LogPageHistory = true;
			BnbWorkareaManager.SetPageStyleOfUser(this);
			// tweak page title settings
			bnb.PageNameOverride = "Attendance";
			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
			}

			BnbAttendance attendance = bnb.GetPageDomainObject("BnbAttendance",true) as BnbAttendance;
			string indv = attendance.IndividualID.ToString();
			NavigationBar1.RootDomainObject = "BnbIndividual";
			NavigationBar1.RootDomainObjectID = indv;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
