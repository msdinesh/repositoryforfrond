<%@ Page Language="c#" Codebehind="ProjectPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.ProjectPage" %>

<%@ Register Src="../UserControls/PMFStatusPanel.ascx" TagName="PMFStatusPanel" TagPrefix="uc2" %>

<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.ServicesControls"
    TagPrefix="cc1" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="SFEventAttendeesGrid" Src="../UserControls/SFEventAttendeesGrid.ascx" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="SFEventPersonnelGrid" Src="../UserControls/SFEventPersonnelGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbdmssearchcontrol" Namespace="BonoboWebControls.DMSControls" Assembly="BonoboWebControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Project Page</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<link  rel="stylesheet" href ="../Css/calendar.css" type="text/css"/>
		
		<script type="text/javascript" language="javascript">	
		
		function showHidePriority()
		{
		var chkBoxPriority=document.getElementById('chkBxPriority'); 
		showHideBgColor(chkBoxPriority);
		
		}
		
		function loadEvent()
		{		
		var chkBxPriority=document.getElementById('chkBxPriority');
		showHideBgColor(chkBxPriority);	
        var editClicked=document.getElementById('BnbDataButtons3_Edit');
        projectTitleKeyPress();
        if(editClicked.disabled)
        {
        var panelID1= document.getElementById('narrativeID');
        panelID1.style.display='block';
        var panelID2= document.getElementById('proposalContractID');
        panelID2.style.display='block';
        var panelID3= document.getElementById('projectStatusID');
        panelID3.style.display='block';        
        } 
		}	
		
		function showHideBgColor(chkBoxPriority)
		{
		if(chkBoxPriority.checked)
		{		
        document.getElementById('trShowYellow').style.backgroundColor= (chkBoxPriority.checked?'Yellow':'');
		document.getElementById('tdShowHideTxt').style.visibility=(document.getElementById('tdShowHideTxt').style.visibility=="hidden")?"visible":"hidden";
		}
		else
		{
		document.getElementById('trShowYellow').style.backgroundColor='';
		document.getElementById('tdShowHideTxt').style.visibility="hidden";
		}
 		}
		
		function emailPageLink()
		{
		var str=encodeURIComponent(window.location);
        var replaceTabName = document.getElementById('uxTabControl_SelectedTabID').value;
        var url = decodeURIComponent(str);
        if(url.split("&ControlID=").length== 2)
        {
        var tabName = url.split("&ControlID=")[1];
        url = url.replace(tabName,replaceTabName);
        
        window.open('mailto:?subject=PGMS-Page Link&body=%0d%0a%0d%0a%0d%0a%0d%0a'+encodeURIComponent(url)+''); window.focus(); return false;
        }
        else
        {
        url = url + '&ControlID=' + replaceTabName;
        
        window.open('mailto:?subject=PGMS-Page Link&body=%0d%0a%0d%0a%0d%0a%0d%0a'+encodeURIComponent(url)+''); window.focus(); return false;
        } 
		}
		function showHidePanel(hyperlinkID,pnlID)
		{
		var panelID= document.getElementById(pnlID);
		var hpText=document.getElementById(hyperlinkID);		
		if(panelID.style.display=='block')
		{
		panelID.style.display='none';
		hpText.innerHTML="+ Show Details";
		}
		else
		{
		panelID.style.display='block';
		hpText.innerHTML="- Hide Details";
		}
		}
		
		function projectTitleKeyPress(evt)
		 {
            document.getElementById('BnbTextBox1').onkeydown=function()
            {
                var projKeyCode = (window.event)?event.keyCode:evt.which;
                var projTitle=document.getElementById('BnbTextBox1').value;
                if(projTitle != null)
                {
                    if(projTitle.length >= 50  && !(projKeyCode == 8 || projKeyCode == 9 || projKeyCode == 35 || projKeyCode == 36 || projKeyCode == 46 || projKeyCode == 37 || projKeyCode == 39))
                    { 
 	                    document.getElementById('spnViewWarning').style.display='block';
 	                    return false;
                    }
                    else
                    {
                         document.getElementById('BnbTextBox1').onkeydown=function()
                         {
                             document.getElementById('spnViewWarning').style.display='none';
                             return true;
                         }
                    }
                }
                //mouse events
                document.getElementById('BnbTextBox1').onmousedown=function()
                {
                    var projTitle=document.getElementById('BnbTextBox1').value;
                    if(projTitle.length >= 50)
                    {
                        document.getElementById('BnbTextBox1').onpaste=function()
                        {
                            document.getElementById('spnViewWarning').style.display='block';
                            return false;
                        }
                    }
                 }
            }
        }
           
        document.onkeydown= projectTitleKeyPress;
        document.onmousedown=projectTitleKeyPress;
		</script>
		
	</head>
	<body onload="loadEvent()"  onclick="hideCalendar(event);">
		<form id="Form1" method="post" runat="server">
				
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
    	
    	<div id="baseContent">	
    	<uc1:Title id="titleBar" runat="server" imagesource="../Images/bnbproject24silver.gif"></uc1:Title>
    	<uc2:PMFStatusPanel ID="PMFStatusPanel2" runat="server" />			
			<table>
			    <tr>
			         <td>		
                        <bnbpagecontrol:bnbdatabuttons id="BnbDataButtons3" runat="server" CssClass="databuttons" UndoText="Undo" SaveText="Save"
					    EditAllText="Edit All" NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px"
					    NewWidth="55px" NewButtonEnabled="True"></bnbpagecontrol:bnbdatabuttons>
				    </td>
				    <td align="right" style="width:500px; padding:5px;">
			            <a href="#" onclick="emailPageLink();return false;" > <b >Email Page Link</b></a>
			        </td>
			        <td>
			            <asp:Button ID="btnUpload" runat="server" Text="UPLOAD NEW DOCUMENT" OnClick="btnUpload_Click" />
			        </td>
			    </tr>
			</table>
			 <span id="spnViewWarning" runat="server" class="viewwarning" style="display:none;">Project Title must not exceed 50 characters</span>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox2" runat="server" CssClass="messagebox" SuppressRecordSavedMessage="false"
					PopupMessage="false"></bnbpagecontrol:bnbmessagebox>	
				<!-- table for data buttons -->					
				
				<asp:Label id="lblProjectStatus" runat="server" CssClass="viewwarning"></asp:Label>&nbsp;
			
                <bnbpagecontrol:BnbTabControl ID="uxTabControl" runat="server" />
                <table cellspacing="0px" width="100%">
                    <tr id="trShowYellow"   >
                        <td id="tdShowHideTxt" style="visibility:hidden; color:Red;font-size:15px; font-weight:bold; font-style:italic; padding:5px; width:200px;">
                        High Priority Project
                        </td>
                        <td  style="height:30px; width:750px; font-weight:bold; font-size:12px; " align="right">
                        PRIORITY<asp:CheckBox ID="chkBxPriority" Enabled="false"  runat="server"  /></td>
                    </tr>
                </table>			
			<P>
             </P>
            <asp:Panel id="Main" runat="server" CssClass="tcTab" BnbTabName="Main">
				
            <asp:Panel ID="pnlMainHeader" runat="server">
            <table cellspacing="0px">
                <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlGeneral','mainID'); return false;" >
                    <td style="width:500px; padding:5px;">General Information</td>
                    <td align="right" style=" width:450px; padding:5px;"><a href="#" id="hlGeneral">- Hide Details</a></td>
            
                </tr>
            </table>
            <div id="mainID" style="display:block;"  >
                <table id="Table1" border="0" class="fieldtable">		
                <tr>
                    <td style="font-weight:bold; color:Silver;">General Project Overview</td>
                </tr>		
					<tr>
						<td class="label">Project&nbsp;Title</td>
						<td style="width: 263px"><bnbdatacontrol:bnbtextbox id="BnbTextBox1" runat="server" CssClass="textbox" Width="250px" Height="20px" DataMember="BnbProject.ProjectTitle"
								MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></td>
						<td class="label">Abbrev</td>
						<td><bnbdatacontrol:bnbtextbox id="Bnbtextbox2" runat="server" CssClass="textbox" Width="90px" Height="20px" DataMember="BnbProject.ProjectAbbr"
								MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></td>
					</tr>
					<p>
				<asp:panel id="pnlVSOGoals" runat="server">
				    <tr>
					    <td class="label" >Programme</td>
						<td  colspan="3"><bnbdatagrid:bnbdatagridfordomainobjectlist id="bnbdgVSOGoals" runat="server" CssClass="datagrid" Width="200px" DataMember="BnbProject.Objectives"
										    SortBy="ID" VisibleOnNewParentRecord="false" AddButtonVisible="true" EditButtonText="Edit" AddButtonText="Add" EditButtonVisible="true"
										    DeleteButtonVisible="True" DataGridForDomainObjectListType="Editable" DataGridType="Editable"
										    DataProperties="MonitorObjectiveID [BnbLookupControl:vlkuGoalsAndThemes:200px]"></bnbdatagrid:bnbdatagridfordomainobjectlist></td>
					<tr>					    
					<td class="label" >VSO Goal</TD>
						<td  colspan="3"><bnbdatagrid:bnbdatagridfordomainobjectlist id="bnbdgProjectVSOGoal" runat="server" CssClass="datagrid" Width="200px" DataMember="BnbProject.VsoGoalObjectives"
											DataProperties="MonitorObjectiveID [BnbLookupControl:lkuMonitorObjective:150px]" DataGridType="Editable" DataGridForDomainObjectListType="Editable"
											DeleteButtonVisible="True" EditButtonVisible="true" AddButtonText="Add" EditButtonText="Edit" AddButtonVisible="true" VisibleOnNewParentRecord="false"
											SortBy="ID"></bnbdatagrid:bnbdatagridfordomainobjectlist></td>	
					<tr>
						<td class="label" >Country Office</TD>
						<td  colspan="3"><bnbdatagrid:bnbdatagridfordomainobjectlist id="bnbdgProgrammeOffices" runat="server" CssClass="datagrid" Width="200px" DataMember="BnbProject.ProgrammeOffices"
											DataProperties="ProgrammeOfficeID [BnbLookupControl:lkuProgrammeOffice:150px]" DataGridType="Editable" DataGridForDomainObjectListType="Editable"
											DeleteButtonVisible="True" EditButtonVisible="true" AddButtonText="Add" EditButtonText="Edit" AddButtonVisible="true" VisibleOnNewParentRecord="false"
											SortBy="ID"></bnbdatagrid:bnbdatagridfordomainobjectlist></td>
					</tr>													
				    </tr>
				</asp:panel>
				<asp:panel id="pnlNewModeProgrammeOfficeAndGoal" runat="server">

					<tr><td class="label" >Country Office</td>
					    <td><bnbdatacontrol:BnbLookupControl id="BnbLookupControl14" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
							DataMember="BnbProjectProgrammeOffice.ProgrammeOfficeID" DataTextField="Description" LookupControlType="ComboBox"
							LookupTableName="lkuProgrammeOffice" ListBoxRows="4"></bnbdatacontrol:BnbLookupControl></td>
				    <tr>
					    <td class="label" >VSO Goal/Theme</td>
					    <td><bnbdatacontrol:BnbLookupControl id="BnbLookupControl15" runat="server" CssClass="lookupcontrol" Height="22px" Width="250px"
							DataMember="BnbProjectObjective.MonitorObjectiveID" DataTextField="Description" LookupControlType="ComboBox"
							LookupTableName="vlkuGoalsAndThemes" ListBoxRows="4"></bnbdatacontrol:BnbLookupControl>
					    </td>
					</tr>
		            </tr>
		        </asp:panel></P>
							
					<tr>
					<td class="label">Project&nbsp;Accountable</td>
						<td><bnbdatacontrol:bnblookupcontrol id="lkuProjectAccountableOfficer" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
								DataMember="BnbProject.ProjectAccountableOfficerID" LookupTableName="vlkuBonoboUser" LookupControlType="ComboBox"
								ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:bnblookupcontrol><br>
							  <bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol3" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
								DataMember="BnbProject.ProjectAccountableDepartmentID" LookupTableName="appDepartment" LookupControlType="ReadOnlyLabel"
								ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:bnblookupcontrol>
						<td class="label">Project&nbsp;Responsible&nbsp;</td>
						<td>
							<p><bnbdatacontrol:bnblookupcontrol id="lkuProjectResponsibleOfficer" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									DataMember="BnbProject.ProjectResponsibleOfficerID" LookupTableName="vlkuBonoboUser" LookupControlType="ComboBox"
									ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:bnblookupcontrol><br>
								 <bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol4" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									DataMember="BnbProject.ProjectResponsibleDepartmentID" LookupTableName="appDepartment" LookupControlType="ReadOnlyLabel"
									ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:bnblookupcontrol>
						    </p>
						</td>
					</tr>
					<tr>
						<td class="label">Project&nbsp;Status</td>
						<td><bnbdatacontrol:bnblookupcontrol id="lkupProjectStatus" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
								DataMember="BnbProject.CurrentProjectStatus.ProjectStatusID" LookupTableName="lkuProjectStatus" LookupControlType="ComboBox"
								ListBoxRows="4" DataTextField="Description"  SortByDisplayOrder="True"></bnbdatacontrol:bnblookupcontrol></TD>
					    <td class="label" style="height: 25px">Project Duration (months)</td>
						<td style="height: 25px"><bnbdatacontrol:bnbdecimalbox id="BnbDecimalBox6" runat="server" CssClass="textbox" Width="90px" Height="20px"
								DataMember="BnbProject.Duration" TextAlign="left" DecimalPlaces="2"></bnbdatacontrol:bnbdecimalbox></td>
					</tr>
					<tr>
						<td class="label">Start Date</td>
						<td style="width: 263px"><bnbdatacontrol:bnbdatebox id="BnbDateBox1" ShowCalender="true"  runat="server" CssClass="datebox" Width="90px" Height="20px" DataMember="BnbProject.StartDate"
								DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></td>
						<td class="label">End Date</td>
						<td><bnbdatacontrol:bnbdatebox id="BnbDateBox2" ShowCalender="true"  runat="server" CssClass="datebox" Width="90px" Height="20px" DataMember="BnbProject.EndDate"
								DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></td>
					
					</tr>
					<tr>
					    <td class="label">
                                    Project Contact</td>
					    <td><asp:Label ID="lblProjectContact" runat="server"></asp:Label>
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl8" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox"
                                LookupTableName="vlkuBonoboUser" DataMember="BnbProject.ProgrammeOfficerID"></bnbdatacontrol:BnbLookupControl>
					    
					    </td>
					     <td class="label">Project Region</td>
						<td><bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol2" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
								DataMember="BnbProject.ProjectRegionID" LookupTableName="lkuProgrammeOfficeRegion" LookupControlType="ComboBox"
								ListBoxRows="4" DataTextField="Description"  SortByDisplayOrder="True"></bnbdatacontrol:bnblookupcontrol></TD>
					</tr>
					<tr>
					    <td style="font-weight:bold; color:Silver;">General Status Overview</td>
					</tr>
					<tr>
					<td class="label">Project Priority</td>
					<td><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl6" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
								DataMember="BnbProject.ProjectPriorityID" LookupTableName="lkuProjectPriority" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"
								SortByDisplayOrder="True"></bnbdatacontrol:bnblookupcontrol></td>
					</tr>
					<tr>
					    <td class="label">Overall Project Risk  Assessment</td>
					    <td><span class="textbox"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl7" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
								DataMember="BnbProject.RiskID" LookupTableName="lkuProjectRisk" LookupControlType="ReadOnlyLabel" ListBoxRows="4" DataTextField="Description"
								SortByDisplayOrder="True" NoLineBreaks="False"></bnbdatacontrol:bnblookupcontrol></span></td>
					    <td class="label">Budget Line</td>
					    <td>
						<bnbdatacontrol:BnbTextBox id="BnbTextBox9" runat="server" CssClass="textbox" Width="250px" Height="20px" DataMember="BnbProject.BudgetLine"
							MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:BnbTextBox>
					    </td>					
					</tr>
					
					<tr>
						<td class="label" style="height: 25px">Project Code</td>
						<td style="height: 25px; width: 263px;"><bnbdatacontrol:bnbtextbox id="BnbTextBox3" runat="server" CssClass="textbox" Width="90px" Height="20px" DataMember="BnbProject.ProjectCode"
								MultiLine="false" Rows="1" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></td>
					    <td class="label">Grant Code list</td>
                        <td>
                            <bnbdatagrid:BnbDataGridForView ID="BnbDGFVGrantCodelist" runat="server" CssClass="datagrid" 
                            ViewName="vwbonoboGrantcodeList" ColumnsToExclude="ProjectCode" Width="150px" />
                        </td> 							
					</tr>
					<tr>
					</tr>
					<tr><td style="font-weight:bold; color:Silver;">Seeking Funding Overview</td></tr>
				     <tr>
					<td class="label">Seeking Funding?</td>
					<td><asp:CheckBox ID="chkBxSeekingFunding" runat="server" Enabled="false" /></td>
					<td class="label">Funding Comments</td>
					<td><bnbdatacontrol:BnbTextBox id="BnbTextBox5" runat="server" CssClass="textbox" Width="250px" Height="20px" DataMember="BnbProject.FundingComments"
							MultiLine="True" Rows="3" StringLength="0" ControlType="Text"></bnbdatacontrol:BnbTextBox></td>
				</tr>					
			</table>
       </div>
</asp:Panel>
        <br />
			<asp:Panel ID="pnlNarrative" runat="server">
            <table cellspacing="0px">
                <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlNarrative','narrativeID'); return false;">
                    <td style="width:500px; padding:5px;">Narrative</td>
                    <td align="right" style=" width:450px; padding:5px;"><a href="#" id="hlNarrative" >+ Show Details</a></td>            
                </tr>
            </table>
			</asp:Panel>
			<div id="narrativeID" style="display:none;">
			<asp:Panel ID="pnlNarrativeContent" runat="server">
               <br /> 
               <table id="Table2" border="0" class="fieldtable">	
					<tr>
						<td class="label">Narrative Summary</td>
						<td colSpan="3"><bnbdatacontrol:bnbtextbox id="BnbTextBox4" runat="server" CssClass="textbox" Width="465px" Height="20px" DataMember="BnbProject.ProjectSummary"
								MultiLine="True" Rows="3" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></td>
					</tr>					
                    <tr>
                        <td class="label"> Objectives</td>
                        <td colspan="3"><bnbdatacontrol:BnbTextBox ID="BnbTextBox11" runat="server" ControlType="Text" CssClass="textbox"
                                DataMember="BnbProject.ProjectObjectives" Height="20px" MultiLine="True" Rows="3"
                                StringLength="0" Width="465px" /></td>
                    </tr>
                     <tr>
                        <td class="label" style="height: 25px">Problem Summary</td>
                        <td colspan="3" style="height: 25px"><bnbdatacontrol:BnbTextBox ID="BnbTextBox14" runat="server" ControlType="Text" CssClass="textbox"
                                DataMember="BnbProject.ProblemSummary" Height="20px" MultiLine="True" Rows="3"
                                StringLength="0" Width="465px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label"> Aim</td>
                        <td colspan="3"><bnbdatacontrol:BnbTextBox ID="BnbTextBox10" runat="server" ControlType="Text" CssClass="textbox"
                                DataMember="BnbProject.ProjectAims" Height="20px" MultiLine="True" Rows="3"
                                StringLength="0" Width="465px" /></td>
                    </tr>
                    <tr>
                        <td class="label"> Activities</td>
                        <td colspan="3"><bnbdatacontrol:BnbTextBox ID="BnbTextBox13" runat="server" ControlType="Text" CssClass="textbox"
                                DataMember="BnbProject.ProblemActivities" Height="20px" MultiLine="True" Rows="3"
                                StringLength="0" Width="465px" /></td>
                    </tr>
                   	<tr>
                        <td class="label"> Outcomes</td>
                        <td colspan="3"><bnbdatacontrol:BnbTextBox ID="BnbTextBox12" runat="server" ControlType="Text" CssClass="textbox"
                                DataMember="BnbProject.ProjectOutcomes" Height="20px" MultiLine="True" Rows="3"
                                StringLength="0" Width="465px" /></td>
                    </tr>				
                </table>
<%--			Testing<bnbdatacontrol:BnbTextBox ID="txt" runat="server" />
--%>				<!-- table for block of fields at base of project details section (contains other tables) -->
		    </asp:Panel></div>
            <br />         
			<asp:Panel ID="pnlProposalOverview" runat="server">
                <table cellspacing="0px">
                    <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlContracts','proposalContractID'); return false;">
                        <td style="width:500px; padding:5px;">Contracts And Proposals Overview</td>
                        <td align="right" style=" width:450px; padding:5px;"><a href="#" id="hlContracts">- Hide Details</a></td>
                    </tr>
                </table>
			</asp:Panel>
		<div id="proposalContractID" style="display:block;">
			<asp:Panel ID="pnlProposalContractContent" runat="server">
			<p>Contract Overview</p>
			<p><bnbdatagrid:bnbdatagridforview id="BnbDataGridForView1" runat="server" CssClass="datagrid" Width="100%" GuidKey="tblGrant_GrantID"
					RedirectPage="ContractPage.aspx" QueryStringKey="BnbProject" FieldName="tblProject_ProjectID" GuidDataProperty="GrantID"
					DomainObject="BnbGrant" ViewName="vwBonoboProjectContracts" ViewLinksVisible="true" NewLinkVisible="False"
					NewLinkText="New Contract" ViewLinksText="View"></bnbdatagrid:bnbdatagridforview></p>
			<p>Proposal Overview</p>
			<p><bnbdatagrid:bnbdatagridforview id="BnbDataGridForView2" runat="server" CssClass="datagrid" Width="100%" GuidKey="tblGrant_GrantID"
					NewLinkReturnToParentPageDisabled="True" RedirectPage="ProposalPage.aspx" QueryStringKey="BnbProject" FieldName="tblProject_ProjectID"
					GuidDataProperty="GrantID" DomainObject="BnbGrant" ViewName="vwBonoboProjectProposals" ViewLinksVisible="true"
					NewLinkVisible="true" NewLinkText="New Proposal" ViewLinksText="View"></bnbdatagrid:bnbdatagridforview></P>
			
			<p>
				<!-- table for block of fields at base of project details section (contains other tables) -->
			</asp:Panel>
		</div>
        <br />
                

        <asp:Panel ID="pnlProjectStatus" runat="server">
            <table cellspacing="0px">
                <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlProject','projectStatusID'); return false;">
                    <td style="width:500px; padding:5px;">Project Reporting Requirements</td>
                    <td align="right" style="width:450px; padding:5px;"><a href="#" id="hlProject">+ Show Details</a></td>
                </tr>
            </table>
        </asp:Panel>
        <div id="projectStatusID" style="display:none;">
         <p style="font-weight:bold; color:Silver;">Reporting Overview</p>
         <p>
             <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView3" runat="server" CssClass="datagrid"
                        Width="100%" GuidKey="tblGrant_GrantID" NewLinkReturnToParentPageDisabled="True"
                        RedirectPage="ContractPage.aspx" QueryStringKey="BnbProject" FieldName="tblProject_ProjectID"
                        GuidDataProperty="GrantID" DomainObject="BnbGrant" ViewName="vwBonoboProjectReports"
                        ViewLinksVisible="false" NewLinkVisible="false">
             </bnbdatagrid:BnbDataGridForView>
         </p><br />
        <%--<asp:Panel ID="pnlProjectContent" runat="server">
				<TABLE id="Table7" border="0" class="fieldtable">
					<TR>
						<TD>
							<!-- panel for Table9 -->
							<asp:panel id="pnlProgrammeOffices" runat="server">
								<!-- table for regions through to programme offices -->
								<TABLE id="Table9" width="100%" border="0">
									<!--<TR>
										<TD class="label" width="30">No specific<BR>
											Programme Office</TD>
										<TD>
											<bnbdatacontrol:BnbCheckBox id="BnbCheckBox2" runat="server" CssClass="checkbox" DataMember="BnbProject.Global"></bnbdatacontrol:BnbCheckBox></TD>
									</TR>-->
									<TR>
										<TD class="label" width="30">Regions</TD>
										<TD>
											<asp:Label id="lblRegions" runat="server"></asp:Label></TD>
									</TR>
									
								</TABLE>
							</asp:panel></TD>
						
					</TR>
					<!-- end of table6 --> 
				</TABLE>
        </asp:Panel>--%></div>
    
       

			<P></P> </asp:Panel>
			
			<asp:panel id="ProjectFinancing" runat="server" CssClass="tcTab" BnbTabName="Financing">
			<table id="Table3" border="0" class="fieldtable">
				<tr>
					<td class="label"> Project  Total Cost&nbsp;(�)</td>
					<td><bnbdatacontrol:bnbdecimalbox id="BnbDecimalBox4" runat="server" CssClass="textbox" Width="80px" Height="20px"
							DataMember="BnbProject.ProjectTotalCost" DecimalPlaces="0"></bnbdatacontrol:bnbdecimalbox></td>
				</tr>
            </table><br />
			 <asp:Panel ID="Financing" runat="server">
                    <table cellspacing="0px">
                        <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlProjectOverview','projectOverViewID'); return false;">
                            <td style="width: 500px; padding: 5px;">
                                Project Overview Tab - Contract Only</td>
                            <td align="right" style="width: 450px; padding: 5px;">
                                <a href="#" id="hlProjectOverview">- Hide Details</a></td>
                        </tr>
                    </table>
                    <div id="projectOverViewID" style="display: block;">
                        <p>
                            <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView4" runat="server" CssClass="datagrid"
                                DisplayNoResultMessageOnly="false" DisplayResultCount="false" NewLinkText="New"
                                NewLinkVisible="False" ShowResultsOnNewButtonClick="false" ViewLinksText="View"
                                ViewLinksVisible="False" Width="100%" FieldName="tblProject_ProjectID" QueryStringKey="BnbProject"
                                ViewName="vwBonoboProjectOverview" />
                        </p>
                    </div>
                    <br />
                    <table cellspacing="0px">
                        <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlDonorFinanceCost','donorFinanaceCostID'); return false;">
                            <td style="width: 500px; padding: 5px;">
                                Donor Finance Costs - Contracts and Proposals</td>
                            <td align="right" style="width: 450px; padding: 5px;">
                                <a href="#" id="hlDonorFinanceCost">- Hide Details</a></td>
                        </tr>
                    </table>
                    <div id="donorFinanaceCostID" style="display: block;">
                        <p>
                            <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView5"  runat="server" CssClass="datagrid"
                                DisplayNoResultMessageOnly="false" RedirectPage="ProposalPage.aspx" DisplayResultCount="false" NewLinkVisible="False"
                                ShowResultsOnNewButtonClick="false" ViewLinksText="View" ViewLinksVisible="True"
                                Width="100%" DomainObject="BnbGrant" FieldName="tblProject_ProjectID" GuidKey="tblGrant_GrantID" QueryStringKey="BnbProject"
                                ViewName="vwBonoboDonorFinanceCost" NewLinkText="New" />
                        </p>
                    </div>
                    <br />
                    <table cellspacing="0px">
                        <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlPaymentSchedule','paymentScheduleID'); return false;">
                            <td style="width: 500px; padding: 5px;">
                                Payment Schedule</td>
                            <td align="right" style="width: 450px; padding: 5px;">
                                <a href="#" id="hlPaymentSchedule">- Hide Details</a></td>
                        </tr>
                    </table>
                    <div id="paymentScheduleID" style="display: block;">
                    <br />
                        <asp:GridView ID="gvPaymentSchedule" AutoGenerateColumns="false" OnPreRender="gvPaymentSchedule_OnPreRender" runat="server" CssClass="datagrid">
                        <Columns>
                            <asp:BoundField DataField="tblCompany_CompanyName" HeaderText="Organisation Name" />
                            <asp:BoundField DataField="tblGrant_OverrideFundCode" HeaderText="Grant Code" />
                            <asp:BoundField DataField="Custom_Claimtype" HeaderText="Claim Type"/>
                            <asp:BoundField DataField="tblGrant_ClaimIncomeReceivedBy" HeaderText="Income Received By"/>
                            <asp:BoundField DataField="tblGrant_ClaimDueAmountCurrency" DataFormatString="{0:N0}" HeaderText="Amount (in Currency)"/>
                            <asp:BoundField DataField="tblGrant_ClaimDueAmountSterling" DataFormatString="{0:N0}" HeaderText="Amount (�) "/>
                            <asp:BoundField DataField="tblGrant_ClaimDueDate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date"/>
                            <asp:BoundField DataField="Custom_ClaimStatus" HeaderText="Claim Status"/>
                            <asp:BoundField DataField="tblGrant_ClaimSubmittedBy" HeaderText="Claim Received By"/>
                            <asp:BoundField DataField="tblGrant_ClaimDateReceived" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date Received"/>
                            <asp:BoundField DataField="tblGrant_AmountReceivedDue" DataFormatString="{0:N0}" HeaderText="Due (in Currency)"/>
                            <asp:BoundField DataField="tblGrant_AmountReceivedCurrency" DataFormatString="{0:N0}" HeaderText="Received (in Currency)"/>
                            <asp:BoundField DataField="tblGrant_AmountReceivedSterling" DataFormatString="{0:N0}" HeaderText="Received (�)"/>
                            <asp:BoundField DataField="tblGrant_AmountReceivedDateDue" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date Due"/>
                            <asp:BoundField DataField="tblGrant_AmountDateReceived" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date Received"/>
                        </Columns>
                        </asp:GridView> 
                    </div><br /><br /><br /><br />
                </asp:Panel>
                <p>
            </asp:Panel>
			<asp:Panel id="ProjectRisk" runat="server" CssClass="tcTab" BnbTabName="Risk">			
			<!-- TPT: ADDED Risk Categorization - Iteration 4 -->
                <asp:Panel runat="server" ID="RiskCategorizationPanel">
                     <table cellspacing="0px">
                        <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlRiskCategorization','dvRiskCategorization'); return false;">
                            <td style="width: 500px; padding: 5px;">
                                Risk Categorisation</td>
                            <td align="right" style="width: 450px; padding: 5px;">
                                <a href="#" id="hlRiskCategorization">- Hide Details</a></td>
                        </tr>
                    </table><br />
                    <div id="dvRiskCategorization" style="display: block">
                    <table border="0" id="table9" class="fieldtable" cellpadding="5" cellspacing="5">                                          
                        <tr>
                            <td class="label" style="width: 40%">
                                <p class="paragraph">
                                    <b class="bold">Match funding requirement</b>
                                </p>
                            </td>
                            <td style="width: 60%">
                                <bnbdatacontrol:BnbLookupControl ID="lkupGrantMatchFunding" ShowBlankRow="true" runat="server" CssClass="lookupcontrol"
                                    DataTextField="Description" Height="22px" ListBoxRows="4" LookupControlType="ComboBox"
                                    DataMember="BnbProject.ProjectRisk.MatchFundingID"
                                    LookupTableName="lkuProjectRiskMatchFunding" Width="500px" SortByDisplayOrder="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="width: 40%">
                                <p class="paragraph">
                                    <b class="bold">Financial Feasibility (see PTC)</b>
                                </p>
                            </td>
                            <td style="width: 60%">
                                <bnbdatacontrol:BnbLookupControl ID="lkuProjectFeasibility" runat="server" CssClass="lookupcontrol"
                                    DataTextField="Description" Height="22px" ListBoxRows="4" LookupControlType="ComboBox"
                                    DataMember="BnbProject.ProjectRisk.ProjectFeasibilityID" ShowBlankRow="true" 
                                    LookupTableName="lkuProjectFeasibility" Width="500px" SortByDisplayOrder="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="width: 40%">
                                <p class="paragraph">
                                    <b class="bold">Grant management experience</b>
                                </p>
                            </td>
                            <td style="width: 60%">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl18" runat="server" CssClass="lookupcontrol"
                                    DataTextField="Description" Height="22px" ListBoxRows="4" LookupControlType="ComboBox"
                                    DataMember="BnbProject.ProjectRisk.POCapacityID"
                                    LookupTableName="lkuProjectRiskPOCapacity" Width="500px" SortByDisplayOrder="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="width: 40%">
                                <p class="paragraph">
                                    <b class="bold">Alignment to strategy</b>
                                    </p>
                            </td>
                            <td style="width: 60%">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl10" runat="server" CssClass="lookupcontrol"
                                    DataTextField="Description" Height="22px" ListBoxRows="4" LookupControlType="ComboBox"
                                    DataMember="BnbProject.ProjectRisk.CorporateOperationalPlansID"
                                    LookupTableName="lkuProjectRiskCorporateOperPlans" Width="500px" SortByDisplayOrder="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="width: 40%">
                                <p class="paragraph">
                                    <b class="bold">Volunteer Recruitment</b>
                                </p>
                            </td>
                            <td style="width: 60%">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl22" runat="server" CssClass="lookupcontrol"
                                    DataTextField="Description" Height="22px" ListBoxRows="4" LookupControlType="ComboBox"
                                    DataMember="BnbProject.ProjectRisk.VolunteerRecruitabilityID"
                                    LookupTableName="lkuProjectRiskVolRecruit" Width="500px" SortByDisplayOrder="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="width: 40%">
                                <p class="paragraph">
                                    <b class="bold">Contract documentation</b>
                                </p>
                            </td>
                            <td style="width: 60%">
                                <bnbdatacontrol:BnbLookupControl ID="lkupContractualCondition" runat="server" CssClass="lookupcontrol"
                                    DataTextField="Description" Height="22px" ListBoxRows="4" LookupControlType="ComboBox"
                                    DataMember="BnbProject.ProjectRisk.ContractualConditionsID" ShowBlankRow="true" 
                                    LookupTableName="lkuProjectRiskContractualCondn" Width="500px" SortByDisplayOrder="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="width: 40%">
                                <p class="paragraph">
                                    <b class="bold">Comments on risks above</b>
                                </p>
                            </td>
                            <td style="width: 60%">
                                 <bnbdatacontrol:BnbTextBox ID="BnbTextBox6" runat="server" ControlType="Text" CssClass="textbox"
                                    Width="450px" MultiLine="True" DataMember="BnbProject.ProjectRisk.RiskComments"
                                    Rows="4" />
                            </td>
                        </tr>
                         <tr>
					        <td class="label" style="width: 40%">
					            <p class="paragraph">
					                <b class="bold">Overall Project Risk Assessment</b>
					            </p></td>
					        <td style="width: 60%"><bnbdatacontrol:bnblookupcontrol id="BnbLookupControl5" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
								        DataMember="BnbProject.RiskID" LookupTableName="lkuProjectRisk" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"
								        SortByDisplayOrder="True" NoLineBreaks="False"></bnbdatacontrol:bnblookupcontrol></td>
				        </tr>
				        <tr>
                            <td class="label" width="40%">
                                <p class="paragraph">
                                    <b class="bold">Has a more detailed risk assessment <br /> of this project been discussed with CD and RD?</b>
                                </p>
                            </td>
                            <td width="60%">
                                <bnbdatacontrol:bnblookupcontrol id="BnbLookupControl1" runat="server" CssClass="lookupcontrol" Width="250px"
							        DataMember="BnbProject.ProjectRisk.RiskAssessmentCompletedID" LookupTableName="lkuRiskAssessmentCompleted" LookupControlType="RadioButtonList" ListBoxRows="4" DataTextField="Description"
							        SortByDisplayOrder="true"></bnbdatacontrol:bnblookupcontrol>
							</td>
						</tr>
                    </table>
                    </div> 
                </asp:Panel>
                <!-- Contract Risk Overview added in Risk Tab for Iteration 4 -->
                <asp:Panel runat="server" ID="RiskOverviewPanel">
                    <table cellspacing="0px">
                        <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlRiskOverview','dvRiskOverview'); return false;">
                            <td style="width: 500px; padding: 5px;">
                                Contract Risk Overview!</td>
                            <td align="right" style="width: 450px; padding: 5px;">
                                <a href="#" id="hlRiskOverview">- Hide Details</a></td>
                        </tr>
                    </table><br />
                    <div id="dvRiskOverview" style="display: block">
                    <table border="0" id="tblRisk" class="fieldtable">
                        <tr>
                            <td class="label" width="25%">
                                Procurement</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox15" runat="server" ControlType="Text" CssClass="textbox"
                                    Width="250px" MultiLine="True" DataMember="BnbProject.ProjectRisk.Procurement"
                                    Rows="4" />
                            </td>
                            <td class="label" width="25%">
                                Visibility/Publicity</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox16" runat="server" ControlType="Text" CssClass="textbox"
                                    Width="250px" MultiLine="True" DataMember="BnbProject.ProjectRisk.Visibility"
                                    Rows="4" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%">
                                Asset management/Ownership</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox17" runat="server" ControlType="Text" CssClass="textbox"
                                    Width="250px" MultiLine="True" DataMember="BnbProject.ProjectRisk.AssetManagement"
                                    Rows="4" />
                            </td>
                            <td class="label" width="25%">
                                Personnel Restriction</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox18" runat="server" ControlType="Text" CssClass="textbox"
                                    Width="250px" MultiLine="True" DataMember="BnbProject.ProjectRisk.PersonnelRestriction"
                                    Rows="4" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%">
                                Legal Compliance/Jurisdiction</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox19" runat="server" ControlType="Text" CssClass="textbox"
                                    Width="250px" MultiLine="True" DataMember="BnbProject.ProjectRisk.LegalCompliance"
                                    Rows="4" />
                            </td>
                            <td class="label" width="25%">
                                Matched Funding Requirements</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox20" runat="server" ControlType="Text" CssClass="textbox"
                                    Width="250px" MultiLine="True" DataMember="BnbProject.ProjectRisk.MatchedFundingRequirements"
                                    Rows="4" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%">
                                Audit Requirements</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox21" runat="server" ControlType="Text" CssClass="textbox"
                                    Width="250px" MultiLine="True" DataMember="BnbProject.ProjectRisk.AuditRequirements"
                                    Rows="4" />
                            </td>
                            <td class="label" width="25%">
                                Financial Requirements</td>
                            <td width="25%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox22" runat="server" ControlType="Text" CssClass="textbox"
                                    Width="250px" MultiLine="True" DataMember="BnbProject.ProjectRisk.FinancialRequirements"
                                    Rows="4" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="25%" style="height: 25px">
                                Filing Period</td>
                            <td width="25%" style="height: 25px">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox23" runat="server" ControlType="Text" CssClass="textbox"
                                    Width="250px" MultiLine="True" DataMember="BnbProject.ProjectRisk.FilingPeriod"
                                    Rows="4" />
                            </td>
                            <td class="label" width="25%" style="height: 25px">
                                Other Comments</td>
                            <td width="25%" style="height: 25px">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox24" runat="server" ControlType="Text" CssClass="textbox"
                                    Width="250px" MultiLine="True" DataMember="BnbProject.ProjectRisk.OtherComments"
                                    Rows="4" />
                            </td>
                        </tr>
                         <tr>
                            <td class="label" width="25%" style="height: 25px">
                                    Changes Requested by Donor</td>
                                <td width="25%" style="height: 25px">
                                    <bnbdatacontrol:BnbTextBox ID="BnbTextBox7" runat="server" ControlType="Text" CssClass="textbox"
                                        Width="250px" MultiLine="True" DataMember="BnbProject.ProjectRisk.ChangesRequestedbyDonor"
                                        Rows="4" />
                                </td>
                            </tr>
                    </table>
                  </div><br />
                </asp:Panel> 
			</asp:Panel>
			<asp:Panel id="DMSDocuments" runat="server" CssClass="tcTab" BnbTabName="Project Documents">
			
			<p>This section shows Donor documents from DMS that have been linked to the current Project 
				using the PGMS field in the Document Profile.</p>
				
				<p><bnbdmsSearchControl:BnbDMSSearchControl id="dmsSearchControl" runat="server" CssClass="datagrid" Width="100%" ViewLinksText="View"
					NewLinkText="New" NewLinkVisible="true" ViewLinksVisible="true" ShowResultsOnNewButtonClick="false" DisplayResultCount="false"
					DisplayNoResultMessageOnly="false" CacheByQueryKey="BnbProject" RedirectPageURL="../Redirect.aspx" NoResultMessage="(No Donor Documents Found)" ></bnbdmsSearchControl:BnbDMSSearchControl></p>
			    <p>
                    <asp:HyperLink ID="uxGeneratePMPGHyper" runat="server">Generate PMPG Document</asp:HyperLink>&nbsp;
                </p>
      			<p>
				<bnbdmsSearchControl:BnbDMSSearchControl id="dmsSearchControlPMPG" runat="server" CssClass="datagrid" Width="100%" ViewLinksText="View"
					NewLinkText="New" NewLinkVisible="true" ViewLinksVisible="true" ShowResultsOnNewButtonClick="false" DisplayResultCount="false" 
					DisplayNoResultMessageOnly="false" CacheByQueryKey="BnbProject"  RedirectPageURL="../Redirect.aspx" NoResultMessage="(No PMPG Documents Found)" NewDocLinkText="Upload automatically generated PMPG document..." /></p>
		
			</asp:Panel>
			<asp:panel id="PMPGDocumentsTab" runat="server" CssClass="tcTab" BnbTabName="Sign Off">
			    <p>                                    
                </p>
                <table cellspacing="0px">
                <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlApproval','divApprovalID'); return false;">
                        <td style="width: 500px; padding: 5px;">
                            Approval</td>
                        <td align="right" style="width: 450px; padding: 5px;">
                            <a href="#" id="hlApproval">- Hide Details</a></td>
                    </tr>
                    <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlApproval','divApprovalID'); return false;"
                        style="font-weight: lighter">
                        <td colspan="2" style="padding: 5px;">
                            <br />
                            <i>NB: Proposals can be approved by Project Accountable (if under �250k pa and low risk),
                                by RD (if over �250k pa or high risk) or by GLT (if over �1m). If you have been
                                delegated the task of approving the 'proposal' 'contract' or 'closure' please make
                                sure you have approval from the responsible person(s) in writing and attach their
                                e-mail/authorization 
                            </i></td>
                    </tr>
                </table><br />
                <div id="divApprovalID" style="display: block;">
                    <table cellspacing="0px" width="100%">
                    <tr style="height:20px;background-color: rgb(238,238,247); font-weight:bold; color: gray ">
                    <td style="width:24px">
                        <asp:Image ID="imgProposal" runat="server" ImageUrl="~/Images/bnbgrantproposal24silver.gif" />
                        </td><td>Proposals</td></tr>
                    </table><br />
                    <span >I confirm that the resourcing issues (financial, volunteer supply and capacity) associated with this proposal has been discussed with the relevant internal stakeholders, and that the project is of high quality and aligns clearly with the objectives of the donor. </span>
                    <p>
                        <bnbdatagrid:BnbDataGridForDomainObjectList ID="bnbDGDD" runat="server" CssClass="datagrid"
                            DataGridType="editable" EditButtonVisible="true" EditButtonText="Edit" DataGridForDomainObjectListType="Editable"
                            DataMember="BnbProject.Proposal" Width="100%" WrapText="true" SourceHeaderColumns="ProposalApprovalUserID,ProposalApprovaldate,ProposalApprovalComments" DestinationHeaderColumns="Approved By,Approved On,Please confirm that proposal has been reviewed by <br /> Regional/Programme Funding Manager and <br />Regional Finance Officer prior to approval" 
                            DataProperties="CompanyID[BnbLookupControl:vlkuOrganisationNames],IsProposalApproved[BnbCheckBox:50px],ProposalApprovalUserID[BnbLookupControl:vlkuBonoboUser],ProposalApprovaldate[BnbDateBox],ProposalApprovalComments[BnbTextBox:300px:true]"
                            VisibleOnNewParentRecord="false" />
                    </p>
                    <br />
                    <table cellspacing="0px" width="100%">
                    <tr style="height:20px;background-color: rgb(238,238,247); font-size :12px; font-weight:bold; color: gray ">
                    <td style="width:24px">
                        <asp:Image ID="imgContract" runat="server" ImageUrl="~/Images/bnbgrantcontract24silver.gif" />
                        </td><td>Contracts</td></tr>
                    </table><br />
                    <span >I have reviewed all the implications of the terms and conditions of the contract on VSO's working policies and procedures, and confirm that VSO will be able to adhere to these during project implementation.</span>
                    <br /><br />
                    <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList1"
                        runat="server" CssClass="datagrid" Width="100%" WrapText="true" DataGridType="editable" EditButtonVisible="true"
                         EditButtonText="Edit" DataGridForDomainObjectListType="Editable" SourceHeaderColumns="ContractApprovalUserID,ContractApprovalDate" DestinationHeaderColumns="Approved By,Approved On" DataMember="BnbProject.Contracts"
                        DataProperties="CompanyID[BnbLookupControl:vlkuOrganisationNames],IsContractApproved[BnbCheckBox],ContractApprovalUserID[BnbLookupControl:vlkuBonoboUser],ContractApprovalDate[BnbDateBox],ContractApprovalComments[BnbTextBox:200px:true]"
                        VisibleOnNewParentRecord="false" />
                    <br />
                    <table cellspacing="0px" width="100%">
                    <tr style="height:20px;background-color: rgb(238,238,247); font-size :12px; font-weight:bold; color: gray ">
                    <td style="width:24px">
                        <asp:Image ID="imgProject" runat="server" ImageUrl="~/Images/bnbproject24silver.gif" />
                        </td><td>Project Closure</td></tr>
                    </table><br />
                    <span >I confirm that all required conditions of the contact are fulfilled, final reports have been submitted, final payments have been made and the grant code has been closed.</span><br /><br />
                    <span> Click 'Edit All' at the top left corner of this page to modify this section.If the �Closure Approved?� tickbox is greyed out and cannot be ticked then please check that each contract linked to the project has been closed. </span><br /><br />
                    <div>
                        <table border="0" class="datagrid" width="100%">
                            <tr>
                                <th class="label" style="width: 20%">
                                    Project Title</th>
                                <th class="label" style="width: 5%">
                                    Closure Approved?
                                </th>
                                <th class="label">
                                    Approved By
                                </th>
                                <th class="label">
                                    Approved On</th>
                                <th class="label">
                                    Comments</th>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblProjecttitle" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <bnbdatacontrol:BnbCheckBox ID="ckbxUser" runat="server" DataMember="BnbProject.IsProjectClosureApproved" />
                                </td>
                                <td>
                                    <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl23" runat="server" CssClass="lookupcontrol"
                                        Width="220px" Height="22px" DataMember="BnbProject.ProjectClosureUserID" LookupTableName="vlkuBonoboUser"
                                        LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" SortByDisplayOrder="True"
                                        NoLineBreaks="False"></bnbdatacontrol:BnbLookupControl>
                                </td>
                                <td>
                                    <bnbdatacontrol:BnbDateBox ID="BnbDateBox3" ShowCalender="true" runat="server" CssClass="datebox"
                                        Width="90px" Height="20px" DataMember="BnbProject.ProjectClosureApprovaldate"
                                        DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:BnbDateBox>
                                </td>
                                <td>
                                    <bnbdatacontrol:BnbTextBox ID="BnbTextBox25" runat="server" CssClass="textbox" Width="250px"
                                        Height="20px" DataMember="BnbProject.ApprovalComments" MultiLine="true" Rows="3"
                                        StringLength="0" ControlType="Text"></bnbdatacontrol:BnbTextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br /><br /><br />
                
                 <table cellspacing="0px">
                    <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlAudit','dvAuditID'); return false;">
                        <td style="width: 500px; padding: 5px;">
                            Project Audit Trail</td>
                        <td align="right" style="width: 450px; padding: 5px;">
                            <a href="#" id="hlAudit">+ Show Details</a></td>
                    </tr>
                    <tr class="sectionHeadingForShowHide" onclick="showHidePanel('hlAudit','dvAuditID'); return false;"
                        style="font-weight: lighter">
                        <td colspan="2" style="padding: 5px;">
                            <br />
                             <i>This section details the latest actions made to this project. Click 'View History' link to view the full history.</i></td>
                    </tr>
                </table>
              <div id="dvAuditID" style="display: none;">
               <p>Select an item from the below list and press 'Go' to view Audit trail of the corresponding item</p>
                <asp:DropDownList ID="AuditSelectList1" runat="server">
                </asp:DropDownList>&nbsp;&nbsp;<asp:Button ID="Button1" runat="server" Text="Go" OnClick="Button1_Click" />
                 <asp:Panel ID="pnlSelectedAuditReport" runat="server" Visible="false">
                <p class="sectionHeading" style="padding: 3px; background-color: rgb(238,238,247);
                        color: gray">
                <asp:Label id="lblAuditHeading" runat="server"></asp:Label>                   
                </p>
                        <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView7" runat="server" CssClass="datagrid"
                            Width="100%" NewLinkReturnToParentPageDisabled="True" QueryStringKey="BnbProject" 
                            FieldName="tblProjectAudit_ProjectID"   ViewName="vwProjectGeneralAudit" GuidKey="tblProjectAudit_ProjectID"
                            ViewLinksVisible="false" NewLinkVisible="false" ViewLinksText="View">
                        </bnbdatagrid:BnbDataGridForView>
                 <br /></asp:Panel>
                <asp:Panel ID="pnlAuditTrialAllReports" runat="server">
                <p class="sectionHeading" style="padding: 3px; background-color: rgb(238,238,247);color: gray">
                <asp:Label id="Label1" runat="server">Project General</asp:Label>                   
                </p>
                        <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView8" runat="server" CssClass="datagrid"
                        Width="100%" NewLinkReturnToParentPageDisabled="True" QueryStringKey="BnbProject"
                        FieldName="tblProjectAudit_ProjectID" ViewName="vwProjectGeneralAudit" GuidKey="tblProjectAudit_ProjectID"
                        ViewLinksVisible="false" NewLinkVisible="false" ViewLinksText="View"></bnbdatagrid:BnbDataGridForView>
                <br />
                <p class="sectionHeading" style="padding: 3px; background-color: rgb(238,238,247);color: gray">
                <asp:Label id="Label2" runat="server">Proposal General</asp:Label>                   
                </p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView9" runat="server" CssClass="datagrid"
                        Width="100%" NewLinkReturnToParentPageDisabled="True" QueryStringKey="BnbProject"
                        FieldName="tblProjectAudit_ProjectID" ViewName="vwProposalGeneralAudit" GuidKey="tblProjectAudit_ProjectID"
                        ViewLinksVisible="false" NewLinkVisible="false" ViewLinksText="View"></bnbdatagrid:BnbDataGridForView>
                <br />
                <p class="sectionHeading" style="padding: 3px; background-color: rgb(238,238,247);color: gray">
                <asp:Label id="Label4" runat="server">Proposal Progress</asp:Label>                   
                </p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView10" runat="server" CssClass="datagrid"
                        Width="100%" NewLinkReturnToParentPageDisabled="True" QueryStringKey="BnbProject"
                        FieldName="tblProjectAudit_ProjectID" ViewName="vwProposalProgressAudit" GuidKey="tblProjectAudit_ProjectID"
                        ViewLinksVisible="false" NewLinkVisible="false" ViewLinksText="View"></bnbdatagrid:BnbDataGridForView>
                <br />                
                <p class="sectionHeading" style="padding: 3px; background-color: rgb(238,238,247);color: gray">
                <asp:Label id="Label3" runat="server">Proposal Risks</asp:Label>                   
                </p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView11" runat="server" CssClass="datagrid"
                        Width="100%" NewLinkReturnToParentPageDisabled="True" QueryStringKey="BnbProject"
                        FieldName="tblProjectAudit_ProjectID" ViewName="vwProposalRiskAudit" GuidKey="tblProjectAudit_ProjectID"
                        ViewLinksVisible="false" NewLinkVisible="false" ViewLinksText="View"></bnbdatagrid:BnbDataGridForView>
                <br />
                 <p class="sectionHeading" style="padding: 3px; background-color: rgb(238,238,247);color: gray">
                <asp:Label id="Label10" runat="server">Proposal Income Pipeline</asp:Label>                   
                </p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView6" runat="server" CssClass="datagrid"
                        Width="100%" NewLinkReturnToParentPageDisabled="True" QueryStringKey="BnbProject"
                        FieldName="tblProjectAudit_ProjectID" ViewName="vwProposalIncomePipelineAudit" GuidKey="tblProjectAudit_ProjectID"
                        ViewLinksVisible="false" NewLinkVisible="false" ViewLinksText="View"></bnbdatagrid:BnbDataGridForView>
                <br />
                <p class="sectionHeading" style="padding: 3px; background-color: rgb(238,238,247);color: gray">
                <asp:Label id="Label5" runat="server">Contracts General</asp:Label>                   
                </p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView12" runat="server" CssClass="datagrid"
                        Width="100%" NewLinkReturnToParentPageDisabled="True" QueryStringKey="BnbProject"
                        FieldName="tblProjectAudit_ProjectID" ViewName="vwContractGeneralAudit" GuidKey="tblProjectAudit_ProjectID"
                        ViewLinksVisible="false" NewLinkVisible="false" ViewLinksText="View"></bnbdatagrid:BnbDataGridForView>
                <br />
                <p class="sectionHeading" style="padding: 3px; background-color: rgb(238,238,247);color: gray">
                <asp:Label id="Label6" runat="server">Contracts Progress</asp:Label>                   
                </p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView13" runat="server" CssClass="datagrid"
                        Width="100%" NewLinkReturnToParentPageDisabled="True" QueryStringKey="BnbProject"
                        FieldName="tblProjectAudit_ProjectID" ViewName="vwContractProgressAudit" GuidKey="tblProjectAudit_ProjectID"
                        ViewLinksVisible="false" NewLinkVisible="false" ViewLinksText="View"></bnbdatagrid:BnbDataGridForView>
                <br />
                <p class="sectionHeading" style="padding: 3px; background-color: rgb(238,238,247);color: gray">
                <asp:Label id="Label7" runat="server">Contract Claim</asp:Label>                   
                </p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView14" runat="server" CssClass="datagrid"
                        Width="100%" NewLinkReturnToParentPageDisabled="True" QueryStringKey="BnbProject"
                        FieldName="tblProjectAudit_ProjectID" ViewName="vwContractClaimAudit" GuidKey="tblProjectAudit_ProjectID"
                        ViewLinksVisible="false" NewLinkVisible="false" ViewLinksText="View"></bnbdatagrid:BnbDataGridForView>
                <br />    
                <p class="sectionHeading" style="padding: 3px; background-color: rgb(238,238,247);color: gray">
                <asp:Label id="Label8" runat="server">Contract Report</asp:Label>                   
                </p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView15" runat="server" CssClass="datagrid"
                        Width="100%" NewLinkReturnToParentPageDisabled="True" QueryStringKey="BnbProject"
                        FieldName="tblProjectAudit_ProjectID" ViewName="vwContractReportAudit" GuidKey="tblProjectAudit_ProjectID"
                        ViewLinksVisible="false" NewLinkVisible="false" ViewLinksText="View"></bnbdatagrid:BnbDataGridForView>
                <br /> 
                <p class="sectionHeading" style="padding: 3px; background-color: rgb(238,238,247);color: gray">
                <asp:Label id="Label9" runat="server">Contract Report Due Date</asp:Label>                   
                </p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView16" runat="server" CssClass="datagrid"
                        Width="100%" NewLinkReturnToParentPageDisabled="True" QueryStringKey="BnbProject"
                        FieldName="tblProjectAudit_ProjectID" ViewName="vwContractReportDueDateAudit" GuidKey="tblProjectAudit_ProjectID"
                        ViewLinksVisible="false" NewLinkVisible="false" ViewLinksText="View"></bnbdatagrid:BnbDataGridForView>
                <br />                                                             
                </asp:Panel>   
              </div>         
			</asp:panel>
			
		</div>
		</form>
	</body>
</HTML>
