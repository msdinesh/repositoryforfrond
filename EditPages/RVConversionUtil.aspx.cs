using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboDomainObjects;
using BonoboEngine;
using BonoboEngine.Query;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for RVConversionUtil.
	/// </summary>
	public partial class RVConversionUtil : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			titleBar.TitleText ="RV Conversion";
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
			SetLabels();
			if(!Page.IsPostBack )
			{
                menuBar.SetOptionalFindParameter(10);
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				this.panelOne.Visible = false;
				BonoboWebControls.BnbWebFormManager.LogAction(titleBar.TitleText, null, this);
                uxVSOTypeLookup.SelectedValue = BnbConst.VSOGroup_VSO;
			}
            SetLabelsForVSOType();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void buttonImportRecords_Click(object sender, System.EventArgs e)
		{
			
			this.panelOne.Visible = true;
			try
			{
                int vsoGroupID = int.Parse(uxVSOTypeLookup.SelectedValue.ToString());
				// do a query to get the RVTracking records marked as ready
				BnbListCondition rvsts = new BnbListCondition("lkuRVRecordStatus_RVRecordStatusID", BnbConst.RVRecordStatus_Ready);
				BnbCriteria crit = new BnbCriteria(rvsts);
                crit.QueryElements.Add(new BnbListCondition("lkuVSOGroup_VSOGroupID",vsoGroupID));
				BnbQuery testQuery = new BnbQuery("vwBonoboFindRVTracking", crit);

				BnbQueryDataSet queryResult = testQuery.Execute();
				DataTable results = queryResult.Tables["Results"];

			
				// get the RVTrackingID values and put into an array of Guids
				Guid[] idArray = new Guid[results.Rows.Count];
				int i =0;
				foreach(DataRow rowLoop in results.Rows)
				{
					idArray[i] = (Guid)rowLoop["tblRVTracking_RVTrackingID"];
					i++;
				}

				// now create a new BnbRVConvertLog object
				BnbEditManager em = new BnbEditManager();
				BnbRVConvertLog newRVCL = new BnbRVConvertLog(true);
				newRVCL.RegisterForEdit(em);

				// now run the conversion job
				newRVCL.RunConversion(idArray, false);
				em.SaveChanges();
			

				// display the results
				
				lblRVCLRunDate.Text = newRVCL.RunDate.ToString() ;
				lblRVCLFailed.Text = newRVCL.RecordsFailed.ToString();
				lblRVCLTransfered.Text = newRVCL.RecordsTransferred.ToString();
				if (newRVCL.RunDetails !=null)
					txtRunDetails.Text = newRVCL.RunDetails.ToString();
				
				BonoboWebControls.BnbWebFormManager.LogAction(titleBar.TitleText + " (Batch Run)",
					String.Format("{0} records transferred, {1} records failed.",
						newRVCL.RecordsTransferred.ToString(),
						newRVCL.RecordsFailed.ToString()), this);
			}
			catch (ApplicationException catchAll)
			{
				lblErrors.Text = catchAll.Message.ToString();
			}
			
			catch (Exception ex)
			{
				lblErrors.Text = lblErrors.Text + ex.Message.ToString();
			}
		}

		private void SetLabels()
		{
			lblDraft.Text = QueryRVRecords(BnbConst.RVRecordStatus_Draft,-1).ToString();
			lblReady.Text = QueryRVRecords(BnbConst.RVRecordStatus_Ready,-1).ToString();
			lblFailed.Text = QueryRVRecords(BnbConst.RVRecordStatus_Failed,-1).ToString(); 
		}

        private void SetLabelsForVSOType()
        {
            int vsoGroupID = int.Parse(uxVSOTypeLookup.SelectedValue.ToString());
            lblDraftType.Text = QueryRVRecords(BnbConst.RVRecordStatus_Draft, vsoGroupID).ToString();
            lblReadyType.Text = QueryRVRecords(BnbConst.RVRecordStatus_Ready, vsoGroupID).ToString();
            lblFailedType.Text = QueryRVRecords(BnbConst.RVRecordStatus_Failed, vsoGroupID ).ToString();
        }

		private int QueryRVRecords(int rvRecordStatus, int vsoGroupID)
		{
			try
			{
				BnbListCondition rvsts = new BnbListCondition("tblRVTracking_RVRecordStatusID", rvRecordStatus);
				BnbCriteria crit = new BnbCriteria(rvsts);
                // if vsotypeid specified, add another criteria
                if (vsoGroupID > -1)
                    crit.QueryElements.Add(new BnbListCondition("lkuVSOGroup_VSOGroupID", vsoGroupID));
                BnbQuery testQuery = new BnbQuery("vwBonoboRVTrackingTotals", crit);


				BnbQueryDataSet queryResult = testQuery.Execute();
				DataTable results = queryResult.Tables["Results"];
                // sum over result set, in some cases several rows returned
                int total = 0;
                foreach (DataRow rowLoop in results.Rows)
                    total += (int)rowLoop["Custom_Count"];
				return total;
			}
			catch( ApplicationException catchAll)
			{
				lblErrors.Text = catchAll.Message.ToString();
				return 0;
			}
			
		}

		protected void lnkNewRVTracking_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("./WizardNewRVTracking.aspx");
		}

		protected void buttonCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("../FindGeneral.aspx");
		}
		
	}
}
