using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboWebControls;

namespace Frond.EditPages
{
    public partial class XmlImportDetailsPage : System.Web.UI.Page
    {

        private BnbWebFormManager bnb = null;

        #region " Page_Load "

        protected void Page_Load(object sender, EventArgs e)
        {
            bnb = new BnbWebFormManager(this);
            BnbWorkareaManager.SetPageStyleOfUser(this);
            if (!this.IsPostBack)
            {
                titleBar.TitleText = "Xml Import Details";
                lblRowsForImport.Text = Page.Request.QueryString["TotalCount"].ToString();
                lblSuccessRows.Text = Page.Request.QueryString["Success"].ToString();
                lblFailedRows.Text = Page.Request.QueryString["Failure"].ToString();

            }
        }

        #endregion

        #region " Cancel_Click "

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("XmlImportPage.aspx");
        }

        #endregion
    }
}
