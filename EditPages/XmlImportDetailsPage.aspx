<%@ Page Language="C#" AutoEventWireup="true" Codebehind="XmlImportDetailsPage.aspx.cs"
    Inherits="Frond.EditPages.XmlImportDetailsPage" %>

<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.DataGrids"
    TagPrefix="bnbdatagrid" %>
<%@ Register Assembly="BonoboWebControls" Namespace="BonoboWebControls.DataControls"
    TagPrefix="bnbdatacontrol" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Xml Import Details</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="form1" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" runat="server" ImageSource="../Images/batchprocess24silver.gif">
            </uc1:Title>
            <div style="float: left">
                <asp:Panel ID="panelXmlImportDetails" runat="server" Visible="true">
                    <table class="fieldtable" id="tblDetails" width="100%" border="0">
                        <tr>
                            <td width="50%" class="label">
                                No of rows for import &nbsp;
                            </td>
                            <td style="width: 50%">
                                <asp:Label ID="lblRowsForImport" runat="server" Text="Label"></asp:Label></td>
                        </tr>
                        <tr>
                            <td width="50%" class="label">
                                No of rows successfully import &nbsp;
                            </td>
                            <td style="width: 50%">
                                <asp:Label ID="lblSuccessRows" runat="server" Text="Label"></asp:Label></td>
                        </tr>
                        <tr>
                            <td width="50%" class="label">
                                No of rows failed &nbsp;
                            </td>
                            <td style="width: 50%">
                                <asp:Label ID="lblFailedRows" runat="server" Text="Label"></asp:Label></td>
                        </tr>
                    </table>
                    <bnbdatagrid:BnbDataGridForView ID="BnbAuditView" runat="server" CssClass="datagrid"
                        Width="100%" DomainObject="BnbXmlImportAuditItem" FieldName="tblXmlImportAuditItem_XmlImportAuditHeaderID"
                        QueryStringKey="BnbXmlImportAuditHeader" RedirectPage="XmlImportPage.aspx" ViewLinksVisible="false"
                        ViewName="vwXmlImportAudit" NewLinkVisible="false" GuidKey="lnkPositionWebText_PositionWebTextID"
                        ReturnToHostedPage="True" ColumnNames="tblXmlImportAuditItem_ImportRowNo,tblXmlImportAuditItem_ImportDetails"
                        DisplayNoResultMessageOnly="false" DisplayResultCount="false" ShowResultsOnNewButtonClick="false"
                        ViewLinksText="View"></bnbdatagrid:BnbDataGridForView>
                </asp:Panel>
                <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel" /></div>
        </div>
    </form>
</body>
</html>
