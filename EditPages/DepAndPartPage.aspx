<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="BonoboWebControls.ServicesControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Page language="c#" Codebehind="DepAndPartPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.DepAndPartPage" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Dependants and Partners</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/bnbindividual24silver.gif"></uc1:title>
				<uc1:NavigationBar id="NavigationBar1" runat="server"></uc1:NavigationBar>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" UndoText="Undo" SaveText="Save"
					EditAllText="Edit All" NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px"
					NewWidth="55px" NewButtonEnabled="False"></bnbpagecontrol:bnbdatabuttons>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:bnbmessagebox>
				<p>
					<TABLE class="fieldtable" id="Table1" width="100%" border="0">
						<TR>
							<TD class="label" style="HEIGHT: 20px" width="10%">Link From</TD>
							<TD style="HEIGHT: 20px" width="40%">
								<bnbgenericcontrols:BnbHyperlink id="uxRelativeFromHyper" runat="server"></bnbgenericcontrols:BnbHyperlink>
							</TD>
							<TD class="label" style="HEIGHT: 20px" width="10%">To</TD>
							<TD style="HEIGHT: 20px" width="40%">
								<bnbgenericcontrols:BnbHyperlink id="uxRelativeToHyper" runat="server"></bnbgenericcontrols:BnbHyperlink>
							</TD>
						</TR>
					</TABLE>
					<TABLE class="fieldtable" id="Table2" width="100%" border="0">
						<TR>
							<TD class="label" style="HEIGHT: 20px" width="50%">Forenames</TD>
							<TD style="HEIGHT: 20px" width="25%">
								<bnbdatacontrol:BnbTextBox id="uxForename" runat="server" CssClass="textbox" Width="250px" Height="20px" ControlType="Text"
									StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
							</TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 20px" width="50%">
								<asp:Label id="Label1" runat="server" Visible="False">Develper note: these fields<br /> are bound in code</asp:Label>Surname</TD>
							<TD style="HEIGHT: 20px" width="25%">
								<bnbdatacontrol:BnbTextBox id="uxSurname" runat="server" CssClass="textbox" Width="250px" Height="20px" ControlType="Text"
									StringLength="0" Rows="1" MultiLine="false"></bnbdatacontrol:BnbTextBox>
							</TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 20px" width="50%">Date of Birth</TD>
							<TD style="HEIGHT: 20px" width="25%">
								<bnbdatacontrol:BnbDateBox id="uxDOB" runat="server" CssClass="datebox" Width="90px" Height="20px" DateCulture="en-GB"
									DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbDateBox>
								<asp:Label id="uxDOBLabel" runat="server" Visible="False"></asp:Label>
							</TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 20px" width="50%">Age</TD>
							<TD style="HEIGHT: 20px" width="25%">
								<bnbdatacontrol:BnbDecimalBox id="uxAge" runat="server" CssClass="textbox" Height="20px" Width="88px" TextAlign="left"
									FormatExpression="#,0"></bnbdatacontrol:BnbDecimalBox></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 20px" width="50%">Relationship type</TD>
							<TD style="HEIGHT: 20px" width="50%">
								<bnbdatacontrol:BnbLookupControl id="uxRelationshipType" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									LookupTableName="lkuRelativeType" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" DataMember="BnbRelative.RelationshipTypeID"></bnbdatacontrol:BnbLookupControl>
							</TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 20px" width="50%">Intended Location</TD>
							<TD style="HEIGHT: 20px" width="50%">
								<bnbdatacontrol:BnbLookupControl id="uxIntendedLocation" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									LookupTableName="lkuRelativeApplicationLocation" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"
									DataMember="BnbRelative.MostRecentRelativeApplication.IntendedLocationID"></bnbdatacontrol:BnbLookupControl>
							</TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 20px" width="50%">Actual Location</TD>
							<TD style="HEIGHT: 20px" width="50%">
								<bnbdatacontrol:BnbLookupControl id="uxActualLocation" runat="server" CssClass="lookupcontrol" Width="250px" Height="22px"
									LookupTableName="lkuRelativeApplicationLocation" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"
									DataMember="BnbRelative.MostRecentRelativeApplication.ActualLocationID"></bnbdatacontrol:BnbLookupControl>
							</TD>
						</TR>
					</TABLE>
				</p>
			</div>
		</form>
	</body>
</HTML>
