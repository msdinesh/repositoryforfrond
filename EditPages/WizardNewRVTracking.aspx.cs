using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using System.Globalization;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for WizardNewRVTracking.
	/// </summary>
	public partial class WizardNewRVTracking : System.Web.UI.Page
	{
		
		
		protected UserControls.WizardButtons wizardButtons;
		protected int HiddenRVRecordTypeID;
		protected Guid HiddenRVTrackingID;




		/// <summary>
		/// Returns the RVRecordTypeID of RV selected by the user!
		/// </summary>
		public Guid RVTrackingID
		{
			get{return HiddenRVTrackingID;}
			set{HiddenRVTrackingID = value;}
		}

		/// <summary>
		/// Returns the RVRecordType selected by the user!
		/// </summary>
		public string RVRecordType
		{
			get
			{
				return lblHiddenRVRecordType.Text.ToString(); 
			}
			set{lblHiddenRVRecordType.Text = value.ToString();}
		}



		/// <summary>
		/// Returns the RVRecordTypeID of RV selected by the user!
		/// </summary>
		public int RVRecordTypeID
		{
			get
			{
				if (lblHiddenRVRecordTypeID.Text =="")
					return 0;
				else
				return Convert.ToInt32(lblHiddenRVRecordTypeID.Text);
			}
			set{lblHiddenRVRecordTypeID.Text = value.ToString();}
		}


	
		/// <summary>
		/// Returns the ApplicationId the funding (may be) attached to
		/// may also return Guid.Empty if no Application yet (i.e. and unfilled placement)
		/// </summary>
		public Guid ApplicationID
		{
			get
			{
				if (lblHiddenApplicationID.Text == "")
					return Guid.Empty;
				else
					return new Guid(lblHiddenApplicationID.Text );
			}
			set{lblHiddenApplicationID.Text = value.ToString();}
		}


		protected void Page_Load(object sender, System.EventArgs e)
		{
			BnbEngine.SessionManager.ClearObjectCache();
			
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);

			wizardButtons.AddPanel(panelZero);
			wizardButtons.AddPanel(panelOne);
			wizardButtons.AddPanel(panelTwo);

			if (!this.IsPostBack)
			{
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				initWizard();
				BonoboWebControls.BnbWebFormManager.LogAction("New RV Tracking Wizard", null, this);
			}
		}

		public void SetupPanelZero()
		{
			initWizard();
		}

		
		public void initWizard()
		{
			// rvrecordtype lookup on panel zero
			BnbLookupDataTable rvrecordtypeLookup = BnbEngine.LookupManager.GetLookup("lkuRVRecordType");
			DataView rvrecordtypeView = new DataView(rvrecordtypeLookup);
			rvrecordtypeView.RowFilter = "Exclude = 0";
			rvrecordtypeView.Sort = "Description";
			dropDownRVRecordType.DataSource = rvrecordtypeView;
			
			dropDownRVRecordType.DataValueField = "IntID";
			dropDownRVRecordType.DataTextField = "Description";
			dropDownRVRecordType.DataBind();
					
			ListItem blankItem = new ListItem("<Please Select!>","0");
			dropDownRVRecordType.Items.Insert(0,blankItem);
			
			//set new RV
			if(Request.QueryString["BnbRVTracking"]=="new")
			{
				ListItem defaultItem = dropDownRVRecordType.Items.FindByValue(BnbConst.RVRecordType_NewRV.ToString());
				if (defaultItem != null)
					defaultItem.Selected = true;
			}
		}

		public void SetupPanelOne()
		{
			this.RVRecordType = dropDownRVRecordType.SelectedItem.Text.ToString(); 
			this.RVRecordTypeID = Convert.ToInt32(dropDownRVRecordType.SelectedValue.ToString());
		}

		public void SetupPanelTwo()
		{
			if (this.RVRecordType == "")
			{
				this.RVRecordType = dropDownRVRecordType.SelectedItem.Text.ToString(); 
				this.RVRecordTypeID = Convert.ToInt32(dropDownRVRecordType.SelectedValue.ToString());
			}
			if(this.RVRecordTypeID == BnbConst.RVRecordType_EditRV || this.RVRecordTypeID == BnbConst.RVRecordType_NewService)
			{
				System.Guid applicationID = bdgRV.SelectedDataRowID;

				BnbApplication bnbApp = BnbApplication.Retrieve(applicationID); 
				this.ApplicationID = bnbApp.ID; 
				txtSurName.Text = bnbApp.Individual.Surname;
				txtForeName.Text = bnbApp.Individual.Forename;
				lblVolRef.Text = bnbApp.Individual.Additional.ReferenceNo; 
				lblRVStatus.Text = this.RVRecordType;
				if(this.RVRecordTypeID == BnbConst.RVRecordType_NewService)
				{
					txtSurName.Enabled = false;
					txtForeName.Enabled = false;						
				}
			}
		
			if(this.RVRecordTypeID == BnbConst.RVRecordType_NewRV)
			{
				txtSurName.Enabled = true;
				txtForeName.Enabled = true;
				lblRVStatus.Text = this.RVRecordType;
			}
				
		}

		

		private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
		{
			if (e.CurrentPanel == 0)
			{
				if (Convert.ToInt32(dropDownRVRecordType.SelectedValue.ToString())==0)
				{
					lblPanelZeroFeedback.Text = "Please select RV Record type from the drop down list before pressing Next";
					e.Proceed  = false;
				}
				if (Convert.ToInt32(dropDownRVRecordType.SelectedValue.ToString())==BnbConst.RVRecordType_NewRV)
				{
					this.wizardButtons.CurrentPanel =e.CurrentPanel +1;
				}
				
			}
		
			if (e.CurrentPanel == 1)
			{
				if ((RVRecordTypeID  == BnbConst.RVRecordType_EditRV || RVRecordTypeID == BnbConst.RVRecordType_NewService) && bdgRV.SelectedDataRowID == Guid.Empty)
				{
					lblPanelOneFeedback.Text = "Please use the Find and select an RV Row before pressing Next";
					e.Proceed = false;
				}
				
				
			}
			
			
		}

		private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			if (e.CurrentPanel == 0)
			{
				this.SetupPanelZero();
			}
			
			if (e.CurrentPanel == 1)
			{
				this.SetupPanelOne();
			}
			if (e.CurrentPanel == 2)
			{
				this.SetupPanelTwo();
			}
		}

		private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			Response.Redirect("../FindGeneral.aspx");
		}

		private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			this.CreateRV();
			if(lblHiddenRVTrackingID.Text !="")
			{
				Guid id = new Guid(lblHiddenRVTrackingID.Text.ToString());
				RedirectToRVTrackingPage(id);

			}
		}

		/// <summary>
		/// Creates RVTracking record
		/// </summary>
		private void CreateRV()
		{
			BnbEditManager em = new BnbEditManager();
			BnbRVTracking newRV = new BnbRVTracking(true);
			newRV.RegisterForEdit(em);
			
			newRV.RVRecordTypeID = this.RVRecordTypeID;
			newRV.RVRecordStatusID = BnbConst.RVRecordStatus_Draft;

			if(this.RVRecordTypeID == BnbConst.RVRecordType_NewRV)
			{
				newRV.Surname = txtSurName.Text;
				newRV.Forename = txtForeName.Text;
			}
					
			if(this.RVRecordTypeID == BnbConst.RVRecordType_EditRV || this.RVRecordTypeID == BnbConst.RVRecordType_NewService )
			{
				BnbApplication bnbApp =  BnbApplication.Retrieve(this.ApplicationID);
				if(this.RVRecordTypeID == BnbConst.RVRecordType_EditRV)
					newRV.ApplicationID = bnbApp.ID;
				
				newRV.VolReferenceNo = bnbApp.Individual.Additional.ReferenceNo;
									
				BnbIndividual bnbInd = bnbApp.Individual;
				newRV.IndividualID  = bnbInd.ID;
				newRV.VolReferenceNo = bnbInd.Old_indv_id;
				newRV.Forename = bnbInd.Forename;
				newRV.Surname = bnbInd.Surname;
				newRV.SurnamePrefix = bnbInd.SurnamePrefix;
				newRV.Title = bnbInd.Title;
				newRV.Gender = bnbInd.Gender;
				newRV.DateOfBirth = bnbInd.DateOfBirth;
				newRV.DateOfDeath = bnbInd.Additional.DateOfDeath;
				newRV.OccupationTypeID = bnbInd.Additional.OccupationTypeID;
				newRV.OccupationRoleID = bnbInd.Additional.OccupationRoleID;
				newRV.RVComments = bnbInd.Additional.RVComments;
				newRV.SkillID = bnbInd.LeadSkill.SkillID;
				newRV.EthnicityID = bnbInd.Additional.EthnicityID;
				newRV.EthnicityOther = bnbInd.Additional.EthnicityOther;
				newRV.AddressLine1 = bnbInd.CurrentIndividualAddress.Address.AddressLine1;   
				newRV.AddressLine2 = bnbInd.CurrentIndividualAddress.Address.AddressLine2;
				newRV.AddressLine3 = bnbInd.CurrentIndividualAddress.Address.AddressLine3;
				newRV.AddressLine4 = bnbInd.CurrentIndividualAddress.Address.AddressLine4;
				newRV.County =bnbInd.CurrentIndividualAddress.Address.County; 
				newRV.Postcode =bnbInd.CurrentIndividualAddress.Address.PostCode;
				newRV.AddressCountryID =bnbInd.CurrentIndividualAddress.Address.CountryID;
				newRV.Telephone = bnbInd.CurrentIndividualAddress.Address.GetTelephone(bnbInd);
				newRV.Fax = bnbInd.CurrentIndividualAddress.Address.GetFax(bnbInd);
				newRV.AddressID = bnbInd.CurrentIndividualAddress.Address.ID;
				
				if(bnbApp.PlacedService!=null)
				{
					BnbEmployer emp = BnbEmployer.Retrieve(bnbApp.PlacedService.Position.Role.EmployerID);
					newRV.CountryID  = emp.CountryID;
				}

				if(bnbInd.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Email)!=null)
					newRV.Email = bnbInd.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Email).ContactNumber;
				if(bnbInd.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Mobile)!=null)
					newRV.Mobile = bnbInd.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Mobile).ContactNumber;
				if(bnbInd.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_WorkTelephone)!=null)
                   newRV.WorkTelephone = bnbInd.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_WorkTelephone).ContactNumber;
				
				SetNationality(bnbInd,newRV,em);
				SetAddressDistribution(bnbInd,newRV,em);

				
				if(bnbInd.CurrentContactBlock != null)
				{
					if(bnbInd.CurrentContactBlock.ContactBlockTypeID != 1000)
								 newRV.MailBlock =true;
					else
					newRV.MailBlock =false;
				}
				else
					newRV.MailBlock =false;

				if(this.RVRecordTypeID == BnbConst.RVRecordType_EditRV)
				{
					newRV.SOSDate = bnbApp.SOSDate;
					newRV.EOSDate = bnbApp.EOSDate;
					newRV.StatusID = bnbApp.CurrentStatus.StatusID;
					if(bnbApp.PlacedService!=null)
					{
						newRV.ServiceID = bnbApp.PlacedService.ID;
						newRV.PositionID = bnbApp.PlacedService.Position.ID;
						newRV.RoleID = bnbApp.PlacedService.Position.RoleID;
						newRV.EmployerID = bnbApp.PlacedService.Position.Role.EmployerID; 
					}
						
						
						
				}
			}
	
			try
			{
				em.SaveChanges();
				lblHiddenRVTrackingID.Text = newRV.ID.ToString();
				
			}
			catch(BnbProblemException pe)
			{
				lblPanelTwoFeedback.Text = lblPanelTwoFeedback.Text + "Unfortunately, the RV record could not be created due to the following problems: <br/>";
				foreach(BnbProblem p in pe.Problems)
					lblPanelTwoFeedback.Text += p.Message + "<br/>";

			}
			
		}

		
		/// <summary>
		/// Redirects to the RVTracking page with relevant RVTrackingID
		/// </summary>
		/// <param name="rvTrackingID"></param>
		private void RedirectToRVTrackingPage(Guid rvTrackingID)
		{
			string reDirectURL;
			if (rvTrackingID != Guid.Empty)
			{
				reDirectURL = String.Format("EditPages/RVTrackingPage.aspx?BnbRVTracking={0}",rvTrackingID);
				Response.Redirect("~\\" + reDirectURL);
			}
		}
		
		/// <summary>
		/// Copies nationlity from BnbIndividual object to the BnbRVTracking objects passed in
		/// </summary>
		/// <param name="sourceIndividual"></param>
		/// <param name="destRVInfo"></param>
		/// <param name="em"></param>
		private static void SetNationality(BnbIndividual sourceIndividual,BnbRVTracking destRVInfo, BnbEditManager em)
		{
				while(destRVInfo.Nationalities.Count > 0)
				{
					BnbIndividualNationality ob = (BnbIndividualNationality )destRVInfo.Nationalities[0];
					ob.RegisterForEdit(em);
					destRVInfo.Nationalities.RemoveAndDelete(ob);
				}

				foreach(BnbIndividualNationality ob in sourceIndividual.IndividualNationalities)
				{
					BnbRVTrackingNationality newRVNationality = new BnbRVTrackingNationality(true);   
					newRVNationality.RegisterForEdit(em);
					destRVInfo.Nationalities.Add(newRVNationality);
					newRVNationality.NationalityID = ob.NationalityID; 
					newRVNationality.NationalityOrder = ob.NationalityOrder;
				}
		}


		/// <summary>
		/// Extracts AddressDistributions info from the specified BnbIndividual object creates new BnbIndividualAddressDistribution objects and adds to the BnbRVTracking  object passed in 
		/// After calling, the new objects can be retrieved from the BnbEditManager.
		/// </summary>
		/// <param name="sourceRVInfo"></param>
		/// <param name="destIndividual"></param> 
		/// <param name="em"></param>
		private static void SetAddressDistribution(BnbIndividual sourceIndividual,BnbRVTracking destRVInfo,BnbEditManager em)
		{
			foreach(BnbIndividualAddressDistribution ob in sourceIndividual.IndividualAddressDistributions)
			{
				BnbRVTrackingAddressDistribution newRVAddrsDistrb = new BnbRVTrackingAddressDistribution(true);
				newRVAddrsDistrb.RegisterForEdit(em);
				destRVInfo.AddressDistributions.Add(newRVAddrsDistrb);
				newRVAddrsDistrb.AddressDistributionID = ob.AddressDistributionID; 
				newRVAddrsDistrb.Comments  = ob.Comments;
			}

		}


		private void ShowVolDetails()
		{
			BnbApplication bnbApp =  BnbApplication.Retrieve(this.ApplicationID);
			
			lblVolRef.Text = bnbApp.Individual.Additional.ReferenceNo; 
			lblRVStatus.Text = this.RVRecordTypeID.ToString();
			if(this.RVRecordTypeID == BnbConst.RVRecordType_NewRV)
			{
				txtSurName.Enabled = true;
				txtForeName.Enabled = true;
			}
			if(this.RVRecordTypeID == BnbConst.RVRecordType_EditRV  || this.RVRecordTypeID ==BnbConst.RVRecordType_NewService )
			{
				txtSurName.Enabled = false;
				txtForeName.Enabled = false;
				txtSurName.Text = bnbApp.Individual.Surname;
				txtForeName.Text = bnbApp.Individual.Forename;
			}
			 

		}


		private void FindRV()
		{
			lblPanelOneFeedback.Text = "";
			// build criteria
			BnbCriteria findRVCriteria = new BnbCriteria();
			
			if (txtFindRefNo.Text != "")
				findRVCriteria.QueryElements.Add(new BnbTextCondition("tblIndividualKeyInfo_old_indv_id", 
					txtFindRefNo.Text, BnbTextCompareOptions.ExactMatch));

			if (txtFindSurname.Text != "")
				findRVCriteria.QueryElements.Add(new BnbTextCondition("tblIndividualKeyInfo_Surname", 
					txtFindSurname.Text, BnbTextCompareOptions.StartOfField));

			if (txtFindForename.Text != "")
				findRVCriteria.QueryElements.Add(new BnbTextCondition("tblIndividualKeyInfo_Forename", 
					txtFindForename.Text, BnbTextCompareOptions.StartOfField));

			if (System.DateTime.Parse(BnbDateBox1.Text, CultureInfo.InvariantCulture) != System.DateTime.MinValue)
				findRVCriteria.QueryElements.Add(new BnbDateCondition("tblApplication_EOSDate", 
					System.DateTime.Parse(BnbDateBox1.Text, CultureInfo.InvariantCulture), BnbDateCompareOptions.FieldFromDate));
			
			if (System.DateTime.Parse(BnbDateBox2.Text, CultureInfo.InvariantCulture) != System.DateTime.MinValue)
				findRVCriteria.QueryElements.Add(new BnbDateCondition("tblApplication_EOSDate", 
					System.DateTime.Parse(BnbDateBox2.Text, CultureInfo.InvariantCulture), BnbDateCompareOptions.FieldToDate));
			
			// so far, if we dont have any criteria, then reject
			if (findRVCriteria.QueryElements.Count == 0)
			{
				lblPanelOneFeedback.Text = "Please enter a Ref No, Surname, Forename or Postcode before pressing Find";
				return;
			}

			// country might not be specified
			if (BnbLookupControl1.SelectedValue.ToString()!= "00000000-0000-0000-0000-000000000000")
				findRVCriteria.QueryElements.Add(new BnbListCondition("tblEmployer_CountryID", 
					new Guid(BnbLookupControl1.SelectedValue.ToString()  )));
				
			// supply criteria to grid
			bdgRV.Criteria = findRVCriteria;
			bdgRV.MaxRows = 20;
			bdgRV.Visible = true;
			bdgRV.DataBind();
			
			lblSearchHint.Visible = true;
		}

		protected void cmdIndivFind_Click(object sender, System.EventArgs e)
		{
			FindRV();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			wizardButtons.ShowPanel +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
			wizardButtons.ValidatePanel +=new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
			wizardButtons.CancelWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
			wizardButtons.FinishWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);

		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		
	}
}
