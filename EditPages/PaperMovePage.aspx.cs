using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboDomainObjects;
using BonoboWebControls;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for PaperMove.
	/// </summary>
	public partial class PaperMovePage : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
		protected UserControls.NavigationBar NavigationBar1;
		protected void Page_Load(object sender, System.EventArgs e)
		{
			BnbWebFormManager bnb = null;
#if DEBUG	
			if (Request.QueryString.ToString() == "")
				BnbWebFormManager.ReloadPage("BnbPaperMove=E69089E3-9369-4B34-0B9E-3844BFF0C480");
			else
				bnb = new BnbWebFormManager(this,"BnbPaperMove");
#else	
			bnb = new BnbWebFormManager(this,"BnbPaperMove");
#endif
			bnb.LogPageHistory = true;
			BnbWorkareaManager.SetPageStyleOfUser(this);
			// tweak page title settings
			bnb.PageNameOverride = "Paper Move";

			NavigationBar1.RootDomainObject = "BnbApplication";
			if (!this.IsPostBack)
			{
				titleBar.TitleText = "Paper Move";
			}
		
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
