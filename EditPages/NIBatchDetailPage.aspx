<%@ Page language="c#" Codebehind="NIBatchDetailPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.NIBatchDetailPage" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/batchprocess24silver.gif"></uc1:title>
				<uc1:NavigationBar id="NavigationBar1" runat="server"></uc1:NavigationBar>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" CssClass="databuttons" UndoText="Undo" SaveText="Save"
					EditAllText="Edit All" NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px"
					NewWidth="55px" NewButtonEnabled="True" NewButtonVisible="False"></bnbpagecontrol:bnbdatabuttons>
				<bnbpagecontrol:bnbmessagebox id="BnbMessageBox1" runat="server" CssClass="messagebox"></bnbpagecontrol:bnbmessagebox>
				<TABLE class="fieldtable" id="Table1" style="WIDTH: 408px; HEIGHT: 212px" border="0">
					<TR>
						<TD class="label" style="WIDTH: 98px; HEIGHT: 16px" width="98">Ref No</TD>
						<TD style="WIDTH: 185px; HEIGHT: 16px" width="185">
							<bnbdatacontrol:BnbTextBox id="BnbTextBox2" runat="server" Width="54px" Height="8px" CssClass="textbox" DataMember="BnbNIBatchDetail.Application.Individual.RefNo"
								StringLength="0" Rows="1" MultiLine="false" ControlType="Text"></bnbdatacontrol:BnbTextBox></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 98px; HEIGHT: 19px" width="98">Name</TD>
						<TD style="WIDTH: 185px; HEIGHT: 19px" width="185">
							<bnbdatacontrol:BnbTextBox id="BnbTextBox1" runat="server" Width="54px" Height="20px" CssClass="textbox" DataMember="BnbNIBatchDetail.Application.Individual.FullName"
								StringLength="0" Rows="1" MultiLine="false" ControlType="Text"></bnbdatacontrol:BnbTextBox></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 98px; HEIGHT: 16px" width="98">Include</TD>
						<TD style="WIDTH: 185px; HEIGHT: 16px" width="185">
							<bnbdatacontrol:BnbCheckBox id="BnbCheckBox1" runat="server" CssClass="checkbox" DataMember="BnbNIBatchDetail.Include"></bnbdatacontrol:BnbCheckBox></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 98px" width="98">Reasons</TD>
						<TD style="WIDTH: 185px" width="185">
							<bnbdatagrid:BnbDataGridForDomainObjectList id="BnbDataGridForDomainObjectList1" runat="server" Width="208px" CssClass="datagrid"
								DataMember="BnbNIBatchDetail.NIBatchDetailReasons" SortBy="ID" VisibleOnNewParentRecord="false" AddButtonVisible="true"
								EditButtonText="Edit" AddButtonText="Add" EditButtonVisible="true" DeleteButtonVisible="false" DataGridForDomainObjectListType="Editable"
								DataProperties="BenefitBatchReasonID[BnbLookupControl:LkuBenefitBatchReason]" HideColumnHeaders="True"></bnbdatagrid:BnbDataGridForDomainObjectList></TD>
					</TR>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
