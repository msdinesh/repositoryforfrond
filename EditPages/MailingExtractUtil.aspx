<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="MailingExtractUtil.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.MailingExtractUtil" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Mailing Extract Utility</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<script>

var browserType;

if (document.layers) {browserType = "nn4"}
if (document.all) {browserType = "ie"}
if (window.navigator.userAgent.toLowerCase().match("gecko")) {
   browserType= "gecko"
}

function hidePostbox() {
  document.body.style.cursor = 'default';
  if (browserType == "gecko" )
     document.poppedLayer = 
         eval('document.getElementById("inprogress")');
  else if (browserType == "ie")
     document.poppedLayer = 
        eval('document.getElementById("inprogress")');
  else
     document.poppedLayer =   
        eval('document.layers["inprogress"]');
  document.poppedLayer.style.visibility = "hidden";
}

function showPostbox() {
  document.body.style.cursor = 'wait';
  if (browserType == "gecko" )
     document.poppedLayer = 
         eval('document.getElementById("inprogress")');
  else if (browserType == "ie")
     document.poppedLayer = 
        eval('document.getElementById("inprogress")');
  else
     document.poppedLayer = 
         eval('document.layers["inprogress"]');
  document.poppedLayer.style.visibility = "visible";
  // reload image on timer to make animation work
  setTimeout('document.images["imgInProgress"].src = "../Images/mailextractinprogress.gif"', 200); 
}

		</script>
	</HEAD>
	<body onload="javascript:hidePostbox();">
		<form id="Form2" method="post" runat="server" onSubmit="imgInProgress.src=imgInProgress.src;return true;">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server"  imagesource="../Images/batchprocess24silver.gif"></uc1:title>
				<table width="70%" align="center">
					<tr>
						<td align="center"><asp:imagebutton id="imageGoButton" runat="server" Height="100px"></asp:imagebutton></td>
					</tr>
					<tr>
						<td align="center">
							<p><asp:label id="lblInstructions" runat="server">Please click on the postbox to start the mailing extract for the</asp:label></p>
						</td>
					</tr>
					<tr>
						<td align="center">
							<p><asp:label id="lblResultsMessage" runat="server"></asp:label></p>
						</td>
					</tr>
					<tr>
						<td align="center">
							<div id="inprogress"><asp:image id="imgInProgress" runat="server" ImageUrl="../Images/mailextractinprogress.gif"></asp:image></div>
						</td>
					</tr>
					<tr>
						<TD><asp:panel id="afterpanel" runat="server">
								<TABLE class="fieldtable" width="100%">
									<TR>
										<TD class="label">
											<asp:label id="Label3" runat="server" Width="456px">Mailing extract created on</asp:label></TD>
										<TD>
											<bnbdatacontrol:bnbstandalonedatebox id="dbRunOn" runat="server" Height="20px" Width="90px" ReadOnly="True" DateFormat="dd/MMM/yyyy"
												DateCulture="en-GB" CssClass="datebox" Enabled="False"></bnbdatacontrol:bnbstandalonedatebox></TD>
									</TR>
									<TR>
										<TD class="label">
											<asp:label id="Label5" runat="server">By</asp:label></TD>
										<TD>
											<asp:label id="lblRunBy" runat="server"></asp:label></TD>
									</TR>
									<TR>
										<TD class="label">
											<asp:label id="Label7" runat="server">Department</asp:label></TD>
										<TD>
											<asp:label id="lblRunDepartment" runat="server"></asp:label></TD>
									</TR>
									<TR>
										<TD class="label">
											<asp:label id="Label6" runat="server">Reference</asp:label></TD>
										<TD>
											<asp:label id="lblMailingReference" runat="server"></asp:label></TD>
									</TR>
									<TR>
										<TD class="label">
											<asp:label id="Label8" runat="server">Type</asp:label></TD>
										<TD>
											<asp:label id="lblMailingType" runat="server"></asp:label></TD>
									</TR>
									<TR>
										<TD class="label">
											<asp:label id="Label9" runat="server">Number of records</asp:label></TD>
										<TD>
											<asp:label id="lblNumberRecords" runat="server"></asp:label></TD>
									</TR>
									<TR>
										<TD align="center" colSpan="4">
											<asp:label id="lblEmailSent" runat="server">An email containing the mailing extract files has been sent to you.<br><br>If you do not receive this email, then you may download the file by clicking the button below relating to the format that you need. Once the file is open, you can save it by clicking on Save in your browser.</asp:label>
											<asp:label id="lblFromFind" runat="server">To get a copy of this mailing extract, click on the button relating to the format you need it in. You can then save the file by clicking on Save in your browser.</asp:label><BR>
										</TD>
									</TR>
									<TR>
										<TD align="center" colSpan="4">
											<asp:button id="btnXML" runat="server" Text="XML"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
											<asp:button id="btnTXT" runat="server" Text="TXT"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
											<asp:button id="btnCSV" runat="server" Text="CSV"></asp:button></TD>
									</TR>
								</TABLE>
							</asp:panel>
						</TD>
					</tr>
				</table>
			</div>
		</form>
	</body>
</HTML>
