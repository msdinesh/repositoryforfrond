<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Page language="c#" Codebehind="WizardNewDP.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.WizardNewDP" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>New Dependants and Partners Wizard</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<DIV class="wizardheader" ms_positioning="FlowLayout">
					<H3><IMG class="icon24" src="../Images/wizard24silver.gif"> New Dependants and 
						Partners Wizard</H3>
				</DIV>
				<asp:panel id="panelZero" runat="server" CssClass="wizardpanel">
					<P><EM>This wizard will help you to add a new&nbsp;Dependant or Partner.</EM></P>
					<P><EM>If the Dependant or Partner is already in the database, then the wizard will 
							help you find the record and link to it. If they are not already in the 
							database, a new Dependant/Partner Individual record will be created.</EM></P>
					<P>Please enter the Ref No or Forename, Surname and Date of birth of the dependent 
						relative or partner and press Next</P>
					<P></P>
					<P>
						<TABLE class="wizardtable" id="Table2">
							<TR>
								<TD class="label" style="HEIGHT: 6px">Dependant/Partner For</TD>
								<TD>
									<asp:Label id="uxIndividualSummaryLabel1" runat="server"></asp:Label></TD>
							</TR>
							<TR>
								<TD class="label" style="HEIGHT: 6px">Forename</TD>
								<TD>
									<asp:TextBox id="txtFirstname" runat="server" Width="240px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD class="label">Surname</TD>
								<TD>
									<asp:TextBox id="txtSurname" runat="server" Width="240px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD class="label">Ref No</TD>
								<TD>
									<asp:TextBox id="txtRefNo" runat="server"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD class="label">Date of birth</TD>
								<TD>
									<bnbdatacontrol:BnbStandaloneDateBox id="dbDateOfBirth" runat="server" CssClass="datebox" Width="90px" Height="20px"
										DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbStandaloneDateBox></TD>
							</TR>
						</TABLE>
						<INPUT id="lblHiddenIndividualID" type="hidden" name="lblHiddenIndividualID" runat="server">
						<INPUT id="lblHiddenRelatedIndividualID" type="hidden" name="lblHiddenRelatedIndividualID"
							runat="server">
						<asp:label id="lblPanelZeroFeedback" runat="server" CssClass="feedback"></asp:label></P>
				</asp:panel><asp:panel id="panelOne" runat="server" CssClass="wizardpanel">
					<asp:panel id="panelDupIndividual" runat="server">
						<P>The following similar&nbsp;individuals already exist in the database. If any of 
							them match the dependant or partner that you were about to add, then please 
							select it and press next.</P>
						<P></P>
						<P>
							<asp:DataGrid id="dataGridDependants" runat="server" CssClass="datagrid" AutoGenerateColumns="False">
								<HeaderStyle CssClass="datagridheader"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn>
										<ItemTemplate>
											<asp:Label id="Label2" runat="server"></asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn Visible="False" DataField="tblIndividualKeyInfo_IndividualID" HeaderText="HiddenID"></asp:BoundColumn>
									<asp:BoundColumn DataField="tblIndividualKeyInfo_Forename" HeaderText="Forenames"></asp:BoundColumn>
									<asp:BoundColumn DataField="tblIndividualKeyInfo_Surname" HeaderText="Surname"></asp:BoundColumn>
									<asp:BoundColumn DataField="tblIndividualKeyInfo_old_indv_id" HeaderText="Ref No"></asp:BoundColumn>
									<asp:BoundColumn DataField="tblIndividualAdditional_PreviousSurname" HeaderText="Previous Surname"></asp:BoundColumn>
									<asp:BoundColumn DataField="tblIndividualKeyInfo_DateOfBirth" HeaderText="Date of birth" DataFormatString="{0:dd-MMM-yyyy}"></asp:BoundColumn>
									<asp:BoundColumn DataField="tblAddress_AddressLine1" HeaderText="Address Line 1"></asp:BoundColumn>
								</Columns>
							</asp:DataGrid></P>
						<P>If none of these existing individuals match, then press Next to create a new 
							Dependant/Partner Individual.<BR>
					</asp:panel>
					<asp:label id="labelNoDuplicates" runat="server">No similar individual already exists. Press Next to create a new Dependent/Partner Individual</asp:label>
					<P>
						<asp:label id="lblPanelOneFeedback" runat="server" CssClass="feedback"></asp:label></P>
					<P></P>
				</asp:panel><asp:panel id="panelTwo" runat="server" CssClass="wizardpanel">
					<P>Please enter further details about the new dependent or partner, then press 
						Finish to link the new record.
					</P>
					<TABLE class="wizardtable" id="Table4">
						<TR>
							<TD class="label">Dependant/Partner For</TD>
							<TD>
								<asp:Label id="uxIndividualSummaryLabel2" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label">Forenames</TD>
							<TD>
								<asp:TextBox id="uxForenameFinalTextBox" runat="server"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label">Surname</TD>
							<TD>
								<asp:TextBox id="uxSurnameFinalTextBox" runat="server"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label">Date of Birth</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneDateBox id="uxDateOfBirthFinal" runat="server" CssClass="datebox" Width="90px" Height="20px"
									DateCulture="en-GB" DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbStandaloneDateBox></TD>
						</TR>
						<TR>
							<TD class="label">Ref No</TD>
							<TD>
								<asp:Label id="uxPartnerRefNoLabel" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label">Relationship Type</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="cmbRelationshipType" runat="server" Width="250px" Height="20px" ShowBlankRow="False"
									AutoPostBack="False" LookupTableName="lkuRelativeType" LookupControlType="ComboBox" SortByDisplayOrder="True" DataTextField="Description"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 21px">Intended location</TD>
							<TD style="HEIGHT: 21px">
								<bnbdatacontrol:BnbStandaloneLookupControl id="cmbIntendedLocation" runat="server" Width="250px" Height="20px" ShowBlankRow="True"
									AutoPostBack="False" LookupTableName="lkuRelativeApplicationLocation" LookupControlType="ComboBox" SortByDisplayOrder="True"
									DataTextField="Description"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" style="HEIGHT: 21px">Actual location</TD>
							<TD style="HEIGHT: 21px">
								<bnbdatacontrol:BnbStandaloneLookupControl id="cmbActualLocation" runat="server" Width="250px" Height="20px" ShowBlankRow="True"
									AutoPostBack="False" LookupTableName="lkuRelativeApplicationLocation" LookupControlType="ComboBox" SortByDisplayOrder="True"
									DataTextField="Description"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
					</TABLE>
					<P>
						<asp:Label id="lblPanelTwoFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				</asp:panel>
				<P></P>
				<P><uc1:wizardbuttons id="wizardButtons" runat="server"></uc1:wizardbuttons></P>
				<P><asp:label id="labelHiddenRequestIDUnused" runat="server" Visible="False"></asp:label></P>
				<p></p>
			</div>
			</TABLE>
		</form>
	</body>
</HTML>
