using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using BonoboWebControls;
using System.Text;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for WizardNewEnquiry.
	/// </summary>
	public partial class WizardNewEnquiry : System.Web.UI.Page
	{
		#region controls declaration

		
		protected System.Web.UI.WebControls.Label labelNoDuplicates;
		protected System.Web.UI.WebControls.Label labelPanelOneFeedback;
		protected System.Web.UI.WebControls.Label lblFeedback;
		protected System.Web.UI.WebControls.CheckBox chkMakeNewIndividual;
		protected UserControls.WizardButtons wizardButtons;
		protected UserControls.IndividualAddressPanel IndividualAddressPanel1;
		
		#endregion controls declaration


		#region page_load
		protected void Page_Load(object sender, System.EventArgs e)
		{
			BnbEngine.SessionManager.ClearObjectCache();
			BnbWorkareaManager.SetPageStyleOfUser(this);
			
			if (! this.IsPostBack)
			{
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				if (this.Request.QueryString["BnbIndividual"] != null )
				{	
					this.IndividualID = new Guid(this.Request.QueryString["BnbIndividual"]);
					this.Mode = "IndvKnown";
				}
				else
				{
					this.Mode = "Normal";			
				}
				BnbWebFormManager.LogAction("New Enquiry Wizard", null, this);
				if (this.Request.UrlReferrer != null)
					txtHiddenReferrer.Text = this.Request.UrlReferrer.ToString();
				else
					txtHiddenReferrer.Text = "~/Menu.aspx";
			}

			if (this.Mode == "IndvKnown")
			{
				// just the last few panels
				panelGetName.Visible = false;
				panelDuplicateCheck.Visible = false;
				panelAddress.Visible = false;
				wizardButtons.AddPanel(panelChooseAppType);
				if (radEnquiry2.Checked)
				{
					wizardButtons.AddPanel(panelEnquirerDetails);
					panelApplicantDetails.Visible = false;
				}
				else
				{
					wizardButtons.AddPanel(panelApplicantDetails);
					panelEnquirerDetails.Visible = false;
				}
			}
			else
			{
				// most panels
				wizardButtons.AddPanel(panelGetName);
				wizardButtons.AddPanel(panelDuplicateCheck);
				if (this.IndividualSelectedFromList)
					panelAddress.Visible = false;
				else
					wizardButtons.AddPanel(panelAddress);
					

				if (radEnquiry.Checked)
				{
					wizardButtons.AddPanel(panelEnquirerDetails);
					panelApplicantDetails.Visible = false;
				}
				else
				{
					wizardButtons.AddPanel(panelApplicantDetails);
					panelEnquirerDetails.Visible = false;
				}
				panelChooseAppType.Visible = false;

			}
            //For capturing Geographical locations
            doLoadGeo();
		}

		#endregion page_load

		#region property
		/// <summary>
		/// stores the individual that the action is being added to
		/// </summary>
		private Guid IndividualID
		{
			get
			{
				if (lblHiddenIndividualID.Value.Trim().Length >0)
					return new Guid(lblHiddenIndividualID.Value);
				else
					return Guid.Empty;
			}
			set{lblHiddenIndividualID.Value = value.ToString();}
		}

		/// <summary>
		/// Mode "Normal"  = normal (all panels)
		/// Mode "IndvKnown" = show last panel only because Individual already known
		/// </summary>
		public string Mode
		{
			get
			{
				return uxHiddenMode.Text;
			}
			set
			{
				uxHiddenMode.Text = value;
			}
		}
		#endregion property

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			wizardButtons.ShowPanel +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
			wizardButtons.ValidatePanel +=new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
			wizardButtons.CancelWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
			wizardButtons.FinishWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);

		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


		#region public methods

		public bool IndividualSelectedFromList
		{
			get
			{
				Guid guiIndividualID = bdgIndividuals.SelectedDataRowID;
				return !(guiIndividualID == Guid.Empty);
			}
		}

		public void initWizard()
		{
			
		}

		public void SetupPanelGetName()
		{
			// clear individualid to allow repeated searches
			this.IndividualID = Guid.Empty;
			bdgIndividuals.SelectedDataRowID = Guid.Empty;
			radEnquiry.Checked = true;
			

		}
		public void SetupPanelAddress()
		{
			
				IndividualAddressPanel1.Forename = txtFirstname.Text;
				IndividualAddressPanel1.Surname = txtSurname.Text;
				
			lblPanelAddressFeedback.Text = "";
			chkIgnoreAddressWarnings.Checked = false;
			chkIgnoreAddressWarnings.Visible = false;
		}

		public void SetupPanelChooseAppType()
		{
			radEnquiry2.Checked = true;
		}

		public void SetupPanelDuplicateCheck()
		{
			
			lblPanelOneFeedback.Text = "";
			
			string forenameBit = txtFirstname.Text;
			if (forenameBit.Length > 5)
				forenameBit = forenameBit.Substring(0,5);

			BnbTextCondition surnameMatch = new BnbTextCondition("tblIndividualKeyInfo_Surname", txtSurname.Text, BnbTextCompareOptions.StartOfField);
			BnbTextCondition forenameMatch = new BnbTextCondition("tblIndividualKeyInfo_Forename", forenameBit, BnbTextCompareOptions.StartOfField);

			BnbCriteria dupCriteria = new BnbCriteria(surnameMatch);
			dupCriteria.QueryElements.Add(forenameMatch);
			bdgIndividuals.Criteria = dupCriteria;
			bdgIndividuals.MaxRows = 30;
			//bdgIndividuals.DataBind();
			
			lblMaxRowsExceeded.Text = "";
			if (bdgIndividuals.ResultCount > 0)
			{
				subpanelChoose.Visible = true;
				lblNoDuplicates.Visible = false;
				if (bdgIndividuals.MaxRowsExceeded )
					lblMaxRowsExceeded.Text = "More than 30 possible matches were found. Only the first 30 are shown here.";
			}
			else
			{
				subpanelChoose.Visible = false;
				lblNoDuplicates.Visible = true;
			}
		}

		public void SetupPanelEnquirerDetails()
		{
			cmbRecruitment.SelectedValue = BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID;
            cmbRecruitedBy.SelectedValue = BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID;
			cmbVSOType.SelectedValue = BnbConst.VSOType_VSOStandard;
			cmbMethod.SelectedValue = BnbConst.EnquiryMethod_Phone;
			cmbStatusGroupEnquirer.SelectedValue =  BnbConst.StatusGroup_Enquirer;
			cmbStatus.SelectedValue = BnbConst.Status_EnquirerEnquirer;
			
			if (this.IndividualID != Guid.Empty)
			{
				BnbIndividual tempIndv = BnbIndividual.Retrieve(this.IndividualID);
				uxSummarySurname.Text = tempIndv.Surname;
				uxSummaryForename.Text = tempIndv.Forename;
				uxSummaryRefNo.Text = tempIndv.RefNo;
				if (BnbRuleUtils.HasValue(tempIndv.DateOfBirth))
				{
					txtDOB.Date = tempIndv.DateOfBirth;
					txtDOB.Enabled = false;
				}
				if(tempIndv.Additional.Comments != null && tempIndv.Additional.Comments != "" )
				{
					txtComments.Text = tempIndv.Additional.Comments.ToString();
					txtComments.ReadOnly = true;
					txtComments.CssClass = "textboxdisabled";
				}
			}
			else
			{
				uxSummarySurname.Text = txtSurname.Text;
				uxSummaryForename.Text = txtFirstname.Text;
				uxSummaryRefNo.Text = "(New)";
			}

		}

		public void SetupPanelApplicantDetails()
		{
			cmbRecruitmentBase2.SelectedValue =  BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID;
            cmbRecruitedBy2.SelectedValue = BnbEngine.SessionManager.GetSessionInfo().DefaultRecruitmentBaseID;

			cmbVSOType2.SelectedValue = BnbConst.VSOType_VSOStandard;
			cmbApplicationSource2.SelectedValue = BnbConst.ApplicationSource_PaperAppForm;
			cmbStatusGroupApplicant.SelectedValue =  BnbConst.StatusGroup_Applicant;
			cmbStatus2.SelectedValue = BnbConst.Status_ApplicantApplicant;
			
			if (this.IndividualID != Guid.Empty)
			{
				BnbIndividual tempIndv = BnbIndividual.Retrieve(this.IndividualID);
				uxSummarySurname2.Text = tempIndv.Surname;
				uxSummaryForename2.Text = tempIndv.Forename;
				uxSummaryRefNo2.Text = tempIndv.RefNo;
				if (BnbRuleUtils.HasValue(tempIndv.DateOfBirth))
				{
					txtDOB2.Date = tempIndv.DateOfBirth;
					txtDOB2.Enabled = false;
				}
				if (tempIndv.IndividualSkills.GetLead() != null)
				{
					BnbIndividualSkill leadSkill = tempIndv.IndividualSkills.GetLead() as BnbIndividualSkill;
					cmbLeadSkill2.SelectedValue = leadSkill.SkillID;
					cmbLeadSkill2.LookupControlType = BonoboWebControls.DataControls.LookupControlType.ReadOnlyLabel;
				}
				if (tempIndv.IndividualMaritalStatuses.Count > 0)
				{
					tempIndv.IndividualMaritalStatuses.Sort("MaritalStatusDate",true);
					BnbIndividualMaritalStatus mainMarital = tempIndv.IndividualMaritalStatuses[0] as BnbIndividualMaritalStatus;
					cmbMaritalStatus2.SelectedValue = mainMarital.MaritalStatusID;
					cmbMaritalStatus2.LookupControlType = BonoboWebControls.DataControls.LookupControlType.ReadOnlyLabel;
				}
				if(tempIndv.Additional.Comments != null && tempIndv.Additional.Comments != "")
				{
				txtComments2.Text = tempIndv.Additional.Comments.ToString();
				txtComments2.ReadOnly = true;
				txtComments2.CssClass = "textboxdisabled";
				}
			}
			else
			{
				uxSummarySurname2.Text = txtSurname.Text;
				uxSummaryForename2.Text = txtFirstname.Text;
				uxSummaryRefNo2.Text = "(New)";
			}
		}
		#endregion public methods

		#region private methods

		private BnbIndividual RegisterIndividualObjects(BnbEditManager em)
		{
			BnbIndividual objIndividual;
			if (this.IndividualID != Guid.Empty)
			{
				objIndividual = BnbIndividual.Retrieve(this.IndividualID);
				objIndividual.RegisterForEdit(em);	
			}

			else
			{
				objIndividual = new BnbIndividual(true);
				objIndividual.RegisterForEdit(em);	
				objIndividual.Forename = IndividualAddressPanel1.Forename;
				objIndividual.Surname = IndividualAddressPanel1.Surname;
				IndividualAddressPanel1.saveIndividualAddressDetails(objIndividual,em);
						
			}
			return objIndividual;
		}

		private void CreateNewEnquirer()
		{
		
			BnbEditManager em = new BnbEditManager();
			BnbIndividual objIndividual = this.RegisterIndividualObjects(em);

			BnbApplication objApplication = new BnbApplication(true);
			objApplication.RegisterForEdit(em);

			//add the application to the individual
			objIndividual.Applications.Add(objApplication);

			// only do DOB if field is enabled
			// (otherwise it means that they already have one)
			if (txtDOB.Enabled)
			{
				if (!txtDOB.IsValid)
				{
					em.EditCompleted();
					lblPanelEnquierDetailsFeedback.Text = "Date of Birth was not in 'dd/mmm/yyyy' format";
					return;
				}
				objIndividual.DateOfBirth = txtDOB.Date;
			}

			// only do comments if the field is enabled
			// (otherwise it means they already have comments)
			if (!txtComments.ReadOnly && txtComments.Text != "")
			{
				objIndividual.Additional.RegisterForEdit(em);
				objIndividual.Additional.Comments = txtComments.Text;
			}

			if (cmbMethod.SelectedValue != null)
				objApplication.EnquiryMethodID = Convert.ToInt32(cmbMethod.SelectedValue);
			if (cmbRecruitment.SelectedValue != null)
                objApplication.RecruitmentBaseID = Convert.ToInt32(cmbRecruitment.SelectedValue);
            //VAF-761
            if (cmbRecruitedBy.SelectedValue != null)
                objApplication.RecruitedByID = Convert.ToInt32(cmbRecruitedBy.SelectedValue);
			if (cmbVSOType.SelectedValue != null)
				objApplication.VSOTypeID = Convert.ToInt32(cmbVSOType.SelectedValue);
				
			objApplication.AddApplicationStatus(Convert.ToInt32(cmbStatus.SelectedValue));
				
			this.SaveAndRedirect(em, objApplication);
		}	
		
		private void CreateNewApplicant()
		{
			BnbEditManager em = new BnbEditManager();
			BnbIndividual objIndividual = this.RegisterIndividualObjects(em);

			BnbApplication objApplication = new BnbApplication(true);
			objApplication.RegisterForEdit(em);

			//add the application to the individual
			objIndividual.Applications.Add(objApplication);
			

			// only do DOB if field is enabled
			// (otherwise it means that they already have one)
			if (txtDOB2.Enabled)
			{
				if (!txtDOB2.IsValid)
				{
					em.EditCompleted();
					lblPanelApplicantDetailsFeedback.Text = "Date of Birth was not in 'dd/mmm/yyyy' format";
					return;
				}
				objIndividual.DateOfBirth = txtDOB2.Date;
			}
			
			if (cmbRecruitmentBase2.SelectedValue != null)
				objApplication.RecruitmentBaseID = Convert.ToInt32(cmbRecruitmentBase2.SelectedValue);
            if (cmbRecruitedBy2.SelectedValue != null)
                objApplication.RecruitedByID = Convert.ToInt32(cmbRecruitedBy2.SelectedValue);
			if (cmbVSOType2.SelectedValue != null)
				objApplication.VSOTypeID = Convert.ToInt32(cmbVSOType2.SelectedValue);
			if (cmbApplicationSource2.SelectedValue != null)
				objApplication.ApplicationSourceID = Convert.ToInt32(cmbApplicationSource2.SelectedValue);
			if (!txtAvailableFrom2.IsValid)
			{
				em.EditCompleted();
				lblPanelApplicantDetailsFeedback.Text = "Available From was not in 'dd/mmm/yyyy' format";
				return;
			}
			objApplication.AvailabilityDate = txtAvailableFrom2.Date;				
			
			objApplication.AddApplicationStatus(Convert.ToInt32(cmbStatus2.SelectedValue));
			
			// only do marital status if the individual doesnt have any
			// (control will be disabled if they do)
			if (objIndividual.IndividualMaritalStatuses.Count == 0
				&& cmbMaritalStatus2.SelectedValue.ToString() != "-1")
			{
				BnbIndividualMaritalStatus objMarriage = new BnbIndividualMaritalStatus(true);
				objMarriage.RegisterForEdit(em);
				if (cmbMaritalStatus2.SelectedValue != null)
					objMarriage.MaritalStatusID = Convert.ToInt32(cmbMaritalStatus2.SelectedValue);
				//objMarriage.MaritalStatusDate = DateTime.Now;
				objIndividual.IndividualMaritalStatuses.Add(objMarriage);
			}

			// only do skill if the individual doesnt have any
			// (control will be disabled if they do)
			if (objIndividual.IndividualSkills.Count == 0 
				&& cmbLeadSkill2.SelectedValue.ToString() != "-1")
			{
				BnbIndividualSkill objSkill = new BnbIndividualSkill(true);
				objSkill.RegisterForEdit(em);
				if(cmbLeadSkill2.SelectedValue != null)
					objSkill.SkillID = Convert.ToInt32(cmbLeadSkill2.SelectedValue);
				objSkill.Order = 1;
				objIndividual.IndividualSkills.Add(objSkill);
			}
			//only do comments if field enabled
			// (control will be disabled if they already have comments
			if (!txtComments2.ReadOnly && txtComments2.Text != "")
			{
				objIndividual.Additional.RegisterForEdit(em);
				objIndividual.Additional.Comments = txtComments2.Text;
			}


			this.SaveAndRedirect(em, objApplication);


		}

		private void SaveAndRedirect(BnbEditManager em, BnbApplication app)
		{
			try
			{
				em.SaveChanges();
				BnbEngine.SessionManager.ClearObjectCache();
				Response.Redirect("ApplicationPage.aspx?BnbApplication=" + app.ID.ToString());
			}
			catch(BnbProblemException pe)
			{
				string feedback = "Unfortunately, the record could not be created due to the following problems: <br/>";
				foreach(BnbProblem p in pe.Problems)
					feedback += p.Message + "<br/>";
				lblPanelEnquierDetailsFeedback.Text = feedback;
				lblPanelApplicantDetailsFeedback.Text = feedback;

			}
		}

		private bool AddressPanelValidate()
		{
			BnbIndividual dummy = new BnbIndividual(true);
			BnbEditManager em = new BnbEditManager();
			dummy.RegisterForEdit(em);
			dummy.Forename = txtFirstname.Text;
			dummy.Surname = txtSurname.Text;
			IndividualAddressPanel1.saveIndividualAddressDetails(dummy,em);

			// only do warnings if checkbox not checked
			if (!chkIgnoreAddressWarnings.Checked)
			{
				BnbWarningCollection warnColl = em.GetEditWarnings();
				if (warnColl.Count > 0)
				{
					chkIgnoreAddressWarnings.Visible = true;
					lblPanelAddressFeedback.Text = "Please check the following warnings:<br/>";
					foreach(BnbWarning warn in warnColl)
						lblPanelAddressFeedback.Text += warn.Message + "<br/>";
					return false;
				}
			}

			// if no warnings, do validation
			try
			{
				em.Validate();
			} 
			catch(BnbProblemException pe)
			{
				lblPanelAddressFeedback.Text = "Please check the following problems:<br/>";
				foreach(BnbProblem p in pe.Problems)
					lblPanelAddressFeedback.Text += p.Message + "<br/>";
				return false;
			}
			return true;
		}

		#endregion private methods

		#region events
		private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			if (e.CurrentPanelHtmlID == panelGetName.ID )
			{
				this.SetupPanelGetName();
			}
			if (e.CurrentPanelHtmlID == panelDuplicateCheck.ID)
			{
				this.SetupPanelDuplicateCheck();
			}
			if (e.CurrentPanelHtmlID == panelAddress.ID)
			{
				this.SetupPanelAddress();
			}
			if (e.CurrentPanelHtmlID == panelChooseAppType.ID)
			{
				this.SetupPanelChooseAppType();
			}
			if (e.CurrentPanelHtmlID == panelEnquirerDetails.ID)
			{
				this.SetupPanelEnquirerDetails();
			}
			if (e.CurrentPanelHtmlID == panelApplicantDetails.ID)
			{
				this.SetupPanelApplicantDetails();
			}
		}

		private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			Response.Redirect(txtHiddenReferrer.Text);
		}

		private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			// different methods depending on whether its enquiry or app
			if (panelEnquirerDetails.Visible)
				this.CreateNewEnquirer();
			else
				this.CreateNewApplicant();
		}

		/// <summary>
		/// If the current panel represents the list of individuals found with previous panel criteria then
		/// If the individual is not found then create it with wizardnewindividual 
		/// otherwise continue with this wizard.
		/// </summary>
		/// <param name="Sender"></param>
		/// <param name="e"></param>
		private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
		{
			if (e.CurrentPanelHtmlID == panelGetName.ID)
			{
				if (txtFirstname.Text == "" || txtSurname.Text == "")
				{
					e.Proceed = false;
					labelPanelZeroFeedback.Text = "Please enter a Forename and Surname before pressing Next";
				}
			}
			if (e.CurrentPanelHtmlID == panelDuplicateCheck.ID)
			{
				e.Proceed = true;
				this.IndividualID = bdgIndividuals.SelectedDataRowID;
			}
			if (e.CurrentPanelHtmlID == panelAddress.ID)
			{
				e.Proceed = AddressPanelValidate();
			}
		}


		#endregion events

        /// <summary>
        /// //For capturing Geographical locations
        /// </summary>
        private void doLoadGeo()
        {
            if (HasPermissionToLocateOnMap())
            {
                StringBuilder script = new StringBuilder();
                script.Append("pageName ='WizardNewEnquiry';");
                // Page mode
                script.Append("mode='new/edit';");
                if (!(IndividualAddressPanel1.Latitude.Trim() == string.Empty))
                {
                    ViewState["Latittude"] = IndividualAddressPanel1.Latitude;
                    ViewState["Longitude"] = IndividualAddressPanel1.Longitude;
                }

                if (!(ViewState["Latittude"] == null))
                {
                    string latitude = ViewState["Latittude"].ToString();
                    string longitude = ViewState["Longitude"].ToString();
                    script.Append("latVal = " + latitude + ";lngVal = " + longitude + ";");
                }
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "WNELoadGeo", script.ToString(), true);
            }
        }


        /// <summary>
        /// Check permisssion to access the Google map functionality
        /// </summary>
        private bool HasPermissionToLocateOnMap()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_LocateAddressOnMap))
                return true;
            else
                return false;
        }

	}
}
