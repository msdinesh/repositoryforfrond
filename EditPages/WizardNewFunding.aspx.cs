using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;

namespace Frond.EditPages
{
	/// <summary>
	/// </summary>
	public partial class WizardNewFunding : System.Web.UI.Page
	{
		protected UserControls.WizardButtons wizardButtons;
		#region controls

	

		#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
#if DEBUG
			if (!this.IsPostBack && this.Request.QueryString.ToString() == "")
				Response.Redirect("WizardNewFunding.aspx?BnbApplication=41D25807-FA94-40CC-9138-C7BFFC38ED4E");
#endif


			BnbEngine.SessionManager.ClearObjectCache();
			
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);

			if (!this.IsPostBack)
			{
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				if (this.Request.UrlReferrer != null)
					lblHiddenReferrer.Text = this.Request.UrlReferrer.ToString();
				else
					lblHiddenReferrer.Text = "~/Menu.aspx";

				this.ParseQueryString();
				this.InitWizard();
				BonoboWebControls.BnbWebFormManager.LogAction("New Sponsorship Wizard", null, this);
			}

			wizardButtons.AddPanel(panelZero);
			wizardButtons.AddPanel(panelOne);
			wizardButtons.AddPanel(panelTwo);

			attachKeyHandlerAttributesToFormFields();
		}

		/// <summary>
		/// Attaches a javascript call to handle submitting the find screen when the return key is pressed
		/// </summary>
		private void attachKeyHandlerAttributesToFormFields()
		{
			//KeyDownTextBoxDefaultButtonHandler

			//organisation and placement / organisation and volunteer
			txtFindOrgName.Attributes.Add("onkeydown","KeyDownTextBoxDefaultButtonHandler(cmdOrgFind);");
			drpFindOrgCountry.Attributes.Add("onkeydown","KeyDownTextBoxDefaultButtonHandler(cmdOrgFind);");

			//cmdIndivFind
			//individual and placement / individual and volunteer
			txtFindRefNo.Attributes.Add("onkeydown","KeyDownTextBoxDefaultButtonHandler(cmdIndivFind);");
			txtFindSurname.Attributes.Add("onkeydown","KeyDownTextBoxDefaultButtonHandler(cmdIndivFind);");
			txtFindForename.Attributes.Add("onkeydown","KeyDownTextBoxDefaultButtonHandler(cmdIndivFind);");
			txtFindPostcode.Attributes.Add("onkeydown","KeyDownTextBoxDefaultButtonHandler(cmdIndivFind);");
			drpFindIndivCountry.Attributes.Add("onkeydown","KeyDownTextBoxDefaultButtonHandler(cmdIndivFind);");
		}

		/// <summary>
		/// Returns the PositionID the funding (may be) attached to
		/// may also return Guid.Empty if no Position yet (i.e. an unlinked volunteer)
		/// </summary>
		public Guid PositionID
		{
			get{return new Guid(lblHiddenPositionID.Text);}
			set{lblHiddenPositionID.Text = value.ToString();}
		}
		
		/// <summary>
		/// Returns the ApplicationId the funding (may be) attached to
		/// may also return Guid.Empty if no Application yet (i.e. and unfilled placement)
		/// </summary>
		public Guid ApplicationID
		{
			get{return new Guid(lblHiddenApplicationID.Text);}
			set{lblHiddenApplicationID.Text = value.ToString();}
		}

		/// <summary>
		/// Returns the CompanyID of the 'org' selected by the user (if any)
		/// </summary>
		public Guid CompanyID
		{
			get
			{
				if (lblHiddenCompanyID.Text == "")
					return Guid.Empty;
				else
					return new Guid(lblHiddenCompanyID.Text);
			}
			set{lblHiddenCompanyID.Text = value.ToString();}
		}

		/// <summary>
		/// Returns the IndividualID of the donor individual selected by the user (if any)
		/// </summary>
		public Guid DonorIndividualID
		{
			get
			{
				if (lblHiddenDonorIndividualID.Text == "")
					return Guid.Empty;
				else
					return new Guid(lblHiddenDonorIndividualID.Text );
			}
			set{lblHiddenDonorIndividualID.Text = value.ToString();}
		}

		/// <summary>
		/// The association (volunteer or Placement) selected by the user
		/// </summary>
		public int FundingAssociationID
		{
			get{return int.Parse(lblHiddenAssociationID.Text);}
			set{lblHiddenAssociationID.Text = value.ToString();}
		}

		private void InitWizard()
		{
			// panel zero
			rbDonorOrganisation.Checked = true;

			Guid defaultCountry = BnbEngine.SessionManager.GetSessionInfo().DefaultCountryID;
            //Code Integrated for displaying defaultcountry only to starfish users
            if ((BnbEngine.SessionManager.GetSessionInfo().WebStyleID) == 11)
			if (defaultCountry != Guid.Empty)
				drpFindOrgCountry.SelectedValue = defaultCountry.ToString();
//				drpFindOrgCountry.Items.FindByValue(defaultCountry.ToString()).Selected = true;

			this.PopulateCombo("lkuCountry", drpFindIndivCountry, true);
			// default to user country
			if (defaultCountry != Guid.Empty)
				drpFindIndivCountry.Items.FindByValue(defaultCountry.ToString()).Selected = true;


			// default financial year
			int defaultFY = BnbRuleUtils.GetFinancialYearID(DateTime.Now);
			if (defaultFY>0)
				drpFinancialYear.SelectedValue = defaultFY; 
			// default dept type
			drpFundingSecuredBy.SelectedValue = BnbConst.FunderDepartmentType_Fundraising.ToString();
			// default donor user
			drpDonorOwner.SelectedValue = BnbEngine.SessionManager.GetSessionInfo().UserID.ToString();
			// status to reserved?
			drpFundingStatus.SelectedValue = BnbConst.FundingStatus_FundReserved.ToString();
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			wizardButtons.CancelWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
			wizardButtons.FinishWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
			wizardButtons.ShowPanel +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
			wizardButtons.ValidatePanel +=new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			this.ReturnToOriginalPage();
		}

		private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			this.SaveFunding();
		}

		private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			if (e.CurrentPanel == 0)
				this.SetupPanelZero();

			if (e.CurrentPanel == 1)
				this.SetupPanelOne();

			if (e.CurrentPanel == 2)
				this.SetupPanelTwo();

		}

		private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
		{
			if (e.CurrentPanel == 0)
			{
				if (rbVolunteer.Checked == false && rbPlacement.Checked == false)
				{
					lblPanelZeroFeedback.Text = "You must select Placement or Volunteer";
					e.Proceed = false;
				}
				if (rbDonorOrganisation.Checked == false && rbDonorIndividual.Checked == false)
				{
					lblPanelZeroFeedback.Text = "You must select whether the Donor is an Organisation or an Individual";
					e.Proceed = false;
				}

			}
			if (e.CurrentPanel == 1)
			{
				if (rbDonorOrganisation.Checked && bdgOrgs.SelectedDataRowID == Guid.Empty)
				{
					lblPanelOneFeedback.Text = "Please use the Find and select an Organisation Row before pressing Next";
					e.Proceed = false;
				}
				if (!rbDonorOrganisation.Checked && bdgIndiv.SelectedDataRowID == Guid.Empty)
				{
					lblPanelOneFeedback.Text = "Please use the Find and select an Individual Row before pressing Next";
					e.Proceed = false;
				}
				
			}

		}

		private void SetupPanelZero()
		{
			// fill in app and pos details
			bool hasApp = false;
			bool hasPos = false;
			BnbApplication tempApp = null;
			BnbPosition tempPos = null;
			if (this.ApplicationID != Guid.Empty)
			{
				tempApp = BnbApplication.Retrieve(this.ApplicationID);
				lblVolunteer1.Text = tempApp.InstanceDescription;
				lblVolunteer2.Text = tempApp.InstanceDescription;
				hasApp = true;
			} 
			else
			{
				lblVolunteer1.Text = "(Placement is not filled)";
				lblVolunteer2.Text = "(Placement is not filled)";
			}
			if (this.PositionID != Guid.Empty)
			{
				tempPos = BnbPosition.Retrieve(this.PositionID);
				lblPlacement1.Text = tempPos.InstanceDescription;
				lblPlacement2.Text = tempPos.InstanceDescription;
				hasPos = true;
			} 
			else
			{
				lblPlacement1.Text = "(Volunteer is not placed)";
				lblPlacement2.Text = "(Volunteer is not placed)";
			}
			// quick consistency check
			if (hasApp == false && hasPos == false)
				throw new ApplicationException("Page.ApplicationID and Page.PositionID should not both be empty");

			if (hasApp == false && hasPos == true)
			{
				rbVolunteer.Enabled = false;
				rbVolunteer.Checked = false;
				rbPlacement.Checked = true;
				
			}
			if (hasPos == false && hasApp == true)
			{
				rbPlacement.Enabled = false;
				rbVolunteer.Checked = true;
				
			}
			if (hasPos == true && hasApp == true)
				rbPlacement.Checked = true;

			//<!-- VAF-52
			if((tempPos != null && tempPos.PlacedService != null && tempPos.PlacedService.Application != null && tempPos.PlacedService.Application.CurrentStatus != null && tempPos.PlacedService.Application.CurrentStatus.StatusID == BnbConst.Status_OverseasOverseas) ||
				(tempApp != null && tempApp.CurrentStatus != null && tempApp.CurrentStatus.StatusID == BnbConst.Status_OverseasOverseas))
			{
				rbPlacement.Enabled = true;
				rbPlacement.Checked = false;
				rbVolunteer.Enabled = true;
				rbVolunteer.Checked = true;
			}
			//-->

			lblPanelZeroFeedback.Text = "";
		}

		private void SetupPanelOne()
		{
			if (rbVolunteer.Checked)
				this.FundingAssociationID = BnbConst.FundingType_Volunteer;
			else
				this.FundingAssociationID = BnbConst.FundingType_Placement;

			if (rbDonorOrganisation.Checked)
			{
				panelChooseOrg.Visible = true;
				panelChooseIndv.Visible = false;
				lblSearchHintIndiv.Visible = false;
			}
			else
			{
				panelChooseOrg.Visible = false;
				panelChooseIndv.Visible = true;
				lblSearchHint.Visible = false;
			}

			lblPanelOneFeedback.Text = "";
		}

		private void SetupPanelTwo()
		{
			// get selected org
			if (rbDonorOrganisation.Checked)
				this.CompanyID = bdgOrgs.SelectedDataRowID;
			else
				this.DonorIndividualID = bdgIndiv.SelectedDataRowID;

			// fill in labels
			if (rbDonorOrganisation.Checked)
			{
				BnbCompany tempComp = BnbCompany.Retrieve(this.CompanyID);
				lblOrgName.Text = tempComp.InstanceDescription;
				lblDonorIndivName.Text = "(None)";
			}
			else
			{
				lblOrgName.Text = "(None)";
				BnbIndividual tempIndiv = BnbIndividual.Retrieve(this.DonorIndividualID);
				lblDonorIndivName.Text = tempIndiv.InstanceDescription;
			}
			// placement or vol
			lblAssociation.Text = BnbEngine.LookupManager.GetLookupItemDescription("lkuFundingType", this.FundingAssociationID);

			lblPanelTwoFeedback.Text = "";

		}

		private void SaveFunding()
		{
			lblPanelTwoFeedback.Text = "";
			// check that a value has been supplied - 
			// not mandatory strictly speaking but is good to remind the user
			if (txtValue.Text == "")
			{
				lblPanelTwoFeedback.Text = "You must specify a Funding Value";
				return;
			}
			// create the new Funding record and save
			BnbEditManager em = new BnbEditManager();
			BnbFunding newFunding = new BnbFunding(true);
			newFunding.RegisterForEdit(em);

			// link to other objects
			if (this.ApplicationID != Guid.Empty)
			{
				BnbApplication linkedApp = BnbApplication.Retrieve(this.ApplicationID);
				//linkedApp.RegisterForEdit(em);
				linkedApp.Funding.Add(newFunding);
			}
			if (this.PositionID != Guid.Empty)
			{
				BnbPosition linkedPos = BnbPosition.Retrieve(this.PositionID);
				//linkedPos.RegisterForEdit(em);
				// logic in BnbFunding may already have done this, so check before adding
				if (!linkedPos.Funding.Contains(newFunding.ID))
					linkedPos.Funding.Add(newFunding);
			}

			// link to company or indiv
			if (rbDonorOrganisation.Checked)
			{
				BnbCompany linkedComp = BnbCompany.Retrieve(this.CompanyID);
				//linkedComp.RegisterForEdit(em);
				linkedComp.Funding.Add(newFunding);
			}
			else
			{
				BnbIndividual linkedDonorIndiv = BnbIndividual.Retrieve(this.DonorIndividualID);
				//linkedDonorIndiv.RegisterForEdit(em);
				linkedDonorIndiv.DonatedFunding.Add(newFunding);

			}

			// fill in other properties
			newFunding.FundingTypeID = this.FundingAssociationID;
			newFunding.FundingValue = txtValue.Text;
			if (!uxReceivedOn.IsValid)
			{
				em.EditCompleted();
				lblPanelTwoFeedback.Text = "Received On was not in 'dd/mmm/yyyy' format";
				return;
			}
			newFunding.ReceivedDate = uxReceivedOn.Date;
			
			if (drpDonorOwner.SelectedValue != null)
				newFunding.ForUserID = new Guid(drpDonorOwner.SelectedValue.ToString());
			if (drpFundingSecuredBy.SelectedValue != null)
				newFunding.FunderDepartmentID = int.Parse(drpFundingSecuredBy.SelectedValue.ToString());
			if (drpFinancialYear.SelectedValue != null)
				newFunding.FinancialYearID = int.Parse(drpFinancialYear.SelectedValue.ToString());
			if (drpFundingStatus.SelectedValue != null)
				newFunding.FundingStatusID = int.Parse(drpFundingStatus.SelectedValue.ToString());
			if (drpCurrency.SelectedValue != null)
				newFunding.CurrencyID = int.Parse(drpCurrency.SelectedValue.ToString());
			newFunding.ConvertedValue = txtConvertedValue.Text;
			
			try
			{
				em.SaveChanges();
				this.RedirectToFundingPage(newFunding.ID);
			}
			catch(BnbProblemException pe)
			{
				lblPanelTwoFeedback.Text = "Unfortunately, the Funding record could not be created due to the following problems: <br/>";
				foreach(BnbProblem p in pe.Problems)
					lblPanelTwoFeedback.Text += p.Message + "<br/>";

			}

		}

		private void ReturnToOriginalPage()
		{
			Response.Redirect(lblHiddenReferrer.Text);
		}

		private void ParseQueryString()
		{
			Guid appID = Guid.Empty;
			Guid posID = Guid.Empty;
			if (this.Request.QueryString["BnbPosition"] != null)
				posID = new Guid(this.Request.QueryString["BnbPosition"]);
			if (this.Request.QueryString["BnbApplication"] != null)
				appID = new Guid(this.Request.QueryString["BnbApplication"]);

			// if one if blank, attempt to derive from the other
			if (appID != Guid.Empty && posID == Guid.Empty)
			{
				BnbApplication tempApp = BnbApplication.Retrieve(appID);
				if (tempApp.PlacedService != null && tempApp.PlacedService.Position != null)
					posID = tempApp.PlacedService.Position.ID;
			}
			if (posID != Guid.Empty && appID == Guid.Empty)
			{
				BnbPosition tempPos = BnbPosition.Retrieve(posID);
				if (tempPos.PlacedService != null && tempPos.PlacedService.Application != null)
					appID = tempPos.PlacedService.Application.ID;
			}

			// if both blank, throw an error
			if (posID == Guid.Empty && appID == Guid.Empty)
				throw new ApplicationException("WizardNewFunding.aspx expects a BnbApplication parameter or a BnbPosition parameter in the querystring");

			// store them in page for later use
			this.ApplicationID = appID;
			this.PositionID = posID;
		}

		private void RedirectToFundingPage(Guid fundingID)
		{
			string fundUrl = Redirect.GetFundingRedirectURL(fundingID);
			Response.Redirect("~\\" + fundUrl);
		}
	
		private void PopulateCombo(string lookupName, DropDownList combo, bool addBlank)
		{
			string defaultFilter = "Exclude = 0";
			this.PopulateCombo(lookupName, defaultFilter, combo, addBlank);
		}

		protected void cmdIndivFind_Click(object sender, System.EventArgs e)
		{
			this.FindIndiv();
		}

		protected void cmdOrgFind_Click(object sender, System.EventArgs e)
		{
			this.FindOrg();
		}

		private void FindOrg()
		{
			lblPanelOneFeedback.Text = "";
			// there must be bit of name
			if (txtFindOrgName.Text == "")
			{
				lblPanelOneFeedback.Text = "You must enter part of an Org Name before pressing Find";
				return;
			}
			
			// build criteria
			BnbTextCondition orgNameCondition = new BnbTextCondition("tblCompany_CompanyName", txtFindOrgName.Text, BnbTextCompareOptions.AnyPartOfField);
			BnbCriteria findOrgCriteria = new BnbCriteria(orgNameCondition);
			// country might not be specified
			if (drpFindOrgCountry.SelectedValue != null)
			{
				BnbListCondition orgCountryCondition = new BnbListCondition("tblAddress_CountryID", new Guid(drpFindOrgCountry.SelectedValue.ToString()));
				findOrgCriteria.QueryElements.Add(orgCountryCondition);
			}
			// supply criteria to grid
			bdgOrgs.Criteria = findOrgCriteria;
			bdgOrgs.MaxRows = 20;
			bdgOrgs.Visible = true;
			bdgOrgs.DataBind();
		
			lblSearchHint.Visible = true;
		}

		private void FindIndiv()
		{
			lblPanelOneFeedback.Text = "";
			// build criteria
			BnbCriteria findIndivCriteria = new BnbCriteria();
			// build criteria
			if (txtFindRefNo.Text != "")
				findIndivCriteria.QueryElements.Add(new BnbTextCondition("tblIndividualKeyInfo_old_indv_id", 
						txtFindRefNo.Text, BnbTextCompareOptions.ExactMatch));

			if (txtFindSurname.Text != "")
				findIndivCriteria.QueryElements.Add(new BnbTextCondition("tblIndividualKeyInfo_Surname", 
						txtFindSurname.Text, BnbTextCompareOptions.StartOfField));

			if (txtFindForename.Text != "")
				findIndivCriteria.QueryElements.Add(new BnbTextCondition("tblIndividualKeyInfo_Forename", 
					txtFindForename.Text, BnbTextCompareOptions.StartOfField));

			if (txtFindPostcode.Text != "")
				findIndivCriteria.QueryElements.Add(new BnbTextCondition("tblIndividualKeyInfo_Forename", 
					txtFindPostcode.Text, BnbTextCompareOptions.StartOfField));

			// so far, if we dont have any criteria, then reject
			if (findIndivCriteria.QueryElements.Count == 0)
			{
				lblPanelOneFeedback.Text = "Please enter a Ref No, Surname, Forename or Postcode before pressing Find";
				return;
			}

			// country might not be specified
			if (drpFindIndivCountry.SelectedValue != "0")
				findIndivCriteria.QueryElements.Add(new BnbListCondition("tblAddress_CountryID", 
					new Guid(drpFindIndivCountry.SelectedValue)));
	
			// supply criteria to grid
			bdgIndiv.Criteria = findIndivCriteria;
			bdgIndiv.MaxRows = 20;
			bdgIndiv.Visible = true;
			bdgIndiv.DataBind();
			
			lblSearchHintIndiv.Visible = true;
		}

		private void PopulateCombo(string lookupName, string filter, DropDownList combo, bool addBlank)
		{
			combo.Items.Clear();
			BnbLookupDataTable lookup = BnbEngine.LookupManager.GetLookup(lookupName);
			DataView lookupView = new DataView(lookup);
			lookupView.RowFilter = filter;
			lookupView.Sort = "Description";
			combo.DataSource = lookupView;
			if (lookup.KeyType == LookupTableKeyType.GuidKey)
				combo.DataValueField = "GuidID";
			else
				combo.DataValueField = "IntID";

			combo.DataTextField = "Description";
			combo.DataBind();
			if (addBlank)
			{
				combo.Items.Add(new ListItem("","0"));
			}
		}

		
	}
}
