using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Text;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboDomainObjects;
using BonoboWebControls;
using BonoboWebControls.DataGrids;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for IndividualDonorPageNewTab.
	/// </summary>
	public partial class IndividualPage : System.Web.UI.Page
	{
		protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox7;
		protected BonoboWebControls.DataControls.BnbDomainObjectHyperlink hyperlinkPartner;
		protected BonoboWebControls.DataControls.BnbTextBox BnbTextBox8;
		protected BonoboWebControls.DataControls.BnbDateBox BnbDateBox2;
		protected BonoboWebControls.DataControls.BnbTextBox bbbb;
		protected BonoboWebControls.DataControls.BnbCheckBox BnbCheckBox1;
		protected UserControls.Title titleBar;
		protected System.Web.UI.WebControls.Panel Panel1;
		protected System.Web.UI.WebControls.Panel Panel2;
		protected System.Web.UI.WebControls.Panel Panel3;
		protected System.Web.UI.WebControls.Panel Panel4;
		protected System.Web.UI.WebControls.Panel Panel10;
		protected System.Web.UI.WebControls.Panel Panel5;
		protected UserControls.NavigationBar navigationBar;
	
		private BnbWebFormManager bnb;

		protected void Page_Load(object sender, System.EventArgs e)
		{

		
#if DEBUG			
			if(Request.QueryString.ToString() == "")
			{
				//this reloads the page with a specific ID when debugging
//				BnbWebFormManager.ReloadPage("BnbIndividual=D72BFDD9-AADF-11D4-908F-00508BACE998");
				BnbWebFormManager.ReloadPage("BnbIndividual=989CE9F9-587B-4DC0-9764-E3C11A6A3D49");
			}
#endif
          
		navigationBar.RootDomainObject = "BnbIndividual";
		BnbWorkareaManager.SetPageStyleOfUser(this);

			// re-direct to wizard if new mode
			if (this.Request.QueryString["BnbIndividual"] != null &&
				this.Request.QueryString["BnbIndividual"].ToLower() == "new")
			{
				doRedirectToIndividualWizard();
			}

			// Put user code to initialize the page here
			bnb = new BnbWebFormManager(this,"BnbIndividual");

			// tweak logging settings
			bnb.PageNameOverride = "Individual";
			bnb.LogPageHistory = true;
			// need to turn off HideHtmlBody due to tabs
			bnb.HideHtmlBodyOnLoad = false;

			doAddressLabelAndContactNumbers();
			
			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;

				// work out which tab to show based on where we came from
				if (this.Request.UrlReferrer != null)
				{
					string referrer = this.Request.UrlReferrer.AbsolutePath;
					if (referrer.EndsWith("AddressPage.aspx")
						|| referrer.EndsWith("WizardNewWorkAddress.aspx"))
						BnbTabControl1.SelectedTabID = AddressTab.ID;
					if (referrer.EndsWith("ContactBlockPage.aspx"))
						BnbTabControl1.SelectedTabID = ContactBlockingTab.ID;
					if (referrer.EndsWith("FundingPage.aspx"))
						BnbTabControl1.SelectedTabID = SponsorshipTab.ID;
					if (referrer.EndsWith("ActionPage.aspx"))
						BnbTabControl1.SelectedTabID = ActionsTab.ID;
					if (referrer.EndsWith("EventPage.aspx") 
						|| referrer.EndsWith("AttendancePage.aspx")
						|| referrer.EndsWith("EventPersonnelPage.aspx"))
						BnbTabControl1.SelectedTabID = EventsTab.ID;
				}

                //setting the correct tab if it's been requested from Menu page - our volunteers tab
                if (this.Request.QueryString["TAB"] != null)
                {
                    string selectab = this.Request.QueryString["TAB"].ToString();
                    if (selectab.ToUpper() == "VOLADDR")
                        BnbTabControl1.SelectedTabID = AddressTab.ID;
                }

				doPopulateAddressType();

			}

            doNewActionHyperLink();
			doExtendAddressTypeControl();

			// call shared code to show any view warnings
			Utilities.ShowViewWarnings(bnb, lblViewWarnings);

			//Set new d & p hyperlinks
			BnbIndividual indiv = bnb.GetPageDomainObject("BnbIndividual") as BnbIndividual;
			hypNewDP.NavigateUrl="./WizardNewDP.aspx?BnbIndividual="+indiv.ID.ToString();

			doDAndPGridHyperlinks();
			doEventGridHyperlinks();
			doAddressCompanyHyperlinks();
			doConfidentialAddresses();
			doApplicationHyperlink();
			doPartnerHyperlink();
		}

        private void doNewActionHyperLink()
        {
            BnbIndividual indiv = bnb.GetPageDomainObject("BnbIndividual") as BnbIndividual;
            if (indiv.Applications.Count < 1)
            {
                BnbDataGridForView3.NewLinkVisible = false;
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			//
			InitializeComponent();
			BnbDataButtons1.NewClick +=new EventHandler(BnbDataButtons1_NewClick);
			base.OnInit(e);
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void doPopulateAddressType()
		{
			Utilities.PopulateAddressTypeDropDown(drpNewAddressType);
			
		}

		private void doExtendAddressTypeControl()
		{
			string indID = Request.QueryString["BnbIndividual"];
			if (indID != "" && indID.ToLower() != "new")
			{
				string url = String.Format("AddressPage.aspx?BnbIndividual={0}&BnbIndividualAddress=new&PageState=new",
					indID);
				drpNewAddressType.Attributes.Add("onchange",
					String.Format("javascript:location.href='{0}&AddressTypeID='+this.value;",
					url)); 
			}
			else
			{
				drpNewAddressType.Enabled = false;
			}
			
		}

		private void doRedirectToIndividualWizard()
		{
			if (this.Request.QueryString["BnbIndividual"] != null &&
				this.Request.QueryString["BnbIndividual"].ToLower() == "new")
				Response.Redirect("WizardNewIndividual.aspx");
		}

		private void doAddressLabelAndContactNumbers()
		{
			//building up a label of all the address details
			BnbIndividual indiv = bnb.GetPageDomainObject("BnbIndividual") as BnbIndividual;
			BnbAddress currentAddress = indiv.CurrentIndividualAddress.Address;
			string[] addressLines = currentAddress.GetAddressLines();

			StringBuilder stbAddress = new StringBuilder();
			foreach(string line in addressLines)
				stbAddress.Append(line + "<br/>");

			
			if ((currentAddress.GetTelephone(indiv) != null) &&
				(currentAddress.GetTelephone(indiv) != ""))
				stbAddress.Append(currentAddress.GetTelephone(indiv) + "<br/>");
				
			if ((currentAddress.GetFax(indiv) != null) &&
				(currentAddress.GetFax(indiv) != ""))
				stbAddress.Append("Fax: " + currentAddress.GetFax(indiv) + "<br/>");
	
			uxCurrentAddressLabel.Text = stbAddress.ToString();

			if(indiv.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Email)!= null)
				lblEmail.Text = indiv.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Email).ContactNumber;
			if(indiv.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Mobile)!= null)	
				lblMobile.Text = indiv.ContactNumbers.FindContactNumberType(BnbConst.ContactNumber_Mobile).ContactNumber;
	

		}

		private void doEventGridHyperlinks()
		{
         
			// add extra hyperlinks into the two event grids
			BnbDataGridHyperlink eventHyperCol = new BnbDataGridHyperlink();
			eventHyperCol.ColumnNameToRenderControl = "tblEvent_EventDescription";
			eventHyperCol.HrefTemplate = "EventPage.aspx?BnbEvent={0}";
			eventHyperCol.KeyColumnNames = new string[] {"tblEvent_EventID"};
			uxAttendanceDataGrid.ColumnControls.Add(eventHyperCol);

			BnbDataGridHyperlink eventHyperCol2 = new BnbDataGridHyperlink();
			eventHyperCol2.ColumnNameToRenderControl = "tblEvent_EventDescription";
			eventHyperCol2.HrefTemplate = "EventPage.aspx?BnbEvent={0}";
			eventHyperCol2.KeyColumnNames = new string[] {"tblEventPersonnel_EventID"};
			uxEventPersonnelDataGrid.ColumnControls.Add(eventHyperCol2);

		}


		private void doConfidentialAddresses()
		{
			BnbIndividual ind = bnb.GetPageDomainObject("BnbIndividual") as BnbIndividual;
			if (ind != null && ind.Additional != null)
			{
				if (ind.Additional.ConfidentialAddress 
					&& !BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_ViewConfidentialAddress))
				{
					// hide the address grids and show a message instead
					uxAddressLiveGrid.Visible = false;
					uxAddressOldGrid.Visible = false;
					uxLiveAddressConfidentialLabel.Visible = true;
					uxOldAddressConfidentialLabel.Visible = true;
					uxLiveAddressConfidentialLabel.Text = "(Confidential)";
					uxOldAddressConfidentialLabel.Text = "(Confidential)";
					// also disable the new address combo
					drpNewAddressType.Enabled = false;
				}
			}
		}

		private void doAddressCompanyHyperlinks()
		{
			// add extra hyperlinks into the two event grids
			BnbDataGridHyperlink addressCompanyHyperCol = new BnbDataGridHyperlink();
			addressCompanyHyperCol.ColumnNameToRenderControl = "Custom_AddressOrg";
			addressCompanyHyperCol.HrefTemplate = "OrganisationPage.aspx?BnbCompany={0}";
			addressCompanyHyperCol.KeyColumnNames = new string[] {"tblCompany_CompanyID"};
			uxAddressLiveGrid.ColumnControls.Add(addressCompanyHyperCol);

			BnbDataGridHyperlink addressCompanyHyperCol2 = new BnbDataGridHyperlink();
			addressCompanyHyperCol2.ColumnNameToRenderControl = "Custom_AddressOrg";
			addressCompanyHyperCol2.HrefTemplate = "OrganisationPage.aspx?BnbCompany={0}";
			addressCompanyHyperCol2.KeyColumnNames = new string[] {"tblCompany_CompanyID"};
			uxAddressOldGrid.ColumnControls.Add(addressCompanyHyperCol2);

		}


		private void doDAndPGridHyperlinks()
		{
			// add extra hyperlinks into the two event grids
			BnbDataGridHyperlink hyperCol = new BnbDataGridHyperlink();
			hyperCol.ColumnNameToRenderControl = "tblIndividualKeyInfo_old_indv_id";
			hyperCol.HrefTemplate = "IndividualPage.aspx?BnbIndividual={0}";
			// use the tblIndividualAdditional id, so that hyperlinks only
			// appear for relatives with individual additional records
			hyperCol.KeyColumnNames = new string[] {"tblIndividualAdditional_IndividualID"};
			uxRelativesGrid.ColumnControls.Add(hyperCol);

		}

		
		private void doApplicationHyperlink()
		{
			BnbIndividual indiv = bnb.GetPageDomainObject("BnbIndividual") as BnbIndividual;
			if (indiv!= null)
				uxNewApplicationHyper.NavigateUrl="./WizardNewEnquiry.aspx?BnbIndividual="+indiv.ID.ToString();

		}

		private void doPartnerHyperlink()
		{
			BnbIndividual indiv = bnb.GetPageDomainObject("BnbIndividual") as BnbIndividual;
			if (indiv!= null)
			{
				if (indiv.Partner != null)
				{
					BnbIndividual partner = indiv.Partner;
					uxPartnerHyper.Text = String.Format("{0} - {1}", partner.InstanceDescription, 
						partner.PartnerTypeWithStatus);
					if (partner.IndividualType == BnbIndividual.BnbIndividualType.HasAdditional)
						uxPartnerHyper.NavigateUrl = String.Format("./IndividualPage.aspx?BnbIndividual={0}",
							partner.ID);
					else
						uxPartnerHyper.NavigateUrl = null;
				}
				else
				{
					uxPartnerHyper.Text = "(None)";
					uxPartnerHyper.NavigateUrl = null;
				}
			}
		}

		private void BnbDataButtons1_NewClick(object sender, EventArgs e)
		{
			// re-directing in this way allows the wizard to work out the urlreferrer
			this.SmartNavigation = false;
			Response.Redirect("WizardNewIndividual.aspx");
		}
	}
}
