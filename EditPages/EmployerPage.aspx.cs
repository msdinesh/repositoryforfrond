using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;
using BonoboEngine;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for EmployerPage.
    /// </summary>
    public partial class EmployerPage : System.Web.UI.Page
    {
        //protected System.Web.UI.WebControls.PlaceHolder titlePlaceHolder;

        //protected UserControls.SFNavigationBar SFNavigationBar1;
          protected void Page_Load(object sender, System.EventArgs e)
        {
            // re-direct to wizard if new mode
            if (this.Request.QueryString["BnbEmployer"] != null && this.Request.QueryString["BnbEmployer"].ToLower() == "new")
            {
                string valueOfOrganisation = Request.QueryString["BnbOrganisation"];
                Response.Redirect("WizardNewEmployer.aspx?BnbOrganisation=" + valueOfOrganisation);
            }
            BnbWebFormManager bnb = null;

#if DEBUG
            // this part will not be compiled in Release Mode; only in Debug Mode
            if (Request.ServerVariables["QUERY_STRING"] == "")
            {
                BnbWebFormManager.ReloadPage("BnbEmployer=282da8d0-5835-11d6-ad3d-000102769f09");
                //"BnbEmployer=ABFEAAF2-AAE8-11D4-908F-00508BACE998");
            }
            else
            {
                bnb = new BnbWebFormManager(this, "BnbEmployer");
            }
#else
			// but this part will compile in Release Mode
			bnb = new BnbWebFormManager(this,"BnbEmployer");
#endif
            // turn on logging for this page
            bnb.LogPageHistory = true;

            //<Integartion>
            bnb.PageNameOverride = "Employer";
            BnbWorkareaManager.SetPageStyleOfUser(this);

            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null)
                    titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
            }
            //</Integartion>
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new EventHandler(EmployerPage_Init);

        }
        #endregion

        private void EmployerPage_Init(object sender, EventArgs e)
        {
            navigationBar.RootDomainObject = "BnbEmployer";
        }
    }
}
