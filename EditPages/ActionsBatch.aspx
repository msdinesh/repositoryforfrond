<%@ Page language="c#" Codebehind="ActionsBatch.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.ActionsBatch" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrols" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent"><uc1:title id="titleBar" runat="server" imagesource="../Images/batchprocess24silver.gif"></uc1:title>
				<TABLE class="fieldtable" width="300" id="Table1" border="0">
					<TR>
						<TD class="label" width="25%">Action Type</TD>
						<TD width="25%"><bnbdatacontrol:bnbstandalonelookupcontrol id="dropdownActionType" runat="server" AutoPostBack="True" Height="22px" Width="259px"
								DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuContactType" CssClass="lookupcontrol"></bnbdatacontrol:bnbstandalonelookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" width="25%">VSO Description</TD>
						<TD width="25%"><bnbdatacontrol:bnbstandalonelookupcontrol id="dropdownDescription" runat="server" AutoPostBack="True" Height="22px" Width="259"
								DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuContactDescription" CssClass="lookupcontrol"
								ColumnRelatedToParent="ContactTypeId" ParentControlID="dropdownActionType"></bnbdatacontrol:bnbstandalonelookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" width="25%">Information</TD>
						<TD width="25%"><bnbdatacontrol:bnbtextbox id="textBoxInformation" runat="server" Height="78px" Width="260px" CssClass="textbox"
								StringLength="0" Rows="3" MultiLine="True" DataMember="BnbIndividualContactBlock.Reason"></bnbdatacontrol:bnbtextbox>
						</TD>
					</TR>
					<TR>
						<TD class="label" width="25%">Status</TD>
						<TD width="25%"><bnbdatacontrol:bnbstandalonelookupcontrol id="dropdownStatus" runat="server" AutoPostBack="False" Height="22px" Width="259px"
								DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuContactStatus" CssClass="lookupcontrol"></bnbdatacontrol:bnbstandalonelookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" width="25%">Sent Via</TD>
						<TD width="25%"><bnbdatacontrol:bnbstandalonelookupcontrol id="dropdownSentVia" runat="server" AutoPostBack="False" Height="22px" Width="259px"
								DataTextField="Description" ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuSentVia" CssClass="lookupcontrol"></bnbdatacontrol:bnbstandalonelookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" width="25%">Actions relate to:</TD>
						<TD width="25%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:radiobutton id="radioIndividual" runat="server" Checked="True" Text="Individual" GroupName="RelatedTo"></asp:radiobutton>&nbsp;&nbsp;&nbsp;&nbsp; 
							&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:radiobutton id="radioVolunteer" runat="server" Text="Volunteer" GroupName="RelatedTo"></asp:radiobutton></TD>
					</TR>
					<TR>
						<TD class="label" width="25%">CSV File</TD>
						<TD width="25%"><INPUT id="fileUploader" style="WIDTH: 256px; HEIGHT: 19px" type="file" size="23" name="File1"
								runat="server">
						</TD>
					</TR>
					<TR>
						<TD width="25%"></TD>
						<TD width="25%"><asp:button id="buttonCreateActions" runat="server" Width="96px" Text="Create Actions" onclick="buttonCreateActions_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:button id="buttonCancel" runat="server" Width="96px" Text="Cancel" onclick="buttonCancel_Click"></asp:button></TD>
					</TR>
					<TR>
						<td width="25%"></td>
						<td align="left" width="25%"><asp:label id="successLabel" runat="server" Height="22px" Width="259px" Font-Bold="True" ForeColor="blue"></asp:label></td>
					</TR>
					<TR>
						<td width="25%"></td>
						<td align="left" width="25%"><asp:label id="errorLabel" runat="server" Height="22px" Width="259px" Font-Bold="True" ForeColor="red"></asp:label></td>
					</TR>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
