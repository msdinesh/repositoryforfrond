<%@ Register TagPrefix="bnbpagecontrols" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>

<%@ Page Language="c#" Codebehind="JobPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.JobPage" %>

<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register Src="../UserControls/NavigationBar.ascx" TagName="NavigationBar" TagPrefix="uc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar" TitleText="JobPage" runat="server" ImageSource="../Images/bnbrole24silver.gif">
            </uc1:Title>
            <uc2:NavigationBar ID="navigationBar" runat="server"></uc2:NavigationBar>
            <table style="margin: 0px" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="left" width="50%">
                        <bnbpagecontrols:BnbDataButtons ID="BnbDataButtons1" runat="server" UndoText="Undo"
                            SaveText="Save" EditAllText="Edit All" NewText="New" HeightAllButtons="25px"
                            UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px" CssClass="databuttons">
                        </bnbpagecontrols:BnbDataButtons>
                    </td>
                    <td align="right" width="50%">
                        <a href="../FindGeneral.aspx?FindTypeID=1" target="_top">Find Job</a></td>
                </tr>
            </table>
            <bnbpagecontrols:BnbMessageBox ID="BnbMessageBox1" runat="server" CssClass="messagebox">
            </bnbpagecontrols:BnbMessageBox>
            <bnbpagecontrols:BnbTabControl ID="BnbTabControl1" runat="server"></bnbpagecontrols:BnbTabControl>
            <asp:Panel ID="JobDetailsTab" runat="server" CssClass="tcTab" BnbTabName="Job Details">
                <p>
                    <table class="fieldtable" id="Table1" width="100%" border="0">
                        <tr>
                            <td class="label" width="50%">
                                Job Reference</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox1" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" DataMember="BnbRole.FullJobReference" ReadOnly="True" DataProperty="FullJobReference"
                                    DomainObject="BnbRole" MultiLine="false" Rows="1"></bnbdatacontrol:BnbTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="height: 25px" width="50%">
                                Employer Name</td>
                            <td style="height: 25px" width="50%">
                                <bnbdatacontrol:BnbDomainObjectHyperlink ID="BnbDomainObjectHyperlink1" runat="server"
                                    DataMember="BnbEmployer.EmployerName" DataProperty="EmployerName" DomainObject="BnbEmployer"
                                    Target="_top" GuidProperty="BnbEmployer.ID" RedirectPage="EmployerPage.aspx"
                                    HyperlinkText="[BnbDomainObjectHyperlink]"></bnbdatacontrol:BnbDomainObjectHyperlink>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="50%">
                                Job Title</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox3" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" DataMember="BnbRole.RoleName" DataProperty="RoleName" DomainObject="BnbRole"
                                    MultiLine="false" Rows="1"></bnbdatacontrol:BnbTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="50%">
                                Job Abbreviation</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox4" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" DataMember="BnbRole.RoleAbbrev" DataProperty="RoleAbbrev" DomainObject="BnbRole"
                                    MultiLine="false" Rows="1"></bnbdatacontrol:BnbTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="50%">
                                Job Skills</td>
                            <td width="50%">
                                <p>
                                    <bnbdatacontrol:BnbLookupControl ID="bnblckRoleSkill" runat="server" CssClass="lookupcontrol"
                                        Height="22px" Width="250px" DataMember="BnbRoleSkill.SkillID" DataTextField="Description"
                                        ListBoxRows="4" LookupTableName="vlkuBonoboRoleSkillWithGroupAbbrev" LookupControlType="ComboBox">
                                    </bnbdatacontrol:BnbLookupControl>
                                    <bnbdatagrid:BnbDataGridForDomainObjectList ID="BnbDataGridForDomainObjectList1"
                                        runat="server" CssClass="datagrid" Width="200px" DataMember="BnbRole.RoleSkills"
                                        DataProperties="SkillID [BnbLookupControl:vlkuBonoboRoleSkillWithGroupAbbrev]"
                                        DataGridType="Editable" DeleteButtonVisible="false" EditButtonVisible="true"
                                        AddButtonText="Add" EditButtonText="Edit" AddButtonVisible="True"></bnbdatagrid:BnbDataGridForDomainObjectList>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="50%">
                                Comments</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbTextBox ID="BnbTextBox5" runat="server" CssClass="textbox" Height="20px"
                                    Width="250px" DataMember="BnbRole.Comments" DataProperty="Comments" DomainObject="BnbRole"
                                    MultiLine="True" Rows="3"></bnbdatacontrol:BnbTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="height: 20px" width="50%">
                                Logged by</td>
                            <td style="height: 20px" width="50%">
                                <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl1" runat="server" CssClass="lookupcontrol"
                                    Height="22px" Width="250px" DataMember="BnbRole.CreatedBy" DataProperty="CreatedBy"
                                    DomainObject="BnbRole" DataTextField="Description" ListBoxRows="4" LookupTableName="vlkuBonoboUser"
                                    LookupControlType="ComboBox" AlwaysReadOnly="False" ControlType="ComboBox"></bnbdatacontrol:BnbLookupControl>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" width="50%">
                                Logged On</td>
                            <td width="50%">
                                <bnbdatacontrol:BnbDateBox ID="BnbDateBox1" runat="server" CssClass="datebox" Height="20px"
                                    Width="90px" DataMember="BnbRole.CreatedOn" ReadOnly="True" DataProperty="CreatedOn"
                                    DomainObject="BnbRole" DateFormat="dd/mm/yyyy" DateCulture="en-GB"></bnbdatacontrol:BnbDateBox>
                            </td>
                        </tr>
                    </table>
                </p>
            </asp:Panel>
            <asp:Panel ID="PlacementsTab" runat="server" CssClass="tcTab" BnbTabName="Placements">
                <p>
                    <asp:HyperLink ID="hypNewPlacement" runat="server">New Placement</asp:HyperLink></p>
                    <bnbdatagrid:BnbDataGridForView ID="BnbDataGridForView1" runat="server" CssClass="datagrid"
                        Width="100%" DomainObject="BnbPosition" RedirectPage="PlacementPage.aspx" ViewLinksVisible="True"
                        GuidKey="tblPosition_PositionID" NewLinkVisible="false" 
                        FieldName="tblPosition_RoleID" QueryStringKey="BnbRole" HrefForViewHLink="PlacementPage.aspx?BnbRole="
                        ViewHLinkGuid="BnbPosition" QueryStringGuid="BnbRole" ViewName="vwBonoboJobPositions"
                        NewLinkReturnToParentPageDisabled="True"></bnbdatagrid:BnbDataGridForView>
                </p>
            </asp:Panel>
            <p>
                &nbsp;</p>
        </div>
    </form>
</body>
</html>
