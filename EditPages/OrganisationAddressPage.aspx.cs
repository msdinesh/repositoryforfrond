using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;
using BonoboDomainObjects;
using System.Text;
using BonoboEngine;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for OrganisationAddressPage.
	/// </summary>
	public partial class OrganisationAddressPage : System.Web.UI.Page
	{
		#region Controls declaration
		protected UserControls.Title titleBar;
		
		#endregion Controls declaration
		
		#region page variables declaration
		BnbWebFormManager bnb = null;
		#endregion page variables declaration

		#region Page load
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
#if DEBUG	
			if(Request.QueryString.ToString() == "")
				//bnb = new BnbWebFormManager("BnbCompany=0564E937-7660-11D7-B700-0002A5E02600&BnbCompanyAddress=0564E939-7660-11D7-B700-0002A5E02600");
				BnbWebFormManager.ReloadPage("BnbCompany=0564E937-7660-11D7-B700-0002A5E02600&BnbCompanyAddress=0564E939-7660-11D7-B700-0002A5E02600");
			else 		
				bnb = new BnbWebFormManager(this,"BnbCompanyAddress");
#else	
			bnb = new BnbWebFormManager(this,"BnbCompanyAddress");
#endif
			bnb.EditModeEvent +=new EventHandler(bnb_EditModeEvent);
			bnb.ViewModeEvent+=new EventHandler(bnb_ViewModeEvent);
			bnb.SaveEvent +=new EventHandler(bnb_SaveEvent);


			BnbWorkareaManager.SetPageStyleOfUser(this);
			bnb.LogPageHistory = true;
			if (!this.IsPostBack)
			{
				if (bnb.ObjectTree != null)
					titleBar.TitleText = bnb.PageTitleDescriptionCurrent;

				doLoadTelephone();
                //Added newly for role based access
                DoMapVisibility();
			}
			// this is needed so that the postcode find button does not trigger a regular postback
			uxPostcodeSearch.Attributes.Add("onclick","javascript:__doBonoboPostBack(this.name,'keepediting');");
			uxAddressLine1Search.Attributes.Add("onclick","javascript:__doBonoboPostBack(this.name,'keepediting');");
            AssignGeoDetails("LoadValues");
		}
		#endregion Page load

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			this.uxAddressLine1Search.Click +=new EventHandler(this.uxPostcodeSearch_Click);
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

	
		/* this not needed after VAM-378
		private void doNewAddressFillIn()
		{
			// is address transfer filled in?
			string addressTransfer = Request.QueryString["AddressTransfer"];
			if (addressTransfer != null)
			{
				// pull the address details from the wizard out of the Session[]
				Hashtable addressTrans = (Hashtable)Session["AddressTransfer"];
				
				BnbAddress a = bnb.GetPageDomainObject("BnbCompanyAddress.Address",true) as BnbAddress;
				a.AddressLine1 = (string)addressTrans["Line1"];
				a.AddressLine2 = (string)addressTrans["Line2"];
				a.AddressLine3 = (string)addressTrans["Line3"];
				a.PostCode = (string)addressTrans["PostCode"];
				a.County = (string)addressTrans["County"];
				a.CountryID = (Guid)addressTrans["CountryID"];

			}
		}
		*/
		
		/// <summary>
		/// fires when the web form manager goes into edit mode
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void bnb_EditModeEvent(object sender, EventArgs e)
		{
			doDisplayTelephoneNumbers(true);
			// make the find button visible
			uxPostcodeSearch.Visible = true;
			uxAddressLine1Search.Visible = true;
            if (HasPermissionToLocateOnMap())
            {
                btnShowLocation.Visible = true;
            }
			// was the address list clicked?
			// Note that WebFormManagerMode must be set to true on the addresslookupcomposite for
			// this to work in the EditModeEvent
			if (BnbWebFormManager.EventTarget == "uxAddressLookupComposite")
			{
				uxLine1.OverrideText = uxAddressLookupComposite.Line1;
				uxLine2.OverrideText = uxAddressLookupComposite.Line2;
				uxLine3.OverrideText = uxAddressLookupComposite.Line3;
				uxLine4.OverrideText = uxAddressLookupComposite.Town;
				uxCounty.OverrideText = uxAddressLookupComposite.County;
				uxPostcode.OverrideText = uxAddressLookupComposite.PostCode;
			}
            //Set Page mode for GMap
            SetGeoMode("new/edit");
		}

		/// <summary>
		/// fires when the web form manager goes into view mode
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void bnb_ViewModeEvent(object sender, EventArgs e)
		{
			// make the find button invisible
			uxPostcodeSearch.Visible = false;
			uxAddressLine1Search.Visible = false;
            btnShowLocation.Visible = false;
			uxAddressLookupComposite.Visible = false;

			doDisplayTelephoneNumbers(false);
            doLoadGeographicalDetails();
            //Set Page mode for GMap
            SetGeoMode("view");
            //For capturing Geographical locations
            AssignGeoDetails("ViewValues");
		}

		protected void uxPostcodeSearch_Click(object sender, System.EventArgs e)
		{
			// need to get postcode and country from request form
			 
			string postcode = Request.Form["uxPostcode"];
			string countryID = Request.Form["uxCountry"]; 
			string line1 = Request.Form["uxLine1"];
			
			
			// do search and activate control
			uxAddressLookupComposite.CountryID = new Guid(countryID);
			uxAddressLookupComposite.PostCode = postcode;
			uxAddressLookupComposite.Line1 = line1;
			uxAddressLookupComposite.PerformSearch();
			uxAddressLookupComposite.Visible = true;
		}

		/// <summary>
		/// Tel and Fax numbers are represented by separate BnbContactNumber objects.
		/// They cannot be bound using the BnbWebFormManager, so are implemented as normal
		/// ASP.NET controls on the page
		/// This finds the right ones and loads the data into the controls
		/// </summary>
		private void doLoadTelephone()
		{
			BnbCompanyAddress caLink = bnb.GetPageDomainObject("BnbCompanyAddress") as BnbCompanyAddress;
			if (caLink != null)
			{
				// pass 'null' individual owner to get main tel and main fax for a BnbCompany
				string tel = caLink.Address.GetTelephone(null);
				string fax = caLink.Address.GetFax(null);
				// there are different controls depending on edit mode
				if (tel!= null)
				{
					uxTelephoneTextBox.Text = tel;
					uxTelephoneLabel.Text = tel;
				}
				else
				{
					uxTelephoneLabel.Text = "";
					uxTelephoneTextBox.Text = "";
				}
				if (fax!=null)
				{
					uxFaxTextBox.Text = fax;
					uxFaxLabel.Text = fax;
				}
				else
				{
					uxFaxLabel.Text = "";
					uxFaxTextBox.Text = "";
				}
			}
			else
			{
				// if no object, must be new mode
				uxTelephoneLabel.Text = "";
				uxTelephoneTextBox.Text = "";
				uxFaxLabel.Text = "";
				uxFaxTextBox.Text = "";
			}
		}

		/// <summary>
		/// Tel and Fax numbers are represented by separate BnbContactNumber objects.
		/// They cannot be bound using the BnbWebFormManager, so are implemented as normal
		/// ASP.NET controls on the page
		/// This displays the correct controls depending on edit mode
		/// </summary>
		private void doDisplayTelephoneNumbers(bool editMode)
		{
			uxTelephoneTextBox.Visible = editMode;
			uxFaxTextBox.Visible = editMode;
			uxTelephoneLabel.Visible = !editMode;
			uxFaxLabel.Visible = !editMode;

			
		}

		/// <summary>
		/// Called by the webformmanager when saving
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void bnb_SaveEvent(object sender, EventArgs e)
		{
			this.doSaveTelephone();
            this.doSaveGeographicalDetails();
		}

		/// <summary>
		/// Tel and Fax numbers are represented by separate BnbContactNumber objects.
		/// They cannot be bound using the BnbWebFormManager, so are implemented as normal
		/// ASP.NET controls on the page
		/// This checks for data in the controls and saves the objects
		/// </summary>
		private void doSaveTelephone()
		{
			// find the main object
			BnbCompanyAddress caLink = bnb.GetPageDomainObject("BnbCompanyAddress", true) as BnbCompanyAddress;
			
			if (uxTelephoneTextBox.Text != "")
				caLink.Address.SetTelephone(null, uxTelephoneTextBox.Text);
			else
				caLink.Address.RemoveTelephone(null);

			if (uxFaxTextBox.Text != "")
				caLink.Address.SetFax(null, uxFaxTextBox.Text);
			else
				caLink.Address.RemoveFax(null);

		}

        /// <summary>
        /// Geographical details cannot be bound using the BnbWebFormManager, so are implemented as normal
        /// ASP.NET controls on the page
        /// This finds the right ones and loads the data into the controls
        /// </summary>
        private void doLoadGeographicalDetails()
        {
            if (HasPermissionToLocateOnMap())
            {
                BnbCompanyAddress caLink = bnb.GetPageDomainObject("BnbCompanyAddress") as BnbCompanyAddress;
                if (caLink != null)
                {
                    float lat = caLink.Address.Latitude;
                    float lng = caLink.Address.Longitude;

                    showLocation.Latitude = lat.ToString();
                    showLocation.Longitude = lng.ToString();
                }

            }
        }

        /// <summary>
        /// Latitude, Longitude cannot be bound using the BnbWebFormManager, 
        /// so are implemented as normal
        /// ASP.NET controls on the page
        /// This checks for data in the controls and saves the objects
        /// </summary>
        private void doSaveGeographicalDetails()
        {
            if (HasPermissionToLocateOnMap())
            {
                // find the main object
                BnbCompanyAddress caLink = bnb.GetPageDomainObject("BnbCompanyAddress", true) as BnbCompanyAddress;

                if (showLocation.Latitude.Trim() != "")
                    caLink.Address.Latitude = Convert.ToSingle(showLocation.Latitude);
                else
                    //Setting null value for validation
                    caLink.Address.Latitude = Convert.ToSingle(0.0);

                if (showLocation.Longitude.Trim() != "")
                    caLink.Address.Longitude = Convert.ToSingle(showLocation.Longitude);
                else
                    //Setting null value for validation
                    caLink.Address.Longitude = Convert.ToSingle(0.0);

            }
        }

        //For capturing Geographical locations
        //Generate client script for setting Geographical locations
        private void AssignGeoDetails(string scriptID)
        {
            if (HasPermissionToLocateOnMap())
            {
                showLocation.CssName = "gmaptableNormal";
                StringBuilder script = new StringBuilder();
                script.Append("pageName = 'OrganisationAddressPage';");
                if (!(showLocation.Latitude.Trim() == string.Empty))
                {
                    ViewState["Latittude"] = showLocation.Latitude;
                    ViewState["Longitude"] = showLocation.Longitude;
                }

                if (!(ViewState["Latittude"] == null))
                {
                    string latitude = ViewState["Latittude"].ToString();
                    string longitude = ViewState["Longitude"].ToString();

                    script.Append("latVal = " + latitude + ";lngVal = " + longitude + ";");
                }
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), scriptID, script.ToString(), true);
            }
        }

        //For capturing Geographical locations
        //Set page mode to control GMap 
        private void SetGeoMode(string mode)
        {
            if (HasPermissionToLocateOnMap())
            {
                string strScript = "mode='" + mode + "';";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "SetMode", strScript, true);
            }
        }

        /// <summary>
        /// Check permisssion to access the Google map functionality
        /// </summary>
        private bool HasPermissionToLocateOnMap()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_LocateAddressOnMap))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Added newly for role based access
        /// </summary>
        private void DoMapVisibility()
        {
            if (HasPermissionToLocateOnMap())
            {
                headingGeoDetails.Visible = true;
                showLocation.Visible = true;
                btnShowLocation.Visible = true;
            }
            else
            {
                
                headingGeoDetails.Visible = false;
                showLocation.Visible = false;
                btnShowLocation.Visible = false;
            }
        }
	}
}
