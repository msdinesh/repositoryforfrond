using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;
using System.Text;

namespace Frond.EditPages
{
	/// <summary>
	/// Summary description for WizardNewOrganisation.
	/// </summary>
	public partial class WizardNewOrganisation : System.Web.UI.Page
	{
		//protected System.Web.UI.WebControls.Panel panelCompanyChoose;
		protected System.Web.UI.WebControls.TextBox txtAddressLine1;
		protected System.Web.UI.WebControls.TextBox txtAddressLine2;
		protected System.Web.UI.WebControls.TextBox txtAddressLine3;
		protected System.Web.UI.WebControls.TextBox txtAddressLine4;
		protected System.Web.UI.WebControls.TextBox txtPostcode;
		protected System.Web.UI.WebControls.TextBox txtTelephone;
		
		protected BonoboWebControls.ServicesControls.BnbAddressLookupComposite BnbAddressLookup;
		protected System.Web.UI.WebControls.Button cmdFind;
		protected UserControls.WizardButtons wizardButtons;
		protected UserControls.OrganisationAddressPanel uxOrganisationAddressUserControl;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			BnbEngine.SessionManager.ClearObjectCache();

			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
			
			wizardButtons.AddPanel(panelZero);
			wizardButtons.AddPanel(panelOne);
			wizardButtons.AddPanel(panelTwo);

			if (!this.IsPostBack)
			{
				BonoboWebControls.BnbWorkareaManager.CheckPageAccess(this);
				initWizard();
				BonoboWebControls.BnbWebFormManager.LogAction("New Organisation Wizard", null, this);
				if (this.Request.UrlReferrer != null)
					txtHiddenReferrer.Text = this.Request.UrlReferrer.ToString();
				else
					txtHiddenReferrer.Text = "~/Menu.aspx";

			}
            //For capturing Geographical locations
            doLoadGeo();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			wizardButtons.ShowPanel +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_ShowPanel);
			wizardButtons.ValidatePanel +=new Frond.UserControls.WizardButtons.WizardValidationEventHandler(wizardButtons_ValidatePanel);
			wizardButtons.CancelWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_CancelWizard);
			wizardButtons.FinishWizard +=new Frond.UserControls.WizardButtons.WizardEventHandler(wizardButtons_FinishWizard);
			

			dataGridCompanies.ItemCommand += new DataGridCommandEventHandler(dataGridCompanies_ItemCommand);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		public void initWizard()
		{
			// country lookup on panel zero
			BnbLookupDataTable countryLookup = BnbEngine.LookupManager.GetLookup("lkuCountry");
			DataView countryView = new DataView(countryLookup);
			countryView.RowFilter = "Exclude = 0";
			countryView.Sort = "Description";
			dropDownCountry.DataSource = countryView;
			dropDownCountry.DataValueField = "GuidID";
			dropDownCountry.DataTextField = "Description";
			dropDownCountry.DataBind();           
			// vaf-62: add a blank item
			ListItem blankItem = new ListItem("<Any>","");
			dropDownCountry.Items.Insert(0,blankItem);
			Guid defaultCountryID = BnbEngine.SessionManager.GetSessionInfo().DefaultCountryID;
         	ListItem defaultItem = dropDownCountry.Items.FindByValue(defaultCountryID.ToString());
            //Code Integrated for displaying defaultcountry only to starfish users
            if ((BnbEngine.SessionManager.GetSessionInfo().WebStyleID) == 11)
			if (defaultItem != null)
				defaultItem.Selected = true;
		
			FillCombo(cmbRelationToVSO,"lkuRelationshipWithVso","Exclude = 0","IntID",true);

            // dept lookup on panel two
            BnbLookupDataTable deptLookup = BnbEngine.LookupManager.GetLookup("appDepartment");
            DataView deptView = new DataView(deptLookup);
            deptView.RowFilter = "Exclude = 0";
            deptView.Sort = "Description";
            dropDownDonorContact.DataSource = deptView;
            dropDownDonorContact.DataValueField = "IntID";
            dropDownDonorContact.DataTextField = "Description";
            dropDownDonorContact.DataBind();
            dropDownDonorContact.Items.Insert(0, "");
            /*int defaultDept = BnbEngine.SessionManager.GetSessionInfo().DepartmentID;
            foreach (ListItem loopItem in dropDownDonorContact.Items)
                if (loopItem.Value == defaultDept.ToString())
                    loopItem.Selected = true;
            */
            //TPT: Added for PGMSP-17
            doLoadGCContactUserID();
            doLoadFunderAgencyType();
		}
		
		private void FillCombo(DropDownList ComboName, string LookupName, string Filter, string DataValueFieldname, bool BlankItem)
		{
			BnbLookupDataTable dtLookup = BnbEngine.LookupManager.GetLookup(LookupName);//"lkuRelationshipWithVso");
			DataView dtView = new DataView(dtLookup);
			dtView.RowFilter = Filter;//"Exclude = 0";
			dtView.Sort = "Description";
			ComboName.DataSource = dtView;
			ComboName.DataValueField = DataValueFieldname;//"GuidID";
			ComboName.DataTextField = "Description";
			ComboName.DataBind();
			if (BlankItem)
			{
				// vaf-62: add a blank item
				ListItem blankItem = new ListItem("<Any>","");
				ComboName.Items.Insert(0,blankItem);
			}

		}
		public void SetupPanelZero()
		{

			// nothing

		}

		public void SetupPanelOne()
		{

			
			string companyNameBit = textBoxCompanyName.Text;
			if (companyNameBit.Length > 8)
				companyNameBit = companyNameBit.Substring(0,8);

			BnbTextCondition nameMatch = new BnbTextCondition("tblCompany_CompanyName", companyNameBit, BnbTextCompareOptions.AnyPartOfField);
			BnbCriteria dupCriteria = new BnbCriteria(nameMatch);
			// vaf-62
			// if country selected, add to criteria
			if (dropDownCountry.SelectedValue != "")
			{
				Guid countryID = new Guid(dropDownCountry.SelectedValue);
				BnbListCondition countryMatch = new BnbListCondition("tblAddress_CountryID", countryID);
				dupCriteria.QueryElements.Add(countryMatch);
			}

			BnbQuery dupQuery = new BnbQuery("vwBonoboCompanyDuplicateCheck", dupCriteria);
			BnbQueryDataSet dupDataSet = dupQuery.Execute();
			DataTable dupResults = BonoboWebControls.BnbWebControlServices.GetHtmlEncodedDataTable(dupDataSet.Tables["Results"]);
					
			if (dupResults.Rows.Count > 0)
			{
				panelDupOrg.Visible = true;
				labelNoDuplicates.Visible = false;
				dataGridCompanies.DataSource = dupResults;
				dataGridCompanies.DataBind();
			}
			else
			{
				panelDupOrg.Visible = false;
				labelNoDuplicates.Visible = true;
			}



		}
		
		public void SetupPanelTwo()
		{
			// copy country and company name from panel zero
			if (dropDownCountry.SelectedValue != "")
			{
				uxOrganisationAddressUserControl.CountryComboSelectedValue = dropDownCountry.SelectedValue;
			}
			textBoxCompanyName2.Text = textBoxCompanyName.Text;
		}

		

		private void wizardButtons_ValidatePanel(object Sender, Frond.UserControls.WizardButtons.WizardValidationEventArgs e)
		{
			if (e.CurrentPanel == 0)
			{
				
                if (textBoxCompanyName.Text == "")
                {
                    e.Proceed = false;
                    labelPanelZeroFeedback.Visible = true; 
                }
                else
                    labelPanelZeroFeedback.Visible = false;

                if (dropDownCountry.SelectedValue == "")
                {
                    e.Proceed = false;
                    labelPanelZeroFeedbackOrgCountry.Visible = true;
                }
                else
                    labelPanelZeroFeedbackOrgCountry.Visible = false;

                if (textBoxCompanyName.Text != "" && dropDownCountry.SelectedValue != "")
                {
                    e.Proceed = true;
                }
			            
			}
			
		}

		private void wizardButtons_ShowPanel(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			if (e.CurrentPanel == 0)
			{
				this.SetupPanelZero();
			}
			if (e.CurrentPanel == 1)
			{
				this.SetupPanelOne();
			}
			if (e.CurrentPanel == 2)
			{
				this.SetupPanelTwo();
			}
		}

		private void wizardButtons_CancelWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			Response.Redirect(txtHiddenReferrer.Text);
		}

		private void wizardButtons_FinishWizard(object Sender, Frond.UserControls.WizardButtons.WizardEventArgs e)
		{
			this.CreateNewCompany();
		}

		private void dataGridCompanies_ItemCommand(object source, DataGridCommandEventArgs e)
		{
			TableCell idCell = e.Item.Cells[1];
			Guid companyID = new Guid(idCell.Text);
			Response.Redirect("OrganisationPage.aspx?BnbCompany=" + companyID.ToString());
		}



		/// <summary>
		/// takes the data from the various controls on the wizard and uses
		/// it to create a totally new company.
		/// and valdiation messages are shown in labelPanelTwoFeedback.
		/// if no problems, re-directs to org donor page
		/// </summary>
		private void CreateNewCompany()
		{
			BnbEditManager em = new BnbEditManager();
			BnbCompany newComp = new BnbCompany(true);
			newComp.RegisterForEdit(em);
			newComp.CompanyName = textBoxCompanyName2.Text;
            //TPT: Added for PGMSP-17    
            if (drpDownPrimaryContact.SelectedValue != "")
                newComp.GCContactUserID = new Guid(drpDownPrimaryContact.SelectedValue);
            if (drpDownFunderAgencyType.SelectedValue != "")
                newComp.FunderAgencyTypeID = Convert.ToInt32(drpDownFunderAgencyType.SelectedValue);

			// get address data from usercontrol
			uxOrganisationAddressUserControl.SaveOrganisationAddressDetails(newComp, null, em);
            if (dropDownDonorContact.SelectedValue != "")
                newComp.KeyDonorContactID = int.Parse(dropDownDonorContact.SelectedValue);
			// add the  donor relationship
			
			
			if (cmbRelationToVSO.SelectedValue.Length > 0)
			{
				BnbCompanyRelationshipWithVSO newRel = new BnbCompanyRelationshipWithVSO(true);
				newRel.RegisterForEdit(em);	
				newRel.RelationshipWithVSOID = Convert.ToInt32(cmbRelationToVSO.SelectedValue);
				newComp.CompanyRelationshipWithVSO.Add(newRel);
			}
			try
			{
				em.SaveChanges();
				Response.Redirect("OrganisationPage.aspx?BnbCompany=" + newComp.ID.ToString());
			}
			catch(BnbProblemException pe)
			{
				lblPanelTwoFeedback.Text = "Unfortunately, the Organisation could not be created due to the following problems: <br/>";
				foreach(BnbProblem p in pe.Problems)
					lblPanelTwoFeedback.Text += p.Message + "<br/>";

			}
		
		}


        //TPT: Added for PGMSP-17
        private void doLoadGCContactUserID()
        {
            DataTable gCContactUser = BnbEngine.LookupManager.GetLookup("vlkuBonoboUser");
            gCContactUser.DefaultView.RowFilter = "Exclude = 0";
            gCContactUser.DefaultView.Sort = "Description";

            drpDownPrimaryContact.DataSource = gCContactUser;
            drpDownPrimaryContact.DataTextField = "Description";
            drpDownPrimaryContact.DataValueField = "GuidID";
            drpDownPrimaryContact.DataBind();
            drpDownPrimaryContact.Items.Insert(0, "");
        }

        private void doLoadFunderAgencyType()
        {
            DataTable funderAgencyType = BnbEngine.LookupManager.GetLookup("lkuFunderAgencyType");
            funderAgencyType.DefaultView.RowFilter = "Exclude = 0";
            funderAgencyType.DefaultView.Sort = "Description";

            drpDownFunderAgencyType.DataSource = funderAgencyType;
            drpDownFunderAgencyType.DataTextField = "Description";
            drpDownFunderAgencyType.DataValueField = "IntID";
            drpDownFunderAgencyType.DataBind();
            drpDownFunderAgencyType.Items.Insert(0, "");
        }

        //For capturing Geographical locations
        private void doLoadGeo()
        {
            if (HasPermissionToLocateOnMap())
            {
                StringBuilder script = new StringBuilder();
                script.Append("pageName ='WizardNewOrganisation';");
                //Page mode
                script.Append("mode='new/edit';");
                if (!(uxOrganisationAddressUserControl.Latitude.Trim() == string.Empty))
                {
                    ViewState["Latittude"] = uxOrganisationAddressUserControl.Latitude;
                    ViewState["Longitude"] = uxOrganisationAddressUserControl.Longitude;
                }

                if (!(ViewState["Latittude"] == null))
                {
                    string latitude = ViewState["Latittude"].ToString();
                    string longitude = ViewState["Longitude"].ToString();
                    script.Append("latVal = " + latitude + ";lngVal = " + longitude + ";");
                }
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "WNOLoadGeo", script.ToString(), true);
            }
        }

        /// <summary>
        /// Check permisssion to access the Google map functionality
        /// </summary>
        private bool HasPermissionToLocateOnMap()
        {
            if (BnbEngine.SessionManager.GetSessionInfo().HasPermission(BnbConst.Permission_LocateAddressOnMap))
                return true;
            else
                return false;
        }
	
	}
}
