<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>

<%@ Page Language="c#" Codebehind="RequestPage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.RequestsPage" %>

<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls"
    Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls"
    Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>ProgrammePage</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
    <link href="../Css/FrondDefault.css" type="text/css" rel="stylesheet" />
    <asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc1:MenuBar ID="menuBar" runat="server"></uc1:MenuBar>
        </div>
        <div id="baseContent">
            <uc1:Title ID="titleBar"  runat="server" ImageSource="../Images/bnbevent24silver.gif">
            </uc1:Title>
      
                <bnbpagecontrol:BnbDataButtons ID="BnbDataButtons1" runat="server" CssClass="databuttons"
                    NewWidth="55px" EditAllWidth="55px" SaveWidth="55px" UndoWidth="55px" HeightAllButtons="25px"
                    NewText="New" EditAllText="Edit All" SaveText="Save" UndoText="Undo"></bnbpagecontrol:BnbDataButtons>
          
                <bnbpagecontrol:BnbMessageBox ID="BnbMessageBox1" runat="server" CssClass="messagebox">
                </bnbpagecontrol:BnbMessageBox>
         
     
                <table id="Table1" width="100%" border="0" class="fieldtable">
                    <tr>
                        <td class="label" width="50%">
                            Programme Ref</td>
                        <td width="50%">
                            <bnbdatacontrol:BnbDomainObjectHyperlink ID="BnbDomainObjectHyperlink2" runat="server"
                                RedirectPage="ProgrammePage.aspx" GuidProperty="BnbProgramme.ID" HyperlinkText="[BnbDomainObjectHyperlink]"
                                Target="_top" DataMember="BnbProgramme.ProgrammeRef"></bnbdatacontrol:BnbDomainObjectHyperlink>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="50%">
                            Programme Office</td>
                        <td width="50%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl4" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataMember="BnbProgramme.ProgrammeOfficeID" LookupTableName="lkuProgrammeOffice"
                                LookupControlType="ReadOnlyLabel" ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="50%">
                            Programme Name</td>
                        <td width="50%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl5" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataMember="BnbProgramme.ProgrammeAreaID" LookupTableName="lkuProgrammeArea"
                                LookupControlType="ReadOnlyLabel" ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="50%">
                            Request Description</td>
                        <td width="50%">
                            <bnbdatacontrol:BnbTextBox ID="BnbTextBox1" runat="server" CssClass="textbox" Height="20px"
                                Width="250px" DataMember="BnbRequest.RequestDescription" MultiLine="True" Rows="4"
                                StringLength="2000"></bnbdatacontrol:BnbTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 29px" width="50%">
                            Request Skill Group</td>
                        <td style="height: 29px" width="50%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl2" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataMember="BnbRequest.RoleSkillGroupID" LookupTableName="lkuRoleSkillGroup"
                                LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" AutoPostBack="True">
                            </bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 27px" width="50%">
                            Request Skill</td>
                        <td style="height: 27px" width="50%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl3" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataMember="BnbRequest.RoleSkillID" LookupTableName="lkuRoleSkill"
                                LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" ColumnRelatedToParent="RoleSkillGroupID"
                                ParentControlID="BnbLookupControl2"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 27px" width="50%">
                            VSO Type</td>
                        <td style="height: 27px" width="50%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl1" runat="server" CssClass="lookupcontrol"
                                Height="22px" Width="250px" DataMember="BnbRequest.VSOTypeID" LookupTableName="lkuVSOType"
                                LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description"></bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="height: 27px" width="50%">
                            Duration in months</td>
                        <td style="height: 27px" width="50%">
                            <bnbdatacontrol:BnbLookupControl ID="BnbLookupControl6" runat="server" CssClass="lookupcontrol"
                                Width="250px" Height="22px" DataMember="BnbRequest.DurationID" DataTextField="Description"
                                ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="lkuRequestDuration">
                            </bnbdatacontrol:BnbLookupControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="50%">
                            Volunteer Start Date</td>
                        <td width="50%">
                            <bnbdatacontrol:BnbDateBox ID="BnbDateBox1" runat="server" CssClass="datebox" Height="20px"
                                Width="90px" DataMember="BnbRequest.VolStartDate" DateFormat="dd/mm/yyyy" DateCulture="en-GB">
                            </bnbdatacontrol:BnbDateBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="50%">
                            Active</td>
                        <td width="50%">
                            <bnbdatacontrol:BnbCheckBox ID="BnbCheckBox3" runat="server" CssClass="checkbox"
                                DataMember="BnbRequest.Active"></bnbdatacontrol:BnbCheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="50%">
                            Linked to Placement</td>
                        <td width="50%">
                            <bnbdatacontrol:BnbDomainObjectHyperlink ID="BnbDomainObjectHyperlink1" runat="server"
                                RedirectPage="PlacementPage.aspx" GuidProperty="BnbRequest.PositionID" HyperlinkText="[BnbDomainObjectHyperlink]"
                                Target="_top" DataMember="BnbRequest.Position.FullPlacementReference"></bnbdatacontrol:BnbDomainObjectHyperlink>
                        </td>
                    </tr>
                </table>
           
        </div>
    </form>

</body>
</html>
