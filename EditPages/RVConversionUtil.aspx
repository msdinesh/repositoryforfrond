<%@ Page language="c#" Codebehind="RVConversionUtil.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.RVConversionUtil" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrols" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>RVConversionUtil</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/batchprocess24silver.gif"></uc1:title>
				<asp:panel id="panelZero" runat="server">
				<p></p>
					<TABLE class="fieldtable" id="Table1" style="HEIGHT: 108px;width:600px;"  border="0">
                        <tr>
                            <td style="width:400px;" >
                            </td>
                            <td class="label" style="text-align:left;" >
                                All VSO Groups</td>
                            <td class="label" style="text-align:left;">
                                Selected VSO Group<br />
                                <bnbdatacontrol:BnbStandaloneLookupControl ID="uxVSOTypeLookup"
                                    runat="server" CssClass="lookupcontrol" DataTextField="Description" Height="22px"
                                    ListBoxRows="4" LookupControlType="ComboBox" LookupTableName="vlkuBonoboVSOGroupForRVConversion"
                                    Width="182px" AutoPostBack="True" />
                            </td>
                        </tr>
						<TR>
							<TD class="label" style="width:400px;">No. of records with 
								RVRecordStatus 'Draft'&nbsp;
							</TD>
							<TD >
								<asp:Label id="lblDraft" runat="server"></asp:Label></TD>
                            <td >
                                <asp:Label ID="lblDraftType" runat="server" ></asp:Label></td>
						</TR>
						<TR>
							<TD class="label" style="width:400px;">
								<P>No. of records with RVRecordStatus 'Ready'</P>
							</TD>
							<TD >
								<asp:Label id="lblReady" runat="server"></asp:Label></TD>
                            <td >
                                <asp:Label ID="lblReadyType" runat="server" ></asp:Label></td>
						</TR>
						<TR>
							<TD class="label" style="width:400px;">
								<P>No. of records with RVRecordStatus 'Failed'</P>
							</TD>
							<TD >
								<P>
									<asp:Label id="lblFailed" runat="server"></asp:Label></P>
							</TD>
                            <td >
                                <asp:Label ID="lblFailedType" runat="server" ></asp:Label></td>
						</TR>
						<TR>
							<TD style="WIDTH: 379px" width="379">
								</TD>
							<TD width="25%"></TD>
                            <td width="25%">
                            <asp:button id="buttonImportRecords" runat="server" Text="Import Ready Records for selected Group" Width="253px" onclick="buttonImportRecords_Click"></asp:button>&nbsp;&nbsp;&nbsp;<br />
								<asp:button id="buttonCancel" runat="server" Text="Cancel" Width="49px" onclick="buttonCancel_Click"></asp:button>
                            </td>
						</TR>
					</TABLE>
				</asp:panel>
				<asp:panel id="panelOne" runat="server">
					<P></P>
					<TABLE class="fieldtable" id="Table2" style="HEIGHT: 187px" width="872" border="0">
						<TR>
							<TD class="label" style="WIDTH: 139px" width="139">Run date</TD>
							<TD style="HEIGHT: 9px">
								<asp:Label id="lblRVCLRunDate" runat="server" Width="104px"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 139px" width="139">No. of Records&nbsp;Failed&nbsp;
							</TD>
							<TD style="HEIGHT: 9px">
								<asp:Label id="lblRVCLFailed" runat="server" Width="112px"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 139px" width="139">No. of Records Transfered</TD>
							<TD style="HEIGHT: 1px">
								<asp:Label id="lblRVCLTransfered" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 139px; HEIGHT: 23px" width="139">Run Details</TD>
							<TD style="HEIGHT: 23px" align="left">
								<asp:TextBox id="txtRunDetails" runat="server" Width="624px" TextMode="MultiLine" Height="72px"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 139px" width="139"></TD>
							<TD align="left">
								<asp:Label id="lblErrors" runat="server" Width="610px" ForeColor="Red"></asp:Label></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 139px" width="139"></TD>
							<TD align="left">
								</TD>
						</TR>
					</TABLE>
				</asp:panel>
				<asp:LinkButton id="lnkNewRVTracking" runat="server" Width="312px" onclick="lnkNewRVTracking_Click">New RV Tracking</asp:LinkButton>
			</div>
		</form>
	</body>
</HTML>
