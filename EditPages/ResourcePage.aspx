<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="NavigationBar" Src="../UserControls/NavigationBar.ascx" %>
<%@ Page language="c#" Codebehind="ResourcePage.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.ResourcePage" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>SupplierPage</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal><LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet"><asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent" style="WIDTH: 775px; HEIGHT: 599px"><uc1:title id="titleBar" runat="server" imagesource="../Images/bnbindividualresource24silver.gif"></uc1:title><uc1:navigationbar id="NavigationBar1" runat="server"></uc1:navigationbar>
				<bnbpagecontrol:bnbdatabuttons id="BnbDataButtons1" runat="server" UndoText="Undo" SaveText="Save" EditAllText="Edit All"
					NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px"
					NewButtonEnabled="False" CssClass="databuttons"></bnbpagecontrol:bnbdatabuttons><bnbpagecontrol:bnbmessagebox id="BnbMessageBox2" runat="server" CssClass="messagebox" SuppressRecordSavedMessage="false"
					PopupMessage="false"></bnbpagecontrol:bnbmessagebox>
				<TABLE class="fieldtable" id="Table1" width="100%" border="0">
					<TR>
						<TD class="label" style="HEIGHT: 33px" align="right" width="25%">Individual</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbgenericcontrols:bnbhyperlink id="BnblnkIndividual" runat="server" NavigateUrl="IndividualPage.aspx"></bnbgenericcontrols:bnbhyperlink></TD>
					<TR>
						<TD class="label" style="HEIGHT: 33px" align="right" width="25%">Resource Type</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnblookupcontrol id="BnblkType" runat="server" CssClass="lookupcontrol" AutoPostBack="True" DataMember="BnbIndividualResource.ResourceTypeID"
								LookupTableName="lkuResourceType" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Height="22px" Width="250px"></bnbdatacontrol:bnblookupcontrol></TD>
						<TD class="label" style="WIDTH: 123px; HEIGHT: 33px" width="123">Resource</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnblookupcontrol id="BnbLkuResource" runat="server" CssClass="lookupcontrol" DataMember="BnbIndividualResource.ResourceID"
								LookupTableName="lkuResource" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Height="22px" Width="250px" ParentControlID="BnblkType"
								ColumnRelatedToParent="ResourceTypeID"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 123px; HEIGHT: 33px" align="right" width="123">From</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnbdatebox id="BnbDateBox1" runat="server" CssClass="datebox" DataMember="BnbIndividualResource.FromDate"
								Height="20px" Width="90px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
						<TD class="label" style="HEIGHT: 33px" width="25%">To</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnbdatebox id="BnbDateBox2" runat="server" CssClass="datebox" DataMember="BnbIndividualResource.ToDate"
								Height="20px" Width="90px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 123px; HEIGHT: 33px" align="right" width="123">Recommendation 
							to use again</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnblookupcontrol id="BnblkuRecommendation" runat="server" CssClass="lookupcontrol" AutoPostBack="False"
								DataMember="BnbIndividualResource.ResourceRecommendationID" LookupTableName="lkuResourceRecommendation" LookupControlType="ComboBox" ListBoxRows="4"
								DataTextField="Description" Height="22px" Width="250px"></bnbdatacontrol:bnblookupcontrol></TD>
						<TD class="label" style="HEIGHT: 33px" width="25%">Recommended By</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol5" runat="server" CssClass="lookupcontrol" DataMember="BnbIndividualResource.RecommendationByID"
								LookupTableName="vlkuBonoboUser" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Height="22px" Width="250px"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 123px; HEIGHT: 33px" align="right" width="123">Recommended 
							Date</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnbdatebox id="BnbDateBox3" runat="server" CssClass="datebox" DataMember="BnbIndividualResource.RecommendationDate"
								Height="20px" Width="90px" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"></bnbdatacontrol:bnbdatebox></TD>
						<TD class="label" style="HEIGHT: 33px" width="25%">Recommended Reason</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol7" runat="server" CssClass="lookupcontrol" DataMember="BnbIndividualResource.ResourceReasonID"
								LookupTableName="lkuResourceReason" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Height="22px" Width="250px"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 123px; HEIGHT: 33px" align="right" width="123">Level 
							of commitment</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol8" runat="server" CssClass="lookupcontrol" AutoPostBack="False"
								DataMember="BnbIndividualResource.ResourceLevelOfCommitmentOfferedID" LookupTableName="lkuResourceLevelOfCommitment" LookupControlType="ComboBox"
								ListBoxRows="4" DataTextField="Description" Height="22px" Width="250px"></bnbdatacontrol:bnblookupcontrol></TD>
						<TD class="label" style="HEIGHT: 33px" width="25%">Locality</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnblookupcontrol id="Bnblookupcontrol9" runat="server" CssClass="lookupcontrol" DataMember="BnbIndividualResource.ResourceLocalityID"
								LookupTableName="lkuResourceLocality" LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Height="22px" Width="250px"></bnbdatacontrol:bnblookupcontrol></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 123px; HEIGHT: 33px" align="right" width="123">Special 
							link</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnbcheckbox id="BnbCheckBox1" runat="server" CssClass="checkbox" DataMember="BnbIndividualResource.Special"></bnbdatacontrol:bnbcheckbox></TD>
						<TD class="label" style="HEIGHT: 33px" width="25%">Comments</TD>
						<TD style="HEIGHT: 33px" width="25%"><bnbdatacontrol:bnbtextbox id="Bnbtextbox2" runat="server" CssClass="textbox" DataMember="BnbIndividualResource.Comments"
								Height="20px" Width="250px" MultiLine="false" Rows="3" StringLength="0" ControlType="Text"></bnbdatacontrol:bnbtextbox></TD>
					</TR>
					<TR>
						<TD class="label" style="WIDTH: 123px; HEIGHT: 33px" align="right" width="123">Status</TD>
						<TD style="HEIGHT: 33px" colSpan="3"><bnbdatagrid:bnbdatagridfordomainobjectlist id="BnbDataGridForDomainObjectList1" runat="server" CssClass="datagrid" DataMember="BnbIndividualResource.ResourceStatusProgress"
								DataProperties="ResourceStatusID [BnbLookupControl:lkuResourceStatus], DateFrom[BnbDateBox],DateTo[BnbDateBox]" DataGridType="Editable" DataGridForDomainObjectListType="Editable"
								DeleteButtonVisible="True" EditButtonVisible="true" AddButtonText="Add" EditButtonText="Edit" AddButtonVisible="True" VisibleOnNewParentRecord="false" SortBy="ID"></bnbdatagrid:bnbdatagridfordomainobjectlist></TD>
					</TR>
				</TABLE>
				<P><bnbpagecontrol:bnbdatabuttons id="BnbDataButtons2" runat="server" UndoText="Undo" SaveText="Save" EditAllText="Edit All"
						NewText="New" HeightAllButtons="25px" UndoWidth="55px" SaveWidth="55px" EditAllWidth="55px" NewWidth="55px"
						NewButtonEnabled="False" CssClass="databuttons"></bnbpagecontrol:bnbdatabuttons></P>
			</div>
			<P>&nbsp;</P>
			<P>&nbsp;</P>
		</form>
	</body>
</HTML>
