<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="WizardButtons" Src="../UserControls/WizardButtons.ascx" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Page language="c#" Codebehind="WizardNewFunding.aspx.cs" AutoEventWireup="True" Inherits="Frond.EditPages.WizardNewFunding" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>WizardNewPersonnel</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<script language="javascript" type="text/javascript">
		function KeyDownTextBoxDefaultButtonHandler(btn)
		{
			if(btn == null)
			{
				return false;
			}
			// process only the Enter key
			if (event.keyCode == 13)
			{
				// cancel the default submit
				event.returnValue=false;
				event.cancel = true;
				// submit the form by programmatically clicking the specified button
				btn.click();
			}
		}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<DIV class="wizardheader" ms_positioning="FlowLayout">
					<H3><img class="icon24" src="../Images/wizard24silver.gif"> New Sponsorship Wizard</H3>
				</DIV>
				<asp:panel id="panelZero" runat="server" CssClass="wizardpanel">
					<P><EM>This wizard will help you to add&nbsp;new&nbsp;Sponsorship to a Placement or 
							Volunteer.</EM>
					<P></P>
					<P>
						<TABLE class="wizardtable" id="Table2">
							<TR>
								<TD class="label" style="WIDTH: 25%">Placement</TD>
								<TD>
									<asp:Label id="lblPlacement1" runat="server"></asp:Label></TD>
							</TR>
							<TR>
								<TD class="label" style="WIDTH: 25%">Volunteer</TD>
								<TD>
									<asp:Label id="lblVolunteer1" runat="server"></asp:Label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%"></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%">
									<asp:Label id="lblAttachAdvice" runat="server">Please select whether the Sponsorship should be attached to the Placement or the Volunteer:</asp:Label></TD>
								<TD>
									<asp:RadioButton id="rbPlacement" runat="server" Checked="False" Text="Placement" GroupName="grpAttach"></asp:RadioButton><BR>
									<asp:RadioButton id="rbVolunteer" runat="server" Checked="True" Text="Volunteer" GroupName="grpAttach"></asp:RadioButton></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%"></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%">Please select whether the Sponsorship is being supplied by 
									an Organisation or an Individual</TD>
								<TD>
									<asp:RadioButton id="rbDonorOrganisation" runat="server" Text="Donor is an Organisation" GroupName="grpDonorEntity"></asp:RadioButton><BR>
									<asp:RadioButton id="rbDonorIndividual" runat="server" Text="Donor is an Individual" GroupName="grpDonorEntity"></asp:RadioButton></TD>
							</TR>
						</TABLE>
					<P>
						<asp:label id="lblPanelZeroFeedback" runat="server" CssClass="feedback"></asp:label></P>
				</asp:panel><asp:panel id="panelOne" runat="server" CssClass="wizardpanel">
					<asp:Panel id="panelChooseOrg" runat="server">
						<P>Please select the Organisation that is providing the Sponsorship:</P>
						<P>Enter the Org Name and/or Country here and press Find, then select one of the 
							results:</P>
						<TABLE>
							<TR>
								<TD style="WIDTH: 25%; TEXT-ALIGN: right">Org Name (any part of)
								</TD>
								<TD>
									<asp:TextBox id="txtFindOrgName" runat="server" Width="192px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 16px">Org Country</TD>
								<TD style="HEIGHT: 16px">
									<bnbdatacontrol:BnbStandaloneLookupControl id="drpFindOrgCountry" runat="server" CssClass="lookupcontrol" Width="250px" LookupTableName="lkuCountry"
										LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Height="22px"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD>
									<asp:Button id="cmdOrgFind" runat="server" Text="Find" onclick="cmdOrgFind_Click"></asp:Button></TD>
							</TR>
						</TABLE>
						<P>
							<bnbdatagrid:BnbDataGridForView id="bdgOrgs" runat="server" CssClass="datagrid" Width="100%" GuidKey="tblCompany_CompanyID"
								ColumnNames="tblCompany_CompanyName, tblAddress_AddressLine1, tblAddress_Postcode, lkuCountry_Description" ViewName="vwBonoboFindOrganisationFunder"
								DataGridType="RadioButtonList" DisplayNoResultMessageOnly="True" DisplayResultCount="false" ShowResultsOnNewButtonClick="false"
								ViewLinksVisible="true" NewLinkVisible="False" NewLinkText="New" ViewLinksText="View" Visible="False"></bnbdatagrid:BnbDataGridForView><BR>
					</asp:Panel>
					<asp:Panel id="panelChooseIndv" runat="server">
						<P>Please select the Individual who is providing the Sponsorship:</P>
						<P>Enter the Surname, Forename, Postcode and/or Country here and press Find, then 
							select one of the results:</P>
						<TABLE>
							<TR>
								<TD style="WIDTH: 25%; TEXT-ALIGN: right">Ref No</TD>
								<TD>
									<asp:TextBox id="txtFindRefNo" runat="server" Width="72px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%; TEXT-ALIGN: right">Surname (starts with)</TD>
								<TD>
									<asp:TextBox id="txtFindSurname" runat="server"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%; TEXT-ALIGN: right">Forename (starts with)</TD>
								<TD>
									<asp:TextBox id="txtFindForename" runat="server"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%; TEXT-ALIGN: right">Postcode</TD>
								<TD>
									<asp:TextBox id="txtFindPostcode" runat="server" Width="112px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%; TEXT-ALIGN: right">Country</TD>
								<TD>
									<asp:DropDownList id="drpFindIndivCountry" runat="server"></asp:DropDownList></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 25%; TEXT-ALIGN: right"></TD>
								<TD>
									<asp:Button id="cmdIndivFind" runat="server" Text="Find" onclick="cmdIndivFind_Click"></asp:Button></TD>
							</TR>
						</TABLE>
						<P>
							<bnbdatagrid:BnbDataGridForView id="bdgIndiv" runat="server" CssClass="datagrid" Width="100%" GuidKey="tblIndividualKeyInfo_IndividualID"
								ViewName="vwBonoboFindIndividualFunder" DataGridType="RadioButtonList" DisplayNoResultMessageOnly="True" DisplayResultCount="false"
								ShowResultsOnNewButtonClick="false" ViewLinksVisible="False" NewLinkVisible="False" NewLinkText="New" ViewLinksText="View"
								Visible="False"></bnbdatagrid:BnbDataGridForView></P>
					</asp:Panel>
					<P></P>
					<P>
						<asp:Label id="lblSearchHint" runat="server" Visible="False">(If you cannot find the Organisation you are looking for, it may not be in the database yet. If this is the case, you can add it as a new Organisation using the <a href="WizardNewOrganisation.aspx">
								New Organisation Wizard</a>)</asp:Label>
						<asp:Label id="lblSearchHintIndiv" runat="server" Visible="False">(If you cannot find the Individual you are looking for, they may not be in the database yet. If this is the case, you can add it as a new Individual using the <a href="WizardNewIndividual.aspx">
								New Individual Wizard</a>)</asp:Label></P>
					<P>
						<asp:Label id="lblPanelOneFeedback" runat="server" CssClass="feedback"></asp:Label>
				</asp:panel><asp:panel id="panelTwo" runat="server" CssClass="wizardpanel">
					<P>Please enter further details about the new Sponsorship, then press Finish to 
						create the new record:</P>
					<TABLE class="wizardtable" id="Table4">
						<TR>
							<TD class="label" style="WIDTH: 25%; HEIGHT: 24px">Placement</TD>
							<TD style="HEIGHT: 24px">
								<asp:Label id="lblPlacement2" runat="server"></asp:Label></TD>
							<TD class="label" style="WIDTH: 25%; HEIGHT: 24px">Donor Manager</TD>
							<TD style="HEIGHT: 24px">
								<bnbdatacontrol:BnbStandaloneLookupControl id="drpDonorOwner" runat="server" CssClass="lookupcontrol" Width="250px" LookupTableName="vlkuBonoboUser"
									LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Height="22px"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 25%; HEIGHT: 17px">
								<P>Volunteer</P>
							</TD>
							<TD style="HEIGHT: 17px">
								<asp:Label id="lblVolunteer2" runat="server"></asp:Label></TD>
							<TD class="label" style="WIDTH: 25%; HEIGHT: 17px">Funding Secured By</TD>
							<TD style="HEIGHT: 17px">
								<bnbdatacontrol:BnbStandaloneLookupControl id="drpFundingSecuredBy" runat="server" CssClass="lookupcontrol" Width="250px" LookupTableName="lkuFunderDepartmentType"
									LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Height="22px"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 25%; HEIGHT: 15px">Association</TD>
							<TD style="HEIGHT: 15px">
								<asp:Label id="lblAssociation" runat="server"></asp:Label></TD>
							<TD class="label" style="WIDTH: 25%; HEIGHT: 15px">Financial Year</TD>
							<TD style="HEIGHT: 15px">
								<bnbdatacontrol:BnbStandaloneLookupControl id="drpFinancialYear" runat="server" CssClass="lookupcontrol" Width="250px" LookupTableName="lkuFinancialYear"
									LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Height="22px"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 25%">Donor Org</TD>
							<TD>
								<asp:Label id="lblOrgName" runat="server"></asp:Label></TD>
							<TD class="label" style="WIDTH: 25%">Value �</TD>
							<TD>
								<asp:TextBox id="txtValue" runat="server"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="label" style="WIDTH: 25%">Donor Individual</TD>
							<TD>
								<asp:Label id="lblDonorIndivName" runat="server"></asp:Label></TD>
							<TD class="label" style="WIDTH: 25%">Funding Status</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="drpFundingStatus" runat="server" CssClass="lookupcontrol" Width="250px" LookupTableName="lkuFundingStatus"
									LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Height="22px"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 25%"></TD>
							<TD></TD>
							<TD class="label" style="WIDTH: 25%">Received On</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneDateBox id="uxReceivedOn" runat="server" CssClass="datebox" Width="90px" Height="20px" DateCulture="en-GB"
									DateFormat="dd/MMM/yyyy"></bnbdatacontrol:BnbStandaloneDateBox></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 25%"></TD>
							<TD></TD>
							<TD class="label" style="WIDTH: 25%">Currency</TD>
							<TD>
								<bnbdatacontrol:BnbStandaloneLookupControl id="drpCurrency" runat="server" CssClass="lookupcontrol" Width="250px" LookupTableName="lkuCurrency"
									LookupControlType="ComboBox" ListBoxRows="4" DataTextField="Description" Height="22px" ShowBlankRow="True"></bnbdatacontrol:BnbStandaloneLookupControl></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 25%"></TD>
							<TD></TD>
							<TD class="label" style="WIDTH: 25%">Currency&nbsp;Value</TD>
							<TD>
								<asp:TextBox id="txtConvertedValue" runat="server"></asp:TextBox></TD>
						</TR>
					</TABLE>
					<P>
						<asp:Label id="lblPanelTwoFeedback" runat="server" CssClass="feedback"></asp:Label></P>
				</asp:panel>
				<P></P>
				<P><uc1:wizardbuttons id="wizardButtons" runat="server"></uc1:wizardbuttons></P>
				<P><asp:label id="lblHiddenPositionID" runat="server" Visible="False"></asp:label><asp:label id="lblHiddenApplicationID" runat="server" Visible="False"></asp:label><asp:label id="lblHiddenAssociationID" runat="server" Visible="False"></asp:label><asp:label id="lblHiddenCompanyID" runat="server" Visible="False"></asp:label><asp:label id="lblHiddenReferrer" runat="server" Visible="False"></asp:label><asp:label id="lblHiddenDonorIndividualID" runat="server" Visible="False"></asp:label></P>
				<P></P>
			</div>
		</form>
	</body>
</HTML>
