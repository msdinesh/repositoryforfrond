using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BonoboWebControls;

namespace Frond.EditPages
{
    /// <summary>
    /// Summary description for ProgrammePage.
    /// </summary>
    public partial class ProgrammeCommentsPage : System.Web.UI.Page
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
#if DEBUG
            BnbWebFormManager bnb = null;
            if (Request.ServerVariables["QUERY_STRING"] == "")
            {
                BnbWebFormManager.ReloadPage("BnbProgramme=F4C1C71E-F32B-481A-8D52-06669CCE8169&BnbProgrammeComments=62EDC311-AD98-40B0-9B02-D7E07AA0720A");//8C0C07E0-9EBE-4D92-9E2A-37036BF1DFDB"
            }
            else
            {
                bnb = new BnbWebFormManager(this, "BnbProgrammeComments");
            }
#else
			BnbWebFormManager bnb = new BnbWebFormManager(this,"BnbProgrammeComments");
#endif

            // turn on logging
            bnb.LogPageHistory = true;
            //<Integartion>
            bnb.PageNameOverride = "Notes";
            BnbWorkareaManager.SetPageStyleOfUser(this);

            if (!this.IsPostBack)
            {
                if (bnb.ObjectTree != null)
                    titleBar.TitleText = bnb.PageTitleDescriptionCurrent;
            }
            //</Integartion>
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
