using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboEngine;
using BonoboEngine.Query;
using BonoboDomainObjects;

namespace Frond.EditPages
{
    public partial class WizardNewProject : System.Web.UI.Page
    {

        #region Lookup's for Main Information Fields

        private void fillProgrammeOfficeCombo()
        {
            DataTable programmeOffice = BnbEngine.LookupManager.GetLookup("lkuProgrammeOffice");
            programmeOffice.DefaultView.RowFilter = "Exclude = 0";
            programmeOffice.DefaultView.Sort = "Description";
            drpProgrammeOffice.DataSource = programmeOffice;
            drpProgrammeOffice.DataTextField = "Description";
            drpProgrammeOffice.DataValueField = "IntID";
            drpProgrammeOffice.DataBind();
            drpProgrammeOffice.Items.Insert(0, "");
        }

        private void fillProjectStatusCombo()
        {
            DataTable projectStatusTable = BnbEngine.LookupManager.GetLookup("lkuProjectStatus");
            projectStatusTable.DefaultView.RowFilter = "Exclude = 0";
            projectStatusTable.DefaultView.Sort = "DisplayOrder";
            drpProjectStatus.DataSource = projectStatusTable;
            drpProjectStatus.DataTextField = "Description";
            drpProjectStatus.DataValueField = "IntID";
            drpProjectStatus.DataBind();
            drpProjectStatus.Items.Insert(0, "");
        }

        private void fillProjectOfficersAndDepartmentCombo()
        {
            DataTable projectOfficer = BnbEngine.LookupManager.GetLookup("vlkuBonoboUser");
            projectOfficer.DefaultView.RowFilter = "Exclude = 0";
            projectOfficer.DefaultView.Sort = "Description";

            drpProjectAccountable.DataSource = projectOfficer;
            drpProjectAccountable.DataTextField = "Description";
            drpProjectAccountable.DataValueField = "GuidID";
            drpProjectAccountable.DataBind();
            drpProjectAccountable.Items.Insert(0, "");

        }
        //TPT:Added for PGMSP-18
        private void fillVSOGoalCombo()
        {
            DataTable vsoGoalTable = BnbEngine.LookupManager.GetLookup("lkuMonitorObjective");
            vsoGoalTable.DefaultView.RowFilter = "Exclude = 0";
            vsoGoalTable.DefaultView.Sort = "Description";
            drpVsoGoal.DataSource = vsoGoalTable;
            drpVsoGoal.DataTextField = "Description";
            drpVsoGoal.DataValueField = "IntID";
            drpVsoGoal.DataBind();
            drpVsoGoal.Items.Insert(0, "");
        }

        //TPT:Added for PGMSP-18
        private void fillProjectRegionCombo()
        {
            DataTable projectRegionTable = BnbEngine.LookupManager.GetLookup("lkuProgrammeOfficeRegion");
            projectRegionTable.DefaultView.RowFilter = "Exclude = 0";
            projectRegionTable.DefaultView.Sort = "Description";
            drpProjectRegion.DataSource = projectRegionTable;
            drpProjectRegion.DataTextField = "Description";
            drpProjectRegion.DataValueField = "IntID";
            drpProjectRegion.DataBind();
            drpProjectRegion.Items.Insert(0, "");
        }
        #endregion

        #region Lookup's for Risk Information Fields

        private void fillOverallRiskCombo()
        {
            DataTable riskTable = BnbEngine.LookupManager.GetLookup("lkuProjectRisk");
            riskTable.DefaultView.RowFilter = "Exclude=0";
            riskTable.DefaultView.Sort = "DisplayOrder";
            drpOverallRisk.DataSource = riskTable;
            drpOverallRisk.DataTextField = "Description";
            drpOverallRisk.DataValueField = "IntID";
            drpOverallRisk.DataBind();
            // set 'High' as default Project Risk in Project wizard
            drpOverallRisk.Items[1].Selected = true;
            //drpOverallRisk.Items.Insert(0, new ListItem("", ""));
        }
        private void fillCorporateOperationalPlansCombo()
        {
            DataTable operationalPlan = BnbEngine.LookupManager.GetLookup("lkuProjectRiskCorporateOperPlans");
            operationalPlan.DefaultView.RowFilter = "Exclude=0";
            operationalPlan.DefaultView.Sort = "DisplayOrder";
            drpOperPlans.DataSource = operationalPlan;
            drpOperPlans.DataTextField = "Description";
            drpOperPlans.DataValueField = "IntID";
            drpOperPlans.DataBind();
            drpOperPlans.Items.Insert(0, new ListItem("", ""));
        }
               
        private void fillMatchedFundingCombo()
        {
            DataTable matchedFunding = BnbEngine.LookupManager.GetLookup("lkuProjectRiskMatchFunding");
            matchedFunding.DefaultView.RowFilter = "Exclude=0";
            matchedFunding.DefaultView.Sort = "DisplayOrder";
            drpMatchFunding.DataSource = matchedFunding;
            drpMatchFunding.DataTextField = "Description";
            drpMatchFunding.DataValueField = "IntID";
            drpMatchFunding.DataBind();
            drpMatchFunding.Items.Insert(0, "");
            //drpMatchFunding.Items.FindByValue(BnbConst.RiskType_NotApplicable.ToString()).Selected = true;
        }

        private void fillContractualConditionCombo()
        {
            DataTable contractualCondition = BnbEngine.LookupManager.GetLookup("lkuProjectRiskContractualCondn");
            contractualCondition.DefaultView.RowFilter = "Exclude=0";
            contractualCondition.DefaultView.Sort = "DisplayOrder";
            drpContractCondn.DataSource = contractualCondition;
            drpContractCondn.DataTextField = "Description";
            drpContractCondn.DataValueField = "IntID";
            drpContractCondn.DataBind();
            drpContractCondn.Items.Insert(0, "");
            //drpContractCondn.Items.FindByValue(BnbConst.RiskType_NotApplicable.ToString()).Selected = true;
        }

        private void fillPOCapacityCombo()
        {
            DataTable poCapacity = BnbEngine.LookupManager.GetLookup("lkuProjectRiskPOCapacity");
            poCapacity.DefaultView.RowFilter = "Exclude=0";
            poCapacity.DefaultView.Sort = "DisplayOrder";
            drpPOCapacity.DataSource = poCapacity;
            drpPOCapacity.DataTextField = "Description";
            drpPOCapacity.DataValueField = "IntID";
            drpPOCapacity.DataBind();
            drpPOCapacity.Items.Insert(0, new ListItem("", ""));
        }

        private void fillVolunteerRecruitabiltyCombo()
        {
            DataTable VolunteerRecruitabilty = BnbEngine.LookupManager.GetLookup("lkuProjectRiskVolRecruit");
            VolunteerRecruitabilty.DefaultView.RowFilter = "Exclude=0";
            VolunteerRecruitabilty.DefaultView.Sort = "DisplayOrder";
            drpVolRecruit.DataSource = VolunteerRecruitabilty;
            drpVolRecruit.DataTextField = "Description";
            drpVolRecruit.DataValueField = "IntID";
            drpVolRecruit.DataBind();
            drpVolRecruit.Items.Insert(0, new ListItem("", ""));
        }
        private void fillProjectFeasiblityCombo()
        {
            DataTable dtProjectFeasibility = BnbEngine.LookupManager.GetLookup("lkuProjectFeasibility");
            dtProjectFeasibility.DefaultView.RowFilter = "Exclude=0";
            dtProjectFeasibility.DefaultView.Sort = "DisplayOrder";
            drpProjectFeasibility.DataSource = dtProjectFeasibility;
            drpProjectFeasibility.DataTextField = "Description";
            drpProjectFeasibility.DataValueField = "IntID";
            drpProjectFeasibility.DataBind();
            drpProjectFeasibility.Items.Insert(0, new ListItem("", ""));
        }

        #endregion

        #region " SaveProjectMainDetails "

        private void SaveProjectMainDetails(BnbProject objProject, BnbEditManager em)
        {
            if (rbtnCheckList.SelectedValue == rbtnCheckList.Items[0].Value)
                objProject.ProjectTypeID = BnbConst.ProjectType_Programme;
            else
                objProject.ProjectTypeID = BnbConst.ProjectType_Project;

            objProject.ProjectTitle = txtProjectTitleStep3.Text;
            //objProject.ProjectAbbr = txtAbbrev.Text;

            if (drpProjectAccountable.SelectedValue != "")
            {
                objProject.ProjectAccountableOfficerID = new Guid(drpProjectAccountable.SelectedValue);
                objProject.ProjectAccountableDepartmentID = objProject.GetOfficerDepartment(objProject.ProjectAccountableOfficerID.ToString());
            }
           
            //TPT:Added for PGMSP-18
            if (drpProjectRegion.SelectedValue != "")
                objProject.ProjectRegionID = Convert.ToInt32(drpProjectRegion.SelectedValue);
            if (txtProjectTotalCost.Text != "")
                objProject.ProjectTotalCost = Convert.ToDecimal(txtProjectTotalCost.Text);

            objProject.ProjectSummary = txtNarrative.Text;
            

            BnbProjectStatusProgress ProjectStatus = new BnbProjectStatusProgress(true);
            ProjectStatus.RegisterForEdit(em);
            if (drpProjectStatus.SelectedValue != "")
                ProjectStatus.ProjectStatusID = Convert.ToInt32(drpProjectStatus.SelectedValue);
            objProject.ProjectStatusProgresses.Add(ProjectStatus);

            BnbProjectProgrammeOffice ProgrammeOffice = new BnbProjectProgrammeOffice(true);
            ProgrammeOffice.RegisterForEdit(em);
            if (drpProgrammeOffice.SelectedValue != "")
                ProgrammeOffice.ProgrammeOfficeID = Convert.ToInt32(drpProgrammeOffice.SelectedValue);
            objProject.ProgrammeOffices.Add(ProgrammeOffice);

            //TPT:Added for PGMSP-18
            BnbProjectVsoGoalObjective vsoGoals = new BnbProjectVsoGoalObjective(true);
            vsoGoals.RegisterForEdit(em);
            if (drpVsoGoal.SelectedValue != "")
                vsoGoals.MonitorObjectiveID = Convert.ToInt32(drpVsoGoal.SelectedValue);
            objProject.VsoGoalObjectives.Add(vsoGoals);
        }

        #endregion

        #region " SaveProjectFinancialAndRiskDetails "

        private void SaveProjectFinancialAndRiskDetails(BnbProject objProject, BnbEditManager em)
        {
            if (rbtnCheckList.SelectedValue == rbtnCheckList.Items[1].Value)
            {
                if (drpOverallRisk.SelectedValue != "")
                    objProject.RiskID = Convert.ToInt32(drpOverallRisk.SelectedValue);

                if (drpOperPlans.SelectedValue != "")
                    objProject.ProjectRisk.CorporateOperationalPlansID = Convert.ToInt32(drpOperPlans.SelectedValue);

                if (drpMatchFunding.SelectedValue != "")
                    objProject.ProjectRisk.MatchFundingID = Convert.ToInt32(drpMatchFunding.SelectedValue);

                if (drpContractCondn.SelectedValue != "")
                    objProject.ProjectRisk.ContractualConditionsID = Convert.ToInt32(drpContractCondn.SelectedValue);

                if (drpPOCapacity.SelectedValue != "")
                    objProject.ProjectRisk.POCapacityID = Convert.ToInt32(drpPOCapacity.SelectedValue);

                if (drpVolRecruit.SelectedValue != "")
                    objProject.ProjectRisk.VolunteerRecruitabilityID = Convert.ToInt32(drpVolRecruit.SelectedValue);

                if (drpProjectFeasibility.SelectedValue != string.Empty)
                    objProject.ProjectRisk.ProjectFeasibilityID = Convert.ToInt32(drpProjectFeasibility.SelectedValue);

                if (txtRiskComments.Text != string.Empty)
                    objProject.ProjectRisk.RiskComments = txtRiskComments.Text;

                if (rblRiskAssessmentCompleted.SelectedItem.Text == BnbConst.YesNo_Yes)
                    objProject.ProjectRisk.RiskAssessmentCompletedID = BnbConst.YesNoForRadioButton_Yes;
                else
                    objProject.ProjectRisk.RiskAssessmentCompletedID = BnbConst.YesNoForRadioButton_No;
            }
            else
            {
                objProject.RiskID = BnbDomainObject.NullInt;
                objProject.ProjectRisk.CorporateOperationalPlansID = BnbDomainObject.NullInt;
                objProject.ProjectRisk.MatchFundingID = BnbDomainObject.NullInt;
                objProject.ProjectRisk.ContractualConditionsID = BnbDomainObject.NullInt;
                objProject.ProjectRisk.POCapacityID = BnbDomainObject.NullInt;
                objProject.ProjectRisk.VolunteerRecruitabilityID = BnbDomainObject.NullInt;
                objProject.ProjectRisk.ProjectFeasibilityID = BnbDomainObject.NullInt;
                objProject.ProjectRisk.RiskComments = BnbDomainObject.NullString;
                objProject.ProjectRisk.RiskAssessmentCompletedID = BnbDomainObject.NullInt;
            }

        }

        #endregion

    }
}
