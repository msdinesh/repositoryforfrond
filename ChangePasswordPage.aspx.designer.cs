//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3053
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Frond {
    
    public partial class ChangePasswordPage {
        protected System.Web.UI.WebControls.Literal literalBeforeCss;
        protected System.Web.UI.WebControls.Literal literalAfterCss;
        protected System.Web.UI.HtmlControls.HtmlForm Form1;
        protected Frond.UserControls.MenuBar MenuBar1;
        protected Frond.UserControls.Title titleBar;
        protected System.Web.UI.WebControls.Label lblMessage;
        protected System.Web.UI.WebControls.TextBox txtNewPassword;
        protected System.Web.UI.WebControls.TextBox txtConfirmNewPassword;
        protected System.Web.UI.WebControls.Button btnChange;
    }
}
