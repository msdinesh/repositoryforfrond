<%@ Register TagPrefix="uc1" TagName="Title" Src="UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="RandomImage" Src="UserControls/RandomImage.ascx" %>
<%@ Page language="c#" Codebehind="Login.aspx.cs" AutoEventWireup="True" Inherits="Frond.Login" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>SFLogin</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK rel="stylesheet" type="text/css" href="Css/FrondDefault.css">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server" autocomplete="off">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:Title id="titleBar" runat="server" imagesource="Images/login24silver.gif"></uc1:Title>
				<P></P>
				<table class="splashTable" id="uxSplashTable" Runat="server">
					<tr>
						<td class="splashCell">
							<P>&nbsp;<asp:label id="lblLoginResult" runat="server" ForeColor="Red" CssClass="errorMessage"></asp:label>
							</P>
							<P>
								<TABLE id="splashLoginTable" cellSpacing="1" cellPadding="1" border="0">
									<TR>
										<TD class="label" align="right">Login:</TD>
										<TD><asp:textbox id="txtUsername" runat="server" cssclass="textinput"></asp:textbox></TD>
									</TR>
									<TR>
										<TD class="label" align="right">Password:</TD>
										<TD><asp:textbox id="txtPassword" runat="server" cssclass="textinput" TextMode="Password"></asp:textbox></TD>
									</TR>
									<tr>
										<TD></TD>
										<TD class="label" align="right">Remember Me
											<asp:CheckBox id="cbRememberMe" runat="server"></asp:CheckBox></TD>
									</tr>
								</TABLE>
							</P>
							<P>&nbsp;
								<asp:button id="cmdLogin" runat="server" CssClass="CommandButton" Text="Login" onclick="cmdLogin_Click"></asp:button></P>
						</td>
						<td class="splashCell">
							<uc1:RandomImage id="uxRandomImage1" runat="server" ImageLibraryXMLFilename="Xml\ImageLibrary.XML"></uc1:RandomImage>
						</td>
					</tr>
					<tr>
						<td class="splashCell">
							<uc1:RandomImage id="uxRandomImage2" runat="server" ImageLibraryXMLFilename="Xml\ImageLibrary.XML"></uc1:RandomImage>
						</td>
						<td class="splashCell">
							<div class="splashVSOLogo">
								<IMG src="Images/vso_whiteonpurple_72dpi.jpg">
							</div>
						</td>
					</tr>
				</table>
				<p><asp:linkbutton id="cmdShowVersions" CssClass="smalllinkbutton" Text="Show configuration info" Runat="server" onclick="cmdShowVersions_Click"></asp:linkbutton></p>
				<P>
					<TABLE class="fieldtable" id="tblVersions" cellSpacing="1" cellPadding="1" border="0" Runat="server">
						<TR>
							<TD class="fieldlabel"><STRONG>Database Info</STRONG></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">Database Server:</TD>
							<TD class="fieldvalue"><asp:label id="lblDatabaseServer" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">Database:</TD>
							<TD class="fieldvalue"><asp:label id="lblDatabase" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">Full Url:</TD>
							<TD class="fieldvalue">
								<asp:label id="uxUrlDesc" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel"><STRONG>Assembly Info</STRONG></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">VSO.Frond.dll&nbsp;version:</TD>
							<TD class="fieldvalue"><asp:label id="lblAssemblyVersion" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel"><STRONG>Powered by Bonobo</STRONG></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">VSO.BonoboEngine.dll&nbsp;version:</TD>
							<TD class="fieldvalue"><asp:label id="lblBonoboEngine" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">VSO.BonoboDomainObjects.dll&nbsp;version:</TD>
							<TD class="fieldvalue"><asp:label id="lblBonoboDomainObjects" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">VSO.BonoboWebControls.dll&nbsp;version:</TD>
							<TD class="fieldvalue"><asp:label id="lblBonoboWebControls" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">VSO.SF2ReportsEngine.dll version:</TD>
							<TD class="fieldvalue">
								<asp:Label id="lblSF2ReportsEngine" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel"><STRONG>Client Info</STRONG></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">
                                Client IP</TD>
							<TD class="fieldvalue"><asp:label id="lblHostIP" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">VSO Office</TD>
							<TD class="fieldvalue"><asp:label id="lblVSOOffice" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">Session Length allowed</TD>
							<TD class="fieldvalue"><asp:label id="lblSessionAllowed" runat="server"></asp:label></TD>
						</TR>
					</TABLE>
				</P>
			</div>
		</form>
	</body>
</HTML>
