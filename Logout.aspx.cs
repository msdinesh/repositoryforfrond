using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using BonoboEngine;
using BonoboWebControls;

namespace Frond
{
	/// <summary>
	/// Summary description for SFLogout.
	/// </summary>
	public partial class Logout : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
		protected UserControls.MenuBar menuBar;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			bool sessionTimedOut = false;
			if (this.Request.QueryString["SessionTimedOut"] != null)
				sessionTimedOut = true;

			// only end Bonobo Session if user explicitly logged out
			if (!sessionTimedOut)
				BnbEngine.SessionManager.EndSession();
			// end authentication
			FormsAuthentication.SignOut();
			Session.Abandon();
			titleBar.TitleText = "You have been Logged Out";
			if (sessionTimedOut)
				titleBar.TitleText = "Your session timed-out on the server";

			// set web styleid of menu bar based on URL
			menuBar.OverrideWebStyleID = Login.DeriveWebStyleIDFromURL(this.Request);
			BnbWorkareaManager.SetPageStyle(this, Login.DeriveWebStyleIDFromURL(this.Request));


		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
