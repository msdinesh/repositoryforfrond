-- Frond 1.8.1.3 meta-data changes
/*
Script created by SQL Data Compare version 5.1.0.106 from Red Gate Software Ltd at 01/06/2009 17:00:12

Run this script on LONVA03.VAUKLive

This script will make changes to LONVA03.VAUKLive to make it the same as DEV04.Frond1813

Note that this script will carry out all DELETE commands for all tables first, then all the UPDATES and then all the INSERTS
It will disable foreign key constraints at the beginning of the script, and re-enable them at the end
*/
SET NUMERIC_ROUNDABORT OFF
GO
SET XACT_ABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

BEGIN TRANSACTION

-- Drop constraints from [dbo].[appMetaViewFieldMap]
ALTER TABLE [dbo].[appMetaViewFieldMap] DROP CONSTRAINT [FK_appMetaViewFieldMap_appMetaCustomField]
ALTER TABLE [dbo].[appMetaViewFieldMap] DROP CONSTRAINT [FK_appMetaViewFieldMap_appMetaField]
ALTER TABLE [dbo].[appMetaViewFieldMap] DROP CONSTRAINT [FK_appMetaViewFieldMap_appMetaView]

-- Drop constraints from [dbo].[appMetaView]
ALTER TABLE [dbo].[appMetaView] DROP CONSTRAINT [appMetaTable_appMetaView_FK1]

-- Drop constraints from [dbo].[appMetaField]
ALTER TABLE [dbo].[appMetaField] DROP CONSTRAINT [appMetaFieldType_appMetaField_FK1]
ALTER TABLE [dbo].[appMetaField] DROP CONSTRAINT [appMetaTable_appMetaField_FK1]

-- Drop constraints from [dbo].[appMetaCustomField]
ALTER TABLE [dbo].[appMetaCustomField] DROP CONSTRAINT [FK_appMetaCustomField_appMetaField]
ALTER TABLE [dbo].[appMetaCustomField] DROP CONSTRAINT [FK_appMetaCustomField_appMetaFieldType]

-- Update 51 rows in [dbo].[appMetaField]
UPDATE [dbo].[appMetaField] SET [FieldOrder]=1 WHERE [MetaFieldID]=4638
UPDATE [dbo].[appMetaField] SET [FieldOrder]=2 WHERE [MetaFieldID]=4639
UPDATE [dbo].[appMetaField] SET [FieldOrder]=3 WHERE [MetaFieldID]=4640
UPDATE [dbo].[appMetaField] SET [FieldOrder]=4 WHERE [MetaFieldID]=4641
UPDATE [dbo].[appMetaField] SET [FieldOrder]=5 WHERE [MetaFieldID]=4642
UPDATE [dbo].[appMetaField] SET [FieldOrder]=6 WHERE [MetaFieldID]=4643
UPDATE [dbo].[appMetaField] SET [FieldOrder]=7 WHERE [MetaFieldID]=4644
UPDATE [dbo].[appMetaField] SET [FieldOrder]=8 WHERE [MetaFieldID]=4645
UPDATE [dbo].[appMetaField] SET [FieldOrder]=9 WHERE [MetaFieldID]=4646
UPDATE [dbo].[appMetaField] SET [FieldOrder]=10 WHERE [MetaFieldID]=4647
UPDATE [dbo].[appMetaField] SET [FieldOrder]=11 WHERE [MetaFieldID]=4648
UPDATE [dbo].[appMetaField] SET [MetaFieldTypeID]=1011, [FieldOrder]=12 WHERE [MetaFieldID]=4649
UPDATE [dbo].[appMetaField] SET [FieldOrder]=13 WHERE [MetaFieldID]=4650
UPDATE [dbo].[appMetaField] SET [FieldOrder]=14 WHERE [MetaFieldID]=4651
UPDATE [dbo].[appMetaField] SET [FieldOrder]=15 WHERE [MetaFieldID]=4652
UPDATE [dbo].[appMetaField] SET [FieldOrder]=16 WHERE [MetaFieldID]=4653
UPDATE [dbo].[appMetaField] SET [FieldOrder]=17 WHERE [MetaFieldID]=4654
UPDATE [dbo].[appMetaField] SET [MetaFieldTypeID]=1011, [FieldOrder]=18 WHERE [MetaFieldID]=4655
UPDATE [dbo].[appMetaField] SET [FieldOrder]=19 WHERE [MetaFieldID]=4656
UPDATE [dbo].[appMetaField] SET [FieldOrder]=20 WHERE [MetaFieldID]=4657
UPDATE [dbo].[appMetaField] SET [FieldOrder]=21 WHERE [MetaFieldID]=4658
UPDATE [dbo].[appMetaField] SET [FieldOrder]=22 WHERE [MetaFieldID]=4659
UPDATE [dbo].[appMetaField] SET [FieldOrder]=23 WHERE [MetaFieldID]=4660
UPDATE [dbo].[appMetaField] SET [FieldOrder]=24 WHERE [MetaFieldID]=4661
UPDATE [dbo].[appMetaField] SET [FieldOrder]=25 WHERE [MetaFieldID]=4662
UPDATE [dbo].[appMetaField] SET [FieldOrder]=26 WHERE [MetaFieldID]=4663
UPDATE [dbo].[appMetaField] SET [FieldOrder]=27 WHERE [MetaFieldID]=4664
UPDATE [dbo].[appMetaField] SET [FieldOrder]=28 WHERE [MetaFieldID]=4665
UPDATE [dbo].[appMetaField] SET [FieldOrder]=29 WHERE [MetaFieldID]=4666
UPDATE [dbo].[appMetaField] SET [FieldOrder]=30 WHERE [MetaFieldID]=4667
UPDATE [dbo].[appMetaField] SET [FieldOrder]=31 WHERE [MetaFieldID]=4668
UPDATE [dbo].[appMetaField] SET [FieldOrder]=32 WHERE [MetaFieldID]=4669
UPDATE [dbo].[appMetaField] SET [FieldOrder]=33 WHERE [MetaFieldID]=4670
UPDATE [dbo].[appMetaField] SET [FieldOrder]=34 WHERE [MetaFieldID]=4671
UPDATE [dbo].[appMetaField] SET [FieldOrder]=35 WHERE [MetaFieldID]=4672
UPDATE [dbo].[appMetaField] SET [FieldOrder]=36 WHERE [MetaFieldID]=4673
UPDATE [dbo].[appMetaField] SET [FieldOrder]=37 WHERE [MetaFieldID]=4674
UPDATE [dbo].[appMetaField] SET [FieldOrder]=38 WHERE [MetaFieldID]=4675
UPDATE [dbo].[appMetaField] SET [FieldOrder]=39 WHERE [MetaFieldID]=4676
UPDATE [dbo].[appMetaField] SET [FieldOrder]=40 WHERE [MetaFieldID]=4677
UPDATE [dbo].[appMetaField] SET [FieldOrder]=41 WHERE [MetaFieldID]=4678
UPDATE [dbo].[appMetaField] SET [FieldOrder]=42 WHERE [MetaFieldID]=4679
UPDATE [dbo].[appMetaField] SET [FieldOrder]=43 WHERE [MetaFieldID]=4680
UPDATE [dbo].[appMetaField] SET [FieldOrder]=44 WHERE [MetaFieldID]=4681
UPDATE [dbo].[appMetaField] SET [FieldOrder]=45 WHERE [MetaFieldID]=4682
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46 WHERE [MetaFieldID]=4683
UPDATE [dbo].[appMetaField] SET [FieldOrder]=47 WHERE [MetaFieldID]=4684
UPDATE [dbo].[appMetaField] SET [FieldOrder]=48 WHERE [MetaFieldID]=4685
UPDATE [dbo].[appMetaField] SET [FieldOrder]=49 WHERE [MetaFieldID]=4686
UPDATE [dbo].[appMetaField] SET [FieldOrder]=50 WHERE [MetaFieldID]=4687
UPDATE [dbo].[appMetaField] SET [FieldOrder]=51 WHERE [MetaFieldID]=4688

-- Add 1 row to [dbo].[appMetaCustomField]
SET IDENTITY_INSERT [dbo].[appMetaCustomField] ON
INSERT INTO [dbo].[appMetaCustomField] ([MetaCustomFieldID], [CustomFieldName], [CustomFieldAlias], [MetaFieldTypeID], [CustomFieldDescription], [TranslatesKeyFieldID], [MetaApplicationID]) VALUES (259, N'Count', N'Count', 1008, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[appMetaCustomField] OFF

-- Add 7 rows to [dbo].[appMetaField]
SET IDENTITY_INSERT [dbo].[appMetaField] ON
INSERT INTO [dbo].[appMetaField] ([MetaFieldID], [MetaTableID], [FieldName], [FieldUserAlias], [IsPrimaryKey], [NoLongerUsed], [ForeignKeyToMetaFieldID], [MaxLength], [MetaFieldTypeID], [FieldDescription], [FieldOrder], [TranslatesKeyFieldID], [IsRowGuidCol], [MetaApplicationID], [ToolTip]) VALUES (8188, 1091, N'RaisersEdgeID', NULL, 0, 0, NULL, 50, 1006, NULL, 52, NULL, 0, NULL, NULL)
INSERT INTO [dbo].[appMetaField] ([MetaFieldID], [MetaTableID], [FieldName], [FieldUserAlias], [IsPrimaryKey], [NoLongerUsed], [ForeignKeyToMetaFieldID], [MaxLength], [MetaFieldTypeID], [FieldDescription], [FieldOrder], [TranslatesKeyFieldID], [IsRowGuidCol], [MetaApplicationID], [ToolTip]) VALUES (8189, 1091, N'IdentityCardNo', NULL, 0, 0, NULL, 50, 1006, NULL, 53, NULL, 0, NULL, NULL)
INSERT INTO [dbo].[appMetaField] ([MetaFieldID], [MetaTableID], [FieldName], [FieldUserAlias], [IsPrimaryKey], [NoLongerUsed], [ForeignKeyToMetaFieldID], [MaxLength], [MetaFieldTypeID], [FieldDescription], [FieldOrder], [TranslatesKeyFieldID], [IsRowGuidCol], [MetaApplicationID], [ToolTip]) VALUES (8190, 1091, N'MaritalStatusID', NULL, 0, 0, 3863, NULL, 1008, NULL, 54, NULL, 0, NULL, NULL)
INSERT INTO [dbo].[appMetaField] ([MetaFieldID], [MetaTableID], [FieldName], [FieldUserAlias], [IsPrimaryKey], [NoLongerUsed], [ForeignKeyToMetaFieldID], [MaxLength], [MetaFieldTypeID], [FieldDescription], [FieldOrder], [TranslatesKeyFieldID], [IsRowGuidCol], [MetaApplicationID], [ToolTip]) VALUES (8191, 1091, N'MaritalStatusDate', NULL, 0, 0, NULL, NULL, 1007, NULL, 55, NULL, 0, NULL, NULL)
INSERT INTO [dbo].[appMetaField] ([MetaFieldID], [MetaTableID], [FieldName], [FieldUserAlias], [IsPrimaryKey], [NoLongerUsed], [ForeignKeyToMetaFieldID], [MaxLength], [MetaFieldTypeID], [FieldDescription], [FieldOrder], [TranslatesKeyFieldID], [IsRowGuidCol], [MetaApplicationID], [ToolTip]) VALUES (8192, 1091, N'AlternateEmail', NULL, 0, 0, NULL, 50, 1006, NULL, 56, NULL, 0, NULL, NULL)
INSERT INTO [dbo].[appMetaField] ([MetaFieldID], [MetaTableID], [FieldName], [FieldUserAlias], [IsPrimaryKey], [NoLongerUsed], [ForeignKeyToMetaFieldID], [MaxLength], [MetaFieldTypeID], [FieldDescription], [FieldOrder], [TranslatesKeyFieldID], [IsRowGuidCol], [MetaApplicationID], [ToolTip]) VALUES (8193, 1091, N'VSOTypeID', NULL, 0, 0, 5875, NULL, 1008, NULL, 57, NULL, 0, NULL, NULL)
INSERT INTO [dbo].[appMetaField] ([MetaFieldID], [MetaTableID], [FieldName], [FieldUserAlias], [IsPrimaryKey], [NoLongerUsed], [ForeignKeyToMetaFieldID], [MaxLength], [MetaFieldTypeID], [FieldDescription], [FieldOrder], [TranslatesKeyFieldID], [IsRowGuidCol], [MetaApplicationID], [ToolTip]) VALUES (8194, 1091, N'RecruitmentBaseID', NULL, 0, 0, 4140, NULL, 1008, NULL, 58, NULL, 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[appMetaField] OFF

-- Add 2 rows to [dbo].[appMetaView]
SET IDENTITY_INSERT [dbo].[appMetaView] ON
INSERT INTO [dbo].[appMetaView] ([MetaViewID], [ViewName], [ViewUserAlias], [IsLookupView], [LookupForTableID], [ViewKeyField], [DefaultOrderFields], [MetaApplicationID], [IsDefaultLookupViewForTable], [IsQueryView], [DefaultColumnDisplayList], [UserDescription], [DeveloperNotes]) VALUES (1633, N'vlkuBonoboVSOGroupForRVConversion', NULL, 1, 1248, N'VSOGroupID', NULL, NULL, 0, 0, NULL, NULL, NULL)
INSERT INTO [dbo].[appMetaView] ([MetaViewID], [ViewName], [ViewUserAlias], [IsLookupView], [LookupForTableID], [ViewKeyField], [DefaultOrderFields], [MetaApplicationID], [IsDefaultLookupViewForTable], [IsQueryView], [DefaultColumnDisplayList], [UserDescription], [DeveloperNotes]) VALUES (1634, N'vwBonoboRVTrackingTotals', NULL, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[appMetaView] OFF

-- Add 6 rows to [dbo].[appMetaViewFieldMap]
SET IDENTITY_INSERT [dbo].[appMetaViewFieldMap] ON
INSERT INTO [dbo].[appMetaViewFieldMap] ([MetaViewFieldID], [MetaViewID], [ViewColumnName], [MapsToMetaFieldID], [MapsToMetaCustomFieldID], [TranslatesViewColumn], [MetaApplicationID]) VALUES (4600, 1246, N'tblRVTracking_VSOTypeID', 8193, NULL, NULL, NULL)
INSERT INTO [dbo].[appMetaViewFieldMap] ([MetaViewFieldID], [MetaViewID], [ViewColumnName], [MapsToMetaFieldID], [MapsToMetaCustomFieldID], [TranslatesViewColumn], [MetaApplicationID]) VALUES (4601, 1246, N'lkuVSOType_Description', 5876, NULL, NULL, NULL)
INSERT INTO [dbo].[appMetaViewFieldMap] ([MetaViewFieldID], [MetaViewID], [ViewColumnName], [MapsToMetaFieldID], [MapsToMetaCustomFieldID], [TranslatesViewColumn], [MetaApplicationID]) VALUES (4602, 1246, N'lkuVSOGroup_VSOGroupID', 6224, NULL, NULL, NULL)
INSERT INTO [dbo].[appMetaViewFieldMap] ([MetaViewFieldID], [MetaViewID], [ViewColumnName], [MapsToMetaFieldID], [MapsToMetaCustomFieldID], [TranslatesViewColumn], [MetaApplicationID]) VALUES (4603, 1634, N'tblRVTracking_RVRecordStatusID', 4648, NULL, NULL, NULL)
INSERT INTO [dbo].[appMetaViewFieldMap] ([MetaViewFieldID], [MetaViewID], [ViewColumnName], [MapsToMetaFieldID], [MapsToMetaCustomFieldID], [TranslatesViewColumn], [MetaApplicationID]) VALUES (4604, 1634, N'lkuVSOGroup_VSOGroupID', 6224, NULL, NULL, NULL)
INSERT INTO [dbo].[appMetaViewFieldMap] ([MetaViewFieldID], [MetaViewID], [ViewColumnName], [MapsToMetaFieldID], [MapsToMetaCustomFieldID], [TranslatesViewColumn], [MetaApplicationID]) VALUES (4605, 1634, N'Custom_Count', NULL, 259, NULL, NULL)
SET IDENTITY_INSERT [dbo].[appMetaViewFieldMap] OFF

-- Add constraints to [dbo].[appMetaViewFieldMap]
ALTER TABLE [dbo].[appMetaViewFieldMap] ADD CONSTRAINT [FK_appMetaViewFieldMap_appMetaCustomField] FOREIGN KEY ([MapsToMetaCustomFieldID]) REFERENCES [dbo].[appMetaCustomField] ([MetaCustomFieldID])
ALTER TABLE [dbo].[appMetaViewFieldMap] ADD CONSTRAINT [FK_appMetaViewFieldMap_appMetaField] FOREIGN KEY ([MapsToMetaFieldID]) REFERENCES [dbo].[appMetaField] ([MetaFieldID])
ALTER TABLE [dbo].[appMetaViewFieldMap] ADD CONSTRAINT [FK_appMetaViewFieldMap_appMetaView] FOREIGN KEY ([MetaViewID]) REFERENCES [dbo].[appMetaView] ([MetaViewID])

-- Add constraints to [dbo].[appMetaView]
ALTER TABLE [dbo].[appMetaView] WITH NOCHECK ADD CONSTRAINT [appMetaTable_appMetaView_FK1] FOREIGN KEY ([LookupForTableID]) REFERENCES [dbo].[appMetaTable] ([MetaTableID]) NOT FOR REPLICATION

-- Add constraints to [dbo].[appMetaField]
ALTER TABLE [dbo].[appMetaField] ADD CONSTRAINT [appMetaFieldType_appMetaField_FK1] FOREIGN KEY ([MetaFieldTypeID]) REFERENCES [dbo].[appMetaFieldType] ([MetaFieldTypeID])
ALTER TABLE [dbo].[appMetaField] WITH NOCHECK ADD CONSTRAINT [appMetaTable_appMetaField_FK1] FOREIGN KEY ([MetaTableID]) REFERENCES [dbo].[appMetaTable] ([MetaTableID]) NOT FOR REPLICATION

-- Add constraints to [dbo].[appMetaCustomField]
ALTER TABLE [dbo].[appMetaCustomField] ADD CONSTRAINT [FK_appMetaCustomField_appMetaField] FOREIGN KEY ([TranslatesKeyFieldID]) REFERENCES [dbo].[appMetaField] ([MetaFieldID])
ALTER TABLE [dbo].[appMetaCustomField] ADD CONSTRAINT [FK_appMetaCustomField_appMetaFieldType] FOREIGN KEY ([MetaFieldTypeID]) REFERENCES [dbo].[appMetaFieldType] ([MetaFieldTypeID])

COMMIT TRANSACTION
