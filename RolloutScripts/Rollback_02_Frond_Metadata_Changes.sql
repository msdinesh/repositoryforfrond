-- rollback script for frond 1.8.1.3 metadata changes
/*
Script created by SQL Data Compare version 5.1.0.106 from Red Gate Software Ltd at 01/06/2009 17:00:54

Run this script on DEV04.Frond1813

This script will make changes to DEV04.Frond1813 to make it the same as LONVA03.VAUKLive

Note that this script will carry out all DELETE commands for all tables first, then all the UPDATES and then all the INSERTS
It will disable foreign key constraints at the beginning of the script, and re-enable them at the end
*/
SET NUMERIC_ROUNDABORT OFF
GO
SET XACT_ABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

BEGIN TRANSACTION

-- Drop constraints from [dbo].[appMetaViewFieldMap]
ALTER TABLE [dbo].[appMetaViewFieldMap] DROP CONSTRAINT [FK_appMetaViewFieldMap_appMetaCustomField]
ALTER TABLE [dbo].[appMetaViewFieldMap] DROP CONSTRAINT [FK_appMetaViewFieldMap_appMetaField]
ALTER TABLE [dbo].[appMetaViewFieldMap] DROP CONSTRAINT [FK_appMetaViewFieldMap_appMetaView]

-- Drop constraints from [dbo].[appMetaView]
ALTER TABLE [dbo].[appMetaView] DROP CONSTRAINT [appMetaTable_appMetaView_FK1]

-- Drop constraints from [dbo].[appMetaField]
ALTER TABLE [dbo].[appMetaField] DROP CONSTRAINT [appMetaFieldType_appMetaField_FK1]
ALTER TABLE [dbo].[appMetaField] DROP CONSTRAINT [appMetaTable_appMetaField_FK1]

-- Drop constraints from [dbo].[appMetaCustomField]
ALTER TABLE [dbo].[appMetaCustomField] DROP CONSTRAINT [FK_appMetaCustomField_appMetaField]
ALTER TABLE [dbo].[appMetaCustomField] DROP CONSTRAINT [FK_appMetaCustomField_appMetaFieldType]

-- Delete 6 rows from [dbo].[appMetaViewFieldMap]
DELETE FROM [dbo].[appMetaViewFieldMap] WHERE [MetaViewFieldID]=4600
DELETE FROM [dbo].[appMetaViewFieldMap] WHERE [MetaViewFieldID]=4601
DELETE FROM [dbo].[appMetaViewFieldMap] WHERE [MetaViewFieldID]=4602
DELETE FROM [dbo].[appMetaViewFieldMap] WHERE [MetaViewFieldID]=4603
DELETE FROM [dbo].[appMetaViewFieldMap] WHERE [MetaViewFieldID]=4604
DELETE FROM [dbo].[appMetaViewFieldMap] WHERE [MetaViewFieldID]=4605

-- Delete 2 rows from [dbo].[appMetaView]
DELETE FROM [dbo].[appMetaView] WHERE [MetaViewID]=1633
DELETE FROM [dbo].[appMetaView] WHERE [MetaViewID]=1634

-- Delete 7 rows from [dbo].[appMetaField]
DELETE FROM [dbo].[appMetaField] WHERE [MetaFieldID]=8188
DELETE FROM [dbo].[appMetaField] WHERE [MetaFieldID]=8189
DELETE FROM [dbo].[appMetaField] WHERE [MetaFieldID]=8190
DELETE FROM [dbo].[appMetaField] WHERE [MetaFieldID]=8191
DELETE FROM [dbo].[appMetaField] WHERE [MetaFieldID]=8192
DELETE FROM [dbo].[appMetaField] WHERE [MetaFieldID]=8193
DELETE FROM [dbo].[appMetaField] WHERE [MetaFieldID]=8194

-- Delete 1 row from [dbo].[appMetaCustomField]
DELETE FROM [dbo].[appMetaCustomField] WHERE [MetaCustomFieldID]=259

-- Update 51 rows in [dbo].[appMetaField]
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46380 WHERE [MetaFieldID]=4638
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46390 WHERE [MetaFieldID]=4639
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46400 WHERE [MetaFieldID]=4640
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46410 WHERE [MetaFieldID]=4641
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46420 WHERE [MetaFieldID]=4642
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46430 WHERE [MetaFieldID]=4643
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46440 WHERE [MetaFieldID]=4644
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46450 WHERE [MetaFieldID]=4645
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46460 WHERE [MetaFieldID]=4646
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46470 WHERE [MetaFieldID]=4647
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46480 WHERE [MetaFieldID]=4648
UPDATE [dbo].[appMetaField] SET [MetaFieldTypeID]=1006, [FieldOrder]=46490 WHERE [MetaFieldID]=4649
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46500 WHERE [MetaFieldID]=4650
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46510 WHERE [MetaFieldID]=4651
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46520 WHERE [MetaFieldID]=4652
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46530 WHERE [MetaFieldID]=4653
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46540 WHERE [MetaFieldID]=4654
UPDATE [dbo].[appMetaField] SET [MetaFieldTypeID]=1006, [FieldOrder]=46550 WHERE [MetaFieldID]=4655
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46560 WHERE [MetaFieldID]=4656
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46570 WHERE [MetaFieldID]=4657
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46580 WHERE [MetaFieldID]=4658
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46590 WHERE [MetaFieldID]=4659
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46600 WHERE [MetaFieldID]=4660
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46610 WHERE [MetaFieldID]=4661
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46620 WHERE [MetaFieldID]=4662
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46630 WHERE [MetaFieldID]=4663
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46640 WHERE [MetaFieldID]=4664
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46650 WHERE [MetaFieldID]=4665
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46660 WHERE [MetaFieldID]=4666
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46670 WHERE [MetaFieldID]=4667
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46680 WHERE [MetaFieldID]=4668
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46690 WHERE [MetaFieldID]=4669
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46700 WHERE [MetaFieldID]=4670
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46710 WHERE [MetaFieldID]=4671
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46720 WHERE [MetaFieldID]=4672
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46730 WHERE [MetaFieldID]=4673
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46740 WHERE [MetaFieldID]=4674
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46750 WHERE [MetaFieldID]=4675
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46760 WHERE [MetaFieldID]=4676
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46770 WHERE [MetaFieldID]=4677
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46780 WHERE [MetaFieldID]=4678
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46790 WHERE [MetaFieldID]=4679
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46800 WHERE [MetaFieldID]=4680
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46810 WHERE [MetaFieldID]=4681
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46820 WHERE [MetaFieldID]=4682
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46830 WHERE [MetaFieldID]=4683
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46840 WHERE [MetaFieldID]=4684
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46850 WHERE [MetaFieldID]=4685
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46860 WHERE [MetaFieldID]=4686
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46870 WHERE [MetaFieldID]=4687
UPDATE [dbo].[appMetaField] SET [FieldOrder]=46880 WHERE [MetaFieldID]=4688

-- Add constraints to [dbo].[appMetaViewFieldMap]
ALTER TABLE [dbo].[appMetaViewFieldMap] ADD CONSTRAINT [FK_appMetaViewFieldMap_appMetaCustomField] FOREIGN KEY ([MapsToMetaCustomFieldID]) REFERENCES [dbo].[appMetaCustomField] ([MetaCustomFieldID])
ALTER TABLE [dbo].[appMetaViewFieldMap] ADD CONSTRAINT [FK_appMetaViewFieldMap_appMetaField] FOREIGN KEY ([MapsToMetaFieldID]) REFERENCES [dbo].[appMetaField] ([MetaFieldID])
ALTER TABLE [dbo].[appMetaViewFieldMap] ADD CONSTRAINT [FK_appMetaViewFieldMap_appMetaView] FOREIGN KEY ([MetaViewID]) REFERENCES [dbo].[appMetaView] ([MetaViewID])

-- Add constraints to [dbo].[appMetaView]
ALTER TABLE [dbo].[appMetaView] WITH NOCHECK ADD CONSTRAINT [appMetaTable_appMetaView_FK1] FOREIGN KEY ([LookupForTableID]) REFERENCES [dbo].[appMetaTable] ([MetaTableID]) NOT FOR REPLICATION

-- Add constraints to [dbo].[appMetaField]
ALTER TABLE [dbo].[appMetaField] ADD CONSTRAINT [appMetaFieldType_appMetaField_FK1] FOREIGN KEY ([MetaFieldTypeID]) REFERENCES [dbo].[appMetaFieldType] ([MetaFieldTypeID])
ALTER TABLE [dbo].[appMetaField] WITH NOCHECK ADD CONSTRAINT [appMetaTable_appMetaField_FK1] FOREIGN KEY ([MetaTableID]) REFERENCES [dbo].[appMetaTable] ([MetaTableID]) NOT FOR REPLICATION

-- Add constraints to [dbo].[appMetaCustomField]
ALTER TABLE [dbo].[appMetaCustomField] ADD CONSTRAINT [FK_appMetaCustomField_appMetaField] FOREIGN KEY ([TranslatesKeyFieldID]) REFERENCES [dbo].[appMetaField] ([MetaFieldID])
ALTER TABLE [dbo].[appMetaCustomField] ADD CONSTRAINT [FK_appMetaCustomField_appMetaFieldType] FOREIGN KEY ([MetaFieldTypeID]) REFERENCES [dbo].[appMetaFieldType] ([MetaFieldTypeID])

COMMIT TRANSACTION
