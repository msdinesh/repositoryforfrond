-- for Frond 1.8.1.3 release
/*
Script created by SQL Compare version 5.1.0.80 from Red Gate Software Ltd at 01/06/2009 16:51:27
Run this script on LONVA03.VAUKLive to make it the same as DEV04.Frond1813
Please back up your database before running this script
*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[tblRVTracking]'
GO
ALTER TABLE [dbo].[tblRVTracking] ADD
[RaisersEdgeID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdentityCardNo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaritalStatusID] [int] NULL,
[MaritalStatusDate] [datetime] NULL,
[AlternateEmail] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VSOTypeID] [int] NULL,
[RecruitmentBaseID] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [idx_tblRVTracking_VSOTypeID] on [dbo].[tblRVTracking]'
GO
CREATE NONCLUSTERED INDEX [idx_tblRVTracking_VSOTypeID] ON [dbo].[tblRVTracking] ([VSOTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[vwBonoboRVTrackingTotals]'
GO

CREATE VIEW [dbo].[vwBonoboRVTrackingTotals]
AS
SELECT rv.RVRecordStatusID as tblRVTracking_RVRecordStatusID, 
		vtype.VSOGroupID as lkuVSOGroup_VSOGroupID,
		count(*) as Custom_Count
FROM tblRVTracking rv
LEFT OUTER JOIN lkuVSOType vtype on vtype.VSOTypeID = rv.VSOTypeID
LEFT OUTER JOIN lkuVSOGroup vgroup on vgroup.VSOGroupID = vtype.VSOGroupID
WHERE rv.RVRecordStatusID not in (1002, 1005)
GROUP BY rv.RVRecordStatusID, vtype.VSOGroupID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[vlkuBonoboVSOGroupForRVConversion]'
GO
create view [dbo].[vlkuBonoboVSOGroupForRVConversion]
as
-- this lookup is used for RVConversion
-- the case statment below is used to un-exclude some excluded types
select vg.VSOGroupID,
vg.[Description] ,
case when vg.VSOGroupID = 6525 then cast(0 as bit)
else vg.Exclude end as Exclude
from lkuVSOGroup vg

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[vwBonoboFindRVTracking]'
GO
ALTER  VIEW [dbo].[vwBonoboFindRVTracking]
AS
SELECT RVTrackingID AS tblRVTracking_RVTrackingID, 
       Surname AS tblRVTracking_Surname,
       Forename AS tblRVTracking_Forename,
       SurnamePrefix AS tblRVTracking_SurnamePrefix,
	rv.VolReferenceNo as tblRVTracking_VolReferenceNo,
	c.Description as lkuCountry_Description,
	rv.CountryID as tblRVTracking_CountryID,
	rvtype.Description as lkuRVRecordType_Description,
	rvstatus.Description as lkuRVRecordStatus_Description,
	rvstatus.RVRecordStatusID as lkuRVRecordStatus_RVRecordStatusID,
	rv.VSOTypeID as tblRVTracking_VSOTypeID,
	vtype.VSOGroupID as lkuVSOGroup_VSOGroupID,
	vgroup.[Description] + '/' + vtype.[Description] as lkuVSOType_Description
FROM tblRVTracking rv
INNER JOIN lkuRVRecordType rvtype on rvtype.RVRecordTypeID = rv.RVRecordTypeID
INNER JOIN lkuRVRecordStatus rvstatus on rvstatus.RVRecordStatusID = rv.RVRecordStatusID
LEFT OUTER JOIN lkuCountry c on c.CountryID = rv.CountryID
LEFT OUTER JOIN lkuVSOType vtype on vtype.VSOTypeID = rv.VSOTypeID
LEFT OUTER JOIN lkuVSOGroup vgroup on vgroup.VSOGroupID = vtype.VSOGroupID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
