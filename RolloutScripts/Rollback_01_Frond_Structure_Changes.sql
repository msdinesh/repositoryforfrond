-- rollback script for 1.8.1.3 structure changes
/*
Script created by SQL Compare version 5.1.0.80 from Red Gate Software Ltd at 01/06/2009 16:56:43
Run this script on DEV04.Frond1813 to make it the same as LONVA03.VAUKLive
Please back up your database before running this script
*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping index [idx_tblRVTracking_VSOTypeID] from [dbo].[tblRVTracking]'
GO
DROP INDEX [idx_tblRVTracking_VSOTypeID] ON [dbo].[tblRVTracking]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[vlkuBonoboVSOGroupForRVConversion]'
GO
DROP VIEW [dbo].[vlkuBonoboVSOGroupForRVConversion]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[vwBonoboRVTrackingTotals]'
GO
DROP VIEW [dbo].[vwBonoboRVTrackingTotals]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[tblRVTracking]'
GO
ALTER TABLE [dbo].[tblRVTracking] DROP
COLUMN [RaisersEdgeID],
COLUMN [IdentityCardNo],
COLUMN [MaritalStatusID],
COLUMN [MaritalStatusDate],
COLUMN [AlternateEmail],
COLUMN [VSOTypeID],
COLUMN [RecruitmentBaseID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[vwBonoboFindRVTracking]'
GO

ALTER  VIEW dbo.vwBonoboFindRVTracking
AS
SELECT RVTrackingID AS tblRVTracking_RVTrackingID, 
       Surname AS tblRVTracking_Surname,
       Forename AS tblRVTracking_Forename,
       SurnamePrefix AS tblRVTracking_SurnamePrefix,
	rv.VolReferenceNo as tblRVTracking_VolReferenceNo,
	c.Description as lkuCountry_Description,
	rv.CountryID as tblRVTracking_CountryID,
	rvtype.Description as lkuRVRecordType_Description,
	rvstatus.Description as lkuRVRecordStatus_Description,
	rvstatus.RVRecordStatusID as lkuRVRecordStatus_RVRecordStatusID
FROM tblRVTracking rv
INNER JOIN lkuRVRecordType rvtype on rvtype.RVRecordTypeID = rv.RVRecordTypeID
INNER JOIN lkuRVRecordStatus rvstatus on rvstatus.RVRecordStatusID = rv.RVRecordStatusID
LEFT OUTER JOIN lkuCountry c on c.CountryID = rv.CountryID



 



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
