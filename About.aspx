<%@ Register TagPrefix="uc1" TagName="RandomImage" Src="UserControls/RandomImage.ascx" %>
<%@ Page language="c#" Codebehind="About.aspx.cs" AutoEventWireup="True" Inherits="Frond.About" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>About</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK rel="stylesheet" type="text/css" href="Css/FrondDefault.css">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="Images/batchprocess24silver.gif"></uc1:title>
				<p></p>
				<bnbpagecontrol:BnbTabControl id="BnbTabControl1" runat="server" IgnoreWorkareas="true" GetShowAllTabsVerticallyFromSession="false"></bnbpagecontrol:BnbTabControl>
				<asp:Panel id="ConfigInfoPanel" runat="server" CssClass="tcTab" BnbTabName="Config Info">
					<TABLE class="fieldtable" id="tblVersions" cellSpacing="1" cellPadding="1" border="0" Runat="server">
						<TR>
							<TD class="fieldlabel"><STRONG>Database Info</STRONG></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">Database Server:</TD>
							<TD class="fieldvalue">
								<asp:label id="lblDatabaseServer" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">Database:</TD>
							<TD class="fieldvalue">
								<asp:label id="lblDatabase" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel"><STRONG>Web Server Info</STRONG></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">Web Server:</TD>
							<TD class="fieldvalue">
								<asp:label id="uxWebServerDesc" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">Virtual Path:</TD>
							<TD class="fieldvalue">
								<asp:label id="uxVirtualDirDesc" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">Full Url:</TD>
							<TD class="fieldvalue">
								<asp:label id="uxUrlDesc" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel"><STRONG>Assembly Info</STRONG></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">VSO.Frond.dll&nbsp;version:</TD>
							<TD class="fieldvalue">
								<asp:label id="lblAssemblyVersion" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel"><STRONG>Powered by Bonobo</STRONG></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">VSO.BonoboEngine.dll&nbsp;version:</TD>
							<TD class="fieldvalue">
								<asp:label id="lblBonoboEngine" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">VSO.BonoboDomainObjects.dll&nbsp;version:</TD>
							<TD class="fieldvalue">
								<asp:label id="lblBonoboDomainObjects" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">VSO.BonoboWebControls.dll&nbsp;version:</TD>
							<TD class="fieldvalue">
								<asp:label id="lblBonoboWebControls" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">VSO.SF2ReportsEngine.dll version:</TD>
							<TD class="fieldvalue">
								<asp:Label id="lblSF2ReportsEngine" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel"><STRONG>Client Info</STRONG></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">
                                Client IP</TD>
							<TD class="fieldvalue">
								<asp:label id="lblHostIP" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">VSO Office</TD>
							<TD class="fieldvalue">
								<asp:label id="lblVSOOffice" runat="server"></asp:label></TD>
						</TR>
						<TR>
							<TD class="fieldlabel">Session Length allowed</TD>
							<TD class="fieldvalue">
								<asp:label id="lblSessionAllowed" runat="server"></asp:label></TD>
						</TR>
					</TABLE>
				</asp:Panel>
				<asp:Panel id="CreditsTab" runat="server" CssClass="tcTab" BnbTabName="Credits">
					<h3>
                        <asp:Label ID="uxVirtualApplicationName" runat="server"></asp:Label>&nbsp; (c) VSO 2008</h3>
                    
                        <asp:Panel ID="uxVirtualAppPanel" runat="server" Visible="False" >
                            <b><asp:Label ID="uxVirtualApplicationName2" runat="server"></asp:Label> runs on top of the Frond Platform application.</b></asp:Panel>
                       
					<P>Frond is a Microsoft .NET Web Application.<BR>
						It is built on top of the 'Bonobo' middleware library.</P>
					<P><I>Frond and Bonobo were built by the following developers, working in or for VSO:</I></P>
					<P>
						<asp:Label id="uxDeveloperCredits" runat="server"></asp:Label></P>
					<P><I>Frond and Bonobo make use of the following third-party systems:</I></P>
					<P>
						<asp:Label id="uxSystemCredits" runat="server"></asp:Label></P>
					<P><I>Frond is managed and supported by the VSO IIT Department, particularly:</I></P>
					<P>Kalton Ahmed<BR>
						Laide Martins<BR>
						Nikki Hill<BR>
						Stuart McSkimming<BR>
						Uzma Kazmi<BR>
					</P>
				</asp:Panel>
			</div>
		</form>
	</body>
</HTML>
