<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="bnbgenericcontrols" Namespace="BonoboWebControls.GenericControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrol" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Page language="c#" Codebehind="FindGeneral.aspx.cs" AutoEventWireup="True" Inherits="Frond.FindGeneral" trace="false" %>
<%@ Register TagPrefix="uc1" TagName="WaitMessagePanel" Src="UserControls/WaitMessagePanel.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="UserControls/Title.ascx" %>
<%@ Register TagPrefix="bnbcriteriacontrol" Namespace="BonoboWebControls.CriteriaControls" Assembly="BonoboWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>FindGeneral</title> 
		<!-- this page needs html 4.0 Transitional apparently because the find criteria relies on quirks mode  -->
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C#" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<link href="Css/FrondDefault.css" type="text/css" rel="stylesheet"/>
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:Title id="titleBar" runat="server" imagesource="Images/find24silver.gif"></uc1:Title>
				
				<div id="findcontrols" class="findcontrols">
					
					<p  style="padding-left:35%">
						Find&nbsp;<asp:dropdownlist id="cboFindType" runat="server" Width="250px"></asp:dropdownlist>&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;<nobr><asp:HyperLink id="lnkAdvancedFind" runat="server" Width="53px" Visible="False" ForeColor="#C000C0" Font-Bold ="true" >[lnkAdvancedFind]</asp:HyperLink>
						</nobr>						
						</p>
					<p align="center"><bnbcriteriacontrol:bnbcriteriabuildercomposite id="BnbCriteriaBuilderComposite1" runat="server" MetaDataXmlUrl="Xml\BonoboFindMetaData.xml"></bnbcriteriacontrol:bnbcriteriabuildercomposite>&nbsp;</p>
					<!-- TPT:Added checkbox and clear button for Iteration 5 -->
					<p align="center" ><asp:CheckBox runat ="server" ID ="chkinclude" CssClass ="checkbox" Visible ="false" Text ="include all projects (including closed projects)" /></p>
                    <p align="center"><asp:button id="cmdFind" runat="server" Text="Find" CssClass="button" onclick="cmdFind_Click"></asp:button>
					<asp:Button ID="cmdclear" runat ="server" Text ="Clear" CssClass ="button" OnClick="cmdclear_Click" /></p>
					<p align="center"><asp:label id="lblMessages" runat="server" CssClass="messagebox"></asp:label></p>
					<p align="center"><bnbgenericcontrols:BnbButton id="cmdWord" Visible="false" runat="server" Text="Publish result in Word" Width="150px" onclick="cmdWord_Click"></bnbgenericcontrols:BnbButton>&nbsp;</p>
					<p align="center">
						 &nbsp;<bnbdatagrid:BnbDataGridForQueryDataSet id="BnbDataGridForQueryDataSet1" runat="server" MetaDataXmlUrl="Xml\BonoboFindMetaData.xml"
							CssClass="datagrid" ColumnsToExclude="tblIndividualAdditional_ConfidentialAddress,tblProject_Global,tblGrant_DonorReference,tblGrant_BudgetLine,tblGrantProgress_GrantStatusID,tblProject_ProjectSummary, Custom_IsDonor"></bnbdatagrid:BnbDataGridForQueryDataSet>
	                 </p>
				</div>
				<uc1:WaitMessagePanel id="uxWaitMessagePanel" runat="server" DivToHide="findcontrols"></uc1:WaitMessagePanel>
			</div>
		</form>
	</body>
</HTML>
