Frond Standard Letters Folder
-----------------------------

This is the folder where user Word Templates for 'Standard Letters' should be kept.
ie. templates to be used for specific Action types.

Other templates should be stored in the Frond/Templates folder instead.

A couple of examples have been added to the Frond project file, however the other 
templates that get put in this folder should NOT be added to the Frond project
(the templates will be maintained independently by users or the business team and so do not
need to be source-controlled)

The document name should match the 'VSO Description' (from lkuContactDescription) that the doc 
corresponds to.

See VAM-147 in JIRA for more details.