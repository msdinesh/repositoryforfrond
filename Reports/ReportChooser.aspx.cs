//INSTANT C# NOTE: Formerly VB.NET project-level imports:
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Globalization;
using SF2ReportsEngine;
using BonoboEngine.Query;
using BonoboWebControls;

namespace Frond.Reports
{
	public partial class ReportChooser : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid dgrAvailableReports;
		protected UserControls.Title titleBar;

		private DataTable m_possibleReports;

	#region  Web Form Designer Generated Code 

		//This call is required by the Web Form Designer.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{

		}

//ORIGINAL LINE: Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles base.Init
		override protected void OnInit(System.EventArgs e)
		{
				base.OnInit(e);
			//CODEGEN: This method call is required by the Web Form Designer
			//Do not modify it using the code editor.
			InitializeComponent();
		}

	#endregion


		public static string GetMyURL()
		{
			string strBase = null;
			strBase = HttpContext.Current.Request.ApplicationPath;
			if (strBase == "/")
			{
					strBase = string.Empty;
				}
			return strBase + "/Reports/ReportChooser.aspx";
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//Put user code to initialize the page here
			if (! Page.IsPostBack)
			{
				titleBar.TitleText = "Report Menu";
				ShowRecentReports();
				//ShowAvailableReports();
				
				ShowRecentTemplates();
				ShowAllReports();
			}
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
		}

		/// <summary>
		/// Returns a datatable of available interactive reports suitable for binding to a datagrid.
		/// </summary>
		/// <returns></returns>
		private DataTable GetAvailableInteractiveReports()
		{
			DataTable possible = this.GetPossibleReports();
			// add some columns for run/about text
			possible.Columns.Add(new DataColumn("RunText", typeof(string)));
			possible.Columns.Add(new DataColumn("AboutText", typeof(string)));
			DataTable allowed = this.ReportChooserData;
			// loop through, deleting any which do not appear in ReportChooser.xml
			foreach(DataRow rowLoop in possible.Rows)
			{
				// set run and about text
				rowLoop["RunText"] = "Run Report";
				rowLoop["AboutText"] = "About Report";
				DataRow[] findRow = allowed.Select("ChoiceGroup = 1 and ReportID = '" + rowLoop["ReportID"].ToString() + "'");
				if (findRow.Length == 0)
					rowLoop.Delete();
				else
				{
					// also, clear template if no permission
					if ((bool)rowLoop["UserHasPermission"] == false)
					{
						rowLoop["ReportTemplateID"] = DBNull.Value;
						rowLoop["RunText"] = "(n/a)";
						rowLoop["AboutText"] = "(n/a)";
					}
				}

			}
			return possible;
		}

		
		/// <summary>
		/// Returns a datatable of available static reports suitable for binding to a datagrid
		/// XMLPath will be in the XMLPath column
		/// </summary>
		/// <returns></returns>
		private DataTable GetAvailableStaticReports()
		{
			DataTable possible = this.GetPossibleReports();
			DataTable staticPossible = new DataTable();
			// copy columns accross
			foreach(DataColumn colLoop in possible.Columns)
				staticPossible.Columns.Add(new DataColumn(colLoop.ColumnName, colLoop.DataType));
			// add another one
			staticPossible.Columns.Add(new DataColumn("XMLPath", typeof(string)));

			// find static reports in xml file
			DataRow[] staticDefn = this.ReportChooserData.Select("ChoiceGroup = 2");
			foreach(DataRow defnLoop in staticDefn)
			{
				// is it in possible reports?
				DataRow[] findRow = possible.Select("ReportID = '" + defnLoop["ReportID"].ToString() + "'");
				if (findRow.Length > 0)
				{
					DataRow reportInfo = findRow[0];
					// copy data accross
					DataRow newStaticPossible = staticPossible.NewRow();
					foreach(DataColumn colLoop in possible.Columns)
						newStaticPossible[colLoop.ColumnName] = reportInfo[colLoop];
					// add xml path
					newStaticPossible["XMLPath"] = defnLoop["XMLPath"];
					staticPossible.Rows.Add(newStaticPossible);
				}
			}
			return staticPossible;
		}

		private DataTable GetPossibleReports()
		{
			if (m_possibleReports == null)
			{
				// if workareas are turned on, call a different method!
				if (BnbWebFormManager.WorkareaChecking)
					m_possibleReports = BnbLegacyReport.GetAvailableReportsForUserWorkarea();
				else
					m_possibleReports = BnbLegacyReport.GetAvailableReportsForUser();

			}
			return m_possibleReports;

		}

		/*
		private void ShowAvailableReports()
		{
			DataTable dtbChoiceInfo = null;
			DataTable dtbChoiceDisplay = null;
			DataTable tblAvailableReports = null;
//INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
//			DataRow dtrLoop = null;
			DataRow dtrNew = null;
			string strGroup = null;
			string strType = null;
			string strRun = null;
			string strAbout = null;
			DataTable tblTemplates = null;



			dtbChoiceInfo = this.ReportChooserData;
			tblAvailableReports = sfReportRequest.GetAvailableReports();

			// make table of choices to go into data grid
			dtbChoiceDisplay = new DataTable();
			dtbChoiceDisplay.Locale = CultureInfo.InvariantCulture;
			dtbChoiceDisplay.Columns.Add(new DataColumn("Group"));
			dtbChoiceDisplay.Columns.Add(new DataColumn("Type"));
			dtbChoiceDisplay.Columns.Add(new DataColumn("Report"));
			dtbChoiceDisplay.Columns.Add(new DataColumn("Run"));
			dtbChoiceDisplay.Columns.Add(new DataColumn("About"));
			dtbChoiceDisplay.Columns.Add(new DataColumn("TypeSort"));

			// loop through
			foreach (DataRow dtrLoop in dtbChoiceInfo.Rows)
			{
				dtrNew = dtbChoiceDisplay.NewRow();

				// if report found 
				DataRow[] foundReport = null;
				foundReport = tblAvailableReports.Select("ReportID = " + System.Convert.ToString(dtrLoop["ReportID"]));
				if (foundReport.Length > 0)
				{

					// group and link
					switch (System.Convert.ToInt32(dtrLoop["ChoiceGroup"]))
					{
						case 1:
							strGroup = "Interactive";
							//try //try to check permission
							//{
								if (sfReportRequest.ReportSecurityCheck((int)(foundReport[0]["ReportID"]))) //permission granted
								{
									strRun = "<a href='" + ReportOptions.GetMyURLWithReportTemplate(foundReport[0]["ReportTemplateID"].ToString()) + "'>Run Report</a>";
									strAbout = "<a href='" + ReportInfo.GetMyURLWithReportTemplate(foundReport[0]["ReportTemplateID"].ToString()) + "'>About Report</a>";
									// for test show id		//strRun = "<a href='" + ReportOptions.GetMyURLWithReportTemplate(foundReport[0]["ReportTemplateID"].ToString()) + "'>" + (foundReport[0]["ReportID"].ToString()) +	"</a>";
								}

								else //no permission
								{
									strRun = "";
									strAbout = "";
								};
							//}

//							catch (Exception exc) //error when asking permission
//							{
//								strRun = "Permission Error";
//								strAbout = "Permission Error";
//							}
							
							break;

						case 2:
							strGroup = "Static";
							strRun = "<a href='" + ReportGroups.GetMyURLWithResultsXML(System.Convert.ToString(dtrLoop["XMLPath"])) + "'>View</a>";
							strAbout = string.Empty;
							break;
					}

					//Report type (Volunteer, Jobs, Programme Office, etc)
					tblTemplates = sfReportRequest.GetReportUserTemplates(Convert.ToInt16(dtrLoop["ReportID"]), "");
					if (tblTemplates.Rows.Count >= 1)
					{
							strType = tblTemplates.Rows[0]["ReportType"].ToString();
						}

					dtrNew["Group"] = strGroup;
					dtrNew["Type"] = strType;
					dtrNew["Run"] = strRun;
					dtrNew["About"] = strAbout;

					// description

					dtrNew["Report"] = foundReport[0]["ReportName"];
					if (foundReport[0]["VMSCatalogueNumber"] != DBNull.Value)
						dtrNew["Report"] = dtrNew["Report"] + " (" + foundReport[0]["VMSCatalogueNumber"] + ")";


					dtbChoiceDisplay.Rows.Add(dtrNew);
				}
			}

			dgrAvailableReports.DataSource = dtbChoiceDisplay;
			dgrAvailableReports.DataBind();

		}
		*/

		private void ShowAllReports()
		{
			dgrAllInteractive.DataSource = this.GetAvailableInteractiveReports();
			dgrAllInteractive.DataBind();

			DataTable staticPossible = this.GetAvailableStaticReports();
			if (staticPossible.Rows.Count > 0)
			{
				dgrAllStatic.DataSource = staticPossible;
				dgrAllStatic.DataBind();
			}
			else
			{
				lblNoStaticReportsMessage.Text = "No Static Reports are defined for this application";
			}


		}

		// returns recordset of report choices
		// (from xml file)
		private DataTable ReportChooserData
		{
			get
			{
				DataSet dtsLoad = null;
				dtsLoad = new DataSet();
				dtsLoad.ReadXml(Server.MapPath("~\\Reports\\ReportChooser.xml"));
				return dtsLoad.Tables["ReportChoice"];
			}
		}

		private void ShowRecentReports()
		{
			DataTable recent = BnbLegacyReport.GetRecentReports();
			DataTable allowed = this.ReportChooserData;
			// check reports are valid for this app
			foreach(DataRow rowLoop in recent.Rows)
			{
				DataRow[] findRow = allowed.Select("ChoiceGroup = 1 and ReportID = '" + rowLoop["ReportID"].ToString() + "'");
				if (findRow.Length == 0)
					rowLoop.Delete();
			}
			// delete any past the first 5
			int count = 0;
			foreach(DataRow rowLoop in recent.Rows)
			{
				count=count+1;
				if (count > 5)
					rowLoop.Delete();
			}
			// if any, bind the result
			if (recent.Rows.Count > 0)
			{
				dgrRecentReports.DataSource = recent;
				dgrRecentReports.DataBind();
			}
			else
			{
				lblNoRecentReportsMessage.Text = "There are no recent Reports to display. Please click on the 'All Reports' tab above to see a list of all available reports";
			}

		
		}

		private void ShowRecentTemplates()
		{
			DataTable recent = BnbLegacyReport.GetRecentReportTemplates();
			DataTable allowed = this.ReportChooserData;
			// check reports are valid for this app
			foreach(DataRow rowLoop in recent.Rows)
			{
				DataRow[] findRow = allowed.Select("ChoiceGroup = 1 and ReportID = '" + rowLoop["ReportID"].ToString() + "'");
				if (findRow.Length == 0)
					rowLoop.Delete();
			}
			// delete any past the first 5
			int count = 0;
			foreach(DataRow rowLoop in recent.Rows)
			{
				count=count+1;
				if (count > 5)
					rowLoop.Delete();
			}
			// bind the result if any
			if (recent.Rows.Count >0)
			{
				dgrRecentTemplates.DataSource = recent;
				dgrRecentTemplates.DataBind();
			}
			else
			{
				lblNoRecentTemplatesMessage.Text = "There are no recent Report Templates to display";
			}

		}

	}

} //end of root namespace