<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Page Language="c#" AutoEventWireup="True" Codebehind="ReportChooser.aspx.cs" Inherits="Frond.Reports.ReportChooser" %>
<%@ Register TagPrefix="bnbpagecontrols" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Report Chooser</title>
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<META http-equiv="REFRESH" content="540"> <!-- refresh every 9 minutes to keep connection alive -->
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:title id="titleBar" runat="server" imagesource="../Images/report24silver.gif"></uc1:title>
				<p></p>
				<bnbpagecontrols:BnbTabControl id="BnbTabControl1" runat="server" IgnoreWorkareas=true></bnbpagecontrols:BnbTabControl>
				<asp:Panel id="RecentReportsTab" CssClass="tcTab" BnbTabName="Recent Reports" runat="server" >
					<P>
						<DIV class="sectionheadingalt">Recently Run Reports</DIV>
					<P></P>
					<P>
						<asp:DataGrid id="dgrRecentReports" runat="server" CssClass="datagrid" AutoGenerateColumns="False"
							Width="536px">
							<HeaderStyle CssClass="datagridheader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="ReportNameFull" HeaderText="Report Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="ReportType" HeaderText="Report Type"></asp:BoundColumn>
								<asp:HyperLinkColumn Text="Run Report" DataNavigateUrlField="ReportTemplateID" DataNavigateUrlFormatString="ReportOptions.aspx?ReportTemplateID={0}"
									HeaderText="Run"></asp:HyperLinkColumn>
							</Columns>
						</asp:DataGrid>
						<asp:Label id="lblNoRecentReportsMessage" runat="server"></asp:Label></P>
					<P>
						<DIV class="sectionheadingalt">Recently Created Report Templates</DIV>
					<P></P>
					<P>
						<asp:DataGrid id="dgrRecentTemplates" runat="server" CssClass="datagrid" AutoGenerateColumns="False"
							Width="648px">
							<HeaderStyle CssClass="datagridheader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="ReportNameFull" HeaderText="Report Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="TemplateName" HeaderText="Template Name"></asp:BoundColumn>
								<asp:HyperLinkColumn Text="Run Template" DataNavigateUrlField="ReportTemplateID" DataNavigateUrlFormatString="ReportOptions.aspx?ReportTemplateID={0}"
									HeaderText="Run"></asp:HyperLinkColumn>
								<asp:HyperLinkColumn Text="About Template" DataNavigateUrlField="ReportTemplateID" DataNavigateUrlFormatString="ReportInfo.aspx?ReportTemplateID={0}"
									HeaderText="About"></asp:HyperLinkColumn>
							</Columns>
						</asp:DataGrid>
						<asp:Label id="lblNoRecentTemplatesMessage" runat="server"></asp:Label></P>
				</asp:Panel>
				<asp:Panel id="AllReportsTab" CssClass="tcTab" BnbTabName="All Reports" runat="server">
					<P>
						<asp:DataGrid id="dgrAllInteractive" runat="server" CssClass="datagrid" AutoGenerateColumns="False">
							<HeaderStyle CssClass="datagridheader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="ReportType" HeaderText="Report Type"></asp:BoundColumn>
								<asp:BoundColumn DataField="ReportNameFull" HeaderText="Report"></asp:BoundColumn>
								<asp:HyperLinkColumn DataNavigateUrlField="ReportTemplateID" DataNavigateUrlFormatString="ReportOptions.aspx?ReportTemplateID={0}"
									DataTextField="RunText" HeaderText="Run"></asp:HyperLinkColumn>
								<asp:HyperLinkColumn DataNavigateUrlField="ReportTemplateID" DataNavigateUrlFormatString="ReportInfo.aspx?ReportTemplateID={0}"
									DataTextField="AboutText" HeaderText="About"></asp:HyperLinkColumn>
							</Columns>
						</asp:DataGrid></P>
				</asp:Panel>
				<asp:Panel id="StaticReportsTab" CssClass="tcTab" BnbTabName="Static Reports" runat="server">
					<P>
						<asp:DataGrid id="dgrAllStatic" runat="server" CssClass="datagrid" AutoGenerateColumns="False">
							<HeaderStyle CssClass="datagridheader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="ReportType" HeaderText="Report Type"></asp:BoundColumn>
								<asp:BoundColumn DataField="ReportNameFull" HeaderText="Report"></asp:BoundColumn>
								<asp:HyperLinkColumn Text="View" DataNavigateUrlField="XMLPath" DataNavigateUrlFormatString="ReportViewer.aspx?ResultsXML={0}"
									HeaderText="View"></asp:HyperLinkColumn>
							</Columns>
						</asp:DataGrid>
						<asp:Label id="lblNoStaticReportsMessage" runat="server"></asp:Label></P>
				</asp:Panel>
			</div>
		</form>
	</body>
</HTML>
