<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Page Language="c#" AutoEventWireup="True" Codebehind="ReportGroups.aspx.cs" Inherits="Frond.Reports.ReportGroups" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Report Groups</title>
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:Title id="titleBar" runat="server" imagesource="../Images/report24silver.gif"></uc1:Title>
				<ASP:TEXTBOX id="txtHiddenReportIndex" runat="server" visible="False"></ASP:TEXTBOX>
				<P><ASP:LABEL id="lblMissingWarning" runat="server"></ASP:LABEL><ASP:LABEL id="lblReportInfo" runat="server"></ASP:LABEL></P>
				<P>
					<TABLE id="Table1" width="100%" border="0">
						<TR>
							<TD>
								<asp:HyperLink id="hlDetails" runat="server">Details</asp:HyperLink>&nbsp;&nbsp;</TD>
							<TD style="TEXT-ALIGN: right">Download as
								<ASP:BUTTON id="cmdGetCSV" runat="server" text="CSV" onclick="cmdGetCSV_Click"></ASP:BUTTON>
								<ASP:BUTTON id="cmdGetExcel" runat="server" text="Excel" onclick="cmdGetExcel_Click"></ASP:BUTTON>
								<ASP:BUTTON id="cmdGetWord" runat="server" text="Word Doc" onclick="cmdGetWord_Click"></ASP:BUTTON>
								<ASP:CHECKBOX id="chkOutputAllCols" runat="server" text="All Columns"></ASP:CHECKBOX></TD>
						</TR>
					</TABLE>
				</P>
				<P><asp:label id="lblReportGroups" runat="server"></asp:label></P>
			</div>
		</form>
	</body>
</HTML>
