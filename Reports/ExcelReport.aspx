<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExcelReport.aspx.cs" Inherits="Frond.Reports.ExcelReport" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ExcelReport</title>
		<style>
			TABLE.datagrid { WIDTH: 100%; BORDER-COLLAPSE: collapse }
			TH { BORDER-RIGHT: silver 0.25pt solid; BORDER-TOP: silver 0.25pt solid; FONT-WEIGHT: bold; FONT-SIZE: 10pt; BORDER-LEFT: silver 0.25pt solid; BORDER-BOTTOM: silver 0.25pt solid; FONT-FAMILY: Arial }
			TD { BORDER-RIGHT: silver 0.25pt solid; BORDER-TOP: silver 0.25pt solid; FONT-SIZE: 10pt; BORDER-LEFT: silver 0.25pt solid; BORDER-BOTTOM: silver 0.25pt solid; FONT-FAMILY: Arial }
			</style>
	</HEAD>
	<body>
		<bnbdatagrid:bnbdatagridforview id="Bnbdatagridforview2" runat="server" ViewLinksText="View" NewLinkText="New" NewLinkVisible="False"
			ViewLinksVisible="False" ShowResultsOnNewButtonClick="false" DisplayResultCount="false" DisplayNoResultMessageOnly="false"
			Width="100%" CssClass="datagrid" ColumnsToExclude="tblProject_Global, tblGrantProgress_ProposalDate"></bnbdatagrid:bnbdatagridforview>
	</body>
</HTML>
</html>
