//INSTANT C# NOTE: Formerly VB.NET project-level imports:
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
namespace Frond.Reports
{
	public partial class SFMessage : System.Web.UI.Page
	{

	#region  Web Form Designer Generated Code 

		//This call is required by the Web Form Designer.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{

		}

//ORIGINAL LINE: Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles base.Init
		override protected void OnInit(System.EventArgs e)
		{
				base.OnInit(e);
			//CODEGEN: This method call is required by the Web Form Designer
			//Do not modify it using the code editor.
			InitializeComponent();
		}

	#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//Put user code to initialize the page here
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
			if (! Page.IsPostBack)
			{
				ShowMessage();
			}

		}

		// returns absolute path to page
		public static string GetMyURL()
		{
			string strBase = null;
			strBase = HttpContext.Current.Request.ApplicationPath;
			if (strBase == "/")
			{
					strBase = string.Empty;
				}
			return strBase + "/Reports/SFMessage.aspx";
		}



		// returns url with specified ReportTemplate parameter
		public static string GetMyURLWithMessage(string pstrMessage)
		{
			// todo - add call to urlencode, to make the message text safe for url
			return SFMessage.GetMyURL() + "?Message=" + pstrMessage;
		}


		protected void ShowMessage()
		{
			string strMessage = null;

			// get from query string
			strMessage = Request.QueryString["Message"];
			lblMessage.Text = strMessage;

		}

	}

} //end of root namespace