using System;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboEngine.Query;
using BonoboEngine;
using BonoboWebControls.DataGrids;
using BonoboWebControls;



namespace Frond.Reports
{
    public partial class WordReport : System.Web.UI.Page
    {
        BnbWebFormManager bnb = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            BnbQuery query = (BnbQuery)Session["findQuery"];
            BnbQueryBuilder qBuilder = new BnbQueryBuilder(query);
            string userDesc = "Search criteria was " + qBuilder.GenerateCriteriaUserDescription();
            lblTitle.Text = userDesc;
            Response.ContentType = "application/msword";
            if (query == null) Response.End();
            Response.AddHeader("Content-Disposition", "attachment;filename=WordReport.doc");
            BnbDataGridForQueryDataSet1.MetaDataXmlUrl = @"Xml\BonoboFindMetaData.xml";
            BnbDataGridForQueryDataSet1.DomainObjectType = Request.QueryString["domainObjectType"];
            BnbDataGridForQueryDataSet1.QueryDataSet = query.Execute();

        }
        private void BnbDataGridForQueryDataSet1_PreRender(object sender, EventArgs e)
        {
            foreach (BnbDataGridColumn col in BnbDataGridForQueryDataSet1.Columns)
            {
                if (col.GetDataType().Equals(typeof(System.Decimal))) col.FormatExpression = "#,0";
            }
        }
    }
}
