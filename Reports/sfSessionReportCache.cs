// class to hold report renderers for a user session
// only holds a finite stack of them
// designed to generate a new index for each report that is added
// even though there will always be a finite number
//INSTANT C# NOTE: Formerly VB.NET project-level imports:
using System;
using System.Collections;
using System.Runtime.Serialization;
using SF2ReportsEngine;
using System.IO;
using BonoboEngine;




namespace Frond.Reports
{
	[Serializable()]
	public class sfSessionReportCache : ISerializable
	{

		private const string mcstrReportCacheKey = "sfSessionReportCache";

		// Private state variables.
		// If you alter these you must modify the 
		// implementation of ISerializable too
		private int mintReportCounter;
		private Hashtable mhshReportCache;
		private int mintMaxReports;

		public sfSessionReportCache()
		{
			mintReportCounter = 0;
			mhshReportCache = new Hashtable();
			mintMaxReports = 5;
		}

		public sfSessionReportCache(int pintMaxReports) : this()
		{
			mintMaxReports = pintMaxReports;
		}

		// adds a report to the cache
		// if there are too many, drops an old one
		// returns the index of the report that has been added
		public int Add(sfReportRenderer presRender)
		{
			int intNewItemKey = 0;
			// clear cache if it is full
			while (mhshReportCache.Count >= this.MaxReports)
			{
				RemoveLowestKey();
			}

			// add report to cache
			intNewItemKey = mintReportCounter;
			mhshReportCache.Add(intNewItemKey, presRender);


			mintReportCounter += 1;
			return intNewItemKey;

		}



		public int MaxReports
		{
			get
			{
				return mintMaxReports;
			}
		}

		private void RemoveLowestKey()
		{
//INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
//			object objKey = null;
			int intLowest = 0;

			intLowest = -1;
			foreach (object objKey in mhshReportCache.Keys)
			{
				// first time, get the key
				if (intLowest == -1)
				{
					intLowest = System.Convert.ToInt32(objKey);
				}
				else
				{
					// otherwise, compare to get lowest
					if (System.Convert.ToInt32(objKey) < intLowest)
					{
						intLowest = System.Convert.ToInt32(objKey);
					}
				}
			}

			// if lowest found, then remove it
			if (intLowest > -1)
			{
				mhshReportCache.Remove(intLowest);
			}
		}

		// returns the report results object with the specifed repoort index
		// (this will have been returned when the report was originally added)
		public sfReportRenderer this[int pintReportIndex]
		{
			get
			{

				if (mhshReportCache.Contains(pintReportIndex))
				{
					return (sfReportRenderer)(mhshReportCache[pintReportIndex]);
				}
				else
				{
					return null;
				}
			}
		}

	#region  Implementation of ISerializable 
		protected sfSessionReportCache(SerializationInfo info, StreamingContext context)
		{
			mintReportCounter = info.GetInt32("ReportCounter");
			mhshReportCache = (Hashtable)(info.GetValue("ReportCache", typeof(Hashtable)));
			mintMaxReports = info.GetInt32("MaxReports");
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("ReportCounter", mintReportCounter);
			info.AddValue("ReportCache", mhshReportCache, typeof(Hashtable));
			info.AddValue("MaxReports", mintMaxReports);
		}
	#endregion

		
		public static sfSessionReportCache GetReportCache(System.Web.SessionState.HttpSessionState psesSession)
		{
			sfSessionReportCache SessionReportCache = null;
			if (psesSession[mcstrReportCacheKey] == null)
			{
				psesSession[mcstrReportCacheKey] = new sfSessionReportCache();
			}
			SessionReportCache = (sfSessionReportCache)(psesSession[mcstrReportCacheKey]);
			return SessionReportCache;
		}


	}

} //end of root namespace