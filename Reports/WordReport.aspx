<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WordReport.aspx.cs" Inherits="Frond.Reports.WordReport" %>
<%@ Register TagPrefix="bnbdatagrid" Namespace="BonoboWebControls.DataGrids" Assembly="BonoboWebControls" %>

<HTML>
	<HEAD>
		<title>WordReport</title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta name="ProgId" content="Word.Document">
		<style>
			@page Section1 {size: 842.0pt 21.0cm; mso-page-orientation: landscape; margin: 1.0cm 1.0cm 1.0cm 1.0cm; mso-paper-source: 0; }
			TABLE.datagrid { BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 100%; BORDER-BOTTOM: black 1px solid; BORDER-COLLAPSE: collapse }
			TH { PADDING-RIGHT: 5.4pt; PADDING-LEFT: 5.4pt; FONT-WEIGHT: bold; FONT-SIZE: 7pt; PADDING-BOTTOM: 0cm; PADDING-TOP: 0cm; FONT-FAMILY: Arial }
			BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid; BORDER-COLLAPSE: collapse;
			TD { PADDING-RIGHT: 5.4pt; PADDING-LEFT: 5.4pt; FONT-SIZE: 7pt; PADDING-BOTTOM: 0cm; PADDING-TOP: 0cm; FONT-FAMILY: Arial;
			BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid; BORDER-COLLAPSE: collapse; }
			DIV.Section1 { page: Section1 }
			</style>
	</HEAD>
	<body>
		<div class="Section1">
			<BR>
			<asp:Label id="lblTitle" runat="server" Width="176px"></asp:Label>
			<BR>
			<BR>

			<bnbdatagrid:BnbDataGridForQueryDataSet id="BnbDataGridForQueryDataSet1" runat="server" CssClass="datagrid" ViewLinkVisible="False"
				ShowReturnedCountMessage="False" ColumnsToExclude="tblProject_Global"></bnbdatagrid:BnbDataGridForQueryDataSet>
          </div>
</body>
</html>
