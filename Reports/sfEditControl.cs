//
//   Abstract base class for editable controls
//
//INSTANT C# NOTE: Formerly VB.NET project-level imports:
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
namespace Frond.Reports
{
	public abstract class sfEditControl : System.Web.UI.UserControl
	{

		// Internal property variables
		private bool m_CanBeEnabled = true;
		private string m_DataField = string.Empty;

		// Viewstate keys
		private const string ViewStateEnabledKey = "sfEditControl.Enabled";
		private const string ViewStateCanBeEnabledKey = "sfEditControl.CanBeEnabled";

		// Override this to allow subclasses to enable and disable their child controls
		protected internal abstract void EnableChildControls(bool Enabled);

		// Override this to provide a text property that can be read.
		// You must implement a Set accessor, even if it merely throws an exception.
		public abstract string Text {get; set;}

		// Override this to provide a consistent way to set the value of the control
		public abstract void SetValue(object value);

		// Default constructor
		protected sfEditControl()
		{
			m_CanBeEnabled = true;
			try
			{
				if (ViewState[ViewStateCanBeEnabledKey] != null)
				{
					m_CanBeEnabled = System.Convert.ToBoolean(ViewState[ViewStateCanBeEnabledKey]);
				}
			}
			finally
			{
				// Do nothing
			}
		}

		//   Enabled property. Controls whether the contents of the 
		//   control are being edited.
		public virtual bool Enabled
		{
			get
			{
				if (ViewState[ViewStateEnabledKey] == null)
				{
					return false;
				}
				return System.Convert.ToBoolean(ViewState[ViewStateEnabledKey]);
			}
			set
			{
				bool IsEnabled = false;
				if (CanBeEnabled)
				{
					IsEnabled = value;
				}
				ViewState[ViewStateEnabledKey] = IsEnabled;
				EnableChildControls(IsEnabled);
			}
		}

		//   CanBeEnabled property. Controls whether the Enabled property can be set
		public virtual bool CanBeEnabled
		{
			get
			{
				return m_CanBeEnabled;
			}
			set
			{
				m_CanBeEnabled = value;
				ViewState[ViewStateCanBeEnabledKey] = value;
			}
		}

		public string DataField
		{
			get
			{
				return m_DataField;
			}
			set
			{
				m_DataField = value;
			}
		}

	}

} //end of root namespace