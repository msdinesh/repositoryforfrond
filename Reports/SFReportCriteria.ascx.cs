// usercontrol to represent a report criteria as a list
//INSTANT C# NOTE: Formerly VB.NET project-level imports:
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SF2ReportsEngine;

namespace Frond.Reports
{
	public partial  class SFReportCriteria : System.Web.UI.UserControl
	{

	#region  Web Form Designer Generated Code 

		//This call is required by the Web Form Designer.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{

		}

//ORIGINAL LINE: Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles base.Init
		override protected void OnInit(System.EventArgs e)
		{
				base.OnInit(e);
			//CODEGEN: This method call is required by the Web Form Designer
			//Do not modify it using the code editor.
			InitializeComponent();
		}

	#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//VAF-67 configure list in page load rather than
			//when CriteriaDefinitionID property is set
			if (!this.IsPostBack)
				ConfigureList();
		}

		public int CriteriaDefinitionID
		{
			get
			{
				return System.Convert.ToInt32(txtHiddenCriteriaDefinitionID.Text);
			}
			set
			{
				txtHiddenCriteriaDefinitionID.Text = System.Convert.ToString(value);
			}
		}

		public string CriteriaDefinitionIDString
		{
			get
			{
				return this.CriteriaDefinitionID.ToString();
			}
			set
			{
				this.CriteriaDefinitionID = int.Parse(value);
			}
		}

		public string FieldName
		{
			get
			{
				return txtHiddenFieldName.Text;
			}
			set
			{
				txtHiddenFieldName.Text = value;
			}
		}

		public string UserDescription
		{
			get
			{
				return lblCriteriaName.Text;
			}
			set
			{
				lblCriteriaName.Text = value;
			}
		}

		/// <summary>
		/// returns the actual html id that the listbox will be rendered with
		/// (useful for client side javascript)
		/// </summary>
		public string ListBoxClientID
		{
			get
			{
				return lstMain.ClientID;
			}

		}

		// populate the list and label based on the criteriadefinitionid
		private void ConfigureList()
		{
			DataTable dtbElements = null;
			//DataView dtvBind = null;
			//DataTable tblCriteriaInfo = null;
			ListItem litSpecialItem = null;
			bool blnHasExcludeField = false;
			bool blnAddRow = false;
			bool blnItemsSelected = false;

			dtbElements = sfReportRequest.GetCriteriaElements(this.CriteriaDefinitionID);

			// <!-- VAF-67
			// get selected values from parent page
			ReportOptions parentPage = this.Page as ReportOptions;
			ArrayList selectedValues = parentPage.ParsedCriteriaForFieldName(this.FieldName);
			// -->
					

			lstMain.Items.Clear();
			// add 'any' item
			// use -1 instead of 0 (VAF-46)
			litSpecialItem = new ListItem("<Any>", "-1");
			lstMain.Items.Add(litSpecialItem);
			
			// add items from recordset
			blnHasExcludeField = dtbElements.Columns.Contains("Exclude");
//INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
//			DataRow rowLoop = null;
			foreach (DataRow rowLoop in dtbElements.Rows)
			{
				blnAddRow = true;
				if (blnHasExcludeField)
				{
					if (sfRepUtils.SafeNullBoolean(rowLoop["Exclude"]) == true)
					{
						blnAddRow = false;
					}
				}
				if (blnAddRow)
				{
					ListItem newItem = new ListItem( sfRepUtils.SafeNullString( rowLoop["Description"]), sfRepUtils.SafeNullString( rowLoop["ID"]));
					lstMain.Items.Add(newItem);
					// <!-- VAF-67
					// is it selected?
					if (selectedValues.Contains(newItem.Value))
					{
						newItem.Selected = true;
						blnItemsSelected = true;
					}
					// -->
				}
			}

			// if nothing selected, then ensure 'any' option is seelcted
			if (!blnItemsSelected)
				litSpecialItem.Selected = true;

		}

		// returns a datatable with all the ID and Description
		// values of the selected items
		// if <ANY> is selected, returns empty table
		private DataTable GetSelectedItems()
		{
			DataTable dtbReturn = null;
//INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
//			ListItem litLoop = null;
			DataRow dtrNew = null;
			dtbReturn = new DataTable();
			dtbReturn.Columns.Add(new DataColumn("ID"));
			dtbReturn.Columns.Add(new DataColumn("Description"));

			// if any is selected then don't return anything
			// otherwise, get all selected items
			if (lstMain.SelectedIndex > 0)
			{
				foreach (ListItem litLoop in lstMain.Items)
				{

					if (litLoop.Selected)
					{
						dtrNew = dtbReturn.NewRow();
						dtrNew["ID"] = litLoop.Value;
						dtrNew["Description"] = litLoop.Text;
						dtbReturn.Rows.Add(dtrNew);
					}
				}
			}

			return dtbReturn;

		}

		// returns a SQL IN statement corresponding to the selected items
		// if <ANY> is selected, an empty string is returned
		public string GetClauseSQL()
		{
			string strClause = null;
//INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
//			ListItem litLoop = null;


			if (lstMain.SelectedIndex == 0)
			{
				// <ANY> is selected
				strClause = "";
			}
			else
			{
				strClause = "";
				foreach (ListItem litLoop in lstMain.Items)
				{
					if (litLoop.Selected)
					{
						if (strClause.Length > 0)
						{
							strClause += ", ";
						}
						strClause += "'" + litLoop.Value + "'";
					}
				}
				// wrap in statement around values
				strClause = this.FieldName + " IN (" + strClause + ")";
			}

			return strClause;
		}

		// returns user version of SQL statement corresponding to the selected items
		// if <ANY> is selected, an empty string is returned
		public string GetClauseDescription()
		{
			string strClause = null;
//INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
//			ListItem litLoop = null;


			if (lstMain.SelectedIndex == 0)
			{
				// <ANY> is selected
				strClause = "";
			}
			else
			{
				strClause = "";
				foreach (ListItem litLoop in lstMain.Items)
				{
					if (litLoop.Selected)
					{
						if (strClause.Length > 0)
						{
							strClause += " or ";
						}
						strClause += litLoop.Text;
					}
				}
				// wrap in statement around values
				strClause = "(" + this.UserDescription + " = " + strClause + ")";
			}

			return strClause;
		}

		protected SFReportCriteria() : base()
		{
		}

	}

} //end of root namespace