//INSTANT C# NOTE: Formerly VB.NET project-level imports:
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SF2ReportsEngine;
using BonoboEngine;

namespace Frond.Reports
{
	public partial class ReportViewer : System.Web.UI.Page
	{

		private sfReportRenderer mrndRender;
		protected UserControls.Title titleBar;

		protected HttpCookie PersistentPreferences;
		private const int mcintDefaultLinesPerPage = 20;
		private int mintShowRow;
		private bool m_suppressIdentRows = false;

	#region  Web Form Designer Generated Code 

		//This call is required by the Web Form Designer.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{

		}

//ORIGINAL LINE: Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles base.Init
		override protected void OnInit(System.EventArgs e)
		{
				base.OnInit(e);
			//CODEGEN: This method call is required by the Web Form Designer
			//Do not modify it using the code editor.
			InitializeComponent();
		}

	#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// stf-152 - make export downloads work over ssl
			Response.Cache.SetCacheability(HttpCacheability.Public);

			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);

			PersistentPreferences = Request.Cookies["PersistentPreferences"];
			if (PersistentPreferences == null)
			{
					PersistentPreferences = new HttpCookie("PersistentPreferences");
				}
			if (PersistentPreferences != null)
			{
					PersistentPreferences.Expires = DateTime.MaxValue;
				}

			if (! Page.IsPostBack)
			{
				ParseQueryString();
				InitControls();
				ShowReportInfo();
				RenderReportPage();
			}

			ReportExistenceCheck();
			
			//stf-375
			this.ReportRenderer.SuppressIdenticalRows = (chkSuppressIdenticalRows.Checked);

		}

		//This function also present in: ReportOptions.aspx.vb
//ORIGINAL LINE: Protected Property PersistentPreference(ByVal PreferenceName As String) As String
//INSTANT C# WARNING: C# does not support parameterized properties - the following property has been divided into two methods:
		protected string GetPersistentPreference(string PreferenceName)
		{
				string GetValue = null;
				if (PersistentPreferences != null)
				{
					switch (PreferenceName)
					{
						case "Linage":
							GetValue = PersistentPreferences.Values[PreferenceName];
							break;
						default: //unknown property name
							GetValue = "";
							break;
					}
				}
				if (GetValue == "")
				{
					switch (PreferenceName)
					{
						case "Linage":
							GetValue = mcintDefaultLinesPerPage.ToString();
							break;
						default: //unknown property name
							GetValue = "";
							break;
					}
				}
				return GetValue;
		}
			protected void SetPersistentPreference(string PreferenceName, string Value)
			{
					if (PersistentPreferences != null)
					{
						switch (PreferenceName)
						{
							case "Linage":
								if (GetPersistentPreference(PreferenceName) == "")
								{
									PersistentPreferences.Values.Add(PreferenceName, Value);
								}
								else
								{
									PersistentPreferences.Values.Set(PreferenceName, Value);
								}
								break;
							default:
								//unknown property name
							break;
						}
						Response.AppendCookie(PersistentPreferences);
					}
			}

		// returns quasi-absolute path to page
		public static string GetMyURL()
		{
			string strBase = null;
			strBase = HttpContext.Current.Request.ApplicationPath;
			if (strBase == "/")
			{
					strBase = string.Empty;
				}
			return strBase + "/Reports/ReportViewer.aspx";
		}

		public static string GetMyURLWithReportIndex(int pintReportIndex)
		{
			string s = null;
			s = ReportViewer.GetMyURL();
			s += "?ReportIndex=";
			s += pintReportIndex;
			return s;
		}

		public static string GetMyURLWithReportIndexShowRow(int pintReportIndex, int pintShowRow)
		{
			return ReportViewer.GetMyURL() + "?ReportIndex=" + pintReportIndex + "&ShowRow=" + pintShowRow;
		}

		public static string GetMyURLWithResultsXML(string pstrResultsXML)
		{
			return ReportViewer.GetMyURL() + "?ResultsXML=" + pstrResultsXML;
		}

		// gets info out of query string and puts into properties
		private void ParseQueryString()
		{
			string strResultsXMLFile = null;
			string strShowRow = null;

			// if report index specified
			if (Request.QueryString["ReportIndex"] != null && Request.QueryString["ReportIndex"] != "")
			{
				// store it
				this.ReportIndex = System.Convert.ToInt32(Request.QueryString["ReportIndex"]);
			}
			else
			{
				// if xml path specified
				if (Request.QueryString["ResultsXML"] != null && Request.QueryString["ResultsXML"] != "")
				{

					strResultsXMLFile = Request.QueryString["ResultsXML"];
					ShowStaticReport(strResultsXMLFile);
				}
			}

			// if show row specified
			strShowRow = Request.QueryString["ShowRow"];
			if (strShowRow != "")
			{
				mintShowRow = System.Convert.ToInt32(strShowRow);
			}
			else
			{
				mintShowRow = 0;
			}

		}

		// if running from a report in the session report cache,
		// this returns the index of the report
		public int ReportIndex
		{
			get
			{
				if (txtHiddenReportIndex.Text == "")
				{
					return -1;
				}
				else
				{
					return System.Convert.ToInt32(txtHiddenReportIndex.Text);
				}
			}
			set
			{
				txtHiddenReportIndex.Text = System.Convert.ToString(value);
			}
		}

		private sfReportRenderer ReportRenderer
		{
			get
			{
				// if not got ref yet
				if (mrndRender == null)
				{
					// if we have an index
					if (this.ReportIndex > -1)
					{
						// try to get report from session cache
						mrndRender = sfSessionReportCache.GetReportCache(Session)[this.ReportIndex];
					}
					else
					{

					}
				}
				// return whatever reference we now have
				return mrndRender;

			}
		}

		private bool SuppressIdenticalRows 
		{
			get { return m_suppressIdentRows; }
			set { m_suppressIdentRows = value; }
		}

		private sfReportResults ReportResults
		{
			get
			{
				return this.ReportRenderer.ReportResults;
			}
		}

		// returns true if a report is loaded
		private bool HasReportResults
		{
			get
			{
				if (this.ReportRenderer == null)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
		}

		// checks if report exists and gives messages if it doesn't
		private void ReportExistenceCheck()
		{
			// if no report
			if (this.HasReportResults == false)
			{

				// if a report index was specified
				if (this.ReportIndex > -1)
				{
					// give suitable warning
					lblMissingWarning.Text = "The report that you ran is no longer cached at the Server. You will have to run it again.";
				}
			}
			else
			{
				lblMissingWarning.Text = "";
			}
		}

		private void InitControls()
		{
			
			int cookieLinesPerPage = System.Convert.ToInt32(GetPersistentPreference("Linage"));
			if (cookieLinesPerPage < 2)
				cookieLinesPerPage = mcintDefaultLinesPerPage;

			// renderer can override lines per page
			if (this.ReportRenderer.ShowAllRowsOnFirstPage)
			{
				this.OverrideLinesPerPage(this.ReportResults.RowCount + 20);
			}
			else
				this.LinesPerPage = cookieLinesPerPage;

			if (mintShowRow == 0)
			{
				this.CurrentPage = this.FirstPage;
			}
			else
			{
//INSTANT C# NOTE: The VB integer division operator \ was replaced 1 time(s) by the regular division operator /
				this.CurrentPage = 1 + ((int)System.Math.Floor((decimal)(mintShowRow / this.LinesPerPage)));
			}
		}

		// number of report lines to show per page
		private int LinesPerPage
		{
			get
			{
				if (txtHiddenLinesPerPage.Text == "")
				{
					txtHiddenLinesPerPage.Text = System.Convert.ToString(mcintDefaultLinesPerPage);
					txtUserLinesPerPage.Text = System.Convert.ToString(mcintDefaultLinesPerPage);
				}
				int lines = System.Convert.ToInt32(txtHiddenLinesPerPage.Text);
				return lines;
			}
			set
			{
				txtHiddenLinesPerPage.Text = System.Convert.ToString(value);
				txtUserLinesPerPage.Text = System.Convert.ToString(value);
				SetPersistentPreference("Linage", System.Convert.ToString(value));
			}
		}

		/// <summary>
		/// Call this to set the lines per page without affecting the user setting
		/// saved in the cookie
		/// </summary>
		/// <param name="overrideValue"></param>
		private void OverrideLinesPerPage(int overrideValue)
		{
			txtHiddenLinesPerPage.Text = System.Convert.ToString(overrideValue );
			txtUserLinesPerPage.Text = System.Convert.ToString(overrideValue);
		
		}

		// shows info about the report run on the page
		private void ShowReportInfo()
		{
			string strInfo = null;
			sfReportRequest reqRequestInfo = null;

			reqRequestInfo = this.ReportResults.Request;

			titleBar.TitleText = "Report Viewer - " + reqRequestInfo.ReportNameFull;
			strInfo += "Total Rows: " + this.ReportResults.RowCount + "   Run at " + this.ReportResults.DataTimestamp.ToString("dd-MMM-yyyy HH:mm:ss") + "<br>";
			strInfo += "Date Option: " + reqRequestInfo.DateDescriptionComplete + "<br>";
			strInfo += "Group By: " + reqRequestInfo.GroupName + " Sort By: " + reqRequestInfo.SortName + "<br>";
			strInfo += "User Criteria: " + reqRequestInfo.UserCriteriaDescription + "<br>";
			strInfo += "Default Criteria: " + reqRequestInfo.DefaultCriteriaDescription;

			lblReportInfo.Text = strInfo;

			hlContents.NavigateUrl = ReportGroups.GetMyURLWithReportIndex(this.ReportIndex);
			hlReportOptions.NavigateUrl = ReportOptions.GetMyURLWithReportIndex(this.ReportIndex);

		}

		private int CurrentPage
		{
			get
			{
				if (txtHiddenPage.Text == "")
				{
					txtHiddenPage.Text = System.Convert.ToString(this.FirstPage);
				}
				return System.Convert.ToInt32(txtHiddenPage.Text);
			}
			set
			{
				txtHiddenPage.Text = System.Convert.ToString(value);
			}
		}

		private int FirstPage
		{
			get
			{
				// always 1!
				return 1;
			}
		}

		private int LastPage
		{
			get
			{
				int intPages = 0;
				if (this.HasReportResults)
				{
					intPages = 0;
					if (this.LinesPerPage > 0)
						intPages = (int)System.Math.Floor((decimal)(this.ReportResults.RowCount / this.LinesPerPage));
					// if there is any remainder, add a page
					if ((this.ReportResults.RowCount % this.LinesPerPage) > 0)
					{
						intPages += 1;
					}
					return intPages;
				}
				//INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
				return 0;
			}
		}

		// returns the first row to be rendered on the page
		// note that first row in data is row 0
		private int FirstRowOnPage
		{
			get
			{
				return ((this.CurrentPage - 1) * this.LinesPerPage);
			}
		}

		// returns the last row to be rendered on the page
		// note that first row in data is row 0
		private int LastRowOnPage
		{
			get
			{
				int intReturn = 0;
				if (this.HasReportResults)
				{
					intReturn = this.FirstRowOnPage + this.LinesPerPage - 1;
					if (intReturn > (this.ReportResults.RowCount - 1))
					{
						intReturn = this.ReportResults.RowCount - 1;
					}
				}
				return intReturn;
			}
		}

		// draws the appropriate page of the report on the screen
		private void RenderReportPage()
		{

			if (this.HasReportResults)
			{

				lblPageInfo.Text = "Page " + this.CurrentPage + " of " + this.LastPage + " (Rows " + (this.FirstRowOnPage + 1) + " to " + (this.LastRowOnPage + 1) + ")";

				lblReportOutput.Text = this.ReportRenderer.RenderAsHTML(this.FirstRowOnPage, this.LastRowOnPage);

			}
		}

		protected void cmdRefreshLinesPerPage_Click(object sender, System.EventArgs e)
		{
			int intFirstRowOnPage = 0;
			// user has changed lines per page
			// get current first row
			intFirstRowOnPage = this.FirstRowOnPage;
			// update lines per page, if user value is ok
			if (sfRepUtils.IsNumeric(txtUserLinesPerPage.Text))
			{
				if (System.Convert.ToInt32(txtUserLinesPerPage.Text) > 0)
				{
					this.LinesPerPage = System.Convert.ToInt32(txtUserLinesPerPage.Text);
				}
			}
			// reset user to match hidden value
			txtUserLinesPerPage.Text = System.Convert.ToString(this.LinesPerPage);

			// adjust page so that first row of old page is still
			// on current page
//INSTANT C# NOTE: The VB integer division operator \ was replaced 1 time(s) by the regular division operator /
			this.CurrentPage = 1 + ((int)System.Math.Floor((decimal)(intFirstRowOnPage / this.LinesPerPage)));

			// ensure current page valid, incase anything goes wrong
			if (this.CurrentPage < this.FirstPage)
			{
				this.CurrentPage = this.FirstPage;
			}
			if (this.CurrentPage > this.LastPage)
			{
				this.CurrentPage = this.LastPage;
			}

			RenderReportPage();
			
		}

		protected void cmdFirst_Click(object sender, System.EventArgs e)
		{
			this.CurrentPage = this.FirstPage;
			RenderReportPage();
			
		}

		protected void cmdPrevious_Click(object sender, System.EventArgs e)
		{
			if (this.CurrentPage > this.FirstPage)
			{
				this.CurrentPage -= 1;
				RenderReportPage();
				
			}
		}

		protected void cmdNext_Click(object sender, System.EventArgs e)
		{
			if (this.CurrentPage < this.LastPage)
			{
				this.CurrentPage += 1;
				RenderReportPage();
				
			}
		}

		protected void cmdLast_Click(object sender, System.EventArgs e)
		{
			this.CurrentPage = this.LastPage;
			RenderReportPage();
			
		}

		protected void cmdGetCSV_Click(object sender, System.EventArgs e)
		{
			
			ReturnCSVFile();
		}

		private void ReturnCSVFile()
		{

			SetResponseHeaders(this.ReportResults.SuggestedFilename + ".csv", false);
			this.ReportRenderer.WriteCsvReport(Response.OutputStream, chkOutputAllCols.Checked);
			Response.End();

		}

		// Sets the headers for a returned report
		// so that it will be downloaded to the user
		// If pblnExcel is set to true, uses specific Excel contenttype
		private void SetResponseHeaders(string pstrFileName, bool pblnExcelFile)
		{

			// incorporate the filename into the response object

			Response.Clear();
			Response.AppendHeader("Content-Disposition", "attachment;filename=" + pstrFileName + ";");
			// this helps browser show progress etc
			// Response.AddHeader("Content-Length", pfilFileInfo.Length.ToString())

			// added by ian to try and get around ssl caching problem
			Response.AppendHeader("Expires", System.DateTime.Now.AddHours(1).ToString());
			Response.AppendHeader("Cache-Control", "store, cache");
			Response.AppendHeader("Pragma", "cache");



			if (pblnExcelFile)
			{
				Response.ContentType = "application/vnd.ms-excel";
			}
			else
			{
				Response.ContentType = "application/octet-stream";
			}

		}

		private void ReturnExcelFile()
		{
			// make the file
			SetResponseHeaders(this.ReportResults.SuggestedFilename + ".xls", true);
			this.ReportRenderer.WriteExcelReport(Response.OutputStream, chkOutputAllCols.Checked);
			Response.End();

		}

		private void ReturnWordFile()
		{

			// make the file
			// 9500 width for portrait, 15300 for landscape
			// (put in XML file later)
			// ignore all columns checkbox
			SetResponseHeaders(this.ReportResults.SuggestedFilename + ".rtf", false);
			this.ReportRenderer.WriteRtfReport(Response.OutputStream, false);
			Response.End();

		}

		protected void cmdGetExcel_Click(object sender, System.EventArgs e)
		{
			ReturnExcelFile();
		}

		protected void cmdGetWord_Click(object sender, System.EventArgs e)
		{

			ReturnWordFile();
		}

		private void ShowStaticReport(string strResultsXMLFile)
		{
			sfReportResults resResults = null;
			sfReportRenderer rndRender = null;
			int intReportIndex = 0;

			// try to make results from xml file
			string sessionID = BnbEngine.SessionManager.GetBonoboSessionID().ToString();
			resResults = new sfReportResults(sessionID, Server.MapPath(strResultsXMLFile));

			// get renderer
			rndRender = sfRenderBroker.GetRenderer(resResults);

			// set to ignore application tag VAF-201
			rndRender.IgnoreApplicationTag = true;

			// add to cache and get index
			intReportIndex = sfSessionReportCache.GetReportCache(Session).Add(rndRender);

			// page must remember it
			// (this will allow the page to pull the renderer back out of report cache)
			this.ReportIndex = intReportIndex;

		}

	}

} //end of root namespace