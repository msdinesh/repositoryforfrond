<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Page Trace="false" AspCompat="false" Language="c#" AutoEventWireup="True" Codebehind="ReportViewer.aspx.cs" Inherits="Frond.Reports.ReportViewer" smartNavigation="True" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<TITLE>Report Viewer</TITLE>
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<META content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<META content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<BODY ms_positioning="FlowLayout">
		<FORM id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
						<uc1:Title id="titleBar" runat="server" imagesource="../Images/report24silver.gif"></uc1:Title>
						<ASP:TEXTBOX id="txtHiddenReportIndex" runat="server" visible="False"></ASP:TEXTBOX><ASP:TEXTBOX id="txtHiddenPage" runat="server" visible="False"></ASP:TEXTBOX>
						<asp:TextBox id="txtHiddenLinesPerPage" runat="server" Visible="False"></asp:TextBox>
						<p>
							<ASP:LABEL id="lblMissingWarning" runat="server"></ASP:LABEL><ASP:LABEL id="lblReportInfo" runat="server"></ASP:LABEL></p>
						<P>
							<TABLE id="Table1" border="0" width="100%">
								<TR>
									<TD>
										<asp:HyperLink id="hlContents" runat="server">Contents</asp:HyperLink>&nbsp;&nbsp;
										<asp:HyperLink id="hlReportOptions" runat="server">Change Report</asp:HyperLink></TD>
									<TD style="TEXT-ALIGN:right">
										<nobr>
											<ASP:CHECKBOX id="chkOutputAllCols" runat="server" text="All Columns"></ASP:CHECKBOX>&nbsp;
											<asp:CheckBox id="chkSuppressIdenticalRows" runat="server" Text="Do not output identical rows"></asp:CheckBox></nobr><br>
										<nobr>Download as
											<ASP:BUTTON id="cmdGetCSV" runat="server" text="CSV" onclick="cmdGetCSV_Click"></ASP:BUTTON>
											<ASP:BUTTON id="cmdGetExcel" runat="server" text="Excel" onclick="cmdGetExcel_Click"></ASP:BUTTON>
											<ASP:BUTTON id="cmdGetWord" runat="server" text="Word Doc" onclick="cmdGetWord_Click"></ASP:BUTTON></nobr></TD>
								</TR>
								<TR>
									<TD>
										<ASP:BUTTON id="cmdFirst" runat="server" text="<<" onclick="cmdFirst_Click"></ASP:BUTTON>
										<ASP:BUTTON id="cmdPrevious" runat="server" text=" < " onclick="cmdPrevious_Click"></ASP:BUTTON>&nbsp;
										<ASP:LABEL id="lblPageInfo" runat="server"></ASP:LABEL>&nbsp;
										<ASP:BUTTON id="cmdNext" runat="server" text=" > " onclick="cmdNext_Click"></ASP:BUTTON>
										<ASP:BUTTON id="cmdLast" runat="server" text=">>" onclick="cmdLast_Click"></ASP:BUTTON>&nbsp;</TD>
									<TD style="TEXT-ALIGN:right">&nbsp; Lines per page:
										<ASP:TEXTBOX id="txtUserLinesPerPage" runat="server" Width="43px"></ASP:TEXTBOX>
										<ASP:BUTTON id="cmdRefreshLinesPerPage" runat="server" text="Update" onclick="cmdRefreshLinesPerPage_Click"></ASP:BUTTON></TD>
								</TR>
							</TABLE>
						</P>
						<P><ASP:LABEL id="lblReportOutput" runat="server"></ASP:LABEL></P>
						<P>&nbsp;</P>
			</div>
		</FORM>
	</BODY>
</HTML>
