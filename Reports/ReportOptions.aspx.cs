//INSTANT C# NOTE: Formerly VB.NET project-level imports:
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;
using System.Globalization;
using SF2ReportsEngine;
using BonoboEngine;
using BonoboEngine.Query;
using System.Web.Mail;
using System.Diagnostics;
using System.IO;

namespace Frond.Reports
{
	public partial class ReportOptions : System.Web.UI.Page
	{
		#region controls



		protected DataRow[] mdtrAllowCriteria;
		protected DataRow mdtrReportChooser;
		protected const string mcstrTemplateTextCurrentUser = "Show All Templates";
		protected const string mcstrTemplateTextAllUsers = "Show My Templates";

		protected const string DateOptionScriptBlockName = "DateOptionScript";

		protected HttpCookie PersistentPreferences;
		private const string mcstrUninitializedPersonalEmail = "your.name@vsoint.org";
		protected UserControls.Title titleBar;

		// <!-- VAF-67: extra page level members to support re-display of reports
		private bool m_UserCriteriaParsed = false;
		private DataTable m_ParsedCriteriaTable = null;
		private ArrayList m_allowedFieldNames = new ArrayList();

		#endregion
		// -->
	#region  Web Form Designer Generated Code 

		//This call is required by the Web Form Designer.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.PreRender += new System.EventHandler(this.ReportOptions_PreRender);

		}

//ORIGINAL LINE: Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles base.Init
		override protected void OnInit(System.EventArgs e)
		{
				base.OnInit(e);
			//CODEGEN: This method call is required by the Web Form Designer
			//Do not modify it using the code editor.
			InitializeComponent();
		}

	#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//Put user code to initialize the page here

			if (! Page.IsPostBack)
			{
				LoadRequestAndDisplay();
				txtReportSentAddress.Text = BnbEngine.SessionManager.GetSessionInfo().EmailAddress;
			}

			if (! (IsClientScriptBlockRegistered(DateOptionScriptBlockName)))
			{
				RegisterClientScriptBlock(DateOptionScriptBlockName, ClientSideDynamicScript);
			}
			// register scripts to hide and show body during load
			RegisterClientScriptBlock("HideBody","<style type=\"text/css\"> BODY { DISPLAY: none; CURSOR: wait } </style>");
			RegisterStartupScript("ShowBody","<style type=\"text/css\"> BODY { DISPLAY: block; CURSOR: default } </style>");

			cmdRunReport.Attributes.Add("onclick","javascript:showLoadingMessage('Running Report ...');");

			uxDateOption.Attributes.Add("onChange", "dateOptionChanged(this)");
			uxGroupOptionCombo.Attributes.Add("onChange","groupSortChanged()");
			uxSortOptionCombo.Attributes.Add("onChange","groupSortChanged()");
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);
			
		}

	
		// returns quasi-absolute path to page
		public static string GetMyURL()
		{
			string strBase = null;
			strBase = HttpContext.Current.Request.ApplicationPath;
			//<!-- Inserted by Napoleon; no swivl call ref
			//Added this line of code so this function works regardless which virtual directory Starfish is installed in
			if (strBase == "/")
			{
					strBase = string.Empty;
				}
			//-->
			return strBase + "/Reports/ReportOptions.aspx";
		}


		// returns url with specified ReportTemplate parameter
		public static string GetMyURLWithReportTemplate(string pstrReportTemplateID)
		{
			return ReportOptions.GetMyURL() + "?ReportTemplateID=" + sfRepUtils.TrimBrackets(pstrReportTemplateID);
		}

		// <!-- VAF-67
		// returns url with specified report index paramater
		public static string GetMyURLWithReportIndex(int pintReportIndex)
		{
			return ReportOptions.GetMyURL() + "?ReportIndex=" + pintReportIndex.ToString();
		}
		// -->


		// uses instructions in querystring to load a request
		// then displays the details
		private void LoadRequestAndDisplay()
		{
			// doc type combo
			cboSendDocType.Items.Add(new ListItem("Excel","xls"));
			cboSendDocType.Items.Add(new ListItem("Word","rtf"));


			string strReportTemplateID = null;
			sfReportRequest reqRequest = null;

			// get report template from query string
			strReportTemplateID = Request.QueryString["ReportTemplateID"];

			// if we got one, load it
			if (strReportTemplateID != null && strReportTemplateID != "")
			{
				string sessionID = BnbEngine.SessionManager.GetBonoboSessionID().ToString();
				reqRequest = new sfReportRequest(sessionID);
				reqRequest.LoadFromReportTemplate(strReportTemplateID);

				this.ReportID = reqRequest.ReportID;
				this.ReportTemplateID = strReportTemplateID;
				this.DateOptionID = Convert.ToString(reqRequest.DateOptionID);
				InitControls();
				// and display it
				this.DisplayRequest(reqRequest);
			}
			else
			{
				// <!-- VAF-67
				// otherwise, look for report index
				// (up to five recently run reports are cached in the sfSessionReportCache object in
				//  the session collection. ReportIndex is used to retrieve the cached info)
				if (Request.QueryString["ReportIndex"] != null && Request.QueryString["ReportIndex"] != "")
				{
					// pull the request out of the report cache
					int reportIndex = System.Convert.ToInt32(Request.QueryString["ReportIndex"]);
					sfReportRenderer tempRender = sfSessionReportCache.GetReportCache(Session)[reportIndex];
					// if not found, might be due to session expiry emptying the cache
					if (tempRender == null)
					{
						// cant retrieve the report options, re-direct to report chooser instead
						Response.Redirect("ReportChooser.aspx");
						return;
					}


					reqRequest = tempRender.ReportRequest;

					this.ReportID = reqRequest.ReportID;
					this.ReportTemplateID = reqRequest.ReportTemplateID;
					this.DateOptionID = Convert.ToString(reqRequest.DateOptionID);
					InitControls();
					// and display it
					this.DisplayRequest(reqRequest);
					 
				}
				// VAF-67 -->
			}

		}

		// populate the combos etc based on the report ID
		private void InitControls()
		{
			DataSet dtsReportOptions = null;
			DataTable tblReportInfo = null;
			DataTable tblDateOption = null;
			DataTable tblGroupOption = null;
			DataTable tblSortOption = null;
			DataTable dtbAllCriteria = null;
			DataTable dtbShowCriteria = null;
			DataRow dtrNewShow = null;


			dtsReportOptions = sfReportRequest.GetReportOptions(this.ReportID);
			tblReportInfo = dtsReportOptions.Tables["Report"];

			// load date options into combo
			tblDateOption = dtsReportOptions.Tables["DateOptions"];
			uxDateOption.DataSource = tblDateOption;
			uxDateOption.DataValueField = "DateOptionID";
			uxDateOption.DataTextField = "DateOptionName";
			uxDateOption.DataBind();

			RefreshDateControls();

			// load group options into combo
			tblGroupOption = dtsReportOptions.Tables["GroupOptions"];
			uxGroupOptionCombo.DataSource = tblGroupOption;
			uxGroupOptionCombo.DataValueField = "GroupOptionID";
			uxGroupOptionCombo.DataTextField = "GroupName";
			uxGroupOptionCombo.DataBind();

			// load sort options into combo
			tblSortOption = dtsReportOptions.Tables["SortOptions"];
			uxSortOptionCombo.DataSource = tblSortOption;
			uxSortOptionCombo.DataValueField = "SortOptionID";
			uxSortOptionCombo.DataTextField = "SortName";
			uxSortOptionCombo.DataBind();

			// get criteria options for report as data table
			dtbAllCriteria = dtsReportOptions.Tables["CriteriaOptions"];
			// make a table for the criteria we actually want to show
			dtbShowCriteria = new DataTable();
			dtbShowCriteria.Columns.Add(new DataColumn("CriteriaDefinitionID"));
			dtbShowCriteria.Columns.Add(new DataColumn("FieldName"));
			dtbShowCriteria.Columns.Add(new DataColumn("UserDescription"));

			// copy rows to ShowCriteria if CriteriaDefinitionId is allowed
			foreach (DataRow dtrLoop in dtbAllCriteria.Rows)
			{
				if (this.IsCriteriaDefinitionIDAllowed(System.Convert.ToInt32(dtrLoop["CriteriaDefinitionID"])))
				{
					// copy the row
					dtrNewShow = dtbShowCriteria.NewRow();
					dtrNewShow["CriteriaDefinitionID"] = dtrLoop["CriteriaDefinitionID"];
					dtrNewShow["FieldName"] = dtrLoop["FieldName"];
					dtrNewShow["UserDescription"] = dtrLoop["UserDescription"];
					dtbShowCriteria.Rows.Add(dtrNewShow);

					// <!-- VAf-67
					// also keep field name in array for use when parsing user filter later
					m_allowedFieldNames.Add(dtrLoop["FieldName"]);
					// -->

				}
				else
				{
					// leave it
				}
			}

			// bind it to the repeater
			repUserCriteria.DataSource = dtbShowCriteria;
			repUserCriteria.DataBind();

			// show templates
			RefreshTemplates();

		}

		// initialises the controls on the screen using the specified request object
		private void DisplayRequest(sfReportRequest preqRequest)
		{
			//DataTable dtbAllowCriteria = null;

			titleBar.TitleText = preqRequest.ReportNameFull;

			this.DateOptionID = preqRequest.DateOptionID.ToString();
			//sfUtils.SetComboSelection(cboDateOption, preqRequest.DateOptionID)
			uxStartDate.Date = preqRequest.StartDate;
			uxEndDate.Date  = preqRequest.EndDate;
			RefreshDateControls();

			// <!-- VAF-67
			// deal with criteria
			this.ParseUserCriteria(preqRequest.UserCriteriaSQL);

			// if the table returned is null
			// that means criteria cannot be edited
			// (because they dont fit the limited capability of the ReportOptions form)
			if (this.ParsedCriteriaTable == null)
			{

				if (preqRequest.UserCriteriaDescription == "")
				{
					uxUserCriteriaDescription.Text = "No user description available yet.  SQL is " + preqRequest.UserCriteriaSQL;
				}
				else
				{
					uxUserCriteriaDescription.Text = preqRequest.UserCriteriaDescription;
				}
				uxUserCriteriaDescription2.Text = uxUserCriteriaDescription.Text;
				this.UserCriteriaSQL = preqRequest.UserCriteriaSQL;

				// add code here later to disable criteria
				this.AllowCriteriaEdit = false;
			}
			else
			{
				// they can be displayed
				// this will happen by the binding of the SFReportCriteria objects
				this.AllowCriteriaEdit = true;
				// page-pre-render will refresh the criteria description
				
			}
			// -->

			// if template has default criteria
			if (preqRequest.DefaultCriteriaSQL != "")
			{

				if (preqRequest.DefaultCriteriaDescription == "")
				{
					uxDefaultCriteriaDescription.Text = "No user description available yet.  SQL is " + preqRequest.DefaultCriteriaSQL;
				}
				else
				{
					uxDefaultCriteriaDescription.Text = preqRequest.DefaultCriteriaDescription;
				}
				this.DefaultCriteriaSQL = preqRequest.DefaultCriteriaSQL;

				// if not fixed, then show remove button
				cmdRemoveDefault.Visible = ! preqRequest.FixedDefaultCriteria;

			}
			else
			{
				this.DefaultCriteriaSQL = "";
				uxDefaultCriteriaDescription.Text = "None";
				cmdRemoveDefault.Visible = false;
			}
			uxDefaultCriteriaDescription2.Text = uxDefaultCriteriaDescription.Text;

			ReportOptions.SetComboSelection(uxGroupOptionCombo, preqRequest.GroupOptionID.ToString());
			ReportOptions.SetComboSelection(uxSortOptionCombo, preqRequest.SortOptionID.ToString());


		}

		/// <summary>
		/// compares the specified criteria to the lsit of allowed field names to 
		/// see if it can be edited
		/// </summary>
		/// <param name="userCriteriaSQL"></param>
		// VAF-67
		private void ParseUserCriteria(string userCriteriaSQL)
		{
			// get a list of allowed fields
			// this should have been setup in call to InitControls earlier
			string[] allowed = m_allowedFieldNames.ToArray(typeof(string)) as string[];


			m_ParsedCriteriaTable = BonoboEngine.Query.BnbLegacyReport.ParseCriteriaString(userCriteriaSQL, allowed);
			m_UserCriteriaParsed = true;
		}

		/// <summary>
		/// Returns true if user criteria have been parsed
		/// </summary>
		// VAF-67
		public bool UserCriteriaParsed
		{
			get{return m_UserCriteriaParsed;}
		}

	
		/// <summary>
		/// Returns the table of parsed criteria, if they are valid for this report
		/// otherwise returns null
		/// </summary>
		// VAF-67
		public DataTable ParsedCriteriaTable
		{
			get{return m_ParsedCriteriaTable;}
		}

		/// <summary>
		/// Returns an arraylist of selected usercriteria values for the specified
		/// fieldname
		/// </summary>
		/// <param name="fieldName"></param>
		/// <returns></returns>
		// VAF-67
		public ArrayList ParsedCriteriaForFieldName(string fieldName)
		{
			ArrayList selectedValues = new ArrayList();
			// table may be null if criteria were no parse-able
			if (m_ParsedCriteriaTable != null)
			{
				DataRow[] matchRows = m_ParsedCriteriaTable.Select("FieldName = '" + fieldName + "'");
				foreach(DataRow rowLoop in matchRows)
					selectedValues.Add(rowLoop["Value"]);
			}
			return selectedValues;
		}

		private int ReportID
		{
			get
			{
				return System.Convert.ToInt32(txtHiddenReportID.Text);
			}
			set
			{
				txtHiddenReportID.Text = System.Convert.ToString(value);
			}
		}

		// this may be set if a report template was loaded
		private string ReportTemplateID
		{
			get
			{
				return txtHiddenReportTemplateID.Text;
			}
			set
			{
				txtHiddenReportTemplateID.Text = value;
			}
		}

		private string DateOptionID
		{
			get
			{
				return txtHiddenDateOptionID.Text;
			}
			set
			{
				txtHiddenDateOptionID.Text = value;
			}
		}

		// TODO: Set visibility according to value of cboDateOption
		private void RefreshDateControls()
		{
			int intDateOptionId = 0;
			sfReportRequest reqTemp = null;
			// which option is selected
			intDateOptionId = (uxDateOption.SelectedItem == null) ? 0 : System.Convert.ToInt32(uxDateOption.SelectedItem.Value);

			string sessionID = BnbEngine.SessionManager.GetBonoboSessionID().ToString();
			reqTemp = new sfReportRequest(sessionID);
			reqTemp.ReportID = this.ReportID;
			reqTemp.DateOptionID = intDateOptionId;

			// default date scheme depends on XML file
			string dateDefaultScheme = "";
			if (this.ReportChooserRow["DateDefault"] != DBNull.Value)
				dateDefaultScheme = this.ReportChooserRow["DateDefault"].ToString();

			// if the date option is a date range 
			// default both dates based on dateDefaultScheme
			if (reqTemp.DateOptionType == 2)
			{
				// based on default date schemas
				switch(dateDefaultScheme)
				{
					case("SixMonthsFromNextMonthStart"):
						// special default dates
						if (uxStartDate.IsEmpty)
							uxStartDate.Date = this.NextMonthStart();
						if (uxEndDate.IsEmpty)
							uxEndDate.Date = this.NextMonthStart().AddMonths(6);
						break;

					case("LastCompleteMonth"):
						// special default dates
						if (uxStartDate.IsEmpty)
							uxStartDate.Date = this.ThisMonthStart().AddMonths(-1);
						if (uxEndDate.IsEmpty)
							uxEndDate.Date = this.ThisMonthStart().AddDays(-1);
						break;

					default:
						// normal default dates
						if (uxStartDate.IsEmpty)
							uxStartDate.Date = System.DateTime.Today;
						if (uxEndDate.IsEmpty)
							uxEndDate.Date = System.DateTime.Today.AddMonths(3);
						break;
				}
				
			}
			// if the date option is a single date
			// default to today
			if (reqTemp.DateOptionType == 1)
			{
				// normal default dates
				if (uxStartDate.IsEmpty)
					uxStartDate.Date =  System.DateTime.Today;
			}
			
			// get date description from request
			reqTemp.StartDate = uxStartDate.Date;
			reqTemp.EndDate = uxEndDate.Date;
			uxDateDescription.Text = reqTemp.DateDescriptionComplete;

			ReportOptions.SetComboSelection(uxDateOption, this.DateOptionID);

		}

		/// <summary>
		/// If today is first of month, returns today.
		/// Otherwise returns first of next month
		/// </summary>
		/// <returns></returns>
		private DateTime NextMonthStart()
		{
			DateTime startOfMonth =  DateTime.Today;
			// if not first of month then move to 1st of next month
			if (startOfMonth.Day != 1)
			{
				startOfMonth = startOfMonth.AddMonths(1);
				startOfMonth = new DateTime( startOfMonth.Year, startOfMonth.Month, 1);
			}
			return startOfMonth;
		}

		/// <summary>
		/// Returns the first day of current month
		/// </summary>
		/// <returns></returns>
		private DateTime ThisMonthStart()
		{
			DateTime startOfMonth = new DateTime( DateTime.Today.Year, DateTime.Today.Month, 1);
			return startOfMonth;
		}

		protected void cboDateOption_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.DateOptionID = uxDateOption.SelectedItem.Value;
			RefreshDateControls();
		}

		private void cmdRefreshDateDescription_Click(object sender, System.EventArgs e)
		{
			RefreshDateControls();
			RefreshUserCriteriaDescription();
		}

		protected void cmdRunReport_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}

		// returns the report request object corresponding to 
		// the currently selected items on the page
		protected sfReportRequest GetUserReportRequest()
		{

			sfReportRequest reqRequest = null;
			sfReportFilter ReportCriteria = new sfReportFilter();

			// work out criteria
			if (this.AllowCriteriaEdit)
			{
				ReportCriteria = ExtractCriteriaFromRepeater();
			}
			else
			{
				ReportCriteria = new sfReportFilter(this.UserCriteriaSQL, uxUserCriteriaDescription.Text);
			}

			// sort out the request
			string sessionID = BnbEngine.SessionManager.GetBonoboSessionID().ToString();
			reqRequest = new sfReportRequest(sessionID);
			reqRequest.ReportID = this.ReportID;
			reqRequest.DateOptionID = System.Convert.ToInt32(uxDateOption.SelectedItem.Value);
			reqRequest.StartDate = uxStartDate.Date;
			reqRequest.EndDate = uxEndDate.Date;
			reqRequest.GroupOptionID = System.Convert.ToInt32(uxGroupOptionCombo.SelectedItem.Value);
			reqRequest.SortOptionID = System.Convert.ToInt32(uxSortOptionCombo.SelectedItem.Value);
			reqRequest.UserCriteriaSQL = ReportCriteria.Filter;
			reqRequest.UserCriteriaDescription = ReportCriteria.Description;
			reqRequest.DefaultCriteriaSQL = this.DefaultCriteriaSQL;
			reqRequest.DefaultCriteriaDescription = uxDefaultCriteriaDescription.Text;

			return reqRequest;

		}


		// runs the report as specified by user selections on page
		// then loads the viewer page
		protected void RunReport()
		{
			lblError.Text = "";
			sfReportRequest reqRequest = null;
			sfReportGenerator repGenerator = null;
			sfReportResults resResults = null;
			sfReportRenderer rndRender = null;
			int intReportIndex = 0;

			// Validator shows red dots but does not prevent postback.
			// manual check here on dates:
			if (!this.CheckIfUserDatesValid())
			{
				lblError.Text = "Invalid date options. Please check your date options and correct.<br/>";
				return;
			}

			// get the request
			reqRequest = GetUserReportRequest();

			// get the results
			string sessionID = BnbEngine.SessionManager.GetBonoboSessionID().ToString();
			repGenerator = new sfReportGenerator(sessionID);
			// try to run the report, catch and sqlexceptions which come back
			bool sqlOK = false;
			try
			{
				resResults = repGenerator.RunReport(reqRequest);
				sqlOK = true;
			} 
			catch(System.Data.SqlClient.SqlException sqle)
			{
				// if it starts 'Report Error' then it has been passed back from the report
				// hence display it in a friendly way
				if (sqle.Message.StartsWith("Report Error"))
					lblError.Text = String.Format("<p>{0}</p>", sqle.Message);
				else
					// re-throw
					throw new ApplicationException("SQL Error while running report: " + sqle.Message, sqle);

			}
			
			if (sqlOK)
			{
				// get a renderer
				rndRender = sfRenderBroker.GetRenderer(resResults);

				// VAF-201 turn off application tags
				rndRender.IgnoreApplicationTag = true;

				// add to cache and get report index
				intReportIndex = sfSessionReportCache.GetReportCache(Session).Add(rndRender);

				// redirect to viewer, using correct index
				//Response.Redirect("ReportViewerCrystal.aspx?ReportIndex=" & intReportIndex, True)
				Response.Redirect(ReportViewer.GetMyURLWithReportIndex(intReportIndex), true);
			}

		}

		// works out request and then kicks off report agent
		// to send report to user
		// docType: either 'rtf' or 'xls'
		protected void SendReport(string pstrEmail, string docType)
		{
			sfReportRequest reqRequest = null;
			string strReqFile = null;
			//string strTempFolder = null;
			string strCommand = null;
			string strArgs = null;
			string strMessage = null;

			// get args from config setting
			string strAgentLogin = System.Convert.ToString(ConfigurationSettings.AppSettings["ReportAgentLoginName"]);
			string strAgentPassword = System.Convert.ToString(ConfigurationSettings.AppSettings["ReportAgentLoginPassword"]);
			string strSMTPServer = System.Convert.ToString(ConfigurationSettings.AppSettings["ReportAgentSMTPServer"]);
			string strReportAgentLocation = System.Convert.ToString(ConfigurationSettings.AppSettings["ReportAgentLocation"]);

			// get the request
			reqRequest = GetUserReportRequest();

			if (!strReportAgentLocation.EndsWith("\\"))
				strReportAgentLocation += "\\";

			strReqFile = strReportAgentLocation + "Temp\\Request" + Guid.NewGuid().ToString() + ".xml";
			

			// save the request to a file
			reqRequest.SaveAsXML(strReqFile);

			// build command

			strArgs = "/mode:E /req:\"" + strReqFile + "\" /to:\"" + pstrEmail + "\" ";
			strArgs += " /valogin:" + strAgentLogin + " /vapass:" + strAgentPassword + " ";
			strArgs += " /smtp:" + strSMTPServer + " /doctype:" + docType;
			strCommand = strReportAgentLocation + "SF2ReportAgent.exe";

			//<!-- VAF-68
			//System.Diagnostics.Process.Start(strCommand, strArgs);
			SendReportToUser(strReqFile,pstrEmail,strAgentLogin, strAgentPassword, strSMTPServer,docType);
			//-->

			// re-direct to message page
			strMessage = "Your report is being run and will be emailed to " + pstrEmail + " shortly. ";

			//Response.Redirect(SFMessage.GetMyURLWithMessage(strMessage));
			Response.Redirect("SFMessage.aspx?Message=" + strMessage);

		}

		#region code copied from sf2 report agent part 2

		//<!-- VAF-68
		internal static void SendReportToUser(string pstrRequestPath, string pstrEmail, string pstrVALogin, string pstrVAPassword, string pstrSMTP, string pstrDocType)
		{

			const string mcstrAgentFrom = "starfish.reportagent@vso.org.uk";

			sfReportRequest reqRequest = null;
			sfReportGenerator objGenerator = null;
			sfReportResults resResults = null;
			sfReportRenderer rndRenderer = null;
			string strBody = null;
			string strAttachFile = null;
			MailMessage objMail = null;
			MailAttachment objAttach = null;
			Stream objFileStream = null;

			// embed the whole thing in a try--catch


			try
			{

				// log onto va
				//bool login = BnbEngine.SessionManager.StartNewSession(pstrVALogin, pstrVAPassword);
				bool login = true;
				if (login)
				{
					string bonoboSessionID = BnbEngine.SessionManager.GetBonoboSessionID().ToString();
					BnbConfigurationInfo bonoboConfig = BnbEngine.SessionManager.GetConfigurationInfo();
					Logging.LogEvent("Logged on to Bonobo. Server: " + bonoboConfig.DatabaseServer + " Database: " + bonoboConfig.DatabaseName, EventLogEntryType.Information);
					// instantiate the report request
					reqRequest = new sfReportRequest(bonoboSessionID, pstrRequestPath);
					Logging.LogEvent("Got request", EventLogEntryType.Information);
					// run it
					objGenerator = new sfReportGenerator(bonoboSessionID);
					resResults = objGenerator.RunReport(reqRequest);
					Logging.LogEvent("Ran report, rows=" + resResults.RowCount, EventLogEntryType.Information);
					// render a csv file
					rndRenderer = sfRenderBroker.GetRenderer(resResults);
					
					strAttachFile = AppDomain.CurrentDomain.BaseDirectory + "Temp\\" + resResults.SuggestedFilename;
					switch(pstrDocType.ToLower())
					{
						case("rtf"):
						case("doc"): 
							strAttachFile += ".rtf";
							objFileStream = new FileStream(strAttachFile, FileMode.CreateNew, FileAccess.Write);
							rndRenderer.WriteRtfReport(objFileStream, false);
							objFileStream.Close();
							break;

						case("xls"):
							strAttachFile += ".xls";
							objFileStream = new FileStream(strAttachFile, FileMode.CreateNew, FileAccess.Write);
							rndRenderer.WriteExcelReport(objFileStream, false);
							objFileStream.Close();
							break;

						default:
							throw new ApplicationException("Report Agent called with unknown doctype parameter of " + pstrDocType);
						
					}
					Logging.LogEvent("Made file", EventLogEntryType.Information);

					// um ... how to send an email?
					objMail = new MailMessage();


					objMail.From = mcstrAgentFrom;
					objMail.To = pstrEmail;
					objMail.BodyFormat = MailFormat.Text;
					objMail.Priority = MailPriority.Normal;
					objMail.Subject = resResults.SuggestedFilename;

					strBody = "Please find attached the results for the following report run:" + "\r" + "\r";
					strBody += reqRequest.ReportNameFull + '\r';
					strBody += "Total Rows: " + resResults.RowCount + "   Run at " + resResults.DataTimestamp.ToString("dd-MMM-yyyy HH:mm:ss") + '\r';
					strBody += "Date Option: " + reqRequest.DateDescriptionComplete + '\r';
					strBody += "Group By: " + reqRequest.GroupName + " Sort By: " + reqRequest.SortName + '\r';
					strBody += "User Criteria: " + reqRequest.UserCriteriaDescription + '\r';
					strBody += "Default Criteria: " + reqRequest.DefaultCriteriaDescription + '\r';
					strBody += "\r";
					strBody += "(Database used was " + bonoboConfig.DatabaseServer + "." + bonoboConfig.DatabaseName + ")  \r";





					//The message text 
					objMail.Body = strBody;

					// make attachment
					// NB: need full path filename for this to work
					objAttach = new MailAttachment(strAttachFile);

					objMail.Attachments.Add(objAttach);


					Logging.LogEvent("Made email", EventLogEntryType.Information);
					SmtpMail.SmtpServer = pstrSMTP;
					Logging.LogEvent("Server: " + SmtpMail.SmtpServer, EventLogEntryType.Information);

					SmtpMail.Send(objMail);

					Logging.LogEvent("Sent email", EventLogEntryType.Information);


					// logging out
					System.IO.File.Delete(strAttachFile);
					System.IO.File.Delete(pstrRequestPath);
					//BnbEngine.SessionManager.EndSession();

				}
				else
				{
					throw new System.ApplicationException("Could not log on to VA");
				}

			}
			catch (System.Exception e)
			{

				Logging.LogEvent(e, EventLogEntryType.Error);
				Logging.LogEvent("Report failed.", EventLogEntryType.FailureAudit);
				// try to email problem
				EmailError(e, pstrEmail, pstrSMTP);

			}
			//-->


		}

		//<!-- VAF-68
		// tries to email problem to user
		// if fails, writes to standard out instead
		private static void EmailError(System.Exception e, string pstrEmail, string pstrSMTP)
		{
			const string mcstrAgentFrom = "starfish.reportagent@vso.org.uk";

			string strBody = null;
			MailMessage objMail = null;

			try
			{

				// um ... how to send an email?
				objMail = new MailMessage();


				objMail.From = mcstrAgentFrom;
				objMail.To = pstrEmail;
				objMail.BodyFormat = MailFormat.Text;
				objMail.Priority = MailPriority.Normal;
				objMail.Subject = "Error while running report";

				strBody = "The Starfish Report Agent encountered an error when trying to run your report." + "\r" + "\r";
				strBody += "If this keeps happening, please forward this email to the IT Helpdesk." + '\r' + '\r';
				strBody += "Error details:" + '\r';
				strBody += "Message:" + e.Message + '\r';
				strBody += "Source: " + e.Source + '\r';
				strBody += "Stack: " + e.StackTrace;

				objMail.Body = strBody;


				SmtpMail.SmtpServer = pstrSMTP;

				SmtpMail.Send(objMail);

			}
			catch (System.Exception mailerror)
			{

				Logging.LogEvent(mailerror, EventLogEntryType.Error);

			}

		}
		//-->
		#endregion

		// returns the 'allowcriteriaID' rows for the current reportid
		protected DataRow[] ReportAllowCriteriaRows
		{
			get
			{
				DataSet dtsLoad = null;
				DataTable dtbReportChoice = null;
				DataRow[] dtrReportRows = null;

				// if not already loaded then load
				if (mdtrAllowCriteria == null)
				{

					// load the data
					dtsLoad = new DataSet();
					dtsLoad.ReadXml(Server.MapPath("~\\Reports\\ReportChooser.xml"));

					dtbReportChoice = dtsLoad.Tables["ReportChoice"];

					// get the row for reportID
					dtrReportRows = dtbReportChoice.Select("ChoiceGroup = '1' and ReportID = '" + this.ReportID + "'");

					if (dtrReportRows.Length > 0)
					{
						mdtrAllowCriteria = dtrReportRows[0].GetChildRows("ReportChoice_AllowCriteriaID");

					}
				}
				return mdtrAllowCriteria;

			}
		}

		/// <summary>
		/// Returns the main 'report chooser' row from the ReportChooser.xml file
		/// </summary>
		protected DataRow ReportChooserRow
		{
			get
			{
				DataSet dtsLoad = null;
				DataTable dtbReportChoice = null;
				DataRow[] dtrReportRows = null;

				// if not already loaded then load
				if (mdtrReportChooser == null)
				{

					// load the data
					dtsLoad = new DataSet();
					dtsLoad.ReadXml(Server.MapPath("~\\Reports\\ReportChooser.xml"));

					dtbReportChoice = dtsLoad.Tables["ReportChoice"];

					// get the row for reportID
					dtrReportRows = dtbReportChoice.Select("ChoiceGroup = '1' and ReportID = '" + this.ReportID + "'");

					if (dtrReportRows.Length > 0)
					{
						mdtrReportChooser = dtrReportRows[0];

					}
				}
				return mdtrReportChooser;

			}
		}

		// returns True if the specified CriteriaDefintionID
		// is allowed for the current report (see me.reportID)
		// what is allowed or not is defined in ReportChooser.xml
		protected bool IsCriteriaDefinitionIDAllowed(int pintCriteriaDefinitionID)
		{
			DataRow[] dtrAllowed = null;
//INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
//			DataRow dtrLoop = null;
			bool blnFound = false;

			// get allowed rows
			dtrAllowed = this.ReportAllowCriteriaRows;

			// look for it
			if (dtrAllowed != null)
			{
				foreach (DataRow dtrLoop in dtrAllowed)
				{
					// think field gets _Text added because it
					// matches the automatic key that gets generated for the
					// AllowCriteriaID table...
					if (System.Convert.ToInt32(dtrLoop["AllowCriteriaID_Text"]) == pintCriteriaDefinitionID)
					{
						blnFound = true;
					}
				}
			}

			return blnFound;

		}

		protected void RefreshUserCriteriaDescription()
		{

			// only do this is user editing is allowed
			if (this.AllowCriteriaEdit)
			{
				uxUserCriteriaDescription.Text = ExtractCriteriaFromRepeater().Description;
			}

		}

		// check which items user has selected in user criteria repeater
		// and returns sql and descriptive strings
		protected sfReportFilter ExtractCriteriaFromRepeater()
		{

			//int intFieldIndex = 0;
			string strThisClauseSQL = null;
			string strThisClauseDescription = null;
			sfReportFilter rv = new sfReportFilter(string.Empty, string.Empty);

			foreach (RepeaterItem itmRepLoop in repUserCriteria.Items)
			{
				// changed to use FindControl instead of looping through controls collection
				SFReportCriteria usrReportCriteria = itmRepLoop.FindControl("usrReportCriteria") as SFReportCriteria;
				if (usrReportCriteria != null)
				{
				
						// get sql and descriptive strings
						strThisClauseSQL = usrReportCriteria.GetClauseSQL();
						strThisClauseDescription = usrReportCriteria.GetClauseDescription();

						// if we have anything
						if (strThisClauseSQL != "")
						{
							// add ANDs if necessary
							if (rv.Filter.Length > 0)
							{
								rv.Filter += " AND ";
								rv.Description += " And ";
							}
							// add clause
							rv.Filter += strThisClauseSQL;
							rv.Description += strThisClauseDescription;
						}

					
				}
			}

			// if sql string empty then set to truism
			if (rv.Filter.Length == 0)
			{
				rv.Filter = "(1=1)";
				rv.Description = "None";
			}

			return rv;

		}

		
		/// <summary>
		///true if user is allowed to edit criteria
		/// by filling in lists
		/// (for loaded templates with criteria, they won't
		/// be able to) 
		/// </summary>
		protected bool AllowCriteriaEdit
		{
			get
			{
				if (uxHiddenAllowCriteriaEdit.Text == "0")
					return false;
				else
					return true;
			}
			set
			{
				repUserCriteria.Visible = value;
				
				if (value)
					uxHiddenAllowCriteriaEdit.Text = "1";
				else
					uxHiddenAllowCriteriaEdit.Text = "0";
			}
		}

		protected string DefaultCriteriaSQL
		{
			get
			{
				return txtHiddenDefaultCriteriaSQL.Text;
			}
			set
			{
				txtHiddenDefaultCriteriaSQL.Text = value;
			}
		}

		// this property is used to store the user criteria sql
		// if the request has sql which repUserCriteria cannot handle
		// (at this point, that means pretty much any sql)
		protected string UserCriteriaSQL
		{
			get
			{
				return txtHiddenUserCriteriaSQL.Text;
			}
			set
			{
				txtHiddenUserCriteriaSQL.Text = value;
			}
		}

		// shows links to templates
		// either for the current user, or for all users,
		// depending on the toggle button
		protected void RefreshTemplates()
		{
			DataTable tblTemplates = null;
			string strUserID = null;
			string strList = null;

			// first, if button text not set, then set it
			if (cmdToggleTemplate.Text == "")
			{
				cmdToggleTemplate.Text = mcstrTemplateTextCurrentUser;

			}


			strUserID = BnbEngine.SessionManager.GetSessionInfo().UserID.ToString();

			// clear user id if button has been toggled
			if (cmdToggleTemplate.Text == mcstrTemplateTextAllUsers)
			{
				strUserID = "";
			}

			tblTemplates = sfReportRequest.GetReportUserTemplates(this.ReportID, strUserID);

			if (tblTemplates.Rows.Count > 0)
			{
				strList = "<table class=\"reportinfo\" >";
				strList += "<tr><td><strong>Report</strong></td><td><strong>Template Name</strong></td><td><strong>User</strong></td><td><strong>About</strong></td></tr>";
				foreach (DataRow rowLoop in tblTemplates.Rows)
				{
					strList += "<tr>";
					strList += "<td>" + sfRepUtils.SafeNullString(rowLoop["ReportName"]) + "</td>";

					// if screen is showing this template, show
					// name as plain text
					if (this.ReportTemplateID != "" & this.ReportTemplateID == rowLoop["ReportTemplateID"].ToString())
					{
						strList += "<td>" + sfRepUtils.SafeNullString(rowLoop["TemplateName"]) + "</td>";
					}
					else
					{
						// otherwise show as a link to this screen
						// so they can load it
						strList += "<td><a href='ReportOptions.aspx?ReportTemplateID=" + System.Convert.ToString(rowLoop["ReportTemplateID"]) + "'>" + sfRepUtils.SafeNullString(rowLoop["TemplateName"]) + "</a></td>";
					}
					if (sfRepUtils.SafeNullString(rowLoop["FullName"]) == "")
					{
						strList += "<td>None</td>";
					}
					else
					{
						strList += "<td>" + sfRepUtils.SafeNullString(rowLoop["FullName"]) + "</td>";
					}
					strList += "<td><a href='" + ReportInfo.GetMyURLWithReportTemplate(rowLoop["ReportTemplateID"].ToString()) + "'>About</a></td>";

					strList += "</tr>";

				}
				strList += "</table>";
				lblTemplates.Text = strList;
			}
			else
			{
				lblTemplates.Text = "None";
			}

		}

		protected void cmdRefreshUserCriteria_Click(object sender, System.EventArgs e)
		{
			RefreshUserCriteriaDescription();
			RefreshDateControls();
		}

		protected void cmdToggleTemplate_Click(object sender, System.EventArgs e)
		{
			if (cmdToggleTemplate.Text == mcstrTemplateTextCurrentUser)
			{
				cmdToggleTemplate.Text = mcstrTemplateTextAllUsers;
			}
			else
			{
				cmdToggleTemplate.Text = mcstrTemplateTextCurrentUser;
			}
			RefreshTemplates();
		}


		protected void cmdRemoveDefault_Click(object sender, System.EventArgs e)
		{
			uxDefaultCriteriaDescription.Text = "None";
			uxDefaultCriteriaDescription2.Text = uxDefaultCriteriaDescription.Text;
			this.DefaultCriteriaSQL = "";
		}


		protected void cmdSendReport_Click(object sender, System.EventArgs e)
		{
			// check email
			// only allow vsoint.org at the moment
			if (!(txtReportSentAddress.Text.EndsWith("@vsoint.org") ||
					txtReportSentAddress.Text.EndsWith("@vso.org.uk")))
			{
				lblReportSendMessage.Text = "Address must be from @vsoint.org or @vso.org.uk";
			}
			else
			{
				// if they have left the default!
				if (txtReportSentAddress.Text == mcstrUninitializedPersonalEmail)
				{
					lblReportSendMessage.Text = "Please input your email address";
				}
				else
				{
					SendReport(txtReportSentAddress.Text, cboSendDocType.SelectedValue);
				}
			}
		}

	#region  Client-side Javascript 
		/// <summary>
		/// Returns dynamic client side javascript, setting up
		/// arrays and objects that vary for each report template
		/// </summary>
		private string ClientSideDynamicScript
		{
			get
			{
				// get report data we need
				DataSet reportOptions = sfReportRequest.GetReportOptions(this.ReportID);
				DataTable dateOptions = reportOptions.Tables["DateOptions"];
				
				// loop through the repeater to get criteria data
				ArrayList listBoxIDArray = new ArrayList();
				ArrayList criteriaUserDescriptionArray = new ArrayList();
				
				foreach(RepeaterItem  repLoop in repUserCriteria.Items)
				{
					SFReportCriteria usr = repLoop.FindControl("usrReportCriteria") as SFReportCriteria;
					if (usr == null)
						throw new ApplicationException(String.Format("Expecting to find SFReportCriteria in repUserCriteria RepeaterItem {0}",
							repLoop.ItemIndex.ToString()));

					listBoxIDArray.Add(usr.ListBoxClientID);
					criteriaUserDescriptionArray.Add(usr.UserDescription);
					
				}

				StringBuilder sb = new StringBuilder(2500);
				// Outer wrapping
				doAppend(sb,"<script language=\"Javascript\">");

				// output array of criteria listbox IDs
				// (if criteria listboxes are to be rendered)
				sb.Append("var criteriaListBoxes=[");
				if (this.AllowCriteriaEdit)
				{
					int counter = 1;
					foreach(string sLoop in listBoxIDArray)
					{
						if (counter > 1)
							sb.Append(",");

						sb.AppendFormat("\"{0}\"",
							sLoop);
						counter += 1;
					}
				}
				sb.Append("];" + Environment.NewLine);

				// output array of criteria user descriptions
				// (if criteria listboxes are to be rendered)
				sb.Append("var criteriaDescriptions=[");
				if (this.AllowCriteriaEdit)
				{
					int counter = 1;
					foreach(string sLoop in criteriaUserDescriptionArray)
					{
						if (counter > 1)
							sb.Append(",");

						sb.AppendFormat("\"{0}\"",
							sLoop);
						counter += 1;
					}
				}
				sb.Append("];" + Environment.NewLine);

				
				doAppend(sb,"var dateOptionMap = new Object();");

				// output javascript to feed the date option (0,1 or 2) into
				// the dateOptionMap object for each relevant DateOptionID
				// this lets the client side javascript work out (for example)
				// that DateOptionID 545 means a DateOption of '1' (which means 1 date box)
				foreach(DataRow rowLoop in dateOptions.Rows)
					doAppend(sb,String.Format("dateOptionMap[\"{0}\"] = {1};",
						rowLoop["DateOptionID"].ToString(),
						rowLoop["DateOption"].ToString()));

				doAppend(sb,"var dateDescriptionMap = new Object();");

				// output javascript to feed the date user description into
				// the dateDescriptionMap object for each relevant DateOptionID
				// this lets the client side javascript work out (for example)
				// that DateOptionID 545 means a dateDescription of 
				//"Planned Start Date between :StartDate and :EndDate"

				foreach(DataRow rowLoop in dateOptions.Rows)
					doAppend(sb,String.Format("dateDescriptionMap[\"{0}\"] = \"{1}\";",
						rowLoop["DateOptionID"].ToString(),
						rowLoop["DateUser"].ToString()));
				
				// global bool to control criteria
				doAppend(sb,String.Format("var allowCriteriaEdit = {0};",
					this.AllowCriteriaEdit.ToString().ToLower()));
				

				// Outer wrapping
				doAppend(sb, "</script>");
				return sb.ToString();
			}
		}

		
		private void doAppend(StringBuilder sBuilder, string stringToAppend) 
		{
			sBuilder.Append(stringToAppend + Environment.NewLine);
		}

		private void SetVisibility(ref StringBuilder sb, Control ctl, bool Visible)
		{
//INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
//			Control Child = null;
			foreach (Control Child in ctl.Controls)
			{
				if (Child is TextBox | Child is DropDownList)
				{
					sb.Append(' ', 8);
					if (Visible)
					{
						sb.Append("show(\"");
					}
					else
					{
						sb.Append("hide(\"");
					}
					sb.Append(Child.ClientID);
					sb.Append("\");");
					sb.Append(System.Environment.NewLine);
				}
			}
		}

		//Private Sub AddUpdateEvents(ByVal DateControl As SFDate)
		//    Dim child As Control
		//    For Each child In DateControl.Controls
		//        If TypeOf child Is TextBox Then
		//            CType(child, TextBox).Attributes.Add("onChange", "updateDescription()")
		//        ElseIf TypeOf child Is DropDownList Then
		//            CType(child, DropDownList).Attributes.Add("onChange", "updateDescription()")
		//        End If
		//    Next
		//End Sub

		// Tells us whether a particular option needs a start or an end date
		private int DateOptionSettings(int dateOptionID)
		{

			sfReportRequest reqTemp = null;
				int ret = 0;

			// Use a temporary report request
			// object to figure out the implications
			// of the chosen option
			string sessionID = BnbEngine.SessionManager.GetBonoboSessionID().ToString();
			reqTemp = new sfReportRequest(sessionID);
			reqTemp.ReportID = this.ReportID;
			reqTemp.DateOptionID = dateOptionID;

			ret = System.Convert.ToInt32(reqTemp.DateOptionType);
			if (ret >= 0)
			{
					return ret;
				}
				else
				{
					return 0;
				}

		}

	#endregion

		public static void SetComboSelection(DropDownList pcboCombo, string pstrKey)
		{
			foreach (ListItem litItem in pcboCombo.Items)
			{
				if (litItem.Value == pstrKey)
				{
					litItem.Selected = true;
				}
				else
				{
					litItem.Selected = false;
				}
			}
		}

		protected void ReportOptions_PreRender(object sender, EventArgs e)
		{
			// <!-- VAF-67
			// this has to be done in PreRender because criteria user controls
			// wont be populated until after page_load
			//if (!this.IsPostBack && this.AllowCriteriaEdit)
				// refresh the critera description 
				//RefreshUserCriteriaDescription();
			// -->

		}

		// VAF-67
		protected void cmdSaveTemplate_Click(object sender, System.EventArgs e)
		{
			this.SaveTemplate(false);
		}

		// VAF-67
		private void SaveTemplate(bool allowOverwrite)
		{
			lblError.Text = "";
			lblSavedMessage.Text = "";
			if (txtTemplateName.Text == "")
			{
				lblError.Text = "You must supply a Template Name to save a Template.";
				return;
			}
			Guid userID = BnbEngine.SessionManager.GetSessionInfo().UserID;
			// if overwrite flag not set, check if template already exists
			if (!allowOverwrite && BnbLegacyReport.TemplateAlreadyExists(userID,
				this.ReportID, txtTemplateName.Text))
			{
				// show other panel - dont save yet
				lblOverwriteTemplateName.Text = txtTemplateName.Text;
				panelTemplateExists.Visible = true;
				panelSaveTemplate.Visible = false;
				return;
			}
			// are dates valid?
			if (!this.CheckIfUserDatesValid())
			{
				lblError.Text = "Invalid date options. Please check your date options and correct.<br/>";
				return;

			}
			// otherwise go ahead with save...
			// get options from page
			sfReportRequest templateRequest = this.GetUserReportRequest();
			// make the dates save for saving
			DateTime safeStartDate = templateRequest.StartDate;
			if (safeStartDate == new DateTime(1,1,1))
				safeStartDate = new DateTime(1900,1,1);
			DateTime safeEndDate = templateRequest.EndDate;
			if (safeEndDate == new DateTime(1,1,1))
				safeEndDate = new DateTime(1900,1,1);
			// save the template.
			BnbLegacyReport.SaveReportTemplate(userID, this.ReportID, txtTemplateName.Text,
					templateRequest.DateOptionID,
					safeStartDate,
					safeEndDate,
					templateRequest.GroupOptionID,
					templateRequest.SortOptionID,
					templateRequest.UserCriteriaSQL,
					templateRequest.UserCriteriaDescription,
					templateRequest.DefaultCriteriaSQL,
					templateRequest.DefaultCriteriaDescription,
					"",
					"");
			// show confirmation and refresh template list
			lblOverwriteTemplateName.Text = "";
			panelTemplateExists.Visible = false;
			panelSaveTemplate.Visible = true;
			lblSavedMessage.Text = "Saved.";
			this.RefreshTemplates();
		}

		// VAF-67
		private void CancelTemplateSave()
		{
			// show original panel again
			lblOverwriteTemplateName.Text = "";
			panelTemplateExists.Visible = false;
			panelSaveTemplate.Visible = true;

		}

		// VAF-67
		protected void cmdTemplateOverwrite_Click(object sender, System.EventArgs e)
		{
			this.SaveTemplate(true);
		}

		// VAF-67
		protected void cmdCancelTemplateSave_Click(object sender, System.EventArgs e)
		{
			this.CancelTemplateSave();
		}

		/// <summary>
		/// Returns false if user dates invalid
		/// otherwise returns true
		/// </summary>
		/// <returns></returns>
		// VAF-67
		private bool CheckIfUserDatesValid()
		{
			int dateOption = Convert.ToInt16(this.DateOptionID);
			// may be -1 in some cases (if null in template and user has not changed combo)
			// in which case get directly from combo
			if (dateOption == -1)
				dateOption = Convert.ToInt16(uxDateOption.SelectedValue);
			int dateSet = DateOptionSettings(dateOption);
			
			if (dateSet >= 1 & ! uxStartDate.IsValid || dateSet >= 2 & ! uxEndDate.IsValid || dateSet >= 2 & ! (uxEndDate.Date >= uxStartDate.Date))
			{
				return false;
			}
			return true;
		}
	}


	#region Code copied from SF2ReportAgent
	//<!-- VAF-68

	public class Logging
	{

		private const string EventSource = "Starfish Report Agent";

		private Logging()
		{
		}

		public static void LogEvent(string pstrMessage, EventLogEntryType Type)
		{
			Console.Out.WriteLine(pstrMessage);
			if (! (EventLog.SourceExists(EventSource)))
			{
				EventLog.CreateEventSource(EventSource, "Application");
			}
			EventLog.WriteEntry(EventSource, pstrMessage, Type);
		}

		public static void LogEvent(Exception e, EventLogEntryType Type)
		{

			string Message = string.Format("{0}:{1}:n{2}", e.Message, e.Source, e.StackTrace);
			LogEvent(Message, Type);

		}
//-->
		#endregion
	}
} //end of root namespace