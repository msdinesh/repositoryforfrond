using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BonoboEngine.Query;
using BonoboWebControls;
using BonoboEngine;
using BonoboWebControls.DataGrids;



namespace Frond.Reports
{
    public partial class ExcelReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
#if DEBUG
            if (Request.QueryString.ToString() == "") BnbWebFormManager.ReloadPage("viewname=vwBonoboGrants&Excel=0");
#endif
            // Put user code to initialize the page here
            if (Request.QueryString["Excel"] != "0") Response.ContentType = "application/vnd.ms-excel";
            // TPT amended: Included System.Text.Encoding.GetEncoding(1252)to remove (Â) symbol from the Excel Reports column heading
            Response.ContentEncoding = System.Text.Encoding.GetEncoding(1252);
            Bnbdatagridforview2.ViewName = Request.QueryString["ViewName"];

        }
        private void Bnbdatagridforview2_PreRender(object sender, EventArgs e)
        {
            foreach (BnbDataGridColumn col in Bnbdatagridforview2.Columns)
            {
                if (!col.GetDataType().Equals(typeof(System.Decimal))) continue;
                if (col.FieldUserAlias.ToLower() == "exchange rate") col.FormatExpression = "#.000";
                else col.FormatExpression = "#,0";
            }
        }

    }
}
