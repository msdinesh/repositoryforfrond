//INSTANT C# NOTE: Formerly VB.NET project-level imports:
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SF2ReportsEngine;
using BonoboEngine;

namespace Frond.Reports
{
	public partial class ReportGroups : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;
		private sfReportRenderer mrndRender;

	#region  Web Form Designer Generated Code 

		//This call is required by the Web Form Designer.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{

		}

//ORIGINAL LINE: Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles base.Init
		override protected void OnInit(System.EventArgs e)
		{
				base.OnInit(e);
			//CODEGEN: This method call is required by the Web Form Designer
			//Do not modify it using the code editor.
			InitializeComponent();
		}

	#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// stf-152 - make export downloads work over ssl
			Response.Cache.SetCacheability(HttpCacheability.Public);

			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);

			if (! Page.IsPostBack)
			{
				ParseQueryString();
				ShowReportInfo();
				RenderGroups();
			}

			ReportExistenceCheck();

		}

		// returns quasi-absolute path to page
		public static string GetMyURL()
		{
			string strBase = null;
			strBase = HttpContext.Current.Request.ApplicationPath;
			//<!-- Inserted by Napoleon; no swivl call ref
			//Added this line of code so this function works regardless which virtual directory Starfish is installed in
			if (strBase == "/")
			{
					strBase = string.Empty;
				}
			//-->
			return strBase + "/Reports/ReportGroups.aspx";
		}

		public static string GetMyURLWithReportIndex(int pintReportIndex)
		{
			return ReportGroups.GetMyURL() + "?ReportIndex=" + pintReportIndex;
		}

		public static string GetMyURLWithResultsXML(string pstrResultsXML)
		{
			return ReportGroups.GetMyURL() + "?ResultsXML=" + pstrResultsXML;
		}


		// gets info out of query string and puts into properties
		private void ParseQueryString()
		{
			string strResultsXMLFile = null;

			// if report index specified
			if (Request.QueryString["ReportIndex"] != "" && Request.QueryString["ReportIndex"] != null)
			{
				// store it
				this.ReportIndex = System.Convert.ToInt32(Request.QueryString["ReportIndex"]);
			}
			else
			{
				// if xml path specified
				if (Request.QueryString["ResultsXML"] != "" && Request.QueryString["ResultsXML"] != null)
				{

					strResultsXMLFile = Request.QueryString["ResultsXML"];
					ShowStaticReport(strResultsXMLFile);
				}
			}

		}

		// if running from a report in the session report cache,
		// this returns the index of the report
		public int ReportIndex
		{
			get
			{
				if (txtHiddenReportIndex.Text == "")
				{
					return -1;
				}
				else
				{
					return System.Convert.ToInt32(txtHiddenReportIndex.Text);
				}
			}
			set
			{
				txtHiddenReportIndex.Text = System.Convert.ToString(value);
			}
		}

		private void ShowStaticReport(string strResultsXMLFile)
		{
			sfReportResults resResults = null;
			sfReportRenderer rndRender = null;
			int intReportIndex = 0;

			// try to make results from xml file
			string sessionID = BnbEngine.SessionManager.GetBonoboSessionID().ToString();
			resResults = new sfReportResults(sessionID, Server.MapPath(strResultsXMLFile));

			// get renderer
			rndRender = sfRenderBroker.GetRenderer(resResults);

			// set to ignore application tag VAF-201
			rndRender.IgnoreApplicationTag = true;

			// add to cache and get index
			intReportIndex = ReportCache.Add(rndRender);

			// page must remember it
			// (this will allow the page to pull the renderer back out of report cache)
			this.ReportIndex = intReportIndex;

		}

		private sfSessionReportCache ReportCache
		{
			get
			{
				return sfSessionReportCache.GetReportCache(Session);
			}
		}

		// shows info about the report run on the page
		private void ShowReportInfo()
		{
			string strInfo = null;
			sfReportRequest reqRequestInfo = null;

			reqRequestInfo = this.ReportResults.Request;

			titleBar.TitleText = "Report Contents - " + reqRequestInfo.ReportNameFull;
			strInfo += "Total Rows: " + this.ReportResults.RowCount + "   Run at " + this.ReportResults.DataTimestamp.ToString("dd-MMM-yyyy HH:mm:ss") + "<br>";
			strInfo += "Date Option: " + reqRequestInfo.DateDescriptionComplete + "<br>";
			strInfo += "Group By: " + reqRequestInfo.GroupName + " Sort By: " + reqRequestInfo.SortName + "<br>";
			strInfo += "User Criteria: " + reqRequestInfo.UserCriteriaDescription + "<br>";
			strInfo += "Default Criteria: " + reqRequestInfo.DefaultCriteriaDescription;

			lblReportInfo.Text = strInfo;

			hlDetails.NavigateUrl = ReportViewer.GetMyURLWithReportIndex(this.ReportIndex);

		}

		private sfReportRenderer ReportRenderer
		{
			get
			{
				// if not got ref yet
				if (mrndRender == null)
				{
					// if we have an index
					if (this.ReportIndex > -1)
					{
						// try to get report from session cache
						mrndRender = ReportCache[this.ReportIndex];
					}
					else
					{

					}
				}
				// return whatever reference we now have
				return mrndRender;

			}
		}

		private sfReportResults ReportResults
		{
			get
			{
				return this.ReportRenderer.ReportResults;
			}
		}

		// returns true if a report is loaded
		private bool HasReportResults
		{
			get
			{
				if (this.ReportRenderer == null)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
		}

		// checks if report exists and gives messages if it doesn't
		private void ReportExistenceCheck()
		{
			// if no report
			if (this.HasReportResults == false)
			{

				// if a report index was specified
				if (this.ReportIndex > -1)
				{
					// give suitable warning
					lblMissingWarning.Text = "The report that you ran is no longer cached at the Server. You will have to run it again.";
				}
			}
			else
			{
				lblMissingWarning.Text = "";
			}
		}

		private void RenderGroups()
		{
			string strTable = null;
			int intLoop = 0;


			strTable += "<table class=\"reportcontents\">";
			strTable += "<tr><td><strong>View</strong></td>";
			strTable += this.ReportRenderer.GroupInfoHeadingsAsHTML();
			strTable += "</tr>";

//INSTANT C# NOTE: The ending condition of VB 'For' loops is tested only on entry to the loop. Instant C# has created a temporary variable in order to use the initial value of this.ReportRenderer.GroupCount for every iteration:
			int tempFor1 = this.ReportRenderer.GroupCount;
			for (intLoop = 0; intLoop < tempFor1; intLoop++)
			{
				strTable += "<tr>";
				strTable += "<td><a href='" + ReportViewer.GetMyURLWithReportIndexShowRow(this.ReportIndex, this.ReportRenderer.GetGroupStartRow(intLoop)) + "'>View</a></td>";
				strTable += this.ReportRenderer.GroupInfoAsHTML(intLoop);
				strTable += "</tr>";
			}
			strTable += "<tr>";
			if (this.ReportRenderer.GroupCount > 0)
			{
				strTable += "<td></td>";
			}
			else
			{
				// if there were no groups, then put a link
				// in the final 'totals' row, so users can get back to report!
				strTable += "<td><a href='" + ReportViewer.GetMyURLWithReportIndex(this.ReportIndex) + "'>View</a></td>";
			}
			strTable += this.ReportRenderer.GroupInfoTotalsAsHTML();
			strTable += "</tr>";

			strTable += "</table>";

			lblReportGroups.Text = strTable;

		}

		private void ReturnCsvReport()
		{

			// write the report
			SetResponseHeaders(this.ReportResults.SuggestedFilename + ".csv", false);
			this.ReportRenderer.WriteCsvReport(Response.OutputStream, chkOutputAllCols.Checked);
			Response.End();

		}

		// inserts the specified file into the 'response'
		// so that it will be downloaded to the user
		// If pblnExcel is set to true, uses specific Excel contenttype
		private void SetResponseHeaders(string pstrFileName, bool pblnExcelFile)
		{

			// incorporate the filename into the response object

			Response.Clear();
			Response.AppendHeader("Content-Disposition", "attachment;filename=" + pstrFileName + ";");
			// this helps browser show progress etc
			// Response.AddHeader("Content-Length", pfilFileInfo.Length.ToString())
			if (pblnExcelFile)
			{
				Response.ContentType = "application/vnd.ms-excel";
			}
			else
			{
				Response.ContentType = "application/octet-stream";
			}

		}

		private void ReturnExcelReport()
		{

			SetResponseHeaders(this.ReportResults.SuggestedFilename + ".xls", true);
			this.ReportRenderer.WriteExcelReport(Response.OutputStream, chkOutputAllCols.Checked);
			Response.End();

		}

		private void ReturnWordReport()
		{

			// make the file
			// ignore all columns checkbox

			SetResponseHeaders(this.ReportResults.SuggestedFilename + ".rtf", false);
			this.ReportRenderer.WriteRtfReport(Response.OutputStream, false);
			Response.End();

		}



		protected void cmdGetCSV_Click(object sender, System.EventArgs e)
		{
			
			ReturnCsvReport();
		}

		protected void cmdGetExcel_Click(object sender, System.EventArgs e)
		{
			
			ReturnExcelReport();
		}

		protected void cmdGetWord_Click(object sender, System.EventArgs e)
		{
			
			ReturnWordReport();
		}
	}

} //end of root namespace