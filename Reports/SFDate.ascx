<%@ Control Language="c#" AutoEventWireup="True" Codebehind="SFDate.ascx.cs" Inherits="Frond.Reports.SFDate" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<asp:Label id="lblMain" runat="server" class="dateLabel">Label</asp:Label>
<asp:TextBox id="txtDay" runat="server" Width="31px" Visible="False" class="textInput"></asp:TextBox>
<asp:DropDownList id="cboMonth" runat="server" Width="50px" Visible="False" class="dropDownList" ></asp:DropDownList>
<asp:TextBox id="txtYear" runat="server" Width="50px" Visible="False" class="textInput" ></asp:TextBox>
<asp:CustomValidator id=valDate Runat=server Display=Dynamic Text="*" ErrorMessage="Invalid date" Visible="false"/>
