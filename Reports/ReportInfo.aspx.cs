//INSTANT C# NOTE: Formerly VB.NET project-level imports:
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BonoboEngine.Query;
using BonoboEngine;
using SF2ReportsEngine;

namespace Frond.Reports
{
	public partial class ReportInfo : System.Web.UI.Page
	{
		protected UserControls.Title titleBar;

	#region  Web Form Designer Generated Code 

		//This call is required by the Web Form Designer.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{

		}

//ORIGINAL LINE: Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles base.Init
		override protected void OnInit(System.EventArgs e)
		{
				base.OnInit(e);
			//CODEGEN: This method call is required by the Web Form Designer
			//Do not modify it using the code editor.
			InitializeComponent();
		}

	#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//Put user code to initialize the page here

			if (! Page.IsPostBack)
			{
				ParseQueryString();
				LoadAndShowReportInfo();
			}
			BonoboWebControls.BnbWorkareaManager.SetPageStyleOfUser(this);

		}

		// returns absolute path to page
		public static string GetMyURL()
		{
			string strBase = null;
			strBase = HttpContext.Current.Request.ApplicationPath;
			//<!-- Inserted by Napoleon; no swivl call ref
			//Added this line of code so the function works regardless which virtual directory Starfish is installed
			if (strBase == "/")
			{
					strBase = string.Empty;
				}
			//-->
			return strBase + "/Reports/ReportInfo.aspx";
		}



		// returns url with specified ReportTemplate parameter
		public static string GetMyURLWithReportTemplate(string pstrReportTemplateID)
		{
			return ReportInfo.GetMyURL() + "?ReportTemplateID=" + sfRepUtils.TrimBrackets(pstrReportTemplateID);
		}

		public static string GetMyURLWithReport(int pintReportID)
		{
			return ReportInfo.GetMyURL() + "?ReportID=" + pintReportID;
		}



		public string ReportTemplateID
		{
			get
			{
				return txtHiddenReportTemplateID.Text;
			}
			set
			{
				txtHiddenReportTemplateID.Text = value;
			}
		}

		protected void ParseQueryString()
		{
			string strReportTemplateId = null;
			int intReportId = 0;

			strReportTemplateId = Request.QueryString["ReportTemplateID"];
			//if none specified
			if (strReportTemplateId == "")
			{
				// try reportID
				intReportId = System.Convert.ToInt32(Request.QueryString["ReportID"]);
				strReportTemplateId = sfReportRequest.GetDefaultReportTemplate(intReportId);
			}
			this.ReportTemplateID = strReportTemplateId;
		}

		protected void LoadAndShowReportInfo()
		{
			sfReportRequest reqTemplateInfo = null;
			DataSet dtsReportOptions = null;
			DataTable tblExportFields = null;
			//string strDefaultTemplateID = null;
			string strSFSessionId = null;

			strSFSessionId = BnbEngine.SessionManager.GetBonoboSessionID().ToString();

			// load template info
			reqTemplateInfo = new sfReportRequest(strSFSessionId);
			reqTemplateInfo.LoadFromReportTemplate(this.ReportTemplateID);

			// get report options
			dtsReportOptions = BnbLegacyReport.GetReportOptions(reqTemplateInfo.ReportID);

			// get export fields
			tblExportFields = sfReportRequest.GetReportExportFields(reqTemplateInfo.ReportID);

			// now start displaying stuff
			this.DisplayBasicInfo(reqTemplateInfo);
			this.DisplayDateOptions(dtsReportOptions.Tables["DateOptions"], reqTemplateInfo.DateOptionID);
			this.DisplayGroupOptions(dtsReportOptions.Tables["GroupOptions"], reqTemplateInfo.GroupOptionID);
			this.DisplaySortOptions(dtsReportOptions.Tables["SortOptions"], reqTemplateInfo.SortOptionID);
			this.DisplayReportCriteria(dtsReportOptions.Tables["CriteriaOptions"]);
			this.DisplayExportFields(tblExportFields);

		}

		protected void DisplayBasicInfo(sfReportRequest preqTemplateInfo)
		{
			string strDefaultCriteriaDescription = null;
			string strUserCriteriaDescription = null;
			string strCreatedUserID = null;
			string strCreatedUserName = null;
			string strReportName = null;
			string strBaseTemplateID = null;
			BnbLookupDataTable tblUserLookup = null;
			string strExclusions = null;

			titleBar.TitleText = "Report Info - " + preqTemplateInfo.ReportNameFull;
			strReportName = preqTemplateInfo.ReportName;
			if (preqTemplateInfo.CatalogueNumber != "")
			{
				strReportName += " (" + preqTemplateInfo.CatalogueNumber + ")";
			}
			lblReportName.Text = strReportName;

			hlRunReport.NavigateUrl = ReportOptions.GetMyURLWithReportTemplate(this.ReportTemplateID);

			// show template info if not a default report
			if (! preqTemplateInfo.IsDefault)
			{
				pnlTemplateInfo.Visible = true;
				pnlBaseReportInfo.Visible = false;
				lblTemplateName.Text = preqTemplateInfo.TemplateName;
				lblTemplateDescription.Text = preqTemplateInfo.TemplateDescription;
				strCreatedUserID = preqTemplateInfo.TemplateCreatedByUserID;
				if (strCreatedUserID != "")
				{
					tblUserLookup = BnbEngine.LookupManager.GetLookup("vlkuBonoboUser");
					DataRow foundRow = tblUserLookup.FindByID(new Guid(strCreatedUserID));
					if (foundRow != null)
					{
						strCreatedUserName = sfRepUtils.SafeNullString(foundRow["Description"]);
					}
					else
					{
						strCreatedUserName = "";
					}
					lblTemplateUser.Text = strCreatedUserName;
				}
					lblSecondaryHeading.Text = preqTemplateInfo.SecondaryHeading;
					lblTemplateDateOption.Text = preqTemplateInfo.DateDescriptionComplete;

					lblLookAtBaseTemplate.Visible = true;
					hlBaseInfo.Visible = true;
					strBaseTemplateID = sfReportRequest.GetDefaultReportTemplate(preqTemplateInfo.ReportID);
					hlBaseInfo.NavigateUrl = ReportInfo.GetMyURLWithReportTemplate(strBaseTemplateID);

				}
				else
				{
				pnlTemplateInfo.Visible = false;
				lblLookAtBaseTemplate.Visible = false;
				hlBaseInfo.Visible = false;
				pnlBaseReportInfo.Visible = true;
				lblReportDescription.Text = preqTemplateInfo.TemplateDescription;
			}

			strExclusions = preqTemplateInfo.ReportExclusions;
			if (strExclusions == "")
			{
				strExclusions = "None";
			}
			lblExclusions.Text = strExclusions;

			strDefaultCriteriaDescription = preqTemplateInfo.DefaultCriteriaDescription;
			strUserCriteriaDescription = preqTemplateInfo.UserCriteriaDescription;

			// check if criteria are defined as SQL only
			if (strDefaultCriteriaDescription == "" & preqTemplateInfo.DefaultCriteriaSQL != "")
			{
				strDefaultCriteriaDescription = "No Description available. SQL is " + preqTemplateInfo.DefaultCriteriaSQL;
			}
			if (strUserCriteriaDescription == "" & preqTemplateInfo.UserCriteriaSQL != "")
			{
				strUserCriteriaDescription = "No Description available. SQL is " + preqTemplateInfo.UserCriteriaSQL;
			}

			if (strDefaultCriteriaDescription == "")
			{
				strDefaultCriteriaDescription = "None";
			}
			lblDefaultCriteria.Text = strDefaultCriteriaDescription;

			if (strUserCriteriaDescription == "")
			{
				strUserCriteriaDescription = "None";
			}
			lblUserCriteria.Text = strUserCriteriaDescription;

		}

		protected void DisplayDateOptions(DataTable ptblDateOptions, int pintDefaultOptionID)
		{
			string strTable = null;
			string strDateOptionName = null;
			string strDateOptionDescription = null;

			strTable = "<table class=\"reportinfo\">";
			strTable += "<tr><td width=\"30%\"><strong>Date Option</strong></td><td><strong>Description</strong></td></tr>";
			if (ptblDateOptions.Rows.Count > 0)
			{
//INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
//				DataRow rowLoop = null;
				foreach (DataRow rowLoop in ptblDateOptions.Rows)
				{

					strDateOptionName = sfRepUtils.SafeNullString(rowLoop["DateOptionName"]);
					if (sfRepUtils.SafeNullInteger(rowLoop["DateOptionID"]) == pintDefaultOptionID)
					{
						strDateOptionName += " (Default)";
					}
					strDateOptionDescription = sfRepUtils.SafeNullString(rowLoop["DateUser"]);

					strTable += "<tr><td>" + strDateOptionName + "</td><td>" + strDateOptionDescription + "</td></tr>";
				}
			}
			else
			{
					strTable += "<tr><td>None</td><td></td></tr>";
			}
			strTable += "</table>";

			lblDateOptions.Text = strTable;

		}

		protected void DisplayGroupOptions(DataTable ptblGroupOptions, int pintDefaultOptionID)
		{
			string strTable = null;
			string strGroupOptionName = null;


			strTable = "<table class=\"reportinfo\">";
			strTable += "<tr><td><strong>Group Option</strong></td></tr>";
			if (ptblGroupOptions.Rows.Count > 0)
			{
//INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
//				DataRow rowLoop = null;
				foreach (DataRow rowLoop in ptblGroupOptions.Rows)
				{

					strGroupOptionName = sfRepUtils.SafeNullString(rowLoop["GroupName"]);
					if (sfRepUtils.SafeNullInteger(rowLoop["GroupOptionID"]) == pintDefaultOptionID)
					{
						strGroupOptionName += " (Default)";
					}

					strTable += "<tr><td>" + strGroupOptionName + "</td></tr>";
				}
			}
			else
			{
				strTable += "<tr><td>None</td></tr>";
			}
			strTable += "</table>";

			lblGroupOptions.Text = strTable;

		}


		protected void DisplaySortOptions(DataTable ptblSortOptions, int pintDefaultOptionID)
		{
			string strTable = null;
			string strSortOptionName = null;

			strTable = "<table class=\"reportinfo\">";
			strTable += "<tr><td><strong>Sort Option</strong></td></tr>";
			if (ptblSortOptions.Rows.Count > 0)
			{
//INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
//				DataRow rowLoop = null;
				foreach (DataRow rowLoop in ptblSortOptions.Rows)
				{

					strSortOptionName = sfRepUtils.SafeNullString(rowLoop["SortName"]);
					if (sfRepUtils.SafeNullInteger(rowLoop["SortOptionID"]) == pintDefaultOptionID)
					{
						strSortOptionName += " (Default)";
					}
					strTable += "<tr><td>" + strSortOptionName + "</td></tr>";
				}
			}
			else
			{
				strTable += "<tr><td>None</td></tr>";
			}
			strTable += "</table>";

			lblSortOptions.Text = strTable;

		}

		protected void DisplayReportCriteria(DataTable ptblReportCriteria)
		{
			string strTable = null;
			string strCriteraName = null;
			string strMode = null;
			string strCriteriaDescription = null;

			strTable = "<table class=\"reportinfo\">";
			strTable += "<tr><td width=\"30%\"><strong>Criteria Name</strong></td><td><strong>Mode</strong></td><td><strong>Description</strong></td></tr>";
			if (ptblReportCriteria.Rows.Count > 0)
			{
//INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
//				DataRow rowLoop = null;
				foreach (DataRow rowLoop in ptblReportCriteria.Rows)
				{
					strCriteraName = sfRepUtils.SafeNullString(rowLoop["UserDescription"]);
					strMode = "";
					switch (sfRepUtils.SafeNullInteger(rowLoop["NotMode"]))
					{
						case 0:
							strMode += "Cannot use NOT";
							break;
						case 1:
							// normal
						break;
						case 2:
							strMode += "Special NOT behaviour";
							break;
					}
					if (sfRepUtils.SafeNullBoolean(rowLoop["DefaultCriteriaOnly"]))
					{
						strMode += " Default Criteria only";
					}
					strCriteriaDescription = sfRepUtils.SafeNullString(rowLoop["ShortHelpText"]);
					strTable += "<tr><td>" + strCriteraName + "</td><td>" + strMode + "</td><td>" + strCriteriaDescription + "</td></tr>";


				}
			}
			else
			{
				strTable += "<tr><td>None</td><td></td><td></td></tr>";
			}
			strTable += "</table>";

			lblCriteriaOptions.Text = strTable;

		}

		protected void DisplayExportFields(DataTable ptblExportFields)
		{
			string strMainTable = null;
			string strExtraTable = null;
			string strFieldUserName = null;
			string strFieldDescription = null;



			// if there are FieldOnReport fields, then list them first
			if (ptblExportFields.Select("FieldOnReport = 1").Length > 0)
			{
				strMainTable = "<table class=\"reportinfo\">";
				strMainTable += "<tr><td width=\"30%\"><strong>Field</strong></td><td><strong>Description</strong></td></tr>";
				DataRow[] reportFields = null;
				reportFields = ptblExportFields.Select("FieldOnReport = 1 And Exclude = 0", "DisplayOrder");
				//DataRow rowLoop = null;
				foreach (DataRow rowLoopWithinLoop in reportFields)
				{
					strFieldUserName = sfRepUtils.SafeNullString(rowLoopWithinLoop["CSVUserDescription"]);
					strFieldDescription = sfRepUtils.SafeNullString(rowLoopWithinLoop["ShortHelpText"]);
					strMainTable += "<tr><td>" + strFieldUserName + "</td><td>" + strFieldDescription + "</td></tr>";
				}
				strMainTable += "</table>";
			}

			// if there are additional export fields, do them next
			if (ptblExportFields.Select("FieldOnReport = 0").Length > 0)
			{
				strExtraTable = "<table class=\"reportinfo\">";
				strExtraTable += "<tr><td width=\"30%\"><strong>Field</strong></td><td><strong>Description</strong></td></tr>";

				DataRow[] exportFields = null;
				exportFields = ptblExportFields.Select("FieldOnReport = 0 And Exclude = 0", "DisplayOrder");
//INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
//				DataRow rowLoop = null;
				foreach (DataRow rowLoopWithinLoop in exportFields)
				{

					strFieldUserName = sfRepUtils.SafeNullString(rowLoopWithinLoop["CSVUserDescription"]);
					strFieldDescription = sfRepUtils.SafeNullString(rowLoopWithinLoop["ShortHelpText"]);
					strExtraTable += "<tr><td>" + strFieldUserName + "</td><td>" + strFieldDescription + "</td></tr>";
				}
				strExtraTable += "</table>";
			}

			pnlFieldsOnReport.Visible = false;
			pnlExportFields.Visible = false;
			if (strMainTable != "")
			{
				pnlFieldsOnReport.Visible = true;
				lblReportFields.Text = strMainTable;
			}
			if (strExtraTable != "")
			{
				pnlExportFields.Visible = true;
				lblExportFields.Text = strExtraTable;
			}


		}



	}

} //end of root namespace