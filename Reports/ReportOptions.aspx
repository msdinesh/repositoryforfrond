<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SFDate" Src="~/Reports/SFDate.ascx" %>
<%@ Page AspCompat="false" Language="c#" AutoEventWireup="True" Codebehind="ReportOptions.aspx.cs" Inherits="Frond.Reports.ReportOptions" smartNavigation="true" %>
<%@ Register TagPrefix="uc1" TagName="WaitMessagePanel" Src="../UserControls/WaitMessagePanel.ascx" %>
<%@ Register TagPrefix="bnbdatacontrol" Namespace="BonoboWebControls.DataControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="bnbpagecontrols" Namespace="BonoboWebControls.PageControls" Assembly="BonoboWebControls" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SFReportCriteria" Src="~/Reports/SFReportCriteria.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Report Options</title>
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal><LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet"><asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<META http-equiv="REFRESH" content="540"> <!-- refresh every 9 minutes to keep connection alive -->
		<script language="javascript">

function userCriteriaChanged() 
{
  // check if criteria edits allowed
  if (!allowCriteriaEdit)
    return;

  var critDesc = "";
  for (var lbIndex=0; lbIndex<criteriaListBoxes.length; lbIndex++)
  {
    var lbDesc = getListboxDescriptions(document.getElementById(criteriaListBoxes[lbIndex]));
    if (lbDesc != "")
    {
      if (critDesc.length > 0)
        critDesc += " And ";

      critDesc += "(" + criteriaDescriptions[lbIndex];
      critDesc += " = ";
      critDesc += lbDesc + ")";
      
    }

  }
  if (critDesc == "")
    critDesc = "(None)";

  var summarySpan = document.getElementById("uxUserCriteriaDescription");
  summarySpan.innerHTML = critDesc;
  var summarySpan2 = document.getElementById("uxUserCriteriaDescription2");
  summarySpan2.innerHTML = critDesc;

}

function getListboxDescriptions(lb)
{
  var combinedString = "";
  for (var i=0; i<lb.options.length; i++) 
  {
    if (lb.options[i].selected && lb.options[i].value != "-1") 
    {

      if (combinedString.length > 0)
        combinedString += " or ";

      combinedString += lb.options[i].text;
    }
  }
  return (combinedString);

}

function dateOptionChanged(combo)
{
  var dateKey = combo.value;
  var dateOption = dateOptionMap[dateKey];
  var startBox = document.getElementById("uxStartDate");
  var endBox = document.getElementById("uxEndDate");
  var startLabel = document.getElementById("uxStartDateLabel");
  var endLabel = document.getElementById("uxEndDateLabel");
  if (dateOption == 0)
  {
    startBox.style.visibility="hidden";
    startLabel.style.visibility="hidden";
    endBox.style.visibility="hidden";
    endLabel.style.visibility="hidden";
  }
  if (dateOption == 1)
  {
    startBox.style.visibility="visible";
    startLabel.style.visibility="visible";
    endBox.style.visibility="hidden";
    endLabel.style.visibility="hidden";
  }
  if (dateOption == 2)
  {
    startBox.style.visibility="visible";
    startLabel.style.visibility="visible";
    endBox.style.visibility="visible";
    endLabel.style.visibility="visible";
  }  
  refreshDateDescription();
}

function groupSortChanged()
{
	var groupCombo = document.getElementById("uxGroupOptionCombo");
	var sortCombo = document.getElementById("uxSortOptionCombo");
	var groupLabel = document.getElementById("uxGroupOptionDescription");
	var sortLabel = document.getElementById("uxSortOptionDescription");
	groupLabel.innerHTML = groupCombo.options[groupCombo.selectedIndex].text;
	sortLabel.innerHTML = sortCombo.options[sortCombo.selectedIndex].text;

}

function refreshDateDescription()
{
  var startBox = document.getElementById("uxStartDate");
  var endBox = document.getElementById("uxEndDate");

  var startDate = convertToDateFormat(startBox.value);
  var endDate = convertToDateFormat(endBox.value);

  var optionCombo = document.getElementById("uxDateOption");
  var dateKey = optionCombo.value;

  var dateDescPattern = new String(dateDescriptionMap[dateKey]); 

  var patternFilled = dateDescPattern.replace(/:StartDate/g, startDate).replace(/:EndDate/g, endDate)

  if (patternFilled == "")
    patternFilled = "(None)";

  var summarySpan = document.getElementById("uxDateDescription");
  summarySpan.innerHTML = patternFilled;



}

// not perfect, but will do
function isDateCorrectFormat(sDate)
{
  var dateParts = sDate.split("/");
  var monthMatch = "jan feb mar apr may jun jul aug sep oct nov dec";
  if (dateParts.length != 3)
    return false;

  if (!(isNumeric(dateParts[0]) && dateParts[0] > 0 && dateParts[0] < 32))
    return false;

  if (!(isNumeric(dateParts[2]) && dateParts[2] > 1899 && dateParts[2] < 2999))
    return false;
    
  if (!(dateParts[1].length == 3 && monthMatch.indexOf(dateParts[1].toLowerCase()) > -1))
    return false;

  return true;
}

function isNumeric(n)
{if(n*1==n)return true;else return false;}

function convertToDateFormat(sDate) 
{
  if (isDateCorrectFormat(sDate))
    return sDate;
  else
    return "(invalid date)";
}


		</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu"><uc1:menubar id="menuBar" runat="server"></uc1:menubar></div>
			<div id="baseContent"><uc1:title id="titleBar" runat="server" imagesource="../Images/report24silver.gif"></uc1:title><asp:textbox id="txtHiddenReportID" runat="server" Visible="False"></asp:textbox><asp:textbox id="txtHiddenReportTemplateID" runat="server" Visible="False"></asp:textbox><asp:textbox id="txtHiddenDateOptionID" runat="server" Visible="False"></asp:textbox>
				<p></p>
				<div id="hideEverything">
				<bnbpagecontrols:bnbtabcontrol id="BnbTabControl1" runat="server" IgnoreWorkareas="true" GetShowAllTabsVerticallyFromSession="false"></bnbpagecontrols:bnbtabcontrol><asp:panel id="SummaryTab" runat="server" CssClass="tcTab" BnbTabName="Summary">
					<DIV class="sectionheading">Date Options</DIV>
					<P>Please select one of the date options, and enter the dates you want to filter 
						by.</P>
					<P>
						<asp:dropdownlist id="uxDateOption" runat="server" AutoPostBack="False" onselectedindexchanged="cboDateOption_SelectedIndexChanged"></asp:dropdownlist>&nbsp;&nbsp;&nbsp;
						<asp:label id="uxStartDateLabel" runat="server">Start </asp:label>&nbsp;
						<bnbdatacontrol:BnbStandaloneDateBox id="uxStartDate" runat="server" CssClass="datebox" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"
							Width="90px"></bnbdatacontrol:BnbStandaloneDateBox>&nbsp;
						<asp:label id="uxEndDateLabel" runat="server">End</asp:label>
						<bnbdatacontrol:BnbStandaloneDateBox id="uxEndDate" runat="server" CssClass="datebox" DateFormat="dd/MMM/yyyy" DateCulture="en-GB"
							Width="90px"></bnbdatacontrol:BnbStandaloneDateBox></P>
					<P>
						<asp:label id="uxDateDescription" runat="server" CssClass="reportItemSummary"></asp:label></P>
					<DIV class="sectionheading">Other Options</DIV>
					<P>User Criteria:
						<asp:Label id="uxUserCriteriaDescription2" runat="server" CssClass="reportItemSummary"></asp:Label></P>
					<P>Default Criteria:
						<asp:Label id="uxDefaultCriteriaDescription2" runat="server" CssClass="reportItemSummary"></asp:Label></P>
					<P>Group by:
						<asp:Label id="uxGroupOptionDescription" runat="server" CssClass="reportItemSummary"></asp:Label>&nbsp;Sort 
						by:
						<asp:Label id="uxSortOptionDescription" runat="server" CssClass="reportItemSummary"></asp:Label></P>
					<DIV class="sectionheading">Run Report
					</DIV>
					<P>Press the 'Run Report' button to run the report directly, or press the 'Send 
						Report' button to have the report emailed to you as an Excel or Word file.</P>
					<P>
						<asp:button id="cmdRunReport" runat="server" Text="Run Report" onclick="cmdRunReport_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:button id="cmdSendReport" runat="server" Text="Send Report" onclick="cmdSendReport_Click"></asp:button>&nbsp;to
						<asp:textbox id="txtReportSentAddress" runat="server" Width="214px"></asp:textbox>&nbsp;as
						<asp:dropdownlist id="cboSendDocType" runat="server"></asp:dropdownlist>&nbsp;
						<asp:label id="lblReportSendMessage" runat="server"></asp:label></P>
					<P>
						<asp:label id="lblError" runat="server" CssClass="messageBox" EnableViewState="false"></asp:label></P>
				</asp:panel><asp:panel id="UserCriteriaTab" runat="server" CssClass="tcTab" BnbTabName="User Criteria">
					<P>
						<asp:repeater id="repUserCriteria" runat="server">
							<HeaderTemplate>
								<P>Please select the Criteria you want to apply by highlighting options in the 
									lists below. A summary of what you have selected will automatically be 
									displayed.</P>
								<table>
									<tr>
							</HeaderTemplate>
							<ItemTemplate>
								<td>
									<uc1:SFReportCriteria id="usrReportCriteria" runat="server" CriteriaDefinitionIDString='<%#DataBinder.Eval(Container.DataItem,"CriteriaDefinitionID")%>' FieldName='<%#DataBinder.Eval(Container.DataItem,"FieldName")%>' UserDescription='<%#DataBinder.Eval(Container.DataItem,"UserDescription")%>' >
									</uc1:SFReportCriteria>
								</td>
							</ItemTemplate>
							<FooterTemplate>
								</tr> </table> (Use CTRL or SHIFT to make multiple selections)
							</FooterTemplate>
						</asp:repeater></P>
					<P>
						<asp:label id="uxUserCriteriaDescription" runat="server" CssClass="reportItemSummary"></asp:label></P>
					<P>
						<asp:textbox id="txtHiddenUserCriteriaSQL" runat="server" Visible="False"></asp:textbox></P>
				</asp:panel><asp:panel id="DefaultCriteriaTab" runat="server" CssClass="tcTab" BnbTabName="Default Criteria">
					<P>Below is a description of the Default Criteria for the report. Pressing the 
						Remove link will turn the Default Criteria off.</P>
					<P>
						<asp:label id="uxDefaultCriteriaDescription" runat="server" CssClass="reportItemSummary"></asp:label></P>
					<P>
						<asp:linkbutton id="cmdRemoveDefault" runat="server" CausesValidation="False" onclick="cmdRemoveDefault_Click">Remove</asp:linkbutton>
						<asp:textbox id="txtHiddenDefaultCriteriaSQL" runat="server" Visible="False"></asp:textbox></P>
				</asp:panel><asp:panel id="GroupingSortingTab" runat="server" CssClass="tcTab" BnbTabName="Grouping and Sorting">
					<P>You can adjust the Grouping and Sorting options of the report here:</P>
					<P>Group on:
						<asp:dropdownlist id="uxGroupOptionCombo" runat="server"></asp:dropdownlist>&nbsp; 
						Sort on:
						<asp:dropdownlist id="uxSortOptionCombo" runat="server"></asp:dropdownlist></P>
				</asp:panel>
				<asp:panel id="TemplatesTab" runat="server" CssClass="tcTab" BnbTabName="Templates">
					<asp:Panel id="panelSaveTemplate" runat="server">
						<P>Save options as a Template called:
							<asp:TextBox id="txtTemplateName" runat="server" Width="176px"></asp:TextBox>
							<asp:Button id="cmdSaveTemplate" runat="server" Text="Save" onclick="cmdSaveTemplate_Click"></asp:Button>
							<asp:Label id="lblSavedMessage" style="FONT-WEIGHT: bold; COLOR: green" runat="server"></asp:Label></P>
					</asp:Panel>
					<asp:Panel id="panelTemplateExists" runat="server" Visible="False">
						<P>You already have a Template called
							<asp:Label id="lblOverwriteTemplateName" runat="server"></asp:Label>&nbsp;for 
							this Report. Do you want to overwrite it?</P>
						<P></P>
						<P>
							<asp:Button id="cmdTemplateOverwrite" runat="server" Text="Yes - Overwrite Template" onclick="cmdTemplateOverwrite_Click"></asp:Button>
							<asp:Button id="cmdCancelTemplateSave" runat="server" Text="No - Dont Save Template" onclick="cmdCancelTemplateSave_Click"></asp:Button></P>
					</asp:Panel>
					<P>
						<asp:linkbutton id="cmdToggleTemplate" runat="server" CausesValidation="False" onclick="cmdToggleTemplate_Click"></asp:linkbutton></P>
					<P>
						<asp:label id="lblTemplates" runat="server"></asp:label></P>
				</asp:panel><asp:textbox id="uxHiddenAllowCriteriaEdit" runat="server" Visible="False"></asp:textbox>
				</div>
				<uc1:WaitMessagePanel id="uxWaitMessagePanel" runat="server" DivToHide="hideEverything"></uc1:WaitMessagePanel>
			</div>
		</form>
		<script language="javascript">
// wire up events
var startBox = document.getElementById("uxStartDate");
var endBox = document.getElementById("uxEndDate");
startBox.onchange = refreshDateDescription;
endBox.onchange = refreshDateDescription;
for(var i=0;i<criteriaListBoxes.length;i++)
{
  var listBox = document.getElementById(criteriaListBoxes[i]);
  listBox.onchange = userCriteriaChanged;
}
// initialise client side descriptions
userCriteriaChanged();
groupSortChanged();
var dateOptionCombo = document.getElementById("uxDateOption");
dateOptionChanged(dateOptionCombo);

		</script>
	</body>
</HTML>
