<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../UserControls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Title" Src="../UserControls/Title.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../UserControls/Header.ascx" %>
<%@ Page Language="c#" AutoEventWireup="True" Codebehind="ReportInfo.aspx.cs" Inherits="Frond.Reports.ReportInfo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<HTML>
	<HEAD>
		<title>Report Info</title>
		<asp:literal id="literalBeforeCss" runat="server"></asp:literal>
		<LINK href="../Css/FrondDefault.css" type="text/css" rel="stylesheet">
		<asp:literal id="literalAfterCss" runat="server"></asp:literal>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<div id="baseMenu">
				<uc1:MenuBar id="menuBar" runat="server"></uc1:MenuBar></div>
			<div id="baseContent">
				<uc1:Title id="titleBar" runat="server" imagesource="../Images/report24silver.gif"></uc1:Title>
				<asp:textbox id="txtHiddenReportTemplateID" runat="server" Visible="False"></asp:textbox>
				<P>
					<asp:HyperLink id="hlRunReport" runat="server">Run this report</asp:HyperLink>&nbsp;
					<asp:Label id="lblLookAtBaseTemplate" runat="server" Visible="False">This report is a user-defined report. To see the original report it is based on,</asp:Label>
					<asp:HyperLink id="hlBaseInfo" runat="server" Visible="False">Click here</asp:HyperLink></P>
				<P>
					<TABLE id="Table1" class="ReportInfo">
						<TR>
							<TD width="30%">Report Name</TD>
							<TD>
								<P><asp:label id="lblReportName" runat="server"></asp:label></P>
							</TD>
						</TR>
					</TABLE>
					<asp:Panel id="pnlTemplateInfo" runat="server" Height="23px">
						<TABLE class="ReportInfo" id="Table2">
							<TR>
								<TD width="30%">Template Name</TD>
								<TD>
									<P>
										<asp:label id="lblTemplateName" runat="server"></asp:label></P>
								</TD>
							</TR>
							<TR>
								<TD>Template Description</TD>
								<TD>
									<asp:Label id="lblTemplateDescription" runat="server"></asp:Label></TD>
							</TR>
							<TR>
								<TD>Created By</TD>
								<TD>
									<asp:Label id="lblTemplateUser" runat="server"></asp:Label></TD>
							</TR>
							<TR>
								<TD>Secondary Heading</TD>
								<TD>
									<asp:Label id="lblSecondaryHeading" runat="server"></asp:Label></TD>
							</TR>
							<TR>
								<TD>Date Option</TD>
								<TD>
									<asp:Label id="lblTemplateDateOption" runat="server"></asp:Label></TD>
							</TR>
						</TABLE>
					</asp:Panel>
					<asp:Panel id="pnlBaseReportInfo" runat="server">
						<TABLE class="ReportInfo" id="Table5">
							<TR>
								<TD width="30%">Report Description</TD>
								<TD>
									<P>
										<asp:label id="lblReportDescription" runat="server"></asp:label></P>
								</TD>
							</TR>
						</TABLE>
					</asp:Panel>
					<TABLE class="ReportInfo" id="Table3">
						<TR>
							<TD width="30%">Report Exclusions</TD>
							<TD>
								<asp:Label id="lblExclusions" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD width="30%">Default Criteria</TD>
							<TD>
								<P><asp:label id="lblDefaultCriteria" runat="server"></asp:label></P>
							</TD>
						</TR>
						<TR>
							<TD>User Criteria</TD>
							<TD>
								<P><asp:label id="lblUserCriteria" runat="server"></asp:label></P>
							</TD>
						</TR>
					</TABLE>
				</P>
				<DIV class="SectionHeading">Date Options</DIV>
				<P><asp:label id="lblDateOptions" runat="server"></asp:label></P>
				<DIV class="SectionHeading">Group and Sort Options</DIV>
				<P>
					<TABLE id="Table4" width="100%" border="0">
						<TR>
							<TD width="40%" style="VERTICAL-ALIGN: top">
								<P style="VERTICAL-ALIGN: top"><asp:label id="lblGroupOptions" runat="server"></asp:label></P>
							</TD>
							<td width="20%">
								<P>&nbsp;</P>
							</td>
							<TD width="40%" style="VERTICAL-ALIGN: top">
								<P style="VERTICAL-ALIGN: top"><asp:label id="lblSortOptions" runat="server"></asp:label></P>
							</TD>
						</TR>
					</TABLE>
				</P>
				<DIV class="SectionHeading">Criteria Options</DIV>
				<P><asp:label id="lblCriteriaOptions" runat="server"></asp:label></P>
				<DIV class="SectionHeading">Report Fields</DIV>
				<asp:Panel id="pnlFieldsOnReport" runat="server" Visible="False">
					<P>These fields appear on the report:</P>
					<P></P>
					<P>
						<asp:Label id="lblReportFields" runat="server"></asp:Label></P>
				</asp:Panel>
				<asp:Panel id="pnlExportFields" runat="server" Visible="False">
					<P>These fields are available for exporting:</P>
					<P></P>
					<P>
						<asp:label id="lblExportFields" runat="server"></asp:label></P>
				</asp:Panel>
			</div>
		</form>
	</body>
</HTML>
