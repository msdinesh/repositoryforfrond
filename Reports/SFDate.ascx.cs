// usercontrol to encapsulate a date control
// renders as a label when not in edit mode

//INSTANT C# NOTE: Formerly VB.NET project-level imports:
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Globalization;
using SF2ReportsEngine;

namespace Frond.Reports
{
	[ValidationProperty("DateValue")]
	public partial  class SFDate : sfEditControl
	{

		private readonly string[] ShortMonths = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

		private static readonly System.DateTime mcdatNullDate = DateTime.MinValue;

		private bool m_Required = false;
		private bool m_TodayAddsTime = false;

		// Viewstate constants
		private const string ViewStateSelectedMonth = "SFDate.SelectedMonth";
	#region  Web Form Designer Generated Code 

		//This call is required by the Web Form Designer.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
		valDate.ServerValidate += new System.Web.UI.WebControls.ServerValidateEventHandler(valDate_ServerValidate);

		}

//ORIGINAL LINE: Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles base.Init
		override protected void OnInit(System.EventArgs e)
		{
				base.OnInit(e);
			//CODEGEN: This method call is required by the Web Form Designer
			//Do not modify it using the code editor.
			InitializeComponent();

		}

	#endregion

		protected SFDate()
		{
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//Put user code to initialize the page here

			// if label is same height as other stuff it looks nicer
			//lblMain.Height = txtDay.Height
			lblMain.Text = Text;
			// Years can't be more than 4 digits long
			txtYear.MaxLength = 4;
			txtDay.MaxLength = 2;

			txtYear.Height = cboMonth.Height;
			txtDay.Height = cboMonth.Height;

			// Maintain the viewstate copy of the selected month
			if (Enabled)
			{
				SelectedMonthIndex = cboMonth.SelectedIndex;
			}
		}

		public object Value
		{
			get
			{
				return DateValue;
			}
			set
			{
				SetDateValue(value);
			}
		}

		public System.DateTime DateValue
		{
			get
			{
				System.DateTime datTemp = new System.DateTime();
				if (SelectedMonthIndex <= 0)
				{
					return mcdatNullDate;
				}
				try
				{
					datTemp = System.DateTime.Parse(DateAsString(), CultureInfo.InvariantCulture);
					if (datTemp.Equals(System.DateTime.Today) & this.TodayAddsTime)
					{
						datTemp = System.DateTime.Now;
					}
				}
				catch (FormatException)
				{
					datTemp = mcdatNullDate;
				}
				return datTemp;
			}
		}
		public void SetDateValue(object pdatValue)
		{

			if ((pdatValue == null) || System.Convert.IsDBNull(pdatValue))
			{
				pdatValue = mcdatNullDate;
			}
			System.DateTime ValueAsDate = new System.DateTime();
			if ((pdatValue) is System.DateTime)
			{
				ValueAsDate = System.Convert.ToDateTime(pdatValue);
			}
			else if (sfRepUtils.IsDate(pdatValue))
			{
				ValueAsDate = System.DateTime.Parse(pdatValue.ToString());
			}
			else
			{
				throw new ArgumentException("Invalid DateValue");
			}
			if (ValueAsDate == mcdatNullDate)
			{
				// blank
				txtDay.Text = string.Empty;
				SelectedMonthIndex = 0;
				txtYear.Text = string.Empty;
			}
			else
			{
				txtDay.Text = System.Convert.ToString(ValueAsDate.Day);
				SelectedMonthIndex = ValueAsDate.Month;
				txtYear.Text = System.Convert.ToString(ValueAsDate.Year);
			}

			// if enabled, then set combo
			if (this.Enabled)
			{
				cboMonth.SelectedIndex = SelectedMonthIndex;
			}
			lblMain.Text = Text;

		}

		protected internal override void EnableChildControls(bool Value)
		{
			lblMain.Visible = ! Value;
			txtDay.Visible = Value;
			cboMonth.Visible = Value;
			txtYear.Visible = Value;
			// Set up the Month combo if necessary
			if (cboMonth.Visible)
			{
				FillMonthCombo();
				cboMonth.SelectedIndex = SelectedMonthIndex;
			}
			else
			{
				cboMonth.Items.Clear();
			}
			valDate.Visible = Value;
		}

		private void FillMonthCombo()
		{

			int i = 0;

			if (cboMonth.Items.Count == 0)
			{
				cboMonth.Items.Add(new ListItem(string.Empty));
//INSTANT C# NOTE: The ending condition of VB 'For' loops is tested only on entry to the loop. Instant C# has created a temporary variable in order to use the initial value of ShortMonths.Length for every iteration:
				int tempFor1 = ShortMonths.Length;
				for (i = 0; i < tempFor1; i++)
				{
					cboMonth.Items.Add(new ListItem(ShortMonths[i]));
				}
			}

		}

		private string DateAsString()
		{
			string strMonth = null;
				string retValue = null;
			if (SelectedMonthIndex > 0)
			{
				strMonth = ShortMonths[SelectedMonthIndex - 1];
			}
			else
			{
				strMonth = " ";
			}
			retValue = txtDay.Text + "/" + strMonth + "/" + txtYear.Text;
			return retValue;
		}

		// Returns true if the date entered is blank
		public bool IsBlank
		{
			get
			{
				return (txtDay.Text.Trim().Length == 0 & SelectedMonthIndex <= 0 & txtYear.Text.Trim().Length == 0);
			}
		}

		public override string Text
		{
			get
			{
				if (! IsBlank)
				{
					return DateAsString();
				}
				else
				{
					return string.Empty;
				}
			}
			set
			{
				throw new InvalidOperationException("You cannot set the Text property of an SFDate control.");
			}
		}

		private int SelectedMonthIndex
		{
			get
			{
				if (ViewState[ViewStateSelectedMonth] == null)
				{
					return 0;
				}
				else
				{
					return int.Parse(ViewState[ViewStateSelectedMonth].ToString(), CultureInfo.InvariantCulture);
				}
			}
			set
			{
				ViewState[ViewStateSelectedMonth] = value;
			}
		}

		public bool IsValidDate
		{
			get
			{
				if (txtYear.Text.Trim().Length == 0)
				{
					return false;
				}
				else if (sfRepUtils.IsNumeric(txtYear.Text) && Convert.ToInt16(txtYear.Text) < 1753)
				{
					return false;
				}
				if (SelectedMonthIndex < 1)
				{
					return false;
				}
				if (txtDay.Text.Trim().Length == 0)
				{
					return false;
				}
				try
				{
					int intYear = int.Parse(txtYear.Text, CultureInfo.InvariantCulture);
					int intDay = int.Parse(txtDay.Text, CultureInfo.InvariantCulture);
					System.DateTime Value = new System.DateTime(intYear, SelectedMonthIndex, intDay);
				}
				catch (FormatException)
				{
					return false;
				}
				catch (ArgumentOutOfRangeException)
				{
					return false;
				}
				catch (ArgumentException)
				{
					return false;
				}
				return true;
			}
		}

		public bool TodayAddsTime
		{
			get
			{
				return m_TodayAddsTime;
			}
			set
			{
				m_TodayAddsTime = value;
			}
		}

		public override void SetValue(object value)
		{
			SetDateValue(value);
		}

		protected bool IsValid()
		{
			return (IsValidDate | (! Required & IsBlank));
		}

		private void valDate_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
		{
			if (! (IsValid()))
			{
				args.IsValid = false;
			}
		}

		public string ErrorMessage
		{
			get
			{
				return valDate.ErrorMessage;
			}
			set
			{
				valDate.ErrorMessage = value;
			}
		}

		public bool Required
		{
			get
			{
				return m_Required;
			}
			set
			{
				m_Required = value;
			}
		}

	}

} //end of root namespace