<%@ Page Language="c#" Codebehind="ChangePasswordPage.aspx.cs" AutoEventWireup="True"
    Inherits="Frond.ChangePasswordPage" %>
<%@ Register Src="UserControls/Title.ascx" TagName="Title" TagPrefix="uc3" %>
<%@ Register Src="UserControls/MenuBar.ascx" TagName="MenuBar" TagPrefix="uc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
test changes
    <title>ChangePassword</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <asp:literal id="literalBeforeCss" runat="server"></asp:literal>
	<LINK rel="stylesheet" type="text/css" href="Css/FrondDefault.css">
	<asp:literal id="literalAfterCss" runat="server"></asp:literal>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div id="baseMenu">
            <uc2:MenuBar ID="MenuBar1" runat="server" />
        </div>
        <div id="baseContent">
            <uc3:Title ID="titleBar" runat="server" />
            <p align="center">
                <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></p>
            <p align="center">
                <table id="Table1" width="100%" border="0">
                    <tr>
                        <td width="50%">
                            <p align="right">
                                New password
                            </p>
                        </td>
                        <td width="50%">
                            <p align="left">
                                <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password"></asp:TextBox></p>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            <p align="right">
                                Confirm new password</p>
                        </td>
                        <td width="50%">
                            <asp:TextBox ID="txtConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox></td>
                    </tr>
                </table>
            </p>
            <p align="center">
                <asp:Button ID="btnChange" runat="server" Text="Change" OnClick="btnChange_Click"></asp:Button></p>
            <p align="center">
                &nbsp;</p>
        </div>
    </form>
</body>
</html>
