//---------------------------------------------------------------------------------------
// Javascript used for Google map manipulations
// Main functions : Capture Latitude, Longitude for the given address
//                  Find Locations using Latitude, Longitude
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
// local variable declarations
//---------------------------------------------------------------------------------------
var totLines;
var startIdx;
var geocoder;
var pageName = '';
var latVal='';
var lngVal='';
var mode ='';
var marker;
var geoMessages=[];
var addressLines=[];
var dragListener;
var message = '';
var fldLat;
var fldLng;
var map;
var geocoder;
var operation = 'Capture';

//---------------------------------------------------------------------------------------
// Actions to be taken during page load
//---------------------------------------------------------------------------------------
function initialize() 
{
   // mapCtrl - Panel used for displaying Google map
   if (!(pageName == ''))
   {
   if (!(mapCtrl == null))
   {
     if (document.getElementById(mapCtrl))
     {
         //Browser compatibility check   
         if (GBrowserIsCompatible())
         {
            var lMapCtrl = new GLargeMapControl();
            var mapTypeCtrl = new GMapTypeControl();
            map = new GMap2(document.getElementById(mapCtrl));
            // Add Large Zoom control to Map
            map.addControl(lMapCtrl);
            // Add Map types control to Map
            map.addControl(mapTypeCtrl);
            // adjust to show full World Map in the view
            setDefaultCenter();
            // 'Search' - Called by Find locations pages
            // 'Capture' - Called by Capture Latitude, Longitude pages
            if (operation == 'Search') 
            {
                DisplaySearch();
            }
            else if (operation == 'Capture')
            {
                setLatLngFields();
                displayLocation();
                setMode();
            }
         }
         else
         {
            document.getElementById('gErrorMessage').innerText = "Browser doesn't support Google Map.";
         }
      }
      }
   }
}

//---------------------------------------------------------------------------------------       
// Configure Latitude Longitude Fields for each page.
// Latitude Longitude Field Id's varies for diferent page.
//---------------------------------------------------------------------------------------
function setLatLngFields()
{
    var ctrlName;
    
    if (pageName == 'AddressPage')
        {
            fldLat    = document.getElementById('showLocation_txtLatitude');
            fldLng    = document.getElementById('showLocation_txtLongitude');
        }
        
     if (pageName == 'EmployerAddressPage')
        {
            fldLat    = document.getElementById('showLocation_txtLatitude');
            fldLng    = document.getElementById('showLocation_txtLongitude');
        }
             if (pageName == 'OvsOrganisationAddressPage')
        {
            fldLat    = document.getElementById('showLocation_txtLatitude');
            fldLng    = document.getElementById('showLocation_txtLongitude');
        }
        
    if (pageName == 'OrganisationAddressPage' || pageName == 'WizardWendy' )
        {
            fldLat    = document.getElementById('showLocation_txtLatitude');
            fldLng    = document.getElementById('showLocation_txtLongitude');
        }
        
    if (pageName == 'WizardNewIndividual' || pageName =='WizardNewEnquiry')
        {   
            ctrlName = 'IndividualAddressPanel1';
            fldLat    = document.getElementById(ctrlName + '_showLocation_txtLatitude');
            fldLng    = document.getElementById(ctrlName + '_showLocation_txtLongitude');
        }
        
    if (pageName == 'WizardNewOrganisation' || pageName =='WizardNewWorkAddress')
        {
            ctrlName = 'uxOrganisationAddressUserControl';
            fldLat  = document.getElementById(ctrlName + '_showLocation_txtLatitude');
            fldLng  = document.getElementById(ctrlName + '_showLocation_txtLongitude');
        }
   

}

//---------------------------------------------------------------------------------------
// Add Address lines to array.
// Address Field Id's varies for diferent page.
//---------------------------------------------------------------------------------------
function setAddressLines()
{   
    var selectedIndex;
    var ctrlName;
    totLines = 7
    if (pageName == 'AddressPage')
        {
            addressLines[0]  = document.getElementById('textAddressLine1').value;
            addressLines[1]  = document.getElementById('textAddressLine2').value;
            addressLines[2]  = document.getElementById('textAddressLine3').value;
            addressLines[3]  = document.getElementById('textAddressLine4').value;
            addressLines[4]  = document.getElementById('textAddressCounty').value;
            selectedIndex = document.getElementById('dropDownAddressCountryID').selectedIndex;
	        addressLines[5]  = document.getElementById('dropDownAddressCountryID').options[selectedIndex].text;
            addressLines[6]  = document.getElementById('textAddressPostcode').value;
        }
            if (pageName == 'EmployerAddressPage')
        {
            addressLines[0]  = document.getElementById('BnbTextBox1').value;
            addressLines[1]  = document.getElementById('BnbTextBox2').value;
            addressLines[2]  = document.getElementById('BnbTextBox3').value;
            addressLines[3]  = document.getElementById('BnbTextBox4').value;
            addressLines[4]  = document.getElementById('BnbTextBox5').value;
            selectedIndex = document.getElementById('BnbLookupControl1').selectedIndex;
	        addressLines[5]  = document.getElementById('BnbLookupControl1').options[selectedIndex].text;
            addressLines[6]  = document.getElementById('BnbTextBox6').value;
        }
      if (pageName == 'OvsOrganisationAddressPage')
        {
            addressLines[0]  = document.getElementById('BnbTextBox1').value;
            addressLines[1]  = document.getElementById('BnbTextBox2').value;
            addressLines[2]  = document.getElementById('BnbTextBox3').value;
            addressLines[3]  = document.getElementById('BnbTextBox4').value;
            addressLines[4]  = document.getElementById('BnbTextBox5').value;
            selectedIndex = document.getElementById('BnbLookupControl1').selectedIndex;
	        addressLines[5]  = document.getElementById('BnbLookupControl1').options[selectedIndex].text;
            addressLines[6]  = document.getElementById('BnbTextBox6').value;
        }
    if (pageName == 'OrganisationAddressPage')
        {
            addressLines[0]  = document.getElementById('uxLine1').value;
            addressLines[1]  = document.getElementById('uxLine2').value;
            addressLines[2]  = document.getElementById('uxLine3').value;
            addressLines[3]  = document.getElementById('uxLine4').value;
            addressLines[4]  = document.getElementById('uxCounty').value;
            selectedIndex = document.getElementById('uxCountry').selectedIndex;
	        addressLines[5]  = document.getElementById('uxCountry').options[selectedIndex].text;
            addressLines[6]  = document.getElementById('uxPostcode').value;
        }
    if (pageName == 'WizardWendy')
        {
            addressLines[0]  = document.getElementById('txtAddressLine1').value;
            addressLines[1]  = document.getElementById('txtAddressLine2').value;
            addressLines[2]  = document.getElementById('txtAddressLine3').value;
            addressLines[3]  = document.getElementById('txtAddressLine4').value;
            addressLines[4]  = document.getElementById('txtCounty').value;
            selectedIndex = document.getElementById('cmbCountry').selectedIndex;
	        addressLines[5]  = document.getElementById('cmbCountry').options[selectedIndex].text;
            addressLines[6]  = document.getElementById('txtPostcode').value;
        }
    if (pageName == 'WizardNewIndividual' || pageName =='WizardNewEnquiry')
        {   
            ctrlName = 'IndividualAddressPanel1';
            addressLines[0]  = document.getElementById(ctrlName + '_txtAddressLine1').value;
            addressLines[1]  = document.getElementById(ctrlName + '_txtAddressLine2').value;
            addressLines[2]  = document.getElementById(ctrlName + '_txtAddressLine3').value;
            addressLines[3]  = document.getElementById(ctrlName + '_txtAddressLine4').value;
            addressLines[4]  = document.getElementById(ctrlName + '_txtCounty').value;
            selectedIndex = document.getElementById(ctrlName + '_drpCountry').selectedIndex;
	        addressLines[5]  = document.getElementById(ctrlName + '_drpCountry').options[selectedIndex].text;
            addressLines[6]  = document.getElementById(ctrlName + '_txtPostcode').value;
        }
    if (pageName == 'WizardNewOrganisation' || pageName =='WizardNewWorkAddress')
        {
            ctrlName = 'uxOrganisationAddressUserControl';
            addressLines[0]    = document.getElementById(ctrlName + '_uxAddressLine1').value;
            addressLines[1]    = document.getElementById(ctrlName + '_uxAddressLine2').value;
            addressLines[2]  = document.getElementById(ctrlName + '_uxAddressLine3').value;
            addressLines[3]  = document.getElementById(ctrlName + '_uxAddressLine4').value;
            addressLines[4]  = document.getElementById(ctrlName + '_uxCounty').value;
            selectedIndex = document.getElementById(ctrlName + '_uxCountryID').selectedIndex;
            addressLines[5]  = document.getElementById(ctrlName + '_uxCountryID').options[selectedIndex].text;
            addressLines[6]  = document.getElementById(ctrlName + '_uxPostcode').value;
        }

}

//---------------------------------------------------------------------------------------
// Disable marker drag event. 
//---------------------------------------------------------------------------------------        
function disableMarkerDrag()
{
  if (marker)
  {
    marker.disableDragging();
  }
}

//---------------------------------------------------------------------------------------
// Used during page load - If has lat, lng values, display those in the Map.
// ('9999', '9999') pair is an invalid location for this application.
//---------------------------------------------------------------------------------------
function displayLocation()
{
        if (!((latVal == '' && lngVal == '') || (latVal == '9999' && lngVal == '9999')))
        {
            var markPoint;
            markPoint = new GLatLng(latVal, lngVal);
            placeMarker(markPoint);
        }
        else 
        {
            setDefaultCenter();
            if (latVal == '9999' && lngVal == '9999')
            {
                clearLatLng();
                document.getElementById('gErrorMessage').innerText = 
                'The existing Latitude / Longitude value of this record ' +
                'is Invalid or may be Confidential and cannot be displayed';
            }
        }
}

//---------------------------------------------------------------------------------------
// Used by GShowLocation user control - Handles control visibilty
//---------------------------------------------------------------------------------------
function setMode()
{
        document.getElementById('tdUserHelp').innerText= 'Address can be fine-tuned by dragging '+
                                                         'and dropping the balloon marker - Latitude '+
                                                         'and Longitude values will be '+
                                                         'automatically updated.';

        if (mode == 'view')
        {
            document.getElementById('tdUserHelp').innerText= '';
            disableMarkerDrag();
        }
        else if (mode == 'new/edit' || (latVal == '9999' && lngVal == '9999'))
        {
             
            setMarkerEvent_dragEnd();
        }
        if (latVal == '9999' && lngVal == '9999')
        {
            clearLatLng();
        }
        setMarkerEvent_click();
}

//---------------------------------------------------------------------------------------
// Set default center to the Map (To view the full world map - (40,8),Zoom - 1)
//---------------------------------------------------------------------------------------
function setDefaultCenter()
{   
    if (pageName == 'FindGeographical')
        map.setCenter(new GLatLng(0, 0), 1);
    else
        map.setCenter(new GLatLng(0, 0), 1);
    geocoder = new GClientGeocoder();
}


//---------------------------------------------------------------------------------------
// Configure Google error messages
//---------------------------------------------------------------------------------------
function AssignGeoErrorMessages()
{
    geoMessages[G_GEO_SUCCESS]            = "Success";
    geoMessages[G_GEO_MISSING_ADDRESS]    = "Missing Address: The address was either missing or had no value.";
    geoMessages[G_GEO_UNKNOWN_ADDRESS]    = "Unknown Address:  No corresponding geographic location could be found for the specified address.";
    geoMessages[G_GEO_UNAVAILABLE_ADDRESS]= "Unavailable Address:  The geocode for the given address cannot be returned due to legal or contractual reasons.";
    geoMessages[G_GEO_BAD_KEY]            = "Bad Key: The API key is either invalid or does not match the domain for which it was given";
    geoMessages[G_GEO_TOO_MANY_QUERIES]   = "Too Many Queries: The daily geocoding quota for this site has been exceeded.";
    geoMessages[G_GEO_SERVER_ERROR]       = "Server error: The geocoding request could not be successfully processed.";
} 

//---------------------------------------------------------------------------------------
// To capture Latitude and longitude of address lines.
//---------------------------------------------------------------------------------------
function showLocation()
{

    startIdx = 0;
    totLines = 0;
    AssignGeoErrorMessages();
    
    setAddressLines();
    clearLatLng();
    locateNextAddress();
}

//---------------------------------------------------------------------------------------
// Locate next address.
//---------------------------------------------------------------------------------------
function locateNextAddress()
{
   if(startIdx < totLines)
    {
     
     locateAddress(locateNextAddress);
     startIdx  = startIdx + 1;
    }
    else
    {
     document.getElementById('gErrorMessage').innerText = message;
    }
}

//---------------------------------------------------------------------------------------
// string Trim() function.
//---------------------------------------------------------------------------------------
function trim(inputString)
{
	var startIdx = 0; 
	var totLength =inputString.length -1;
	while(startIdx < inputString.length && inputString[startIdx] == ' ')
	{	startIdx++; }
	while(totLength > startIdx && inputString[totLength] == ' ')
	{	totLength-=1;	}
	return inputString.substring(startIdx, totLength+1);
}

//---------------------------------------------------------------------------------------
// Display the Latitude and Longitude in the corresponding fields.
//---------------------------------------------------------------------------------------
function setLatLng(latLng)
{
    fldLat.innerText = latLng.lat();
    fldLng.innerText = latLng.lng();
}

//---------------------------------------------------------------------------------------
// Clear the Latitude,Longitude field values. 
//---------------------------------------------------------------------------------------
function clearLatLng()
{
    fldLat.innerText = '';
    fldLng.innerText = '';
}

//---------------------------------------------------------------------------------------
// Group address lines to locate in map.
//---------------------------------------------------------------------------------------
function formAddress()
{
    var idx;
    var addressGroup = '';
    for (idx = startIdx; idx < totLines; idx++)
    {
          if (!(trim(addressLines[idx]) == ''))
          {
              addressGroup +=addressLines[idx];
              addressGroup+=',';
          }
     }
     if (!(addressGroup == ''))
        return addressGroup.substring(0, addressGroup.length-1);
     else
        return addressGroup;
}

//---------------------------------------------------------------------------------------
// Locate the address in map.
//---------------------------------------------------------------------------------------
function locateAddress(next) 
{

    var addressGroup = '';
    addressGroup = formAddress();

    geocoder.getLocations(addressGroup, 
    function(response)
    {
      
        if (response && response.Status.code == 200)
        {
            document.getElementById('gErrorMessage').innerText = '';
            place = response.Placemark[0];
            var markPoint;
            markPoint = new GLatLng(place.Point.coordinates[1], place.Point.coordinates[0]);
            placeMarker(markPoint);
            setLatLng(marker.getLatLng());
            setMarkerEvent_dragEnd();
            setMarkerEvent_click();
         
         }
        else
        {    map.clearOverlays();
             map.setCenter(new GLatLng(0,0),1);
             message = "Error occured : Code - " + response.Status.code;
             if (geoMessages[response.Status.code]) 
             {
                message = geoMessages[response.Status.code];
             }
             next();
        }

    }
    );
}

//---------------------------------------------------------------------------------------
//Marker dragend Event
//---------------------------------------------------------------------------------------
function setMarkerEvent_dragEnd()
{
    if (marker)
    {
         GEvent.addListener(marker, "dragend", 
          function()
          {
             setLatLng(marker.getLatLng());
          }
          );
    }
}

//---------------------------------------------------------------------------------------
//Marker click Event
//---------------------------------------------------------------------------------------
function setMarkerEvent_click()
{
    if (marker)
    {
          GEvent.addListener(marker, "click", 
          function()
          {
            geocoder.getLocations(marker.getLatLng(),
            function(response)
            {
              marker.openInfoWindowHtml(response.Placemark[0].address);
            }
            );
          }
          );
     }
}

//---------------------------------------------------------------------------------------
// Create and place marker over the Map
//---------------------------------------------------------------------------------------
function placeMarker(point)
{
    map.clearOverlays();
    marker = new GMarker(point,{draggable: true,
                                autoPan: false,
                                clickable: true, 
                                bouncy: false});
    map.addOverlay(marker);
    map.setCenter(marker.getLatLng(),4);
}
      
//------------------------------------ End of File----------------------------------------